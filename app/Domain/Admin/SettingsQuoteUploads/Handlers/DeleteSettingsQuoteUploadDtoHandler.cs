﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using Domain.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;

namespace Domain.SettingsQuoteUploads.Handlers
{
    public class DeleteSettingsQuoteUploadDtoHandler : ExistingEntityDtoHandler<SettingsQuoteUpload, DeleteSettingsQuoteUploadDto, int>
    {
        private IRepository _repository;

        public DeleteSettingsQuoteUploadDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(SettingsQuoteUpload entity, DeleteSettingsQuoteUploadDto dto, HandlerResult<int> result)
        {
            entity.Delete();
            result.Processed(entity.Id);
        }
    }
}