﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;

namespace Domain.Admin.SettingsQuoteUploads.Handlers
{
    public class EditSettingsQuoteUploadsDtoHandlers : BaseDtoHandler<EditSettingsQuoteUploadsDto, bool>
    {
        private IRepository _repository;
        public EditSettingsQuoteUploadsDtoHandlers(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            _repository = repository;
        }

        
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void InternalHandle(EditSettingsQuoteUploadsDto dto, HandlerResult<bool> result)
        {
            foreach(var setting in dto.Settings)
            {
                var entity = _repository.GetById<SettingsQuoteUpload>(setting.Id);
                entity.Update(dto.ChannelId, dto.ProductId, dto.Environment, setting.SettingName, setting.SettingValue);
                _repository.Save(entity);
            }
            result.Processed(true);
        }
    }
}