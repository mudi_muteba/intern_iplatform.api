﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.SettingsQuoteUploads.Overrides
{
    public class SettingsQuoteUploadOverrides : IAutoMappingOverride<SettingsQuoteUpload>
    {
        public void Override(AutoMapping<SettingsQuoteUpload> mapping)
        {
            mapping.References(x => x.Product);
            mapping.References(x => x.Channel);
        }
    }
}
