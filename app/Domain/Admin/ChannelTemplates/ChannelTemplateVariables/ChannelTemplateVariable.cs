﻿using System;

using AutoMapper;

using Domain.Base;

using iPlatform.Api.DTOs.Admin.ChannelTemplateVariables;

using MasterData;

namespace Domain.Admin.Channels.ChannelTemplates.ChannelTemplateVariables
{
    public class ChannelTemplateVariable : EntityWithGuid
    {
        public ChannelTemplateVariable()
        {
            IsDeleted = false;
        }

        public virtual ChannelTemplate ChannelTemplate { get; set; }
        public virtual Guid ChannelTemplateSystemId { get; set; }
        public virtual TemplateVariableType TemplateVariableType { get; set; }

        public static ChannelTemplateVariable Create(ChannelTemplateVariableDto dto)
        {
            return Mapper.Map<ChannelTemplateVariable>(dto);
        }

        public virtual void Update(ChannelTemplateVariableDto dto)
        {
            Mapper.Map(dto, this);
        }
    }
}