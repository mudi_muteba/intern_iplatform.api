﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.Channels.ChannelTemplates.ChannelTemplateVariables.Overrides
{
    public class ChannelTemplateAttachmentVariablesOverride : IAutoMappingOverride<ChannelTemplateVariable>
    {
        public void Override(AutoMapping<ChannelTemplateVariable> mapping)
        {
            mapping.References(x => x.ChannelTemplate).Cascade.SaveUpdate();
        }
    }
}
