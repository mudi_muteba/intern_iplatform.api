﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using iPlatform.Api.DTOs.Admin.ChannelTemplateVariables;

using MasterData;

namespace Domain.Admin.Channels.ChannelTemplates.ChannelTemplateVariables.Mappings
{
    public class ChannelTemplateVariablesMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ChannelTemplateVariableDto, ChannelTemplateVariable>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<ChannelTemplateVariable, ChannelTemplateVariableDto>()
                .ForMember(t => t.TemplateVariableType, o => o.MapFrom(s => new TemplateVariableTypes().First(x => x.Id == s.TemplateVariableType.Id)))
                ;

            Mapper.CreateMap<List<ChannelTemplateVariableDto>, List<ChannelTemplateVariable>>()
                ;

            Mapper.CreateMap<List<ChannelTemplateVariable>, List<ChannelTemplateVariableDto>>()
                ;
        }
    }
}