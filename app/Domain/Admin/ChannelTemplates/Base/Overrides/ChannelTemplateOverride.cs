﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

using FluentNHibernate.Mapping;

namespace Domain.Admin.Channels.ChannelTemplates.Overrides
{
    public class ChannelTemplateOverride : IAutoMappingOverride<ChannelTemplate>
    {
        public void Override(AutoMapping<ChannelTemplate> mapping)
        {
            mapping.HasMany(x => x.Attachments).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Variables).Cascade.SaveUpdate();

            mapping.Map(x => x.Content).CustomType("StringClob").CustomSqlType("text");

            mapping.References(x => x.Channel, "ChannelId").LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.TemplateContextType).LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.TemplateType).LazyLoad(Laziness.NoProxy);
        }
    }
}