﻿using System.Collections.Generic;
using System.Linq;

using Domain.Admin.Channels.ChannelTemplates.ChannelTemplateAttachments;
using Domain.Admin.Channels.ChannelTemplates.ChannelTemplateVariables;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin.ChannelTemplates;

using MasterData.Authorisation;

namespace Domain.Admin.Channels.ChannelTemplates.Handlers
{
    public class CreateMultipleChannelTemplatesHandler : BaseDtoHandler<CreateMultipleChannelTemplatesDto, int>
    {
        private IRepository repository;
        public CreateMultipleChannelTemplatesHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(CreateMultipleChannelTemplatesDto dto, HandlerResult<int> result)
        {
            dto.ChannelTemplates
                .GroupBy(x => x.ChannelId)
                .Select(x => x.First())
                .ToList()
                .ForEach(x =>
                {
                    repository.GetAll<ChannelTemplate>()
                        .Where(y => !y.IsDeleted && y.Channel.Id == x.ChannelId)
                        .ToList()
                        .ForEach(y =>
                        {
                            repository.GetAll<ChannelTemplateAttachment>()
                                .Where(z => z.ChannelTemplate.Id == y.Id)
                                .ToList()
                                .ForEach(z =>
                                {
                                    z.Delete();
                                    repository.Save(z);
                                });

                            repository.GetAll<ChannelTemplateVariable>()
                                .Where(z => z.ChannelTemplate.Id == y.Id)
                                .ToList()
                                .ForEach(z =>
                                {
                                    z.Delete();
                                    repository.Save(z);
                                });

                            y.Delete();
                            repository.Save(y);
                        });
                });

            dto.ChannelTemplates
                .ForEach(x =>
                {
                    repository.Save(ChannelTemplate.Create(x));
                });

            result.Processed(1);
        }
    }
}