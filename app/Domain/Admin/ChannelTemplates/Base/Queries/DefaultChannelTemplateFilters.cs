﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Admin.Channels.ChannelTemplates.Queries
{
    public class DefaultChannelTemplateFilters : IApplyDefaultFilters<ChannelTemplate>
    {
        public IQueryable<ChannelTemplate> Apply(ExecutionContext executionContext, IQueryable<ChannelTemplate> query)
        {
            return query.Where(c => c.IsDeleted == false);
        }
    }
}