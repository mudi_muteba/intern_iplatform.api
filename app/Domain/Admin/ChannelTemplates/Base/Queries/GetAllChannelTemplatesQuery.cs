﻿using System.Collections.Generic;
using System.Linq;

using Domain.Admin.Channels.ChannelTemplates;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using MasterData.Authorisation;

namespace Domain.Admin.Channeltemplates.Queries
{
    public class GetAllChannelTemplatesQuery : BaseQuery<ChannelTemplate>, IChannelFreeQuery
    {
        public GetAllChannelTemplatesQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ChannelTemplate>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        protected internal override IQueryable<ChannelTemplate> Execute(IQueryable<ChannelTemplate> query)
        {
            return query;
        }
    }
}