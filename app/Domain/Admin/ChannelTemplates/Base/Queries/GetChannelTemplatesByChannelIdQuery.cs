﻿using System;
using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin.ChannelTemplates;

using MasterData.Authorisation;

namespace Domain.Admin.Channels.ChannelTemplates.Queries
{
    public class GetChannelTemplatesByChannelIdQuery : BaseQuery<ChannelTemplate>, ISearchQuery<SearchChannelTemplatesDto>
    {
        private SearchChannelTemplatesDto _criteria;

        public GetChannelTemplatesByChannelIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ChannelTemplate>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>(){};
            }
        }

        public void WithCriteria(SearchChannelTemplatesDto criteria)
        {
            this._criteria = criteria;
        }

        public GetChannelTemplatesByChannelIdQuery WithSystemId(Guid systemId)
        {
            this._criteria = new SearchChannelTemplatesDto { SystemId = systemId };
            return this;
        }

        protected internal override IQueryable<ChannelTemplate> Execute(IQueryable<ChannelTemplate> query)
        {
            return query.Where(x => x.Channel.Id == _criteria.ChannelId);
        }
    }
}