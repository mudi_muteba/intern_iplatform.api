﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin.ChannelTemplates;

using MasterData.Authorisation;

namespace Domain.Admin.Channels.ChannelTemplates.Queries
{
    public class SearchChannelTemplatesQuery : BaseQuery<ChannelTemplate>, ISearchQuery<SearchChannelTemplatesDto>
    {
        private SearchChannelTemplatesDto _criteria;

        public SearchChannelTemplatesQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultChannelTemplateFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchChannelTemplatesDto criteria)
        {
            this._criteria = criteria;
        }

        protected internal override IQueryable<ChannelTemplate> Execute(IQueryable<ChannelTemplate> query)
        {
            if (_criteria.ChannelId != 0)
                query = query.Where(x => x.Channel.Id == _criteria.ChannelId);

            if (_criteria.TemplateType != null)
                query = query.Where(x => x.TemplateType.Id == _criteria.TemplateType.Id);

            if (_criteria.TemplateContextType != null)
                query = query.Where(x => x.TemplateContextType.Id == _criteria.TemplateContextType.Id);

            return query;
        }
    }
}