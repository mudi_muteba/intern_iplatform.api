﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Admin.Channels.ChannelTemplates.ChannelTemplateAttachments;
using Domain.Admin.Channels.ChannelTemplates.ChannelTemplateVariables;

using Domain.Base;

using iPlatform.Api.DTOs.Admin.ChannelTemplates;

using MasterData;

namespace Domain.Admin.Channels.ChannelTemplates
{
    public class ChannelTemplate : Entity
    {
        public ChannelTemplate()
        {
            IsDeleted = false;
        }

        public virtual Channel Channel { get; set; }

        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Content { get; set; }
        public virtual string Extension { get; set; }
        public virtual string ContentType { get; set; }

        public virtual TemplateType TemplateType { get; set; }
        public virtual TemplateContextType TemplateContextType { get; set; }

        public virtual bool IsDefault { get; set; }

        public virtual IList<ChannelTemplateAttachment> Attachments { get; protected internal set; }
        public virtual IList<ChannelTemplateVariable> Variables { get; protected internal set; }

        public static ChannelTemplate Create(CreateChannelTemplateDto dto)
        {
            return Mapper.Map<ChannelTemplate>(dto);
        }
    }
}