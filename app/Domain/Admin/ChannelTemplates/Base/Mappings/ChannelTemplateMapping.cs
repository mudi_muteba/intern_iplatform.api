﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin.ChannelTemplates;
using iPlatform.Api.DTOs.Base;

using MasterData;

namespace Domain.Admin.Channels.ChannelTemplates.Mappings
{
    public class ChannelTemplateMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateChannelTemplateDto, ChannelTemplate>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)))
                .ForMember(t => t.TemplateContextType, o => o.MapFrom(s => new TemplateContextTypes().First(x => x.Id == s.TemplateContextType.Id)))
                .ForMember(t => t.TemplateType, o => o.MapFrom(s => new TemplateTypes().First(x => x.Id == s.TemplateType.Id)))
                .ForMember(t => t.Attachments, o => o.MapFrom(s => s.Attachments))
                .ForMember(t => t.Variables, o => o.MapFrom(s => s.Variables))
                ;

            Mapper.CreateMap<ChannelTemplate, ChannelTemplateDto>()
                .ForMember(t => t.TemplateContextType, o => o.MapFrom(s => new TemplateContextTypes().First(x => x.Id == s.TemplateContextType.Id)))
                .ForMember(t => t.TemplateType, o => o.MapFrom(s => new TemplateTypes().First(x => x.Id == s.TemplateType.Id)))
                .ForMember(t => t.Attachments, o => o.MapFrom(s => s.Attachments))
                .ForMember(t => t.Variables, o => o.MapFrom(s => s.Variables))
                ;

            Mapper.CreateMap<List<ChannelTemplate>, ListResultDto<ChannelTemplateDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<PagedResults<ChannelTemplate>, PagedResultDto<ChannelTemplateDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s.Results))
                ;

            Mapper.CreateMap<List<ChannelTemplate>, ListResultDto<ChannelTemplateDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;
        }
    }
}