﻿using System;

using AutoMapper;

using Domain.Base;

using iPlatform.Api.DTOs.Admin.ChannelTemplateAttachments;

namespace Domain.Admin.Channels.ChannelTemplates.ChannelTemplateAttachments
{
    public class ChannelTemplateAttachment : EntityWithGuid
    {
        public ChannelTemplateAttachment()
        {
            IsDeleted = false;
        }

        public virtual ChannelTemplate ChannelTemplate { get; set; }
        public virtual Guid ChannelTemplateSystemId { get; set; }

        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Extension { get; set; }
        public virtual string Attachment { get; set; }
        public virtual string ContentType { get; set; }

        public static ChannelTemplateAttachment Create(ChannelTemplateAttachmentDto dto)
        {
            return Mapper.Map<ChannelTemplateAttachment>(dto);
        }

        public virtual void Update(ChannelTemplateAttachmentDto dto)
        {
            Mapper.Map(dto, this);
        }
    }
}