﻿using System.Collections.Generic;

using AutoMapper;

using iPlatform.Api.DTOs.Admin.ChannelTemplateAttachments;

namespace Domain.Admin.Channels.ChannelTemplates.ChannelTemplateAttachments.Mappings
{
    public class ChannelTemplateAttachmentMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ChannelTemplateAttachmentDto, ChannelTemplateAttachment>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<ChannelTemplateAttachment, ChannelTemplateAttachmentDto>()
                ;

            Mapper.CreateMap<List<ChannelTemplateAttachmentDto>, List<ChannelTemplateAttachment>>()
                ;

            Mapper.CreateMap<List<ChannelTemplateAttachment>, List<ChannelTemplateAttachmentDto>>()
                ;
        }
    }
}