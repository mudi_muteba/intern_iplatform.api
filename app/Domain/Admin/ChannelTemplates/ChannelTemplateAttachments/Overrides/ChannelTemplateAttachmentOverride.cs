﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.Channels.ChannelTemplates.ChannelTemplateAttachments.Overrides
{
    public class ChannelTemplateAttachmentOverride : IAutoMappingOverride<ChannelTemplateAttachment>
    {
        public void Override(AutoMapping<ChannelTemplateAttachment> mapping)
        {
            mapping.Map(x => x.Attachment).CustomType("StringClob").CustomSqlType("text");

            mapping.References(x => x.ChannelTemplate).Cascade.SaveUpdate();
        }
    }
}
