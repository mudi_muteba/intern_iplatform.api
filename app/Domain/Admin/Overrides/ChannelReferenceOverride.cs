﻿using Domain.Admin;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.Overrides
{
    public class ChannelReferenceOverride : IAutoMappingOverride<ChannelReference>
    {
        public void Override(AutoMapping<ChannelReference> mapping)
        {
        }
    }
}