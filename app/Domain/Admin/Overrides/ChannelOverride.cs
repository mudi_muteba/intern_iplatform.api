﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Admin.Overrides
{
    public class ChannelOverride : IAutoMappingOverride<Channel>
    {
        public void Override(AutoMapping<Channel> mapping)
        {
            mapping.HasMany(c => c.ChannelConfigurations).Inverse().Cascade.SaveUpdate();

            mapping.HasMany(c => c.ChannelTemplates).Cascade.SaveUpdate();
            mapping.HasMany(c => c.EmailCommunicationSettings).Cascade.SaveUpdate();
            mapping.HasMany(c => c.SMSCommunicationSettings).Cascade.SaveUpdate();
            mapping.HasMany(c => c.Reports).Cascade.SaveUpdate();

            mapping.References(c => c.Organization);
        }
    }
}