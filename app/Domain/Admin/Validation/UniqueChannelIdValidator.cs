﻿using System.Linq;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Admin;
using ValidationMessages.Channels;

namespace Domain.Admin.Validation
{
    public class UniqueChannelIdValidator : IValidateDto<CreateChannelDto>
    {
        private readonly GetChannelById _query;

        public UniqueChannelIdValidator(IProvideContext contextProvider, IRepository repository)
        {
            _query = new GetChannelById(contextProvider, repository);
        }

        public void Validate(CreateChannelDto dto, ExecutionResult result)
        {
            var existingChannel = GetExistingChannelById(dto.Id);

            if (existingChannel.Any())
            {
                result.AddValidationMessage(ChannelValidationMessages.DuplicateId.AddParameters(new[] { dto.Id.ToString() }));
            }
        }

        private IQueryable<Channel> GetExistingChannelById(int id)
        {
            return _query.WithChannelId(id).ExecuteQuery();
        }
    }
}
