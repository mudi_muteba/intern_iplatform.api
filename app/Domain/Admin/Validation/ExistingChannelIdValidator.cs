﻿using System;
using System.Linq;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Admin.ChannelSettings;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Admin.ChannelEvents;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using iPlatform.Api.DTOs.PolicyBindings;
using ValidationMessages.Channels;

namespace Domain.Admin.Validation
{
    public class ExistingChannelIdValidator : 
        IValidateDto<SaveSettingsQuoteUploadsDto>, 
        IValidateDto<EditSettingsQuoteUploadsDto>, 
        IValidateDto<CreateSettingsiRateDto>, 
        IValidateDto<EditSettingsiRateDto>, 
        IValidateDto<CreateChannelSettingDto>,
        IValidateDto<CreateUserChannelDto>,
        IValidateDto<CreateChannelEventDto>,
        IValidateDto<EditChannelEventDto>,
        IValidateDto<EditMapVapQuestionDefinitionDto>,
        IValidateDto<CreateMapVapQuestionDefinitionDto>,
        IValidateDto<SavePolicyBindingDto>,
        IValidateDto<GetPolicyBindingByQuoteDto>,
        IValidateDto<SubmitPolicyBindingDto>
    {
        private readonly GetChannelById _queryById;

        private readonly GetChannelBySystemIdQuery _queryBySystemId;

        public ExistingChannelIdValidator(IProvideContext contextProvider, IRepository repository)
        {
            _queryById = new GetChannelById(contextProvider, repository);
            _queryBySystemId = new GetChannelBySystemIdQuery(contextProvider, repository);
        }

        private IQueryable<Channel> GetExistingChannelById(int channelId)
        {
            return _queryById.WithChannelId(channelId).ExecuteQuery();
        }

        private IQueryable<Channel> GetExistingChannelBySystemId(Guid channelSystemId)
        {
            return _queryBySystemId.WithSystemId(channelSystemId).ExecuteQuery();
        }

        private void Validate(int channelId, ExecutionResult result)
        {
            var channel = GetExistingChannelById(channelId);

            if (!channel.Any())
            {
                result.AddValidationMessage(ChannelValidationMessages.ChannelIdInvalid);
            }
        }

        private void Validate(Guid channelSystemId, ExecutionResult result)
        {
            var channel = GetExistingChannelBySystemId(channelSystemId);
            
            if (!channel.Any())
            {
                result.AddValidationMessage(ChannelValidationMessages.SystemIdNotExist);
            }
            
        }

        public void Validate(EditMapVapQuestionDefinitionDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }
        public void Validate(CreateMapVapQuestionDefinitionDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(CreateChannelEventDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(EditChannelEventDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }
        public void Validate(CreateEmailCommunicationSettingDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(EditSettingsiPersonDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }
        public void Validate(CreateSettingsiPersonDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(SaveSettingsQuoteUploadsDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(EditSettingsQuoteUploadsDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(CreateSettingsiRateDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(EditSettingsiRateDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(CreateChannelSettingDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(CreateUserChannelDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(SavePolicyBindingDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelSystemId, result);
        }

        public void Validate(GetPolicyBindingByQuoteDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }

        public void Validate(SubmitPolicyBindingDto dto, ExecutionResult result)
        {
            Validate(dto.ChannelId, result);
        }
    }
}
