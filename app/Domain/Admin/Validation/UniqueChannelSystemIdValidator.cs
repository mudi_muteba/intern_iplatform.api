﻿using System;
using System.Linq;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Admin;
using ValidationMessages.Channels;

namespace Domain.Admin.Validation
{
    public class UniqueChannelSystemIdValidator : IValidateDto<CreateChannelDto>
    {
        private readonly GetChannelBySystemIdQuery _query;

        private readonly GetChannelById _queryById;

        public UniqueChannelSystemIdValidator(IProvideContext contextProvider, IRepository repository)
        {
            _query = new GetChannelBySystemIdQuery(contextProvider, repository);
            _queryById = new GetChannelById(contextProvider, repository);
        }

        public void Validate(CreateChannelDto dto, ExecutionResult result)
        {
            var existingChannel = GetExistingChannelBySystemId(dto.SystemId);

            if (existingChannel.Any())
            {
                result.AddValidationMessage(ChannelValidationMessages.DuplicateSystemId.AddParameters(new[] { dto.SystemId.ToString() }));
            }
        }

        private IQueryable<Channel> GetExistingChannelBySystemId(Guid systemId)
        {
            return _query.WithSystemId(systemId).ExecuteQuery();
        }
    }
}
