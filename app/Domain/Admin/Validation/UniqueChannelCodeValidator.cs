﻿using System.Linq;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Admin;
using ValidationMessages.Channels;

namespace Domain.Admin.Validation
{
    public class UniqueChannelCodeValidator : IValidateDto<CreateChannelDto>, IValidateDto<EditChannelDto>
    {
        private readonly GetChannelByCode _query;
        private readonly GetAllChannelsQuery _queryAllChannels;

        public UniqueChannelCodeValidator(IProvideContext contextProvider, IRepository repository)
        {
            _query = new GetChannelByCode(contextProvider, repository);
            _queryAllChannels = new GetAllChannelsQuery(contextProvider, repository);
        }

        public void Validate(CreateChannelDto dto, ExecutionResult result)
        {
            var existingChannel = GetExistingChannelByCode(dto.Code);

            if (existingChannel.Any())
            {
                result.AddValidationMessage(ChannelValidationMessages.DuplicateCode.AddParameters(new[] { dto.Code }));
            }
        }

        private IQueryable<Channel> GetExistingChannelByCode(string code)
        {
            return _query.WithCode(code).ExecuteQuery();
        }

        private IQueryable<Channel> GetAllExistingChannels()
        {
            return _queryAllChannels.ExecuteQuery();
        }

        public void Validate(EditChannelDto dto, ExecutionResult result)
        {
            var existingChannel = GetAllExistingChannels();

            if (existingChannel.Any())
            {
                var channel = existingChannel.Where(c => c.Code == dto.Code && c.Id != dto.Id).ToList();

                if (channel.Any())
                {
                    result.AddValidationMessage(ChannelValidationMessages.DuplicateCode.AddParameters(new[] { dto.Code }));
                }
            }
        }
    }
}
