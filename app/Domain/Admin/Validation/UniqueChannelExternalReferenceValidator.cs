﻿using System.Linq;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Admin;
using ValidationMessages.Channels;

namespace Domain.Admin.Validation
{
    public class UniqueChannelExternalReferenceValidator : IValidateDto<CreateChannelDto>, IValidateDto<EditChannelDto>
    {
        private readonly GetChannelByExternalReference _query;
        private readonly GetAllChannelsQuery _queryAll;
        public UniqueChannelExternalReferenceValidator(GetChannelByExternalReference query, GetAllChannelsQuery queryAll)
        {
            _query = query;
            _queryAll = queryAll;

        }

        public void Validate(CreateChannelDto dto, ExecutionResult result)
        {
            var existingChannel = GetExistingChannelByReference(dto.ExternalReference);

            if (existingChannel.Any())
            {
                result.AddValidationMessage(ChannelValidationMessages.DuplicateExternalReference.AddParameters(new[] { dto.ExternalReference }));
            }
        }

        private IQueryable<Channel> GetExistingChannelByReference(string externalReference)
        {
            return _query.WithExternalReference(externalReference).ExecuteQuery();
        }

        private IQueryable<Channel> GetAllExistingChannels()
        {
            return _queryAll.ExecuteQuery();
        }

        public void Validate(EditChannelDto dto, ExecutionResult result)
        {
            var existingChannel = GetAllExistingChannels();

            if (existingChannel.Any())
            {
                var channel = existingChannel.Where(c => c.ExternalReference == dto.ExternalReference && c.Id != dto.Id).ToList();

                if (channel.Any())
                {
                    result.AddValidationMessage(ChannelValidationMessages.DuplicateExternalReference.AddParameters(new[] { dto.ExternalReference }));
                }
            }
        }
    }
}
