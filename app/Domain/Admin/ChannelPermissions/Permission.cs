﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Admin.ChannelPermissions
{
    public class Permission
    {
        public virtual bool IsAllowed { get; private set; }

        public Permission()
        {
        }

        public Permission(bool isAllowed)
        {
            IsAllowed = isAllowed;
        }
    }
}
