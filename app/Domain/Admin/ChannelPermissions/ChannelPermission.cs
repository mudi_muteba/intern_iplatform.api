﻿using AutoMapper;
using Domain.Base;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Admin.ChannelPermissions
{
    public class ChannelPermission : Entity
    {
        public ChannelPermission()
        {
            IsDeleted = false;
        }

        public ChannelPermission(Guid channelId, Product product, Permission allowProposalCreation, Permission allowQuoting,
            Permission allowSecondLevelUnderwriting)
        {
            ChannelId = channelId;
            Product = product;
            AllowProposalCreation = allowProposalCreation.IsAllowed;
            AllowQuoting = allowQuoting.IsAllowed;
            AllowSecondLevelUnderwriting = allowSecondLevelUnderwriting.IsAllowed;
        }
        public virtual Product Product { get; protected internal set; }
        public virtual Guid ChannelId { get; protected internal set; }
        public virtual bool AllowProposalCreation { get; protected internal set; }
        public virtual bool AllowQuoting { get; protected internal set; }
        public virtual bool AllowSecondLevelUnderwriting { get; protected internal set; }

        public static ChannelPermission Create(CreateChannelPermissionDto dto)
        {
            var entity = Mapper.Map<ChannelPermission>(dto);

            return entity;
        }

        public virtual void Update(EditChannelPermissionDto dto)
        {
            Mapper.Map(dto, this);
        }

        public virtual void Disable(DisableChannelPermissionDto dto)
        {
            Delete();
        }

    }
}
