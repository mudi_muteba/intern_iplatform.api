﻿using Domain.Admin;
using Domain.Products;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.ChannelPermissions.Overrides
{
    public class ChannelPermissionOverride : IAutoMappingOverride<ChannelPermission>
    {
        public void Override(AutoMapping<ChannelPermission> mapping)
        {
            mapping.References(c => c.Product, "ProductId");
        }
    }
}