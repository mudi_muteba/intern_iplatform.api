﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Culture;
using System.Linq;
using System.Collections.Generic;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;

namespace Domain.Admin.ChannelPermissions.Mappings
{
    public class ChannelPermissionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ChannelPermission, ChannelPermissionDto>()
                .ForMember(t => t.ProductCode, o => o.MapFrom(s => s.Product.ProductCode))
                .ForMember(t => t.AllowProposalCreation, o => o.MapFrom(s => new PermissionDto(s.AllowProposalCreation)))
                .ForMember(t => t.AllowQuoting, o => o.MapFrom(s => new PermissionDto(s.AllowQuoting)))
                .ForMember(t => t.AllowSecondLevelUnderwriting, o => o.MapFrom(s => new PermissionDto(s.AllowSecondLevelUnderwriting)))
                .ForMember(t => t.InsurerCode, o => o.MapFrom(s => s.Product.ProductOwner.Code))
                ;

            Mapper.CreateMap<List<ChannelPermission>, ListResultDto<ChannelPermissionDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<CreateChannelPermissionDto, ChannelPermission>()
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }))
                ;

            Mapper.CreateMap<EditChannelPermissionDto, ChannelPermission>()
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }))
                ;
        }
    }
}