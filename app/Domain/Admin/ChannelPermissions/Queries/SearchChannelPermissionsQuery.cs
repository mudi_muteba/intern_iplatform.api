﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using System;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;

namespace Domain.Admin.ChannelPermissions.Queries
{
    public class SearchChannelPermissionsQuery : BaseQuery<ChannelPermission>, ISearchQuery<ChannelPermissionSearchDto>
    {
        private ChannelPermissionSearchDto criteria;

        public SearchChannelPermissionsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ChannelPermission>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(ChannelPermissionSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<ChannelPermission> Execute(IQueryable<ChannelPermission> query)
        {
            if (!string.IsNullOrEmpty(criteria.ChannelId))
                query = query.Where(a => a.ChannelId == Guid.Parse(criteria.ChannelId));

            //is not needed if product code is provided
            if (!string.IsNullOrEmpty(criteria.InsurerCode))
                query = query.Where(a => a.Product.ProductOwner.Code == criteria.InsurerCode);

            if (!string.IsNullOrEmpty(criteria.ProductCode))
                query = query.Where(a => a.Product.ProductCode == criteria.ProductCode);

            return query;
        }
    }

    public static class GuidHelper
    {
        public static bool IsNullOrEmpty(this Guid? guid)
        {
          return (!guid.HasValue || guid.Value == Guid.Empty);
        }
    }

}