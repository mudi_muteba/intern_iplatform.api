﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using System;

namespace Domain.Admin.ChannelPermissions.Queries
{
    public class GetChannelPermissionsQuery : BaseQuery<ChannelPermission>
    {
        public GetChannelPermissionsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ChannelPermission>())
        {
        }

        private Guid channelId;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }
        public GetChannelPermissionsQuery WithChannelId(Guid channelId)
        {
            this.channelId = channelId;
            return this;
        }

        protected internal override IQueryable<ChannelPermission> Execute(IQueryable<ChannelPermission> query)
        {
            return query.Where(c => c.ChannelId == channelId);
        }
    }
}