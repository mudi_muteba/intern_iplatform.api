﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin;
using Domain.Products;
using System.Linq;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;

namespace Domain.Admin.ChannelPermissions.Handlers
{
    public class CreateChannelPermissionHandler : CreationDtoHandler<ChannelPermission, CreateChannelPermissionDto, int>
    {
        private IRepository repository;
        public CreateChannelPermissionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            this.repository = repository;
        }

        
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(ChannelPermission entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ChannelPermission HandleCreation(CreateChannelPermissionDto dto, HandlerResult<int> result)
        {
            var entity = ChannelPermission.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}