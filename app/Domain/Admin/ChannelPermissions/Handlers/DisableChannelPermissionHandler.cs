﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;

namespace Domain.Admin.ChannelPermissions.Handlers
{
    public class DisableChannelPermissionHandler : ExistingEntityDtoHandler<ChannelPermission, DisableChannelPermissionDto, int>
    {
        public DisableChannelPermissionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(ChannelPermission entity, DisableChannelPermissionDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}