﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using Domain.Admin.ChannelPermissions.Handlers;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using Domain.Admin.ChannelPermissions;
using Domain.Products;

namespace Domain.Campaigns.Handlers
{
    public class EditChannelPermissionHandler : ExistingEntityDtoHandler<ChannelPermission, EditChannelPermissionDto, int>
    {
        public EditChannelPermissionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(ChannelPermission entity, EditChannelPermissionDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}