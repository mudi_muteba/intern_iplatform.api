﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Products;
using ValidationMessages.Campaigns;
using Domain.Products.Queries;
using Domain.Products;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using ValidationMessages.Channels;

namespace Domain.Admin.ChannelPermissions.Validations
{
    public class EditChannelPermissionValidator : IValidateDto<EditChannelPermissionDto>
    {
        private readonly IRepository repository;
        private readonly IProvideContext contextProvider;
        public EditChannelPermissionValidator(IProvideContext contextProvider, IRepository repository)
        {
            this.contextProvider = contextProvider;
            this.repository = repository;
        }

        public void Validate(EditChannelPermissionDto dto, ExecutionResult result)
        {
            var product = repository.GetAll<Product>()
                .Where(x => x.ProductCode == dto.ProductCode &&
                    x.ProductOwner.Code == dto.InsurerCode).FirstOrDefault();

            if (product != null)
            {
                dto.ProductId = product.Id;

                var channelPermission = repository.GetAll<ChannelPermission>().
                    Where(x => x.ChannelId == dto.ChannelId && x.Product.Id == product.Id && x.IsDeleted != true && x.Id != dto.Id).FirstOrDefault();

                if(channelPermission != null)
                    result.AddValidationMessage(ChannelValidationMessages.ExistingChannelPermission);

            }
        }
    }
}