﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using ValidationMessages.Channels;

namespace Domain.Admin.ChannelPermissions.Validations
{
    public class CreateChannelPermissionValidator : IValidateDto<CreateChannelPermissionDto>
    {
        private readonly IRepository repository;
        private readonly IProvideContext contextProvider;
        public CreateChannelPermissionValidator(IProvideContext contextProvider, IRepository repository)
        {
            this.contextProvider = contextProvider;
            this.repository = repository;
        }

        public void Validate(CreateChannelPermissionDto dto, ExecutionResult result)
        {
            var product = repository.GetAll<Product>()
                .Where(x => x.ProductCode == dto.ProductCode &&
                    x.ProductOwner.Code == dto.InsurerCode).FirstOrDefault();
            if (product != null)
            {
                var channelPermission = repository.GetAll<ChannelPermission>().
                    Where(x => x.ChannelId == dto.ChannelId && x.Product.Id == product.Id && x.IsDeleted != true).FirstOrDefault();

                if(channelPermission != null)
                    result.AddValidationMessage(ChannelValidationMessages.ExistingChannelPermission);
            }
        }
    }
}