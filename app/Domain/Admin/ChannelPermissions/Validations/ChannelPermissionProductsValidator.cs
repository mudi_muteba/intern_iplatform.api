﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Products;
using ValidationMessages.Campaigns;
using Domain.Products.Queries;
using Domain.Products;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using ValidationMessages.Channels;

namespace Domain.Admin.ChannelPermissions.Validations
{
    public class ChannelPermissionProductsValidator : IValidateDto<CreateChannelPermissionDto>, IValidateDto<EditChannelPermissionDto>
    {
        private readonly IRepository repository;
        private readonly IProvideContext contextProvider;
        public ChannelPermissionProductsValidator(IProvideContext contextProvider, IRepository repository)
        {
            this.contextProvider = contextProvider;
            this.repository = repository;
        }

        public void Validate(CreateChannelPermissionDto dto, ExecutionResult result)
        {
            dto.ProductId = ValidateProduct(dto.ProductCode, dto.InsurerCode, result);
        }


        public void Validate(EditChannelPermissionDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.ProductCode, dto.InsurerCode, result);
        }


        private int ValidateProduct (string productCode, string insurerCode, ExecutionResult result)
        {
            var product = repository.GetAll<Product>()
                .Where(x => x.ProductCode == productCode &&
                    x.ProductOwner.Code == insurerCode).FirstOrDefault();

            if(product == null)
            {
                result.AddValidationMessage(ChannelValidationMessages.ProductInsurerCodeInvalid.AddParameters(new[] { productCode, insurerCode }));
                return 0;
            }

            return product.Id;  
        }
    }
}