﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Admin.Channels.EmailCommunicationSettings.Queries
{
    public class DefaultEmailCommunicationSettingFilters : IApplyDefaultFilters<EmailCommunicationSetting>
    {
        public IQueryable<EmailCommunicationSetting> Apply(ExecutionContext executionContext, IQueryable<EmailCommunicationSetting> query)
        {
            return query.Where(c => c.IsDeleted == false);
        }
    }
}