﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using MasterData.Authorisation;

using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;

namespace Domain.Admin.Channels.EmailCommunicationSettings.Queries
{
    public class SearchEmailCommunicationSettingsQuery : BaseQuery<EmailCommunicationSetting>, ISearchQuery<SearchEmailCommunicationsDto>
    {
        private SearchEmailCommunicationsDto criteria;

        public SearchEmailCommunicationSettingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultEmailCommunicationSettingFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchEmailCommunicationsDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<EmailCommunicationSetting> Execute(IQueryable<EmailCommunicationSetting> query)
        {

            if (!string.IsNullOrEmpty(criteria.Channel))
                query = query.Where(x => x.Channel.Name.Contains(criteria.Channel));

            if (!string.IsNullOrEmpty(criteria.host))
                query = query.Where(x => x.Host.Contains(criteria.host));

            return query;
        }
    }
}