﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using MasterData.Authorisation;

namespace Domain.Admin.Channels.EmailCommunicationSettings.Queries
{
    public class GetAllEmailCommunicationSettingsQuery : BaseQuery<EmailCommunicationSetting>
    {
        public GetAllEmailCommunicationSettingsQuery(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new DefaultEmailCommunicationSettingFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        protected internal override IQueryable<EmailCommunicationSetting> Execute(IQueryable<EmailCommunicationSetting> query)
        {
            return query;
        }
    }
}