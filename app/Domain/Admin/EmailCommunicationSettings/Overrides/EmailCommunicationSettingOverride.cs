﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

using FluentNHibernate.Mapping;

namespace Domain.Admin.Channels.EmailCommunicationSettings.Overrides
{
    public class EmailCommunicationSettingOverride : IAutoMappingOverride<EmailCommunicationSetting>
    {
        public void Override(AutoMapping<EmailCommunicationSetting> mapping)
        {
            mapping.References(x => x.Channel, "ChannelId").LazyLoad(Laziness.NoProxy);
        }
    }
}