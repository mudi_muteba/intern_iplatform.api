﻿using AutoMapper;

using Domain.Base.Repository;
using Domain.Emailing.Factory;

using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Base;

using System.Collections.Generic;

namespace Domain.Admin.Channels.EmailCommunicationSettings.Mappings
{
    public class EmailCommunicationSettingMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<EmailCommunicationSetting, EmailCommunicationSettingDto>()
                ;

            Mapper.CreateMap<EmailCommunicationSetting, AppSettingsEmailSetting>()
                .ForMember(t => t.CustomFrom, o => o.Ignore())
                ;

            Mapper.CreateMap<EmailCommunicationSettingDto, AppSettingsEmailSetting>();


            Mapper.CreateMap<CreateEmailCommunicationSettingDto, EmailCommunicationSetting>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)))
                ;

            Mapper.CreateMap<PagedResults<EmailCommunicationSetting>, PagedResultDto<EmailCommunicationSettingDto>>();

            Mapper.CreateMap<List<EmailCommunicationSetting>, ListResultDto<EmailCommunicationSettingDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<CreateEmailCommunicationSettingDto, EmailCommunicationSetting>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)));
        }
    }
}