﻿using AutoMapper;

using Domain.Base;

using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;

namespace Domain.Admin.Channels.EmailCommunicationSettings
{
    public class EmailCommunicationSetting : Entity
    {
        public EmailCommunicationSetting() { }

        public virtual Channel Channel { get; set; }

        public virtual string Host { get; set; }
        public virtual int Port { get; set; }
        public virtual bool UseSSL { get; set; }
        public virtual bool UseDefaultCredentials { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual string DefaultFrom { get; set; }
        public virtual string DefaultBCC { get; set; }
        public virtual string DefaultContactNumber { get; set; }
        public virtual string SubAccountID { get; set; }

        public static EmailCommunicationSetting Create(CreateEmailCommunicationSettingDto dto)
        {
            return Mapper.Map<EmailCommunicationSetting>(dto);
        }
    }
}
