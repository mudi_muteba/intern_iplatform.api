﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;

using MasterData.Authorisation;

namespace Domain.Admin.Channels.EmailCommunicationSettings.Handlers
{
    public class CreateMultipleEmailCommunicationSettingsHandler : BaseDtoHandler<CreateMultipleEmailCommunicationSettingsDto, int>
    {
        private IRepository repository;
        public CreateMultipleEmailCommunicationSettingsHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(CreateMultipleEmailCommunicationSettingsDto dto, HandlerResult<int> result)
        {
            dto.EmailCommunicationSettings
                .GroupBy(x => x.ChannelId)
                .Select(x => x.First())
                .ToList()
                .ForEach(x =>
                {
                    repository.GetAll<EmailCommunicationSetting>()
                        .Where(y => !y.IsDeleted && y.Channel.Id == x.ChannelId)
                        .ToList()
                        .ForEach(y =>
                        {
                            y.Delete();
                            repository.Save(y);
                        });
                });

            dto.EmailCommunicationSettings
                .ForEach(x =>
                {
                    repository.Save(EmailCommunicationSetting.Create(x));
                });

            result.Processed(1);
        }
    }
}