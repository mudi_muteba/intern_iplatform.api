﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetChannelBySystemIdQuery : BaseQuery<Channel>
    {
        private Guid id;

        public GetChannelBySystemIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Channel>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetChannelBySystemIdQuery WithSystemId(Guid id)
        {
            this.id = id;
            return this;
        }

        protected internal override IQueryable<Channel> Execute(IQueryable<Channel> query)
        {
            return query
                .Where(c => c.SystemId == id);
        }
    }
}