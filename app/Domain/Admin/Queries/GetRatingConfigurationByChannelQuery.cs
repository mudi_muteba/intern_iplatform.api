﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Ratings;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetRatingConfigurationByChannelQuery : BaseQuery<RatingConfiguration>
    {
        private Guid channelId;

        public GetRatingConfigurationByChannelQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultRatingConfigurationFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetRatingConfigurationByChannelQuery ForChannel(Guid channelId)
        {
            this.channelId = channelId;
            return this;
        }

        protected internal override IQueryable<RatingConfiguration> Execute(IQueryable<RatingConfiguration> query)
        {
            return query
                .Where(rs => rs.ChannelSystemId == channelId);
        }
    }
}