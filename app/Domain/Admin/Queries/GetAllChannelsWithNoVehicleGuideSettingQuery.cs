﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Lookups.VehicleGuide;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetAllChannelsWithNoVehicleGuideSettingQuery : BaseQuery<Channel>
    {
        private readonly IRepository _Repository;

        public GetAllChannelsWithNoVehicleGuideSettingQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Channel>())
        {
            _Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        /// <summary>
        /// Get a list of channels that do not have a vehicle guide setting configured.
        /// </summary>
        /// <param name="query"></param>
        protected internal override IQueryable<Channel> Execute(IQueryable<Channel> query)
        {
            List<int> channelIds = _Repository.GetAll<VehicleGuideSetting>()
                .Where(vgs => !vgs.IsDeleted)
                .Select(c => c.Channel.Id)
                .ToList();

            query = query.Where(c => !channelIds.Contains(c.Id));

            return query;
        }
    }
}
