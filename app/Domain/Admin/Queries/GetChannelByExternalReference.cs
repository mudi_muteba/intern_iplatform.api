﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetChannelByExternalReference : BaseQuery<Channel>
    {
        private string _externalReference;

        public GetChannelByExternalReference(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Channel>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetChannelByExternalReference WithExternalReference(string externalReference)
        {
            this._externalReference = externalReference;
            return this;
        }

        protected internal override IQueryable<Channel> Execute(IQueryable<Channel> query)
        {
            return query
                .Where(c => c.ExternalReference == _externalReference);
        }
    }
}
