﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Admin.Queries
{
    public class DefaultRatingUsersConfigurationFilters : IApplyDefaultFilters<SettingsiRateUser.SettingsiRateUser>
    {
        public IQueryable<SettingsiRateUser.SettingsiRateUser> Apply(ExecutionContext executionContext, IQueryable<SettingsiRateUser.SettingsiRateUser> query)
        {
            return query;
        }
    }
}