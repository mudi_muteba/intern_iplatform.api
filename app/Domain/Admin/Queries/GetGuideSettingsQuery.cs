using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Lookups.VehicleGuide;
using Domain.Ratings;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetGuideSettingsQuery : BaseQuery<VehicleGuideSetting>
    {
        private ChannelReference channel;

        public GetGuideSettingsQuery(IProvideContext contextProvider, 
            IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<VehicleGuideSetting>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetGuideSettingsQuery WithChannel(ChannelReference channel)
        {
            this.channel = channel;
            return this;
        }

        protected internal override IQueryable<VehicleGuideSetting> Execute(IQueryable<VehicleGuideSetting> query)
        {
            return query
                .Where(c => c.Channel.Id == channel.Id)
                .Where(c => c.IsDeleted != true);
        }
    }
}