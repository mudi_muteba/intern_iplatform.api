using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Lookups.Person;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetPersonLookupSettingsQuery : BaseQuery<PersonLookupSetting>
    {
        private int channelId;

        public GetPersonLookupSettingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PersonLookupSetting>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetPersonLookupSettingsQuery ForChannel(int channelId)
        {
            this.channelId = channelId;
            return this;
        }

        protected internal override IQueryable<PersonLookupSetting> Execute(IQueryable<PersonLookupSetting> query)
        {
            return query
                .Where(c => c.IsDeleted != true)
                .Where(c => c.Channel.Id == channelId);

        }
    }
}