﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Users;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetChannelsByUserIdQuery : BaseQuery<Channel>
    {
        private readonly IProvideContext _contextProvider;
        /*Gets channels by user id*/
        public GetChannelsByUserIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Channel>())
        {
            _contextProvider = contextProvider;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        protected internal override IQueryable<Channel> Execute(IQueryable<Channel> query)
        {
            var channelIds =_contextProvider.Get().ChannelIds.Split(',').ToList();
            query = query.Where(a=> channelIds.Contains(a.Id.ToString()));
            return query;
        }
    }
}