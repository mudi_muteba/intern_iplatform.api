﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetChannelById : BaseQuery<Channel>
    {
        private int channelId;

        public GetChannelById(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new NoDefaultFilters<Channel>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetChannelById WithChannelId(int channelId)
        {
            this.channelId = channelId;
            return this;
        }

        protected internal override IQueryable<Channel> Execute(IQueryable<Channel> query)
        {
            return query.Where(c => c.Id == channelId);
        }
    }
}