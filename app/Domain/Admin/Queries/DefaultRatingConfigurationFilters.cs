﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Ratings;

namespace Domain.Admin.Queries
{
    public class DefaultRatingConfigurationFilters : IApplyDefaultFilters<RatingConfiguration>
    {
        public IQueryable<RatingConfiguration> Apply(ExecutionContext executionContext, IQueryable<RatingConfiguration> query)
        {
            return query;
        }
    }
}