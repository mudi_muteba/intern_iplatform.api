﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetRatingUserConfigurationByUser : BaseQuery<SettingsiRateUser.SettingsiRateUser>
    {
        private int m_UserId;

        public GetRatingUserConfigurationByUser(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultRatingUsersConfigurationFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetRatingUserConfigurationByUser ForUser(int userId)
        {
            this.m_UserId = userId;
            return this;
        }

        protected internal override IQueryable<SettingsiRateUser.SettingsiRateUser> Execute(IQueryable<SettingsiRateUser.SettingsiRateUser> query)
        {
            return query.Where(rs => rs.User.Id == m_UserId);
        }
    }
}