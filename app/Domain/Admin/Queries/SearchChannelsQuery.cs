﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin;
using Shared.Extentions;
using iPlatform.Api.DTOs.Base;
using System;

namespace Domain.Admin.Queries
{
    public class SearchChannelsQuery : BaseQuery<Channel>, ISearchQuery<SearchChannelsDto>
    {
        public SearchChannelsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Channel>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }
        private SearchChannelsDto criteria;

        public void WithCriteria(SearchChannelsDto criteria)
        {
            if (!criteria.OrderBy.Any())
                criteria.OrderBy = new List<OrderByField>();

            this.criteria = criteria;
        }

        protected internal override IQueryable<Channel> Execute(IQueryable<Channel> query)
        {
            if (!string.IsNullOrEmpty(criteria.Name))
                query = query.Where(x => x.Name.StartsWith(criteria.Name));

            if (!string.IsNullOrEmpty(criteria.Code))
                query = query.Where(x => x.Code.StartsWith(criteria.Code));

            if (!criteria.SystemId.IsEmpty())
                query = query.Where(x => x.SystemId.Equals(criteria.SystemId));

            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);

            query = AddOrdering(query);

            return query;
        }

        private IQueryable<Channel> AddOrdering(IQueryable<Channel> query)
        {
            query = new OrderByBuilder<Channel>(OrderByDefinition).Build(query, criteria.OrderBy);

            return query;
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("Name"), () => "Name"},
                    {new OrderByField("Code"), () => "Code"},
                    {new OrderByField("IsDefault"), () => "IsDefault"},
                    {new OrderByField("IsActive"), () => "IsActive"},
                    {new OrderByField("ActivatedOn"), () => "ActivatedOn"},
                };
            }
        }
    }
}