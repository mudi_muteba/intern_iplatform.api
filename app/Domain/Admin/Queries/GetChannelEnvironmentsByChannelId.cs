﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetChannelEnvironmentsByChannelId : BaseQuery<ChannelEnvironment>
    {
        private int channelId;
        public string environment;

        public GetChannelEnvironmentsByChannelId(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new NoDefaultFilters<ChannelEnvironment>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetChannelEnvironmentsByChannelId WithChannelId(int channelId)
        {
            this.channelId = channelId;
            return this;
        }

        public GetChannelEnvironmentsByChannelId WithEnvironment(string environment)
        {
            this.environment = environment;
            return this;
        }

        protected internal override IQueryable<ChannelEnvironment> Execute(IQueryable<ChannelEnvironment> query)
        {
            if (environment != null)
                query = query.Where(x => x.Key.ToLower() == environment.ToLower());

            return query.Where(x => x.Channel.Id == channelId);
        }
    }
}