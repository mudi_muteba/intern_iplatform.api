﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using Domain.Admin.ChannelEvents;

namespace Domain.Admin.Queries
{
    public class GetChannelEventConfigurationQuery : BaseQuery<ChannelEvent>
    {
        private int channelId;
        private string eventName;
        private string productCode;

        public GetChannelEventConfigurationQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ChannelEvent>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetChannelEventConfigurationQuery ForChannel(int channelId)
        {
            this.channelId = channelId;
            return this;
        }

        public GetChannelEventConfigurationQuery EventName(string eventName)
        {
            this.eventName = eventName;
            return this;
        }

        public GetChannelEventConfigurationQuery ForProduct(string productCode)
        {
            this.productCode = productCode;
            return this;
        }

        protected internal override IQueryable<ChannelEvent> Execute(IQueryable<ChannelEvent> query)
        {
            if (!string.IsNullOrWhiteSpace(productCode))
            {
                query = query.Where(q => q.ProductCode == this.productCode);
            }

            return query.Where(q => q.Channel.Id == channelId && q.EventName == eventName);
        }
    }
}