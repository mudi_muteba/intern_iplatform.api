﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetAllChannelsQuery : BaseQuery<Channel>
    {
        public GetAllChannelsQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<Channel>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected internal override IQueryable<Channel> Execute(IQueryable<Channel> query)
        {
            return query.Where(x => ContextProvider.Get().Channels.Contains(x.Id)).OrderBy(q => q.Name);
        }
    }
}