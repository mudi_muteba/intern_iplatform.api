﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.Queries
{
    public class GetChannelByCode : BaseQuery<Channel>
    {
        private string _code;

        public GetChannelByCode(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Channel>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetChannelByCode WithCode(string code)
        {
            this._code = code;
            return this;
        }

        protected internal override IQueryable<Channel> Execute(IQueryable<Channel> query)
        {
            return query
                .Where(c => c.Code == _code);
        }
    }
}
