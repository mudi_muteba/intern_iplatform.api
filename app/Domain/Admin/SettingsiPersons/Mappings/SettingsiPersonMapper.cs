﻿using AutoMapper;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;

namespace Domain.Admin.SettingsiPersons.Mappings
{
    public class SettingsiPersonMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SettingsiPerson, SettingsiPersonDto>();

            Mapper.CreateMap<EditSettingsiPersonDto, SettingsiPerson>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }))
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel() { Id = s.ChannelId }))
                ;

            Mapper.CreateMap<PagedResults<SettingsiPerson>, PagedResultDto<ListSettingsiPersonDto>>();

            Mapper.CreateMap<List<SettingsiPerson>, ListResultDto<ListSettingsiPersonDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;
            Mapper.CreateMap<SettingsiPerson, ListSettingsiPersonDto>();

            Mapper.CreateMap<CreateSettingsiPersonDto, SettingsiPerson>()
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }))
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel() { Id = s.ChannelId }))
                ;

            Mapper.CreateMap<EditSettingsiPersonDto, CreateSettingsiPersonDto>();
        }
    }
}