﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiPersons.Handlers
{
    public class CreateSettingsiPersonHandler : CreationDtoHandler<SettingsiPerson, CreateSettingsiPersonDto, int>
    {
        private IRepository repository;
        public CreateSettingsiPersonHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void EntitySaved(SettingsiPerson entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override SettingsiPerson HandleCreation(CreateSettingsiPersonDto dto, HandlerResult<int> result)
        {
            var entity = SettingsiPerson.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}