﻿using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using MasterData.Authorisation;
using System.Linq;
using AutoMapper;

namespace Domain.Admin.SettingsiPersons.Handlers
{
    public class SaveMultipleSettingsiPersonDtoHandler : BaseDtoHandler<SaveMultipleSettingsiPersonDto, bool>
    {
        private IRepository _repository;
        public SaveMultipleSettingsiPersonDtoHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider)
        {
            this._repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void InternalHandle(SaveMultipleSettingsiPersonDto dto, HandlerResult<bool> result)
        {
            var settings = _repository.GetAll<SettingsiPerson>().Where(x => x.Channel.Id == dto.ChannelId && !x.IsDeleted).ToList();

            RemoveOldSettings(dto, settings);
            UpdateExistingSettings(dto, settings);
            AddNewSettings(dto, settings);
            result.Processed(true);
        }

        private void RemoveOldSettings(SaveMultipleSettingsiPersonDto dto, List<SettingsiPerson> settings)
        {
            var toBeDeleted = settings.Where(t => dto.Settings.All(y => y.ProductId != t.Product.Id)).ToList();

            foreach (var setting in toBeDeleted)
            {
                setting.Delete();
                _repository.Save(setting);
            }
        }

        private void UpdateExistingSettings(SaveMultipleSettingsiPersonDto dto, List<SettingsiPerson> settings)
        {
            var toBeUpdated = settings.Where(t => dto.Settings.Any(y => t.Product.Id == y.ProductId)).ToList();

            foreach (var setting in toBeUpdated)
            {
                var dtosetting = dto.Settings.First(x => x.ProductId.Equals(setting.Product.Id));

                Mapper.Map(dtosetting, setting);

                _repository.Save(setting);
            }
        }

        private void AddNewSettings(SaveMultipleSettingsiPersonDto dto, List<SettingsiPerson> settings)
        {
            var newSettings = dto.Settings.Where(x => settings.All(y => y.Product.Id != x.ProductId)).ToList();

            foreach (var setting in newSettings)
            {
                var createdto = Mapper.Map<CreateSettingsiPersonDto>(setting);
                var entity = SettingsiPerson.Create(createdto);
                _repository.Save(entity);
            }
        }
    }
}