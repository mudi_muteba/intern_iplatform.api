﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiPersons.Handlers
{
    public class EditSettingsiPersonHandler : ExistingEntityDtoHandler<SettingsiPerson, EditSettingsiPersonDto, int>
    {


        public EditSettingsiPersonHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(SettingsiPerson entity, EditSettingsiPersonDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}