﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiPersons.Handlers
{
    public class DisableSettingsiPersonHandler : ExistingEntityDtoHandler<SettingsiPerson, DeleteSettingsiPersonDto, int>
    {
        public DisableSettingsiPersonHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(SettingsiPerson entity, DeleteSettingsiPersonDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}