﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Admin.SettingsiPersons.Queries
{
    public class DefaultSettingsiPersonFilters : IApplyDefaultFilters<SettingsiPerson>
    {
        public IQueryable<SettingsiPerson> Apply(ExecutionContext executionContext, IQueryable<SettingsiPerson> query)
        {
            return query.Where(c => c.IsDeleted == false);
        }
    }
}