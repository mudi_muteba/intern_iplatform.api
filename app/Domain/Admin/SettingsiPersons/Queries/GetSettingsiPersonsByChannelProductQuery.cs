﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiPersons.Queries
{
    public class GetSettingsiPersonsByChannelProductQuery : BaseQuery<SettingsiPerson>
    {
        public GetSettingsiPersonsByChannelProductQuery(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new DefaultSettingsiPersonFilters())
        {
        }
        private int _productId;
        private int _channelId;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }


        public GetSettingsiPersonsByChannelProductQuery WithProductId(int productId)
        {
            _productId = productId;
            return this;
        }
        public GetSettingsiPersonsByChannelProductQuery WithChannelId(int channelId)
        {
            _channelId = channelId;
            return this;
        }
        protected internal override IQueryable<SettingsiPerson> Execute(IQueryable<SettingsiPerson> query)
        {
            if (_channelId > 0)
                query = query.Where(x => x.Channel.Id == _channelId);

            if (_productId > 0)
                query = query.Where(x => x.Product.Id == _productId);

            return query;
        }
    }
}