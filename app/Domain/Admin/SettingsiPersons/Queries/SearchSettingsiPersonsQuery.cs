﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base;
using System;

namespace Domain.Admin.SettingsiPersons.Queries
{
    public class SearchSettingsiPersonsQuery : BaseQuery<SettingsiPerson>, ISearchQuery<SearchSettingsiPersonDto>
    {
        private SearchSettingsiPersonDto criteria;

        public SearchSettingsiPersonsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultSettingsiPersonFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchSettingsiPersonDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<SettingsiPerson> Execute(IQueryable<SettingsiPerson> query)
        {
            if (!string.IsNullOrEmpty(criteria.SubscriberCode))
                query = query.Where(x => x.SubscriberCode.StartsWith(criteria.SubscriberCode));

            if (!string.IsNullOrEmpty(criteria.Channel))
                query = query.Where(x => x.Channel.Name.StartsWith(criteria.Channel));

            if (!string.IsNullOrEmpty(criteria.Product))
                query = query.Where(x => x.Product.Name.StartsWith(criteria.Product));

            if (!string.IsNullOrEmpty(criteria.Environment))
                query = query.Where(x => x.EnvironmentType.Name.StartsWith(criteria.Environment));

            query = AddOrdering(query);

            return query;
        }


        private IQueryable<SettingsiPerson> AddOrdering(IQueryable<SettingsiPerson> query)
        {
            query = new OrderByBuilder<SettingsiPerson>(OrderByDefinition).Build(query, criteria.OrderBy);

            return query;
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("SubscriberCode"), () => "SubscriberCode"},
                    {new OrderByField("Channel"), () => "Channel.Name"},
                    {new OrderByField("EnvironmentName"), () => "EnvironmentType.Name"},
                    {new OrderByField("EnquirerContactName"), () => "EnquirerContactName"},
                    {new OrderByField("EnquirerContactNumber"), () => "EnquirerContactPhoneNo"},
                };
            }
        }
    }
}