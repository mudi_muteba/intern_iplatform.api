﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiPersons.Queries
{
    public class GetAllSettingsiPersonsQuery : BaseQuery<SettingsiPerson>
    {
        public GetAllSettingsiPersonsQuery(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new DefaultSettingsiPersonFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        protected internal override IQueryable<SettingsiPerson> Execute(IQueryable<SettingsiPerson> query)
        {
            return query;
        }
    }
}