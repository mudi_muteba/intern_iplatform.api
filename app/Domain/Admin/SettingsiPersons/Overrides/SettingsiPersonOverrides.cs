﻿using System;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.SettingsiPersons.Overrides
{
    public class SettingsiPersonOverrides : IAutoMappingOverride<SettingsiPerson>
    {
        public void Override(AutoMapping<SettingsiPerson> mapping)
        {
            mapping.References(x => x.Product);
            mapping.References(x => x.Channel);
        }
    }
}
