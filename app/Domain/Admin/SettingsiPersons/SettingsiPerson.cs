﻿using System;
using Domain.Admin;
using Domain.Base;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using AutoMapper;
using MasterData;

namespace Domain.Admin.SettingsiPersons
{
    public class SettingsiPerson : Entity
    {
        public virtual Channel Channel { get; protected internal set; }
        public virtual Product Product { get; set; }
        public virtual bool CheckRequired { get; set; }
        public virtual string SubscriberCode { get; set; }
        public virtual string BranchNumber { get; set; }
        public virtual string BatchNumber { get; set; }
        public virtual string SecurityCode { get; set; }
        public virtual string EnquirerContactName { get; set; }
        public virtual string EnquirerContactPhoneNo { get; set; }
        public virtual EnvironmentType EnvironmentType { get; set; }

        public static SettingsiPerson Create(CreateSettingsiPersonDto dto)
        {
            var entity = Mapper.Map<SettingsiPerson>(dto);

            return entity;
        }

        public virtual void Update(EditSettingsiPersonDto dto)
        {
            Mapper.Map(dto, this);
        }

        public virtual void Disable(DeleteSettingsiPersonDto dto)
        {
            Delete();
        }
    }
}