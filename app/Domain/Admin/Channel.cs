﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using Domain.Admin.Channels.ChannelTemplates;
using Domain.Admin.Channels.EmailCommunicationSettings;
using Domain.Admin.Channels.SMSCommunicationSettings;

using Domain.Reports.Base;

using Domain.Organizations;
using iPlatform.Api.DTOs.Admin;
using MasterData;

namespace Domain.Admin
{
    public class Channel : ChannelReference
    {
        public virtual bool IsActive { get; protected internal set; }
        public virtual Guid SystemId { get; protected internal set; }
        public virtual DateTime? ActivatedOn { get; protected internal set; }
        public virtual DateTime? DeactivatedOn { get; protected internal set; }
        public virtual bool IsDefault { get; protected internal set; }
        public virtual Currency Currency { get; protected internal set; }
        public virtual string DateFormat { get; protected internal set; }
        public virtual Language Language { get; protected internal set; }
        public virtual string Name { get; protected internal set; }
        public virtual bool PasswordStrengthEnabled { get; protected internal set; }
        public virtual string Code { get; protected internal set; }
        public virtual Country Country { get; protected internal set; }

        public virtual IList<ChannelTemplate> ChannelTemplates { get; protected internal set; }
        public virtual IList<EmailCommunicationSetting> EmailCommunicationSettings { get; protected internal set; }
        public virtual IList<SMSCommunicationSetting> SMSCommunicationSettings { get; protected internal set; }
        public virtual IList<Report> Reports { get; protected internal set; }

        public virtual Organization Organization  { get; protected internal set; }
        public virtual string ExternalReference { get; set; }
        public virtual bool RequestBroker { get; set; }
        public virtual bool RequestAccountExecutive { get; set; }
        public virtual bool HasPolicyBinding { get; set; }

        public virtual Channel ParentChannel { get; set; }

        public Channel()
        {
            SystemId = Guid.NewGuid();

            ChannelTemplates = new List<ChannelTemplate>();
            EmailCommunicationSettings = new List<EmailCommunicationSetting>();
            SMSCommunicationSettings = new List<SMSCommunicationSetting>();
            Reports = new List<Report>();
        }

        public Channel(int channelId, bool isDefault)
        {
            SystemId = Guid.NewGuid();
            Id = channelId;
            IsDefault = isDefault;
        }

        public Channel(int channelId)
        {
            SystemId = Guid.NewGuid();
            Id = channelId;
        }

        public Channel(int id, int currencyId, string dateFormat, int languageId)
        {
            Id = id;
            Currency = new Currencies().FirstOrDefault(x => x.Id == currencyId);
            DateFormat = dateFormat;
            Language = new Languages().FirstOrDefault(x => x.Id == languageId);
        }

        public Channel(bool isActive, Guid systemId, DateTime? activatedOn, DateTime? deactivatedOn, bool isDefault): this()
        {

            IsActive = isActive;
            SystemId = systemId;
            ActivatedOn = activatedOn;
            DeactivatedOn = deactivatedOn;
            IsDefault = isDefault;
        }

        public Channel(int id, bool isActive, Guid systemId, DateTime? activatedOn, DateTime? deactivatedOn, bool isDefault) : this()
        {
            Id = id;
            IsActive = isActive;
            SystemId = systemId;
            ActivatedOn = activatedOn;
            DeactivatedOn = deactivatedOn;
            IsDefault = isDefault;
        }

        /// <summary>
        /// Constructor for Acceptance Tests/TestEntities
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isActive"></param>
        /// <param name="systemId"></param>
        /// <param name="activatedOn"></param>
        /// <param name="deactivatedOn"></param>
        /// <param name="isDefault"></param>
        /// <param name="code"></param>
        /// <param name="country"></param>
        public Channel(int id, bool isActive, Guid systemId, DateTime? activatedOn, DateTime? deactivatedOn, bool isDefault, string code, Country country) : this()
        {
            Id = id;
            IsActive = isActive;
            SystemId = systemId;
            ActivatedOn = activatedOn;
            DeactivatedOn = deactivatedOn;
            IsDefault = isDefault;
            Code = code;
            EmailCommunicationSettings = new List<EmailCommunicationSetting>();
            Country = country;
        }

        public Channel(int id, bool isActive, Guid systemId, DateTime? activatedOn, DateTime? deactivatedOn, bool isDefault, string code, Language language, Currency currency, Country country ) : this()
        {
            Id = id;
            IsActive = isActive;
            SystemId = systemId;
            ActivatedOn = activatedOn;
            DeactivatedOn = deactivatedOn;
            IsDefault = isDefault;
            Code = code;
            EmailCommunicationSettings = new List<EmailCommunicationSetting>();
            Language = language;
            Currency = currency;
            Country = country;
        }

        public virtual void Activate(Guid channelId, bool isDefault = false)
        {
            IsDefault = isDefault;
            if (IsActive)
                return;

            IsActive = true;
            ActivatedOn = DateTime.UtcNow;
            SystemId = channelId;
        }

        public virtual void Deactivate()
        {
            IsActive = false;
            DeactivatedOn = DateTime.UtcNow;
            IsDefault = false;
        }

        public virtual void NotDefault()
        {
            IsDefault = false;
        }

        protected bool Equals(Channel other)
        {
            return IsActive.Equals(other.IsActive) && SystemId.Equals(other.SystemId) && ActivatedOn.Equals(other.ActivatedOn) && DeactivatedOn.Equals(other.DeactivatedOn) && IsDefault.Equals(other.IsDefault);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Channel) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = IsActive.GetHashCode();
                hashCode = (hashCode*397) ^ SystemId.GetHashCode();
                hashCode = (hashCode*397) ^ ActivatedOn.GetHashCode();
                hashCode = (hashCode*397) ^ DeactivatedOn.GetHashCode();
                hashCode = (hashCode*397) ^ IsDefault.GetHashCode();
                return hashCode;
            }
        }

        public static Channel Create(CreateChannelDto createChannelDto)
        {
            var channel = Mapper.Map<Channel>(createChannelDto);

            return channel;
        }

        private void Activate()
        {
            IsActive = true;
            ActivatedOn = DateTime.UtcNow;
        }

        public virtual void Update(EditChannelDto editChannelDto)
        {
            Mapper.Map(editChannelDto, this);
            //Activate
            if (!IsActive && editChannelDto.IsActive)
                Activate();
            //Deactivate
            if (IsActive && !editChannelDto.IsActive)
                Deactivate();
        }

        /// <summary>
        /// Example:
        /// ^                               Start anchor
        /// (?=.*[A-Z].*[A-Z])              Ensure string has two uppercase letters.
        /// (?=.*[!@#$&*])                  Ensure string has one special case letter.
        /// (?=.*[0 - 9].*[0 - 9])          Ensure string has two digits.
        /// (?=.*[a - z].*[a - z].*[a - z]) Ensure string has three lowercase letters.
        /// .{8}                            Ensure string is of length 8.
        /// $                               End anchor.
        /// </summary>
        public virtual bool IsStrongPassword(string password = "")
        {
            var match = Regex.Match(password, "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8}$");
            return match.Success;
        }
    }
}