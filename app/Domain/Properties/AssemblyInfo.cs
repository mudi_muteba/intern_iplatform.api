﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("iPlatform.Unit.Tests")]
[assembly: InternalsVisibleTo("iPlatform.Acceptance.Tests")]
[assembly: InternalsVisibleTo("iPlatform.TestHelper")]
