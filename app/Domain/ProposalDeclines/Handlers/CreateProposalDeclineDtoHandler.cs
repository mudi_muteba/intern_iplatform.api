﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.ProposalDeclines;
using MasterData.Authorisation;

namespace Domain.ProposalDeclines.Handlers
{
    public class CreateProposalDeclineDtoHandler : CreationDtoHandler<ProposalDecline, CreateProposalDeclineDto, int>
    {
        public CreateProposalDeclineDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void EntitySaved(ProposalDecline entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ProposalDecline HandleCreation(CreateProposalDeclineDto dto, HandlerResult<int> result)
        {
            var proposalDeclineEntry = ProposalDecline.Create(dto);

            result.Processed(proposalDeclineEntry.Id);

            return proposalDeclineEntry;
        }
    }
}
