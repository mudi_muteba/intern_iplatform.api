﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.ProposalDeclines;

namespace Domain.ProposalDeclines.Mappings
{
    public class ProposalDeclineMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateProposalDeclineDto, ProposalDecline>();

            Mapper.CreateMap<ProposalDecline, ProposalDeclineDto>();

            Mapper.CreateMap<ProposalDeclineDto, ProposalDecline>();

            Mapper.CreateMap<List<ProposalDecline>, ListResultDto<ProposalDeclineDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<PagedResults<ProposalDecline>, PagedResultDto<ProposalDeclineDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<List<ProposalDecline>,ListResultDto<ProposalDecline>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));
        }
    }
}
