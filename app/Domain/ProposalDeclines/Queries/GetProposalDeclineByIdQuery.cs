﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.JsonDataStores;
using MasterData.Authorisation;

namespace Domain.ProposalDeclines.Queries
{
    public class GetProposalDeclineByIdQuery : BaseQuery<ProposalDecline>
    {
        public GetProposalDeclineByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProposalDecline>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public int id;
        public void WithGuid(int id)
        {
            this.id = id;
        }

        protected internal override IQueryable<ProposalDecline> Execute(IQueryable<ProposalDecline> query)
        {
            return query
                .Where(x => x.Id == id);
        }
    }
}
