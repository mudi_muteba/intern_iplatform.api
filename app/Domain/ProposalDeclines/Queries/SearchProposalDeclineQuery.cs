﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.JsonDataStores;
using iPlatform.Api.DTOs.JsonDataStores;
using iPlatform.Api.DTOs.ProposalDeclines;
using MasterData.Authorisation;
using NHibernate.Loader.Criteria;

namespace Domain.ProposalDeclines.Queries
{
    public class SearchProposalDeclineQuery : BaseQuery<ProposalDecline>, ISearchQuery<ProposalDeclineSearchDto>
    {
        private ProposalDeclineSearchDto criteria;

        public SearchProposalDeclineQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProposalDecline>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(ProposalDeclineSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<ProposalDecline> Execute(IQueryable<ProposalDecline> query)
        {
            if (criteria.PartyId >= 0)
            {
                query = query.Where(x => x.PartyId == criteria.PartyId)
                    .OrderByDescending(x => x.Id);
            }
            else if (!string.IsNullOrEmpty(criteria.DeclinedReason))
            {
                query = query.Where(x => x.DeclinedReason == criteria.DeclinedReason)
                    .OrderByDescending(x => x.Id);
            }
            else if (!string.IsNullOrEmpty(criteria.RiskItemDeclined))
            {
                query = query.Where(x => x.RiskItemDeclined == criteria.RiskItemDeclined)
                    .OrderByDescending(x => x.Id);
            }

            return query;
        }
    }
}
