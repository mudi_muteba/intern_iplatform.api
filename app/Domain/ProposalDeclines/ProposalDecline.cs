﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.ProposalDeclines;

namespace Domain.ProposalDeclines
{
    public class ProposalDecline : Entity
    {
        public virtual int PartyId { get; set; }
        public virtual string RiskItemDeclined { get; set; }
        public virtual string DeclinedReason { get; set; }
        public virtual DateTime TimeStamp { get; set; }

        public ProposalDecline()
        {

        }

        public static ProposalDecline Create(CreateProposalDeclineDto dto)
        {
            var proposalDecline = Mapper.Map<ProposalDecline>(dto);

            return proposalDecline;
        }
    }
}
