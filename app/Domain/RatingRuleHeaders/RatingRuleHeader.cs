﻿using System;
using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.RatingRuleHeaders;

namespace Domain.RatingRuleHeaders
{
   public class RatingRuleHeader: Entity
    {
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }

        public static RatingRuleHeader Create(CreateRatingRuleHeaderDto dto)
        {
           return Mapper.Map<RatingRuleHeader>(dto);
        }

        public virtual void Update(EditRatingRuleHeaderDto dto)
        {
             Mapper.Map<RatingRuleHeader>(dto);
        }

        public virtual void Delete(DeleteRatingRuleHeaderDto dto)
        {
            Delete();
        }

        public virtual void GetById()
        {

        }
    }
}
