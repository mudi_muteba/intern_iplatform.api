﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using MasterData.Authorisation;

namespace Domain.RatingRuleHeaders.Handlers
{
   public class CreateRatingRuleHeaderDtoHandler : CreationDtoHandler<RatingRuleHeader, CreateRatingRuleHeaderDto, int>
    {
        public CreateRatingRuleHeaderDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(RatingRuleHeader entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override RatingRuleHeader HandleCreation(CreateRatingRuleHeaderDto dto, HandlerResult<int> result)
        {
            return RatingRuleHeader.Create(dto);
        }
    }
}
