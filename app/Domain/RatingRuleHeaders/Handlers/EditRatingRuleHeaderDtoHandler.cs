﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using MasterData.Authorisation;

namespace Domain.RatingRuleHeaders.Handlers
{
    public class EditRatingRuleHeaderDtoHandler : ExistingEntityDtoHandler<RatingRuleHeader, EditRatingRuleHeaderDto, int>
    {
        public EditRatingRuleHeaderDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(RatingRuleHeader entity, EditRatingRuleHeaderDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
