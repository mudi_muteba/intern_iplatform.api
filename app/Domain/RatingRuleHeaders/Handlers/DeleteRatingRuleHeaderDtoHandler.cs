﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using MasterData.Authorisation;

namespace Domain.RatingRuleHeaders.Handlers
{
   public  class DeleteRatingRuleHeaderDtoHandler : ExistingEntityDtoHandler<RatingRuleHeader, DeleteRatingRuleHeaderDto, int>
    {
        public DeleteRatingRuleHeaderDtoHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(RatingRuleHeader entity, DeleteRatingRuleHeaderDto dto, HandlerResult<int> result)
        {
            entity.Delete();
            result.Processed(entity.Id);
        }
    }
}