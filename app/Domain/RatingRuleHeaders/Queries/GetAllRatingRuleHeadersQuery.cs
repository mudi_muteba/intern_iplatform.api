﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.RatingRuleHeaders.Queries
{
    public class GetAllRatingRuleHeadersQuery : BaseQuery<RatingRuleHeader>
    {
        public GetAllRatingRuleHeadersQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultRatingRuleHeadersFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected internal override IQueryable<RatingRuleHeader> Execute(IQueryable<RatingRuleHeader> query)
        {
            //TODO: Add ordering
            return query;
        }
    }
}
