﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.RatingRuleHeaders.Queries
{
    public class DefaultRatingRuleHeadersFilter : IApplyDefaultFilters<RatingRuleHeader>
    {
        public IQueryable<RatingRuleHeader> Apply(ExecutionContext executionContext, IQueryable<RatingRuleHeader> query)
        {
            return query.Where(q => !q.IsDeleted);
        }
    }
}
