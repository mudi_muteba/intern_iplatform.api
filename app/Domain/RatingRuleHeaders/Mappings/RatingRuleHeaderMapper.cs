﻿using AutoMapper;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using Domain.Base.Repository;
namespace Domain.RatingRuleHeaders.Mappings
{
    public class RatingRuleHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<RatingRuleHeader, RatingRuleHeaderDto>().ReverseMap();
            Mapper.CreateMap<RatingRuleHeader, CreateRatingRuleHeaderDto>().ReverseMap();
            Mapper.CreateMap<RatingRuleHeader, EditRatingRuleHeaderDto>().ReverseMap();
            Mapper.CreateMap<RatingRuleHeader, DeleteRatingRuleHeaderDto>().ReverseMap();
            Mapper.CreateMap<List<RatingRuleHeader>, PagedResultDto<ListRatingRuleHeaderDto>>();
            Mapper.CreateMap<PagedResults<RatingRuleHeader>, PagedResultDto<ListRatingRuleHeaderDto>>();

            Mapper.CreateMap<List<RatingRuleHeader>, ListResultDto<ListRatingRuleHeaderDto>>()
               .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<IEnumerable<RatingRuleHeader>, ListResultDto<ListRatingRuleHeaderDto>>()
                .ForMember(d => d.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<RatingRuleHeader, ListRatingRuleHeaderDto>().ReverseMap();
        }
    }
}
