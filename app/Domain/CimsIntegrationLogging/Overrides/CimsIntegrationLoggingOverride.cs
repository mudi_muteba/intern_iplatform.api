﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.CimsIntegrationLogging.Overrides
{
    public class CimsIntegrationLoggingOverride : IAutoMappingOverride<CimsConnectIntegrationLog>
    {
        public void Override(AutoMapping<CimsConnectIntegrationLog> mapping)
        {
            mapping.IgnoreProperty(x => x.IsDeleted);
            mapping.Map(x => x.ResponseData).Length(99999);
        }
    }
}