﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.CimsIntegrationLogging;

namespace Domain.CimsIntegrationLogging.Mappings
{
    public class CimsConnectIntegrationLogMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateCimsIntegrationLoggingDto, CimsConnectIntegrationLog>();
        }
    }
}