﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.CimsIntegrationLogging;

namespace Domain.CimsIntegrationLogging.Handlers
{
    public class CimsIntegrationLoggingHandler : CreationDtoHandler<CimsConnectIntegrationLog, CreateCimsIntegrationLoggingDto, int>
    {
        public CimsIntegrationLoggingHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            
        }

        
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(CimsConnectIntegrationLog entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override CimsConnectIntegrationLog HandleCreation(CreateCimsIntegrationLoggingDto dto, HandlerResult<int> result)
        {
            var cims = CimsConnectIntegrationLog.Create(dto);
            result.Processed(cims.Id);

            return cims;
        }

    }
}