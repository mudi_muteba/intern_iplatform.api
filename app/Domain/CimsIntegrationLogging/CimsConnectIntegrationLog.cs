﻿using Domain.Base;
using iPlatform.Api.DTOs.CimsIntegrationLogging;
using System;
using System.Collections.Generic;
using AutoMapper;


namespace Domain.CimsIntegrationLogging
{
    public class CimsConnectIntegrationLog : Entity
    {
        public virtual Guid RequestId { get; set; }
        public virtual string RequestData { get; set; }
        public virtual string ResponseData { get; set; }


        public static CimsConnectIntegrationLog Create(CreateCimsIntegrationLoggingDto createCimsIntegrationLoggingDto)
        {
            var cims = Mapper.Map<CimsConnectIntegrationLog>(createCimsIntegrationLoggingDto);
            return cims;
        }
    }
}
