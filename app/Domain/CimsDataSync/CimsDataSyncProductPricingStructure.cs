﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.ProductPricingStructure;
using Domain.Base;

namespace Domain.CimsDataSync
{
    public class CimsDataSyncProductPricingStructure : Entity
    {
        public CimsDataSyncProductPricingStructure() { }

        public CimsDataSyncProductPricingStructure(object[] obj)
        {
            RowsAffected = obj[0] != null ? (int)obj[0] : 0;
            Message = obj[1] != null ? (string)obj[1] : string.Empty;
        }
        
        public virtual int RowsAffected { get; set; }
        
        public virtual string Message { get; set; }
    }
}
