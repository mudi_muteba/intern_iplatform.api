﻿using AutoMapper;
using Domain.CimsDataSync.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.CimsDataSync;

namespace Domain.CimsDataSync.Mappings
{
    public class CimsDataSyncMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CimsDataSyncProductPricingStructure, CimsDataSyncProductPricingStructureStoredProcedureDto>()
                .ForMember(d => d.RowsAffected, o => o.MapFrom(s => s.RowsAffected));

            Mapper.CreateMap<CimsDataSyncChannelUserDto, CimsDataSyncChannelUserStoredProcedureParameters>();
        }
    }
}
