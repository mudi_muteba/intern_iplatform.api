﻿using Domain.Base.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;

namespace Domain.CimsDataSync.Queries.Filters
{
    public class DefaultCimsDataSyncProductPricingStructureFilter : IApplyDefaultFilters<CimsDataSyncProductPricingStructure>
    {
        public IQueryable<CimsDataSyncProductPricingStructure> Apply(ExecutionContext executionContext, IQueryable<CimsDataSyncProductPricingStructure> query)
        {
            return query;
        }
    }
}
