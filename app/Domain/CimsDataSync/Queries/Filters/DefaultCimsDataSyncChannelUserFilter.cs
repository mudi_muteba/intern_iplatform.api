﻿using Domain.Base.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;

namespace Domain.CimsDataSync.Queries.Filters
{
    public class DefaultCimsDataSyncChannelUserFilter : IApplyDefaultFilters<CimsDataSyncChannelUser>
    {
        public IQueryable<CimsDataSyncChannelUser> Apply(ExecutionContext executionContext, IQueryable<CimsDataSyncChannelUser> query)
        {
            return query;
        }
    }
}
