﻿using Domain.Base.Queries;
using Domain.CimsDataSync.StoredProcedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData.Authorisation;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.CimsDataSync.Queries.Filters;

namespace Domain.CimsDataSync.Queries
{
    public class CimsDataSyncChannelUserQuery : BaseStoredProcedureQuery<CimsDataSyncChannelUser, CimsDataSyncChannelUserStoredProcedure>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public CimsDataSyncChannelUserQuery(IProvideContext contextProvider, IRepository repository) 
            :base(contextProvider, repository, new DefaultCimsDataSyncChannelUserFilter())
        {
        }

        protected internal override IList<CimsDataSyncChannelUser> Execute(IList<CimsDataSyncChannelUser> storedProcedure)
        {
            return storedProcedure;
        }
    }
}
