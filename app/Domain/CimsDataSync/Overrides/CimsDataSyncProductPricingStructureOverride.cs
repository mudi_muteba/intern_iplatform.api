﻿using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Automapping;

namespace Domain.CimsDataSync.Overrides
{
    public class CimsDataSyncProductPricingStructureOverride : IAutoMappingOverride<CimsDataSyncProductPricingStructure>
    {
        public void Override(AutoMapping<CimsDataSyncProductPricingStructure> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
