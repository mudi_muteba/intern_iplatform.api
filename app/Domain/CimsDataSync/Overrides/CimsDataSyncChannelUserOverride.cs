﻿using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Automapping;

namespace Domain.CimsDataSync.Overrides
{
    public class CimsDataSyncChannelUserOverride : IAutoMappingOverride<CimsDataSyncChannelUser>
    {
        public void Override(AutoMapping<CimsDataSyncChannelUser> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
