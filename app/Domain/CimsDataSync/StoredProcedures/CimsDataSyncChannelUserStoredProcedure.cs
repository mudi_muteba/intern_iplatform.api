﻿using AutoMapper;
using Domain.Base.Repository.StoredProcedures;
using Domain.CimsDataSync.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.CimsDataSync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.CimsDataSync.StoredProcedures
{
    public class CimsDataSyncChannelUserStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "CimsDataSyncChannelUser";
            }
        }

        public CimsDataSyncChannelUserStoredProcedure() { }

        public CimsDataSyncChannelUserStoredProcedure(CimsDataSyncChannelUserDto cimsDataSyncChannelUserDto)
        {
            CimsDataSyncChannelUserStoredProcedureParameters parameters = Mapper.Map<CimsDataSyncChannelUserStoredProcedureParameters>(cimsDataSyncChannelUserDto);
            base.SetParameters(parameters);
        }
    }
}
