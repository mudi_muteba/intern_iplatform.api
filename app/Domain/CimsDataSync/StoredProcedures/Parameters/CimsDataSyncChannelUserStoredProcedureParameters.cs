﻿using Domain.Base.Repository.StoredProcedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.CimsDataSync.StoredProcedures.Parameters
{
    public class CimsDataSyncChannelUserStoredProcedureParameters: IStoredProcedureCriteria
    {
        public string ProductCodes { get; set; }

        public string DatabaseName { get; set; }

        public string Client { get; set; }
    }
}
