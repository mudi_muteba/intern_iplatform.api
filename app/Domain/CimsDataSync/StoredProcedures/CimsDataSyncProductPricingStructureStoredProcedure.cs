﻿using Domain.Base.Repository.StoredProcedures;
using Domain.CimsDataSync.StoredProcedures.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.CimsDataSync.StoredProcedures
{
    public class CimsDataSyncProductPricingStructureStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "CimsDataSyncProductPricingStructure";
            }
        }
        
        public CimsDataSyncProductPricingStructureStoredProcedure() { }

        public CimsDataSyncProductPricingStructureStoredProcedure(string dbName)
        {
            CimsDataSyncPricingStructureStoredProcedureParameters parameters = new CimsDataSyncPricingStructureStoredProcedureParameters()
            {
                DatabaseName = dbName
            };

            base.SetParameters(parameters);
        }
    }
}
