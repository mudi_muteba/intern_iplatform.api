﻿using System;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Occupations;
using Domain.Party.ContactDetails;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.ContactDetail;
using iPlatform.Api.DTOs.SalesForce;
using Shared.Extentions;

namespace Domain.Individuals.Mappings
{
    public class IndividualMapper : Profile
    {

        protected override void Configure()
        {
            Mapper.CreateMap<Individual, ListIndividualDto>()
                .ForMember(t => t.LeadId, o=> o.MapFrom(s => s.Lead.Id))
                ;

            Mapper.CreateMap<Individual, BasicIndividualDto>();

            Mapper.CreateMap<PagedResults<Individual>, PagedResultDto<ListIndividualDto>>();

            Mapper.CreateMap<Individual, SalesForceSubmitDto>()
                .ForMember(t => t.Email, o => o.MapFrom(a => a.ContactDetail.Email))
                .ForMember(t => t.FirstName, o => o.MapFrom(a => a.FirstName))
                .ForMember(t => t.LastName, o => o.MapFrom(a => a.Surname))
                .ForMember(t => t.VMSA_NationalId__c, o => o.MapFrom(a => a.IdentityNo))
                .ForMember(t => t.VMSA_DateOfSubmission__c, o => o.MapFrom( a=> DateTime.UtcNow.ToString("yyyy-MM-ddThh:mm:ssZ")))
                .ForMember(t => t.GCC__CAT_LocalTimeZone__c, o => o.MapFrom(a => "Time Zone: South Africa (UTC+2:00)"))
                .ForMember(t => t.Salutation, o => o.Ignore())
                .ForMember(t => t.VMSA_CMPID__c, o => o.Ignore())
                .ForMember(t => t.VMSA_QuoteAmount__c, o => o.Ignore())
                .ForMember(t => t.VMSA_QuoteNumber__c, o => o.Ignore())
                .ForMember(t => t.VMSA_QuoteStatus__c, o => o.Ignore())
                .ForMember(t => t.VMSA_QuotedProduct__c, o => o.Ignore())
                .ForMember(t => t.VMSA_SubmittedFromWebsitePage__c, o => o.Ignore())
                .ForMember(t => t.GCC__CAT_HomePhone__c, o => o.Ignore())
                .ForMember(t => t.MobilePhone, o => o.Ignore())
                .ForMember(t => t.Phone, o => o.Ignore())
                .AfterMap((s,d) =>
                {
                    if(s.ContactDetail != null)
                    {
                        d.GCC__CAT_HomePhone__c = s.ContactDetail.Home.AddExtToNumber();
                        d.MobilePhone = s.ContactDetail.Cell.AddExtToNumber();
                        d.Phone = s.ContactDetail.Work.AddExtToNumber();
                    }
                })
                ;

            Mapper.CreateMap<CreateIndividualDto, Individual>()
                .ForMember(t => t.Channel, o => o.MapFrom(a => new Channel(a.ChannelId)))
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.DateCreated, o => o.Ignore())
                .ForMember(t => t.DateUpdated, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Campaigns, o => o.Ignore())
                ;

            Mapper.CreateMap<Individual, IndividualDto>()
                .ForMember(t => t.Events, o => o.MapFrom(s => s.Audit))
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.LeadId, o => o.MapFrom(s => s.Lead.Id))
                .ForMember(t => t.DeclarationInfo, o => o.MapFrom(s => new DeclarationInfoDto 
                { 
                    AnyJudgements = s.AnyJudgements,
                    AnyJudgementsReason = s.AnyJudgementsReason,
                    BeenSequestratedOrLiquidated = s.BeenSequestratedOrLiquidated,
                    BeenSequestratedOrLiquidatedReason = s.BeenSequestratedOrLiquidatedReason,
                    UnderAdministrationOrDebtReview = s.UnderAdministrationOrDebtReview,
                    UnderAdministrationOrDebtReviewReason = s.UnderAdministrationOrDebtReviewReason,
                    InsurancePolicyCancelledOrRenewalRefused = s.InsurancePolicyCancelledOrRenewalRefused,
                    InsurancePolicyCancelledOrRenewalRefusedReason = s.InsurancePolicyCancelledOrRenewalRefusedReason
                }));

            Mapper.CreateMap<Individual, IndividualSimplifiedDto>()
                .ForMember(t => t.Events, o => o.MapFrom(s => s.Audit))
                ;

            Mapper.CreateMap<ContactDetail, ContactDetailDto>();


            Mapper.CreateMap<EditIndividualDto, Individual>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Campaigns, o => o.Ignore())
                .ForMember(t => t.Occupation, o => o.MapFrom(s => new Occupation{Id = s.Occupation.Id}))
                ;
            

            Mapper.CreateMap<Individual, EditIndividualDto>()
                .ForMember(t => t.Events, o => o.MapFrom(s => s.Audit))
                .ForMember(t => t.ChannelId, o => o.MapFrom(s => s.Channel.Id))
                .ForMember(t => t.ContactDetail, o => o.MapFrom(s => s.ContactDetail));

            Mapper.CreateMap<ContactDetail, CreateContactDetailDto>();

            Mapper.CreateMap<ContactDetailDto, CreateContactDetailDto>();


        }
    }
}