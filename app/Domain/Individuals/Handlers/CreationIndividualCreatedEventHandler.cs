﻿using System.Linq;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Individuals.Events;
using Domain.Users;
using Domain.Campaigns;
using Domain.Party;

namespace Domain.Individuals.Handlers
{
    public class CreationIndividualCreatedEventHandler : BaseEventHandler<IndividualCreatedEvent>
    {
        public CreationIndividualCreatedEventHandler(IRepository repository, IProvideContext contextProvider)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        private IRepository _repository { get; set; }
        private IProvideContext _contextProvider { get; set; }


        public override void Handle(IndividualCreatedEvent @event)
        {
            if (@event.Individual != null)
            {
                ExecutionContext context = _contextProvider.Get();
                var user = _repository.GetById<User>(context.UserId);

                Campaign campaign = null;
            
                PartyCampaign partyCampaign = @event.Individual.Campaigns.FirstOrDefault();
                if (partyCampaign != null)
                    campaign = partyCampaign.Campaign;

                @event.Individual.Created(@event.Source, user, campaign);
            }
        }
    }
}