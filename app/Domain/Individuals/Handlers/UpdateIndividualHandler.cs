﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.SalesForce.Events;
using iPlatform.Api.DTOs.Individual;
using MasterData.Authorisation;

namespace Domain.Individuals.Handlers
{
    public class UpdateIndividualHandler : ExistingEntityDtoHandler<Individual, EditIndividualDto, int>
    {
        private readonly IProvideContext _contextProvider;

        public UpdateIndividualHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _contextProvider = contextProvider;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Edit
                };
            }
        }

        protected override Individual GetEntityById(EditIndividualDto dto)
        {
            return repository.GetById<Individual>(dto.Id);
        }

        protected override void HandleExistingEntityChange(Individual entity, EditIndividualDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);

            var source = string.Empty;
            if (_contextProvider.Get().RequestHeaders.ContainsKey("source"))
                source = _contextProvider.Get().RequestHeaders["source"].FirstOrDefault();

            if (!source.Equals("aig-widget")) return;

            var cmpid = dto.CmpidSource;

            entity.RaiseEvent(new SalesForceEntryEvent("Contact Only", entity, cmpid, null, dto.ContactOnlyIntendedProduct));

        }
    }
}