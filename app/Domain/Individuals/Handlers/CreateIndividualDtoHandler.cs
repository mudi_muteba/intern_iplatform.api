﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.AuditLeadlog.Events;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.SalesForce.Events;
using Domain.Users;
using iPlatform.Api.DTOs.AuditLeadLog;
using iPlatform.Api.DTOs.Individual;
using Infrastructure.Configuration;
using MasterData.Authorisation;

namespace Domain.Individuals.Handlers
{
    public class CreateIndividualDtoHandler : CreationDtoHandler<Individual, CreateIndividualDto, int>
    {
        private readonly IProvideContext _contextProvider;

        public CreateIndividualDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(Individual entity, HandlerResult<int> result)
        {
            RaiseCreateAuditLeadLogEvent(entity);
            _repository.PublishEvents(entity );
            result.Processed(entity.Id);
        }

        protected override Individual HandleCreation(CreateIndividualDto dto, HandlerResult<int> result)
        {
            //TODO: Get this from the token, it will be used for validation as well 

            var channelId = dto.ChannelId;
            if (channelId == 0)
            {
                var user = _repository.GetById<User>(dto.Context.UserId);
                var firstOrDefault = user.Channels.FirstOrDefault(a => a.IsDefault);
                if (firstOrDefault != null)
                    channelId = firstOrDefault.Channel.Id;
            }

            var channel = _repository.GetById<Channel>(channelId);
            var campaigns = dto.ResolveCampaigns(_contextProvider, _repository);

            var individual = Individual.Create(dto, channel, campaigns);

            var source = string.Empty;
            if (_contextProvider.Get().RequestHeaders.ContainsKey("source"))
                source = _contextProvider.Get().RequestHeaders["source"].FirstOrDefault();

            if (!source.Equals("aig-widget")) return individual;

            var cmpid = dto.CmpidSource;

            individual.RaiseEvent(new SalesForceEntryEvent("Contact Only", individual, cmpid, null, dto.ContactOnlyIntendedProduct));

            return individual;
        }

        private void RaiseCreateAuditLeadLogEvent(Individual individual)
        {
            Channel channel = _repository.GetById<Channel>(individual.Channel.Id);
            individual.RaiseEvent(new CreateAuditLeadLogEvent(
                new CreateAuditLeadLogDto()
                {
                    LeadId = individual.Lead.Id,
                    IDNumber = individual.IdentityNo,
                    Name = String.Concat(individual.FirstName + " ", String.IsNullOrWhiteSpace(individual.MiddleName) ? "" : individual.MiddleName + " ", individual.Surname),
                    Timestamp = DateTime.UtcNow,
                    IsAccepted = false,
                    SystemId = channel.SystemId,
                    EnvironmentType = new AdminConfigurationReader().EnvironmentTypeKey
                }));
        }
    }
}