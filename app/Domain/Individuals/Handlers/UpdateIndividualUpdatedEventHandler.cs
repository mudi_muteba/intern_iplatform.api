﻿using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Individuals.Events;
using Domain.Party;
using Domain.Users;
using System.Linq;

namespace Domain.Individuals.Handlers
{
    public class UpdateIndividualUpdatedEventHandler : BaseEventHandler<IndividualUpdatedEvent>
    {
        public UpdateIndividualUpdatedEventHandler(IRepository repository, IProvideContext contextProvider)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        private IRepository _repository { get; set; }
        private IProvideContext _contextProvider { get; set; }


        public override void Handle(IndividualUpdatedEvent @event)
        {
            ExecutionContext context = _contextProvider.Get();
            var user = _repository.GetById<User>(context.UserId);

            Campaign campaign = null;
            PartyCampaign partyCampaign = @event.Individual.Campaigns.FirstOrDefault();
            if (partyCampaign != null)
                campaign = partyCampaign.Campaign;

            @event.Individual.Updated(@event.Source, user, campaign);
        }
    }
}