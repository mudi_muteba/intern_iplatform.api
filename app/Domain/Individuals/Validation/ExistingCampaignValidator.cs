﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Individual;
using ValidationMessages.Individual;
using Domain.Campaigns.Queries;
using System.Collections.Generic;
using Domain.Campaigns;
using ValidationMessages.Campaigns;
using iPlatform.Api.DTOs.Leads;

namespace Domain.Individuals.Validation
{
    public class ExistingCampaignValidator : IValidateDto<CreateIndividualDto>, IValidateDto<EditIndividualDto>
    {
        private readonly GetCampaignByIdQuery queryById;
        private GetCampaignByReferenceQuery _queryByReference;
        private readonly IRepository repository;
        public ExistingCampaignValidator(IProvideContext contextProvider, IRepository repository)
        {
            queryById = new GetCampaignByIdQuery(contextProvider, repository);
            _queryByReference = new GetCampaignByReferenceQuery(contextProvider, repository);
            this.repository = repository;
        }

        public void Validate(CreateIndividualDto dto, ExecutionResult result)
        {
            ValidateCampaigns(dto.Campaigns, result);
        }

        public void Validate(EditIndividualDto dto, ExecutionResult result)
        {
            ValidateCampaigns(dto.Campaigns, result);
        }


        private void ValidateCampaigns(List<CampaignInfoDto> campaigns, ExecutionResult result)
        {
            foreach (var campaign in campaigns)
            {
                if (campaign.Id < 1 && string.IsNullOrWhiteSpace(campaign.CampaignReference))
                    result.AddValidationMessage(CampaignValidationMessages.UnknownCampaign.AddParameters(new[] { campaign.Id.ToString() }));

                if (campaign.Id > 0)
                {
                    var queryResult = repository.GetAll<Campaign>().Where(x => x.Id == campaign.Id);
                    //var result = queryById.ById().ExecuteQuery();

                    if (!queryResult.Any())
                        result.AddValidationMessage(CampaignValidationMessages.UnknownCampaign.AddParameters(new[] { campaign.Id.ToString() }));
                }
                else
                {
                    var queryResult = _queryByReference.WithReference(campaign.CampaignReference).ExecuteQuery();
                    if (!queryResult.Any())
                        result.AddValidationMessage(CampaignValidationMessages.UnknownCampaign.AddParameters(new[] { campaign.CampaignReference }));
                }
                
            }
        }

    }
}