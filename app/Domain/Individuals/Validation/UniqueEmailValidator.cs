﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Individual;
using ValidationMessages.Individual;
using iPlatform.Api.DTOs.Leads;
using Domain.Party.Queries;

namespace Domain.Individuals.Validation
{
    public class UniqueEmailValidator : IValidateDto<CreateLeadIndividualDto>
    {
        private readonly GetPartyByEmailQuery query;

        public UniqueEmailValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetPartyByEmailQuery(contextProvider, repository);
        }

        public void Validate(CreateIndividualDto dto, ExecutionResult result)
        {
            if (string.IsNullOrEmpty(dto.IdentityNo)) return;

            var existingIndividual = GetExistingIndividualsByEmail(dto.ContactDetail.Email);
            if (existingIndividual.Any())
                result.AddValidationMessage(
                    IndividualValidationMessages.DuplicateEmail().AddParameters(new[] { dto.ContactDetail.Email }));
        }

        public void Validate(CreateLeadIndividualDto dto, ExecutionResult result)
        {
            if (string.IsNullOrEmpty(dto.IdentityNo)) return;

            var existingIndividual = GetExistingIndividualsByEmail(dto.ContactDetail.Email);
            if (existingIndividual.Any())
                result.AddValidationMessage(
                    IndividualValidationMessages.DuplicateEmail().AddParameters(new[] { dto.ContactDetail.Email }));
        }

        private IQueryable<Party.Party> GetExistingIndividualsByEmail(string email)
        {
            return query.ByEmail(email).ExecuteQuery();
        }
    }
}