﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using ValidationMessages.Individual;

namespace Domain.Individuals.Validation
{
    public class UniqueIndividualValidator : IValidateDto<CreateIndividualDto>, IValidateDto<SubmitLeadDto>
    {
        private readonly GetIndividualByIdNumberQuery query;

        public UniqueIndividualValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetIndividualByIdNumberQuery(contextProvider, repository);
        }

        public void Validate(CreateIndividualDto dto, ExecutionResult result)
        {
            if (string.IsNullOrEmpty(dto.IdentityNo)) return;

            var existingIndividual = GetExistingIndividualsByName(dto.IdentityNo);
            if (existingIndividual.Any())
                result.AddValidationMessage(
                    IndividualValidationMessages.DuplicateIdNumber().AddParameters(new[] {dto.IdentityNo}));
        }

        public void Validate(SubmitLeadDto dto, ExecutionResult result)
        {
            var existingIndividual = GetExistingIndividualsByName(dto.InsuredInfo.IDNumber);

            if (existingIndividual.Any())
                IndividualValidationMessages.DuplicateIdNumber().AddParameters(new[] {dto.InsuredInfo.IDNumber});
        }

        private IQueryable<Individual> GetExistingIndividualsByName(string idNumber)
        {
            return query.ByIdNumber(idNumber).ExecuteQuery();
        }
    }
}