using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Individual;

namespace Domain.Individuals
{
    public static class CampaignResolver
    {
        public static List<Campaign> ResolveCampaigns(this CreateIndividualDto dto, IProvideContext contextProvider, IRepository repository)
        {
            var list = new List<Campaign>();
            var queryById = new GetCampaignByIdQuery(contextProvider, repository);
            var _queryByReference = new GetCampaignByReferenceQuery(contextProvider, repository);

            foreach (var campaign in dto.Campaigns)
            {
                if (campaign.Id < 1 && string.IsNullOrWhiteSpace(campaign.CampaignReference))
                    continue;
                if (campaign.Id > 0)
                {
                    var queryResult = queryById.ById(campaign.Id).ExecuteQuery();
                    list.AddRange(queryResult.ToList());
                }
                else
                {
                    var queryResult = _queryByReference.WithReference(campaign.CampaignReference).ExecuteQuery();
                    list.AddRange(queryResult.ToList().Select(a => a.Campaign));
                }
            }

            return list;
        }
        public static List<Campaign> ResolveCampaigns(this EditIndividualDto dto, IProvideContext contextProvider, IRepository repository, List<int> channels = null)
        {
            var list = new List<Campaign>();
            var queryById = new GetCampaignByIdQuery(contextProvider, repository);
            var _queryByReference = new GetCampaignByReferenceQuery(contextProvider, repository);

            if (dto != null && dto.Campaigns.Any())
                foreach (var campaign in dto.Campaigns)
                {
                    if (campaign.Id < 1 && string.IsNullOrWhiteSpace(campaign.CampaignReference))
                        continue;
                    if (campaign.Id > 0)
                    {
                        var queryResult = queryById.ById(campaign.Id).ExecuteQuery();
                        list.AddRange(queryResult.ToList());
                    }
                    else
                    {
                        var queryResult = _queryByReference.WithReference(campaign.CampaignReference).ExecuteQuery();
                        list.AddRange(queryResult.ToList().Select(a => a.Campaign));
                    }
                }
            else
            {
                var campaigns = repository.GetAll<Campaign>();

                if (channels.Any())
                    campaigns = campaigns.Where(x => x.Channel.Id == channels.First());

                var campaign = campaigns.Where(x => x.DefaultCampaign).FirstOrDefault();
                if (campaign == null)
                    campaign = campaigns.FirstOrDefault();

                list.Add(campaign);
            }
            return list;
        }
    }
}