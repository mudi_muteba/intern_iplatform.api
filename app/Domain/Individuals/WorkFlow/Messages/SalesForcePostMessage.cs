﻿using System;
using Workflow.Messages;

namespace Domain.Individuals.WorkFlow.Messages
{
    public class SalesForcePostMessage : WorkflowExecutionMessage, IWorkflowRetryMessage
    {
        public int RetryCount { get; set; }
        public int RetryLimit { get; set; }
        private TimeSpan? _messageDelay = TimeSpan.FromMinutes(2);
        public TimeSpan? MessageDelay
        {
            get { return _messageDelay; }
            set { _messageDelay = value; }
        }
        public DateTime? FuturePublishDate { get; set; }

        public SalesForcePostMessage() { }

        public SalesForcePostMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }

        public SalesForcePostMessage(SalesForcePostMetaData metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}