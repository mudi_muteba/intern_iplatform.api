﻿using iPlatform.Api.DTOs.SalesForce;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Individuals.WorkFlow.Messages
{
    public class SalesForcePostMetaData : ITaskMetadata
    {
        public SalesForceLeadStatusDto SalesForceLeadStatus;

        public SalesForcePostMetaData()
        {
        }

        public SalesForcePostMetaData WithMetaData(SalesForceLeadStatusDto salesForceLeadStatus)
        {
            SalesForceLeadStatus = salesForceLeadStatus;
            return this;
        }

    }
}