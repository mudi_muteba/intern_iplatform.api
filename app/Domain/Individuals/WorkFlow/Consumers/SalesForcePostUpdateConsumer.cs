using System.Configuration;
using Domain.Individuals.WorkFlow.Messages;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Individuals.WorkFlow.Consumers
{
    public class SalesForcePostUpdateConsumer : AbstractMessageConsumer<SalesForcePostMessage>
    {
        private readonly IConnector _connector;
        private readonly IWorkflowRouter _router;

        public SalesForcePostUpdateConsumer(IConnector connector,
            IWorkflowRouter router,
            IWorkflowExecutor executor)
            : base(router, executor)
        {
            _connector = connector;
            _router = router;
        }

        public override void Consume(SalesForcePostMessage message)
        {
            this.Debug(() => "SalesForcePostUpdateConsumer Started");

            var metaData = (SalesForcePostMetaData) message.Metadata;
            var userName = ConfigurationManager.AppSettings[@"iPlatform/connector/userName"];
            var userPassword = ConfigurationManager.AppSettings[@"iPlatform/connector/apiKey"];
            var token = _connector.Authentication.Authenticate(userName, userPassword, "local", "", ApiRequestType.Workflow);
            _connector.SetToken(token.Token);

            if (metaData == null)
            {
                this.Error(() => string.Format("Sales force meta data empty CorrelationId {0}", message.CorrelationId));
                return;
            }

            this.Debug(() => string.Format("Processing consumers: {0} for party {1} and messageId {2}", metaData.SalesForceLeadStatus.QuoteStatus, metaData.SalesForceLeadStatus.PartyId,
                                metaData.SalesForceLeadStatus.MessageId));

            //Decide to send to sales force 
            new ContactOnlyConsumer(_connector,_router, metaData.SalesForceLeadStatus).SendMessage();
            new RiskLevelConsumer(_connector,_router, metaData.SalesForceLeadStatus).SendMessage();
            new QuoteLevelConsumer(_connector,_router, metaData.SalesForceLeadStatus).SendMessage();
            new StatusConsumer(_connector,_router, metaData.SalesForceLeadStatus).SendMessage();

            this.Debug(() => string.Format("Processing message DONE: {0} for party {1} and messageId {2}", metaData.SalesForceLeadStatus.QuoteStatus, metaData.SalesForceLeadStatus.PartyId,
                        metaData.SalesForceLeadStatus.MessageId));
        }
    }
}