using System;
using System.Linq;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.SalesForce;
using Workflow.Messages;

namespace Domain.Individuals.WorkFlow.Consumers
{
    public class ContactOnlyConsumer : ConsumerBase
    {
        private static readonly ILog Log = LogManager.GetLogger<ContactOnlyConsumer>();
        private readonly IConnector _connector;
        private readonly SalesForceLeadStatusDto _message;

        public ContactOnlyConsumer(IConnector connector, IWorkflowRouter router, SalesForceLeadStatusDto message)
            : base(connector, message, router)
        {
            _connector = connector;
            _message = message;
        }

        public void SendMessage()
        {
            if (_message == null)
                throw new Exception("SalesForcePostMessage METADATA NULL");

            if (_message.QuoteStatus != "Contact Only")
                return;

            Log.DebugFormat("Processing message: {0} for party {1} and messageId {2}", _message.QuoteStatus, _message.PartyId,
                _message.MessageId);

            if (!Criteria()) return;
            if (DelaySuspendMessage()) return;

            Log.DebugFormat("SalesForcePostUpdateConsumer Calling Connector - message not delayed");
            var responseDto = _connector.IndividualManagement.Individual(_message.PartyId).SalesForce().Post(_message);
            if (!responseDto.IsSuccess || responseDto.Errors.Any())
            {
                Log.ErrorFormat(responseDto.PrintErrors());
                throw new Exception("Error posting sales force message " + _message);
            }
        }

        private bool Criteria()
        {
            var response = _connector.IndividualManagement.Individual(_message.PartyId).Proposals().Get();
            return response.Response == null || !response.Response.Results.Any();
        }
    }
}