using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.SalesForce;
using Workflow.Messages;

namespace Domain.Individuals.WorkFlow.Consumers
{
    public class StatusConsumer : ConsumerBase
    {
        private readonly IConnector _connector;
        private readonly SalesForceLeadStatusDto _message;
        private static readonly ILog Log = LogManager.GetLogger<StatusConsumer>();

        public StatusConsumer(IConnector connector, IWorkflowRouter router, SalesForceLeadStatusDto message)
            : base(connector,message,router)
        {
            _connector = connector;
            _message = message;
        }

        public void SendMessage()
        {
            if (_message == null)
                throw new Exception("SalesForcePostMessage METADATA NULL");

            var allowedStatuses = new List<string>() { "Declined", "UW Referral", "Intent to Buy" };

            if (!allowedStatuses.Contains(_message.QuoteStatus))
                return;

            Log.DebugFormat("Processing message: {0} for party {1} and messageId {2}", _message.QuoteStatus, _message.PartyId, _message.MessageId);


            Log.DebugFormat("SalesForcePostUpdateConsumer Calling Connector");
            var responseDto = _connector.IndividualManagement.Individual(_message.PartyId).SalesForce().Post(_message);
            if (!responseDto.IsSuccess || responseDto.Errors.Any())
            {
                Log.ErrorFormat(responseDto.PrintErrors());
                throw new Exception("Error posting sales force message " + _message);
            }
        }

    }
}