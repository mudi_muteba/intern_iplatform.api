using System;
using System.Configuration;
using Common.Logging;
using Domain.Individuals.WorkFlow.Messages;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.SalesForce;
using Workflow.Messages;

namespace Domain.Individuals.WorkFlow.Consumers
{
    public abstract class ConsumerBase
    {
        private readonly IConnector _connector;
        private readonly SalesForceLeadStatusDto _message;
        private readonly IWorkflowRouter _router;
        private static readonly ILog Log = LogManager.GetLogger<ConsumerBase>();

        protected ConsumerBase(IConnector connector, SalesForceLeadStatusDto message, IWorkflowRouter router)
        {
            _connector = connector;
            _message = message;
            _router = router;
        }

        public bool DelaySuspendMessage()
        {
            var item
                =
                _connector.IndividualManagement.Individual(_message.PartyId)
                    .SalesForce()
                    .GetSfEntryByPartyId(_message.PartyId);

            if (item.Response.Suspended)
            {
                Log.DebugFormat("Message suspended for party {0} - message not sent", _message.PartyId);
                if (item.Response.Delayed)
                RemoveDelay();
                return true;
            }
            if (!item.Response.Delayed) return false;
 
            Log.DebugFormat("Message delayed for party {0}",_message.PartyId);

            RemoveDelay();

            Log.DebugFormat("Message delayed updated to false for party {0} - Republish message", _message.PartyId);

            PublishMessage(_message);

            return true;
        }

        private void RemoveDelay()
        {
            _connector.IndividualManagement.Individual(_message.PartyId)
                .SalesForce()
                .UpdateSfEntry(new EditSalesForceEntryDto
                {
                    Id = _message.PartyId,
                    Delayed = false
                });
        }

        private void PublishMessage(SalesForceLeadStatusDto salesForceLeadStatus)
        {
            var message = new WorkflowRoutingMessage();
            var metadata = new SalesForcePostMetaData().WithMetaData(salesForceLeadStatus);
            var delay = ConfigurationManager.AppSettings[@"salesforce/delay"];


            message.AddMessage(
                new SalesForcePostMessage(metadata));
            try
            {
                _router.PublishDelayed(message, TimeSpan.FromSeconds(double.Parse(delay)));
                Log.DebugFormat("Delayed salesforce entry published message for party {0}", salesForceLeadStatus.PartyId);
            }
            catch (Exception e)
            {
                Log.ErrorFormat("Delayed salesforce entry publish failed {0}", e.Message);
                throw;
            }
        }
    }
}