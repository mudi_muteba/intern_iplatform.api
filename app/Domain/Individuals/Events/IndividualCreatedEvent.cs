﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Individual;
using Newtonsoft.Json;

namespace Domain.Individuals.Events
{
    public class IndividualCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        [JsonIgnore]
        public Individual Individual { get; private set; }

        public int UserId { get; private set; }

        public string Source { get; private set; }

        public IndividualCreatedEvent(string _source, Individual _individual, EventAudit audit, List<ChannelReference> channelReferences)
            : base(audit, channelReferences )
        {
            Individual = _individual;
            Source = _source;
        }
    }
}