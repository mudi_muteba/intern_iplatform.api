﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Newtonsoft.Json;

namespace Domain.Individuals.Events
{
    public class IndividualUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public IndividualUpdatedEvent(string _source, Individual _individual, EventAudit audit, List<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            Individual = _individual;
            Source = _source;
        }

        [JsonIgnore]
        public Individual Individual { get; private set; }
        public int UserId { get; private set; }
        public string Source { get; private set; }
    }
}