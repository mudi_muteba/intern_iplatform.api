﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Individuals.Overrides
{
    public class IndividualOverride : IAutoMappingOverride<Individual>
    {
        public void Override(AutoMapping<Individual> mapping)
        {
            mapping.References(c => c.Occupation);
        }
    }
}