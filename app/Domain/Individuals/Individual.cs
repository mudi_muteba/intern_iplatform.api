﻿using System;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Individuals.Events;
using Domain.Leads;
using Domain.Party.Contacts.Events;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Individual;
using MasterData;
using Domain.Party;
using System.Collections.Generic;
using Domain.Campaigns;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Helper;
using Domain.Base.Repository;
using Domain.Campaigns.Mappings;
using Domain.Occupations;
using Domain.SalesForce.Events;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.SalesForce;

namespace Domain.Individuals
{
    public class Individual : Party.Party
    {
        private string _firstName;
        private string _surname;
        public virtual string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                base.DisplayName = string.Format("{0}, {1}", _surname, _firstName);
            }
        }
        public virtual string MiddleName { get; set; }
        public virtual string Surname
        {
            get { return _surname; }
            set
            {
                _surname = value;
                base.DisplayName = string.Format("{0}, {1}", _surname, _firstName);
            }
        }

        public virtual string PreferredName { get; set; }
        public virtual Title Title { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual DateTime? DateOfBirth { get; set; }
        public virtual string IdentityNo { get; set; }
        public virtual string PassportNo { get; set; }
        public virtual string ExternalReference { get; set; }
        public virtual MaritalStatus MaritalStatus { get; set; }
        public virtual Language Language { get; set; }
        public virtual Occupation Occupation { get; set; }
        public virtual bool AnyJudgements { get; set; }
        public virtual string AnyJudgementsReason { get; set; }
        public virtual bool UnderAdministrationOrDebtReview { get; set; }
        public virtual string UnderAdministrationOrDebtReviewReason { get; set; }
        public virtual bool BeenSequestratedOrLiquidated { get; set; }
        public virtual string BeenSequestratedOrLiquidatedReason { get; set; }
        public virtual bool InsurancePolicyCancelledOrRenewalRefused { get; set; }
        public virtual string InsurancePolicyCancelledOrRenewalRefusedReason { get; set; }
        public virtual string PinNo { get; set; }
        public Individual()
        {
            base.PartyType = PartyTypes.Individual;
            Campaigns = new List<PartyCampaign>();
        }

        public static Individual Create(CreateIndividualDto createIndividualDto, ChannelReference channel, List<Campaign> campaigns)
        {
            var individual = Mapper.Map<Individual>(createIndividualDto);
            individual.Channel = channel;
            individual.DateCreated = DateTime.UtcNow;
            individual.DateUpdated = DateTime.UtcNow;
            AddPartyCampaigns(individual, campaigns);

            //Create Lead
            individual.Lead = new Lead(individual);
            individual.Lead.ExternalReference = createIndividualDto.LeadExternalReference;

            individual.RaiseEvent(new IndividualCreatedEvent(createIndividualDto.Source, individual, createIndividualDto.CreateAudit(), new List<ChannelReference> { new Channel(channel.Id) }));


            return individual;
        }

        public virtual void Update(EditIndividualDto individualContactDto)
        {
            Mapper.Map(individualContactDto, this);
            DateUpdated = DateTime.UtcNow;
            UpdatePartyCampaigns(individualContactDto);

            RaiseEvent(new IndividualUpdatedEvent(individualContactDto.Source, this, individualContactDto.CreateAudit(), new List<ChannelReference> { new Channel(Channel.Id) }));
        }

        public virtual void UpdateMaritalStatus(string maritalId)
        {
            MaritalStatus = new MaritalStatuses().FirstOrDefault(x => string.Equals(x.Name, maritalId, StringComparison.InvariantCultureIgnoreCase)) ?? new MaritalStatuses().FirstOrDefault(x => string.Equals(x.Name, "Unknown", StringComparison.InvariantCultureIgnoreCase));
            DateUpdated = DateTime.UtcNow;
        }
        #region Campaign
        public static void AddPartyCampaigns(Individual entity, List<Campaign> campaigns)
        {

            foreach (var campaign in campaigns)
                entity.AddCampaign(campaign);

            if (campaigns == null || !campaigns.Any())
                //Find and assign default campaign
                entity.AddCampaign(Mapper.Map<CampaignDefaultDto, Campaign>(new CampaignDefaultDto(entity.Channel.Id)));

        }

        public virtual void AddCampaign(Campaign campaign)
        {
            if (campaign == null)
                return;

            if (Campaigns == null)
                Campaigns = new List<PartyCampaign>();

            var existing = Campaigns.FirstOrDefault(x => x.Campaign.Id == campaign.Id);
            if (existing == null)
                Campaigns.Add(new PartyCampaign { Party = this, Campaign = campaign });
        }

        public virtual void UpdatePartyCampaigns(EditIndividualDto dto)
        {
            //delete products that have been removed from list
            RemoveOldCampaigns(dto.Campaigns);

            //Add new products from list
            var newCampaigns = dto.Campaigns.Where(x => !Campaigns.Any(y => y.Campaign.Id == x.Id)).ToList();

            foreach (var campaign in newCampaigns)
            {
                AddCampaign(new Campaign() { Id = campaign.Id });
            }
        }

        public virtual void RemoveOldCampaigns(List<CampaignInfoDto> campaigns)
        {
            var toBeDeleted = Campaigns.Where(t => !campaigns.Any(y => y.Id == t.Campaign.Id));

            foreach (var partyCampaign in toBeDeleted)
            {
                partyCampaign.Delete();
            }
        }
        #endregion

        public virtual SalesForceSubmitDto GetSalesForceSubmitDto()
        {
            var dto = Mapper.Map(this, new SalesForceSubmitDto());
            return dto;
        }

        protected bool Equals(Individual other)
        {
            return string.Equals(_firstName, other._firstName) && string.Equals(_surname, other._surname) && Equals(Title, other.Title) && Equals(Gender, other.Gender) && string.Equals(MiddleName, other.MiddleName) && DateOfBirth.Equals(other.DateOfBirth) && string.Equals(IdentityNo, other.IdentityNo) && string.Equals(PassportNo, other.PassportNo) && Equals(MaritalStatus, other.MaritalStatus) && Equals(Language, other.Language);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Individual)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (_firstName != null ? _firstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (_surname != null ? _surname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Title != null ? Title.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Gender != null ? Gender.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (MiddleName != null ? MiddleName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ DateOfBirth.GetHashCode();
                hashCode = (hashCode * 397) ^ (IdentityNo != null ? IdentityNo.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PassportNo != null ? PassportNo.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (MaritalStatus != null ? MaritalStatus.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Language != null ? Language.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
