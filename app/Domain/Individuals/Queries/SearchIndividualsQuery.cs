﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Individual;
using MasterData.Authorisation;
using Domain.Leads;
using Domain.Users;

namespace Domain.Individuals.Queries
{
    public class SearchIndividualsQuery : BaseQuery<Individual>, ISearchQuery<IndividualSearchDto>
    {
        private IndividualSearchDto criteria;

        public SearchIndividualsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Individual>())
        {
            this.repository = repository;
        }

        private readonly IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    IndividualAuthorisation.List
                };
            }
        }

        public void WithCriteria(IndividualSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<Individual> Execute(IQueryable<Individual> query)
        {
            query = query.Where(x => x.Lead != null && !x.Lead.IsDeleted && x.Lead.Id != 0
                );

            if (string.IsNullOrEmpty(criteria.PolicyNumber)
                && string.IsNullOrEmpty(criteria.CellNumber)
                && string.IsNullOrEmpty(criteria.Email)
                && string.IsNullOrEmpty(criteria.IdNumber)
                && string.IsNullOrEmpty(criteria.FirstName)
                && string.IsNullOrEmpty(criteria.SurName)
                && string.IsNullOrEmpty(criteria.ExternalReference)
                && criteria.LeadId == 0)
                return query;

            query = query.Where(a =>  a.IdentityNo.Contains(criteria.IdNumber)
                        || a.ContactDetail.Cell.Contains(criteria.CellNumber)
                        || a.FirstName.Contains(criteria.FirstName)
                        || a.Surname.Contains(criteria.SurName)
                        || a.ExternalReference.Contains(criteria.ExternalReference)
                        || a.ContactDetail.Email.Contains(criteria.Email)
                        || a.Lead.Id == criteria.LeadId);

            return query;
        }
    }
}