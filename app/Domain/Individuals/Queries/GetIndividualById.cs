using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Individuals.Queries
{
    public class GetIndividualById : BaseQuery<Individual>
    {
        private int _id;

        public GetIndividualById(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Individual>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.List
                };
            }
        }

        public GetIndividualById WithId(int id)
        {
            _id = id;
            return this;
        }

        protected internal override IQueryable<Individual> Execute(IQueryable<Individual> query)
        {
            return Repository.GetAll<Individual>().Where(ph => ph.Id == _id);
        }
    }
}