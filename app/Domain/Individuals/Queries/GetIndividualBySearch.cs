﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData.Authorisation;

namespace Domain.Individuals.Queries
{
    public class GetIndividualBySearch : BaseQuery<Individual>
    {
        private ContactDetailDto _contact;
        private string _externalReference;
        private string _idNumber;

        public GetIndividualBySearch(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Individual>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.List
                };
            }
        }

        public GetIndividualBySearch ByIdNumber(string idNumber)
        {
            _idNumber = idNumber;
            return this;
        }

        public GetIndividualBySearch ByContactDetails(ContactDetailDto contact)
        {
            _contact = contact;
            return this;
        }

        public GetIndividualBySearch ByExternalReference(string externalReference)
        {
            _externalReference = externalReference;
            return this;
        }
        
        protected internal override IQueryable<Individual> Execute(IQueryable<Individual> query)
        {
            if (!string.IsNullOrEmpty(_externalReference))
            {
                query = query.Where(c => c.ExternalReference.ToString().ToLower().Equals(_externalReference));
                return query;
            }

            if (!string.IsNullOrEmpty(_idNumber))
                query = query.Where(c => c.IdentityNo.Equals(_idNumber));

            if (!string.IsNullOrEmpty(_contact.Email))
                query = query.Where(c => c.ContactDetail.Email.Equals(_contact.Email));

            if (!string.IsNullOrEmpty(_contact.Cell))
                query = query.Where(c => c.ContactDetail.Direct.Equals(_contact.Cell) ||
                                         c.ContactDetail.Work.Equals(_contact.Cell) ||
                                         c.ContactDetail.Cell.Equals(_contact.Cell) ||
                                         c.ContactDetail.Home.Equals(_contact.Cell));

            return query;
        }
    }
}