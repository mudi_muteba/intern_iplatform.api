﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Individual;
using MasterData.Authorisation;
using Domain.Leads;

namespace Domain.Individuals.Queries
{
    public class SearchCampaignIndividualsQuery : BaseQuery<Individual>, ISearchQuery<IndividualSearchDto>
    {
        private IndividualSearchDto criteria;

        public SearchCampaignIndividualsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Individual>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    IndividualAuthorisation.List
                };
            }
        }

        public void WithCriteria(IndividualSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<Individual> Execute(IQueryable<Individual> query)
        {

            if (criteria.AgentId == 0 && criteria.CampaignId == 0)
                return null;

            if (criteria.CampaignId > 0 && criteria.AgentId == 0)
                query = query.Where(a => a.Lead.CampaignLeadBuckets.Any(x => x.Campaign.Id == criteria.CampaignId));

            if (criteria.CampaignId > 0 && criteria.AgentId > 0)
                query = query.Where(a => a.Lead.CampaignLeadBuckets.Any(x => x.Campaign.Id == criteria.CampaignId && x.Agent.Id == criteria.AgentId));

            return query;
        }
    }
}