﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Campaigns.Queries;
using MasterData.Authorisation;

namespace Domain.Individuals.Queries
{
    public class GetIndividualByIdNumberQuery : BaseQuery<Individual>
    {
        private string idNumber;

        public GetIndividualByIdNumberQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Individual>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.List
                };
            }
        }

        public GetIndividualByIdNumberQuery ByIdNumber(string idNumber)
        {
            this.idNumber = idNumber;
            return this;
        }

        protected internal override IQueryable<Individual> Execute(IQueryable<Individual> query)
        {
            return query
                .Where(c => c.IdentityNo == idNumber);
        }
    }
}