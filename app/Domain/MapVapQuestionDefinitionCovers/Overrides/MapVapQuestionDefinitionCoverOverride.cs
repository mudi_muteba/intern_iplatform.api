﻿using Domain.Activities;
using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;
using LinqToExcel.Extensions;

namespace Domain.MapVapQuestionDefinitionCovers.Overrides
{
    public class MapVapQuestionDefinitionCoverOverride : IAutoMappingOverride<MapVapQuestionDefinitionCover>
    {
        public void Override(AutoMapping<MapVapQuestionDefinitionCover> mapping)
        {
            mapping.References(x => x.MapVapQuestionDefinition);

        }
    }
}