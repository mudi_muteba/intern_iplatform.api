﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;
using Domain.QuestionDefinitions;
using MasterData;

namespace Domain.MapVapQuestionDefinitionCovers
{
    public class MapVapQuestionDefinitionCover : Entity
    {
        public virtual MapVapQuestionDefinition MapVapQuestionDefinition { get; set; }

        public virtual Cover Cover { get; set; }

        public MapVapQuestionDefinitionCover() { }

        public MapVapQuestionDefinitionCover(MapVapQuestionDefinition mapVapQuestionDefinition,
            Cover cover)
        {
            MapVapQuestionDefinition = mapVapQuestionDefinition;
            Cover = cover;
        }
    }
}
