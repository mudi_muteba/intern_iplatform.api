﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using iPlatform.Api.DTOs.MapVapQuestionDefinitionCover;
using MasterData;

namespace Domain.MapVapQuestionDefinitionCovers.Mappings
{
    public class MapVapQuestionDefinitionCoverMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateMapVapQuestionDefinitionCoverDto, MapVapQuestionDefinitionCover>()
                .ForMember(d => d.Cover, o => o.MapFrom(s => new Cover() { Id = s.CoverId }));

            Mapper.CreateMap<EditMapVapQuestionDefinitionCoverDto, MapVapQuestionDefinitionCover>()
                .ForMember(d => d.Cover, o => o.MapFrom(s => new Cover() { Id = s.CoverId }));

            Mapper.CreateMap<MapVapQuestionDefinitionCoverDto, MapVapQuestionDefinitionCover>()
                .ReverseMap();
        }
    }
}
