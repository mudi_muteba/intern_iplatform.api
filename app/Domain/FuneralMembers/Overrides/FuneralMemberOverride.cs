﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.FuneralMembers.Overrides
{
    public class FuneralMemberOverride : IAutoMappingOverride<FuneralMember>
    {
        public void Override(AutoMapping<FuneralMember> mapping)
        {
            mapping.References(x => x.ProposalDefinition, "ProposalDefinitionId");
        }
    }
}
