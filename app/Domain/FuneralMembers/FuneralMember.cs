﻿using AutoMapper;
using Domain.Base;
using Domain.Party.ProposalDefinitions;
using iPlatform.Api.DTOs.FuneralMembers;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.FuneralMembers
{
    public class FuneralMember : Entity
    {
        public FuneralMember()
        {

        }

        public virtual MemberRelationship MemberRelationship { get; set; }
        public virtual string Initials { get; set; }
        public virtual string Surname { get; set; }
        public virtual string IdNumber { get; set; }
        public virtual bool IsStudent { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual DateTime DateOnCover { get; set; }
        public virtual string SumInsured { get; set; }
        public virtual ProposalDefinition ProposalDefinition { get; set; }


        public static FuneralMember Create(CreateFuneralMemberDto dto)
        {
            var funeralMember = Mapper.Map<FuneralMember>(dto);

            return funeralMember;
        }

        public virtual void Update(EditFuneralMemberDto editFuneralMemberDto)
        {
            Mapper.Map(editFuneralMemberDto, this);

        }

        public virtual void Disable()
        {
            this.Delete();
        }

    }
}
