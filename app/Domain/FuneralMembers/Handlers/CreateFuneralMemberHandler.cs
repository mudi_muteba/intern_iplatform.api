﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.FuneralMembers;
using MasterData.Authorisation;

namespace Domain.FuneralMembers.Handlers
{
    public class CreateFuneralMemberHandler : CreationDtoHandler<FuneralMember, CreateFuneralMemberDto, int>
    {
        public CreateFuneralMemberHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    FuneralMemberAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(FuneralMember entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override FuneralMember HandleCreation(CreateFuneralMemberDto dto, HandlerResult<int> result)
        {
            var funeralMember = FuneralMember.Create(dto);

            result.Processed(funeralMember.Id);

            return funeralMember;
        }

        
    }
}