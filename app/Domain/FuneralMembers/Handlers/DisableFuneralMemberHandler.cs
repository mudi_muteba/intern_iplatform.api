﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.FuneralMembers;
using MasterData.Authorisation;
using Domain.FuneralMembers;

namespace Domain.FuneralMembers.Handlers
{
    public class DisableFuneralMemberHandler : ExistingEntityDtoHandler<FuneralMember, DisableFuneralMemberDto, int>
    {
        public DisableFuneralMemberHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    FuneralMemberAuthorisation.Delete
                };
            }
        }

        protected override void HandleExistingEntityChange(FuneralMember entity, DisableFuneralMemberDto dto,
            HandlerResult<int> result)
        {
            entity.Disable();
            result.Processed(entity.Id);
        }
    }
}