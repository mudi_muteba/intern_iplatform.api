﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.FuneralMembers;
using MasterData.Authorisation;

namespace Domain.FuneralMembers.Handlers
{
    public class EditFuneralMemberHandler : ExistingEntityDtoHandler<FuneralMember, EditFuneralMemberDto, int>
    {
        public EditFuneralMemberHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    FuneralMemberAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(FuneralMember entity, EditFuneralMemberDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}