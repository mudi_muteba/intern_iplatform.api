﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.FuneralMembers.Queries
{
    public class GetFuneralMembersByProposalDefinitionIdQuery : BaseQuery<FuneralMember>
    {
        private int id;

        public GetFuneralMembersByProposalDefinitionIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<FuneralMember>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    FuneralMemberAuthorisation.List
                };
            }
        }

        public GetFuneralMembersByProposalDefinitionIdQuery ByProposalDefinitionId(int id)
        {
            this.id = id;
            return this;
        }

        protected internal override IQueryable<FuneralMember> Execute(IQueryable<FuneralMember> query)
        {
            return query
                .Where(c => c.ProposalDefinition.Id == id);
        }
    }
}