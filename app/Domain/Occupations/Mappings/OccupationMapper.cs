﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Occupations;
using Domain.Party.ContactDetails;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Occupation;
using iPlatform.Api.DTOs.Party.ContactDetail;

namespace Domain.Individuals.Mappings
{
    public class OccupationMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Occupation, OccupationDto>();

            Mapper.CreateMap<OccupationDto, Occupation>();
        }
    }
}