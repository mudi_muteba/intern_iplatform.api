﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Occupations
{
    public class Occupation : Entity
    {
        public virtual string Name { get; set; }

        public virtual string Code { get; set; }
    }
}
