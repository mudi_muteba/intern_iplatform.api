﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Occupation;

namespace Domain.Occupations.Queries
{
    public class SearchOccupationsQuery : BaseQuery<Occupation>, ISearchQuery<OccupationSearchDto>
    {
        private OccupationSearchDto criteria;

        public SearchOccupationsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Occupation>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(OccupationSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<Occupation> Execute(IQueryable<Occupation> query)
        {
            if (!string.IsNullOrWhiteSpace(criteria.Name))
                query = query.Where(x => x.Name.Contains(criteria.Name));

            return query;
        }
    }
}