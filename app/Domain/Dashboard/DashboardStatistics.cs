using Domain.Base;

namespace Domain.Dashboard
{
    public class DashboardStatistics : Entity
    {
        public DashboardStatistics() { }

        public virtual int LeadsReceived { get; set; }
        public virtual int LeadContacted { get; set; }
        public virtual int LeadsUncontactable { get; set; }
        public virtual int LeadsPending { get; set; }
        public virtual int LeadsCompleted { get; set; }
        public virtual int LeadsUnactioned { get; set; }
    }
}