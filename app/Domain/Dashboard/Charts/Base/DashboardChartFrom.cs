using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartFrom : Entity
    {
        public virtual string template { get; set; }
    }
}