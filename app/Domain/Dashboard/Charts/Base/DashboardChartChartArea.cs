using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartChartArea : Entity
    {
        public virtual string background { get; set; }
        public virtual DashboardChartMargin margin { get; set; }
    }
}