using System.Collections.Generic;
using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChart : Entity
    {
        public virtual DashboardChartAxisDefault axisDefaults { get; set; }
        public virtual DashboardChartCategoryAxis categoryAxis { get; set; }
        public virtual DashboardChartChartArea chartArea { get; set; }
        public virtual DashboardChartLegend legend { get; set; }
        public virtual List<DashboardChartSeries> series { get; set; }
        public virtual DashboardChartSeriesDefault seriesDefaults { get; set; }
        public virtual DashboardChartTitle title { get; set; }
        public virtual DashboardChartTooltip tooltip { get; set; }
        public virtual DashboardChartValueAxis valueAxis { get; set; }
        public virtual DashboardChartXAxis xAxis { get; set; }
        public virtual DashboardChartYAxis yAxis { get; set; }
    }
}