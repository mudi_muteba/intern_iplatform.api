using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartPlotBand : Entity
    {
        public virtual string color { get; set; }
        public virtual int from { get; set; }
        public virtual decimal opacity { get; set; }
        public virtual int to { get; set; }
    }
}