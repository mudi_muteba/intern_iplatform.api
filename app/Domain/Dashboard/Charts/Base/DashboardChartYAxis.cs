using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartYAxis : Entity
    {
        public virtual int axisCrossingValue { get; set; }
        public virtual DashboardChartCrosshair crosshair { get; set; }
        public virtual DashboardChartLabel labels { get; set; }
        public virtual DashboardChartLine line { get; set; }
        public virtual int max { get; set; }
        public virtual int min { get; set; }
        public virtual DashboardChartTitle title { get; set; }
    }
}