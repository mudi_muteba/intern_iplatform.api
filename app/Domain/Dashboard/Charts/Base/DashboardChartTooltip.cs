using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartTooltip : Entity
    {
        public virtual string format { get; set; }
        public virtual int opacity { get; set; }
        public virtual string template { get; set; }
        public virtual bool visible { get; set; }
    }
}