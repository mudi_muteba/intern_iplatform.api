using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartLabel : Entity
    {
        public virtual string background { get; set; }
        public virtual string color { get; set; }
        public virtual string format { get; set; }
        public virtual DashboardChartFrom from { get; set; }
        public virtual string position { get; set; }
        public virtual string rotation { get; set; }
        public virtual int skip { get; set; }
        public virtual string template { get; set; }
        public virtual DashboardChartTo to { get; set; }
        public virtual bool visible { get; set; }
    }
}