using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartTitle : Entity
    {
        public virtual string position { get; set; }
        public virtual string text { get; set; }
    }
}