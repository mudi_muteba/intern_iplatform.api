using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartArea : Entity
    {
        public virtual DashboardChartLine line { get; set; }
    }
}