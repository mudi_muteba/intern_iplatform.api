using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartMargin : Entity
    {
        public virtual int left { get; set; }
    }
}