using System.Collections.Generic;
using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartSeries : Entity
    {
        public virtual List<dynamic> data { get; set; }
        public virtual DashboardChartLabel labels { get; set; }
        public virtual string name { get; set; }
        public virtual int startAngle { get; set; }
        public virtual DashboardChartTooltip tooltip { get; set; }
        public virtual string type { get; set; }
    }
}