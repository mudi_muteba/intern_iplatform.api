using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartAxisDefault : Entity
    {
        public virtual DashboardChartMajorGridLine majorGridLines { get; set; }
    }
}