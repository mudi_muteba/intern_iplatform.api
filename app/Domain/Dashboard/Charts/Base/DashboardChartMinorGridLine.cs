using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartMinorGridLine : Entity
    {
        public virtual bool visible { get; set; }
    }
}