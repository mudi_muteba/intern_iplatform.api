using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartTo : Entity
    {
        public virtual string template { get; set; }
    }
}