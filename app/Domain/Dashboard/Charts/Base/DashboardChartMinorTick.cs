using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartMinorTick : Entity
    {
        public virtual bool visible { get; set; }
    }
}