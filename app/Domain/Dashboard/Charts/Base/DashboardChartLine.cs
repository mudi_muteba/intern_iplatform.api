using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartLine :Entity
    {
        public virtual string style { get; set; }
        public virtual bool visible { get; set; }
        public virtual int width { get; set; }
    }
}