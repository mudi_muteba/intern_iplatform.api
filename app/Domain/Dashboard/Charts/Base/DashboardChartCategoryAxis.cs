using System.Collections.Generic;
using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartCategoryAxis : Entity
    {
        public virtual List<dynamic> categories { get; set; }
        public virtual DashboardChartLabel labels { get; set; }
        public virtual DashboardChartMajorGridLine majorGridLines { get; set; }
        public virtual DashboardChartMajorTick majorTicks { get; set; }
    }
}