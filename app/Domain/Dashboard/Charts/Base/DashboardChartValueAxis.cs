using System.Collections.Generic;
using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartValueAxis : Entity
    {
        public virtual int axisCrossingValue { get; set; }
        public virtual DashboardChartLabel labels { get; set; }
        public virtual DashboardChartLine line { get; set; }
        public virtual DashboardChartMajorGridLine majorGridLines { get; set; }
        public virtual int max { get; set; }
        public virtual int min { get; set; }
        public virtual DashboardChartMinorGridLine minorGridLines { get; set; }
        public virtual DashboardChartMinorTick minorTicks { get; set; }
        public virtual List<DashboardChartPlotBand> plotBands { get; set; }
    }
}