using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartCrosshair: Entity
    {
        public virtual DashboardChartTooltip tooltip { get; set; }
        public virtual bool visible { get; set; }
    }

}