﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartSeriesDefaultOverride : IAutoMappingOverride<DashboardChartSeriesDefault>
    {
        public void Override(AutoMapping<DashboardChartSeriesDefault> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
