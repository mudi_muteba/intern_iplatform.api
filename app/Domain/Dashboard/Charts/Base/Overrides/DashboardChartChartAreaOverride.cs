﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartChartAreaOverride : IAutoMappingOverride<DashboardChartChartArea>
    {
        public void Override(AutoMapping<DashboardChartChartArea> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
