﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartAreaOverride : IAutoMappingOverride<DashboardChartArea>
    {
        public void Override(AutoMapping<DashboardChartArea> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
