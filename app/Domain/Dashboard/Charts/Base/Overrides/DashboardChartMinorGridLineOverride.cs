﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartMinorGridLineOverride : IAutoMappingOverride<DashboardChartMinorGridLine>
    {
        public void Override(AutoMapping<DashboardChartMinorGridLine> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
