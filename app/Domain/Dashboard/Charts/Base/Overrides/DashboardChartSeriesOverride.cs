﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartSeriesOverride : IAutoMappingOverride<DashboardChartSeries>
    {
        public void Override(AutoMapping<DashboardChartSeries> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
