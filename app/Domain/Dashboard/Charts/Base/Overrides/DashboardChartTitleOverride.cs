﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartTitleOverride : IAutoMappingOverride<DashboardChartTitle>
    {
        public void Override(AutoMapping<DashboardChartTitle> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
