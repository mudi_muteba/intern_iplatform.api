﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartLegendOverride : IAutoMappingOverride<DashboardChartLegend>
    {
        public void Override(AutoMapping<DashboardChartLegend> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
