﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartLabelOverride : IAutoMappingOverride<DashboardChartLabel>
    {
        public void Override(AutoMapping<DashboardChartLabel> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
