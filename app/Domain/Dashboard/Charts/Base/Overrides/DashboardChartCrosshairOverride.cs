﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartCrosshairOverride : IAutoMappingOverride<DashboardChartCrosshair>
    {
        public void Override(AutoMapping<DashboardChartCrosshair> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
