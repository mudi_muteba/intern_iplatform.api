﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartXAxisOverride : IAutoMappingOverride<DashboardChartXAxis>
    {
        public void Override(AutoMapping<DashboardChartXAxis> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
