﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartMajorTickOverride : IAutoMappingOverride<DashboardChartMajorTick>
    {
        public void Override(AutoMapping<DashboardChartMajorTick> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
