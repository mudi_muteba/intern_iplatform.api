﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartFromOverride : IAutoMappingOverride<DashboardChartFrom>
    {
        public void Override(AutoMapping<DashboardChartFrom> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
