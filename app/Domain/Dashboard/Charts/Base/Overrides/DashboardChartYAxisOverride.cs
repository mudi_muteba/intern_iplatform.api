﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartYAxisOverride : IAutoMappingOverride<DashboardChartYAxis>
    {
        public void Override(AutoMapping<DashboardChartYAxis> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
