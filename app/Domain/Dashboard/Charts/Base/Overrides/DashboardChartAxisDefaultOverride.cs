﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartAxisDefaultOverride : IAutoMappingOverride<DashboardChartAxisDefault>
    {
        public void Override(AutoMapping<DashboardChartAxisDefault> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
