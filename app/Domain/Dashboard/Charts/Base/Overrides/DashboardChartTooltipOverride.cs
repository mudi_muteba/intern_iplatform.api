﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartToolTipOverride : IAutoMappingOverride<DashboardChartTooltip>
    {
        public void Override(AutoMapping<DashboardChartTooltip> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
