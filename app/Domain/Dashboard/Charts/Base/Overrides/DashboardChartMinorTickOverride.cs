﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartMinorTickOverride : IAutoMappingOverride<DashboardChartMinorTick>
    {
        public void Override(AutoMapping<DashboardChartMinorTick> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
