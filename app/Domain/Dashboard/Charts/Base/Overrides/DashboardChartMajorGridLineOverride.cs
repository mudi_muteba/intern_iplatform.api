﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartMajorGridLineOverride : IAutoMappingOverride<DashboardChartMajorGridLine>
    {
        public void Override(AutoMapping<DashboardChartMajorGridLine> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
