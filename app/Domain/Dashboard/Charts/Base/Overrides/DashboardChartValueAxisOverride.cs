﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartValueAxisOverride : IAutoMappingOverride<DashboardChartValueAxis>
    {
        public void Override(AutoMapping<DashboardChartValueAxis> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
