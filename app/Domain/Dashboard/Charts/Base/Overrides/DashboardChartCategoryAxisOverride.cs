﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartCategoryAxisOverride : IAutoMappingOverride<DashboardChartCategoryAxis>
    {
        public void Override(AutoMapping<DashboardChartCategoryAxis> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
