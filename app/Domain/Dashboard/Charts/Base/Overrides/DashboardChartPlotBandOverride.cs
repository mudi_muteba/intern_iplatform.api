﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartPlotBandOverride : IAutoMappingOverride<DashboardChartPlotBand>
    {
        public void Override(AutoMapping<DashboardChartPlotBand> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
