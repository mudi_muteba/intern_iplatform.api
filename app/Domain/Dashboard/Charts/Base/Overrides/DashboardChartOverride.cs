﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartOverride : IAutoMappingOverride<DashboardChart>
    {
        public void Override(AutoMapping<DashboardChart> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
