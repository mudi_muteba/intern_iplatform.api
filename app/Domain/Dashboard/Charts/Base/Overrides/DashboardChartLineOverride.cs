﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartLineOverride : IAutoMappingOverride<DashboardChartLine>
    {
        public void Override(AutoMapping<DashboardChartLine> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
