﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartToOverride : IAutoMappingOverride<DashboardChartTo>
    {
        public void Override(AutoMapping<DashboardChartTo> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
