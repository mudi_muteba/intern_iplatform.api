﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard.Charts.Base.Overrides
{
    public class DashboardChartMarginOverride : IAutoMappingOverride<DashboardChartMargin>
    {
        public void Override(AutoMapping<DashboardChartMargin> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
