using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartSeriesDefault : Entity
    {
        public virtual DashboardChartArea area { get; set; }
        public virtual bool dynamicSlope { get; set; }
        public virtual bool dynamicHeight { get; set; }
        public virtual DashboardChartLabel labels { get; set; }
        public virtual int startAngle { get; set; }
        public virtual string style { get; set; }
        public virtual string type { get; set; }
    }
}