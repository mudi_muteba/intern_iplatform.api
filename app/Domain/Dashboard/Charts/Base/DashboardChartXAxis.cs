using System.Collections.Generic;
using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartXAxis : Entity
    {
        public virtual int axisCrossingValue { get; set; }
        public virtual DashboardChartCrosshair crosshair { get; set; }
        public virtual DashboardChartLabel labels { get; set; }
        public virtual int majorUnit { get; set; }
        public virtual int max { get; set; }
        public virtual int min { get; set; }
        public virtual List<DashboardChartPlotBand> plotBands { get; set; }
        public virtual DashboardChartTitle title { get; set; }
    }
}