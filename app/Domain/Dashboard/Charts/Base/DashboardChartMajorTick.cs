using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartMajorTick : Entity
    {
        public virtual bool visible { get; set; }
    }
}