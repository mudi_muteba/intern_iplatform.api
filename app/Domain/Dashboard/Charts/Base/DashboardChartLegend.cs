using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartLegend : Entity
    {
        public virtual string position { get; set; }
        public virtual bool visible { get; set; }
    }
}