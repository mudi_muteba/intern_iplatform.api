using Domain.Base;

namespace Domain.Dashboard.Charts.Base
{
    public class DashboardChartMajorGridLine : Entity
    {
        public virtual bool visible { get; set; }
    }
}