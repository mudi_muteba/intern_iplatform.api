﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard
{
    public class DashboardOverride : IAutoMappingOverride<Dashboard>
    {
        public void Override(AutoMapping<Dashboard> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
