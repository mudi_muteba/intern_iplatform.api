﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Dashboard
{
    public class DashboardStatisticsOverride : IAutoMappingOverride<DashboardStatistics>
    {
        public void Override(AutoMapping<DashboardStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
