using Domain.Base;

namespace Domain.Dashboard
{
    public class Dashboard : Entity
    {
        public Dashboard() { }

        public virtual DashboardStatistics Statistics { get; set; }
    }
}