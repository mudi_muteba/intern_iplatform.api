﻿using System.ComponentModel.DataAnnotations;
using Domain.Admin;
using Domain.Base;

namespace Domain.ChannelTags
{
    public class ChannelTag : Entity
    {
        [Required]
        public virtual Channel Channel { get; set; }

        protected ChannelTag()
        {
        }

        public ChannelTag(Channel channel)
        {
            Channel = channel;
        }
    }
}