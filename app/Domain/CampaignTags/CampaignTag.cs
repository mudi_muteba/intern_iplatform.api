﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;
using Domain.Campaigns;

namespace Domain.CampaignTags
{
    public class CampaignTag : Entity
    {
        [Required]
        public virtual Campaign Campaign { get; set; }

        protected CampaignTag()
        {
        }

        public CampaignTag(Campaign campaign)
        {
            Campaign = campaign;
        }
    }
}