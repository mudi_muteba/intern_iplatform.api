﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Activities;
using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Statistics;
using Domain.Base.Execution;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Statistics.Handlers
{
    public class SameDayConversionHandler : BaseDtoHandler<GetDayStatsDto, StatisticsDto>
    {
        private readonly IRepository _repository;

        public SameDayConversionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void InternalHandle(GetDayStatsDto dto, HandlerResult<StatisticsDto> result)
        {
            var stats = new StatisticsDto();

            var date = DateTime.UtcNow;
            var today = date.Date;

            var createActivities = _repository.GetAll<CreateLeadActivity>()
                                                .Where(a => a.User.Id == dto.AgentId 
                                                    && a.DateCreated > today 
                                                    && a.IsDeleted == false)
                                                .ToList();

            var createLeadIds = createActivities.Select(a => a.Lead.Id).ToList();


            if (createActivities.Any())
            {
                var quoteAccpetedActivities = _repository.GetAll<CreateLeadActivity>()
                    .Count(a => a.User.Id == dto.AgentId
                                && a.DateCreated > today
                                && a.IsDeleted == false
                                && createLeadIds.Contains(a.Lead.Id));

                stats = new StatisticsDto
                {
                    NewLeads = createActivities.Count(),
                    QuotesAccepted = quoteAccpetedActivities
                };
            }

            result.Processed(stats);
        }
    }
}
