﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Activities;
using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Statistics;
using Domain.Base.Execution;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Statistics.Handlers
{
    public class MonthlyConversionHandler : BaseDtoHandler<GetMonthlyStatsDto, StatisticsDto>
    {
        private readonly IRepository _repository;

        public MonthlyConversionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {;
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void InternalHandle(GetMonthlyStatsDto dto, HandlerResult<StatisticsDto> result)
        {
            var now = DateTime.UtcNow;
            var startOfMonth = new DateTime(now.Year, now.Month, 1);

            var createActivities = _repository.GetAll<CreateLeadActivity>()
                                                .Count(a => a.User.Id == dto.AgentId 
                                                    && a.DateCreated > startOfMonth 
                                                    && a.IsDeleted == false);

            var quoteAccpetedActivities = _repository.GetAll<QuoteAcceptedLeadActivity>()
                .Count(a => a.User.Id == dto.AgentId
                            && a.DateCreated > startOfMonth
                            && a.IsDeleted == false);

            var stats = new StatisticsDto
            {
                NewLeads = createActivities,
                QuotesAccepted = quoteAccpetedActivities
            };

            result.Processed(stats);
        }
    }
}
