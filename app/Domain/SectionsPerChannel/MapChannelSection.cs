﻿using Domain.Base;

namespace Domain.SectionsPerChannel
{
    public class MapChannelSection : Entity
    {
        public virtual string Section { get; set; }
        public virtual int ChannelId { get; set; }

    }
}