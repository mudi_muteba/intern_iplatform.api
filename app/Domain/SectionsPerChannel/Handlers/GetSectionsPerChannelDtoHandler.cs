﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Individuals;
using Domain.Leads;
using Domain.Party.Quotes;
using Domain.Party.Quotes.Builders;
using Domain.Party.Quotes.Events;
using Domain.Proposals.Mappings;
using Domain.Ratings.Engines;
using Domain.SalesForce.Events;
using Domain.Users;
using iPlatform.Api.DTOs.Proposals;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData.Authorisation;
using NHibernate.Proxy;
using Domain.Activities;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using Domain.SectionsPerChannel;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.SectionsPerChannel;

namespace Domain.SectionsPerChannel.Handlers
{
    public class GetSectionsPerChannelDtoHandler : BaseDtoHandler<GetSectionsPerChannelDto, ResultChannelSectionDto>
    {
        private readonly IProvideContext _contextProvider;
        private readonly IRatingEngine _engine;
        private readonly IExecutionPlan _executionPlan;
        private readonly SystemChannels _systemChannels;
        private readonly IRepository _repository;
        private ExecutionResult _result;

        public GetSectionsPerChannelDtoHandler(IProvideContext contextProvider, IRepository repository, IRatingEngine engine,
            IExecutionPlan executionPlan,SystemChannels systemChannels)
            : base(contextProvider)
        {
            _repository = repository;
            _contextProvider = contextProvider;
            _engine = engine;
            _executionPlan = executionPlan;
            _systemChannels = systemChannels;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void InternalHandle(GetSectionsPerChannelDto dto, HandlerResult<ResultChannelSectionDto> result)
        {
            _result = result;

            var channels = GetSectionsByChannelId(dto.ChannelId);
            if (channels.Any())
            {

                var sections = channels.Select(x => x.Section).ToList();
                var response = new ResultChannelSectionDto() {ChannelId = dto.ChannelId, Sections = sections }; 
                result.Processed(response);
            }
        }

        

        private List<MapChannelSection> GetSectionsByChannelId(int channelid)
        {
            return _repository.GetAll<MapChannelSection>().Where(x => x.ChannelId == channelid).ToList();
        }


    }
}