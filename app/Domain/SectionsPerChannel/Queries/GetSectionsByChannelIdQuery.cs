﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.SectionsPerChannel;
using MasterData.Authorisation;

namespace Domain.SectionsPerChannel.Queries
{
    public class GetSectionsByChannelIdQuery : BaseQuery<MapChannelSection>, ISearchQuery<GetSectionsPerChannelDto>
    {
        private GetSectionsPerChannelDto criteria;

        public GetSectionsByChannelIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultSGetSectionsByChannelIdQueryalesTagFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    //CampaignAuthorisation.List
                };
            }
        }

        protected internal override IQueryable<MapChannelSection> Execute(IQueryable<MapChannelSection> query)
        {
            query =  query.Where(x=> x.ChannelId == criteria.ChannelId);

            return query;
        }



        public void WithCriteria(GetSectionsPerChannelDto criteria)
        {
            this.criteria = criteria;
        }
    }
}
      