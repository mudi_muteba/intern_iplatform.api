﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.SectionsPerChannel.Queries
{
    public class DefaultSGetSectionsByChannelIdQueryalesTagFilters : IApplyDefaultFilters<MapChannelSection>
    {
        public IQueryable<MapChannelSection> Apply(ExecutionContext executionContext, IQueryable<MapChannelSection> query)
        {
            return query
                .Where(c => c.IsDeleted == false);
        }
    }
}