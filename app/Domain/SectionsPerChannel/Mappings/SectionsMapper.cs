﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Repository;
using Domain.Sales;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Sales;
using iPlatform.Api.DTOs.SectionsPerChannel;

namespace Domain.SectionsPerChannel.Mappings
{
    public class SectionsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<MapChannelSection, ChannelSectionDto>();
            Mapper.CreateMap<PagedResults<MapChannelSection>, PagedResultDto<ChannelSectionDto>>();

        }
    }
}
