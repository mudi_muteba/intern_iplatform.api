﻿using Domain.Base;

namespace Domain.Reports.PolicyBinding
{
    public class PolicyBindingReportQuestionAnwser : Entity
    {
        public PolicyBindingReportQuestionAnwser() { }

        public PolicyBindingReportQuestionAnwser(object[] obj)
        {
            Question = obj[0] != null ? (string)obj[0] : "";
            Answer = obj[1] != null ? (string)obj[1] : "";
        }

        public virtual string Question { get; set; }
        public virtual string Answer { get; set; }
    }
}
