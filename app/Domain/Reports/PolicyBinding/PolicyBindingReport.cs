﻿using Domain.Reports.Base;
using iPlatform.Api.DTOs.Reports.PolicyBinding;
using iPlatform.Api.DTOs.Reports.PolicyBinding.Criteria;

namespace Domain.Reports.PolicyBinding
{
    public class PolicyBindingReport : ReportGenerator
    {
        public PolicyBindingReport() : base() { }

        public virtual PolicyBindingReportContainerDto Get(int quoteId, int channelId, int currentChannelId, int partyId, int agentId)
        {
            var criteria = new PolicyBindingReportCriteriaDto
            {
                QuoteId = quoteId,
                ChannelId = channelId,
                PartyId = partyId,
                AgentId = agentId,
                CurrentChannelId = currentChannelId
            };

            var response = Connector.ReportManagement.PolicyBindingReport.GetPolicyBindingReport(criteria).Response;
            return response;
        }
    }
}
