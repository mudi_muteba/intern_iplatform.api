﻿using Domain.Base;

namespace Domain.Reports.PolicyBinding
{
    public class PolicyBindingReportClient : Entity
    {
        public PolicyBindingReportClient()
        {
        }

        public virtual string DisplayName { get; set; }
        public virtual string Email { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Address { get; set; }
    }
}
