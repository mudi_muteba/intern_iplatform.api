﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.PolicyBinding.Queries.Filters;
using Domain.Reports.PolicyBinding.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.PolicyBinding.Queries
{
    public class GetProposalCoverReportQuestionAnwserQuery : BaseStoredProcedureQuery<ProposalCoverReportQuestionAnwser, ProposalCoverReportQuestionAnwserStoredProcedure>
    {
        public GetProposalCoverReportQuestionAnwserQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultProposalCoverReportQuestionAnwserFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ProposalCoverReportQuestionAnwser> Execute(IList<ProposalCoverReportQuestionAnwser> query)
        {
            return query;
        }
    }
}