﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.PolicyBinding.Queries.Filters
{
    public class DefaultPolicyBindingReportQuestionAnwserFilters : IApplyDefaultFilters<PolicyBindingReportQuestionAnwser>
    {
        public IQueryable<PolicyBindingReportQuestionAnwser> Apply(ExecutionContext executionContext, IQueryable<PolicyBindingReportQuestionAnwser> query)
        {
            return query;
        }
    }
}