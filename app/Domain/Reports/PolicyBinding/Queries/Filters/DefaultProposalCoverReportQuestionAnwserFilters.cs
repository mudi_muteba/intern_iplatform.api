﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.PolicyBinding.Queries.Filters
{
    public class DefaultProposalCoverReportQuestionAnwserFilters : IApplyDefaultFilters<ProposalCoverReportQuestionAnwser>
    {
        public IQueryable<ProposalCoverReportQuestionAnwser> Apply(ExecutionContext executionContext, IQueryable<ProposalCoverReportQuestionAnwser> query)
        {
            return query;
        }
    }
}
