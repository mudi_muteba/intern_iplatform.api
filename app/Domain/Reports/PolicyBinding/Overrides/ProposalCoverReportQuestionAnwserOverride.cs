﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.PolicyBinding.Overrides
{
    public class ProposalCoverReportQuestionAnwserOverride : IAutoMappingOverride<ProposalCoverReportQuestionAnwser>
    {
        public void Override(AutoMapping<ProposalCoverReportQuestionAnwser> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}