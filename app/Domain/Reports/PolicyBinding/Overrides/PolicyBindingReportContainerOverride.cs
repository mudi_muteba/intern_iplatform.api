﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.PolicyBinding.Overrides
{
    public class PolicyBindingReportContainerOverride : IAutoMappingOverride<PolicyBindingReportContainer>
    {
        public void Override(AutoMapping<PolicyBindingReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
