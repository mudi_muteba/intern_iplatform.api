﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.PolicyBinding.Overrides
{
    public class PolicyBindingReportClientOverride : IAutoMappingOverride<PolicyBindingReportClient>
    {
        public void Override(AutoMapping<PolicyBindingReportClient> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}

