﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.PolicyBinding.Overrides
{
    public class PolicyBindingReportQuestionAnswerOverride : IAutoMappingOverride<PolicyBindingReportQuestionAnswer>
    {
        public void Override(AutoMapping<PolicyBindingReportQuestionAnswer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}