﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.PolicyBinding.Overrides
{
    public class PolicyBindingReportChannelOverride : IAutoMappingOverride<PolicyBindingReportChannel>
    {
        public void Override(AutoMapping<PolicyBindingReportChannel> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}