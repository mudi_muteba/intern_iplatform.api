﻿using Domain.Base;

namespace Domain.Reports.PolicyBinding
{
    public class PolicyBindingReportQuestionAnswer : Entity
    {
        public PolicyBindingReportQuestionAnswer()
        {
            
        }

        public virtual string Question { get; set; }
        public virtual string Answer { get; set; }
    }
}
