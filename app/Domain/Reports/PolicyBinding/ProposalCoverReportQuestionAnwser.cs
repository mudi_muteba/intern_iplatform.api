﻿using Domain.Base;

namespace Domain.Reports.PolicyBinding
{
    public class ProposalCoverReportQuestionAnwser : Entity
    {
        public ProposalCoverReportQuestionAnwser() { }

        public ProposalCoverReportQuestionAnwser(object[] obj)
        {
            CoverName = obj[0] != null ? (string)obj[0] : "";
            Asset = obj[1] != null ? (string)obj[1] : "";
            Question = obj[2] != null ? (string)obj[2] : "";
            Answer = obj[3] != null ? (string)obj[3] : "";
        }

        public virtual string CoverName { get; set; }
        public virtual string Asset { get; set; }
        public virtual string Question { get; set; }
        public virtual string Answer { get; set; }
    }
}

