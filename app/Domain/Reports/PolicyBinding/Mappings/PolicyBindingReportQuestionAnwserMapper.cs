﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.PolicyBinding;

namespace Domain.Reports.PolicyBinding.Mappings
{
    public class PolicyBindingReportQuestionAnwserMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PolicyBindingReportQuestionAnwser, PolicyBindingReportQuestionAnswerDto>();

            Mapper.CreateMap<QueryResult<IList<PolicyBindingReportQuestionAnwser>>, POSTResponseDto<IList<PolicyBindingReportQuestionAnswerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}
