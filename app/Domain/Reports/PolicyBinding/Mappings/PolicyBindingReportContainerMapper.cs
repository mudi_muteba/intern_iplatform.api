﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.PolicyBinding;

namespace Domain.Reports.PolicyBinding.Mappings
{
    public class PolicyBindingReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PolicyBindingReportContainer, PolicyBindingReportContainerDto>();

            Mapper.CreateMap<QueryResult<PolicyBindingReportContainerDto>, POSTResponseDto<PolicyBindingReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<PolicyBindingReportContainer>>, POSTResponseDto<IList<PolicyBindingReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}
