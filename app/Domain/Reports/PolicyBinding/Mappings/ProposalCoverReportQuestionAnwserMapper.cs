﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.PolicyBinding;

namespace Domain.Reports.PolicyBinding.Mappings
{
    public class ProposalCoverReportQuestionAnwserMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ProposalCoverReportQuestionAnwser, ProposalCoverReportQuestionAnswerDto>();

            Mapper.CreateMap<QueryResult<IList<ProposalCoverReportQuestionAnwser>>, POSTResponseDto<IList<ProposalCoverReportQuestionAnswerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}

