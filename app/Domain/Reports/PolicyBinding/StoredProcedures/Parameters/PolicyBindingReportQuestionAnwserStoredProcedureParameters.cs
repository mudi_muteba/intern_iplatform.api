﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.PolicyBinding.StoredProcedures.Parameters
{
    public class PolicyBindingReportQuestionAnwserStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int QuoteId { get; set; }
    }
}
