﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.PolicyBinding.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.PolicyBinding.Criteria;

namespace Domain.Reports.PolicyBinding.StoredProcedures
{
    public class PolicyBindingReportQuestionAnwserStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_PolicyBinding_QuestionAnswer";
            }
        }

        public PolicyBindingReportQuestionAnwserStoredProcedure() { }

        public PolicyBindingReportQuestionAnwserStoredProcedure(PolicyBindingReportCriteriaDto criteria) : base ()
        {
            base.
                SetParameters(new PolicyBindingReportQuestionAnwserStoredProcedureParameters
                {
                    QuoteId = criteria.QuoteId
                });
        }
    }
}
