﻿using Domain.Base.Repository.StoredProcedures;
using iPlatform.Api.DTOs.Reports.PolicyBinding.Criteria;

namespace Domain.Reports.PolicyBinding.StoredProcedures
{
    public class ProposalCoverReportQuestionAnwserStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_ProposalCover_QuestionAnswer";
            }
        }

        public ProposalCoverReportQuestionAnwserStoredProcedure() { }

        public ProposalCoverReportQuestionAnwserStoredProcedure(ProposalCoverReportCriteriaDto criteria) : base ()
        {
            base.
                SetParameters(new ProposalCoverReportQuestionAnwserStoredProcedureParameters
                {
                    QuoteId = criteria.QuoteId
                });
        }
    }
}
