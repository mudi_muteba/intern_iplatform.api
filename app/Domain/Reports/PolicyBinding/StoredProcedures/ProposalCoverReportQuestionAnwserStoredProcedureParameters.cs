﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.PolicyBinding.StoredProcedures
{
    public class ProposalCoverReportQuestionAnwserStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int QuoteId { get; set; }
    }
}

