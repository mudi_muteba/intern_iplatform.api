﻿using System.Collections.Generic;
using Domain.Base;

namespace Domain.Reports.PolicyBinding
{
    public class PolicyBindingReportContainer : Entity
    {
        public PolicyBindingReportContainer() { }

        public PolicyBindingReportContainer(List<PolicyBindingReportQuestionAnswer> questionAnswers)
        {
            QuestionAnswer = questionAnswers;
        }

        public virtual PolicyBindingReportChannel Channel { get; set; }
        public virtual PolicyBindingReportClient Client { get; set; }
        public virtual List<PolicyBindingReportQuestionAnswer> QuestionAnswer { get; set; }
    }
}