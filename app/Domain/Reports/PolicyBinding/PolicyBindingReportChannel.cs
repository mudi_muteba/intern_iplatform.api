﻿using Domain.Base;

namespace Domain.Reports.PolicyBinding
{
    public class PolicyBindingReportChannel : Entity
    {
        public PolicyBindingReportChannel()
        {
        }

        public virtual string Name { get; set; }
    }
}
