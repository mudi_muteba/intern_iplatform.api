﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.DigitalSignature.Popi.Overrides
{
    public class PopiReportExtractOverride : IAutoMappingOverride<PopiDocReportExtract>
    {
        public void Override(AutoMapping<PopiDocReportExtract> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
