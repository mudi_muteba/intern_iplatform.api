﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.DigitalSignature.Popi.Queries.Filters;
using Domain.Reports.DigitalSignature.Popi.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.DigitalSignature.Popi.Queries
{
    public class GetPopiDocReportQuery : BaseStoredProcedureQuery<PopiDocReportExtract, PopiDocReportStoredProcedure>
    {
        public GetPopiDocReportQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new PopiDocFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<PopiDocReportExtract> Execute(IList<PopiDocReportExtract> query)
        {
            return query;
        }
    }
}