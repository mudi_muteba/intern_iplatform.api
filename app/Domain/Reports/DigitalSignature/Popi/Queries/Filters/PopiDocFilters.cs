﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.SignFlow;

namespace Domain.Reports.DigitalSignature.Popi.Queries.Filters
{
    public class PopiDocFilters : IApplyDefaultFilters<PopiDocReportExtract>
    {
        public IQueryable<PopiDocReportExtract> Apply(ExecutionContext executionContext, IQueryable<PopiDocReportExtract> query)
        {
            return query;
        }
    }
}