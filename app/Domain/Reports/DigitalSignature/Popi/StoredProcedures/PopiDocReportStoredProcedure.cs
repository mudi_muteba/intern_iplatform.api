﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.DigitalSignature.Popi.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.Popi.StoredProcedures
{
    public class PopiDocReportStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_SignFlow_PopiDoc";
            }
        }

        public PopiDocReportStoredProcedure() { }

        public PopiDocReportStoredProcedure(PopiDocReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new PopiDocStoredProcedureParameters
                {
                    Id = criteria.Id
                });
        }
    }
}