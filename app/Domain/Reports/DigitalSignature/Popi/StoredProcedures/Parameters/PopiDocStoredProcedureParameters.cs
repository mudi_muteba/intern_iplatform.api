﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.DigitalSignature.Popi.StoredProcedures.Parameters
{
    public class PopiDocStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int Id { get; set; }
    }
}