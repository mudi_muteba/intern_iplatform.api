﻿using Domain.Reports.Base;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.Popi
{
    public class PopiDocReport : ReportGenerator
    {
        public PopiDocReport() : base() { }

        public virtual PopiDocReportDto Get(int id, string insurerCode)
        {
            var criteria = new PopiDocReportCriteriaDto
            {
                Id = id,
                InsurerCode = insurerCode
            };

            var response = Connector.SignFlowManagement.SignFlow.GetDataForPopiDocReport(criteria).Response;
            return response;
        }
    }
}
