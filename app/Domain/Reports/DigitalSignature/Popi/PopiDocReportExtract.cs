﻿using Domain.Base;

namespace Domain.Reports.DigitalSignature.Popi
{
    public class PopiDocReportExtract : Entity
    {
        public PopiDocReportExtract() { }

        public PopiDocReportExtract(object[] obj)
        {
            this.Client = obj[0] != null ? (string)obj[0] : "";
            this.FirstName = obj[1] != null ? (string)obj[1] : "";
            this.Surname = obj[2] != null ? (string)obj[2] : "";
        }

        public virtual string Client { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string Surname { get; set; }
    }
}
