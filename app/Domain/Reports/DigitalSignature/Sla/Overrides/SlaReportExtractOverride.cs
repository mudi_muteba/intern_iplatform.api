﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.DigitalSignature.Sla.Overrides
{
    public class SlaReportExtractOverride : IAutoMappingOverride<SlaReportExtract>
    {
        public void Override(AutoMapping<SlaReportExtract> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
