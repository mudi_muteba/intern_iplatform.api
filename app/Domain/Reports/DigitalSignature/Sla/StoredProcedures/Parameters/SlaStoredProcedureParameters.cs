﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.DigitalSignature.Sla.StoredProcedures.Parameters
{
    public class SlaStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int Id { get; set; }
        public string InsurerCode { get; set; }
    }
}