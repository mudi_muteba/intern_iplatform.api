﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.DigitalSignature.Sla.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.Sla.StoredProcedures
{
    public class SlaReportStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_SignFlow_Sla";
            }
        }

        public SlaReportStoredProcedure() { }

        public SlaReportStoredProcedure(SlaReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new SlaStoredProcedureParameters
                {
                    Id = criteria.Id,
                    InsurerCode = criteria.InsurerCode
                });
        }
    }
}