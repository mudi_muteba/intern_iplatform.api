﻿using Domain.Reports.Base;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.Sla
{
    public class SlaReport : ReportGenerator
    {
        public SlaReport() : base() { }

        public virtual SlaReportDto Get(int id, string insurerCode)
        {
            var criteria = new SlaReportCriteriaDto
            {
                Id = id,
                InsurerCode = insurerCode
            };

            var response = Connector.SignFlowManagement.SignFlow.GetDataForSlaReport(criteria).Response;
            return response;
        }
    }
}
