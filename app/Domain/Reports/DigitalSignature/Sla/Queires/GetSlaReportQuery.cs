﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.DigitalSignature.Sla.Queires.Filters;
using Domain.Reports.DigitalSignature.Sla.StoredProcedures;
using Domain.SignFlow;
using MasterData.Authorisation;

namespace Domain.Reports.DigitalSignature.Sla.Queires
{
    public class GetSlaReportQuery : BaseStoredProcedureQuery<SlaReportExtract, SlaReportStoredProcedure>
    {
        public GetSlaReportQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new SlaFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<SlaReportExtract> Execute(IList<SlaReportExtract> query)
        {
            return query;
        }
    }
}