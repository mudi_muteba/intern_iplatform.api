﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.SignFlow;

namespace Domain.Reports.DigitalSignature.Sla.Queires.Filters
{
    public class SlaFilters : IApplyDefaultFilters<SlaReportExtract>
    {
        public IQueryable<SlaReportExtract> Apply(ExecutionContext executionContext, IQueryable<SlaReportExtract> query)
        {
            return query;
        }
    }
}