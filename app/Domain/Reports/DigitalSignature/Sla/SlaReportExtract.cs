﻿using Domain.Base;

namespace Domain.Reports.DigitalSignature.Sla
{
    public class SlaReportExtract : Entity
    {
        public SlaReportExtract() { }

        public SlaReportExtract(object[] obj)
        {
            this.BrokerName = obj[0] != null ? (string)obj[0] : "";
            this.ClientName = obj[1] != null ? (string)obj[1] : "";
            this.ClientIndentityNo = obj[2] != null ? (string)obj[2] : "";
            this.PostalAddress = obj[3] != null ? (string)obj[3] : "";
        }

        public virtual string BrokerName { get; set; }
        public virtual string ClientName { get; set; }
        public virtual string ClientIndentityNo { get; set; }
        public virtual string PostalAddress { get; set; }
    }
}
