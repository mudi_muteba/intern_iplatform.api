﻿using Domain.Reports.Base;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.BrokerNote
{
    public class BrokerNoteReport : ReportGenerator
    {
        public BrokerNoteReport() : base() { }

        public virtual BrokerNoteReportDto Get(int id, string insurerCode)
        {
            var criteria = new BrokerNoteReportCriteriaDto
            {
                Id = id,
                InsurerCode = insurerCode
            };

            var response = Connector.SignFlowManagement.SignFlow.GetDataForBrokerNoteReport(criteria).Response;
            return response;
        }
    }
}
