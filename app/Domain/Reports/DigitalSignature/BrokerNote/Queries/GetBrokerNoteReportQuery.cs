﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.DigitalSignature.BrokerNote.Queries.Filters;
using Domain.Reports.DigitalSignature.BrokerNote.StoredProcedures;
using Domain.SignFlow;
using MasterData.Authorisation;

namespace Domain.Reports.DigitalSignature.BrokerNote.Queries
{
    public class GetBrokerNoteReportQuery : BaseStoredProcedureQuery<BrokerNoteReportExtract, BrokerNoteReportStoredProcedure>
    {
        public GetBrokerNoteReportQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new BrokerNoteFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<BrokerNoteReportExtract> Execute(IList<BrokerNoteReportExtract> query)
        {
            return query;
        }
    }
}
