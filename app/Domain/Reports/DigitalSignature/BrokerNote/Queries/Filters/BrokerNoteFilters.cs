﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.SignFlow;

namespace Domain.Reports.DigitalSignature.BrokerNote.Queries.Filters
{
    public class BrokerNoteFilters : IApplyDefaultFilters<BrokerNoteReportExtract>
    {
        public IQueryable<BrokerNoteReportExtract> Apply(ExecutionContext executionContext, IQueryable<BrokerNoteReportExtract> query)
        {
            return query;
        }
    }
}