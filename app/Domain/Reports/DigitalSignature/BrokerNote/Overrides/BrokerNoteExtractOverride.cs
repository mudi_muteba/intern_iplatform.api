﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.DigitalSignature.BrokerNote.Overrides
{
    public class BrokerNoteExtractOverride : IAutoMappingOverride<BrokerNoteReportExtract>
    {
        public void Override(AutoMapping<BrokerNoteReportExtract> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
