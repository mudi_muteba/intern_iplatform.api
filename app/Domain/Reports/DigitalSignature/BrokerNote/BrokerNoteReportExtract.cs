﻿using Domain.Base;

namespace Domain.Reports.DigitalSignature.BrokerNote
{
    public class BrokerNoteReportExtract : Entity
    {
        public BrokerNoteReportExtract() { }

        public BrokerNoteReportExtract(object[] obj)
        {
            this.Insured = obj[0] != null ? (string)obj[0] : "";
            this.Appointee = obj[1] != null ? (string)obj[1] : "";
        }

        public virtual string Insured { get; set; }
        public virtual string Appointee { get; set; }
    }
}
