﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.DigitalSignature.BrokerNote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.BrokerNote.StoredProcedures
{
    public class BrokerNoteReportStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_SignFlow_BrokerNote";
            }
        }

        public BrokerNoteReportStoredProcedure() { }

        public BrokerNoteReportStoredProcedure(BrokerNoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new BrokerNoteStoredProcedureParameters
                {
                    Id = criteria.Id,
                    InsurerCode = criteria.InsurerCode
                });
        }
    }
}