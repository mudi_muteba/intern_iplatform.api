﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.DigitalSignature.BrokerNote.StoredProcedures.Parameters
{
    public class BrokerNoteStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int Id { get; set; }
        public string InsurerCode { get; set; }
    }
}