﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.Overrides
{
    public class RecordOfAdviceReportExtractOverride : IAutoMappingOverride<RecordOfAdviceReportExtract>
    {
        public void Override(AutoMapping<RecordOfAdviceReportExtract> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
