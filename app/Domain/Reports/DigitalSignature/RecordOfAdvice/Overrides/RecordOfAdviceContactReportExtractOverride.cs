﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.Overrides
{
    public class RecordOfAdviceContactReportExtractOverride : IAutoMappingOverride<RecordOfAdviceContactReportExtract>
    {
        public void Override(AutoMapping<RecordOfAdviceContactReportExtract> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
