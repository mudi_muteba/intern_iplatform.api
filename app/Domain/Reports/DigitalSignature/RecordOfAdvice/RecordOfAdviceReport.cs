﻿using Domain.Reports.Base;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice
{
    public class RecordOfAdviceReport : ReportGenerator
    {
        public RecordOfAdviceReport() : base() { }

        public virtual RecordOfAdviceReportDto Get(int id, string insurerCode)
        {
            var criteria = new RecordOfAdviceReportCriteriaDto
            {
                Id = id,
                InsurerCode = insurerCode
            };

            var response = Connector.SignFlowManagement.SignFlow.GetDataForRecordOfAdviceReport(criteria).Response;
            return response;
        }
    }
}
