﻿using Domain.Base;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice
{
    public class RecordOfAdviceReportExtract : Entity
    {
        public RecordOfAdviceReportExtract() { }

        public RecordOfAdviceReportExtract(object[] obj)
        {
            this.Name = obj[0] != null ? (string)obj[0] : "";
            this.Suminsured = obj[1] != null ? (decimal)obj[1] : 0;
            this.Description = obj[2] != null ? (string)obj[2] : "";
        }

        public virtual string Name { get; set; }
        public virtual decimal Suminsured { get; set; }
        public virtual string Description { get; set; }
    }
}
