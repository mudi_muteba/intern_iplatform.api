﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.StoredProcedures.Parameters
{
    public class RecordOfAdviceContactStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int Id { get; set; }
    }
}