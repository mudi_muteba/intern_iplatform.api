﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.StoredProcedures.Parameters
{
    public class RecordOfAdviceStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int Id { get; set; }
    }
}