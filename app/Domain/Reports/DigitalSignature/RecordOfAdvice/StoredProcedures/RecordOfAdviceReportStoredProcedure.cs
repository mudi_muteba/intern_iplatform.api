﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.DigitalSignature.RecordOfAdvice.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.StoredProcedures
{
    public class RecordOfAdviceReportStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_SignFlow_RecordOfAdvice";
            }
        }

        public RecordOfAdviceReportStoredProcedure() { }

        public RecordOfAdviceReportStoredProcedure(RecordOfAdviceReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new RecordOfAdviceStoredProcedureParameters
                {
                    Id = criteria.Id
                });
        }
    }
}