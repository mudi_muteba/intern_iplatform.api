﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.DigitalSignature.RecordOfAdvice.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.StoredProcedures
{
    public class RecordOfAdviceContactReportStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_SignFlow_RecordOfAdviceContact";
            }
        }

        public RecordOfAdviceContactReportStoredProcedure() { }

        public RecordOfAdviceContactReportStoredProcedure(RecordOfAdviceContactReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new RecordOfAdviceContactStoredProcedureParameters
                {
                    Id = criteria.Id
                });
        }
    }
}