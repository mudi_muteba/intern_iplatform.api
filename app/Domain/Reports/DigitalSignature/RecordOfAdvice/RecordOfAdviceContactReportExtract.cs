﻿using Domain.Base;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice
{
    public class RecordOfAdviceContactReportExtract : Entity
    {
        public RecordOfAdviceContactReportExtract() { }

        public RecordOfAdviceContactReportExtract(object[] obj)
        {
            this.InsuredName = obj[0] != null ? (string)obj[0] : "";
            this.InsuredId = obj[1] != null ? (string)obj[1] : "";
            this.Broker = obj[2] != null ? (string)obj[2] : "";
            this.Occupation = obj[3] != null ? (string)obj[3] : "";
            this.EmailAddress = obj[4] != null ? (string)obj[4] : "";
            this.ContactWork = obj[5] != null ? (string)obj[5] : "";
            this.ContactMobile = obj[6] != null ? (string)obj[6] : "";
            this.ContactHome = obj[7] != null ? (string)obj[7] : "";
            this.PostalAddress = obj[8] != null ? (string)obj[8] : "";
            this.PhysicalAddress = obj[9] != null ? (string)obj[9] : "";
        }

        public virtual string InsuredName { get; set; }
        public virtual string InsuredId { get; set; }
        public virtual string Broker { get; set; }
        public virtual string Occupation { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual string ContactWork { get; set; }
        public virtual string ContactMobile { get; set; }
        public virtual string ContactHome { get; set; }
        public virtual string PostalAddress { get; set; }
        public virtual string PhysicalAddress { get; set; }
    }
}
