﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.Queries.Filters
{
    public class RecordOfAdviceContactFilters : IApplyDefaultFilters<RecordOfAdviceContactReportExtract>
    {
        public IQueryable<RecordOfAdviceContactReportExtract> Apply(ExecutionContext executionContext, IQueryable<RecordOfAdviceContactReportExtract> query)
        {
            return query;
        }
    }
}