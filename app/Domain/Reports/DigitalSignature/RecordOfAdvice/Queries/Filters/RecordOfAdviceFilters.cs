﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.Queries.Filters
{
    public class RecordOfAdviceFilters : IApplyDefaultFilters<RecordOfAdviceReportExtract>
    {
        public IQueryable<RecordOfAdviceReportExtract> Apply(ExecutionContext executionContext, IQueryable<RecordOfAdviceReportExtract> query)
        {
            return query;
        }
    }
}