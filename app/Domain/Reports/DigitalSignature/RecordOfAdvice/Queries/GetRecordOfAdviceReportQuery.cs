﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.DigitalSignature.RecordOfAdvice.Queries.Filters;
using Domain.Reports.DigitalSignature.RecordOfAdvice.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.Queries
{
    public class GetRecordOfAdviceReportQuery : BaseStoredProcedureQuery<RecordOfAdviceReportExtract, RecordOfAdviceReportStoredProcedure>
    {
        public GetRecordOfAdviceReportQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new RecordOfAdviceFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<RecordOfAdviceReportExtract> Execute(IList<RecordOfAdviceReportExtract> query)
        {
            return query;
        }
    }
}