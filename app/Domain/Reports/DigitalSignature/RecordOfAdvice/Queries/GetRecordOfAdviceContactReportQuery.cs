﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.DigitalSignature.RecordOfAdvice.Queries.Filters;
using Domain.Reports.DigitalSignature.RecordOfAdvice.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.DigitalSignature.RecordOfAdvice.Queries
{
    public class GetRecordOfAdviceContactReportQuery : BaseStoredProcedureQuery<RecordOfAdviceContactReportExtract, RecordOfAdviceContactReportStoredProcedure>
    {
        public GetRecordOfAdviceContactReportQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new RecordOfAdviceContactFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<RecordOfAdviceContactReportExtract> Execute(IList<RecordOfAdviceContactReportExtract> query)
        {
            return query;
        }
    }
}