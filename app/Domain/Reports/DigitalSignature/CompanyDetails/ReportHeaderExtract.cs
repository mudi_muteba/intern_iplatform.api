﻿using Domain.Base;

namespace Domain.Reports.DigitalSignature.CompanyDetails
{
    public class ReportHeaderExtract : Entity
    {
        public ReportHeaderExtract() { }

        public ReportHeaderExtract(object[] obj)
        {
            this.CompanyAddress = obj[0] != null ? (string)obj[0] : "";
            this.CompanyWorkTelephone = obj[1] != null ? (string)obj[1] : "";
            this.CompanyMobileTelephone = obj[2] != null ? (string)obj[2] : "";
        }

        public virtual string CompanyAddress { get; set; }
        public virtual string CompanyWorkTelephone { get; set; }
        public virtual string CompanyMobileTelephone { get; set; }
    }
}
