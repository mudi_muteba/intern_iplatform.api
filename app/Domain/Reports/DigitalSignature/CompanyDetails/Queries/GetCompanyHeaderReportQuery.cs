﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.DigitalSignature.CompanyDetails.Queries.Filters;
using Domain.Reports.DigitalSignature.CompanyDetails.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.DigitalSignature.CompanyDetails.Queries
{
    public class GetCompanyHeaderReportQuery : BaseStoredProcedureQuery<ReportHeaderExtract, GetCompanyDetailsStoredProcedure>
    {
        public GetCompanyHeaderReportQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new CompanyDetialsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ReportHeaderExtract> Execute(IList<ReportHeaderExtract> query)
        {
            return query;
        }
    }
}
