﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.DigitalSignature.CompanyDetails.Queries.Filters
{
    public class CompanyDetialsFilters : IApplyDefaultFilters<ReportHeaderExtract>
    {
        public IQueryable<ReportHeaderExtract> Apply(ExecutionContext executionContext, IQueryable<ReportHeaderExtract> query)
        {
            return query;
        }
    }
}