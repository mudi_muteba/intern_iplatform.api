﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.DigitalSignature.CompanyDetails.Overrides
{
    public class ReportHeaderExtractOverride : IAutoMappingOverride<ReportHeaderExtract>
    {
        public void Override(AutoMapping<ReportHeaderExtract> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
