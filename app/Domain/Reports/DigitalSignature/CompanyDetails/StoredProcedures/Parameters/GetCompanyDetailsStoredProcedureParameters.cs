﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.DigitalSignature.CompanyDetails.StoredProcedures.Parameters
{
    public class GetCompanyDetailsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string Code { get; set; }
    }
}