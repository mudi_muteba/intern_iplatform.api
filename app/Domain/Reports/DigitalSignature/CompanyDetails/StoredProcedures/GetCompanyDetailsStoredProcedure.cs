﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.DigitalSignature.CompanyDetails.StoredProcedures.Parameters;

namespace Domain.Reports.DigitalSignature.CompanyDetails.StoredProcedures
{
    public class GetCompanyDetailsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_SignFlow_CompanyDetails";
            }
        }

        public GetCompanyDetailsStoredProcedure() { }

        public GetCompanyDetailsStoredProcedure(string criteria) : base()
        {
            base.
                SetParameters(new GetCompanyDetailsStoredProcedureParameters
                {
                    Code = criteria
                });
        }
    }
}