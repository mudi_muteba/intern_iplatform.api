﻿using Domain.Reports.Base;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.DebitOrder
{
    public class DebitOrderReport : ReportGenerator
    {
        public DebitOrderReport() : base() { }

        public virtual DebitOrderReportDto Get(int id, string insurerCode)
        {
            var criteria = new DebitOrderReportCriteriaDto
            {
                Id = id,
                InsurerCode = insurerCode
            };

            var response = Connector.SignFlowManagement.SignFlow.GetDataForDebitOrderReport(criteria).Response;
            return response;
        }
    }
}
