﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.DigitalSignature.DebitOrder.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;

namespace Domain.Reports.DigitalSignature.DebitOrder.StoredProcedures
{
    public class DebitOrderReportStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_SignFlow_DebitOrder";
            }
        }

        public DebitOrderReportStoredProcedure() { }

        public DebitOrderReportStoredProcedure(DebitOrderReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new DeditOrderStoredProcedureParameters
                {
                    Id = criteria.Id
                });
        }
    }
}