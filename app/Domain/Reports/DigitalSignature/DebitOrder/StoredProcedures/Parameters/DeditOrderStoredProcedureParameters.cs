﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.DigitalSignature.DebitOrder.StoredProcedures.Parameters
{
    public class DeditOrderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int Id { get; set; }
    }
}