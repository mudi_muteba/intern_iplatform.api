﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.SignFlow;

namespace Domain.Reports.DigitalSignature.DebitOrder.Queries.Filters
{
    public class DeditOrderFilters : IApplyDefaultFilters<DebitOrderReportExtract>
    {
        public IQueryable<DebitOrderReportExtract> Apply(ExecutionContext executionContext, IQueryable<DebitOrderReportExtract> query)
        {
            return query;
        }
    }
}