﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.DigitalSignature.DebitOrder.Queries.Filters;
using Domain.Reports.DigitalSignature.DebitOrder.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.DigitalSignature.DebitOrder.Queries
{
    public class GetDebitOrderReportQuery : BaseStoredProcedureQuery<DebitOrderReportExtract, DebitOrderReportStoredProcedure>
    {
        public GetDebitOrderReportQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DeditOrderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<DebitOrderReportExtract> Execute(IList<DebitOrderReportExtract> query)
        {
            return query;
        }
    }
}