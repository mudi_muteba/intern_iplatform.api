﻿using Domain.Base;

namespace Domain.Reports.DigitalSignature.DebitOrder
{
    public class DebitOrderReportExtract : Entity
    {
        public DebitOrderReportExtract() { }

        public DebitOrderReportExtract(object[] obj)
        {
            this.InsuredName = obj[0] != null ? (string)obj[0] : "";
            this.InsuredId = obj[1] != null ? (string)obj[1] : "";
            this.ContactWork = obj[2] != null ? (string)obj[2] : "";
            this.ContactMobile = obj[3] != null ? (string)obj[3] : "";
            this.ContactHome = obj[4] != null ? (string)obj[4] : "";
            this.PostalAddress = obj[5] != null ? (string)obj[5] : "";
            this.BankDetailAccountName = obj[6] != null ? (string)obj[6] : "";
            this.BankDetailsBankName = obj[7] != null ? (string)obj[7] : "";
            this.BankDetailBranchName = obj[8] != null ? (string)obj[8] : "";
            this.BankDetailBranchCode = obj[9] != null ? (string)obj[9] : "";
            this.BankDetailAccountNumber = obj[10] != null ? (string)obj[10] : "";
            this.BankDetailAccountType = obj[11] != null ? (string)obj[11] : "";
        }

        public virtual string InsuredName { get; set; }
        public virtual string InsuredId { get; set; }
        public virtual string ContactWork { get; set; }
        public virtual string ContactMobile { get; set; }
        public virtual string ContactHome { get; set; }
        public virtual string PostalAddress { get; set; }
        public virtual string BankDetailAccountName { get; set; }
        public virtual string BankDetailsBankName { get; set; }
        public virtual string BankDetailBranchName { get; set; }
        public virtual string BankDetailBranchCode { get; set; }
        public virtual string BankDetailAccountNumber { get; set; }
        public virtual string BankDetailAccountType { get; set; }
    }
}
