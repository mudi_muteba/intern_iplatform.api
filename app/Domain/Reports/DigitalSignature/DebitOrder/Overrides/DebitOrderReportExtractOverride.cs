﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.DigitalSignature.DebitOrder.Overrides
{
    public class DebitOrderReportExtractOverride : IAutoMappingOverride<DebitOrderReportExtract>
    {
        public void Override(AutoMapping<DebitOrderReportExtract> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
