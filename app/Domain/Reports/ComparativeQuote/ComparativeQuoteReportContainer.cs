﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportContainer : Entity
    {
        public ComparativeQuoteReportContainer() { }

        public ComparativeQuoteReportContainer(ComparativeQuoteReportSettings settings, ComparativeQuoteReportHeader header, List<ComparativeQuoteReportSummary> summary, List<ComparativeQuoteReportValueAddedProduct> valueAddedProducts, List<ComparativeQuoteReportCover> covers, List<ComparativeQuoteReportProductAddendum> productAddendums)
        {
            this.Settings = settings;
            this.Header = header;
            this.Summary = summary;
            this.ValueAddedProducts = valueAddedProducts;
            this.Covers = covers;
            this.ProductAddendums = productAddendums;
        }

        public virtual ComparativeQuoteReportSettings Settings { get; set; }
        public virtual ComparativeQuoteReportHeader Header { get; set; }
        public virtual List<ComparativeQuoteReportSummary> Summary { get; set; }
        public virtual List<ComparativeQuoteReportValueAddedProduct> ValueAddedProducts { get; set; }
        public virtual List<ComparativeQuoteReportCover> Covers { get; set; }
        public virtual List<ComparativeQuoteReportProductAddendum> ProductAddendums { get; set; }
        public virtual List<ComparativeQuoteReportAdditionalBenefit> AdditionalBenefits { get; set; }
    }
}
