﻿using System.Collections.Generic;
using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportProductAdditionalExcessCategory : Entity
    {
        public ComparativeQuoteReportProductAdditionalExcessCategory() { }

        public ComparativeQuoteReportProductAdditionalExcessCategory(object[] obj)
        {
            this.Category = obj[0] != null ? (string)obj[0] : "";
            this.Nothing = "Not Used";
        }

        public virtual string Category { get; set; }
        public virtual string Nothing { get; set; }

        public virtual List<ComparativeQuoteReportProductAdditionalExcess> AdditionalExcess { get; set; }
    }
}