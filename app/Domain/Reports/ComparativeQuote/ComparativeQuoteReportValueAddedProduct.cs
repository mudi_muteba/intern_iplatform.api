﻿using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportValueAddedProduct : Entity
    {
        public ComparativeQuoteReportValueAddedProduct() { }

        public ComparativeQuoteReportValueAddedProduct(object[] obj)
        {
            this.Code = obj[0] != null ? (string)obj[0] : "";
            this.Product = obj[1] != null ? (string)obj[1] : "";
            this.ValueAddedProducts = obj[2] != null ? (string) obj[2] : "";
            this.Total = obj[3] != null ? (decimal)obj[3] : 0;
        }

        public virtual string Code { get; set; }
        public virtual string Product { get; set; }
        public virtual string ValueAddedProducts { get; set; }
        public virtual decimal Total { get; set; }
    }
}
