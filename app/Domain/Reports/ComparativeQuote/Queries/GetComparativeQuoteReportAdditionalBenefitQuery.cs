﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.ComparativeQuote.Queries.Filters;
using Domain.Reports.ComparativeQuote.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.ComparativeQuote.Queries
{
    public class GetComparativeQuoteReportAdditionalBenefitQuery : BaseStoredProcedureQuery<ComparativeQuoteReportAdditionalBenefit, ComparativeQuoteReportAdditionalBenefitStoredProcedure>
    {
        public GetComparativeQuoteReportAdditionalBenefitQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultComparativeQuoteReportAdditionalBenefitFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ComparativeQuoteReportAdditionalBenefit> Execute(IList<ComparativeQuoteReportAdditionalBenefit> query)
        {
            return query;
        }
    }
}
