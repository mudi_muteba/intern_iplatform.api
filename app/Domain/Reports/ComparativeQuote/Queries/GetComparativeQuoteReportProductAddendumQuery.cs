﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.ComparativeQuote.Queries.Filters;
using Domain.Reports.ComparativeQuote.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.ComparativeQuote.Queries
{
    public class GetComparativeQuoteReportProductAddendumQuery : BaseStoredProcedureQuery<ComparativeQuoteReportProductAddendum, ComparativeQuoteReportProductAddendumStoredProcedure>
    {
        public GetComparativeQuoteReportProductAddendumQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultComparativeQuoteReportProductAddendumFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ComparativeQuoteReportProductAddendum> Execute(IList<ComparativeQuoteReportProductAddendum> query)
        {
            return query;
        }
    }
}
