﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.ComparativeQuote.Queries.Filters;
using Domain.Reports.ComparativeQuote.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.ComparativeQuote.Queries
{
    public class GetComparativeQuoteReportInsurerCoverAssetQuery : BaseStoredProcedureQuery<ComparativeQuoteReportInsurerCoverAsset, ComparativeQuoteReportInsurerCoverAssetStoredProcedure>
    {
        public GetComparativeQuoteReportInsurerCoverAssetQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultComparativeQuoteReportInsurerCoverAssetFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ComparativeQuoteReportInsurerCoverAsset> Execute(IList<ComparativeQuoteReportInsurerCoverAsset> query)
        {
            return query;
        }
    }
}
