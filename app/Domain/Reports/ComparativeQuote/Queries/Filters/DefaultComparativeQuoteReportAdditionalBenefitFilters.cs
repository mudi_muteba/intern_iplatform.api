﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportAdditionalBenefitFilters : IApplyDefaultFilters<ComparativeQuoteReportAdditionalBenefit>
    {
        public IQueryable<ComparativeQuoteReportAdditionalBenefit> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportAdditionalBenefit> query)
        {
            return query;
        }
    }
}