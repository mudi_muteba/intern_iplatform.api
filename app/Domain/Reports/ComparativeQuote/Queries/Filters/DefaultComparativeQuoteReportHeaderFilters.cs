﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportHeaderFilters : IApplyDefaultFilters<ComparativeQuoteReportHeader>
    {
        public IQueryable<ComparativeQuoteReportHeader> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportHeader> query)
        {
            return query;
        }
    }
}