﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportInsurerCoverAdditionalExcessCategoriesFilters : IApplyDefaultFilters<ComparativeQuoteReportProductAdditionalExcessCategory>
    {
        public IQueryable<ComparativeQuoteReportProductAdditionalExcessCategory> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportProductAdditionalExcessCategory> query)
        {
            return query;
        }
    }
}