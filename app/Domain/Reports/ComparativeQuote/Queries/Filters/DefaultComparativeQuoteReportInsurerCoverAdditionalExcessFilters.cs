﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportInsurerCoverAdditionalExcessFilters : IApplyDefaultFilters<ComparativeQuoteReportProductAdditionalExcess>
    {
        public IQueryable<ComparativeQuoteReportProductAdditionalExcess> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportProductAdditionalExcess> query)
        {
            return query;
        }
    }
}