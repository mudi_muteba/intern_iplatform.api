﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportCoverFilters : IApplyDefaultFilters<ComparativeQuoteReportCover>
    {
        public IQueryable<ComparativeQuoteReportCover> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportCover> query)
        {
            return query;
        }
    }
}