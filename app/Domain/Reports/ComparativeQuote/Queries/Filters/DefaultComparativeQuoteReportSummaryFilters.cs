﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportSummaryFilters : IApplyDefaultFilters<ComparativeQuoteReportSummary>
    {
        public IQueryable<ComparativeQuoteReportSummary> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportSummary> query)
        {
            return query;
        }
    }
}