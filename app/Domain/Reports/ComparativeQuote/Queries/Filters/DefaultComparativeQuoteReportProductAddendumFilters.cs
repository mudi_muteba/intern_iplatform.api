﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportProductAddendumFilters : IApplyDefaultFilters<ComparativeQuoteReportProductAddendum>
    {
        public IQueryable<ComparativeQuoteReportProductAddendum> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportProductAddendum> query)
        {
            return query;
        }
    }
}