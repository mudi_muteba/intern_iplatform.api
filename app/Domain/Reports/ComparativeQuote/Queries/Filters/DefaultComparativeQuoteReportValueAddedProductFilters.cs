﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportValueAddedProductFilters : IApplyDefaultFilters<ComparativeQuoteReportValueAddedProduct>
    {
        public IQueryable<ComparativeQuoteReportValueAddedProduct> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportValueAddedProduct> query)
        {
            return query;
        }
    }
}