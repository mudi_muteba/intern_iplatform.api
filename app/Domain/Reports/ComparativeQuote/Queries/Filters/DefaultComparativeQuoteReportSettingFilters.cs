﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportSettingFilters : IApplyDefaultFilters<ComparativeQuoteReportSettings>
    {
        public IQueryable<ComparativeQuoteReportSettings> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportSettings> query)
        {
            return query;
        }
    }
}