﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportInsurerCoverAssetFilters : IApplyDefaultFilters<ComparativeQuoteReportInsurerCoverAsset>
    {
        public IQueryable<ComparativeQuoteReportInsurerCoverAsset> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportInsurerCoverAsset> query)
        {
            return query;
        }
    }
}