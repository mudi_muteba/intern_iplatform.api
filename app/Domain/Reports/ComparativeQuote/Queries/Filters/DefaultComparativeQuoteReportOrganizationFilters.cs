﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportOrganizationFilters : IApplyDefaultFilters<ComparativeQuoteReportOrganization>
    {
        public IQueryable<ComparativeQuoteReportOrganization> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportOrganization> query)
        {
            return query;
        }
    }
}