﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.ComparativeQuote.Queries.Filters
{
    public class DefaultComparativeQuoteReportInsurerCoverBenefitFilters : IApplyDefaultFilters<ComparativeQuoteReportInsurerCoverBenefit>
    {
        public IQueryable<ComparativeQuoteReportInsurerCoverBenefit> Apply(ExecutionContext executionContext, IQueryable<ComparativeQuoteReportInsurerCoverBenefit> query)
        {
            return query;
        }
    }
}