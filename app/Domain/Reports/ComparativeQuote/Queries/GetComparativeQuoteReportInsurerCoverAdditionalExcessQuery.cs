﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.ComparativeQuote.Queries.Filters;
using Domain.Reports.ComparativeQuote.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.ComparativeQuote.Queries
{
    public class GetComparativeQuoteReportInsurerCoverAdditionalExcessQuery : BaseStoredProcedureQuery<ComparativeQuoteReportProductAdditionalExcess, ComparativeQuoteReportInsurerCoverAdditionalExcessStoredProcedure>
    {
        public GetComparativeQuoteReportInsurerCoverAdditionalExcessQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultComparativeQuoteReportInsurerCoverAdditionalExcessFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ComparativeQuoteReportProductAdditionalExcess> Execute(IList<ComparativeQuoteReportProductAdditionalExcess> query)
        {
            return query;
        }
    }
}
