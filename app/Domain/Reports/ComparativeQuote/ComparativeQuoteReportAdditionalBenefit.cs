﻿using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportAdditionalBenefit : Entity
    {
        public ComparativeQuoteReportAdditionalBenefit() { }

        public ComparativeQuoteReportAdditionalBenefit(object[] obj)
        {
            this.Name = obj[0] != null ? (string)obj[0] : "";
            this.Value = obj[1] != null ? (string)obj[1] : "";
        }

        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
    }
}
