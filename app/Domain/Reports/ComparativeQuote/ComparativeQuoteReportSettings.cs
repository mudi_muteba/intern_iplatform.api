﻿using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportSettings : Entity
    {
        public ComparativeQuoteReportSettings() { }

        public ComparativeQuoteReportSettings(object[] obj)
        {
            #region Assignments
            this.QuoteCode = (string)obj[0];

            this.QuoteTitleLabel = (string)obj[1];
            this.QuoteSummaryLabel = (string)obj[2];
            this.QuoteAssetsLabel = (string)obj[3];
            this.QuoateDateLabel = (string)obj[4];
            this.QuoteExpiresLabel = (string)obj[5];
            this.QuoteNumberLabel = (string)obj[6];

            this.ClientHeaderLabel = (string)obj[7];
            this.ClientGeneralNameLabel = (string)obj[8];
            this.ClientGeneralIdentityLabel = (string)obj[9];
            this.ClientAddressPhysicalLabel = (string)obj[10];
            this.ClientAddressPostalLabel = (string)obj[11];
            this.ClientContactWorkLabel = (string)obj[12];
            this.ClientContactHomeLabel = (string)obj[13];
            this.ClientContactCellLabel = (string)obj[14];
            this.ClientContactFaxLabel = (string)obj[15];
            this.ClientContactEmailLabel = (string)obj[16];

            this.CompanyHeaderLabel = (string)obj[17];
            this.CompanyGeneralNameLabel = (string)obj[18];
            this.CompanyAddressPhysicalLabel = (string)obj[19];
            this.CompanyAddressPostalLabel = (string)obj[20];
            this.CompanyContactWorkLabel = (string)obj[21];
            this.CompanyContactFaxLabel = (string)obj[22];
            this.CompanyContactEmailLabel = (string)obj[23];
            this.CompanyContactWebsiteLabel = (string)obj[24];
            this.CompanyAuthRegistrationLabel = (string)obj[25];
            this.CompanyAuthTaxLabel = (string)obj[26];
            this.CompanyAuthLicenseLabel = (string)obj[27];

            this.CompanyHeaderValue = (string)obj[28];
            this.CompanyGeneralNameValue = (string)obj[29];
            this.CompanyAddressPhysicalValue = (string)obj[30];
            this.CompanyAddressPostalValue = (string)obj[31];
            this.CompanyContactWorkValue = (string)obj[32];
            this.CompanyContactFaxValue = (string)obj[33];
            this.CompanyContactEmailValue = (string)obj[34];
            this.CompanyContactWebsiteValue = (string)obj[35];
            this.CompanyAuthRegistrationValue = (string)obj[36];
            this.CompanyAuthTaxValue = (string)obj[37];
            this.CompanyAuthLicenseValue = (string)obj[38];

            this.QuoteTitleVisibility = (bool)obj[39];
            this.QuoteSummaryVisibility = (bool)obj[40];
            this.QuoteAssetsVisibility = (bool)obj[41];
            this.QuoateDateVisibility = (bool)obj[42];
            this.QuoteExpiresVisibility = (bool)obj[43];
            this.QuoteNumberVisibility = (bool)obj[44];

            this.ClientHeaderVisibility = (bool)obj[45];
            this.ClientGeneralNameVisibility = (bool)obj[46];
            this.ClientGeneralIdentityVisibility = (bool)obj[47];
            this.ClientAddressPhysicalVisibility = (bool)obj[48];
            this.ClientAddressPostalVisibility = (bool)obj[49];
            this.ClientContactWorkVisibility = (bool)obj[50];
            this.ClientContactHomeVisibility = (bool)obj[51];
            this.ClientContactCellVisibility = (bool)obj[52];
            this.ClientContactFaxVisibility = (bool)obj[53];
            this.ClientContactEmailVisibility = (bool)obj[54];

            this.CompanyHeaderVisibility = (bool)obj[55];
            this.CompanyGeneralNameVisibility = (bool)obj[56];
            this.CompanyAddressPhysicalVisibility = (bool)obj[57];
            this.CompanyAddressPostalVisibility = (bool)obj[58];
            this.CompanyContactWorkVisibility = (bool)obj[59];
            this.CompanyContactFaxVisibility = (bool)obj[60];
            this.CompanyContactEmailVisibility = (bool)obj[61];
            this.CompanyContactWebsiteVisibility = (bool)obj[62];
            this.CompanyAuthRegistrationVisibility = (bool)obj[63];
            this.CompanyAuthTaxVisibility = (bool)obj[64];
            this.CompanyAuthLicenseVisibility = (bool)obj[65];

            this.CompanyHeaderVisibility = (bool)obj[66];
            this.CompanyGeneralNameVisibility = (bool)obj[67];
            this.CompanyAddressPhysicalVisibility = (bool)obj[68];

            this.QuoteFooter = (string)obj[69];

            this.QuoteSummaryFeesVisibility = (bool)obj[70];
            this.QuoteSummaryTotalVisibility = (bool)obj[71];
            this.CostFooterVisiblity = (bool)obj[72];

            //New Fields
            this.QuoteAdditionalExcessLabel = (string) obj[73];
            this.QuoteProductDescriptionLabel = (string) obj[74];
            this.CompanyStatutoryDisclosureLabel = (string) obj[75];
            this.CompanyStatutoryDisclosureValue = (string) obj[76];
            #endregion
        }

        #region General
        public virtual string QuoteCode { get; set; }
        public virtual string QuoteFooter { get; set; }
        #endregion

        #region Labels
        public virtual string QuoteTitleLabel { get; set; }
        public virtual string QuoteSummaryLabel { get; set; }
        public virtual string QuoteAssetsLabel { get; set; }
        public virtual string QuoateDateLabel { get; set; }
        public virtual string QuoteExpiresLabel { get; set; }
        public virtual string QuoteNumberLabel { get; set; }
        public virtual string QuoteAdditionalExcessLabel { get; set; }
        public virtual string QuoteProductDescriptionLabel { get; set; }

        public virtual string ClientHeaderLabel { get; set; }
        public virtual string ClientGeneralNameLabel { get; set; }
        public virtual string ClientGeneralIdentityLabel { get; set; }
        public virtual string ClientAddressPhysicalLabel { get; set; }
        public virtual string ClientAddressPostalLabel { get; set; }
        public virtual string ClientContactWorkLabel { get; set; }
        public virtual string ClientContactHomeLabel { get; set; }
        public virtual string ClientContactCellLabel { get; set; }
        public virtual string ClientContactFaxLabel { get; set; }
        public virtual string ClientContactEmailLabel { get; set; }
        public virtual string ClientContactIdentityLabel { get; set; }

        public virtual string CompanyHeaderLabel { get; set; }
        public virtual string CompanyGeneralNameLabel { get; set; }
        public virtual string CompanyAddressPhysicalLabel { get; set; }
        public virtual string CompanyAddressPostalLabel { get; set; }
        public virtual string CompanyContactWorkLabel { get; set; }
        public virtual string CompanyContactFaxLabel { get; set; }
        public virtual string CompanyContactEmailLabel { get; set; }
        public virtual string CompanyAuthRegistrationLabel { get; set; }
        public virtual string CompanyAuthTaxLabel { get; set; }
        public virtual string CompanyAuthLicenseLabel { get; set; }
        public virtual string CompanyContactWebsiteLabel { get; set; }
        public virtual string CompanyStatutoryDisclosureLabel { get; set; }
        #endregion

        #region Values
        public virtual string CompanyHeaderValue { get; set; }
        public virtual string CompanyGeneralNameValue { get; set; }
        public virtual string CompanyAddressPhysicalValue { get; set; }
        public virtual string CompanyAddressPostalValue { get; set; }
        public virtual string CompanyContactWorkValue { get; set; }
        public virtual string CompanyContactCellValue { get; set; }
        public virtual string CompanyContactFaxValue { get; set; }
        public virtual string CompanyContactEmailValue { get; set; }
        public virtual string CompanyAuthRegistrationValue { get; set; }
        public virtual string CompanyAuthTaxValue { get; set; }
        public virtual string CompanyAuthLicenseValue { get; set; }
        public virtual string CompanyContactWebsiteValue { get; set; }
        public virtual string CompanyStatutoryDisclosureValue { get; set; }
        
        #endregion

        #region Visiblity
        public virtual bool QuoteTitleVisibility { get; set; }
        public virtual bool QuoteSummaryVisibility { get; set; }
        public virtual bool QuoteAssetsVisibility { get; set; }
        public virtual bool QuoateDateVisibility { get; set; }
        public virtual bool QuoteExpiresVisibility { get; set; }
        public virtual bool QuoteNumberVisibility { get; set; }

        public virtual bool ClientHeaderVisibility { get; set; }
        public virtual bool ClientGeneralNameVisibility { get; set; }
        public virtual bool ClientGeneralIdentityVisibility { get; set; }
        public virtual bool ClientAddressPhysicalVisibility { get; set; }
        public virtual bool ClientAddressPostalVisibility { get; set; }
        public virtual bool ClientContactWorkVisibility { get; set; }
        public virtual bool ClientContactHomeVisibility { get; set; }
        public virtual bool ClientContactCellVisibility { get; set; }
        public virtual bool ClientContactFaxVisibility { get; set; }
        public virtual bool ClientContactEmailVisibility { get; set; }

        public virtual bool CompanyHeaderVisibility { get; set; }
        public virtual bool CompanyGeneralNameVisibility { get; set; }
        public virtual bool CompanyAddressPhysicalVisibility { get; set; }
        public virtual bool CompanyAddressPostalVisibility { get; set; }
        public virtual bool CompanyContactWorkVisibility { get; set; }
        public virtual bool CompanyContactFaxVisibility { get; set; }
        public virtual bool CompanyContactEmailVisibility { get; set; }
        public virtual bool CompanyAuthRegistrationVisibility { get; set; }
        public virtual bool CompanyAuthTaxVisibility { get; set; }
        public virtual bool CompanyAuthLicenseVisibility { get; set; }
        public virtual bool CompanyContactWebsiteVisibility { get; set; }

        public virtual bool QuoteSummaryFeesVisibility { get; set; }
        public virtual bool QuoteSummaryTotalVisibility { get; set; }
        public virtual bool CostFooterVisiblity { get; set; }
        #endregion
    }
}
