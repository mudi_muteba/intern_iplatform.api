﻿using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportProductAddendum : Entity
    {
        public ComparativeQuoteReportProductAddendum() { }

        public ComparativeQuoteReportProductAddendum(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string)obj[0] : "";
            this.Product = obj[1] != null ? (string)obj[1] : "";
            this.QuoteValidFor = obj[2] != null ? (int)obj[2] : 0;
            this.Addendum = obj[3] != null ? (string)obj[3] : "";
        }

        public virtual string Insurer { get; set; }
        public virtual string Product { get; set; }
        public virtual int QuoteValidFor { get; set; }
        public virtual string Addendum { get; set; }
    }
}
