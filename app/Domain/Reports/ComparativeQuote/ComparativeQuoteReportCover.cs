﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportCover : Entity
    {
        public ComparativeQuoteReportCover() { }

        public ComparativeQuoteReportCover(object[] obj)
        {
            this.CoverId = obj[0] != null ? (int)obj[0] : 0;
            this.Description = obj[1] != null ? (string)obj[1] : "";
        }

        public virtual int CoverId { get; set; }
        public virtual string Description { get; set; }

        public virtual int Count { get; set; }

        public virtual List<ComparativeQuoteReportInsurerCoverAsset> Assets { get; set; }

        public virtual List<ComparativeQuoteReportInsurerCoverBenefit> All { get; set; }
        public virtual List<ComparativeQuoteReportInsurerCoverBenefit> Header { get; set; }
        public virtual List<ComparativeQuoteReportInsurerCoverBenefit> Totals { get; set; }
        public virtual List<ComparativeQuoteReportInsurerCoverBenefit> CommonBenefits { get; set; }
        public virtual List<ComparativeQuoteReportInsurerCoverBenefit> DifferingBenefits { get; set; }

        public virtual List<ComparativeQuoteReportOrganization> Organizations { get; set; }
    }
}
