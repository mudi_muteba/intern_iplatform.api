﻿using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportSummary : Entity
    {
        public ComparativeQuoteReportSummary() { }

        public ComparativeQuoteReportSummary(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string)obj[0] : "";
            this.Code = obj[1] != null ? (string)obj[1] : "";
            this.Product = obj[2] != null ? (string)obj[2] : "";
            this.BasicExcess = obj[3] != null ? (decimal)obj[3] : 0;
            this.Premium = obj[4] != null ? (decimal)obj[4] : 0;
            this.SASRIA = obj[5] != null ? (decimal)obj[5] : 0;
            this.Fees = obj[6] != null ? (decimal)obj[6] : 0;
            this.Total = obj[7] != null ? (decimal)obj[7] : 0;
            this.Commission = obj[8] != null ? (decimal) obj[8] : 0;
        }

        public virtual string Insurer { get; set; }
        public virtual string Code { get; set; }
        public virtual string Product { get; set; }
        public virtual decimal BasicExcess { get; set; }
        public virtual decimal Premium { get; set; }
        public virtual decimal SASRIA { get; set; }
        public virtual decimal Fees { get; set; }
        public virtual decimal Total { get; set; }
        public virtual decimal Commission { get; set; }
    }
}
