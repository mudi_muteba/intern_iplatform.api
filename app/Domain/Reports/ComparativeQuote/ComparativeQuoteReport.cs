﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.ComparativeQuote;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReport : ReportGenerator
    {
        public ComparativeQuoteReport() : base() { }

        public virtual ComparativeQuoteReportContainerDto Get(int ChannelId, string OrganizationCode, int ProposalHeaderId, string QuoteIds, string ReportName)
        {
            var criteria = new ComparativeQuoteReportCriteriaDto
            {
                ChannelId = ChannelId,
                OrganizationCode = OrganizationCode,
                ProposalHeaderId = ProposalHeaderId,
                QuoteIds = QuoteIds,
                ReportName = ReportName
            };

            var response = Connector.ReportManagement.ComparativeQuoteReport.GetComparativeQuoteReport(criteria).Response;
            return response;
        }
    }
}
