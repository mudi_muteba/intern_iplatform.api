﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportOrganization : Entity
    {
        public ComparativeQuoteReportOrganization() { }

        public ComparativeQuoteReportOrganization(object[] obj)
        {
            this.OrganizationId = obj[0] != null ? (int)obj[0] : 0;
            this.TradingName = obj[1] != null ? (string)obj[1] : "";
            this.Code = obj[2] != null ? (string) obj[2] : "";
            this.CoverDefinitionIds = obj[3] != null ? (string) obj[3] : "";
        }

        public virtual int OrganizationId { get; set; }
        public virtual string TradingName { get; set; }
        public virtual string Code { get; set; }
        public virtual string CoverDefinitionIds { get; set; }
        public virtual int Count { get; set; }

        public virtual List<ComparativeQuoteReportProductAdditionalExcessCategory> AdditionalExcessCategories { get; set; }
    }
}
