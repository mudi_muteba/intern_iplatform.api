﻿using System;

using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportHeader : Entity
    {
        public ComparativeQuoteReportHeader() { }

        public ComparativeQuoteReportHeader(object[] obj)
        {
            this.Client = obj[0] != null ? (string)obj[0] : "";
            this.PhysicalAddress = obj[1] != null ? (string)obj[1] : "";
            this.PostalAddress = obj[2] != null ? (string)obj[2] : "";
            this.WorkNo = obj[3] != null ? (string)obj[3] : "";
            this.HomeNo = obj[4] != null ? (string)obj[4] : "";
            this.FaxNo = obj[5] != null ? (string)obj[5] : "";
            this.CellNo = obj[6] != null ? (string)obj[6] : "";
            this.EmailAddress = obj[7] != null ? (string)obj[7] : "";
            this.IdentityNo = obj[8] != null ? (string)obj[8] : "";
            this.QuoteHeaderId = obj[9] != null ? (int)obj[9] : 0;
            this.QuoteNo = obj[10] != null ? (string)obj[10] : "";
            this.QuoteExpiration = obj[11] != null ? (int)obj[11] : 30;
            this.Fees = obj[12] != null ? (decimal)obj[12] : 0;
            this.Sasria = obj[13] != null ? (decimal)obj[13] : 0;
            this.Total = obj[14] != null ? (decimal)obj[14] : 0;
            this.EMailBody = obj[15] != null ? (string)obj[15] : "";
            this.DefaultPhysical = obj[16] != null ? (string)obj[16] : "";
            this.DefaultPostal = obj[17] != null ? (string)obj[17] : "";
            this.NoDefaultPhysical = obj[18] != null ? (string)obj[18] : "";
            this.NoDefaultPostal = obj[19] != null ? (string)obj[19] : "";
            this.FirstName = obj[20] != null ? (string)obj[20] : "";
            this.Surname = obj[21] != null ? (string)obj[21] : "";
        }

        public virtual string Client { get; set; }
        public virtual string PhysicalAddress { get; set; }
        public virtual string PostalAddress { get; set; }
        public virtual string WorkNo { get; set; }
        public virtual string HomeNo { get; set; }
        public virtual string FaxNo { get; set; }
        public virtual string CellNo { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual string IdentityNo { get; set; }
        public virtual string QuoteNo { get; set; }
        public virtual int QuoteHeaderId { get; set; }
        public virtual int QuoteExpiration { get; set; }
        public virtual string EMailBody { get; set; }
        public virtual string DefaultPhysical { get; set; }
        public virtual string DefaultPostal { get; set; }
        public virtual string NoDefaultPhysical { get; set; }
        public virtual string NoDefaultPostal { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual decimal Fees { get; set; }
        public virtual decimal Sasria { get; set; }
        public virtual decimal Total { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string Surname { get; set; }
    }
}
