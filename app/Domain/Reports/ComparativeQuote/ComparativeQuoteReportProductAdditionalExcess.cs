﻿using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportProductAdditionalExcess : Entity
    {
        public ComparativeQuoteReportProductAdditionalExcess() { }

        public ComparativeQuoteReportProductAdditionalExcess(object[] obj)
        {
            this.ProductAdditionalExcessId = obj[0] != null ? (int)obj[0] : 0;
            this.Index = obj[1] != null ? (int)obj[1] : 0;
            this.Category = obj[2] != null ? (string)obj[2] : "";
            this.Description = obj[3] != null ? (string)obj[3] : "";
            this.ActualExcess = obj[4] != null ? (decimal)obj[4] : 0;
            this.MinExcess = obj[5] != null ? (decimal)obj[5] : 0;
            this.MaxExcess = obj[6] != null ? (decimal)obj[6] : 0;
            this.Percentage = obj[7] != null ? (decimal)obj[7] : 0;
            this.IsPercentageOfClaim = obj[8] != null ? (bool)obj[8] : false;
            this.IsPercentageOfItem = obj[9] != null ? (bool)obj[9] : false;
            this.IsPercentageOfSumInsured = obj[10] != null ? (bool)obj[10] : false;
            this.ShouldApplySelectedExcess = obj[11] != null ? (bool) obj[11] : false;
            this.ShouldDisplayExcessValues = obj[12] != null ? (bool) obj[12] : false;
        }

        public virtual int ProductAdditionalExcessId { get; set; }
        public virtual int Index { get; set; }
        public virtual string Category { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal ActualExcess { get; set; }
        public virtual decimal MinExcess { get; set; }
        public virtual decimal MaxExcess { get; set; }
        public virtual decimal Percentage { get; set; }

        public virtual bool IsPercentageOfClaim { get; set; }
        public virtual bool IsPercentageOfItem { get; set; }
        public virtual bool IsPercentageOfSumInsured { get; set; }

        public virtual bool ShouldApplySelectedExcess { get; set; }
        public virtual bool ShouldDisplayExcessValues { get; set; }
    }
}
