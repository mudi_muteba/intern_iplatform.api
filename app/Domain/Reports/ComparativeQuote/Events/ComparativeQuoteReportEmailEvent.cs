﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Reports.ComparativeQuote.Events
{
    public class ComparativeQuoteReportEmailEvent : BaseDomainEvent
    {
        public string UserName { get; set; }

        public ComparativeQuoteReportEmailEvent(string userName, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            UserName = userName;
        }
    }
}