﻿using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportInsurerCoverBenefit : Entity
    {
        public ComparativeQuoteReportInsurerCoverBenefit() { }

        public ComparativeQuoteReportInsurerCoverBenefit(object[] obj)
        {
            this.Description = "";
            this.FirstSelectedInsurerBenefitValue = "0.00";
            this.SecondSelectedInsurerBenefitValue = "0.00";
            this.ThirdSelectedInsurerBenefitValue = "0.00";

            if (obj.Length > 0)
            {
                this.Description = obj[0] != null ? (string)obj[0] : string.Empty;
                this.FirstSelectedInsurerBenefitValue = obj[1] != null ? (string)obj[1] : string.Empty;

                if (obj.Length > 2)
                    this.SecondSelectedInsurerBenefitValue = obj[2] != null ? (string)obj[2] : string.Empty;

                if (obj.Length > 3)
                    this.ThirdSelectedInsurerBenefitValue = obj[3] != null ? (string)obj[3] : string.Empty;
            }
        }

        public virtual string Description { get; set; }
        public virtual string FirstSelectedInsurerBenefitValue { get; set; }
        public virtual string SecondSelectedInsurerBenefitValue { get; set; }
        public virtual string ThirdSelectedInsurerBenefitValue { get; set; }
    }
}
