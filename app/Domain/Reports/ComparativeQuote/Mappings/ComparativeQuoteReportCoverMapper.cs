﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;

namespace Domain.Reports.ComparativeQuote.Mappings
{
    public class ComparativeQuoteReportCoverMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComparativeQuoteReportCover, ComparativeQuoteReportCoverDto>();

            Mapper.CreateMap<QueryResult<IList<ComparativeQuoteReportCover>>, POSTResponseDto<IList<ComparativeQuoteReportCoverDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}