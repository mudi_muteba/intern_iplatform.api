﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;

namespace Domain.Reports.ComparativeQuote.Mappings
{
    public class ComparativeQuoteReportProductAddendumMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComparativeQuoteReportProductAddendum, ComparativeQuoteReportProductAddendumDto>();

            Mapper.CreateMap<QueryResult<IList<ComparativeQuoteReportProductAddendum>>, POSTResponseDto<IList<ComparativeQuoteReportProductAddendumDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}