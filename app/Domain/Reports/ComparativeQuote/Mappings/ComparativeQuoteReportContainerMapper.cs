﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;

namespace Domain.Reports.ComparativeQuote.Mappings
{
    public class ComparativeQuoteReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComparativeQuoteReportContainer, ComparativeQuoteReportContainerDto>();

            Mapper.CreateMap<QueryResult<ComparativeQuoteReportContainerDto>, POSTResponseDto<ComparativeQuoteReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<ComparativeQuoteReportContainer>>, POSTResponseDto<IList<ComparativeQuoteReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}