﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;

namespace Domain.Reports.ComparativeQuote.Mappings
{
    public class ComparativeQuoteReportProductAdditionalExcessCategoryMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComparativeQuoteReportProductAdditionalExcessCategory, ComparativeQuoteReportProductAdditionalExcessCategoryDto>();

            Mapper.CreateMap<QueryResult<IList<ComparativeQuoteReportProductAdditionalExcessCategory>>, POSTResponseDto<IList<ComparativeQuoteReportProductAdditionalExcessCategoryDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}