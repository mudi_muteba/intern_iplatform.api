﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;

namespace Domain.Reports.ComparativeQuote.Mappings
{
    public class ComparativeQuoteReportProductAdditionalExcessMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComparativeQuoteReportProductAdditionalExcess, ComparativeQuoteReportProductAdditionalExcessDto>();

            Mapper.CreateMap<QueryResult<IList<ComparativeQuoteReportProductAdditionalExcess>>, POSTResponseDto<IList<ComparativeQuoteReportProductAdditionalExcessDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}