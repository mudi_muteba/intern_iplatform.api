﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportProductAddendumOverride : IAutoMappingOverride<ComparativeQuoteReportProductAddendum>
    {
        public void Override(AutoMapping<ComparativeQuoteReportProductAddendum> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
