﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportCoverOverride : IAutoMappingOverride<ComparativeQuoteReportCover>
    {
        public void Override(AutoMapping<ComparativeQuoteReportCover> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
