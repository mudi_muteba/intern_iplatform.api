﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportProductAdditionalExcessOverride : IAutoMappingOverride<ComparativeQuoteReportProductAdditionalExcess>
    {
        public void Override(AutoMapping<ComparativeQuoteReportProductAdditionalExcess> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}