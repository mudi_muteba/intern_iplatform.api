﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportOrganizationOverride : IAutoMappingOverride<ComparativeQuoteReportOrganization>
    {
        public void Override(AutoMapping<ComparativeQuoteReportOrganization> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
