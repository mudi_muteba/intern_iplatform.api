﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportHeaderOverride : IAutoMappingOverride<ComparativeQuoteReportHeader>
    {
        public void Override(AutoMapping<ComparativeQuoteReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
