﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportSettingsOverride : IAutoMappingOverride<ComparativeQuoteReportSettings>
    {
        public void Override(AutoMapping<ComparativeQuoteReportSettings> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
