﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportSummaryOverride : IAutoMappingOverride<ComparativeQuoteReportSummary>
    {
        public void Override(AutoMapping<ComparativeQuoteReportSummary> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
