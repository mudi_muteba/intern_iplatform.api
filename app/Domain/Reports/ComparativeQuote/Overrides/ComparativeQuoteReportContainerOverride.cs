﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportContainerOverride : IAutoMappingOverride<ComparativeQuoteReportContainer>
    {
        public void Override(AutoMapping<ComparativeQuoteReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
