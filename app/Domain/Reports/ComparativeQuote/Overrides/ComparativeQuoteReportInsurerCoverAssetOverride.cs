﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportInsurerCoverAssetOverride : IAutoMappingOverride<ComparativeQuoteReportInsurerCoverAsset>
    {
        public void Override(AutoMapping<ComparativeQuoteReportInsurerCoverAsset> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
