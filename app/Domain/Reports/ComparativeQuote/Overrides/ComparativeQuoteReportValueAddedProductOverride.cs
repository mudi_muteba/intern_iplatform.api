﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportValueAddedProductOverride : IAutoMappingOverride<ComparativeQuoteReportValueAddedProductOverride>
    {
        public void Override(AutoMapping<ComparativeQuoteReportValueAddedProductOverride> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
