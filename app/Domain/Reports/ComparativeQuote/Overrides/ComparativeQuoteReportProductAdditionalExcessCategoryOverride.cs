﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportProductAdditionalExcessCategoryOverride : IAutoMappingOverride<ComparativeQuoteReportProductAdditionalExcessCategory>
    {
        public void Override(AutoMapping<ComparativeQuoteReportProductAdditionalExcessCategory> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}