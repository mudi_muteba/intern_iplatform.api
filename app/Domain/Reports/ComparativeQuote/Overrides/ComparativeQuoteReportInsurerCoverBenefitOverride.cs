﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportInsurerCoverBenefitOverride : IAutoMappingOverride<ComparativeQuoteReportInsurerCoverBenefit>
    {
        public void Override(AutoMapping<ComparativeQuoteReportInsurerCoverBenefit> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
