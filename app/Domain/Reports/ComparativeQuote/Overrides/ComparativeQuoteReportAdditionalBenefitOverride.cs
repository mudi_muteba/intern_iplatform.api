﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportAdditionalBenefitOverride : IAutoMappingOverride<ComparativeQuoteReportAdditionalBenefit>
    {
        public void Override(AutoMapping<ComparativeQuoteReportAdditionalBenefit> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
