﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

using AutoMapper;
using Newtonsoft.Json;

using Domain.Admin;
using Domain.Admin.Queries;
using Domain.Base.Handlers;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Emailing;
using Domain.Emailing.Factory;
using Domain.Individuals.Queries;
using Domain.Organizations.Queries;
using Domain.Reports.Base;
using Domain.Reports.Base.Handlers;
using Domain.Reports.Base.Queries;
using Domain.Reports.Base.StoredProcedures;
using Domain.SignFlow.Handlers;

using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.Base.Criteria;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;
using iPlatform.Api.DTOs.SignFlow;

using ToolBox.Templating.Interfaces;
using ToolBox.Communication.Interfaces;

using Workflow.Messages;
using MasterData;
using Domain.Admin.Channeltemplates.Queries;
using RestSharp.Contrib;

namespace Domain.Reports.ComparativeQuote.Handlers
{
    public class ComparativeQuoteReportEmailHandler : BaseDtoHandler<ComparativeQuoteReportEmailDto, ReportEmailResponseDto>
    {
        private readonly IRepository repository;
        private readonly ITemplateEngine templateEngine;
        private readonly IProvideContext contextProvider;
        private readonly IExecutionPlan executionPlan;

        private readonly GetIndividualById individualQuery;
        private readonly SearchOrganizationsQuery organizationQuery;
        private readonly GetChannelById channelQuery;
        private readonly GetAllChannelsQuery allChannelsQuery;
        private readonly GetAllReportsQuery reportQuery;
        private readonly GetReportParamsQuery reportParamsQuery;
        private readonly GetReportDocumentDefinitionsQuery reportDocumentDefinitionsQuery;
        private readonly GetAllChannelTemplatesQuery allChannelTemplatesQuery;

        private const string BaseUrl = @"iPlatform/serviceLocator/iPlatform/baseUrl";
        private const string TemplateBaseFolder = @"templates/baseFolder";
        private const string EnableInsurerTemplate = @"templates/useInsurerTemplate";
        private const string ReportSource = @"Reports/Source";
        private const string ReportStorage = @"Reports/Storage";

        public ComparativeQuoteReportEmailHandler(IProvideContext _contextProvider, IRepository _repository, ITemplateEngine _templateEngine, IExecutionPlan _executionPlan) : base(_contextProvider)
        {
            repository = _repository;
            templateEngine = _templateEngine;
            contextProvider = _contextProvider;
            executionPlan = _executionPlan;

            individualQuery = new GetIndividualById(contextProvider, repository);
            organizationQuery = new SearchOrganizationsQuery(contextProvider, repository);
            channelQuery = new GetChannelById(contextProvider, repository);
            allChannelsQuery = new GetAllChannelsQuery(contextProvider, repository);
            reportQuery = new GetAllReportsQuery(contextProvider, repository);
            reportParamsQuery = new GetReportParamsQuery(contextProvider, repository);
            reportDocumentDefinitionsQuery = new GetReportDocumentDefinitionsQuery(contextProvider, repository);
            allChannelTemplatesQuery = new GetAllChannelTemplatesQuery(contextProvider, repository);
        }

        public override List<MasterData.Authorisation.RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<MasterData.Authorisation.RequiredAuthorisationPoint> { };
            }
        }

        protected Channel GetChannel(ComparativeQuoteReportEmailDto dto)
        {
            var channel = new Channel();
            var usingDefaultChannel = false;

            var defaultChannel = allChannelsQuery
                .ExecuteQuery()
                .Where(x => x.IsDefault)
                .FirstOrDefault();

            if (dto.ChannelId > 0)
            {
                Log.Info(string.Format("ComparativeQuoteReportEmailHandler.InternalHandle() > Finding Channel with Id [{0}]...", dto.ChannelId));

                channel = channelQuery
                    .WithChannelId(dto.ChannelId)
                    .ExecuteQuery()
                    .FirstOrDefault();

                if (channel == null)
                {
                    Log.Info(string.Format("ComparativeQuoteReportEmailHandler.InternalHandle() > Unable to find Channel with Id [{0}], using default channel...", dto.ChannelId));

                    if (defaultChannel == null)
                    {
                        Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > No default Channel was found...");
                        return channel;
                    }
                    else
                        channel = defaultChannel;
                }
            }
            else
            {
                Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > No ChannelId was specified, using default Channel...");

                usingDefaultChannel = true;
                channel = defaultChannel;

                if (channel == null)
                {
                    Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > No default Channel was found...");
                    return channel;
                }
            }

            //E-Mail Communication Settings

            if (!usingDefaultChannel)
            {
                if (!channel.EmailCommunicationSettings.Any())
                {
                    Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > No Email Communication Settings were found for this channel, using default Channel's E-Mail Communication Settings...");

                    if (defaultChannel == null)
                    {
                        Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > No E-Mail Communication Settings were found because no default Channel was found...");
                        channel = null;
                    }
                    else
                        if (!defaultChannel.EmailCommunicationSettings.Any())
                        {
                            Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > The default Channel was found but does not have any E-Mail Communication Settings...");
                            channel = null;
                        }
                        else
                        {
                            channel.EmailCommunicationSettings = defaultChannel.EmailCommunicationSettings;
                        }
                }
            }
            else
            {
                if (!defaultChannel.EmailCommunicationSettings.Any())
                {
                    Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > The default Channel was found but does not contain any E-Mail Communication Settings...");
                    channel = null;
                }
                else
                {
                    channel.EmailCommunicationSettings = channel.EmailCommunicationSettings.ToList();
                }
            }

            return channel;
        }

        protected Channel GetDefaultChannel()
        {
            var defaultChannel = repository.GetAll<Channel>()
                .FirstOrDefault(x => x.Code == "IP");

            return defaultChannel;
        }

        protected void GenerateComparativeQuoteEmailTemplate(Channel channel, string path)
        {
            var insurerCode = channel.Code;

            var htmlTemplateType = new TemplateTypes().Where(y => y.Name == "HTML Template").FirstOrDefault().Id;
            var comparativeQuoteTemplateContextType = new TemplateContextTypes().Where(y => y.Name == "Comparative Quote Template").FirstOrDefault().Id;

            var comparativeQuoteEmailTemplate =
                allChannelTemplatesQuery
                    .ExecuteQuery().ToList()
                    .Where(
                        x =>
                            x.Channel.Id == channel.Id &&
                            x.TemplateType.Id == htmlTemplateType &&
                            x.TemplateContextType.Id == comparativeQuoteTemplateContextType
                    )
                    .ToList()
                    .FirstOrDefault();

            if (comparativeQuoteEmailTemplate == null)
            {
                Log.Info(string.Format("ComparativeQuoteReportEmailHandler.GenerateComparativeQuoteEmailTemplate() > Comparative quote e-mail template not found for Channel with Id [{0}], locating default channel comparative quote template...", channel.Id));

                channel = GetDefaultChannel();

                comparativeQuoteEmailTemplate =
                    allChannelTemplatesQuery
                    .ExecuteQuery().ToList()
                    .Where(
                        x =>
                            x.Channel.Id == channel.Id &&
                            x.TemplateType.Id == htmlTemplateType &&
                            x.TemplateContextType.Id == comparativeQuoteTemplateContextType
                    )
                    .ToList()
                    .FirstOrDefault();

                if (comparativeQuoteEmailTemplate == null)
                {
                    Log.Info(string.Format("ComparativeQuoteReportEmailHandler.GenerateComparativeQuoteEmailTemplate() > Comparative quote e-mail template not found for default Channel with Id [{0}], using default comparative quote template from existing source...", channel.Id));

                    var defaultComparativeQuoteEmailTemplatePath = Path.Combine(path, string.Format("EmailTemplate_ComparativeQuote_{0}.vm", "IP"));
                    var template = File.ReadAllBytes(defaultComparativeQuoteEmailTemplatePath);
                    var finalPath = Path.Combine(path, string.Format("EmailTemplate_ComparativeQuote_{0}.vm", insurerCode));

                    File.WriteAllBytes(finalPath, template);

                    return;
                }
            }

            if (!string.IsNullOrEmpty(comparativeQuoteEmailTemplate.Content))
            {
                var finalPath = Path.Combine(path, string.Format("EmailTemplate_ComparativeQuote_{0}.vm", insurerCode));
                File.WriteAllText(finalPath, HttpUtility.HtmlDecode(comparativeQuoteEmailTemplate.Content));
            }
        }

        protected override void InternalHandle(ComparativeQuoteReportEmailDto dto, HandlerResult<ReportEmailResponseDto> result)
        {
            Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > E-Mail Criteria:");
            Log.Info(JsonConvert.SerializeObject(dto, Formatting.Indented));

            var channel = GetChannel(dto);

            if(channel == null)
                Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > Exiting InternalHandle()...");

            var emailSettings = Mapper.Map<AppSettingsEmailSetting>(channel.EmailCommunicationSettings.FirstOrDefault());

            Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > E-Mail Settings:");
            Log.Info(JsonConvert.SerializeObject(emailSettings, Formatting.Indented));

            var individual = individualQuery
                .WithId(dto.PartyId)
                .ExecuteQuery()
                .FirstOrDefault();

            var reportLayoutQueryResult = executionPlan
                .QueryStoredProcedure<GetReportLayoutQuery, ReportLayout, ReportLayoutStoredProcedure, ReportLayoutDto>(new ReportLayoutStoredProcedure(new ReportLayoutCriteriaDto
                {
                    ChannelId = channel.Id,
                    ReportName = "Comparative Quote"
                }))
                .OnSuccess(Mapper.Map<IList<ReportLayout>, IList<ReportLayoutDto>>)
                .Execute();

            var reportLayout = Mapper.Map<IList<ReportLayoutDto>, ComparativeQuoteReportSettingsDto>(reportLayoutQueryResult.Response) ?? default(ComparativeQuoteReportSettingsDto);

            if (individual == null)
                return;

            var insurerCode = channel.Code;

            var baseUrl = ConfigurationManager.AppSettings[BaseUrl];
            var enableInsurerTemplate = ConfigurationManager.AppSettings[ReportStorage].ToString() != "false";
            var reportSource = ConfigurationManager.AppSettings[ReportSource].ToString();
            var reportLogoSource = Path.Combine(reportSource, "Logos");
            var reportStorage = ConfigurationManager.AppSettings[ReportStorage].ToString();
            var templateBaseFolder = ConfigurationManager.AppSettings[TemplateBaseFolder].ToString();
            var template = string.Format("EmailTemplate_ComparativeQuote_{0}", insurerCode);

            GenerateComparativeQuoteEmailTemplate(channel, Path.Combine(dto.PathMap, templateBaseFolder));

            Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > General Variables:");
            Log.Info(string.Format("baseUrl: {0}, insurerCode: {1}, source: {2}, storage: {3}, useInsurerTemplate: {4}, template: {5}", baseUrl, insurerCode, reportStorage, reportStorage, enableInsurerTemplate, template));

            var data = new Dictionary<string, string>
            {
                {"ClientFirstName", string.Format("{0} {1}", individual.FirstName, individual.Surname)},
                {"ChannelTradingName", reportLayout.CompanyHeaderValue },
                {"AgentFirstName", (dto.AgentName == null || dto.AgentName.Length == 0) ? "us" : dto.AgentName },
                {"AgentSurname", "" },
                {"ChannelContactNumber", emailSettings.DefaultContactNumber },
                {"ChannelQuoteExpirationPeriod", "30"},
                {"ChannelEmailAddress", reportLayout.CompanyContactEmailValue },
                {"ChannelPhysicalAddress", reportLayout.CompanyAddressPhysicalValue },
                {"ChannelWebsiteAddress", reportLayout.CompanyContactWebsiteValue },
                {"ChannelFspNumber", reportLayout.CompanyAuthLicenseValue },
                {"ChannelLogo", "footerLogo"}
            };

            Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > E-Mail Template Data:");
            Log.Info(JsonConvert.SerializeObject(data, Formatting.Indented));

            var uri = new Uri(dto.QuoteUri);
            var filename = Path.GetFileName(uri.LocalPath);

            Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > E-Mail File Uri:");
            Log.Info(string.Format("uri: {0}, filename: {1}", uri, filename));

            var communicationMessageData = new CommunicationMessageData(data);
            var emailCommunicationMessage = new EmailCommunicationMessage(template, communicationMessageData, dto.Email, "Your Comparative Quotation", templateEngine, emailSettings);

            if (emailSettings.DefaultBCC != null && emailSettings.DefaultBCC.Length > 0)
                emailCommunicationMessage.BCCAddresses = emailSettings.DefaultBCC;

            var overrideFrom = string.Empty;

            if (insurerCode.ToLower() != "uap")
                if (dto.AgentEMail != null && dto.AgentEMail.Length > 0)
                    overrideFrom = string.Format("{0} <{1}>", dto.AgentName, dto.AgentEMail);

            using (var mem = new MemoryStream())
            {
                var req = (HttpWebRequest)WebRequest.Create(dto.QuoteUri);
                var res = (HttpWebResponse)req.GetResponse();

                var stream = res.GetResponseStream();
                    stream.CopyTo(mem);

                byte[] bytes = mem.ToArray();

                res.Close();
                mem.Close();

                emailCommunicationMessage.Attachments.Add(
                    new Attachment(new MemoryStream(bytes), filename, MediaTypeNames.Application.Pdf)
                );

                if (enableInsurerTemplate)
                {
                    var logo = string.Format(@"{0}content\reports\logos\{1}.png", AppDomain.CurrentDomain.BaseDirectory, insurerCode);

                    var footerLogo = new EmbeddedResource();
                        footerLogo.Attribute = "footerLogo";
                        footerLogo.Value = @"<img src=""cid:{0}"" title=""Logo"" />";
                        footerLogo.LinkedResource = new LinkedResource(logo);

                    emailCommunicationMessage.EmbeddedResources.Add(footerLogo);
                }

                var message = new MailMessageFactory().Create(emailCommunicationMessage, emailSettings.AddCustomFrom(overrideFrom),false,null);

                new SmtpClientAdapter(emailSettings).Send(message);

                result.Processed(new ReportEmailResponseDto
                {
                    Id = individual.Id,
                    Success = true,
                    Body = message.Body
                });
            }

            //Send digtally signable documents to SignFlow if any specified
            if (dto.DocumentIds == null)
                return;

            if (dto.DocumentIds.Length == 0)
                return;

            var ids = dto.DocumentIds.Split(',').Select(Int32.Parse).ToList();
            var documents = reportQuery.ExecuteQuery().Where(x => ids.Contains(x.Id)).ToList();

            documents = documents.Select(x =>
            {
                var criteria = new ReportParamsCriteriaDto { ReportId = x.Id };
                x.Parameters = reportParamsQuery.ExecuteQuery<ReportParam, ReportParamsStoredProcedure>(new ReportParamsStoredProcedure(criteria)).ToList();

                return x;
            }).ToList();

            var parameters = new Dictionary<string, object>();
                parameters.Add("PartyId", individual.Id);
                parameters.Add("InsurerCode", insurerCode);

            var reportGeneratorDto = new ReportGeneratorDto
            {
                PathMap = AppDomain.CurrentDomain.BaseDirectory,
                Format = MasterData.ExportTypes.PDF,
                Reports = Mapper.Map<List<Report>, List<ReportDto>>(documents),
                Parameters = parameters
            };

            var reportGeneratorResponseDto = new HandlerResult<ReportGeneratorResponseDto>();

            var reportGeneratorHandler = new ReportGeneratorHandler(contextProvider, repository);
                reportGeneratorHandler.Handle(reportGeneratorDto, reportGeneratorResponseDto);

            if (reportGeneratorResponseDto.AllErrorDTOMessages.Any())
            {
                Log.Info("ComparativeQuoteReportEmailHandler.InternalHandle() > SignFlow Document Generator:");
                Log.Info(JsonConvert.SerializeObject(reportGeneratorResponseDto.AllErrorDTOMessages, Formatting.Indented));
            }

            var createMultipleSignFlowDocumentDto = new CreateMultipleSignFlowDocumentDto();
            var createMultipleSignFlowDocumentDtoResult = new HandlerResult<string>();

            var index = -1;

            foreach (var file in reportGeneratorResponseDto.Response.Reports)
            {
                index++;

                var name = new Uri(file).Segments.Last();
                var path = Path.Combine(reportGeneratorDto.PathMap, reportStorage, name);

                var criteria = new ReportDocumentDefinitionCriteriaDto { ReportId = reportGeneratorDto.Reports[index].Id };

                var documentDefinitions =
                        reportDocumentDefinitionsQuery
                            .ExecuteQuery<ReportDocumentDefinition, ReportDocumentDefinitionStoredProcedure>(new ReportDocumentDefinitionStoredProcedure(criteria))
                            .ToList();

                var client = new CorrespondantUser { Email = dto.Email };

                foreach(var documentDefinition in documentDefinitions)
                    client.DocDefinitions.Add(new MasterData.DocumentDefinitions().Where(x => x.Id == documentDefinition.DocumentDefinition.Id).First());

                var createSignFlowDocumentDto = new CreateSignFlowDocumentDto();
                    createSignFlowDocumentDto.CorrespondantUser.Add(client);
                    createSignFlowDocumentDto.PathOfDocument = path;
                    createSignFlowDocumentDto.MessageToUser = "iPlatform/SignFlow Intergration Test";
                    createSignFlowDocumentDto.NameDocUploaded = name;
                    createSignFlowDocumentDto.DueDate = DateTime.Now.AddDays(2);
                    createSignFlowDocumentDto.DocumentType = MasterData.DocumentTypes.BrokerAppointment;

                createMultipleSignFlowDocumentDto.CreateSignFlowDocumentDtos.Add(createSignFlowDocumentDto);
            }

            var createMultipleSignFlowDocumentHandler = new CreateMultipleSignFlowDocumentHandler(contextProvider, repository, executionPlan);
                createMultipleSignFlowDocumentHandler.Handle(createMultipleSignFlowDocumentDto, createMultipleSignFlowDocumentDtoResult);

            if (createMultipleSignFlowDocumentDtoResult.Response != "true")
                throw new Exception(string.Format("Unable to create or send documents to SignFlow: {0}.", createMultipleSignFlowDocumentDtoResult.Response));
        }        
    }
}
