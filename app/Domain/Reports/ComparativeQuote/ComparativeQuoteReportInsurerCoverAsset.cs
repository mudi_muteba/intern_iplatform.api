﻿using Domain.Base;

namespace Domain.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportInsurerCoverAsset : Entity
    {
        public ComparativeQuoteReportInsurerCoverAsset() { }

        public ComparativeQuoteReportInsurerCoverAsset(object[] obj)
        {
            this.Asset = obj[0] != null ? (string)obj[0] : string.Empty;
            this.Value = obj[1] != null ? (decimal)obj[1] : 0;
        }

        public virtual string Asset { get; set; }
        public virtual decimal Value { get; set; }
    }
}
