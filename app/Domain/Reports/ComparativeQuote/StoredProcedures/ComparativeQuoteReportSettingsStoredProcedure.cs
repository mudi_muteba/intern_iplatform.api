﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.ComparativeQuote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace Domain.Reports.ComparativeQuote.StoredProcedures
{
    public class ComparativeQuoteReportSettingsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_Layout";
            }
        }

        public ComparativeQuoteReportSettingsStoredProcedure() { }

        public ComparativeQuoteReportSettingsStoredProcedure(ComparativeQuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new ComparativeQuoteReportSettingsStoredProcedureParameters
                {
                    ReportName = criteria.ReportName
                });
        }
    }
}