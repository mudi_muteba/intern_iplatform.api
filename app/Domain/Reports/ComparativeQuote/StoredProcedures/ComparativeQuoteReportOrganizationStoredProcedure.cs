﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.ComparativeQuote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace Domain.Reports.ComparativeQuote.StoredProcedures
{
    public class ComparativeQuoteReportOrganizationStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_DetailedComparativeQuote_Organizations";
            }
        }

        public ComparativeQuoteReportOrganizationStoredProcedure() { }

        public ComparativeQuoteReportOrganizationStoredProcedure(ComparativeQuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new ComparativeQuoteReportOrganizationStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    QuoteIds = criteria.QuoteIds,
                    CoverId = criteria.CoverId
                });
        }
    }
}