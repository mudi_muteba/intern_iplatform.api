﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.ComparativeQuote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace Domain.Reports.ComparativeQuote.StoredProcedures
{
    public class ComparativeQuoteReportInsurerCoverAdditionalExcessCategoriesStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_DetailedComparativeQuote_AdditionalExcess_Categories_ByCover";
            }
        }

        public ComparativeQuoteReportInsurerCoverAdditionalExcessCategoriesStoredProcedure() { }

        public ComparativeQuoteReportInsurerCoverAdditionalExcessCategoriesStoredProcedure(ComparativeQuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new ComparativeQuoteReportInsurerCoverAdditionalExcessCategoriesStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    CoverDefinitionId = criteria.CoverDefinitionId
                });
        }
    }
}