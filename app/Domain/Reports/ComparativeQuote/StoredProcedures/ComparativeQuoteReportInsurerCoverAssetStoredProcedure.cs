﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.ComparativeQuote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace Domain.Reports.ComparativeQuote.StoredProcedures
{
    public class ComparativeQuoteReportInsurerCoverAssetStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_DetailedComparativeQuote_Assets_ByCover";
            }
        }

        public ComparativeQuoteReportInsurerCoverAssetStoredProcedure() { }

        public ComparativeQuoteReportInsurerCoverAssetStoredProcedure(ComparativeQuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new ComparativeQuoteReportInsurerCoverAssetStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    QuoteIds = criteria.QuoteIds,
                    CoverId = criteria.CoverId
                });
        }
    }
}