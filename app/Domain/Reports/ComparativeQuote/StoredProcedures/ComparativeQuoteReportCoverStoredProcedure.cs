﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.ComparativeQuote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace Domain.Reports.ComparativeQuote.StoredProcedures
{
    public class ComparativeQuoteReportCoverStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_DetailedComparativeQuote_Covers";
            }
        }

        public ComparativeQuoteReportCoverStoredProcedure() { }

        public ComparativeQuoteReportCoverStoredProcedure(ComparativeQuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new ComparativeQuoteReportCoverStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    QuoteIds = criteria.QuoteIds
                });
        }
    }
}