﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.ComparativeQuote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace Domain.Reports.ComparativeQuote.StoredProcedures
{
    public class ComparativeQuoteReportInsurerCoverAdditionalExcessStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_DetailedComparativeQuote_AdditionalExcess_ByCover";
            }
        }

        public ComparativeQuoteReportInsurerCoverAdditionalExcessStoredProcedure() { }

        public ComparativeQuoteReportInsurerCoverAdditionalExcessStoredProcedure(ComparativeQuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new ComparativeQuoteReportInsurerCoverAdditionalExcessStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    CoverDefinitionId = criteria.CoverDefinitionId
                });
        }
    }
}