﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.ComparativeQuote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace Domain.Reports.ComparativeQuote.StoredProcedures
{
    public class ComparativeQuoteReportSummaryStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_DetailedComparativeQuote_Summary";
            }
        }

        public ComparativeQuoteReportSummaryStoredProcedure() { }

        public ComparativeQuoteReportSummaryStoredProcedure(ComparativeQuoteReportCriteriaDto criteria) : base ()
        {
            base.
                SetParameters(new ComparativeQuoteReportSummaryStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    QuoteIds = criteria.QuoteIds
                });
        }
    }
}