﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.ComparativeQuote.StoredProcedures.Parameters
{
    public class ComparativeQuoteReportCoverStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ProposalHeaderId { get; set; }
        public string QuoteIds { get; set; }
    }
}