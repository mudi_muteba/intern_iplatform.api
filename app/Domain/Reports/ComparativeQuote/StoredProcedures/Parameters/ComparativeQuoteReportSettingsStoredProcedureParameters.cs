﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.ComparativeQuote.StoredProcedures.Parameters
{
    public class ComparativeQuoteReportSettingsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string ReportName { get; set; }
    }
}