﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.ComparativeQuote.StoredProcedures.Parameters
{
    public class ComparativeQuoteReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ProposalHeaderId { get; set; }
        public string QuoteIds { get; set; }
        public string Code { get; set; }
    }
}