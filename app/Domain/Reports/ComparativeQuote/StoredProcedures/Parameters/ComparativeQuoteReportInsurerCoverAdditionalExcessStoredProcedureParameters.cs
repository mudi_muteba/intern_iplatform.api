﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.ComparativeQuote.StoredProcedures.Parameters
{
    public class ComparativeQuoteReportInsurerCoverAdditionalExcessStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ProposalHeaderId { get; set; }
        public int CoverDefinitionId { get; set; }
    }
}