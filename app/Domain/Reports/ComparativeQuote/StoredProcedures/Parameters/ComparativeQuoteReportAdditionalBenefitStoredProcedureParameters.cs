﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.ComparativeQuote.StoredProcedures.Parameters
{
    public class ComparativeQuoteReportAdditionalBenefitStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ProposalHeaderId { get; set; }
        public string QuestionIds { get; set; }
    }
}