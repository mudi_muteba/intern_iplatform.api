﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.ComparativeQuote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace Domain.Reports.ComparativeQuote.StoredProcedures
{
    public class ComparativeQuoteReportValueAddedProductStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_DetailedComparativeQuote_ValueAddedProduct";
            }
        }

        public ComparativeQuoteReportValueAddedProductStoredProcedure() { }

        public ComparativeQuoteReportValueAddedProductStoredProcedure(ComparativeQuoteReportCriteriaDto criteria) : base ()
        {
            base.
                SetParameters(new ComparativeQuoteReportValueAddedProductStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    QuoteIds = criteria.QuoteIds
                });
        }
    }
}