﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Reports.Quote;

namespace Domain.Reports.Quote.Queries.Filters
{
    public class DefaultQuoteReportTotalFilters : IApplyDefaultFilters<QuoteReportTotals>
    {
        public IQueryable<QuoteReportTotals> Apply(ExecutionContext executionContext, IQueryable<QuoteReportTotals> query)
        {
            return query;
        }
    }
}