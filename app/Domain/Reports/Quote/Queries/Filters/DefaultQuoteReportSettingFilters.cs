﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Reports.Quote;

namespace Domain.Reports.Quote.Queries.Filters
{
    public class DefaultQuoteReportSettingFilters : IApplyDefaultFilters<QuoteReportSettings>
    {
        public IQueryable<QuoteReportSettings> Apply(ExecutionContext executionContext, IQueryable<QuoteReportSettings> query)
        {
            return query;
        }
    }
}