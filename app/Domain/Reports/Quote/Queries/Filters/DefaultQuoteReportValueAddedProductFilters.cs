﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.Quote.Queries.Filters
{
    public class DefaultQuoteReportValueAddedProductFilters : IApplyDefaultFilters<QuoteReportValueAddedProduct>
    {
        public IQueryable<QuoteReportValueAddedProduct> Apply(ExecutionContext executionContext, IQueryable<QuoteReportValueAddedProduct> query)
        {
            return query;
        }
    }
}