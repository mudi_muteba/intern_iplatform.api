﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Reports.Quote;

namespace Domain.Reports.Quote.Queries.Filters
{
    public class DefaultQuoteReportAssetFilters : IApplyDefaultFilters<QuoteReportAsset>
    {
        public IQueryable<QuoteReportAsset> Apply(ExecutionContext executionContext, IQueryable<QuoteReportAsset> query)
        {
            return query;
        }
    }
}