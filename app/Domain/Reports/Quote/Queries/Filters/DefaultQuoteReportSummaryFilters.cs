﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Reports.Quote;

namespace Domain.Reports.Quote.Queries.Filters
{
    public class DefaultQuoteReportSummaryFilters : IApplyDefaultFilters<QuoteReportSummary>
    {
        public IQueryable<QuoteReportSummary> Apply(ExecutionContext executionContext, IQueryable<QuoteReportSummary> query)
        {
            return query;
        }
    }
}