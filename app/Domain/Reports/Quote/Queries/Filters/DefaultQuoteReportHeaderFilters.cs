﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Reports.Quote;

namespace Domain.Reports.Quote.Queries.Filters
{
    public class DefaultQuoteReportHeaderFilters : IApplyDefaultFilters<QuoteReportHeader>
    {
        public IQueryable<QuoteReportHeader> Apply(ExecutionContext executionContext, IQueryable<QuoteReportHeader> query)
        {
            return query;
        }
    }
}