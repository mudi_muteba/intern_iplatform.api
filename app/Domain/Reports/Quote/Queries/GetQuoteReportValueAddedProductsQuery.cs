﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.Quote.Queries.Filters;
using Domain.Reports.Quote.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.Quote.Queries
{
    public class GetQuoteReportValueAddedProductsQuery : BaseStoredProcedureQuery<QuoteReportValueAddedProduct, QuoteReportValueAddedProductsStoredProcedure>
    {
        public GetQuoteReportValueAddedProductsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultQuoteReportValueAddedProductFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<QuoteReportValueAddedProduct> Execute(IList<QuoteReportValueAddedProduct> query)
        {
            return query;
        }
    }
}
