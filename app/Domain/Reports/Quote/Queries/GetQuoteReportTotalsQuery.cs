﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.Quote.Queries.Filters;
using Domain.Reports.Quote.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.Quote.Queries
{
    public class GetQuoteReportTotalsQuery : BaseStoredProcedureQuery<QuoteReportTotals, QuoteReportTotalsStoredProcedure>
    {
        public GetQuoteReportTotalsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultQuoteReportTotalFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<QuoteReportTotals> Execute(IList<QuoteReportTotals> query)
        {
            return query;
        }
    }
}
