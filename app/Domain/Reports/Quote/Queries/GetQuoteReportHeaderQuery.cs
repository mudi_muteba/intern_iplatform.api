﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.Quote.Queries.Filters;
using Domain.Reports.Quote.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.Quote.Queries
{
    public class GetQuoteReportHeaderQuery : BaseStoredProcedureQuery<QuoteReportHeader, QuoteReportHeaderStoredProcedure>
    {
        public GetQuoteReportHeaderQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultQuoteReportHeaderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<QuoteReportHeader> Execute(IList<QuoteReportHeader> query)
        {
            return query;
        }
    }
}
