﻿using Domain.Base;

namespace Domain.Reports.Quote
{
    public class QuoteReportContainer : Entity
    {
        public QuoteReportContainer() {}

        public QuoteReportContainer(QuoteReportSettings _settings, QuoteReportHeader _header)
        {
            Settings = _settings;
            Header = _header;
        }

        public virtual QuoteReportSettings Settings { get; set; }
        public virtual QuoteReportHeader Header { get; set; }
    }
}
