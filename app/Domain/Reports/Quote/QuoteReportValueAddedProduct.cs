﻿using Domain.Base;

namespace Domain.Reports.Quote
{
    public class QuoteReportValueAddedProduct : Entity
    {
        public QuoteReportValueAddedProduct() { }

        public QuoteReportValueAddedProduct(object[] obj)
        {
            this.Cover = obj[0] != null ? (string)obj[0] : "";
            this.Asset = obj[1] != null ? (string)obj[1] : "";
            this.Product = obj[2] != null ? (string)obj[2] : "";
            this.Definition = obj[3] != null ? (string)obj[3] : "";
        }

        public virtual string Cover { get; set; }
        public virtual string Asset { get; set; }
        public virtual string Product { get; set; }
        public virtual string Definition { get; set; }
    }
}
