﻿using Domain.Base;

namespace Domain.Reports.Quote
{
    public class QuoteReportSummary : Entity
    {
        public QuoteReportSummary() { }

        public QuoteReportSummary(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string)obj[0] : "";
            this.Product = obj[1] != null ? (string)obj[1] : "";
            this.BasicExcess = obj[2] != null ? (decimal)obj[2] : 0;
            this.Premium = obj[3] != null ? (decimal)obj[3] : 0;
            this.SASRIA = obj[4] != null ? (decimal)obj[4] : 0;
            this.Fees = obj[5] != null ? (decimal)obj[5] : 0;
            this.Total = obj[6] != null ? (decimal)obj[6] : 0;
        }

        public virtual string Insurer { get; set; }
        public virtual string Product { get; set; }
        public virtual decimal BasicExcess { get; set; }
        public virtual decimal Premium { get; set; }
        public virtual decimal SASRIA { get; set; }
        public virtual decimal Fees { get; set; }
        public virtual decimal Total { get; set; }
    }
}
