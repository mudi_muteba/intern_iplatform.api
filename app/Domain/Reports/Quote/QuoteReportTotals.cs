﻿using Domain.Base;

namespace Domain.Reports.Quote
{
    public class QuoteReportTotals : Entity
    {
        public QuoteReportTotals() { }

        public QuoteReportTotals(object[] obj)
        {
            this.Fees = obj[0] != null ? (decimal)obj[0] : 0;
            this.SASRIA = obj[1] != null ? (decimal)obj[1] : 0;
            this.Total = obj[2] != null ? (decimal)obj[2] : 0;
        }

        public virtual decimal SASRIA { get; set; }
        public virtual decimal Fees { get; set; }
        public virtual decimal Total { get; set; }
    }
}
