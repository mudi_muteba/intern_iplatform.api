﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;
using Domain.Reports.Quote;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.Quote;

namespace Domain.Reports.Quotes.Mappings
{
    public class QuoteReportTotalsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<QuoteReportTotals, QuoteReportTotalsDto>();

            Mapper.CreateMap<QueryResult<IList<QuoteReportTotalsDto>>, POSTResponseDto<IList<QuoteReportTotalsDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}