﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;
using Domain.Reports.Quote;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.Quote;

namespace Domain.Reports.Quotes.Mappings
{
    public class QuoteReportHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<QuoteReportHeader, QuoteReportHeaderDto>();

            Mapper.CreateMap<QueryResult<IList<QuoteReportHeaderDto>>, POSTResponseDto<IList<QuoteReportHeaderDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}