﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.Quote;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Reports.Quotes.Mappings
{
    public class QuoteReportSettingsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<IList<ReportLayoutDto>, QuoteReportSettingsDto>()
                .ConstructUsing(src => new QuoteReportSettingsDto
                {
                    QuoteCode = "" ?? "iplatform",

                    QuoteTitleLabel = src.Where(x => x.Name == "QuoteTitle").First().Label,
                    QuoteSummaryLabel = src.Where(x => x.Name == "QuoteSummary").First().Label,
                    QuoteAssetsLabel = src.Where(x => x.Name == "QuoteAssets").First().Label,
                    QuoateDateLabel = src.Where(x => x.Name == "QuoteDate").First().Label,
                    QuoteExpiresLabel = src.Where(x => x.Name == "QuoteExpires").First().Label,
                    QuoteNumberLabel = src.Where(x => x.Name == "QuoteNumber").First().Label,

                    ClientHeaderLabel = src.Where(x => x.Name == "ClientHeader").First().Label,
                    ClientGeneralNameLabel = src.Where(x => x.Name == "ClientGeneralName").First().Label,
                    ClientGeneralIdentityLabel = src.Where(x => x.Name == "ClientGeneralIdentity").First().Label,
                    ClientAddressPhysicalLabel = src.Where(x => x.Name == "ClientAddressPhysical").First().Label,
                    ClientAddressPostalLabel = src.Where(x => x.Name == "ClientAddressPostal").First().Label,
                    ClientContactWorkLabel = src.Where(x => x.Name == "ClientContactWork").First().Label,
                    ClientContactHomeLabel = src.Where(x => x.Name == "ClientContactHome").First().Label,
                    ClientContactCellLabel = src.Where(x => x.Name == "ClientContactCell").First().Label,
                    ClientContactFaxLabel = src.Where(x => x.Name == "ClientContactFax").First().Label,
                    ClientContactEmailLabel = src.Where(x => x.Name == "ClientContactEmail").First().Label,

                    CompanyHeaderLabel = src.Where(x => x.Name == "CompanyHeader").First().Label,
                    CompanyGeneralNameLabel = src.Where(x => x.Name == "CompanyGeneralName").First().Label,
                    CompanyAddressPhysicalLabel = src.Where(x => x.Name == "CompanyAddressPhysical").First().Label,
                    CompanyAddressPostalLabel = src.Where(x => x.Name == "CompanyAddressPostal").First().Label,
                    CompanyContactWorkLabel = src.Where(x => x.Name == "CompanyContactWork").First().Label,
                    CompanyContactFaxLabel = src.Where(x => x.Name == "CompanyContactFax").First().Label,
                    CompanyContactEmailLabel = src.Where(x => x.Name == "CompanyContactEmail").First().Label,
                    CompanyContactWebsiteLabel = src.Where(x => x.Name == "CompanyContactWebsite").First().Label,
                    CompanyAuthRegistrationLabel = src.Where(x => x.Name == "CompanyAuthRegistration").First().Label,
                    CompanyAuthTaxLabel = src.Where(x => x.Name == "CompanyAuthTax").First().Label,
                    CompanyAuthLicenseLabel = src.Where(x => x.Name == "CompanyAuthLicense").First().Label,

                    CompanyHeaderValue = src.Where(x => x.Name == "CompanyHeader").First().Value,
                    CompanyGeneralNameValue = src.Where(x => x.Name == "CompanyGeneralName").First().Value,
                    CompanyAddressPhysicalValue = src.Where(x => x.Name == "CompanyAddressPhysical").First().Value,
                    CompanyAddressPostalValue = src.Where(x => x.Name == "CompanyAddressPostal").First().Value,
                    CompanyContactWorkValue = src.Where(x => x.Name == "CompanyContactWork").First().Value,
                    CompanyContactFaxValue = src.Where(x => x.Name == "CompanyContactFax").First().Value,
                    CompanyContactEmailValue = src.Where(x => x.Name == "CompanyContactEmail").First().Value,
                    CompanyContactWebsiteValue = src.Where(x => x.Name == "CompanyContactWebsite").First().Value,
                    CompanyAuthRegistrationValue = src.Where(x => x.Name == "CompanyAuthRegistration").First().Value,
                    CompanyAuthTaxValue = src.Where(x => x.Name == "CompanyAuthTax").First().Value,
                    CompanyAuthLicenseValue = src.Where(x => x.Name == "CompanyAuthLicense").First().Value,

                    QuoteTitleVisibility = src.Where(x => x.Name == "QuoteTitle").First().IsVisible,
                    QuoteSummaryVisibility = src.Where(x => x.Name == "QuoteSummary").First().IsVisible,
                    QuoteAssetsVisibility = src.Where(x => x.Name == "QuoteAssets").First().IsVisible,
                    QuoateDateVisibility = src.Where(x => x.Name == "QuoteDate").First().IsVisible,
                    QuoteExpiresVisibility = src.Where(x => x.Name == "QuoteExpires").First().IsVisible,
                    QuoteNumberVisibility = src.Where(x => x.Name == "QuoteNumber").First().IsVisible,

                    ClientHeaderVisibility = src.Where(x => x.Name == "ClientHeader").First().IsVisible,
                    ClientGeneralNameVisibility = src.Where(x => x.Name == "ClientGeneralName").First().IsVisible,
                    ClientGeneralIdentityVisibility = src.Where(x => x.Name == "ClientGeneralIdentity").First().IsVisible,
                    ClientAddressPhysicalVisibility = src.Where(x => x.Name == "ClientAddressPhysical").First().IsVisible,
                    ClientAddressPostalVisibility = src.Where(x => x.Name == "ClientAddressPostal").First().IsVisible,
                    ClientContactWorkVisibility = src.Where(x => x.Name == "ClientContactWork").First().IsVisible,
                    ClientContactHomeVisibility = src.Where(x => x.Name == "ClientContactHome").First().IsVisible,
                    ClientContactCellVisibility = src.Where(x => x.Name == "ClientContactCell").First().IsVisible,
                    ClientContactFaxVisibility = src.Where(x => x.Name == "ClientContactFax").First().IsVisible,
                    ClientContactEmailVisibility = src.Where(x => x.Name == "ClientContactEmail").First().IsVisible,

                    CompanyHeaderVisibility = src.Where(x => x.Name == "CompanyHeader").First().IsVisible,
                    CompanyGeneralNameVisibility = src.Where(x => x.Name == "CompanyGeneralName").First().IsVisible,
                    CompanyAddressPhysicalVisibility = src.Where(x => x.Name == "CompanyAddressPhysical").First().IsVisible,
                    CompanyAddressPostalVisibility = src.Where(x => x.Name == "CompanyAddressPostal").First().IsVisible,
                    CompanyContactWorkVisibility = src.Where(x => x.Name == "CompanyContactWork").First().IsVisible,
                    CompanyContactFaxVisibility = src.Where(x => x.Name == "CompanyContactFax").First().IsVisible,
                    CompanyContactEmailVisibility = src.Where(x => x.Name == "CompanyContactEmail").First().IsVisible,
                    CompanyContactWebsiteVisibility = src.Where(x => x.Name == "CompanyContactWebsite").First().IsVisible,
                    CompanyAuthRegistrationVisibility = src.Where(x => x.Name == "CompanyAuthRegistration").First().IsVisible,
                    CompanyAuthTaxVisibility = src.Where(x => x.Name == "CompanyAuthTax").First().IsVisible,
                    CompanyAuthLicenseVisibility = src.Where(x => x.Name == "CompanyAuthLicense").First().IsVisible,

                    QuoteFooter = src.Where(x => x.Name == "QuoteFooter").First().Value,

                    QuoteSummaryFeesVisibility = src.Where(x => x.Name == "QuoteSummaryFees").First().IsVisible,
                    QuoteSummaryTotalVisibility = src.Where(x => x.Name == "QuoteSummaryTotal").First().IsVisible,

                    CostFooterVisiblity = src.Where(x => x.Name == "CostFooter").First().IsVisible,
                });

            Mapper.CreateMap<QueryResult<QuoteReportSettingsDto>, POSTResponseDto<QuoteReportSettingsDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}