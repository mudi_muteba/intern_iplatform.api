﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.Quote.StoredProcedures.Parameters
{
    public class QuoteReportSummaryStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ProposalHeaderId { get; set; }
        public string QuoteIds { get; set; }
    }
}