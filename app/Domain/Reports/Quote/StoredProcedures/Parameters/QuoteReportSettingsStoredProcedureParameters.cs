﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.Quote.StoredProcedures.Parameters
{
    public class QuoteReportSettingsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string ReportName { get; set; }
    }
}