﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.Quote.StoredProcedures.Parameters
{
    public class QuoteReportValueAddedProductsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ProposalHeaderId { get; set; }
    }
}