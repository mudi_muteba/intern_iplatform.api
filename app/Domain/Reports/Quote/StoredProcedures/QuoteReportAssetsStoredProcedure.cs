﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Quote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.Quote.Criteria;

namespace Domain.Reports.Quote.StoredProcedures
{
    public class QuoteReportAssetsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_ComparativeQuote_Assets";
            }
        }

        public QuoteReportAssetsStoredProcedure() { }

        public QuoteReportAssetsStoredProcedure(QuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new QuoteReportAssetsStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    QuoteIds = criteria.QuoteIds
                });
        }
    }
}