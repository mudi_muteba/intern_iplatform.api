﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Quote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.Quote.Criteria;

namespace Domain.Reports.Quote.StoredProcedures
{
    public class QuoteReportTotalsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_ComparativeQuote_Totals";
            }
        }

        public QuoteReportTotalsStoredProcedure() { }

        public QuoteReportTotalsStoredProcedure(QuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new QuoteReportTotalsStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    QuoteIds = criteria.QuoteIds
                });
        }
    }
}