﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Quote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.Quote.Criteria;

namespace Domain.Reports.Quote.StoredProcedures
{
    public class QuoteReportSettingsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_Layout";
            }
        }

        public QuoteReportSettingsStoredProcedure() { }

        public QuoteReportSettingsStoredProcedure(QuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new QuoteReportSettingsStoredProcedureParameters
                {
                    ReportName = criteria.ReportName
                });
        }
    }
}