﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Quote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.Quote.Criteria;

namespace Domain.Reports.Quote.StoredProcedures
{
    public class QuoteReportValueAddedProductsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_ComparativeQuote_ValueAddedProducts";
            }
        }

        public QuoteReportValueAddedProductsStoredProcedure() { }

        public QuoteReportValueAddedProductsStoredProcedure(QuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new QuoteReportValueAddedProductsStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId
                });
        }
    }
}