﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Quote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.Quote.Criteria;

namespace Domain.Reports.Quote.StoredProcedures
{
    public class QuoteReportSummaryStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_ComparativeQuote_Summary";
            }
        }

        public QuoteReportSummaryStoredProcedure() { }

        public QuoteReportSummaryStoredProcedure(QuoteReportCriteriaDto criteria) : base ()
        {
            base.
                SetParameters(new QuoteReportSummaryStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    QuoteIds = criteria.QuoteIds
                });
        }
    }
}