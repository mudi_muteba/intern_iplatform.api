﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Quote.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.Quote.Criteria;

namespace Domain.Reports.Quote.StoredProcedures
{
    public class QuoteReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_ComparativeQuote_Header";
            }
       }

        public QuoteReportHeaderStoredProcedure() { }

        public QuoteReportHeaderStoredProcedure(QuoteReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new QuoteReportHeaderStoredProcedureParameters
                {
                    ProposalHeaderId = criteria.ProposalHeaderId,
                    QuoteIds = criteria.QuoteIds,
                    Code = criteria.OrganizationCode
                });
        }
    }
}