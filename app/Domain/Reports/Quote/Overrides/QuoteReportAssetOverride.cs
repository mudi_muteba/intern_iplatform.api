﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Quote
{
    public class QuoteReportAssetOverride : IAutoMappingOverride<QuoteReportAsset>
    {
        public void Override(AutoMapping<QuoteReportAsset> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
