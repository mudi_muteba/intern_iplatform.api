﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Quote
{
    public class QuoteReportSettingsOverride : IAutoMappingOverride<QuoteReportSettings>
    {
        public void Override(AutoMapping<QuoteReportSettings> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
