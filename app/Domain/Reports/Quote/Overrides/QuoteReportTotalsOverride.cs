﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Quote
{
    public class QuoteReportTotalsOverride : IAutoMappingOverride<QuoteReportTotals>
    {
        public void Override(AutoMapping<QuoteReportTotals> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
