﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Quote
{
    public class QuoteReportHeaderOverride : IAutoMappingOverride<QuoteReportHeader>
    {
        public void Override(AutoMapping<QuoteReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
