﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Quote
{
    public class QuoteReportValueAddedProductOverride : IAutoMappingOverride<QuoteReportValueAddedProduct>
    {
        public void Override(AutoMapping<QuoteReportValueAddedProduct> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
