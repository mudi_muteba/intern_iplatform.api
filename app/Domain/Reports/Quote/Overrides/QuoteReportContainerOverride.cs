﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Quote
{
    public class QuoteReportContainerOverrride : IAutoMappingOverride<QuoteReportContainer>
    {
        public void Override(AutoMapping<QuoteReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
