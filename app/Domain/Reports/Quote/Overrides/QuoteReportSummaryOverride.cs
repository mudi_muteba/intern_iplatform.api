﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Quote
{
    public class QuoteReportSummaryOverride : IAutoMappingOverride<QuoteReportSummary>
    {
        public void Override(AutoMapping<QuoteReportSummary> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
