﻿using Domain.Base;

namespace Domain.Reports.Quote
{
    public class QuoteReportAsset : Entity
    {
        public QuoteReportAsset() { }

        public QuoteReportAsset(object[] obj)
        {
            this.AssetNo = obj[0] != null ? (int)obj[0] : 0;
            this.Asset = obj[1] != null ? (string)obj[1] : "";
            this.CoverType = obj[2] != null ? (string)obj[2] : "";
            this.SumInsured = obj[3] != null ? (decimal)obj[3] : 0;
        }

        public virtual int AssetNo { get; set; }
        public virtual string Asset { get; set; }
        public virtual string CoverType { get; set; }
        public virtual decimal SumInsured { get; set; }
    }
}
