﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.Base.StoredProcedures;
using Domain.Reports.Base.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.Base.Queries
{
    public class GetReportLayoutQuery : BaseStoredProcedureQuery<ReportLayout, ReportLayoutStoredProcedure>
    {
        public GetReportLayoutQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultReportLayoutFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ReportLayout> Execute(IList<ReportLayout> query)
        {
            return query;
        }
    }
}
