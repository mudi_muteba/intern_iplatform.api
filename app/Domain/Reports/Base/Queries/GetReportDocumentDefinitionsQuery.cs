﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.Base.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.Base.Queries
{
    public class GetReportDocumentDefinitionsQuery : BaseStoredProcedureQuery<ReportDocumentDefinition, ReportDocumentDefinitionStoredProcedure>
    {
        public GetReportDocumentDefinitionsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultReportDocumentDefinitionFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ReportDocumentDefinition> Execute(IList<ReportDocumentDefinition> query)
        {
            return query;
        }
    }
}
