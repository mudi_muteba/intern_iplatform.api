﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.Base.Queries.Filters
{
    public class DefaultReportListFilters : IApplyDefaultFilters<Report>
    {
        public IQueryable<Report> Apply(ExecutionContext executionContext, IQueryable<Report> query)
        {
            return query;
        }
    }
}