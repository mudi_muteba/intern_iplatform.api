﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.Base.Queries.Filters
{
    public class DefaultReportParamsFilters : IApplyDefaultFilters<ReportParam>
    {
        public IQueryable<ReportParam> Apply(ExecutionContext executionContext, IQueryable<ReportParam> query)
        {
            return query;
        }
    }
}