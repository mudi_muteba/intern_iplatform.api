﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.Base.Queries.Filters
{
    public class DefaultReportLayoutFilters : IApplyDefaultFilters<ReportLayout>
    {
        public IQueryable<ReportLayout> Apply(ExecutionContext executionContext, IQueryable<ReportLayout> query)
        {
            return query;
        }
    }
}