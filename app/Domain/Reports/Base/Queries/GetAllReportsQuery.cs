﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.Base.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.Base.Queries
{
    public class GetAllReportsQuery : BaseQuery<Report>
    {
        public GetAllReportsQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new DefaultReportFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<Report> Execute(IQueryable<Report> query)
        {
            return query;
        }
    }
}