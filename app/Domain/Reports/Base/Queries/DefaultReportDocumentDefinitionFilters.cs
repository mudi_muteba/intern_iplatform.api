﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.Base.Queries
{
    public class DefaultReportDocumentDefinitionFilters : IApplyDefaultFilters<ReportDocumentDefinition>
    {
        public IQueryable<ReportDocumentDefinition> Apply(ExecutionContext executionContext, IQueryable<ReportDocumentDefinition> query)
        {
            return query;
        }
    }
}