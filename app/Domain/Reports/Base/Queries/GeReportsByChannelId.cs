﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.Base.Queries.Filters;

using MasterData.Authorisation;

using iPlatform.Api.DTOs.Reports.Base.Criteria;

namespace Domain.Reports.Base.Queries
{
    public class GeReportsByChannelId : BaseQuery<Report>
    {
        private ReportCriteriaDto _criteria;

        public GeReportsByChannelId(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new DefaultReportFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(ReportCriteriaDto criteria)
        {
            this._criteria = criteria;
        }

        public GeReportsByChannelId WithChannelId(int channelId)
        {
            this._criteria = new ReportCriteriaDto { ChannelId = channelId };
            return this;
        }

        protected internal override IQueryable<Report> Execute(IQueryable<Report> query)
        {
            var result = query.Where(x => x.Channel.Id == _criteria.ChannelId);
            return result;
        }
    }
}