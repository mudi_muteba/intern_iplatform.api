﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.Base.StoredProcedures;
using Domain.Reports.Base.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.Base.Queries
{
    public class GetReportParamsQuery : BaseStoredProcedureQuery<ReportParam, ReportParamsStoredProcedure>
    {
        public GetReportParamsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultReportParamsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ReportParam> Execute(IList<ReportParam> query)
        {
            return query;
        }
    }
}
