﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Domain.Admin;
using Domain.Base;

using Infrastructure.NHibernate.Attributes;

using iPlatform.Api.DTOs.Reports.Base;

using MasterData;

namespace Domain.Reports.Base
{
    public class Report : Entity
    {
        public Report()
        {
            ReportParameters = new List<ReportParam>();
            ReportDocumentDefinitions = new List<ReportDocumentDefinition>();
            ReportLayouts = new List<ReportLayout>();
        }

        public Report(object[] obj)
        {
            Id = obj[0] != null ? int.Parse(obj[0].ToString()) : 0;
            ReportCategory = obj[1] != null ? new ReportCategories().FirstOrDefault(x => x.Id == int.Parse(obj[1].ToString())) : new ReportCategories().FirstOrDefault();
            ReportType = obj[2] != null ? new ReportTypes().FirstOrDefault(x => x.Id == int.Parse(obj[2].ToString())) : new ReportTypes().FirstOrDefault();
            ExportType = obj[3] != null ? new ExportTypes().FirstOrDefault(x => x.Id == int.Parse(obj[3].ToString())) : new ExportTypes().FirstOrDefault();
            Name = obj[4] != null ? obj[4].ToString() : "";
            Description = obj[5] != null ? obj[5].ToString() : "";
            SourceFile = obj[6] != null ? obj[6].ToString() : "";
            IsVisibleInReportViewer = obj[7] != null ? bool.Parse(obj[7].ToString()) : false;
            IsVisibleInScheduler = obj[8] != null ? bool.Parse(obj[8].ToString()) : false;
            IsActive = obj[9] != null ? bool.Parse(obj[9].ToString()) : false;
            VisibleIndex = obj[10] != null ? int.Parse(obj[10].ToString()) : 0;
            Icon = obj[11] != null ? new FontAwesomeIcons().FirstOrDefault(x => x.Id == int.Parse(obj[11].ToString())) : new FontAwesomeIcons().FirstOrDefault();
            Info = obj[12] != null ? obj[12].ToString() : "";
        }

        public virtual Channel Channel { get; set; }

        public virtual IList<ReportParam> ReportParameters { get; protected internal set; }
        public virtual IList<ReportDocumentDefinition> ReportDocumentDefinitions { get; protected internal set; }
        public virtual IList<ReportLayout> ReportLayouts { get; protected internal set; }

        public virtual ReportCategory ReportCategory { get; set; }
        public virtual ReportType ReportType { get; set; }
        public virtual ExportType ExportType { get; set; }

        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string SourceFile { get; set; }

        public virtual string Info { get; set; }
        public virtual string Content { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string Extension { get; set; }

        public virtual bool IsVisibleInReportViewer { get; set; }
        public virtual bool IsVisibleInScheduler { get; set; }
        public virtual bool IsActive { get; set; }

        public virtual int VisibleIndex { get; set; }
        public virtual FontAwesomeIcon Icon { get; set; }

        private IList<ReportParam> _parameters;
        public virtual IList<ReportParam> Parameters
        {
            get { return _parameters ?? (_parameters = new List<ReportParam>()); }
            set { _parameters = value; }
        }

        [DoNotMap]
        public virtual ReportParam this[string paramName]
        {
            get { return Parameters.FirstOrDefault(x => (x.Field + "").Trim().ToLower() == (paramName + "").Trim().ToLower()); }
        }

        public static Report Create(CreateReportDto dto)
        {
            return Mapper.Map<Report>(dto);
        }
    }
}
