﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Base.Overrides
{
    public class ReportDocumentDefinitionOverride : IAutoMappingOverride<ReportDocumentDefinition>
    {
        public void Override(AutoMapping<ReportDocumentDefinition> mapping)
        {
            mapping.References(x => x.Report).Cascade.SaveUpdate();
        }
    }
}