﻿using Domain.ReportSchedules;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Base.Overrides
{
    public class ReportScheduleOverride : IAutoMappingOverride<ReportSchedule>
    {
        public void Override(AutoMapping<ReportSchedule> mapping)
        {
            mapping.Map(x => x.WeekDay).Length(3);
            mapping.Map(x => x.MonthDay).CustomSqlType("tinyint");
            mapping.HasMany(c => c.Recipients).Cascade.SaveUpdate();
            mapping.HasMany(c => c.Campaigns).Cascade.SaveUpdate();
            mapping.HasMany(c => c.Reports).Cascade.SaveUpdate();
            mapping.HasMany(x => x.ReportScheduleParams).Cascade.SaveUpdate();
            mapping.References(x => x.Owner, "OwnerId");
        }
    }
}