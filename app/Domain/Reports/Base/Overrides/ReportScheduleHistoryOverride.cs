﻿using Domain.ReportSchedules;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Base.Overrides
{
    public class ReportScheduleHistoryOverride : IAutoMappingOverride<ReportScheduleHistory>
    {
        public void Override(AutoMapping<ReportScheduleHistory> mapping)
        {
        }
    }
}