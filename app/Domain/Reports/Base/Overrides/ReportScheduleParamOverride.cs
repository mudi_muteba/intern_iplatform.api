﻿using Domain.ReportSchedules;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Base.Overrides
{
    public class ReportScheduleParamOverride : IAutoMappingOverride<ReportScheduleParam>
    {
        public void Override(AutoMapping<ReportScheduleParam> mapping)
        {
            
        }
    }
}