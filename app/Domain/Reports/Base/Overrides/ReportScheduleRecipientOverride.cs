﻿using Domain.ReportSchedules;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Base.Overrides
{
    public class ReportScheduleRecipientOverride : IAutoMappingOverride<ReportScheduleRecipient>
    {
        public void Override(AutoMapping<ReportScheduleRecipient> mapping)
        {
            mapping.References(x => x.Recipient, "RecipientId");
        }
    }
}