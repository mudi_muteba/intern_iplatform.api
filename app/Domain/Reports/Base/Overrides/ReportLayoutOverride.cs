﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Base.Overrides
{
    public class ReportLayoutOverride : IAutoMappingOverride<ReportLayout>
    {
        public void Override(AutoMapping<ReportLayout> mapping)
        {
            mapping.Map(x => x.Value).CustomType("StringClob").CustomSqlType("text").Nullable();

            mapping.References(x => x.Report).Cascade.SaveUpdate();
        }
    }
}