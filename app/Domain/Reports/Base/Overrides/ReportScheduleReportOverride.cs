﻿using Domain.ReportSchedules;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Base.Overrides
{
    public class ReportScheduleReportOverride : IAutoMappingOverride<ReportScheduleReport>
    {
        public void Override(AutoMapping<ReportScheduleReport> mapping)
        {
            mapping.References(x => x.Report).Cascade.None();
            mapping.References(x => x.ReportSchedule).Cascade.None();
        }
    }
}