﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.Base.Overrides
{
    public class ReportParamOverride : IAutoMappingOverride<ReportParam>
    {
        public void Override(AutoMapping<ReportParam> mapping)
        {
            mapping.Map(x => x.DataType, "DataTypeId");
            mapping.References(x => x.Report).Cascade.SaveUpdate();
            mapping.References(x => x.Icon, "IconId");
        }
    }
}