﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Reports.Base.Overrides
{
    public class ReportOverride : IAutoMappingOverride<Report>
    {
        public void Override(AutoMapping<Report> mapping)
        {
            mapping.HasMany(x => x.ReportDocumentDefinitions).Cascade.SaveUpdate();
            mapping.HasMany(x => x.ReportLayouts).Cascade.SaveUpdate();
            mapping.HasMany(x => x.ReportParameters).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Parameters);

            mapping.Map(x => x.Content).CustomType("StringClob").CustomSqlType("text");
            mapping.Map(x => x.Info).CustomType("StringClob").CustomSqlType("text");

            mapping.References(x => x.Channel, "ChannelId").LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.Icon, "IconId");
            mapping.References(x => x.ExportType, "ReportFormatId");
        }
    }
}