﻿using System.Configuration;

using Castle.Windsor;
using Castle.MicroKernel.Registration;
using CommonServiceLocator.WindsorAdapter.Unofficial;
using Microsoft.Practices.ServiceLocation;

using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums;

namespace Domain.Reports.Base
{
    public class ReportGenerator
    {
        public IConnector Connector;

        private const string rootUser = @"iPlatform/connector/userName";
        private const string rootPassword = @"iPlatform/connector/apiKey";

        public ReportGenerator() {
            #if DEBUG
                //"Accept" any SSL Server Certificate if DEBUG mode
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            #endif

            Connector = new Connector();

            var username = ConfigurationManager.AppSettings[rootUser].ToString();
            var password = ConfigurationManager.AppSettings[rootPassword].ToString();

            Connector.SetToken(Connector.Authentication.Authenticate(username, password, string.Empty, string.Empty, ApiRequestType.Api).Token);
        }
    }
}