﻿using Domain.Base;

namespace Domain.Reports.Base
{
    public class ReportLayout : Entity
    {
        public ReportLayout() { }

        public ReportLayout(object[] obj)
        {
            this.Name = obj[0] != null ? (string)obj[0] : "";
            this.Label = obj[1] != null ? (string)obj[1] : "";
            this.Value = obj[2] != null ? (string)obj[2] : "";
            this.IsVisible = obj[3] != null ? (bool)obj[3] : true;
        }

        public virtual Report Report { get; set; }

        public virtual string Name { get; set; }
        public virtual string Label { get; set; }
        public virtual string Value { get; set; }
        public virtual bool IsVisible { get; set; }
    }
}
