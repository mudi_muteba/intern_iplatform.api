﻿namespace Domain.Reports.Base
{
    public static class ReportHelper
    {
        public static bool IsNumeric(this string s)
        {
            float output;
            return float.TryParse(s.Replace(" ", ""), out output);
        }
    }
}