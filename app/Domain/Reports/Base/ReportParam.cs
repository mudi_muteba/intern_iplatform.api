﻿using System.Linq;

using Domain.Base;

using MasterData;

namespace Domain.Reports.Base
{
    public class ReportParam : Entity
    {
        public ReportParam() { }

        public ReportParam(object[] obj)
        {
            Field = obj[0] != null ? obj[0].ToString() : "";
            DataType = obj[1] != null ? new DataTypes().FirstOrDefault(x => x.Id == int.Parse(obj[1].ToString())) : new DataTypes().FirstOrDefault();
            VisibleIndex = obj[2] != null ? int.Parse(obj[2].ToString()) : 0;
            Icon = obj[3] != null ? new FontAwesomeIcons().FirstOrDefault(x => x.Id == int.Parse(obj[3].ToString())) : new FontAwesomeIcons().FirstOrDefault();
            Name = obj[4] != null ? obj[4].ToString() : "";
            Description = obj[5] != null ? obj[5].ToString() : "";
            IsVisibleInReportViewer = obj[6] != null ? bool.Parse(obj[6].ToString()) : false;
            IsVisibleInScheduler = obj[7] != null ? bool.Parse(obj[7].ToString()) : false;
        }

        public virtual Report Report { get; set; }

        public virtual string Field { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual DataType DataType { get; set; }
        public virtual FontAwesomeIcon Icon { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual bool IsVisibleInReportViewer { get; set; }
        public virtual bool IsVisibleInScheduler { get; set; }
    }
}