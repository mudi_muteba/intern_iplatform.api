﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Reports.Base.Workflow.Events
{
    public class ReportScheduleEvent : BaseDomainEvent
    {
        public string UserName { get; set; }

        public ReportScheduleEvent(string userName, EventAudit audit, IEnumerable<ChannelReference> channelReferences) : base(audit, channelReferences)
        {
            UserName = userName;
        }
    }
}