﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Domain.Admin;
using Domain.Admin.Channeltemplates.Queries;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Entities.Extensions;
using Domain.Users;
using Domain.Users.Extensions;
using iPlatform.Api.DTOs.Reports.Base;
using Infrastructure.NHibernate.ContractResolvers;
using MasterData;
using MasterData.Authorisation;
using Newtonsoft.Json;
using NHibernate.Util;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Parameter = Telerik.Reporting.Parameter;
using Domain.Campaigns;

namespace Domain.Reports.Base.Handlers
{
    public class ReportGeneratorHandler : BaseDtoHandler<ReportGeneratorDto, ReportGeneratorResponseDto>
    {
        private readonly IRepository _repository;
        private readonly IProvideContext _contextProvider;

        private readonly GetAllChannelsQuery _allChannelsQuery;
        private readonly GetAllChannelTemplatesQuery _allChannelTemplatesQuery;

        private const string BaseUrl = @"iPlatform/serviceLocator/iPlatform/baseUrl";
        private const string ReportSource = @"Reports/Source";
        private const string ReportStorage = @"Reports/Storage";

        private readonly JsonSerializerSettings _serializerSettings;

        public ReportGeneratorHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _contextProvider = contextProvider;
            _repository = repository;
            _allChannelsQuery = new GetAllChannelsQuery(contextProvider, repository);
            _allChannelTemplatesQuery = new GetAllChannelTemplatesQuery(contextProvider, repository);

            _serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new NHibernateContractResolver(),
                Formatting = Formatting.Indented
            };
        }

        protected void GenerateLogoTemplate(Channel channel, string path)
        {
            var insurerCode = channel.Code;

            var fileTemplateType = new TemplateTypes().FirstOrDefault(y => y.Name == "File Template").Id;
            var logoTemplateContextType = new TemplateContextTypes().FirstOrDefault(y => y.Name == "Logo Template").Id;

            var logoTemplate = _allChannelTemplatesQuery.ExecuteQuery().FirstOrDefault(x => 
                            x.Channel.Id == channel.Id &&
                            x.TemplateType.Id == fileTemplateType &&
                            x.TemplateContextType.Id == logoTemplateContextType);

            if (logoTemplate == null)
            {
                Log.Info(string.Format("ReportGeneratorHandler.GenerateLogoTemplate() > Logo template not found for Channel with Id [{0}], locating default channel logo template...", channel.Id));

                channel = _repository.GetAll<Channel>().FirstOrDefault(x => x.Code == "IP");

                logoTemplate = _allChannelTemplatesQuery.ExecuteQuery().FirstOrDefault(x => 
                            x.Channel.Id == channel.Id &&
                            x.TemplateType.Id == fileTemplateType &&
                            x.TemplateContextType.Id == logoTemplateContextType);

                if (logoTemplate == null)
                {
                    Log.Info(string.Format("ReportGeneratorHandler.GenerateLogoTemplate() > Logo template not found for default Channel with Id [{0}], using default logo from existing source...", channel.Id));

                    var defaultLogoPath = Path.Combine(path, string.Format("{0}.png", "IP"));
                    var logo = File.ReadAllBytes(defaultLogoPath);
                    var finalPath = Path.Combine(path, string.Format("{0}.png", insurerCode));

                    File.WriteAllBytes(finalPath, logo);

                    return;
                }
            }

            if (!string.IsNullOrEmpty(logoTemplate.Content))
            {
                var logo = Convert.FromBase64String(logoTemplate.Content);
                var finalPath = Path.Combine(path, string.Format("{0}.png", insurerCode));

                File.WriteAllBytes(finalPath, logo);
            }
        }

        protected override void InternalHandle(ReportGeneratorDto dto, HandlerResult<ReportGeneratorResponseDto> result)
        {
            Log.Info("ReportGeneratorHandler.InternalHandle() > Criteria:");
            Log.Info(JsonConvert.SerializeObject(dto, _serializerSettings));

            var executionContext = _contextProvider.Get();
            var user = _repository.GetById<User>(executionContext.UserId).ThrowExceptionOnNull<User>(executionContext.UserId);

            var channel = GetChannelBasedOnCampaign(dto, executionContext.ActiveChannelId);
            if (channel == null)
            {
                Log.Info("ReportGeneratorHandler.InternalHandle() > Exiting InternalHandle()...");
                return;
            }

            var insurerCode = channel.Code;
            var baseUrl = ConfigurationManager.AppSettings[BaseUrl];
            var reportSource = ConfigurationManager.AppSettings[ReportSource];
            var reportLogoSource = Path.Combine(reportSource, "Logos");
            var reportStorage = ConfigurationManager.AppSettings[ReportStorage];

            GenerateLogoTemplate(channel, Path.Combine(dto.PathMap, reportLogoSource));

            var channelId = channel.Id;

            Log.Info("ReportGeneratorHandler.InternalHandle() > General Variables:");
            Log.Info(string.Format("source: {0}, storage: {1}, baseUrl: {2}, insurerCode: {3}, channelId: {4}", reportSource, reportStorage, baseUrl, insurerCode, channelId));

            var files = new List<string>();

            //Loop through the reports sent in ReportGeneratorDto
            dto.Reports.ForEach(report =>
            {
                //UAP
                var sourceFile = (insurerCode.ToLower() == "uap" && report.Name.ToLower() == "comparative quote") ? "ComparativeQuote_UAP.trdx": report.SourceFile;
                var uriReportSource = new UriReportSource {Uri = Path.Combine(dto.PathMap, reportSource, sourceFile)};

                //Populate global parameters (usable in any report) automatically
                report.Parameters.ForEach(p =>
                {
                    Parameter parameter;

                    switch (p.Field.Trim().ToLower())
                    {
                        case "baseurl":
                            parameter = new Parameter(p.Field, baseUrl);
                            break;
                        case "channelid":
                            parameter = new Parameter(p.Field, channelId);
                            break;
                        case "channelids":
                            var value = dto.Parameters.FirstOrDefault(x => x.Key.Trim().ToLower() == p.Field.Trim().ToLower()).Value + "";
                            if (string.IsNullOrEmpty(value))
                                value = executionContext.ChannelIds;
                            else
                                user.ThrowOnChannelMismatch(value);
                            parameter = new Parameter(p.Field, value);
                            break;
                        case "insurercode":
                            parameter = new Parameter(p.Field, insurerCode);
                            break;
                        case "organizationcode":
                            parameter = new Parameter(p.Field, insurerCode);
                            break;
                        case "reportname":
                            parameter = new Parameter(p.Field, report.Name);
                            break;
                        default: //Not a global parameter, use passed values
                            parameter = new Parameter(p.Field, dto.Parameters.FirstOrDefault(x => x.Key.Trim().ToLower() == p.Field.Trim().ToLower()).Value);
                            break;
                    }

                    uriReportSource.Parameters.Add(parameter);
                });

                Log.Info("ReportGeneratorHandler.InternalHandle() > uriReportSource:");
                Log.Info(JsonConvert.SerializeObject(uriReportSource, _serializerSettings));

                //Render the file
                var processor = new ReportProcessor();
                var deviceInfo = new Hashtable();

                var file = processor.RenderReport(dto.Format.Key, uriReportSource, deviceInfo);

                //Check for any generation errors
                if (file.Errors.Any())
                {
                    Log.Error("ReportGeneratorHandler.InternalHandle() > Rendered Report File Errors:");
                    Log.Error(JsonConvert.SerializeObject(file.Errors, _serializerSettings));
                }

                //Store the file
                var name = string.Format("{0} {1}.{2}", report.Name, DateTime.Now.ToString("yyyyMMddHHmmss"), file.Extension);
                var path = Path.Combine(dto.PathMap, reportStorage, name);

                Log.Info("ReportGeneratorHandler.InternalHandle() > Generated Report Pathing:");
                Log.Info(string.Format("name: {0}. path: {1}", name, path));

                using (FileStream fs = new FileStream(path, FileMode.Create)) fs.Write(file.DocumentBytes, 0, file.DocumentBytes.Length);

                //Create a Uri to the file
                var uri = Path.Combine(baseUrl, reportStorage, name).Replace(@"\", "/");

                Log.Info("ReportGeneratorHandler.InternalHandle() > Generated Report Uri:");
                Log.Info(uri);

                files.Add(uri);
            });

            Log.Info("ReportGeneratorHandler.InternalHandle() >  Generated Files:");
            Log.Info(JsonConvert.SerializeObject(files, _serializerSettings));

            //Return generated Uris for downlaoding
            result.Processed(new ReportGeneratorResponseDto { Reports = files });
        }

        public override List<RequiredAuthorisationPoint> RequiredRights {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }


        private Channel GetChannelBasedOnCampaign(ReportGeneratorDto dto, int activeChannelId)
        {
            List<int> campaignIds = new List<int>();
            bool multipleChannel = false;
            if(dto.Parameters.ContainsKey("CampaignIds"))
            {
                string[] stringcampaignIds = dto.Parameters.FirstOrDefault(x => x.Key == "CampaignIds").Value.ToString().Split(',');

                if (stringcampaignIds.Length > 0)
                    stringcampaignIds.ForEach(x => campaignIds.Add(Convert.ToInt32(x)));
            }

            if(campaignIds.Any())
            {
                var campaigns = _repository.GetAll<Campaign>().Where(x => campaignIds.Contains(x.Id)).ToList();

                List<int> ChannelIds = new List<int>();

                foreach(Campaign campaign in campaigns)
                {
                    if (!ChannelIds.Contains(campaign.Channel.Id))
                        ChannelIds.Add(campaign.Channel.Id);
                }

                if (ChannelIds.Count > 1)
                    multipleChannel = true;
            }


            if(multipleChannel)
                return _repository.GetAll<Channel>().FirstOrDefault(x => x.Code == "IP");
            else
            return _repository.GetById<Channel>(activeChannelId);
        }
    }
}
