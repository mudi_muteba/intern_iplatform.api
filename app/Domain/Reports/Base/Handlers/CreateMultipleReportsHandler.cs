﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Reports.Base;

using MasterData.Authorisation;
using Domain.Reports.Base;

namespace Domain.Admin.Channels.Reports.Handlers
{
    public class CreateMultipleReportsHandler : BaseDtoHandler<CreateMultipleReportsDto, int>
    {
        private IRepository repository;
        public CreateMultipleReportsHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(CreateMultipleReportsDto dto, HandlerResult<int> result)
        {
            dto.Reports
                .GroupBy(x => x.ChannelId)
                .Select(x => x.First())
                .ToList()
                .ForEach(x =>
                {
                    repository.GetAll<Report>()
                        .Where(y => !y.IsDeleted && y.Channel.Id == x.ChannelId)
                        .ToList()
                        .ForEach(y =>
                        {
                            repository.GetAll<ReportParam>()
                                .Where(z => z.Report.Id == y.Id)
                                .ToList()
                                .ForEach(z =>
                                {
                                    z.Delete();
                                    repository.Save(z);
                                });

                            repository.GetAll<ReportLayout>()
                                .Where(z => z.Report.Id == y.Id)
                                .ToList()
                                .ForEach(z =>
                                {
                                    z.Delete();
                                    repository.Save(z);
                                });

                            repository.GetAll<ReportDocumentDefinition>()
                                .Where(z => z.Report.Id == y.Id)
                                .ToList()
                                .ForEach(z =>
                                {
                                    z.Delete();
                                    repository.Save(z);
                                });

                            y.Delete();
                            repository.Save(y);
                        });
                });

            dto.Reports
                .ForEach(x =>
                {
                    repository.Save(Report.Create(x));
                });

            result.Processed(1);
        }
    }
}