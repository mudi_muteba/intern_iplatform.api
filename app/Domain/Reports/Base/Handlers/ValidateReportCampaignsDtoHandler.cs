﻿using System;
using System.Linq;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Reports;
using MasterData.Authorisation;
using Domain.Campaigns;
using ValidationMessages.Reports;

namespace Domain.Reports.Base.Handlers
{
    public class ValidateReportCampaignsDtoHandler : BaseDtoHandler<ValidateReportCampaignsDto, ValidatedReportCampaignsResponseDto>
    {
        private readonly IRepository m_repository;

        public ValidateReportCampaignsDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            m_repository = repository;
        }


        protected override void InternalHandle(ValidateReportCampaignsDto dto, HandlerResult<ValidatedReportCampaignsResponseDto> result)
        {
            List<Campaign> campaigns = m_repository.GetAll<Campaign>().Where(x => dto.Campaigns.Contains(x.Id)).ToList();

            if(!campaigns.Any())
            {
                result.Processed(new ValidatedReportCampaignsResponseDto
                {
                    IsMultiChannel = false,
                    IsValidated = false,
                    Message = ReportValidationMessages.CampaignsDoNotExist.DefaultMessage
                });
                return;
            }

            List<CampaignChannel> channels = new List<CampaignChannel>();
            campaigns.ForEach(x => {

                CampaignChannel existingChannel = channels.FirstOrDefault(y => y.ChannelId == x.Channel.Id);

                if(existingChannel == null)
                {
                    channels.Add(new CampaignChannel(x.Id, x.Channel.Id, x.Name));
                }
                else
                {
                    existingChannel.CampaignName = string.Format("{0}, {1}", existingChannel.CampaignName, x.Name);
                }
            });

            if(channels.Count > 1)
            {
                result.Processed(new ValidatedReportCampaignsResponseDto {
                    IsMultiChannel = true,
                    IsValidated = true,
                    Message = ReportValidationMessages.MultiChannel.DefaultMessage
                });
                return;
            }

            if(channels.Count == 1)
            {
                List<string> notConfiguredReport = ValidateReports(channels.First().ChannelId, dto);
                if (notConfiguredReport.Count > 0)
                {
                    result.Processed(new ValidatedReportCampaignsResponseDto
                    {
                        IsMultiChannel = false,
                        IsValidated = false,
                        Message = ReportValidationMessages.MissingReportConfiguration.AddParameters(new[] { String.Join(",", notConfiguredReport), channels.First().CampaignName }).DefaultMessage
                    });
                }
                else
                {
                    result.Processed(new ValidatedReportCampaignsResponseDto
                    {
                        IsMultiChannel = false,
                        IsValidated = true,
                        Message = ReportValidationMessages.ReportConfigurationExist.AddParameters(new[] { String.Join(",", notConfiguredReport), channels.First().CampaignName }).DefaultMessage
                    });
                }
            }
        }


        private List<string> ValidateReports(int channelId, ValidateReportCampaignsDto dto)
        {
            List<string> notConfiguredReport = new List<string>();

            foreach(string report in dto.Reports)
            {

                var ChannelTemplate = m_repository.GetAll<Report>().Where(x => x.Channel.Id == channelId 
                && x.Name.ToLowerInvariant() == report.ToLowerInvariant()).ToList();

                if (!ChannelTemplate.Any())
                {
                    notConfiguredReport.Add(report);
                }
            }

            return notConfiguredReport;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }
    }
    public class CampaignChannel
    {
        public CampaignChannel()
        {

        }

        public CampaignChannel(int campaignId, int channelId, string campaignName)
        {
            CampaignId = campaignId;
            ChannelId = channelId;
            CampaignName = campaignName;
        }

        public int CampaignId { get; set; }
        public int ChannelId { get; set; }
        public string CampaignName { get; set; }
    }
}
