﻿using System.Linq;

using Domain.Base;
using MasterData;

namespace Domain.Reports.Base
{
    public class ReportDocumentDefinition : Entity
    {
        public ReportDocumentDefinition() { }

        public ReportDocumentDefinition(object[] obj)
        {
            Report = obj[0] != null ? new Report { Id = int.Parse(obj[0].ToString()) } : new Report();
            this.DocumentDefinition = obj[1] != null ? new DocumentDefinitions().FirstOrDefault(x => x.Id == int.Parse(obj[1].ToString())) : new DocumentDefinitions().FirstOrDefault();
        }

        public virtual Report Report { get; set; }
        public virtual DocumentDefinition DocumentDefinition { get; set; }
    }
}