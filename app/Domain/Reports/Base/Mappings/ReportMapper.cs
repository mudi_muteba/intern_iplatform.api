﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.Base;

using MasterData;
using iPlatform.Api.DTOs.Reports;

namespace Domain.Reports.Base.Mappings
{
    public class ReportMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Report, ReportDto>()
                .ForMember(t => t.ReportCategory, o => o.MapFrom(s => new ReportCategories().First(x => x.Id == s.ReportCategory.Id)))
                .ForMember(t => t.ReportType, o => o.MapFrom(s => new ReportTypes().First(x => x.Id == s.ReportType.Id)))
                .ForMember(t => t.ReportFormat, o => o.NullSubstitute(ExportTypes.PDF))
                .ForMember(t => t.ReportFormat, o => o.MapFrom(s => new ExportTypes().FirstOrDefault(x => x.Id == s.ExportType.Id)))
                .ForMember(t => t.Icon, o => o.MapFrom(s => new FontAwesomeIcons().First(x => x.Id == s.Icon.Id)))
                .ForMember(t => t.DocumentDefinitions, o => o.MapFrom(s => s.ReportDocumentDefinitions))
                .ForMember(t => t.Layouts, o => o.MapFrom(s => s.ReportLayouts))
                .ForMember(t => t.Parameters, o => o.MapFrom(s => s.ReportParameters))
                ;

            Mapper.CreateMap<QueryResult<List<ReportDto>>, POSTResponseDto<List<ReportDto>>>()
                .ForMember(t => t.Errors, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               ;

            Mapper.CreateMap<CreateReportDto, Report>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)))
                .ForMember(t => t.ReportCategory, o => o.MapFrom(s => (new ReportCategories().Any(x => x.Id == s.ReportCategory.Id) ? new ReportCategories().First(x => x.Id == s.ReportCategory.Id) : new ReportCategory { Id = 0, Name = "Unknown" })))
                .ForMember(t => t.ReportType, o => o.MapFrom(s => (new ReportTypes().Any(x => x.Id == s.ReportType.Id) ? new ReportTypes().First(x => x.Id == s.ReportType.Id) : new ReportType { Id = 0, Name = "Unknown" })))
                .ForMember(t => t.ExportType, o => o.MapFrom(s => (new ExportTypes().Any(x => x.Id == s.ExportType.Id) ? new ExportTypes().First(x => x.Id == s.ExportType.Id) : new ExportType { Id = 0, Name = "Unknown" })))
                .ForMember(t => t.Icon, o => o.MapFrom(s => (new FontAwesomeIcons().Any(x => x.Id == s.Icon.Id) ? new FontAwesomeIcons().First(x => x.Id == s.Icon.Id) : new FontAwesomeIcon { Id = 0, Name = "Unknown" })))
                .ForMember(t => t.ReportDocumentDefinitions, o => o.MapFrom(s => s.ReportDocumentDefinitions))
                .ForMember(t => t.ReportLayouts, o => o.MapFrom(s => s.ReportLayouts))
                .ForMember(t => t.ReportParameters, o => o.MapFrom(s => s.ReportParameters))
                ;

            Mapper.CreateMap<List<Report>, ListResultDto<ReportDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<PagedResults<Report>, PagedResultDto<ReportDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s.Results))
                ;

            Mapper.CreateMap<List<Report>, ListResultDto<ReportDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<QueryResult<IList<ReportDto>>, POSTResponseDto<IList<ReportDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore())
               ;

            Mapper.CreateMap<QueryResult<IList<ReportDto>>, GETResponseDto<IList<ReportDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore())
               ;

            Mapper.CreateMap<HandlerResult<ValidatedReportCampaignsResponseDto>, POSTResponseDto<ValidatedReportCampaignsResponseDto>>()
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                ;
        }
    }
}