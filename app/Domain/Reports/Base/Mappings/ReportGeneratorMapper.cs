﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.Base;

namespace Domain.Reports.Base.Mappings
{
    public class ReportGeneratorMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<HandlerResult<ReportGeneratorResponseDto>, POSTResponseDto<ReportGeneratorResponseDto>>()
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               ;
        }
    }
}