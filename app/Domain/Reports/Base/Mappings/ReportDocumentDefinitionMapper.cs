﻿using System.Collections.Generic;

using AutoMapper;
using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.Base;

namespace Domain.Reports.Base.Mappings
{
    public class ReportDocumentDefinitionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ReportDocumentDefinition, ReportDocumentDefinitionDto>()
                .ForMember(t => t.ReportId, o => o.MapFrom(s => s.Report.Id))
                .ForMember(t => t.DocumentDefinitions, o => o.MapFrom(s => s.DocumentDefinition))
                ;

            Mapper.CreateMap<QueryResult<IList<ReportDocumentDefinitionDto>>, POSTResponseDto<IList<ReportDocumentDefinitionDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore())
               ;

            Mapper.CreateMap<QueryResult<IList<ReportDocumentDefinitionDto>>, GETResponseDto<IList<ReportDocumentDefinitionDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<ReportDocumentDefinitionDto, ReportDocumentDefinition>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<ReportDocumentDefinition, ReportDocumentDefinitionDto>()
                .ForMember(t => t.ReportId, o => o.MapFrom(s => s.Report.Id))
                .ForMember(t => t.DocumentDefinitions, o => o.MapFrom(s => s.DocumentDefinition))
                ;

            Mapper.CreateMap<List<ReportDocumentDefinitionDto>, List<ReportDocumentDefinition>>()
                ;

            Mapper.CreateMap<List<ReportDocumentDefinition>, List<ReportDocumentDefinitionDto>>()
                ;
        }
    }
}