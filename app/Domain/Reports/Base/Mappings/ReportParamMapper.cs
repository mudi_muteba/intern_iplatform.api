﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.Base;

namespace Domain.Reports.Base.Mappings
{
    public class ReportParamMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ReportParam, ReportParamDto>()
                .ForMember(t => t.ReportId, o => o.MapFrom(s => s.Report.Id))
                .ForMember(t => t.Report, o => o.Ignore())
                ;

            Mapper.CreateMap<QueryResult<IList<ReportParamDto>>, POSTResponseDto<IList<ReportParamDto>>>()
                .ForMember(t => t.Errors, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               ;

            Mapper.CreateMap<QueryResult<IList<ReportParamDto>>, GETResponseDto<IList<ReportParamDto>>>()
                .ForMember(t => t.Errors, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               ;

            Mapper.CreateMap<ReportParamDto, ReportParam>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<ReportParam, ReportParamDto>()
                .ForMember(t => t.ReportId, o => o.MapFrom(s => s.Report.Id))
                .ForMember(t => t.Report, o => o.Ignore())
                ;

            Mapper.CreateMap<List<ReportParamDto>, List<ReportParam>>()
                ;

            Mapper.CreateMap<List<ReportParam>, List<ReportParamDto>>()
                ;
        }
    }
}