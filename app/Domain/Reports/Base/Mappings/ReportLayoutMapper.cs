﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.Base;

namespace Domain.Reports.Base.Mappings
{
    public class ReportLayoutMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ReportLayout, ReportLayoutDto>()
                .ForMember(t => t.ReportId, o => o.MapFrom(s => s.Report.Id))
                .ForMember(t => t.Report, o => o.Ignore())
                ;

            Mapper.CreateMap<QueryResult<IList<ReportLayoutDto>>, POSTResponseDto<IList<ReportLayoutDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<ReportLayoutDto, ReportLayout>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<ReportLayout, ReportLayoutDto>()
                .ForMember(t => t.ReportId, o => o.MapFrom(s => s.Report.Id))
                .ForMember(t => t.Report, o => o.Ignore())
                ;

            Mapper.CreateMap<List<ReportLayoutDto>, List<ReportLayout>>()
                ;

            Mapper.CreateMap<List<ReportLayout>, List<ReportLayoutDto>>()
                ;
        }
    }
}