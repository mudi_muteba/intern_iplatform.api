﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.Base.StoredProcedures.Parameters
{
    public class ReportListStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ChannelId { get; set; }
    }
}