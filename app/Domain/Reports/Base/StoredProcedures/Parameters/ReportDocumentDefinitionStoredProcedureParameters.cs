﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.Base.StoredProcedures.Parameters
{
    public class ReportDocumentDefinitionStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ReportId { get; set; }
    }
}