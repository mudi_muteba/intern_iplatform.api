﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.Base.StoredProcedures.Parameters
{
    public class ReportLayoutStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ChannelId { get; set; }
        public string ReportName { get; set; }
    }
}