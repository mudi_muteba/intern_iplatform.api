﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.Base.StoredProcedures.Parameters
{
    public class ReportParamsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ReportId { get; set; }
    }
}