﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Base.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.Base.Criteria;

namespace Domain.Reports.Base.StoredProcedures
{
    public class ReportParamsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_ReportParams";
            }
        }

        public ReportParamsStoredProcedure() { }

        public ReportParamsStoredProcedure(ReportParamsCriteriaDto criteria)
        {
            base.
                SetParameters(new ReportParamsStoredProcedureParameters
                {
                    ReportId = criteria.ReportId
                });
        }
    }
}