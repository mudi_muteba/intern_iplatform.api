﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.Base.StoredProcedures
{
    public class ReportListStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_ReportList";
            }
        }

        public ReportListStoredProcedure() { }
    }
}