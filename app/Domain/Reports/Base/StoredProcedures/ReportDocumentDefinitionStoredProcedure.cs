﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Base.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.Base.Criteria;

namespace Domain.Reports.Base.StoredProcedures
{
    public class ReportDocumentDefinitionStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_ReportDocumentDefinition";
            }
        }

        public ReportDocumentDefinitionStoredProcedure() { }

        public ReportDocumentDefinitionStoredProcedure(ReportDocumentDefinitionCriteriaDto criteria)
        {
            base.
                SetParameters(new ReportDocumentDefinitionStoredProcedureParameters
                {
                    ReportId = criteria.ReportId
                });
        }
    }
}