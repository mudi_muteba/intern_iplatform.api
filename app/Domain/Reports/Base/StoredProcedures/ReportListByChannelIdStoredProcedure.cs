﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Base.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.Base.Criteria;

namespace Domain.Reports.Base.StoredProcedures
{
    public class ReportListByChannelIdStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_ReportList_ByChannelId";
            }
        }

        public ReportListByChannelIdStoredProcedure() { }

        public ReportListByChannelIdStoredProcedure(ReportCriteriaDto criteria)
        {
            base.
                SetParameters(new ReportListStoredProcedureParameters
                {
                    ChannelId = criteria.ChannelId
                });
        }
    }
}