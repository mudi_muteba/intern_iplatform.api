﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.Base.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.Base.Criteria;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;
using iPlatform.Api.DTOs.Reports.Quote.Criteria;

namespace Domain.Reports.Base.StoredProcedures
{
    public class ReportLayoutStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_Layout";
            }
        }

        public ReportLayoutStoredProcedure() { }

        public ReportLayoutStoredProcedure(ReportLayoutCriteriaDto criteria)
        {
            base.
                SetParameters(new ReportLayoutStoredProcedureParameters
                {
                    ChannelId = criteria.ChannelId,
                    ReportName = criteria.ReportName
                });
        }

        public ReportLayoutStoredProcedure(QuoteReportCriteriaDto criteria)
        {
            base.
                SetParameters(new ReportLayoutStoredProcedureParameters
                {
                    ChannelId = criteria.ChannelId,
                    ReportName = criteria.ReportName
                });
        }

        public ReportLayoutStoredProcedure(ComparativeQuoteReportCriteriaDto criteria)
        {
            base.
                SetParameters(new ReportLayoutStoredProcedureParameters
                {
                    ChannelId = criteria.ChannelId,
                    ReportName = criteria.ReportName
                });
        }
    }
}