﻿using System;
using Domain.Base;

namespace Domain.Reports.AIG
{
    public class AigReportSalesForceIntegrationLog
    {
        public AigReportSalesForceIntegrationLog() { }

        public AigReportSalesForceIntegrationLog(object[] obj)
        {
            this.CreatedOn = obj[0] != null ? (DateTime)obj[0] : DateTime.MinValue;
            this.PartyId = obj[1] != null ? (int)obj[1] : 0;
            this.JsonData = obj[2] != null ? (string)obj[2] : "";
            this.Response = obj[3] != null ? (string)obj[3] : "";
            this.IsSuccess = obj[4] != null ? (bool)obj[4] : false;
            this.Status = obj[5] != null ? (string)obj[5] : "";
        }
        public virtual string JsonData { get; set; }
        public virtual string Response { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual int PartyId { get; set; }
        public virtual bool IsSuccess { get; set; }
        public virtual string Status { get; set; }
    }
}
