﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.AIG.Queries.Filters;
using Domain.Reports.AIG.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.AIG.Queries
{
    public class AigReportBuildingExtractQuery : BaseStoredProcedureQueryObject<AigReportBuildingExtract, AigReportBuildingExtractStoredProcedure>
    {
        public AigReportBuildingExtractQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new AigReportBuildingExtractFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AigReportBuildingExtract> Execute(IList<AigReportBuildingExtract> query)
        {
            return query;
        }
    }
}
