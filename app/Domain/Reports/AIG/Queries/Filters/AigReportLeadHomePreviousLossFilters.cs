﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportLeadHomePreviousLossFilters : IApplyDefaultFilters<AigReportLeadQuoteHomePreviousLossExtract>
    {
        public IQueryable<AigReportLeadQuoteHomePreviousLossExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportLeadQuoteHomePreviousLossExtract> query)
        {
            return query;
        }
    }
}
