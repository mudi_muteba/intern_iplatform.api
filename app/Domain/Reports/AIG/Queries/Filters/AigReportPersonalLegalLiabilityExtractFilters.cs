﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportPersonalLegalLiabilityExtractFilters : IApplyDefaultFilters<AigReportPersonalLegalLiabilityExtract>
    {
        public IQueryable<AigReportPersonalLegalLiabilityExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportPersonalLegalLiabilityExtract> query)
        {
            return query;
        }
    }
}