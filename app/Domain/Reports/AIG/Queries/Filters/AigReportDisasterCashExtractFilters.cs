﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportDisasterCashExtractFilters : IApplyDefaultFilters<AigReportDisasterCashExtract>
    {
        public IQueryable<AigReportDisasterCashExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportDisasterCashExtract> query)
        {
            return query;
        }
    }
}