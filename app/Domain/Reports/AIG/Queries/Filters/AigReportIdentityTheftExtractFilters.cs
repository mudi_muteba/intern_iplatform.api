﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportIdentityTheftExtractFilters : IApplyDefaultFilters<AigReportIdentityTheftExtract>
    {
        public IQueryable<AigReportIdentityTheftExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportIdentityTheftExtract> query)
        {
            return query;
        }
    }
}