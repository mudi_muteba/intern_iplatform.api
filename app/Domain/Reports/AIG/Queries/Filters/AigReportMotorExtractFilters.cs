﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportMotorExtractFilters : IApplyDefaultFilters<AigReportMotorExtract>
    {
        public IQueryable<AigReportMotorExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportMotorExtract> query)
        {
            return query;
        }
    }
}
