﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportAllRiskExtractFilters : IApplyDefaultFilters<AigReportAllRiskExtract>
    {
        public IQueryable<AigReportAllRiskExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportAllRiskExtract> query)
        {
            return query;
        }
    }
}