﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportContentExtractFilters : IApplyDefaultFilters<AigReportContentExtract>
    {
        public IQueryable<AigReportContentExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportContentExtract> query)
        {
            return query;
        }
    }
}