﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportLeadExtractFilters : IApplyDefaultFilters<AigReportLeadExtract>
    {
        public IQueryable<AigReportLeadExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportLeadExtract> query)
        {
            return query;
        }
    }
}
