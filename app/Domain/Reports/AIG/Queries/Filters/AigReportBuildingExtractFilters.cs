﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportBuildingExtractFilters : IApplyDefaultFilters<AigReportBuildingExtract>
    {
        public IQueryable<AigReportBuildingExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportBuildingExtract> query)
        {
            return query;
        }
    }
}