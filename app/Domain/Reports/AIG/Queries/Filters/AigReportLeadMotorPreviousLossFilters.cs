﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportLeadMotorPreviousLossFilters : IApplyDefaultFilters<AigReportLeadQuoteMotorPreviousLossExtract>
    {
        public IQueryable<AigReportLeadQuoteMotorPreviousLossExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportLeadQuoteMotorPreviousLossExtract> query)
        {
            return query;
        }
    }
}
