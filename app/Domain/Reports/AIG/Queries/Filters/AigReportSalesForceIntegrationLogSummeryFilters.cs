﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.SalesForce;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportSalesForceIntegrationLogSummeryFilters : IApplyDefaultFilters<AigReportSalesForceLogSummery>
    {
        public IQueryable<AigReportSalesForceLogSummery> Apply(ExecutionContext executionContext, IQueryable<AigReportSalesForceLogSummery> query)
        {
            return query;
        }
    }
}
