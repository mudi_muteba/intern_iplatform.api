﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportFuneralExtractFilters : IApplyDefaultFilters<AigReportFuneralExtract>
    {
        public IQueryable<AigReportFuneralExtract> Apply(ExecutionContext executionContext, IQueryable<AigReportFuneralExtract> query)
        {
            return query;
        }
    }
}