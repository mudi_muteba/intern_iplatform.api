﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.SalesForce;

namespace Domain.Reports.AIG.Queries.Filters
{
    public class AigReportSalesForceIntegrationLogFilters : IApplyDefaultFilters<AigReportSalesForceIntegrationLog>
    {
        public IQueryable<AigReportSalesForceIntegrationLog> Apply(ExecutionContext executionContext, IQueryable<AigReportSalesForceIntegrationLog> query)
        {
            return query;
        }
    }
}
