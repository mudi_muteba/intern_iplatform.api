﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.AIG.Queries.Filters;
using Domain.Reports.AIG.StoredProcedures;
using Domain.SalesForce;
using MasterData.Authorisation;

namespace Domain.Reports.AIG.Queries
{
    public class AigReportSalesForceExtractQuery : BaseStoredProcedureQueryObject<AigReportSalesForceIntegrationLog, AigReportSalesForceIntegrationExtractStoredProcedure>
    {
        public AigReportSalesForceExtractQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new AigReportSalesForceIntegrationLogFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AigReportSalesForceIntegrationLog> Execute(IList<AigReportSalesForceIntegrationLog> query)
        {
            return query;
        }
    }
}
