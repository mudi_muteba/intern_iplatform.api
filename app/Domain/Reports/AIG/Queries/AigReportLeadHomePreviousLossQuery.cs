﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.AIG.Queries.Filters;
using Domain.Reports.AIG.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.AIG.Queries
{
    public class AigReportLeadHomePreviousLossQuery : BaseStoredProcedureQueryObject<AigReportLeadQuoteHomePreviousLossExtract, AigReportLeadHomePreviousLossStoredProcedure>
    {
        public AigReportLeadHomePreviousLossQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new AigReportLeadHomePreviousLossFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AigReportLeadQuoteHomePreviousLossExtract> Execute(IList<AigReportLeadQuoteHomePreviousLossExtract> query)
        {
            return query;
        }
    }
}
