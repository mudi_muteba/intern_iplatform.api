﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.AIG.Queries.Filters;
using Domain.Reports.AIG.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.AIG.Queries
{
    public class AigReportPersonalLegalLiabilityExtractQuery : BaseStoredProcedureQueryObject<AigReportPersonalLegalLiabilityExtract, AigReportPersonalLegalLiabilityExtractStoredProcedure>
    {
        public AigReportPersonalLegalLiabilityExtractQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new AigReportPersonalLegalLiabilityExtractFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AigReportPersonalLegalLiabilityExtract> Execute(IList<AigReportPersonalLegalLiabilityExtract> query)
        {
            return query;
        }
    }
}
