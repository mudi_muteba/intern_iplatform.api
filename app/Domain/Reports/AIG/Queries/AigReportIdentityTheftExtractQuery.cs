﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.AIG.Queries.Filters;
using Domain.Reports.AIG.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.AIG.Queries
{
    public class AigReportIdentityTheftExtractQuery : BaseStoredProcedureQueryObject<AigReportIdentityTheftExtract, AigReportIdentityTheftExtractStoredProcedure>
    {
        public AigReportIdentityTheftExtractQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new AigReportIdentityTheftExtractFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AigReportIdentityTheftExtract> Execute(IList<AigReportIdentityTheftExtract> query)
        {
            return query;
        }
    }
}
