﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.AIG.Queries.Filters;
using Domain.Reports.AIG.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.AIG.Queries
{
    public class AigReportLeadExtractQuery : BaseStoredProcedureQueryObject<AigReportLeadExtract, AigReportLeadExtractStoredProcedure>
    {
        public AigReportLeadExtractQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new AigReportLeadExtractFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AigReportLeadExtract> Execute(IList<AigReportLeadExtract> query)
        {
            return query;
        }
    }
}
