﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.AIG.Queries.Filters;
using Domain.Reports.AIG.StoredProcedures;
using Domain.SalesForce;
using MasterData.Authorisation;

namespace Domain.Reports.AIG.Queries
{
    public class AigReportSalesForceLogSummeryExtractQuery : BaseStoredProcedureQueryObject<AigReportSalesForceLogSummery, AigReportSalesForceLogSummeryStoredProcesure>
    {
        public AigReportSalesForceLogSummeryExtractQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new AigReportSalesForceIntegrationLogSummeryFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AigReportSalesForceLogSummery> Execute(IList<AigReportSalesForceLogSummery> query)
        {
            return query;
        }
    }
}
