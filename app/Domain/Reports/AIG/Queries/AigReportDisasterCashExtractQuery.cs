﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.AIG.Queries.Filters;
using Domain.Reports.AIG.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.AIG.Queries
{
    public class AigReportDisasterCashExtractQuery : BaseStoredProcedureQueryObject<AigReportDisasterCashExtract, AigReportDisasterCashExtractStoredProcedure>
    {
        public AigReportDisasterCashExtractQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new AigReportDisasterCashExtractFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AigReportDisasterCashExtract> Execute(IList<AigReportDisasterCashExtract> query)
        {
            return query;
        }
    }
}
