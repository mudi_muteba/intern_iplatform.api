﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Reports.AIG.Queries.Filters;
using Domain.Reports.AIG.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Reports.AIG.Queries
{
    public class AigReportAllRiskExtractQuery : BaseStoredProcedureQueryObject<AigReportAllRiskExtract, AigReportAllRiskExtractStoredProcedure>
    {
        public AigReportAllRiskExtractQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new AigReportAllRiskExtractFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AigReportAllRiskExtract> Execute(IList<AigReportAllRiskExtract> query)
        {
            return query;
        }
    }
}
