﻿using System;
using Domain.Base;

namespace Domain.Reports.AIG
{
    public class AigReportDisasterCashExtract 
    {
        public AigReportDisasterCashExtract()
        {
        }
        public AigReportDisasterCashExtract(object[] obj)
        {
            this.QuoteCreateDate = obj[0] != null ? (string)obj[0] : "";
            this.QuoteCreateTime = obj[1] != null ? (string)obj[1] : "";
            this.DateOfQuote = obj[2] != null ? (string)obj[2] : "";
            this.QuoteNumber = obj[3] != null ? (string)obj[3] : "";
            this.LeadNumber = obj[4] != null ? (int)obj[4] : 0;
            this.AddressSuburb = obj[5] != null ? (string)obj[5] : "";
            this.AddressPostCode = obj[6] != null ? (string)obj[6] : "";
            this.AddressProvince = obj[7] != null ? (string)obj[7] : "";
            this.CashLimit = obj[8] != null ? (string)obj[8] : "";
            this.Premium = obj[9] != null ? (decimal)obj[9] : 0;
        }

        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual string DateOfQuote { get; set; }
        public virtual string QuoteNumber { get; set; }
        public virtual int LeadNumber { get; set; }
        public virtual string AddressSuburb { get; set; }
        public virtual string AddressPostCode { get; set; }
        public virtual string AddressProvince { get; set; }
        public virtual string CashLimit { get; set; }
        public virtual decimal Premium { get; set; }

    }
}
