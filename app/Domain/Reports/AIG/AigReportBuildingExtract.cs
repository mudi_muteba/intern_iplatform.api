﻿namespace Domain.Reports.AIG
{
    public class AigReportBuildingExtract 
    {
        public AigReportBuildingExtract()
        {
        }
        public AigReportBuildingExtract(object[] obj)
        {

            Channel = obj[0] != null ? (string)obj[0] : "";
            QuoteExpired = obj[1] != null ? (string)obj[1] : "";
            QuoteBound = obj[2] != null ? (string)obj[2] : "";
            QuoteBindDate = obj[3] != null ? (string)obj[3] : "";
            QuoteTotalPremium = obj[4] != null ? (decimal)obj[4] : 0;
            QuoteMotorPremium = obj[5] != null ? (decimal)obj[5] : 0;
            QuoteHomePremium = obj[6] != null ? (decimal)obj[6] : 0;
            QuoteFuneralPremium = obj[7] != null ? (decimal)obj[7] : 0;
            QuoteOtherPremium = obj[8] != null ? (decimal)obj[8] : 0;
            QuoteFees = obj[9] != null ? (decimal)obj[9] : 0;
            QuoteSasria = obj[10] != null ? (decimal)obj[10] : 0;
            QuoteIncepptionDate = obj[11] != null ? (string)obj[11] : "";
            QuotePolicyNumber = obj[12] != null ? (string)obj[12] : "";
            QuoteCreateDate = obj[13] != null ? (string)obj[13] : "";
            QuoteCreateTime = obj[14] != null ? (string)obj[14] : "";
            QuoteId = obj[15] != null ? (int)obj[15] : 0;
            Vmsaid = obj[16] != null ? (string)obj[16] : "";
            PropertyNumber = obj[17] != null ? (int)obj[17] : 0;
            CoverType = obj[18] != null ? (string)obj[18] : "";
            AddressSuburb = obj[19] != null ? (string)obj[19] : "";
            AddressPostCode = obj[20] != null ? (string)obj[20] : "";
            AddressProvince = obj[21] != null ? (string)obj[21] : "";
            HomeType = obj[22] != null ? (string)obj[22] : "";
            RoofConstruction = obj[23] != null ? (string)obj[23] : "";
            WallConstruction = obj[24] != null ? (string)obj[24] : "";
            ThatchLapa = obj[25] != null ? (string)obj[25] : "";
            ThatchLapaSize = obj[26] != null ? (string)obj[26] : "";
            LightningConductor = obj[27] != null ? (string)obj[27] : "";
            PropertyBorder = obj[28] != null ? (string)obj[28] : "";
            ConstructionYear = obj[29] != null ? (string)obj[29] : "";
            SquareMetresOfProperty = obj[30] != null ? (string)obj[30] : "";
            NumberOfBathrooms = obj[31] != null ? (string)obj[31] : "";
            PropertyUnderConstruction = obj[32] != null ? (string)obj[32] : "";
            HomeUsage = obj[33] != null ? (string)obj[33] : "";
            BusinessConducted = obj[34] != null ? (string)obj[34] : "";
            Commune = obj[35] != null ? (string)obj[35] : "";
            PeriodPropertyIsUnoccupied = obj[36] != null ? (string)obj[36] : "";
            Ownership = obj[37] != null ? (string)obj[37] : "";
            MortgageBank = obj[38] != null ? (string)obj[38] : "";
            SecurityGatedCommunity = obj[39] != null ? (string)obj[39] : "";
            SecuritySecurityComplex = obj[40] != null ? (string)obj[40] : "";
            SecurityBurglarBars = obj[41] != null ? (string)obj[41] : "";
            SecuritySecurityGates = obj[42] != null ? (string)obj[42] : "";
            SecurityAlarmWithArmedResponse = obj[43] != null ? (string)obj[43] : "";
            VapsWaterPumpingMachinery = obj[44] != null ? (string)obj[44] : "";
            VapsAccidentalDamage = obj[45] != null ? (string)obj[45] : "";
            VapsSubsidenceAndLandslip = obj[46] != null ? (string)obj[46] : "";
            BuildingsAdditionalExcess = obj[47] != null ? (string)obj[47] : "";
            SumInsured = obj[48] != null ? (string)obj[48] : "";
            DiscretionaryDiscount = obj[49] != null ? (string)obj[49] : "";
            Sasria = obj[50] != null ? (decimal)obj[50] : 0;
            PremiumRisk = obj[51] != null ? (decimal)obj[51] : 0;
            PremiumSubsidenceAndLandslip = obj[52] != null ? (decimal)obj[52] : 0;
            PremiumWaterMachinery = obj[53] != null ? (decimal)obj[53] : 0;
            PremiumTotal = obj[54] != null ? (decimal)obj[54] : 0;
        }

        public virtual string Channel { get; set; }
        public virtual string QuoteExpired { get; set; }
        public virtual string QuoteBound { get; set; }
        public virtual string QuoteBindDate { get; set; }
        public virtual decimal QuoteTotalPremium { get; set; }
        public virtual decimal QuoteMotorPremium { get; set; }
        public virtual decimal QuoteHomePremium { get; set; }
        public virtual decimal QuoteFuneralPremium { get; set; }
        public virtual decimal QuoteOtherPremium { get; set; }
        public virtual decimal QuoteFees { get; set; }
        public virtual decimal QuoteSasria { get; set; }
        public virtual string QuoteIncepptionDate { get; set; }
        public virtual string QuotePolicyNumber { get; set; }
        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual int QuoteId { get; set; }
        public virtual string Vmsaid { get; set; }
        public virtual int PropertyNumber { get; set; }
        public virtual string CoverType { get; set; }
        public virtual string AddressSuburb { get; set; }
        public virtual string AddressPostCode { get; set; }
        public virtual string AddressProvince { get; set; }
        public virtual string HomeType { get; set; }
        public virtual string RoofConstruction { get; set; }
        public virtual string WallConstruction { get; set; }
        public virtual string ThatchLapa { get; set; }
        public virtual string ThatchLapaSize { get; set; }
        public virtual string LightningConductor { get; set; }
        public virtual string PropertyBorder { get; set; }
        public virtual string ConstructionYear { get; set; }
        public virtual string SquareMetresOfProperty { get; set; }
        public virtual string NumberOfBathrooms { get; set; }
        public virtual string PropertyUnderConstruction { get; set; }
        public virtual string HomeUsage { get; set; }
        public virtual string BusinessConducted { get; set; }
        public virtual string Commune { get; set; }
        public virtual string PeriodPropertyIsUnoccupied { get; set; }
        public virtual string Ownership { get; set; }
        public virtual string MortgageBank { get; set; }
        public virtual string SecurityGatedCommunity { get; set; }
        public virtual string SecuritySecurityComplex { get; set; }
        public virtual string SecurityBurglarBars { get; set; }
        public virtual string SecuritySecurityGates { get; set; }
        public virtual string SecurityAlarmWithArmedResponse { get; set; }
        public virtual string VapsWaterPumpingMachinery { get; set; }
        public virtual string VapsAccidentalDamage { get; set; }
        public virtual string VapsSubsidenceAndLandslip { get; set; }
        public virtual string BuildingsAdditionalExcess { get; set; }
        public virtual string SumInsured { get; set; }
        public virtual string DiscretionaryDiscount { get; set; }
        public virtual decimal Sasria { get; set; }
        public virtual decimal PremiumRisk { get; set; }
        public virtual decimal PremiumSubsidenceAndLandslip { get; set; }
        public virtual decimal PremiumWaterMachinery { get; set; }
        public virtual decimal PremiumTotal { get; set; }
    }
}
