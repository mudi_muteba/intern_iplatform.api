﻿using System;
using Domain.Base;

namespace Domain.Reports.AIG
{
    public class AigReportSalesForceLogSummery 
    {
        public AigReportSalesForceLogSummery() { }

        public AigReportSalesForceLogSummery(object[] obj)
        {
            this.Description = obj[0] != null ? (string)obj[0] : "";
            this.Total = obj[1] != null ? (int)obj[1] : 0;
        }
        public virtual string Description { get; set; }
        public virtual int Total { get; set; }
    }
}