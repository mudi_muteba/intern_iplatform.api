﻿using AutoMapper;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.AIG.AllRisk;
using iPlatform.Api.DTOs.Reports.AIG.Building;
using iPlatform.Api.DTOs.Reports.AIG.Content;
using iPlatform.Api.DTOs.Reports.AIG.DisasterCash;
using iPlatform.Api.DTOs.Reports.AIG.Funeral;
using iPlatform.Api.DTOs.Reports.AIG.IdentityTheft;
using iPlatform.Api.DTOs.Reports.AIG.Lead;
using iPlatform.Api.DTOs.Reports.AIG.Motor;
using iPlatform.Api.DTOs.Reports.AIG.PersonalLegalLiability;

namespace Domain.Reports.AIG.Mappings
{
    public class AigReportMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<QueryResult<AigReportFuneralDto>, POSTResponseDto<AigReportFuneralDto>>()
            .ForMember(t => t.StatusCode, o => o.Ignore())
            .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
            .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<AigReportDisasterCashDto>, POSTResponseDto<AigReportDisasterCashDto>>()
             .ForMember(t => t.StatusCode, o => o.Ignore())
             .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
             .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<AigReportPersonalLegalLiabilityDto>, POSTResponseDto<AigReportPersonalLegalLiabilityDto>>()
              .ForMember(t => t.StatusCode, o => o.Ignore())
              .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
              .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<AigReportIdentityTheftDto>, POSTResponseDto<AigReportIdentityTheftDto>>()
              .ForMember(t => t.StatusCode, o => o.Ignore())
              .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
              .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<AigReportContentDto>, POSTResponseDto<AigReportContentDto>>()
              .ForMember(t => t.StatusCode, o => o.Ignore())
              .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
              .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<AigReportBuildingDto>, POSTResponseDto<AigReportBuildingDto>>()
              .ForMember(t => t.StatusCode, o => o.Ignore())
              .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
              .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<AigReportAllRiskDto>, POSTResponseDto<AigReportAllRiskDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<AigReportMotorDto>, POSTResponseDto<AigReportMotorDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<AigReportDto>, POSTResponseDto<AigReportDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}
