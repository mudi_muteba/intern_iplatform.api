﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.AIG.AllRisk;
using iPlatform.Api.DTOs.Reports.AIG.Building;
using iPlatform.Api.DTOs.Reports.AIG.Content;
using iPlatform.Api.DTOs.Reports.AIG.DisasterCash;
using iPlatform.Api.DTOs.Reports.AIG.Funeral;
using iPlatform.Api.DTOs.Reports.AIG.IdentityTheft;
using iPlatform.Api.DTOs.Reports.AIG.Lead;
using iPlatform.Api.DTOs.Reports.AIG.Motor;
using iPlatform.Api.DTOs.Reports.AIG.PersonalLegalLiability;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery;

namespace Domain.Reports.AIG.Mappings
{
    public class AigReportLeadExtractMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AigReportFuneralExtract, AigReportFuneralExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportFuneralExtractDto>>, POSTResponseDto<IList<AigReportFuneralExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportDisasterCashExtract, AigReportDisasterCashExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportDisasterCashExtractDto>>, POSTResponseDto<IList<AigReportDisasterCashExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportPersonalLegalLiabilityExtract, AigReportPersonalLegalLiabilityExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportPersonalLegalLiabilityExtractDto>>, POSTResponseDto<IList<AigReportPersonalLegalLiabilityExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportIdentityTheftExtract, AigReportIdentityTheftExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportIdentityTheftExtractDto>>, POSTResponseDto<IList<AigReportIdentityTheftExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportBuildingExtract, AigReportBuildingExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportBuildingExtractDto>>, POSTResponseDto<IList<AigReportBuildingExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportContentExtract, AigReportContentExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportContentExtractDto>>, POSTResponseDto<IList<AigReportContentExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportAllRiskExtract, AigReportAllRiskExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportAllRiskExtractDto>>, POSTResponseDto<IList<AigReportAllRiskExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportMotorExtract, AigReportMotorExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportMotorExtractDto>>, POSTResponseDto<IList<AigReportMotorExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());


            Mapper.CreateMap<AigReportSalesForceIntegrationLog, AigReportSalesForceIntegrationLogExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportSalesForceIntegrationLogExtractDto>>, POSTResponseDto<IList<AigReportSalesForceIntegrationLogExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());


            Mapper.CreateMap<AigReportSalesForceLogSummery, AigReportSalesForceLogSummeryExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportSalesForceLogSummeryExtractDto>>, POSTResponseDto<IList<AigReportSalesForceLogSummeryExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportLeadExtract, AigReportLeadExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportLeadExtractDto>>, POSTResponseDto<IList<AigReportLeadExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportLeadQuoteMotorPreviousLossExtract, AigReportLeadQuoteMotorPreviousLossExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportLeadQuoteMotorPreviousLossExtractDto>>, POSTResponseDto<IList<AigReportLeadQuoteMotorPreviousLossExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportLeadQuoteHomePreviousLossExtract, AigReportLeadQuoteHomePreviousLossExtractDto>();

            Mapper.CreateMap<QueryResult<IList<AigReportLeadQuoteHomePreviousLossExtractDto>>, POSTResponseDto<IList<AigReportLeadQuoteHomePreviousLossExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());


            Mapper.CreateMap<AigReportLeadQuoteHomePreviousLossExtract, AigReportLeadQuotePreviousLossExtractDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.PartyId))
                .ForMember(t => t.Type, o => o.MapFrom(s => s.Type))
                .ForMember(t => t.DateOfLoss, o => o.MapFrom(s => s.DateOfLoss))
                .ForMember(t => t.LossType, o => o.MapFrom(s => s.HomeLossType))
                .ForMember(t => t.LossClaim, o => o.MapFrom(s => s.HomeLossClaim))
                .ForMember(t => t.Location, o => o.MapFrom(s => s.Location));

            Mapper.CreateMap<QueryResult<IList<AigReportLeadQuoteHomePreviousLossExtractDto>>,POSTResponseDto<IList<AigReportLeadQuotePreviousLossExtractDto>>>()
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<AigReportLeadQuoteMotorPreviousLossExtract, AigReportLeadQuotePreviousLossExtractDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.PartyId))
                .ForMember(t => t.Type, o => o.MapFrom(s => s.Type))
                .ForMember(t => t.DateOfLoss, o => o.MapFrom(s => s.DateOfLoss))
                .ForMember(t => t.LossType, o => o.MapFrom(s => s.MotorLossType))
                .ForMember(t => t.LossClaim, o => o.MapFrom(s => s.MotorLossClaim))
                .ForMember(t => t.Location, o => o.MapFrom(s => s.Location));

            Mapper.CreateMap<QueryResult<IList<AigReportLeadQuoteMotorPreviousLossExtractDto>>,POSTResponseDto<IList<AigReportLeadQuotePreviousLossExtractDto>>>()
            .ForMember(t => t.StatusCode, o => o.Ignore())
            .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
            .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<AigReportLeadQuotePreviousLossExtractDto>>, POSTResponseDto<IList<AigReportLeadQuotePreviousLossExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}
