﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.PersonalLegalLiability.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportPersonalLegalLiabilityExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_PersonalLegalLiabilityExtract";
            }
        }

        public AigReportPersonalLegalLiabilityExtractStoredProcedure() { }

        public AigReportPersonalLegalLiabilityExtractStoredProcedure(AigReportPersonalLegalLiabilityExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportPersonalLegalLiabilityExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}