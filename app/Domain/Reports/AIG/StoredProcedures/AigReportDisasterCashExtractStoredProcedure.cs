﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.DisasterCash.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportDisasterCashExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_DisasterCashExtract";
            }
        }

        public AigReportDisasterCashExtractStoredProcedure() { }

        public AigReportDisasterCashExtractStoredProcedure(AigReportDisasterCashExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportDisasterCashExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}