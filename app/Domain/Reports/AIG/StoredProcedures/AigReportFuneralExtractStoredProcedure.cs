﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.Funeral.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportFuneralExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_FuneralExtract";
            }
        }

        public AigReportFuneralExtractStoredProcedure() { }

        public AigReportFuneralExtractStoredProcedure(AigReportFuneralExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportFuneralExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}
