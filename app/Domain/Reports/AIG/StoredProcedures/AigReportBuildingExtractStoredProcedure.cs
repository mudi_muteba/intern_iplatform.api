﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.Building.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportBuildingExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_BuildingExtract";
            }
        }

        public AigReportBuildingExtractStoredProcedure() { }

        public AigReportBuildingExtractStoredProcedure(AigReportBuildingExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportBuildingExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}
