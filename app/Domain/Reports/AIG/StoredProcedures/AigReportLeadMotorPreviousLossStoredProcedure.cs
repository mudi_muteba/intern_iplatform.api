﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.Lead.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportLeadMotorPreviousLossStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_LeadPreviousMotorLoss";
            }
        }

        public AigReportLeadMotorPreviousLossStoredProcedure() { }

        public AigReportLeadMotorPreviousLossStoredProcedure(AigReportLeadExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportLeadMotorPreviousLossStoredProcedureParameters
                {
                });
        }
    }
}
