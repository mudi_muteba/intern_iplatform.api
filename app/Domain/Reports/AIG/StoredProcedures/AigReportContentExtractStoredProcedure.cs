﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.Content.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportContentExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_ContentExtract";
            }
        }

        public AigReportContentExtractStoredProcedure() { }

        public AigReportContentExtractStoredProcedure(AigReportContentExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportContentExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}
