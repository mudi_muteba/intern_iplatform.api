﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.Lead.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportLeadHomePreviousLossStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_LeadPreviousHomeLoss";
            }
        }

        public AigReportLeadHomePreviousLossStoredProcedure() { }

        public AigReportLeadHomePreviousLossStoredProcedure(AigReportLeadExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportLeadHomePreviousLossStoredProcedureParameters
                {
                });
        }
    }
}
