﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.Motor.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportMotorExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_MotorExtract";
            }
        }

        public AigReportMotorExtractStoredProcedure() { }

        public AigReportMotorExtractStoredProcedure(AigReportMotorExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportMotorExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}
