﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.Lead.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportLeadExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_LeadExtract";
            }
        }

        public AigReportLeadExtractStoredProcedure() { }

        public AigReportLeadExtractStoredProcedure(AigReportLeadExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportLeadExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}
