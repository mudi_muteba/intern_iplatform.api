﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.IdentityTheft.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportIdentityTheftExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_IdentityTheftExtract";
            }
        }

        public AigReportIdentityTheftExtractStoredProcedure() { }

        public AigReportIdentityTheftExtractStoredProcedure(AigReportIdentityTheftExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportIdentityTheftExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}
