﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.Motor.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportSalesForceLogSummeryStoredProcesure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_SalesForceIntegrationLogSummeryExtract";
            }
        }

        public AigReportSalesForceLogSummeryStoredProcesure() { }

        public AigReportSalesForceLogSummeryStoredProcesure(AigReportSalesForceLogSummeryExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportSalesForceIntegrationLogSummeryExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}
