﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.Motor.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportSalesForceIntegrationExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_SalesForceIntegrationExtract";
            }
        }

        public AigReportSalesForceIntegrationExtractStoredProcedure() { }

        public AigReportSalesForceIntegrationExtractStoredProcedure(AigReportSalesForceIntegrationExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportMotorExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}
