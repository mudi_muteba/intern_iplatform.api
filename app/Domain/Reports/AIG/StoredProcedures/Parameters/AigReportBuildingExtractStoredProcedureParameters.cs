﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.AIG.StoredProcedures.Parameters
{
    public class AigReportBuildingExtractStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
