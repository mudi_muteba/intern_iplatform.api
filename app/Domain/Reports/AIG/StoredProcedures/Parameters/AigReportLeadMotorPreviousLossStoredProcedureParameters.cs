﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.AIG.StoredProcedures.Parameters
{
    public class AigReportLeadMotorPreviousLossStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int PartyId { get; set; }
    }
}
