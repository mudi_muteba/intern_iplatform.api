﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.AIG.StoredProcedures.Parameters
{
    public class AigReportLeadHomePreviousLossStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int PartyId { get; set; }
    }
}
