﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.AIG.StoredProcedures.Parameters
{
    public class AigReportLeadExtractStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int PartyId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
