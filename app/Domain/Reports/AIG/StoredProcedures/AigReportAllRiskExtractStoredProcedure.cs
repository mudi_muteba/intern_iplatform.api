﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.AIG.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.Reports.AIG.AllRisk.Criteria;

namespace Domain.Reports.AIG.StoredProcedures
{
    public class AigReportAllRiskExtractStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Reports_AIG_AllRiskExtract";
            }
        }

        public AigReportAllRiskExtractStoredProcedure() { }

        public AigReportAllRiskExtractStoredProcedure(AigReportAllRiskExtractCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AigReportAllRiskExtractStoredProcedureParameters
                {
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate
                });
        }
    }
}
