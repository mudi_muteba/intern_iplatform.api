﻿namespace Domain.Reports.AIG
{
    public class AigReportLeadQuoteMotorPreviousLossExtract
    {
        public AigReportLeadQuoteMotorPreviousLossExtract() { }

        public AigReportLeadQuoteMotorPreviousLossExtract(object[] obj)
        {
            this.PartyId = obj[1] != null ? (int)obj[1] : 0;
            this.Type = obj[2] != null ? (string)obj[2] : "";
            this.DateOfLoss = obj[3] != null ? (string)obj[3] : "";
            this.MotorLossType = obj[4] != null ? (string)obj[4] : "";
            this.MotorLossClaim = obj[5] != null ? (string)obj[5] : "";
            this.Location = "";
        }

        public virtual int PartyId { get; set; }
        public virtual string Type { get; set; }
        public virtual string DateOfLoss { get; set; }
        public virtual string MotorLossType { get; set; }
        public virtual string MotorLossClaim { get; set; }
        public virtual string Location { get; set; }

    }
}
