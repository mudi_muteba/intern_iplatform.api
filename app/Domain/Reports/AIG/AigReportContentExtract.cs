﻿using System;
using Domain.Base;

namespace Domain.Reports.AIG
{
    public class AigReportContentExtract 
    {
        public AigReportContentExtract()
        {
        }
        public AigReportContentExtract(object[] obj)
        {
            Channel = obj[0] != null ? (string)obj[0] : "";
            QuoteExpired = obj[1] != null ? (string)obj[1] : "";
            QuoteBound = obj[2] != null ? (string)obj[2] : "";
            QuoteBindDate = obj[3] != null ? (string)obj[3] : "";
            DateOfQuote = obj[4] != null ? (string)obj[4] : "";
            QuoteTotalPremium = obj[5] != null ? (decimal)obj[5] : 0;
            QuoteMotorPremium = obj[6] != null ? (decimal)obj[6] : 0;
            QuoteHomePremium = obj[7] != null ? (decimal)obj[7] : 0;
            QuoteFuneralPremium = obj[8] != null ? (decimal)obj[8] : 0;
            QuoteOtherPremium = obj[9] != null ? (decimal)obj[9] : 0;
            QuoteFees = obj[10] != null ? (decimal)obj[10] : 0;
            QuoteSasria = obj[11] != null ? (decimal)obj[11] : 0;
            QuoteIncepptionDate = obj[12] != null ? (string)obj[12] : "";
            QuotePolicyNumber = obj[13] != null ? (string)obj[13] : "";
            QuoteCreateDate = obj[14] != null ? (string)obj[14] : "";
            QuoteCreateTime = obj[15] != null ? (string)obj[15] : "";
            QuoteId = obj[16] != null ? (int)obj[16] : 0;
            Vmsaid = obj[17] != null ? (string)obj[17] : "";
            PropertyNumber = obj[18] != null ? (int)obj[18] : 0;
            CoverType = obj[19] != null ? (string)obj[19] : "";
            AddressSuburb = obj[20] != null ? (string)obj[20] : "";
            AddressPostCode = obj[21] != null ? (string)obj[21] : "";
            AddressProvince = obj[22] != null ? (string)obj[22] : "";
            HomeType = obj[23] != null ? (string)obj[23] : "";
            WallConstruction = obj[24] != null ? (string)obj[24] : "";
            RoofConstruction = obj[25] != null ? (string)obj[25] : "";
            ThatchLapa = obj[26] != null ? (string)obj[26] : "";
            ExcludingTheft = obj[27] != null ? (string)obj[27] : "";
            ThatchLapaSize = obj[28] != null ? (string)obj[28] : "";
            LightningConductor = obj[29] != null ? (string)obj[29] : "";
            PropertyBorder = obj[30] != null ? (string)obj[30] : "";
            ConstructionYear = obj[31] != null ? (string)obj[31] : "";
            SquareMetresOfProperty = obj[32] != null ? (string)obj[32] : "";
            NumberOfBathrooms = obj[33] != null ? (string)obj[33] : "";
            PropertyUnderConstruction = obj[34] != null ? (string)obj[34] : "";
            HomeUsage = obj[35] != null ? (string)obj[35] : "";
            BusinessConducted = obj[36] != null ? (string)obj[36] : "";
            Commune = obj[37] != null ? (string)obj[37] : "";
            PeriodPropertyIsUnoccupied = obj[38] != null ? (string)obj[38] : "";
            Ownership = obj[39] != null ? (string)obj[39] : "";
            SecurityGatedCommunity = obj[40] != null ? (string)obj[40] : "";
            SecuritySecurityComplex = obj[41] != null ? (string)obj[41] : "";
            SecurityBurglarBars = obj[42] != null ? (string)obj[42] : "";
            SecuritySecurityGates = obj[43] != null ? (string)obj[43] : "";
            SecurityAlarmWithArmedResponse = obj[44] != null ? (string)obj[44] : "";
            VapsAccidentalDamage = obj[45] != null ? (string)obj[45] : "";
            VapsSubsidenceAndLandslip = obj[46] != null ? (string)obj[46] : "";
            ContentsExcess = obj[47] != null ? (string)obj[47] : "";
            SumInsured = obj[48] != null ? (string)obj[48] : "";
            DiscretionaryDiscount = obj[49] != null ? (decimal)obj[49] : 0;
            Sasria = obj[50] != null ? (decimal)obj[50] : 0;
            PremiumRisk = obj[51] != null ? (decimal)obj[51] : 0;
            PremiumSubsidenceAndLandslip = obj[52] != null ? (decimal)obj[52] : 0;
            PremiumTotal = obj[53] != null ? (decimal)obj[53] : 0;
        }

        public virtual string Channel { get; set; }
        public virtual string QuoteExpired { get; set; }
        public virtual string QuoteBound { get; set; }
        public virtual string QuoteBindDate { get; set; }
        public virtual string DateOfQuote { get; set; }
        public virtual decimal QuoteTotalPremium { get; set; }
        public virtual decimal QuoteMotorPremium { get; set; }
        public virtual decimal QuoteHomePremium { get; set; }
        public virtual decimal QuoteFuneralPremium { get; set; }
        public virtual decimal QuoteOtherPremium { get; set; }
        public virtual decimal QuoteFees { get; set; }
        public virtual decimal QuoteSasria { get; set; }
        public virtual string QuoteIncepptionDate { get; set; }
        public virtual string QuotePolicyNumber { get; set; }
        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual int QuoteId { get; set; }
        public virtual string Vmsaid { get; set; }
        public virtual int PropertyNumber { get; set; }
        public virtual string CoverType { get; set; }
        public virtual string AddressSuburb { get; set; }
        public virtual string AddressPostCode { get; set; }
        public virtual string AddressProvince { get; set; }
        public virtual string HomeType { get; set; }
        public virtual string WallConstruction { get; set; }
        public virtual string RoofConstruction { get; set; }
        public virtual string ThatchLapa { get; set; }
        public virtual string ExcludingTheft { get; set; }
        public virtual string ThatchLapaSize { get; set; }
        public virtual string LightningConductor { get; set; }
        public virtual string PropertyBorder { get; set; }
        public virtual string ConstructionYear { get; set; }
        public virtual string SquareMetresOfProperty { get; set; }
        public virtual string NumberOfBathrooms { get; set; }
        public virtual string PropertyUnderConstruction { get; set; }
        public virtual string HomeUsage { get; set; }
        public virtual string BusinessConducted { get; set; }
        public virtual string Commune { get; set; }
        public virtual string PeriodPropertyIsUnoccupied { get; set; }
        public virtual string Ownership { get; set; }
        public virtual string SecurityGatedCommunity { get; set; }
        public virtual string SecuritySecurityComplex { get; set; }
        public virtual string SecurityBurglarBars { get; set; }
        public virtual string SecuritySecurityGates { get; set; }
        public virtual string SecurityAlarmWithArmedResponse { get; set; }
        public virtual string VapsAccidentalDamage { get; set; }
        public virtual string VapsSubsidenceAndLandslip { get; set; }
        public virtual string ContentsExcess { get; set; }
        public virtual string SumInsured { get; set; }
        public virtual decimal DiscretionaryDiscount { get; set; }
        public virtual decimal Sasria { get; set; }
        public virtual decimal PremiumRisk { get; set; }
        public virtual decimal PremiumSubsidenceAndLandslip { get; set; }
        public virtual decimal PremiumTotal { get; set; }
    }
}
