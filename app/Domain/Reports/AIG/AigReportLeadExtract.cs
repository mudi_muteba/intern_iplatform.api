﻿namespace Domain.Reports.AIG
{
    public class AigReportLeadExtract 
    {
        public AigReportLeadExtract() { }

        public AigReportLeadExtract(object[] obj)
        {
            Channel = obj[0] != null ? (string)obj[0] : "";
            QuoteExpired = obj[1] != null ? (string)obj[1] : "";
            QuoteBound = obj[2] != null ? (string)obj[2] : "";
            QuoteBindDate = obj[3] != null ? (string)obj[3] : "";
            QuoteTotalPremium = obj[4] != null ? (decimal)obj[4] : 0;
            QuoteMotorPremium = obj[5] != null ? (decimal)obj[5] : 0;
            QuoteHomePremium = obj[6] != null ? (decimal)obj[6] : 0;
            QuoteFuneralPremium = obj[7] != null ? (decimal)obj[7] : 0;
            QuoteOtherPremium = obj[8] != null ? (decimal)obj[8] : 0;
            QuoteFees = obj[9] != null ? (decimal)obj[9] : 0;
            QuoteSasria = obj[10] != null ? (decimal)obj[10] : 0;
            QuoteIncepptionDate = obj[11] != null ? (string)obj[11] : "";
            QuotePolicyNumber = obj[12] != null ? (string)obj[12] : "";
            QuoteCreateDate = obj[13] != null ? (string)obj[13] : "";
            QuoteCreateTime = obj[14] != null ? (string)obj[14] : "";
            QuoteId = obj[15] != null ? (int)obj[15] : 0;
            Vmsaid = obj[16] != null ? (string)obj[16] : "";
            IdNumber = obj[17] != null ? (string)obj[17] : "";
            Title = obj[18] != null ? (string)obj[18] : "";
            FirstName = obj[19] != null ? (string)obj[19] : "";
            Initials = obj[20] != null ? (string)obj[20] : "";
            Surname = obj[21] != null ? (string)obj[21] : "";
            DateOfBirth = obj[22] != null ? (string)obj[22] : "";
            Age = obj[23] != null ? (int)obj[23] : 0;
            Gender = obj[24] != null ? (string)obj[24] : "";
            MaritalStatus = obj[25] != null ? (string)obj[25] : "";
            ItcCreditScoreBand = obj[26] != null ? (string)obj[26] : "";
            Occupation = obj[27] != null ? (string)obj[27] : "";
            HomeLanguage = obj[28] != null ? (string)obj[28] : "";
            PreferredLanguage = obj[29] != null ? (string)obj[29] : "";
            EmailAddress = obj[30] != null ? (string)obj[30] : "";
            CellPhoneNumber = obj[31] != null ? (string)obj[31] : "";
            HomePhoneNumber = obj[32] != null ? (string)obj[32] : "";
            WorkPhoneNumber = obj[33] != null ? (string)obj[33] : "";
            CommPreferencePolicy = obj[34] != null ? (string)obj[34] : "";
            CommPreferenceMarketing = obj[35] != null ? (string)obj[35] : "";
            DeclarationJudgements = obj[36] != null ? (string)obj[36] : "";
            DeclarationDebtReview = obj[37] != null ? (string)obj[37] : "";
            DeclarationSequestration = obj[38] != null ? (string)obj[38] : "";
            AccountHolderName = obj[39] != null ? (string)obj[39] : "";
            AccountNumber = obj[40] != null ? (string)obj[40] : "";
            AccountType = obj[41] != null ? (string)obj[41] : "";
            BankName = obj[42] != null ? (string)obj[42] : "";
            BranchName = obj[43] != null ? (string)obj[43] : "";
            BranchCode = obj[44] != null ? (string)obj[44] : "";
            CurrentlyInsured = obj[45] != null ? (string)obj[45] : "";
            UninterruptedCover = obj[46] != null ? (string)obj[46] : "";
            CancelledInsurance = obj[47] != null ? (string)obj[47] : "";
            PartyId = obj[48] != null ? (int)obj[48] : 0;
        }

        public virtual string Channel { get; set; }
        public virtual string QuoteExpired { get; set; }
        public virtual string QuoteBound { get; set; }
        public virtual string QuoteBindDate { get; set; }
        public virtual decimal QuoteTotalPremium { get; set; }
        public virtual decimal QuoteMotorPremium { get; set; }
        public virtual decimal QuoteHomePremium { get; set; }
        public virtual decimal QuoteFuneralPremium { get; set; }
        public virtual decimal QuoteOtherPremium { get; set; }
        public virtual decimal QuoteFees { get; set; }
        public virtual decimal QuoteSasria { get; set; }
        public virtual string QuoteIncepptionDate { get; set; }
        public virtual string QuotePolicyNumber { get; set; }
        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual int QuoteId { get; set; }
        public virtual string Vmsaid { get; set; }
        public virtual string IdNumber { get; set; }
        public virtual string Title { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string Initials { get; set; }
        public virtual string Surname { get; set; }
        public virtual string DateOfBirth { get; set; }
        public virtual int Age { get; set; }
        public virtual string Gender { get; set; }
        public virtual string MaritalStatus { get; set; }
        public virtual string ItcCreditScoreBand { get; set; }
        public virtual string Occupation { get; set; }
        public virtual string HomeLanguage { get; set; }
        public virtual string PreferredLanguage { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual string CellPhoneNumber { get; set; }
        public virtual string HomePhoneNumber { get; set; }
        public virtual string WorkPhoneNumber { get; set; }
        public virtual string CommPreferencePolicy { get; set; }
        public virtual string CommPreferenceMarketing { get; set; }
        public virtual string DeclarationJudgements { get; set; }
        public virtual string DeclarationDebtReview { get; set; }
        public virtual string DeclarationSequestration { get; set; }
        public virtual string AccountHolderName { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual string AccountType { get; set; }
        public virtual string BankName { get; set; }
        public virtual string BranchName { get; set; }
        public virtual string BranchCode { get; set; }
        public virtual string CurrentlyInsured { get; set; }
        public virtual string UninterruptedCover { get; set; }
        public virtual string CancelledInsurance { get; set; }
        public virtual int PartyId { get; set; }
    }
}
