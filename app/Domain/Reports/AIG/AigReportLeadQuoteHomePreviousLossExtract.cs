﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;

namespace Domain.Reports.AIG
{
    public class AigReportLeadQuoteHomePreviousLossExtract 
    {
        public AigReportLeadQuoteHomePreviousLossExtract() { }

        public AigReportLeadQuoteHomePreviousLossExtract(object[] obj)
        {
            this.PartyId = obj[1] != null ? (int)obj[1] : 0;
            this.Type = obj[2] != null ? (string)obj[2] : "";
            this.DateOfLoss = obj[3] != null ? (string)obj[3] : "";
            this.HomeLossType = obj[4] != null ? (string)obj[4] : "";
            this.HomeLossClaim = obj[5] != null ? (string)obj[5] : "";
            this.Location = obj[6] != null ? (string)obj[6] : "";
        }

        public virtual int PartyId { get; set; }
        public virtual string Type { get; set; }
        public virtual string DateOfLoss { get; set; }
        public virtual string HomeLossType { get; set; }
        public virtual string HomeLossClaim { get; set; }
        public virtual string Location { get; set; }
    }
}
