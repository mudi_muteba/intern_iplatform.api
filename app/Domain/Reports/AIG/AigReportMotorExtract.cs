﻿using System;

namespace Domain.Reports.AIG
{
    public class AigReportMotorExtract 
    {
        public AigReportMotorExtract() { }

        public AigReportMotorExtract(object[] obj)
        {
            Channel = obj[0] != null ? (string)obj[0] : "";
            QuoteExpired = obj[1] != null ? (string)obj[1] : "";
            QuoteBound = obj[2] != null ? (string)obj[2] : "";
            QuoteBindDate = obj[3] != null ? (string)obj[3] : "";
            DateOfQuote = obj[4] != null ? (string)obj[4] : "";
            QuoteTotalPremium = obj[5] != null ? (decimal)obj[5] : 0;
            QuoteMotorPremium = obj[6] != null ? (decimal)obj[6] : 0;
            QuoteHomePremium = obj[7] != null ? (decimal)obj[7] : 0;
            QuoteFuneralPremium = obj[8] != null ? (decimal)obj[8] : 0;
            QuoteOtherPremium = obj[9] != null ? (decimal)obj[9] : 0;
            QuoteFees = obj[10] != null ? (decimal)obj[10] : 0;
            QuoteSasria = obj[11] != null ? (decimal)obj[11] : 0;
            QuoteIncepptionDate = obj[12] != null ? (string)obj[12] : "";
            QuotePolicyNumber = obj[13] != null ? (string)obj[13] : "";
            QuoteCreateDate = obj[14] != null ? (string)obj[14] : "";
            QuoteCreateTime = obj[15] != null ? (string)obj[15] : "";
            QuoteId = obj[16] != null ? (int)obj[16] : 0;
            Vmsaid = obj[17] != null ? (string)obj[17] : "";
            VehicleNumber = obj[18] != null ? (int)obj[18] : 0;
            NightAddressSuburb = obj[19] != null ? (string)obj[19] : "";
            NightAddressPostCode = obj[20] != null ? (string)obj[20] : "";
            NightAddressProvince = obj[21] != null ? (string)obj[21] : "";
            NightAddressStorage = obj[22] != null ? (string)obj[22] : "";
            DayAddressSuburb = obj[23] != null ? (string)obj[23] : "";
            DayAddressPostCode = obj[24] != null ? (string)obj[24] : "";
            DayAddressProvince = obj[25] != null ? (string)obj[25] : "";
            DayAddressStorage = obj[26] != null ? (string)obj[26] : "";
            VehicleType = obj[27] != null ? (string)obj[27] : "";
            MotorExcess = obj[28] != null ? (string)obj[28] : "";
            RegistrationNumber = obj[29] != null ? (string)obj[29] : "";
            VehicleYear = obj[30] != null ? (string)obj[30] : "";
            VehicleMake = obj[31] != null ? (string)obj[31] : "";
            VehicleModel = obj[32] != null ? (string)obj[32] : "";
            VehicleMmCode = obj[33] != null ? (string)obj[33] : "";
            VinNumber = obj[34] != null ? (string)obj[34] : "";
            Colour = obj[35] != null ? (string)obj[35] : "";
            MetallicPaint = obj[36] != null ? (string)obj[36] : "";
            CoverType = obj[37] != null ? (string)obj[37] : "";
            Modified = obj[38] != null ? (string)obj[38] : "";
            VehicleStatus = obj[39] != null ? (string)obj[39] : "";
            ClassOfUse = obj[40] != null ? (string)obj[40] : "";
            Delivery = obj[41] != null ? (string)obj[41] : "";
            TrackingDevice = obj[42] != null ? (string)obj[42] : "";
            DiscountedTrackingDevice = obj[43] != null ? (string)obj[43] : "";
            Immobiliser = obj[44] != null ? (string)obj[44] : "";
            AntiHijack = obj[45] != null ? (string)obj[45] : "";
            DataDot = obj[46] != null ? (string)obj[46] : "";
            AccSoundEquipment = obj[47] != null ? (decimal)obj[47] : 0;
            AccMagWheels = obj[48] != null ? (decimal)obj[48] : 0;
            AccSunRoof = obj[49] != null ? (decimal)obj[49] : 0;
            AccXenonLights = obj[50] != null ? (decimal)obj[50] : 0;
            AccTowBar = obj[51] != null ? (decimal)obj[51] : 0;
            AccBodyKit = obj[52] != null ? (decimal)obj[52] : 0;
            AccAntiSmashAndGrab = obj[53] != null ? (decimal)obj[53] : 0;
            AccOther = obj[54] != null ? (decimal)obj[54] : 0;
            VapCarHire = obj[55] != null ? (string)obj[55] : "";
            VapTyreAndRim = obj[56] != null ? (string)obj[56] : "";
            VapScratchAndDent = obj[57] != null ? (string)obj[57] : "";
            VapCreditShortfall = obj[58] != null ? (string)obj[58] : "";
            DriverIdNumber = obj[59] != null ? (string)obj[59] : "";
            DriverTitle = obj[60] != null ? (string)obj[60] : "";
            DriverFirstName = obj[61] != null ? (string)obj[61] : "";
            DriverSurname = obj[62] != null ? (string)obj[62] : "";
            DriverGender = obj[63] != null ? (string)obj[63] : "";
            DriverMaritalStatus = obj[64] != null ? (string)obj[64] : "";
            DriverDateOfBirth = obj[65] != null ? (string)obj[65] : "";
            DriverOccupation = obj[66] != null ? (string)obj[66] : "";
            DriverRecentCoverIndicator = obj[67] != null ? (string)obj[67] : "";
            DriverUninterruptedCover = obj[68] != null ? (string)obj[68] : "";
            DriverLicenseType = obj[69] != null ? (string)obj[69] : "";
            DriverLicenseDate = obj[70] != null ? (string)obj[70] : "";
            DriverLicenseEndorsed = obj[71] != null ? (string)obj[71] : "";
            LimitationAutomatic = obj[72] != null ? (string)obj[72] : "";
            LimitationParaplegic = obj[73] != null ? (string)obj[73] : "";
            LimitationAmputee = obj[74] != null ? (string)obj[74] : "";
            LimitationGlasses = obj[75] != null ? (string)obj[75] : "";
            Financed = obj[76] != null ? (string)obj[76] : "";
            ValuationMethod = obj[77] != null ? (string)obj[77] : "";
            SumInsured = obj[78] != null ? (decimal)obj[78] : 0;
            Sasria = obj[79] != null ? (decimal)obj[79] : 0;
            PartyId = obj[80] != null ? (int)obj[80] : 0;
        }


        public virtual string Channel { get; set; }
        public virtual string QuoteExpired { get; set; }
        public virtual string QuoteBound { get; set; }
        public virtual string QuoteBindDate { get; set; }
        public virtual string DateOfQuote { get; set; }
        public virtual decimal QuoteTotalPremium { get; set; }
        public virtual decimal QuoteMotorPremium { get; set; }
        public virtual decimal QuoteHomePremium { get; set; }
        public virtual decimal QuoteFuneralPremium { get; set; }
        public virtual decimal QuoteOtherPremium { get; set; }
        public virtual decimal QuoteFees { get; set; }
        public virtual decimal QuoteSasria { get; set; }
        public virtual string QuoteIncepptionDate { get; set; }
        public virtual string QuotePolicyNumber { get; set; }
        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual int QuoteId { get; set; }
        public virtual string Vmsaid { get; set; }
        public virtual int VehicleNumber { get; set; }
        public virtual string NightAddressSuburb { get; set; }
        public virtual string NightAddressPostCode { get; set; }
        public virtual string NightAddressProvince { get; set; }
        public virtual string NightAddressStorage { get; set; }
        public virtual string DayAddressSuburb { get; set; }
        public virtual string DayAddressPostCode { get; set; }
        public virtual string DayAddressProvince { get; set; }
        public virtual string DayAddressStorage { get; set; }
        public virtual string VehicleType { get; set; }
        public virtual string MotorExcess { get; set; }
        public virtual string RegistrationNumber { get; set; }
        public virtual string VehicleYear { get; set; }
        public virtual string VehicleMake { get; set; }
        public virtual string VehicleModel { get; set; }
        public virtual string VehicleMmCode { get; set; }
        public virtual string VinNumber { get; set; }
        public virtual string Colour { get; set; }
        public virtual string MetallicPaint { get; set; }
        public virtual string CoverType { get; set; }
        public virtual string Modified { get; set; }
        public virtual string VehicleStatus { get; set; }
        public virtual string ClassOfUse { get; set; }
        public virtual string Delivery { get; set; }
        public virtual string TrackingDevice { get; set; }
        public virtual string DiscountedTrackingDevice { get; set; }
        public virtual string Immobiliser { get; set; }
        public virtual string AntiHijack { get; set; }
        public virtual string DataDot { get; set; }
        public virtual decimal AccSoundEquipment { get; set; }
        public virtual decimal AccMagWheels { get; set; }
        public virtual decimal AccSunRoof { get; set; }
        public virtual decimal AccXenonLights { get; set; }
        public virtual decimal AccTowBar { get; set; }
        public virtual decimal AccBodyKit { get; set; }
        public virtual decimal AccAntiSmashAndGrab { get; set; }
        public virtual decimal AccOther { get; set; }
        public virtual string VapCarHire { get; set; }
        public virtual string VapTyreAndRim { get; set; }
        public virtual string VapScratchAndDent { get; set; }
        public virtual string VapCreditShortfall { get; set; }
        public virtual string DriverIdNumber { get; set; }
        public virtual string DriverTitle { get; set; }
        public virtual string DriverFirstName { get; set; }
        public virtual string DriverSurname { get; set; }
        public virtual string DriverGender { get; set; }
        public virtual string DriverMaritalStatus { get; set; }
        public virtual string DriverDateOfBirth { get; set; }
        public virtual string DriverOccupation { get; set; }
        public virtual string DriverRecentCoverIndicator { get; set; }
        public virtual string DriverUninterruptedCover { get; set; }
        public virtual string DriverLicenseType { get; set; }
        public virtual string DriverLicenseDate { get; set; }
        public virtual string DriverLicenseEndorsed { get; set; }
        public virtual string LimitationAutomatic { get; set; }
        public virtual string LimitationParaplegic { get; set; }
        public virtual string LimitationAmputee { get; set; }
        public virtual string LimitationGlasses { get; set; }
        public virtual string Financed { get; set; }
        public virtual string ValuationMethod { get; set; }
        public virtual decimal SumInsured { get; set; }
        public virtual decimal Sasria { get; set; }
        public virtual int PartyId { get; set; }
    }
}
