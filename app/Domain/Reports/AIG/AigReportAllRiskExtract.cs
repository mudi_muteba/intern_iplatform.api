﻿using System;
using Domain.Base;

namespace Domain.Reports.AIG
{
    public class AigReportAllRiskExtract
    {
        public AigReportAllRiskExtract()
        {
        }
        public AigReportAllRiskExtract(object[] obj)
        {
            this.AgentName = obj[0] != null ? (string)obj[0] : "";
            this.Channel = obj[1] != null ? (string)obj[1] : "";
            this.DateOfQuote = obj[2] != null ? (string)obj[2] : "";
            this.QuoteExpired = obj[3] != null ? (string)obj[3] : "";
            this.QuoteBound = obj[4] != null ? (string)obj[4] : "";
            this.QuoteBindDate = obj[5] != null ? (string)obj[5] : "";
            this.QuoteTotalPremium = obj[6] != null ? (decimal)obj[6] : 0;
            this.QuoteMotorPremium = obj[7] != null ? (decimal)obj[7] : 0;
            this.QuoteHomePremium = obj[8] != null ? (decimal)obj[8] : 0;
            this.QuoteFuneralPremium = obj[9] != null ? (decimal)obj[9] : 0;
            this.QuoteOtherPremium = obj[10] != null ? (decimal)obj[10] : 0;
            this.QuoteIncepptionDate = obj[11] != null ? (string)obj[11] : "";
            this.QuotePolicyNumber = obj[12] != null ? (string)obj[12] : "";
            this.QuoteCreateDate = obj[13] != null ? (string)obj[13] : "";
            this.QuoteCreateTime = obj[14] != null ? (string)obj[14] : "";
            this.LeadText = obj[15] != null ? (int)obj[15] : 0;
            this.PropertyNumber = obj[16] != null ? (string)obj[16] : "";
            this.ItemNumber = obj[17] != null ? (int)obj[17] : 0;
            this.AddressSuburb = obj[18] != null ? (string)obj[18] : "";
            this.AddressPostCode = obj[19] != null ? (string)obj[19] : "";
            this.AddressProvince = obj[20] != null ? (string)obj[20] : "";
            this.ItemCategory = obj[21] != null ? (string)obj[21] : "";
            this.ItemDescripption = obj[22] != null ? (string)obj[22] : "";
            this.ItemJewellerySafe = obj[23] != null ? (string)obj[23] : "";
            this.ItemReceiptsForItems = obj[24] != null ? (string)obj[24] : "";
            this.ItemSerialNumber = obj[25] != null ? (string)obj[25] : "";
            this.ItemDiscount = obj[26] != null ? (string)obj[26] : "";
            this.ItemSumInsured = obj[27] != null ? (string)obj[27] :"0";
        }

        public virtual string AgentName { get; set; }
        public virtual string Channel { get; set; }
        public virtual string DateOfQuote { get; set; }
        public virtual string QuoteExpired { get; set; }
        public virtual string QuoteBound { get; set; }
        public virtual string QuoteBindDate { get; set; }
        public virtual decimal QuoteTotalPremium { get; set; }
        public virtual decimal QuoteMotorPremium { get; set; }
        public virtual decimal QuoteHomePremium { get; set; }
        public virtual decimal QuoteFuneralPremium { get; set; }
        public virtual decimal QuoteOtherPremium { get; set; }
        public virtual string QuoteIncepptionDate { get; set; }
        public virtual string QuotePolicyNumber { get; set; }
        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual int LeadText { get; set; }
        public virtual string PropertyNumber { get; set; }
        public virtual int ItemNumber { get; set; }
        public virtual string AddressSuburb { get; set; }
        public virtual string AddressPostCode { get; set; }
        public virtual string AddressProvince { get; set; }
        public virtual string ItemCategory { get; set; }
        public virtual string ItemDescripption { get; set; }
        public virtual string ItemJewellerySafe { get; set; }
        public virtual string ItemReceiptsForItems { get; set; }
        public virtual string ItemSerialNumber { get; set; }
        public virtual string ItemDiscount { get; set; }
        public virtual string ItemSumInsured { get; set; }
    }
}

