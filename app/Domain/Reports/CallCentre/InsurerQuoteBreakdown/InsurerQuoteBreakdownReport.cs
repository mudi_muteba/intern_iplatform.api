﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerQuoteBreakdown;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown
{
    public class InsurerQuoteBreakdownReport : ReportGenerator
    {
        public InsurerQuoteBreakdownReport() : base() { }

        public virtual InsurerQuoteBreakdownReportContainerDto Get(string campaignIds, DateTime dateFrom, DateTime dateTo)
        {
            var criteria = new CallCentreReportCriteriaDto
            {
                CampaignIds = campaignIds,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var response = Connector.ReportManagement.CallCentreReportConnector.GetInsurerQuoteBreakdownReport(criteria).Response;
            return response;
        }
    }
}
