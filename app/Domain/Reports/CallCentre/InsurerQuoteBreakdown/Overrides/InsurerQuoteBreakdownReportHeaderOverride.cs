﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.Overrides
{
    public class InsurerQuoteBreakdownReportHeaderOverride : IAutoMappingOverride<InsurerQuoteBreakdownReportHeader>
    {
        public void Override(AutoMapping<InsurerQuoteBreakdownReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
