﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.Overrides
{
    public class InsurerQuoteBreakdownReportInsurerQuoteStatisticsOverride : IAutoMappingOverride<InsurerQuoteBreakdownReportInsurerQuoteStatistics>
    {
        public void Override(AutoMapping<InsurerQuoteBreakdownReportInsurerQuoteStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
