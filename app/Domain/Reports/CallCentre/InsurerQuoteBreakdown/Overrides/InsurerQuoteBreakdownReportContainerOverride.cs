﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.Overrides
{
    public class InsurerQuoteBreakdownReportContainerOverride : IAutoMappingOverride<InsurerQuoteBreakdownReportContainer>
    {
        public void Override(AutoMapping<InsurerQuoteBreakdownReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
