﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown
{
    public class InsurerQuoteBreakdownReportContainer : Entity
    {
        public InsurerQuoteBreakdownReportContainer() { }

        public InsurerQuoteBreakdownReportContainer(InsurerQuoteBreakdownReportHeader header, List<InsurerQuoteBreakdownReportInsurerQuoteStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual InsurerQuoteBreakdownReportHeader Header { get; set; }
        public virtual List<InsurerQuoteBreakdownReportInsurerQuoteStatistics> Statistics { get; set; }
    }
}
