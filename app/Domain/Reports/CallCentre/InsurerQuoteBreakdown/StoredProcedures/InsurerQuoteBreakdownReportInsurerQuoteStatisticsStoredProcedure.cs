﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.InsurerQuoteBreakdown.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.StoredProcedures
{
    public class InsurerQuoteBreakdownReportInsurerQuoteStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics";
            }
        }

        public InsurerQuoteBreakdownReportInsurerQuoteStatisticsStoredProcedure() { }

        public InsurerQuoteBreakdownReportInsurerQuoteStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new InsurerQuoteBreakdownReportInsurerQuoteStatisticsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}