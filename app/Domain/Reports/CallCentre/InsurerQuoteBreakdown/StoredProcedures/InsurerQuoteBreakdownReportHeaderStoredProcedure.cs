﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.InsurerQuoteBreakdown.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.StoredProcedures
{
    public class InsurerQuoteBreakdownReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_InsurerQuoteBreakdown_Header";
            }
        }

        public InsurerQuoteBreakdownReportHeaderStoredProcedure() { }

        public InsurerQuoteBreakdownReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new InsurerQuoteBreakdownReportHeaderStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId,
                    CampaignIds = criteria.CampaignIds
                });
        }
    }
}