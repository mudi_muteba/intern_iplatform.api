﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.StoredProcedures.Parameters
{
    public class InsurerQuoteBreakdownReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int CampaignId { get; set; }
        public string CampaignIds { get; set; }
    }
}