﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.StoredProcedures.Parameters
{
    public class InsurerQuoteBreakdownReportInsurerQuoteStatisticsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string CampaignIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}