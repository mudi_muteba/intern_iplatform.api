﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerQuoteBreakdown.StoredProcedures;
using Domain.Reports.CallCentre.InsurerQuoteBreakdown.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.Queries
{
    public class GetInsurerQuoteBreakdownReportInsurerQuoteStatisticsQuery : BaseStoredProcedureQuery<InsurerQuoteBreakdownReportInsurerQuoteStatistics, InsurerQuoteBreakdownReportInsurerQuoteStatisticsStoredProcedure>
    {
        public GetInsurerQuoteBreakdownReportInsurerQuoteStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerQuoteBreakdownReportInsurerQuoteStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerQuoteBreakdownReportInsurerQuoteStatistics> Execute(IList<InsurerQuoteBreakdownReportInsurerQuoteStatistics> query)
        {
            return query;
        }
    }
}
