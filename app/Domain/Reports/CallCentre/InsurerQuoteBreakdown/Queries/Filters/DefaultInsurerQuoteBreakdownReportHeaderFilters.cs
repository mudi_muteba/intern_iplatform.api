﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.Queries.Filters
{
    public class DefaultInsurerQuoteBreakdownReportHeaderFilters : IApplyDefaultFilters<InsurerQuoteBreakdownReportHeader>
    {
        public IQueryable<InsurerQuoteBreakdownReportHeader> Apply(ExecutionContext executionContext, IQueryable<InsurerQuoteBreakdownReportHeader> query)
        {
            return query;
        }
    }
}