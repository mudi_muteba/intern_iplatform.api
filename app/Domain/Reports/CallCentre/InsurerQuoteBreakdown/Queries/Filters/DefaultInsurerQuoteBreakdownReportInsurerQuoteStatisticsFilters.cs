﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.Queries.Filters
{
    public class DefaultInsurerQuoteBreakdownReportInsurerQuoteStatisticsFilters : IApplyDefaultFilters<InsurerQuoteBreakdownReportInsurerQuoteStatistics>
    {
        public IQueryable<InsurerQuoteBreakdownReportInsurerQuoteStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerQuoteBreakdownReportInsurerQuoteStatistics> query)
        {
            return query;
        }
    }
}