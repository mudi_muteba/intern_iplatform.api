﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown
{
    public class InsurerQuoteBreakdownReportHeader : Entity
    {
        public InsurerQuoteBreakdownReportHeader() { }

        public InsurerQuoteBreakdownReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
