﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerQuoteBreakdown;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.Mappings
{
    public class InsurerQuoteBreakdownReportHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<InsurerQuoteBreakdownReportHeader, InsurerQuoteBreakdownReportHeaderDto>();

            Mapper.CreateMap<QueryResult<InsurerQuoteBreakdownReportHeaderDto>, POSTResponseDto<InsurerQuoteBreakdownReportHeaderDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<InsurerQuoteBreakdownReportHeader>>, POSTResponseDto<IList<InsurerQuoteBreakdownReportHeaderDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}