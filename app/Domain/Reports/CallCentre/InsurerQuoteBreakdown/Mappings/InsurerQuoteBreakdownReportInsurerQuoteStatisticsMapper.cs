﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerQuoteBreakdown;

namespace Domain.Reports.CallCentre.InsurerQuoteBreakdown.Mappings
{
    public class InsurerQuoteBreakdownReportInsurerQuoteStatisticsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<InsurerQuoteBreakdownReportInsurerQuoteStatistics, InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto>();

            Mapper.CreateMap<QueryResult<InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto>, POSTResponseDto<InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<InsurerQuoteBreakdownReportInsurerQuoteStatistics>>, POSTResponseDto<IList<InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}