﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.AgentPerformance
{
    public class AgentPerformanceReportHeader : Entity
    {
        public AgentPerformanceReportHeader() { }

        public AgentPerformanceReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
