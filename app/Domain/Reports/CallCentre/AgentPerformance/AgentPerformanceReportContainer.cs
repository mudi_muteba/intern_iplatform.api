﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.AgentPerformance
{
    public class AgentPerformanceReportContainer : Entity
    {
        public AgentPerformanceReportContainer() { }

        public AgentPerformanceReportContainer(AgentPerformanceReportHeader header, List<AgentPerformanceReportAgentQuoteStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual AgentPerformanceReportHeader Header { get; set; }
        public virtual List<AgentPerformanceReportAgentQuoteStatistics> Statistics { get; set; }
    }
}
