﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformance;

namespace Domain.Reports.CallCentre.AgentPerformance.Mappings
{
    public class AgentPerformanceReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AgentPerformanceReportContainer, AgentPerformanceReportContainerDto>();

            Mapper.CreateMap<QueryResult<AgentPerformanceReportContainerDto>, POSTResponseDto<AgentPerformanceReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<AgentPerformanceReportContainer>>, POSTResponseDto<IList<AgentPerformanceReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}