﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformance;

namespace Domain.Reports.CallCentre.AgentPerformance.Mappings
{
    public class AgentPerformanceReportHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AgentPerformanceReportHeader, AgentPerformanceReportHeaderDto>();

            Mapper.CreateMap<QueryResult<AgentPerformanceReportHeaderDto>, POSTResponseDto<AgentPerformanceReportHeaderDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<AgentPerformanceReportHeader>>, POSTResponseDto<IList<AgentPerformanceReportHeaderDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}