﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformance;

namespace Domain.Reports.CallCentre.AgentPerformance
{
    public class AgentPerformanceReport : ReportGenerator
    {
        public AgentPerformanceReport() : base() { }

        public virtual AgentPerformanceReportContainerDto Get(string campaignIds, DateTime dateFrom, DateTime dateTo)
        {
            var criteria = new CallCentreReportCriteriaDto
            {
                CampaignIds = campaignIds,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var response = Connector.ReportManagement.CallCentreReportConnector.GetAgentPerformanceReport(criteria).Response;
            return response;
        }
    }
}
