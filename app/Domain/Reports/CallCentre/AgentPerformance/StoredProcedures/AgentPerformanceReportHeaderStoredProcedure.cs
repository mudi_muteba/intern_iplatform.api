﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.AgentPerformance.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.AgentPerformance.StoredProcedures
{
    public class AgentPerformanceReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_AgentPerformance_Header";
            }
        }

        public AgentPerformanceReportHeaderStoredProcedure() { }

        public AgentPerformanceReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AgentPerformanceReportHeaderStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId,
                    CampaignIds = criteria.CampaignIds
                });
        }
    }
}