﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.AgentPerformance.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.AgentPerformance.StoredProcedures
{
    public class AgentPerformanceReportAgentQuoteStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics";
            }
        }

        public AgentPerformanceReportAgentQuoteStatisticsStoredProcedure() { }

        public AgentPerformanceReportAgentQuoteStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AgentPerformanceReportAgentQuoteStatisticsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}