﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.AgentPerformance.StoredProcedures.Parameters
{
    public class AgentPerformanceReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string CampaignIds { get; set; }
        public int CampaignId { get; set; }
    }
}