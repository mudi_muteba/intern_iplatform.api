﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.AgentPerformance.StoredProcedures.Parameters
{
    public class AgentPerformanceReportAgentQuoteStatisticsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string CampaignIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}