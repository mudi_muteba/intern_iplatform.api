﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.AgentPerformance.Overrides
{
    public class AgentPerformanceReportHeaderOverride : IAutoMappingOverride<AgentPerformanceReportHeader>
    {
        public void Override(AutoMapping<AgentPerformanceReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
