﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.AgentPerformance.Overrides
{
    public class AgentPerformanceReportContainerOverride : IAutoMappingOverride<AgentPerformanceReportContainer>
    {
        public void Override(AutoMapping<AgentPerformanceReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
