﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.AgentPerformance.Overrides
{
    public class AgentPerformanceReportAgentQuoteStatisticsOverride : IAutoMappingOverride<AgentPerformanceReportAgentQuoteStatistics>
    {
        public void Override(AutoMapping<AgentPerformanceReportAgentQuoteStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
