﻿using System.Collections.Generic;

using MasterData.Authorisation;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.AgentPerformance.StoredProcedures;
using Domain.Reports.CallCentre.AgentPerformance.Queries.Filters;

namespace Domain.Reports.CallCentre.AgentPerformance.Queries
{
    public class GetAgentPerformanceReportHeaderQuery : BaseStoredProcedureQuery<AgentPerformanceReportHeader, AgentPerformanceReportHeaderStoredProcedure>
    {
        public GetAgentPerformanceReportHeaderQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultAgentPerformanceReportHeaderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AgentPerformanceReportHeader> Execute(IList<AgentPerformanceReportHeader> query)
        {
            return query;
        }
    }
}
