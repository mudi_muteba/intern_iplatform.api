﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.AgentPerformance.Queries.Filters
{
    public class DefaultAgentPerformanceReportHeaderFilters : IApplyDefaultFilters<AgentPerformanceReportHeader>
    {
        public IQueryable<AgentPerformanceReportHeader> Apply(ExecutionContext executionContext, IQueryable<AgentPerformanceReportHeader> query)
        {
            return query;
        }
    }
}