﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.AgentPerformance.Queries.Filters
{
    public class DefaultAgentPerformanceReportAgentQuoteStatisticsFilters : IApplyDefaultFilters<AgentPerformanceReportAgentQuoteStatistics>
    {
        public IQueryable<AgentPerformanceReportAgentQuoteStatistics> Apply(ExecutionContext executionContext, IQueryable<AgentPerformanceReportAgentQuoteStatistics> query)
        {
            return query;
        }
    }
}