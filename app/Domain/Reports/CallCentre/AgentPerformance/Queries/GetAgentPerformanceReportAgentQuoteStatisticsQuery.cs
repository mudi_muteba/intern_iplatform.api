﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.AgentPerformance.StoredProcedures;
using Domain.Reports.CallCentre.AgentPerformance.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.AgentPerformance.Queries
{
    public class GetAgentPerformanceReportAgentQuoteStatisticsQuery : BaseStoredProcedureQuery<AgentPerformanceReportAgentQuoteStatistics, AgentPerformanceReportAgentQuoteStatisticsStoredProcedure>
    {
        public GetAgentPerformanceReportAgentQuoteStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultAgentPerformanceReportAgentQuoteStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AgentPerformanceReportAgentQuoteStatistics> Execute(IList<AgentPerformanceReportAgentQuoteStatistics> query)
        {
            return query;
        }
    }
}
