﻿using Domain.Base;

namespace Domain.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportCampaignProducts : Entity
    {
        public InsurerDataReportCampaignProducts() { }

        public InsurerDataReportCampaignProducts(object[] obj)
        {
            this.ProductOwnerId = obj[0] != null ? (int) obj[0] : 0;
            this.ProductOwnerName = obj[1] != null ? (string) obj[1] : "";
            this.ProductProviderId = obj[2] != null ? (int) obj[2] : 0;
            this.ProductProviderName = obj[3] != null ? (string) obj[3] : "";
            this.ProductId = obj[4] != null ? (int) obj[4] : 0;
            this.ProductName = obj[5] != null ? (string) obj[5] : "";
        }

        public virtual int ProductOwnerId { get; set; }
        public virtual string ProductOwnerName { get; set; }
        public virtual int ProductProviderId { get; set; }
        public virtual string ProductProviderName { get; set; }
        public virtual int ProductId { get; set; }
        public virtual string ProductName { get; set; }
    }
}
