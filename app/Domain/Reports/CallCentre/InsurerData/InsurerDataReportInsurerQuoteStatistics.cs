﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportInsurerQuoteStatistics : Entity
    {
        public InsurerDataReportInsurerQuoteStatistics() { }

        public InsurerDataReportInsurerQuoteStatistics(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
            this.Product = obj[1] != null ? (string) obj[1] : "";
            this.Campaign = obj[2] != null ? (string) obj[2] : "";

            this.Quotes = obj[3] != null ? (int) obj[3] : 0;
            this.Sales = obj[4] != null ? (int) obj[4] : 0;
            this.Closing = obj[5] != null ? (decimal) obj[5] : 0;
            this.AveragePremiumPerAcceptedQuote = obj[6] != null ? (decimal) obj[6] : 0;
            this.AveragePremiumPerQuote = obj[7] != null ? (decimal) obj[7] : 0;
        }

        public virtual string Insurer { get; set; }
        public virtual string Product { get; set; }
        public virtual string Campaign { get; set; }

        public virtual int Quotes { get; set; }
        public virtual int Sales { get; set; }
        public virtual decimal Closing { get; set; }
        public virtual decimal AveragePremiumPerAcceptedQuote { get; set; }
        public virtual decimal AveragePremiumPerQuote { get; set; }

        public virtual int ProductId { get; set; }
    }
}
