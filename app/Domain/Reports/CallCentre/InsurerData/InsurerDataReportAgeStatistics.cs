﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportAgeStatistics : Entity
    {
        public InsurerDataReportAgeStatistics() { }

        public InsurerDataReportAgeStatistics(object[] obj)
        {
            this.Label = obj[0] != null ? (string) obj[0] : "";
            this.Category = obj[1] != null ? (string) obj[1] : "";
            this.Total = obj[2] != null ? (decimal) obj[2] : 0;
            this.Percentage = obj[3] != null ? (decimal) obj[3] : 0;
        }

        public virtual string Label { get; set; }
        public virtual string Category { get; set; }
        public virtual decimal Total { get; set; }
        public virtual decimal Percentage { get; set; }
    }
}
