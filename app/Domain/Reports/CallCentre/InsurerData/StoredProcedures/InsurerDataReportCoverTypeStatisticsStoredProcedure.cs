﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.InsurerData.StoredProcedures
{
    public class InsurerDataReportCoverTypeStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_InsurerData_GetCoverTypeStatistics";
            }
        }

        public InsurerDataReportCoverTypeStatisticsStoredProcedure() { }

        public InsurerDataReportCoverTypeStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new InsurerDataReportCoverTypeStatisticsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    ProductIds = criteria.ProductIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}