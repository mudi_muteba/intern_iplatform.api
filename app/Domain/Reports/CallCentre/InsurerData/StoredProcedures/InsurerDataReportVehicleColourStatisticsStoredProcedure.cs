﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.InsurerData.StoredProcedures
{
    public class InsurerDataReportVehicleColourStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_InsurerData_GetVehicleColourStatistics";
            }
        }

        public InsurerDataReportVehicleColourStatisticsStoredProcedure() { }

        public InsurerDataReportVehicleColourStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new InsurerDataReportVehicleColourStatisticsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    ProductIds = criteria.ProductIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}