﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.InsurerData.StoredProcedures
{
    public class InsurerDataReportCampaignProductsByCoverId : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_InsurerData_GetCampaignProductsByCoverId";
            }
        }

        public InsurerDataReportCampaignProductsByCoverId() { }

        public InsurerDataReportCampaignProductsByCoverId(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new InsurerDataReportCampaignProductsByCoverIdStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    CoverId = criteria.CoverId,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}