﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.InsurerData.StoredProcedures
{
    public class InsurerDataReportVehicleMakeStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_InsurerData_GetVehicleMakeStatistics";
            }
        }

        public InsurerDataReportVehicleMakeStatisticsStoredProcedure() { }

        public InsurerDataReportVehicleMakeStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new InsurerDataReportVehicleMakeStatisticsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    ProductIds = criteria.ProductIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}