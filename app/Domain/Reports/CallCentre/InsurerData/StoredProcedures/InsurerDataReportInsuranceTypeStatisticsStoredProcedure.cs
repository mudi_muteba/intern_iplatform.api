﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.InsurerData.StoredProcedures
{
    public class InsurerDataReportInsuranceTypeStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_InsurerData_GetInsuranceTypeStatistics";
            }
        }

        public InsurerDataReportInsuranceTypeStatisticsStoredProcedure() { }

        public InsurerDataReportInsuranceTypeStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new InsurerDataReportInsuranceTypeStatisticsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    ProductIds = criteria.ProductIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}