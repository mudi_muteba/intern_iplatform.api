﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.InsurerData.StoredProcedures.Parameters
{
    public class InsurerDataReportClassUseStatisticsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string CampaignIds { get; set; }
        public string ProductIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}