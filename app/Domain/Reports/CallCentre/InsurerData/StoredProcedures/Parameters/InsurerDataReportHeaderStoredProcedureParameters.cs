﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.InsurerData.StoredProcedures.Parameters
{
    public class InsurerDataReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int CampaignId { get; set; }
        public string CampaignIds { get; set; }
    }
}