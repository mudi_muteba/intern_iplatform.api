﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.InsurerData.StoredProcedures
{
    public class InsurerDataReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_InsurerData_Header";
            }
        }

        public InsurerDataReportHeaderStoredProcedure() { }

        public InsurerDataReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new InsurerDataReportHeaderStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId,
                    CampaignIds = criteria.CampaignIds
                });
        }
    }
}