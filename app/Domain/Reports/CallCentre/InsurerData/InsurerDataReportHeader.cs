﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportHeader : Entity
    {
        public InsurerDataReportHeader() { }

        public InsurerDataReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
