﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerData;

namespace Domain.Reports.CallCentre.InsurerData.Mappings
{
    public class InsurerDataReportHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<InsurerDataReportHeader, InsurerDataReportHeaderDto>();

            Mapper.CreateMap<QueryResult<InsurerDataReportHeaderDto>, POSTResponseDto<InsurerDataReportHeaderDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<InsurerDataReportHeader>>, POSTResponseDto<IList<InsurerDataReportHeaderDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}