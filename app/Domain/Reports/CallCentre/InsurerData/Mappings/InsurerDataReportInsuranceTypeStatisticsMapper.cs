﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerData;

namespace Domain.Reports.CallCentre.InsurerData.Mappings
{
    public class InsurerDataReportInsuranceTypeStatisticsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<InsurerDataReportInsuranceTypeStatistics, InsurerDataReportInsuranceTypeStatisticsDto>();

            Mapper.CreateMap<QueryResult<InsurerDataReportInsuranceTypeStatisticsDto>, POSTResponseDto<InsurerDataReportInsuranceTypeStatisticsDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<InsurerDataReportInsuranceTypeStatistics>>, POSTResponseDto<IList<InsurerDataReportInsuranceTypeStatisticsDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}