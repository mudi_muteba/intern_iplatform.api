﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportGeneralStatistics : Entity
    {
        public InsurerDataReportGeneralStatistics() { }

        public InsurerDataReportGeneralStatistics(object[] obj)
        {
            this.AverageNoClaimBonus = obj[0] != null ? (decimal) obj[0] : 0;
            this.AverageSumInsured = obj[1] != null ? obj[1].ToString() : "0.00";
        }

        public virtual decimal AverageNoClaimBonus { get; set; }
        public virtual string AverageSumInsured { get; set; }   
    }
}