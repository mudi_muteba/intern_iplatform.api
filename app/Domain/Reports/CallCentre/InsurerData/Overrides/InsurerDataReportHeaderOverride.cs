﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.InsurerData.Overrides
{
    public class InsurerDataReportHeaderOverride : IAutoMappingOverride<InsurerDataReportHeader>
    {
        public void Override(AutoMapping<InsurerDataReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
