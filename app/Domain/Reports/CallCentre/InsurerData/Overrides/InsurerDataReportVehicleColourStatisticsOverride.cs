﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.InsurerData.Overrides
{
    public class InsurerDataReportVehicleColourStatisticsOverride : IAutoMappingOverride<InsurerDataReportInsurerQuoteStatistics>
    {
        public void Override(AutoMapping<InsurerDataReportInsurerQuoteStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
