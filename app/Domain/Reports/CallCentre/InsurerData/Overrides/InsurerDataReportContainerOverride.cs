﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.InsurerData.Overrides
{
    public class InsurerDataReportContainerOverride : IAutoMappingOverride<InsurerDataReportContainer>
    {
        public void Override(AutoMapping<InsurerDataReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
