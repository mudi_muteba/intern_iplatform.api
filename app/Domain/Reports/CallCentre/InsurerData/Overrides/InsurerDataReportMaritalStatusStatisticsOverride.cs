﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.InsurerData.Overrides
{
    public class InsurerDataReportMaritalStatusStatisticsOverride : IAutoMappingOverride<InsurerDataReportMaritalStatusStatistics>
    {
        public void Override(AutoMapping<InsurerDataReportMaritalStatusStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
