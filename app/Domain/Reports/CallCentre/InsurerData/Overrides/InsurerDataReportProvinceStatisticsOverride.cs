﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.InsurerData.Overrides
{
    public class InsurerDataReportProvinceStatisticsOverride : IAutoMappingOverride<InsurerDataReportProvinceStatistics>
    {
        public void Override(AutoMapping<InsurerDataReportProvinceStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
