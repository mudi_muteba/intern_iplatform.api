﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerData.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.InsurerData.Queries
{
    public class GetInsurerDataReportVehicleColourStatisticsQuery : BaseStoredProcedureQuery<InsurerDataReportVehicleColourStatistics, InsurerDataReportVehicleColourStatisticsStoredProcedure>
    {
        public GetInsurerDataReportVehicleColourStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerDataReportVehicleColourStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerDataReportVehicleColourStatistics> Execute(IList<InsurerDataReportVehicleColourStatistics> query)
        {
            return query;
        }
    }
}
