﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerData.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.InsurerData.Queries
{
    public class GetInsurerDataReportGenderStatisticsQuery : BaseStoredProcedureQuery<InsurerDataReportGenderStatistics, InsurerDataReportGenderStatisticsStoredProcedure>
    {
        public GetInsurerDataReportGenderStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerDataReportGenderStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerDataReportGenderStatistics> Execute(IList<InsurerDataReportGenderStatistics> query)
        {
            return query;
        }
    }
}
