﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerData.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.InsurerData.Queries
{
    public class GetInsurerDataReportCoverTypeStatisticsQuery : BaseStoredProcedureQuery<InsurerDataReportCoverTypeStatistics, InsurerDataReportCoverTypeStatisticsStoredProcedure>
    {
        public GetInsurerDataReportCoverTypeStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerDataReportCoverTypeStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerDataReportCoverTypeStatistics> Execute(IList<InsurerDataReportCoverTypeStatistics> query)
        {
            return query;
        }
    }
}
