﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerData.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.InsurerData.Queries
{
    public class GetInsurerDataReportMaritalStatusStatisticsQuery : BaseStoredProcedureQuery<InsurerDataReportMaritalStatusStatistics, InsurerDataReportMaritalStatusStatisticsStoredProcedure>
    {
        public GetInsurerDataReportMaritalStatusStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerDataReportMaritalStatusStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerDataReportMaritalStatusStatistics> Execute(IList<InsurerDataReportMaritalStatusStatistics> query)
        {
            return query;
        }
    }
}
