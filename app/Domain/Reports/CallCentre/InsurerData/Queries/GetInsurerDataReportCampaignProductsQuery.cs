﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerData.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.InsurerData.Queries
{
    public class GetInsurerDataReportCampaignProductsQuery : BaseStoredProcedureQuery<InsurerDataReportCampaignProducts, InsurerDataReportCampaignProductsByCoverId>
    {
        public GetInsurerDataReportCampaignProductsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerDataReportCampaignProductsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerDataReportCampaignProducts> Execute(IList<InsurerDataReportCampaignProducts> query)
        {
            return query;
        }
    }
}
