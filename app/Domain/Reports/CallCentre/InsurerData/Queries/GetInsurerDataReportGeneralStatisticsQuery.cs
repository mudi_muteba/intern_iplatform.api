﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerData.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.InsurerData.Queries
{
    public class GetInsurerDataReportGeneralStatisticsQuery : BaseStoredProcedureQuery<InsurerDataReportGeneralStatistics, InsurerDataReportGeneralStatisticsStoredProcedure>
    {
        public GetInsurerDataReportGeneralStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerDataReportGeneralStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerDataReportGeneralStatistics> Execute(IList<InsurerDataReportGeneralStatistics> query)
        {
            return query;
        }
    }
}
