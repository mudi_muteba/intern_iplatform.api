﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerData.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.InsurerData.Queries
{
    public class GetInsurerDataReportProvinceStatisticsQuery : BaseStoredProcedureQuery<InsurerDataReportProvinceStatistics, InsurerDataReportProvinceStatisticsStoredProcedure>
    {
        public GetInsurerDataReportProvinceStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerDataReportProvinceStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerDataReportProvinceStatistics> Execute(IList<InsurerDataReportProvinceStatistics> query)
        {
            return query;
        }
    }
}
