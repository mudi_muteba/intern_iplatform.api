﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportInsuranceTypeStatisticsFilters : IApplyDefaultFilters<InsurerDataReportInsuranceTypeStatistics>
    {
        public IQueryable<InsurerDataReportInsuranceTypeStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportInsuranceTypeStatistics> query)
        {
            return query;
        }
    }
}