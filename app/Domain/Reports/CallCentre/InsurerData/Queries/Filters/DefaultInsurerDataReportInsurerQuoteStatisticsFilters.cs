﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportInsurerQuoteStatisticsFilters : IApplyDefaultFilters<InsurerDataReportInsurerQuoteStatistics>
    {
        public IQueryable<InsurerDataReportInsurerQuoteStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportInsurerQuoteStatistics> query)
        {
            return query;
        }
    }
}