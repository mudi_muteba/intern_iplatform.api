﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportAgeStatisticsFilters : IApplyDefaultFilters<InsurerDataReportAgeStatistics>
    {
        public IQueryable<InsurerDataReportAgeStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportAgeStatistics> query)
        {
            return query;
        }
    }
}