﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportProvinceStatisticsFilters : IApplyDefaultFilters<InsurerDataReportProvinceStatistics>
    {
        public IQueryable<InsurerDataReportProvinceStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportProvinceStatistics> query)
        {
            return query;
        }
    }
}