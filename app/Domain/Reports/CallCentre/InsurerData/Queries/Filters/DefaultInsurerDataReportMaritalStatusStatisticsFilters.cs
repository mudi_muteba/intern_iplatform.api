﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportMaritalStatusStatisticsFilters : IApplyDefaultFilters<InsurerDataReportMaritalStatusStatistics>
    {
        public IQueryable<InsurerDataReportMaritalStatusStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportMaritalStatusStatistics> query)
        {
            return query;
        }
    }
}