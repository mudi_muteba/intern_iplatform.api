﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportVehicleColourStatisticsFilters : IApplyDefaultFilters<InsurerDataReportVehicleColourStatistics>
    {
        public IQueryable<InsurerDataReportVehicleColourStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportVehicleColourStatistics> query)
        {
            return query;
        }
    }
}