﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportVehicleMakeStatisticsFilters : IApplyDefaultFilters<InsurerDataReportVehicleMakeStatistics>
    {
        public IQueryable<InsurerDataReportVehicleMakeStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportVehicleMakeStatistics> query)
        {
            return query;
        }
    }
}