﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportClassUseStatisticsFilters : IApplyDefaultFilters<InsurerDataReportClassUseStatistics>
    {
        public IQueryable<InsurerDataReportClassUseStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportClassUseStatistics> query)
        {
            return query;
        }
    }
}