﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportCoverTypeStatisticsFilters : IApplyDefaultFilters<InsurerDataReportCoverTypeStatistics>
    {
        public IQueryable<InsurerDataReportCoverTypeStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportCoverTypeStatistics> query)
        {
            return query;
        }
    }
}