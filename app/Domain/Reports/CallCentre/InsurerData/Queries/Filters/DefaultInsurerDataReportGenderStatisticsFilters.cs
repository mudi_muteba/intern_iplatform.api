﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportGenderStatisticsFilters : IApplyDefaultFilters<InsurerDataReportGenderStatistics>
    {
        public IQueryable<InsurerDataReportGenderStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportGenderStatistics> query)
        {
            return query;
        }
    }
}