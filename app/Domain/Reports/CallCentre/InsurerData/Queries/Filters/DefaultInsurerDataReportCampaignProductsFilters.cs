﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportCampaignProductsFilters : IApplyDefaultFilters<InsurerDataReportCampaignProducts>
    {
        public IQueryable<InsurerDataReportCampaignProducts> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportCampaignProducts> query)
        {
            return query;
        }
    }
}