﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportHeaderFilters : IApplyDefaultFilters<InsurerDataReportHeader>
    {
        public IQueryable<InsurerDataReportHeader> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportHeader> query)
        {
            return query;
        }
    }
}