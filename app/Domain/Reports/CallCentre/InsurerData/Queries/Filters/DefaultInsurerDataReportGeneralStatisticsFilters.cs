﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.InsurerData.Queries.Filters
{
    public class DefaultInsurerDataReportGeneralStatisticsFilters : IApplyDefaultFilters<InsurerDataReportGeneralStatistics>
    {
        public IQueryable<InsurerDataReportGeneralStatistics> Apply(ExecutionContext executionContext, IQueryable<InsurerDataReportGeneralStatistics> query)
        {
            return query;
        }
    }
}