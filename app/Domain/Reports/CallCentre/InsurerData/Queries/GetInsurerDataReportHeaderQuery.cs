﻿using System.Collections.Generic;

using MasterData.Authorisation;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerData.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.Queries.Filters;

namespace Domain.Reports.CallCentre.InsurerData.Queries
{
    public class GetInsurerDataReportHeaderQuery : BaseStoredProcedureQuery<InsurerDataReportHeader, InsurerDataReportHeaderStoredProcedure>
    {
        public GetInsurerDataReportHeaderQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerDataReportHeaderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerDataReportHeader> Execute(IList<InsurerDataReportHeader> query)
        {
            return query;
        }
    }
}
