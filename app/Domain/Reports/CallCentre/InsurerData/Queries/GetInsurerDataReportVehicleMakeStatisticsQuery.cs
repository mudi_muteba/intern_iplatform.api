﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerData.StoredProcedures;
using Domain.Reports.CallCentre.InsurerData.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.InsurerData.Queries
{
    public class GetInsurerDataReportVehicleMakeStatisticsQuery : BaseStoredProcedureQuery<InsurerDataReportVehicleMakeStatistics, InsurerDataReportVehicleMakeStatisticsStoredProcedure>
    {
        public GetInsurerDataReportVehicleMakeStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultInsurerDataReportVehicleMakeStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<InsurerDataReportVehicleMakeStatistics> Execute(IList<InsurerDataReportVehicleMakeStatistics> query)
        {
            return query;
        }
    }
}
