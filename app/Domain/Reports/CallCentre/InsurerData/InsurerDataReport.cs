﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerData;

namespace Domain.Reports.CallCentre.InsurerData
{
    public class InsurerDataReport : ReportGenerator
    {
        public InsurerDataReport() : base() { }

        public virtual InsurerDataReportContainerDto Get(string campaignIds, string productIds, DateTime dateFrom, DateTime dateTo)
        {
            var criteria = new CallCentreReportCriteriaDto
            {
                CampaignIds = campaignIds,
                ProductIds = productIds,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var response = Connector.ReportManagement.CallCentreReportConnector.GetInsurerDataReport(criteria).Response;
            return response;
        }
    }
}
