﻿using System.Collections.Generic;

using Domain.Base;
using Domain.Products;

namespace Domain.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportContainer : Entity
    {
        public InsurerDataReportContainer() { }

        public InsurerDataReportContainer(InsurerDataReportHeader header, List<InsurerDataReportInsurerQuoteStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual InsurerDataReportHeader Header { get; set; }
        public virtual List<InsurerDataReportInsurerQuoteStatistics> Statistics { get; set; }

        public virtual InsurerDataReportCampaignProducts Product { get; set; }

        public virtual List<InsurerDataReportAgeStatistics> AgeStatistics { get; set; }
        public virtual List<InsurerDataReportClassUseStatistics> ClassUseStatistics { get; set; }
        public virtual List<InsurerDataReportCoverTypeStatistics> CoverTypeStatistics { get; set; }
        public virtual List<InsurerDataReportGenderStatistics> GenderStatistics { get; set; }
        public virtual InsurerDataReportGeneralStatistics GeneralStatistics { get; set; }
        public virtual List<InsurerDataReportInsuranceTypeStatistics> InsuranceTypeStatistics { get; set; }
        public virtual List<InsurerDataReportMaritalStatusStatistics> MaritalStatusStatistics { get; set; }
        public virtual List<InsurerDataReportProvinceStatistics> ProvinceStatistics { get; set; }
        public virtual List<InsurerDataReportVehicleColourStatistics> VehicleColourStatistics { get; set; }
        public virtual List<InsurerDataReportVehicleMakeStatistics> VehicleMakeStatistics { get; set; }

        public virtual List<InsurerDataReportContainer> StatisticsByProduct { get; set; }
    }
}
