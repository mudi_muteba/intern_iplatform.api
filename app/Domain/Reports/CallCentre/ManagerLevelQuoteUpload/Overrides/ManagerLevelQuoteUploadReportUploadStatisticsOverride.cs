﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Overrides
{
    public class ManagerLevelQuoteUploadReportUploadStatisticsOverride : IAutoMappingOverride<ManagerLevelQuoteUploadReportUploadStatistics>
    {
        public void Override(AutoMapping<ManagerLevelQuoteUploadReportUploadStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
