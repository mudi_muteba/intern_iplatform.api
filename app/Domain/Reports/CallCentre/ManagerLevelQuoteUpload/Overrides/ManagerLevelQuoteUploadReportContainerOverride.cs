﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Overrides
{
    public class ManagerLevelQuoteUploadReportContainerOverride : IAutoMappingOverride<ManagerLevelQuoteUploadReportContainer>
    {
        public void Override(AutoMapping<ManagerLevelQuoteUploadReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
