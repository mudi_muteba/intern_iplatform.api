﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Overrides
{
    public class ManagerLevelQuoteUploadReportHeaderOverride : IAutoMappingOverride<ManagerLevelQuoteUploadReportHeader>
    {
        public void Override(AutoMapping<ManagerLevelQuoteUploadReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
