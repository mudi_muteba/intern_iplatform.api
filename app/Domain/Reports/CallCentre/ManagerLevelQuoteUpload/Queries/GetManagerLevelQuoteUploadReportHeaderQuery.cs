﻿using System.Collections.Generic;

using MasterData.Authorisation;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Queries.Filters;
using Domain.Reports.CallCentre.ManagerLevelQuoteUpload.StoredProcedures;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Queries
{
    public class GetManagerLevelQuoteUploadReportHeaderQuery : BaseStoredProcedureQuery<ManagerLevelQuoteUploadReportHeader, ManagerLevelQuoteUploadReportHeaderStoredProcedure>
    {
        public GetManagerLevelQuoteUploadReportHeaderQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultManagerLevelQuoteUploadReportHeaderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ManagerLevelQuoteUploadReportHeader> Execute(IList<ManagerLevelQuoteUploadReportHeader> query)
        {
            return query;
        }
    }
}
