﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Queries.Filters;
using Domain.Reports.CallCentre.ManagerLevelQuoteUpload.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Queries
{
    public class GetManagerLevelQuoteUploadReportUploadStatisticsQuery : BaseStoredProcedureQuery<ManagerLevelQuoteUploadReportUploadStatistics, ManagerLevelQuoteUploadReportUploadStatisticsStoredProcedure>
    {
        public GetManagerLevelQuoteUploadReportUploadStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultManagerLevelQuoteUploadReportUploadStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<ManagerLevelQuoteUploadReportUploadStatistics> Execute(IList<ManagerLevelQuoteUploadReportUploadStatistics> query)
        {
            return query;
        }
    }
}
