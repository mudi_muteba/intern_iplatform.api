﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Queries.Filters
{
    public class DefaultManagerLevelQuoteUploadReportUploadStatisticsFilters : IApplyDefaultFilters<ManagerLevelQuoteUploadReportUploadStatistics>
    {
        public IQueryable<ManagerLevelQuoteUploadReportUploadStatistics> Apply(ExecutionContext executionContext, IQueryable<ManagerLevelQuoteUploadReportUploadStatistics> query)
        {
            return query;
        }
    }
}