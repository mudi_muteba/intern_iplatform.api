﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Queries.Filters
{
    public class DefaultManagerLevelQuoteUploadReportHeaderFilters : IApplyDefaultFilters<ManagerLevelQuoteUploadReportHeader>
    {
        public IQueryable<ManagerLevelQuoteUploadReportHeader> Apply(ExecutionContext executionContext, IQueryable<ManagerLevelQuoteUploadReportHeader> query)
        {
            return query;
        }
    }
}