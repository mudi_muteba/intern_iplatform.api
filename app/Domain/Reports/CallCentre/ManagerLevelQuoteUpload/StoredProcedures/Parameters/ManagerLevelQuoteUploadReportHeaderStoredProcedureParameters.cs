﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.StoredProcedures.Parameters
{
    public class ManagerLevelQuoteUploadReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ChannelId { get; set; }
    }
}