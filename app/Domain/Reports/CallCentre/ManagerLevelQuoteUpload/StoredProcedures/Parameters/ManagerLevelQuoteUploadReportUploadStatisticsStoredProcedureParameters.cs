﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.StoredProcedures.Parameters
{
    public class ManagerLevelQuoteUploadReportUploadStatisticsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string ChannelIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}