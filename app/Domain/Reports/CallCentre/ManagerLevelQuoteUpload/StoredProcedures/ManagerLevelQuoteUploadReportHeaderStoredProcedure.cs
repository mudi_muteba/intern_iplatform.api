﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.ManagerLevelQuoteUpload.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.StoredProcedures
{
    public class ManagerLevelQuoteUploadReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_ManagerLevelQuoteUpload_Header";
            }
        }

        public ManagerLevelQuoteUploadReportHeaderStoredProcedure() { }

        public ManagerLevelQuoteUploadReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new ManagerLevelQuoteUploadReportHeaderStoredProcedureParameters
                {
                    ChannelId = criteria.ChannelId
                });
        }
    }
}