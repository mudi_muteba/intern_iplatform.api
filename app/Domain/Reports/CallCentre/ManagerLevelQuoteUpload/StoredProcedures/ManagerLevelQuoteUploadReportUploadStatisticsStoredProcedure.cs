﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.ManagerLevelQuoteUpload.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.StoredProcedures
{
    public class ManagerLevelQuoteUploadReportUploadStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_ManagerLevelQuoteUpload_GetUploadStatistics";
            }
        }

        public ManagerLevelQuoteUploadReportUploadStatisticsStoredProcedure() { }

        public ManagerLevelQuoteUploadReportUploadStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new ManagerLevelQuoteUploadReportUploadStatisticsStoredProcedureParameters
                {
                    ChannelIds = criteria.ChannelIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}