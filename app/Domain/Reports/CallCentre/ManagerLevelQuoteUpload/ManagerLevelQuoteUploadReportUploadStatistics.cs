﻿using Domain.Base;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload
{
    public class ManagerLevelQuoteUploadReportUploadStatistics : Entity
    {
        public ManagerLevelQuoteUploadReportUploadStatistics() { }

        public ManagerLevelQuoteUploadReportUploadStatistics(object[] obj)
        {
            this.Channel = obj[0] != null ? (string) obj[0] : "";
            this.Agent = obj[1] != null ? (string) obj[1] : "";
            this.Campaign = obj[2] != null ? (string) obj[2] : "";
            this.Leads = obj[3] != null ? (int) obj[3] : 0;
            this.Quotes = obj[4] != null ? (int) obj[4] : 0;
            this.QuotesPerLead = obj[5] != null ? (decimal) obj[5] : 0;
            this.SecondLevelUnderwriting = obj[6] != null ? (int) obj[6] : 0;
            this.Sales = obj[7] != null ? (int) obj[7] : 0;
            this.Closing = obj[8] != null ? (decimal) obj[8] : 0;
            this.InsurerClosing = obj[9] != null ? (decimal) obj[9] : 0;
            this.InsurerDropOff = obj[10] != null ? (decimal) obj[10] : 0;
            this.AveragePremiumPerAcceptedQuote = obj[11] != null ? (decimal) obj[11] : 0;
        }

        public virtual string Channel { get; set; }
        public virtual string Agent { get; set; }
        public virtual string Campaign { get; set; }

        public virtual int Leads { get; set; }
        public virtual int Quotes { get; set; }
        public virtual decimal QuotesPerLead { get; set; }
        public virtual int SecondLevelUnderwriting { get; set; }
        public virtual int Sales { get; set; }

        public virtual decimal Closing { get; set; }
        public virtual decimal InsurerClosing { get; set; }
        public virtual decimal InsurerDropOff { get; set; }
        public virtual decimal AveragePremiumPerAcceptedQuote { get; set; }
    }
      
}
