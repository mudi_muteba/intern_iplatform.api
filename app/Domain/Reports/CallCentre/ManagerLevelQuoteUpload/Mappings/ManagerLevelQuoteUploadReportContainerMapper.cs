﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.ManagerLevelQuoteUpload;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Mappings
{
    public class ManagerLevelQuoteUploadReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ManagerLevelQuoteUploadReportContainer, ManagerLevelQuoteUploadReportContainerDto>();

            Mapper.CreateMap<QueryResult<ManagerLevelQuoteUploadReportContainerDto>, POSTResponseDto<ManagerLevelQuoteUploadReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<ManagerLevelQuoteUploadReportContainer>>, POSTResponseDto<IList<ManagerLevelQuoteUploadReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}