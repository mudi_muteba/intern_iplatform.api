﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.ManagerLevelQuoteUpload;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Mappings
{
    public class ManagerLevelQuoteUploadReportHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ManagerLevelQuoteUploadReportHeader, ManagerLevelQuoteUploadReportHeaderDto>();

            Mapper.CreateMap<QueryResult<ManagerLevelQuoteUploadReportHeaderDto>, POSTResponseDto<ManagerLevelQuoteUploadReportHeaderDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<ManagerLevelQuoteUploadReportHeader>>, POSTResponseDto<IList<ManagerLevelQuoteUploadReportHeaderDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}