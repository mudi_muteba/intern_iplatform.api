﻿using Domain.Base;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload
{
    public class ManagerLevelQuoteUploadReportHeader : Entity
    {
        public ManagerLevelQuoteUploadReportHeader() { }

        public ManagerLevelQuoteUploadReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }

}
