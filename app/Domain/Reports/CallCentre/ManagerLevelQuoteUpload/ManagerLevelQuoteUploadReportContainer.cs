﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload
{
    public class ManagerLevelQuoteUploadReportContainer : Entity
    {
        public ManagerLevelQuoteUploadReportContainer() { }

        public ManagerLevelQuoteUploadReportContainer(ManagerLevelQuoteUploadReportHeader header, List<ManagerLevelQuoteUploadReportUploadStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual ManagerLevelQuoteUploadReportHeader Header { get; set; }
        public virtual List<ManagerLevelQuoteUploadReportUploadStatistics> Statistics { get; set; }
    }

}
