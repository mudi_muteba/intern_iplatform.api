﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.ManagerLevelQuoteUpload;

namespace Domain.Reports.CallCentre.ManagerLevelQuoteUpload
{
    public class ManagerLevelQuoteUploadReport : ReportGenerator
    {
        public ManagerLevelQuoteUploadReport() : base() { }

        public virtual ManagerLevelQuoteUploadReportContainerDto Get(string channelIds, DateTime dateFrom, DateTime dateTo)
        {
            var criteria = new CallCentreReportCriteriaDto
            {
                ChannelIds = channelIds,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var response = Connector.ReportManagement.CallCentreReportConnector.GetManagerLevelQuoteUploadsReport(criteria).Response;
            return response;
        }
    }
}
