﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.OutboundRatios
{
    public class CallCentreOutboundRatiosReportContainer : Entity
    {
        public CallCentreOutboundRatiosReportContainer() { }

        public CallCentreOutboundRatiosReportContainer(CallCentreOutboundRatiosReportHeader header, List<CallCentreOutboundRatiosReportAgentLeadStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual CallCentreOutboundRatiosReportHeader Header { get; set; }
        public virtual List<CallCentreOutboundRatiosReportAgentLeadStatistics> Statistics { get; set; }
    }
}
