﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.OutboundRatios.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.OutboundRatios.StoredProcedures
{
    public class CallCentreOutboundRatiosReportAgentLeadStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_OutboundRatios_GetAgentLeadStatistics";
            }
        }

        public CallCentreOutboundRatiosReportAgentLeadStatisticsStoredProcedure() { }

        public CallCentreOutboundRatiosReportAgentLeadStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new CallCentreOutboundRatiosReportAgentLeadStatisticsStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}