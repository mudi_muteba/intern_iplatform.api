﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.OutboundRatios.StoredProcedures.Parameters
{
    public class CallCentreOutboundRatiosReportAgentLeadStatisticsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int CampaignId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}