﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.OutboundRatios.StoredProcedures.Parameters
{
    public class CallCentreOutboundRatiosReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int CampaignId { get; set; }
    }
}