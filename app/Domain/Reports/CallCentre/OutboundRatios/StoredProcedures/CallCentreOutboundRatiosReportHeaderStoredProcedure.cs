﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.OutboundRatios.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.OutboundRatios.StoredProcedures
{
    public class CallCentreOutboundRatiosReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_OutboundRatios_Header";
            }
        }

        public CallCentreOutboundRatiosReportHeaderStoredProcedure() { }

        public CallCentreOutboundRatiosReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new CallCentreOutboundRatiosReportHeaderStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId
                });
        }
    }
}