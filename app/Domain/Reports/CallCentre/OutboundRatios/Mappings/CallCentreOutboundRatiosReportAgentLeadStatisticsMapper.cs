﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.OutboundRatios;

namespace Domain.Reports.CallCentre.OutboundRatios.Mappings
{
    public class CallCentreOutboundRatiosReportAgentLeadStatisticsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CallCentreOutboundRatiosReportAgentLeadStatistics, CallCentreOutboundRatiosReportAgentLeadStatisticsDto>();

            Mapper.CreateMap<QueryResult<CallCentreOutboundRatiosReportAgentLeadStatisticsDto>, POSTResponseDto<CallCentreOutboundRatiosReportAgentLeadStatisticsDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<CallCentreOutboundRatiosReportAgentLeadStatistics>>, POSTResponseDto<IList<CallCentreOutboundRatiosReportAgentLeadStatisticsDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}