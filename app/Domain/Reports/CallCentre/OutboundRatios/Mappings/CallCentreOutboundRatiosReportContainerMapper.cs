﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.OutboundRatios;

namespace Domain.Reports.CallCentre.OutboundRatios.Mappings
{
    public class CallCentreOutboundRatiosReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CallCentreOutboundRatiosReportContainer, CallCentreOutboundRatiosReportContainerDto>();

            Mapper.CreateMap<QueryResult<CallCentreOutboundRatiosReportContainerDto>, POSTResponseDto<CallCentreOutboundRatiosReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<CallCentreOutboundRatiosReportContainer>>, POSTResponseDto<IList<CallCentreOutboundRatiosReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}