﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.OutboundRatios.Overrides
{
    public class CallCentreOutboundRatiosReportContainerOverride : IAutoMappingOverride<CallCentreOutboundRatiosReportContainer>
    {
        public void Override(AutoMapping<CallCentreOutboundRatiosReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
