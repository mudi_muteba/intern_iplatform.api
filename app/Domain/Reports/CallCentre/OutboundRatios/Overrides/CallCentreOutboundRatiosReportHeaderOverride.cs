﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.OutboundRatios.Overrides
{
    public class CallCentreOutboundRatiosReportHeaderOverride : IAutoMappingOverride<CallCentreOutboundRatiosReportHeader>
    {
        public void Override(AutoMapping<CallCentreOutboundRatiosReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
