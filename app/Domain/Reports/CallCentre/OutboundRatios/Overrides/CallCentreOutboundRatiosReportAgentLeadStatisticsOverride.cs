﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.OutboundRatios.Overrides
{
    public class CallCentreOutboundRatiosReportAgentLeadStatisticsOverride : IAutoMappingOverride<CallCentreOutboundRatiosReportAgentLeadStatistics>
    {
        public void Override(AutoMapping<CallCentreOutboundRatiosReportAgentLeadStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
