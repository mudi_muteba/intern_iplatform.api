﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.OutboundRatios
{
    public class CallCentreOutboundRatiosReportAgentLeadStatistics : Entity
    {
        public CallCentreOutboundRatiosReportAgentLeadStatistics() { }

        public CallCentreOutboundRatiosReportAgentLeadStatistics(object[] obj)
        {
            this.Agent = obj[0] != null ? (string) obj[0] : "";
            this.Campaign = obj[1] != null ? (string) obj[1] : "";

            this.LeadToSales = obj[2] != null ? (decimal) obj[2] : 0;
            this.ContactedToSales = obj[3] != null ? (decimal) obj[3] : 0;
            this.ContactedToLeadsWorked = obj[4] != null ? (decimal) obj[4] : 0;
            this.SalesToPresentation = obj[5] != null ? (decimal) obj[5] : 0;
            this.PresentationToLeads = obj[6] != null ? (decimal) obj[6] : 0;
        }

        public virtual string Agent { get; set; }
        public virtual string Campaign { get; set; }

        public virtual decimal LeadToSales { get; set; }
        public virtual decimal ContactedToSales { get; set; }
        public virtual decimal ContactedToLeadsWorked { get; set; }
        public virtual decimal SalesToPresentation { get; set; }
        public virtual decimal PresentationToLeads { get; set; }
    }
       
}
