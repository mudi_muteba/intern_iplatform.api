﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.OutboundRatios.Queries.Filters
{
    public class DefaultCallCentreOutboundRatiosReportHeaderFilters : IApplyDefaultFilters<CallCentreOutboundRatiosReportHeader>
    {
        public IQueryable<CallCentreOutboundRatiosReportHeader> Apply(ExecutionContext executionContext, IQueryable<CallCentreOutboundRatiosReportHeader> query)
        {
            return query;
        }
    }
}