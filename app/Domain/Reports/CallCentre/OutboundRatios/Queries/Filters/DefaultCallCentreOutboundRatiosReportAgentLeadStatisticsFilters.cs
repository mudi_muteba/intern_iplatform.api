﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.OutboundRatios.Queries.Filters
{
    public class DefaultCallCentreOutboundRatiosReportAgentLeadStatisticsFilters : IApplyDefaultFilters<CallCentreOutboundRatiosReportAgentLeadStatistics>
    {
        public IQueryable<CallCentreOutboundRatiosReportAgentLeadStatistics> Apply(ExecutionContext executionContext, IQueryable<CallCentreOutboundRatiosReportAgentLeadStatistics> query)
        {
            return query;
        }
    }
}