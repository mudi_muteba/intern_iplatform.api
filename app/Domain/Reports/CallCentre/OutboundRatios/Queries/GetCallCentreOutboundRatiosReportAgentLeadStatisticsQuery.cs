﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.OutboundRatios.StoredProcedures;
using Domain.Reports.CallCentre.OutboundRatios.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.OutboundRatios.Queries
{
    public class GetCallCentreOutboundRatiosReportAgentLeadStatisticsQuery : BaseStoredProcedureQuery<CallCentreOutboundRatiosReportAgentLeadStatistics, CallCentreOutboundRatiosReportAgentLeadStatisticsStoredProcedure>
    {
        public GetCallCentreOutboundRatiosReportAgentLeadStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultCallCentreOutboundRatiosReportAgentLeadStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<CallCentreOutboundRatiosReportAgentLeadStatistics> Execute(IList<CallCentreOutboundRatiosReportAgentLeadStatistics> query)
        {
            return query;
        }
    }
}
