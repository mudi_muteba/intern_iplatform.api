﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.OutboundRatios.StoredProcedures;
using Domain.Reports.CallCentre.OutboundRatios.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.OutboundRatios.Queries
{
    public class GetCallCentreOutboundRatiosReportHeaderQuery : BaseStoredProcedureQuery<CallCentreOutboundRatiosReportHeader, CallCentreOutboundRatiosReportHeaderStoredProcedure>
    {
        public GetCallCentreOutboundRatiosReportHeaderQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultCallCentreOutboundRatiosReportHeaderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<CallCentreOutboundRatiosReportHeader> Execute(IList<CallCentreOutboundRatiosReportHeader> query)
        {
            return query;
        }
    }
}
