﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.OutboundRatios
{
    public class CallCentreOutboundRatiosReportHeader : Entity
    {
        public CallCentreOutboundRatiosReportHeader() { }

        public CallCentreOutboundRatiosReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
