﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.SalesManagement;

namespace Domain.Reports.CallCentre.SalesManagement.Mappings
{
    public class CallCentreSalesManagementReportAgentLeadStatisticsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CallCentreSalesManagementReportAgentLeadStatistics, CallCentreSalesManagementReportAgentLeadStatisticsDto>();

            Mapper.CreateMap<QueryResult<CallCentreSalesManagementReportAgentLeadStatisticsDto>, POSTResponseDto<CallCentreSalesManagementReportAgentLeadStatisticsDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<CallCentreSalesManagementReportAgentLeadStatistics>>, POSTResponseDto<IList<CallCentreSalesManagementReportAgentLeadStatisticsDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}