﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.SalesManagement;

namespace Domain.Reports.CallCentre.SalesManagement.Mappings
{
    public class CallCentreSalesManagementReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CallCentreSalesManagementReportContainer, CallCentreSalesManagementReportContainerDto>();

            Mapper.CreateMap<QueryResult<CallCentreSalesManagementReportContainerDto>, POSTResponseDto<CallCentreSalesManagementReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<CallCentreSalesManagementReportContainer>>, POSTResponseDto<IList<CallCentreSalesManagementReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}