﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.SalesManagement.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.SalesManagement.StoredProcedures
{
    public class CallCentreSalesManagementReportAgentLeadStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_SalesManagement_GetAgentLeadStatistics";
            }
        }

        public CallCentreSalesManagementReportAgentLeadStatisticsStoredProcedure() { }

        public CallCentreSalesManagementReportAgentLeadStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new CallCentreSalesManagementReportAgentLeadStatisticsStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}