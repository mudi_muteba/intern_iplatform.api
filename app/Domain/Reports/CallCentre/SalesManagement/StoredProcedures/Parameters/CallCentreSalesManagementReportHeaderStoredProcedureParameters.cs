﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.SalesManagement.StoredProcedures.Parameters
{
    public class CallCentreSalesManagementReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int CampaignId { get; set; }
    }
}