﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.SalesManagement.StoredProcedures.Parameters
{
    public class CallCentreSalesManagementReportAgentLeadStatisticsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int CampaignId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}