﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.SalesManagement.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.SalesManagement.StoredProcedures
{
    public class CallCentreSalesManagementReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_SalesManagement_Header";
            }
        }

        public CallCentreSalesManagementReportHeaderStoredProcedure() { }

        public CallCentreSalesManagementReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new CallCentreSalesManagementReportHeaderStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId
                });
        }
    }
}