﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.SalesManagement
{
    public class CallCentreSalesManagementReportAgentLeadStatistics : Entity
    {
        public CallCentreSalesManagementReportAgentLeadStatistics() { }

        public CallCentreSalesManagementReportAgentLeadStatistics(object[] obj)
        {
            this.Agent = obj[0] != null ? (string) obj[0] : "";
            this.Campaign = obj[1] != null ? (string) obj[1] : "";

            this.DailySalesTarget = obj[2] != null ? (int) obj[2] : 0;
            this.ActualSales = obj[3] != null ? (int) obj[3] : 0;
            this.MTDSalesTarget = obj[4] != null ? (int) obj[4] : 0;
            this.MTDSales = obj[5] != null ? (int) obj[5] : 0;
            this.ProjectedSales = obj[6] != null ? (int) obj[6] : 0;
            this.AttainmentToSalesTarget = obj[7] != null ? (decimal) obj[7] : 0;
            this.PremiumTarget = obj[8] != null ? (decimal) obj[8] : 0;
            this.ActualPremium = obj[9] != null ? (decimal) obj[9] : 0;
            this.MTDPremiumTarget = obj[10] != null ? (decimal) obj[10] : 0;
            this.MTDPremium = obj[11] != null ? (decimal) obj[11] : 0;
            this.ProjectedPremium = obj[12] != null ? (decimal) obj[12] : 0;
            this.AttainmentToPremiumTarget = obj[13] != null ? (decimal) obj[13] : 0;
        }

        public virtual string Agent { get; set; }
        public virtual string Campaign { get; set; }

        public virtual int DailySalesTarget { get; set; }
        public virtual int ActualSales { get; set; }
        public virtual int MTDSalesTarget { get; set; }
        public virtual int MTDSales { get; set; }
        public virtual int ProjectedSales { get; set; }
        public virtual decimal AttainmentToSalesTarget { get; set; }
        public virtual decimal PremiumTarget { get; set; }
        public virtual decimal ActualPremium { get; set; }
        public virtual decimal MTDPremiumTarget { get; set; }
        public virtual decimal MTDPremium { get; set; }
        public virtual decimal ProjectedPremium { get; set; }
        public virtual decimal AttainmentToPremiumTarget { get; set; }

    }
       
}
