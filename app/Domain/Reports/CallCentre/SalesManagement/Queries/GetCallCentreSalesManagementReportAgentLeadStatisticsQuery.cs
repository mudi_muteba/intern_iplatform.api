﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.SalesManagement.StoredProcedures;
using Domain.Reports.CallCentre.SalesManagement.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.SalesManagement.Queries
{
    public class GetCallCentreSalesManagementReportAgentLeadStatisticsQuery : BaseStoredProcedureQuery<CallCentreSalesManagementReportAgentLeadStatistics, CallCentreSalesManagementReportAgentLeadStatisticsStoredProcedure>
    {
        public GetCallCentreSalesManagementReportAgentLeadStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultCallCentreSalesManagementReportAgentLeadStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<CallCentreSalesManagementReportAgentLeadStatistics> Execute(IList<CallCentreSalesManagementReportAgentLeadStatistics> query)
        {
            return query;
        }
    }
}
