﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.SalesManagement.Queries.Filters
{
    public class DefaultCallCentreSalesManagementReportHeaderFilters : IApplyDefaultFilters<CallCentreSalesManagementReportHeader>
    {
        public IQueryable<CallCentreSalesManagementReportHeader> Apply(ExecutionContext executionContext, IQueryable<CallCentreSalesManagementReportHeader> query)
        {
            return query;
        }
    }
}