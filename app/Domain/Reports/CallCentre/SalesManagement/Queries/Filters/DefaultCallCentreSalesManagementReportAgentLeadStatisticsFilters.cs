﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.SalesManagement.Queries.Filters
{
    public class DefaultCallCentreSalesManagementReportAgentLeadStatisticsFilters : IApplyDefaultFilters<CallCentreSalesManagementReportAgentLeadStatistics>
    {
        public IQueryable<CallCentreSalesManagementReportAgentLeadStatistics> Apply(ExecutionContext executionContext, IQueryable<CallCentreSalesManagementReportAgentLeadStatistics> query)
        {
            return query;
        }
    }
}