﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.SalesManagement
{
    public class CallCentreSalesManagementReportContainer : Entity
    {
        public CallCentreSalesManagementReportContainer() { }

        public CallCentreSalesManagementReportContainer(CallCentreSalesManagementReportHeader header, List<CallCentreSalesManagementReportAgentLeadStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual CallCentreSalesManagementReportHeader Header { get; set; }
        public virtual List<CallCentreSalesManagementReportAgentLeadStatistics> Statistics { get; set; }
    }
}
