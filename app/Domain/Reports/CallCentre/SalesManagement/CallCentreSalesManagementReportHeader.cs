﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.SalesManagement
{
    public class CallCentreSalesManagementReportHeader : Entity
    {
        public CallCentreSalesManagementReportHeader() { }

        public CallCentreSalesManagementReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
