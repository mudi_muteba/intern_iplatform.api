﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.SalesManagement.Overrides
{
    public class CallCentreSalesManagementReportAgentLeadStatisticsOverride : IAutoMappingOverride<CallCentreSalesManagementReportAgentLeadStatistics>
    {
        public void Override(AutoMapping<CallCentreSalesManagementReportAgentLeadStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
