﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.SalesManagement.Overrides
{
    public class CallCentreSalesManagementReportHeaderOverride : IAutoMappingOverride<CallCentreSalesManagementReportHeader>
    {
        public void Override(AutoMapping<CallCentreSalesManagementReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
