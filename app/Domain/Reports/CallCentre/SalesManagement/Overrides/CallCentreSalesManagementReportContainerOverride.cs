﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.SalesManagement.Overrides
{
    public class CallCentreSalesManagementReportContainerOverride : IAutoMappingOverride<CallCentreSalesManagementReportContainer>
    {
        public void Override(AutoMapping<CallCentreSalesManagementReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
