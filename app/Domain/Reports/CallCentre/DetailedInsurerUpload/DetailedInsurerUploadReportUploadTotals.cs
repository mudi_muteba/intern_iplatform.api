﻿using System;
using Domain.Base;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload
{
    public class DetailedInsurerUploadReportUploadTotals : Entity
    {
        public DetailedInsurerUploadReportUploadTotals() { }

        public DetailedInsurerUploadReportUploadTotals(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
            this.Product = obj[1] != null ? (string) obj[1] : "";
            this.Uploads = obj[2] != null ? (int) obj[2] : 0;
            this.AveragePremiumValue = obj[3] != null ? (decimal) obj[3] : 0;
            this.TotalPremiumValue = obj[4] != null ? (decimal) obj[4] : 0;
        }

        public virtual string Insurer { get; set; }
        public virtual string Product { get; set; }
        public virtual int Uploads { get; set; }
        public virtual decimal AveragePremiumValue { get; set; }
        public virtual decimal TotalPremiumValue { get; set; }
    }
}
