﻿using System;
using Domain.Base;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload
{
    public class DetailedInsurerUploadReportUploadStatistics : Entity
    {
        public DetailedInsurerUploadReportUploadStatistics() { }

        public DetailedInsurerUploadReportUploadStatistics(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
            this.Product = obj[1] != null ? (string) obj[1] : "";
            this.Agent = obj[2] != null ? (string) obj[2] : "";
            this.Client = obj[3] != null ? (string) obj[3] : "";
            this.InsurerReference = obj[4] != null ? (string) obj[4] : "";
            this.UploadDate = obj[5] != null ? (DateTime) obj[5] : new DateTime(1900, 1, 1);
            this.PremiumValue = obj[6] != null ? (decimal) obj[6] : 0;
        }

        public virtual string Insurer { get; set; }
        public virtual string Product { get; set; }
        public virtual string Agent { get; set; }
        public virtual string Client { get; set; }
        public virtual string InsurerReference { get; set; }
        public virtual DateTime UploadDate { get; set; }
        public virtual decimal PremiumValue { get; set; }
    }
       
}
