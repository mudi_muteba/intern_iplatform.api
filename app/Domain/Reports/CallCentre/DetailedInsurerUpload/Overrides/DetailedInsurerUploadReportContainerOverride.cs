﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Overrides
{
    public class DetailedInsurerUploadReportContainerOverride : IAutoMappingOverride<DetailedInsurerUploadReportContainer>
    {
        public void Override(AutoMapping<DetailedInsurerUploadReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
