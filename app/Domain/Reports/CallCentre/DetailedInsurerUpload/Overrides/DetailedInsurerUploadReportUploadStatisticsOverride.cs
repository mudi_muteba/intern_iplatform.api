﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Overrides
{
    public class DetailedInsurerUploadReportUploadStatisticsOverride : IAutoMappingOverride<DetailedInsurerUploadReportUploadStatistics>
    {
        public void Override(AutoMapping<DetailedInsurerUploadReportUploadStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
