﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Overrides
{
    public class DetailedInsurerUploadReportHeaderOverride : IAutoMappingOverride<DetailedInsurerUploadReportHeader>
    {
        public void Override(AutoMapping<DetailedInsurerUploadReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
