﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Overrides
{
    public class DetailedInsurerUploadReportUploadTotalsOverride : IAutoMappingOverride<DetailedInsurerUploadReportUploadTotals>
    {
        public void Override(AutoMapping<DetailedInsurerUploadReportUploadTotals> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
