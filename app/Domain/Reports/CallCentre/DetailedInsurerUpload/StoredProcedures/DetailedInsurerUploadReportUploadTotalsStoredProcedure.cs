﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures
{
    public class DetailedInsurerUploadReportUploadTotalsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_DetailedInsurerUpload_GetUploadTotals";
            }
        }

        public DetailedInsurerUploadReportUploadTotalsStoredProcedure() { }

        public DetailedInsurerUploadReportUploadTotalsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new DetailedInsurerUploadReportUploadTotalsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    InsurerIds = criteria.InsurerIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}