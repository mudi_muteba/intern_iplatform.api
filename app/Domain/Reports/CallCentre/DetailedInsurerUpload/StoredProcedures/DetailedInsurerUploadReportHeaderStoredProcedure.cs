﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures
{
    public class DetailedInsurerUploadReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_DetailedInsurerUpload_Header";
            }
        }

        public DetailedInsurerUploadReportHeaderStoredProcedure() { }

        public DetailedInsurerUploadReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new DetailedInsurerUploadReportHeaderStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId,
                    CampaignIds = criteria.CampaignIds
                });
        }
    }
}