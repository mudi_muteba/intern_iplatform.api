﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures.Parameters
{
    public class DetailedInsurerUploadReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int CampaignId { get; set; }
        public string CampaignIds { get; set; }
    }
}