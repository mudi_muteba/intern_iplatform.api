﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures
{
    public class DetailedInsurerUploadReportUploadStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics";
            }
        }

        public DetailedInsurerUploadReportUploadStatisticsStoredProcedure() { }

        public DetailedInsurerUploadReportUploadStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new DetailedInsurerUploadReportUploadStatisticsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    InsurerIds = criteria.InsurerIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}