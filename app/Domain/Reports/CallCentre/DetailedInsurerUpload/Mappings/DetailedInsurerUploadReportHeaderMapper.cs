﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.DetailedInsurerUpload;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Mappings
{
    public class DetailedInsurerUploadReportHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DetailedInsurerUploadReportHeader, DetailedInsurerUploadReportHeaderDto>();

            Mapper.CreateMap<QueryResult<DetailedInsurerUploadReportHeaderDto>, POSTResponseDto<DetailedInsurerUploadReportHeaderDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<DetailedInsurerUploadReportHeader>>, POSTResponseDto<IList<DetailedInsurerUploadReportHeaderDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}