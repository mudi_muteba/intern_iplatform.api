﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.DetailedInsurerUpload;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Mappings
{
    public class DetailedInsurerUploadReportAgentQuoteStatisticsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DetailedInsurerUploadReportUploadTotals, DetailedInsurerUploadReportUploadTotalsDto>();

            Mapper.CreateMap<QueryResult<DetailedInsurerUploadReportUploadTotalsDto>, POSTResponseDto<DetailedInsurerUploadReportUploadTotalsDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<DetailedInsurerUploadReportUploadTotals>>, POSTResponseDto<IList<DetailedInsurerUploadReportUploadTotalsDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}