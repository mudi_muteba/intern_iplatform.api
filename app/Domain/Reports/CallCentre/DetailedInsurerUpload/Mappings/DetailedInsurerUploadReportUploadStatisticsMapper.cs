﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.DetailedInsurerUpload;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Mappings
{
    public class DetailedInsurerUploadReportUploadStatisticsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DetailedInsurerUploadReportUploadStatistics, DetailedInsurerUploadReportUploadStatisticsDto>();

            Mapper.CreateMap<QueryResult<DetailedInsurerUploadReportUploadStatisticsDto>, POSTResponseDto<DetailedInsurerUploadReportUploadStatisticsDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<DetailedInsurerUploadReportUploadStatistics>>, POSTResponseDto<IList<DetailedInsurerUploadReportUploadStatisticsDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}