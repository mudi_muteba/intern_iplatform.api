﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.DetailedInsurerUpload;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload
{
    public class DetailedInsurerUploadReport : ReportGenerator
    {
        public DetailedInsurerUploadReport() : base() { }

        public virtual DetailedInsurerUploadReportContainerDto Get(string campaignIds, string insurerIds, DateTime dateFrom, DateTime dateTo)
        {
            var criteria = new CallCentreReportCriteriaDto
            {
                CampaignIds = campaignIds,
                InsurerIds = insurerIds,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var response = Connector.ReportManagement.CallCentreReportConnector.GetDetailedInsurerUploadReport(criteria).Response;
            return response;
        }
    }
}
