﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload
{
    public class DetailedInsurerUploadReportContainer : Entity
    {
        public DetailedInsurerUploadReportContainer() { }

        public DetailedInsurerUploadReportContainer(DetailedInsurerUploadReportHeader header, List<DetailedInsurerUploadReportUploadStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual DetailedInsurerUploadReportHeader Header { get; set; }
        public virtual List<DetailedInsurerUploadReportUploadStatistics> Statistics { get; set; }
        public virtual List<DetailedInsurerUploadReportUploadTotals> Totals { get; set; }
    }
}