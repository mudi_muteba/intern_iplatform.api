﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures;
using Domain.Reports.CallCentre.DetailedInsurerUpload.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Queries
{
    public class GetDetailedInsurerUploadReportUploadStatisticsQuery : BaseStoredProcedureQuery<DetailedInsurerUploadReportUploadStatistics, DetailedInsurerUploadReportUploadStatisticsStoredProcedure>
    {
        public GetDetailedInsurerUploadReportUploadStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultDetailedInsurerUploadReportUploadStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<DetailedInsurerUploadReportUploadStatistics> Execute(IList<DetailedInsurerUploadReportUploadStatistics> query)
        {
            return query;
        }
    }
}
