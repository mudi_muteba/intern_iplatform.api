﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures;
using Domain.Reports.CallCentre.DetailedInsurerUpload.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Queries
{
    public class GetDetailedInsurerUploadReportUploadTotalsQuery : BaseStoredProcedureQuery<DetailedInsurerUploadReportUploadTotals, DetailedInsurerUploadReportUploadTotalsStoredProcedure>
    {
        public GetDetailedInsurerUploadReportUploadTotalsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultDetailedInsurerUploadReportUploadTotalsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<DetailedInsurerUploadReportUploadTotals> Execute(IList<DetailedInsurerUploadReportUploadTotals> query)
        {
            return query;
        }
    }
}
