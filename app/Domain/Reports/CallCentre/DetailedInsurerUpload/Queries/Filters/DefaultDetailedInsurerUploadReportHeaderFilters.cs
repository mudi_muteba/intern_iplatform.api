﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Queries.Filters
{
    public class DefaultDetailedInsurerUploadReportHeaderFilters : IApplyDefaultFilters<DetailedInsurerUploadReportHeader>
    {
        public IQueryable<DetailedInsurerUploadReportHeader> Apply(ExecutionContext executionContext, IQueryable<DetailedInsurerUploadReportHeader> query)
        {
            return query;
        }
    }
}