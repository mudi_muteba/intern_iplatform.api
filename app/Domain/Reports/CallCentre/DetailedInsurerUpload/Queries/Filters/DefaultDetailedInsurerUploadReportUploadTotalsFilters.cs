﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Queries.Filters
{
    public class DefaultDetailedInsurerUploadReportUploadTotalsFilters : IApplyDefaultFilters<DetailedInsurerUploadReportUploadTotals>
    {
        public IQueryable<DetailedInsurerUploadReportUploadTotals> Apply(ExecutionContext executionContext, IQueryable<DetailedInsurerUploadReportUploadTotals> query)
        {
            return query;
        }
    }
}