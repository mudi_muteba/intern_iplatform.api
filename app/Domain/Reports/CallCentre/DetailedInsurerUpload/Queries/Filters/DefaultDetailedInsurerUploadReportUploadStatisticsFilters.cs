﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload.Queries.Filters
{
    public class DefaultDetailedInsurerUploadReportUploadStatisticsFilters : IApplyDefaultFilters<DetailedInsurerUploadReportUploadStatistics>
    {
        public IQueryable<DetailedInsurerUploadReportUploadStatistics> Apply(ExecutionContext executionContext, IQueryable<DetailedInsurerUploadReportUploadStatistics> query)
        {
            return query;
        }
    }
}