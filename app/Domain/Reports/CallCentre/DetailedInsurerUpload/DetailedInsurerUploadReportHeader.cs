﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.DetailedInsurerUpload
{
    public class DetailedInsurerUploadReportHeader : Entity
    {
        public DetailedInsurerUploadReportHeader() { }

        public DetailedInsurerUploadReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
