﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.LeadManagement
{
    public class LeadManagementReportContainer : Entity
    {
        public LeadManagementReportContainer() { }

        public LeadManagementReportContainer(LeadManagementReportHeader header, List<LeadManagementReportAgentLeadStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual LeadManagementReportHeader Header { get; set; }
        public virtual List<LeadManagementReportAgentLeadStatistics> Statistics { get; set; }
    }
}
