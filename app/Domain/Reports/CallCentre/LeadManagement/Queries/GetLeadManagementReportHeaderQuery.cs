﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.LeadManagement.StoredProcedures;
using Domain.Reports.CallCentre.LeadManagement.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.LeadManagement.Queries
{
    public class GetLeadManagementReportHeaderQuery : BaseStoredProcedureQuery<LeadManagementReportHeader, LeadManagementReportHeaderStoredProcedure>
    {
        public GetLeadManagementReportHeaderQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultLeadManagementReportHeaderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<LeadManagementReportHeader> Execute(IList<LeadManagementReportHeader> query)
        {
            return query;
        }
    }
}
