﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.LeadManagement.StoredProcedures;
using Domain.Reports.CallCentre.LeadManagement.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.LeadManagement.Queries
{
    public class GetLeadManagementReportAgentLeadStatisticsQuery : BaseStoredProcedureQuery<LeadManagementReportAgentLeadStatistics, LeadManagementReportAgentLeadStatisticsStoredProcedure>
    {
        public GetLeadManagementReportAgentLeadStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultLeadManagementReportAgentLeadStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<LeadManagementReportAgentLeadStatistics> Execute(IList<LeadManagementReportAgentLeadStatistics> query)
        {
            return query;
        }
    }
}
