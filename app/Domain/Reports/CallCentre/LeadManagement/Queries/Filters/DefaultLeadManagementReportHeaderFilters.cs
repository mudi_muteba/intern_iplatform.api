﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.LeadManagement.Queries.Filters
{
    public class DefaultLeadManagementReportHeaderFilters : IApplyDefaultFilters<LeadManagementReportHeader>
    {
        public IQueryable<LeadManagementReportHeader> Apply(ExecutionContext executionContext, IQueryable<LeadManagementReportHeader> query)
        {
            return query;
        }
    }
}