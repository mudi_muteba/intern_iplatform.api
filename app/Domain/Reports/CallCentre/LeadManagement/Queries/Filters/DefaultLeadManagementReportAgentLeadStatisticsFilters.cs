﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.LeadManagement.Queries.Filters
{
    public class DefaultLeadManagementReportAgentLeadStatisticsFilters : IApplyDefaultFilters<LeadManagementReportAgentLeadStatistics>
    {
        public IQueryable<LeadManagementReportAgentLeadStatistics> Apply(ExecutionContext executionContext, IQueryable<LeadManagementReportAgentLeadStatistics> query)
        {
            return query;
        }
    }
}