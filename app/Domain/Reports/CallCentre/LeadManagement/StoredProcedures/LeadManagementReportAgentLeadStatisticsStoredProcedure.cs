﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.LeadManagement.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.LeadManagement.StoredProcedures
{
    public class LeadManagementReportAgentLeadStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_LeadManagement_GetAgentLeadStatistics";
            }
        }

        public LeadManagementReportAgentLeadStatisticsStoredProcedure() { }

        public LeadManagementReportAgentLeadStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new LeadManagementReportAgentLeadStatisticsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}