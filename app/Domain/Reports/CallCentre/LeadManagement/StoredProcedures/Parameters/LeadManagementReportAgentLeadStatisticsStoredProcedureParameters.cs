﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.LeadManagement.StoredProcedures.Parameters
{
    public class LeadManagementReportAgentLeadStatisticsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string CampaignIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}