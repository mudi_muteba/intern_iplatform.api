﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.LeadManagement.StoredProcedures.Parameters
{
    public class LeadManagementReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int CampaignId { get; set; }
        public string CampaignIds { get; set; }
    }
}