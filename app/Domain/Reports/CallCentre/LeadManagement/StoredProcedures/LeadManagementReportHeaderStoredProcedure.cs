﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.LeadManagement.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.LeadManagement.StoredProcedures
{
    public class LeadManagementReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_LeadManagement_Header";
            }
        }

        public LeadManagementReportHeaderStoredProcedure() { }

        public LeadManagementReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new LeadManagementReportHeaderStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId,
                    CampaignIds = criteria.CampaignIds
                });
        }
    }
}