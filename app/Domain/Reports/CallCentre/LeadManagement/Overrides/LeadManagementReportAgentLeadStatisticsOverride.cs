﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.LeadManagement.Overrides
{
    public class LeadManagementReportAgentLeadStatisticsOverride : IAutoMappingOverride<LeadManagementReportAgentLeadStatistics>
    {
        public void Override(AutoMapping<LeadManagementReportAgentLeadStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
