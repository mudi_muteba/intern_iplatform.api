﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.LeadManagement.Overrides
{
    public class LeadManagementReportHeaderOverride : IAutoMappingOverride<LeadManagementReportHeader>
    {
        public void Override(AutoMapping<LeadManagementReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
