﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.LeadManagement.Overrides
{
    public class LeadManagementReportContainerOverride : IAutoMappingOverride<LeadManagementReportContainer>
    {
        public void Override(AutoMapping<LeadManagementReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
