﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.LeadManagement;

namespace Domain.Reports.CallCentre.LeadManagement.Mappings
{
    public class LeadManagementReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<LeadManagementReportContainer, LeadManagementReportContainerDto>();

            Mapper.CreateMap<QueryResult<LeadManagementReportContainerDto>, POSTResponseDto<LeadManagementReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<LeadManagementReportContainer>>, POSTResponseDto<IList<LeadManagementReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}