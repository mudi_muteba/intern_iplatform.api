﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.LeadManagement
{
    public class LeadManagementReportHeader : Entity
    {
        public LeadManagementReportHeader() { }

        public LeadManagementReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
