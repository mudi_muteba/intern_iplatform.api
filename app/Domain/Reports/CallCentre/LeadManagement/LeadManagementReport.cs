﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.LeadManagement;

namespace Domain.Reports.CallCentre.LeadManagement
{
    public class LeadManagementReport : ReportGenerator
    {
        public LeadManagementReport() : base() { }

        public virtual LeadManagementReportContainerDto Get(string campaignIds, DateTime dateFrom, DateTime dateTo)
        {
            var criteria = new CallCentreReportCriteriaDto
            {
                CampaignIds = campaignIds,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var response = Connector.ReportManagement.CallCentreReportConnector.GetLeadManagementReport(criteria).Response;
            return response;
        }
    }
}
