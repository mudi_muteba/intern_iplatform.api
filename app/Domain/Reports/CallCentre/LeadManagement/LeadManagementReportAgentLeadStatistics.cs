﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.LeadManagement
{
    public class LeadManagementReportAgentLeadStatistics : Entity
    {
        public LeadManagementReportAgentLeadStatistics() { }

        public LeadManagementReportAgentLeadStatistics(object[] obj)
        {
            this.Agent = obj[0] != null ? (string) obj[0] : "";
            this.Campaign = obj[1] != null ? (string) obj[1] : "";

            this.Received = obj[2] != null ? (int) obj[2] : 0;
            this.Contacted = obj[3] != null ? (int) obj[3] : 0;
            this.Uncontactable = obj[4] != null ? (int) obj[4] : 0;
            this.Pending = obj[5] != null ? (int) obj[5] : 0;
            this.Completed = obj[6] != null ? (int) obj[6] : 0;
            this.Unactioned = obj[7] != null ? (int) obj[7] : 0;
        }

        public virtual string Agent { get; set; }
        public virtual string Campaign { get; set; }

        public virtual int Received { get; set; }
        public virtual int Contacted { get; set; }
        public virtual int Uncontactable { get; set; }
        public virtual int Pending { get; set; }
        public virtual int Completed { get; set; }
        public virtual int Unactioned { get; set; }
    }
       
}
