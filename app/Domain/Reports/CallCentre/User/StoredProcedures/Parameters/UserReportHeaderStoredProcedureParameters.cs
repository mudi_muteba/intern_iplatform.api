﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.User.StoredProcedures.Parameters
{
    public class UserReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ChannelId { get; set; }
    }
}