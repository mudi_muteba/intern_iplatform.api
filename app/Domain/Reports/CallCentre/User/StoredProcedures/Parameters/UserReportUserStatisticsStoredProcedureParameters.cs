﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.User.StoredProcedures.Parameters
{
    public class UserReportUserStatisticsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string ChannelIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}