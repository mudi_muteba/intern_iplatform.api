﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.User.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.User.StoredProcedures
{
    public class UserReportUserStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_User_GetUserStatistics";
            }
        }

        public UserReportUserStatisticsStoredProcedure() { }

        public UserReportUserStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new UserReportUserStatisticsStoredProcedureParameters
                {
                    ChannelIds = criteria.ChannelIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}