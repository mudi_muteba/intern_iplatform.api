﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.User.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.User.StoredProcedures
{
    public class UserReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_User_Header";
            }
        }

        public UserReportHeaderStoredProcedure() { }

        public UserReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new UserReportHeaderStoredProcedureParameters
                {
                    ChannelId = criteria.ChannelId
                });
        }
    }
}