﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.User
{
    public class UserReportHeader : Entity
    {
        public UserReportHeader() { }

        public UserReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
