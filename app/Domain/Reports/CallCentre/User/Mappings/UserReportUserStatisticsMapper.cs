﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.User;

namespace Domain.Reports.CallCentre.User.Mappings
{
    public class UserReportUserStatisticsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UserReportUserStatistics, UserReportUserStatisticsDto>();

            Mapper.CreateMap<QueryResult<UserReportUserStatisticsDto>, POSTResponseDto<UserReportUserStatisticsDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<UserReportUserStatistics>>, POSTResponseDto<IList<UserReportUserStatisticsDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}