﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.User;

namespace Domain.Reports.CallCentre.User.Mappings
{
    public class UserReportHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UserReportHeader, UserReportHeaderDto>();

            Mapper.CreateMap<QueryResult<UserReportHeaderDto>, POSTResponseDto<UserReportHeaderDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<UserReportHeader>>, POSTResponseDto<IList<UserReportHeaderDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}