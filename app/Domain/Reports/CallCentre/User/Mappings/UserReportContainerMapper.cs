﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.User;

namespace Domain.Reports.CallCentre.User.Mappings
{
    public class UserReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UserReportContainer, UserReportContainerDto>();

            Mapper.CreateMap<QueryResult<UserReportContainerDto>, POSTResponseDto<UserReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<UserReportContainer>>, POSTResponseDto<IList<UserReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}