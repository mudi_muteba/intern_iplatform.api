﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.User.Overrides
{
    public class UserReportContainerOverride : IAutoMappingOverride<UserReportContainer>
    {
        public void Override(AutoMapping<UserReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
