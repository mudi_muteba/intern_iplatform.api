﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.User.Overrides
{
    public class UserReportUserStatisticsOverride : IAutoMappingOverride<UserReportUserStatistics>
    {
        public void Override(AutoMapping<UserReportUserStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
