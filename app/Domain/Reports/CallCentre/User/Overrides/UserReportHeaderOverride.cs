﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.User.Overrides
{
    public class UserReportHeaderOverride : IAutoMappingOverride<UserReportHeader>
    {
        public void Override(AutoMapping<UserReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
