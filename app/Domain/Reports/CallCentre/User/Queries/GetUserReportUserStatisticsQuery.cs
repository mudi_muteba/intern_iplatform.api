﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.User.Queries.Filters;
using Domain.Reports.CallCentre.User.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.User.Queries
{
    public class GetUserReportUserStatisticsQuery : BaseStoredProcedureQuery<UserReportUserStatistics, UserReportUserStatisticsStoredProcedure>
    {
        public GetUserReportUserStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultUserReportUserStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<UserReportUserStatistics> Execute(IList<UserReportUserStatistics> query)
        {
            return query;
        }
    }
}
