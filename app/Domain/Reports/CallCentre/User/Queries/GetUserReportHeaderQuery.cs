﻿using System.Collections.Generic;

using MasterData.Authorisation;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.User.Queries.Filters;
using Domain.Reports.CallCentre.User.StoredProcedures;

namespace Domain.Reports.CallCentre.User.Queries
{
    public class GetUserReportHeaderQuery : BaseStoredProcedureQuery<UserReportHeader, UserReportHeaderStoredProcedure>
    {
        public GetUserReportHeaderQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultUserReportHeaderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<UserReportHeader> Execute(IList<UserReportHeader> query)
        {
            return query;
        }
    }
}
