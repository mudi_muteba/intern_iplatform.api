﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.User.Queries.Filters
{
    public class DefaultUserReportHeaderFilters : IApplyDefaultFilters<UserReportHeader>
    {
        public IQueryable<UserReportHeader> Apply(ExecutionContext executionContext, IQueryable<UserReportHeader> query)
        {
            return query;
        }
    }
}