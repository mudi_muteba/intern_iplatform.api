﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.User.Queries.Filters
{
    public class DefaultUserReportUserStatisticsFilters : IApplyDefaultFilters<UserReportUserStatistics>
    {
        public IQueryable<UserReportUserStatistics> Apply(ExecutionContext executionContext, IQueryable<UserReportUserStatistics> query)
        {
            return query;
        }
    }
}