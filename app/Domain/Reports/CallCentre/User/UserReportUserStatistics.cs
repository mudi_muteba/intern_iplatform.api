﻿using Domain.Base;

namespace Domain.Reports.CallCentre.User
{
    public class UserReportUserStatistics : Entity
    {
        public UserReportUserStatistics() { }

        public UserReportUserStatistics(object[] obj)
        {
            this.Channel = obj[0] != null ? (string) obj[0] : "";
            this.Role = obj[1] != null ? (string) obj[1] : "";
            this.Agent = obj[2] != null ? (string) obj[2] : "";
            this.EMail = obj[3] != null ? (string) obj[3] : "";
            this.Active = obj[4] != null ? (string) obj[4] : "";
            this.Billable = obj[5] != null ? (string) obj[5] : "";
        }

        public virtual string Channel { get; set; }
        public virtual string Role { get; set; }
        public virtual string Agent { get; set; }
        public virtual string EMail { get; set; }
        public virtual string Active { get; set; }
        public virtual string Billable { get; set; }
    }
       
}
