﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.User;

namespace Domain.Reports.CallCentre.User
{
    public class UserReport : ReportGenerator
    {
        public UserReport() : base() { }

        public virtual UserReportContainerDto Get(string channelIds, DateTime dateFrom, DateTime dateTo)
        {
            var criteria = new CallCentreReportCriteriaDto
            {
                ChannelIds = channelIds,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var response = Connector.ReportManagement.CallCentreReportConnector.GetUserReport(criteria).Response;
            return response;
        }
    }
}
