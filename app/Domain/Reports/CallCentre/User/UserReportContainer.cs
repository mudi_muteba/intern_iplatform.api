﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.User
{
    public class UserReportContainer : Entity
    {
        public UserReportContainer() { }

        public UserReportContainer(UserReportHeader header, List<UserReportUserStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual UserReportHeader Header { get; set; }
        public virtual List<UserReportUserStatistics> Statistics { get; set; }
    }
}
