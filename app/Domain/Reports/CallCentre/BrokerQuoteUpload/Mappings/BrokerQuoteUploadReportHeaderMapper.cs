﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Mappings
{
    public class BrokerQuoteUploadReportHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<BrokerQuoteUploadReportHeader, BrokerQuoteUploadReportHeaderDto>();

            Mapper.CreateMap<QueryResult<BrokerQuoteUploadReportHeaderDto>, POSTResponseDto<BrokerQuoteUploadReportHeaderDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<BrokerQuoteUploadReportHeader>>, POSTResponseDto<IList<BrokerQuoteUploadReportHeaderDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}