﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Mappings
{
    public class BrokerQuoteUploadReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<BrokerQuoteUploadReportContainer, BrokerQuoteUploadReportContainerDto>();

            Mapper.CreateMap<QueryResult<BrokerQuoteUploadReportContainerDto>, POSTResponseDto<BrokerQuoteUploadReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<BrokerQuoteUploadReportContainer>>, POSTResponseDto<IList<BrokerQuoteUploadReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}