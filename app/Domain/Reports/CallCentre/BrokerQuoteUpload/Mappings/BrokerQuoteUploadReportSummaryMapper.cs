﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Mappings
{
    public class BrokerQuoteUploadReportSummaryMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<BrokerQuoteUploadReportSummary, BrokerQuoteUploadReportSummaryDto>();

            Mapper.CreateMap<QueryResult<BrokerQuoteUploadReportSummaryDto>, POSTResponseDto<BrokerQuoteUploadReportSummaryDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<BrokerQuoteUploadReportSummary>>, POSTResponseDto<IList<BrokerQuoteUploadReportSummaryDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}