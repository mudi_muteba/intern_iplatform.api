﻿using Domain.Base;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReportSummary : Entity
    {
        public BrokerQuoteUploadReportSummary() { }

        public BrokerQuoteUploadReportSummary(object[] obj)
        {
            this.BrokerName = obj[0] != null ? (string) obj[0] : "";
            this.Agent = obj[1] != null ? (string) obj[1] : "";
            this.LeadsCount = obj[2] != null ? (int) obj[2] : 0;
            this.NoOfQuotes = obj[3] != null ? (int) obj[3] : 0;
            this.NoOfQuotesUploaded = obj[4] != null ? (int) obj[4] : 0;
            this.Closing = obj[5] != null ? (decimal) obj[5] : 0;
            this.LeadsSPV = obj[6] != null ? (decimal) obj[6] : 0;
        }

        public virtual string BrokerName { get; set; }
        public virtual string Agent { get; set; }
        public virtual int LeadsCount { get; set; }
        public virtual int NoOfQuotes { get; set; }
        public virtual int NoOfQuotesUploaded { get; set; }
        public virtual decimal Closing { get; set; }
        public virtual decimal LeadsSPV { get; set; }
    }

}