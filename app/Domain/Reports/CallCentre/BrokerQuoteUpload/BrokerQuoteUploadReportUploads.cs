﻿using System;

using Domain.Base;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReportUploads : Entity
    {
        public BrokerQuoteUploadReportUploads() { }

        public BrokerQuoteUploadReportUploads(object[] obj)
        {
            this.BrokerName = obj[0] != null ? (string) obj[0] : "";
            this.Campaign = obj[1] != null ? (string) obj[1] : "";
            this.CustomerName = obj[2] != null ? (string) obj[2] : "";
            this.CustomerId = obj[3] != null ? (string) obj[3] : "";
            this.QuoteNumber = obj[4] != null ? (string) obj[4] : "";
            this.ContactCentreAgent = obj[5] != null ? (string) obj[5] : "";
            this.DateTimeUploaded = obj[6] != null ? (DateTime) obj[6] : new DateTime(2000, 1, 1);
            this.QuotedPremium = obj[7] != null ? (decimal) obj[7] : 0;
        }

        public virtual string BrokerName { get; set; }
        public virtual string Campaign { get; set; }
        public virtual string CustomerName { get; set; }
        public virtual string CustomerId { get; set; }
        public virtual string QuoteNumber { get; set; }
        public virtual string ContactCentreAgent { get; set; }
        public virtual DateTime DateTimeUploaded { get; set; }
        public virtual decimal QuotedPremium { get; set; }
    }
}
