﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReportHeader : Entity
    {
        public BrokerQuoteUploadReportHeader() { }

        public BrokerQuoteUploadReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
