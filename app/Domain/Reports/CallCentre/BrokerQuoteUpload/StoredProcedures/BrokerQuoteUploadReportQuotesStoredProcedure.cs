﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures
{
    public class BrokerQuoteUploadReportQuotesStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_BrokerQuoteUpload_GetQuotes";
            }
        }

        public BrokerQuoteUploadReportQuotesStoredProcedure() { }

        public BrokerQuoteUploadReportQuotesStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new BrokerQuoteUploadReportQuotesStoredProcedureParameters
                {
                    ChannelIds = criteria.ChannelIds,
                    InsurerIds = criteria.InsurerIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}