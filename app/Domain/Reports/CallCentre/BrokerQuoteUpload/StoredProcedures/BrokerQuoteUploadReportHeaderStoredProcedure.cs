﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures
{
    public class BrokerQuoteUploadReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_BrokerQuoteUpload_Header";
            }
        }

        public BrokerQuoteUploadReportHeaderStoredProcedure() { }

        public BrokerQuoteUploadReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new BrokerQuoteUploadReportHeaderStoredProcedureParameters
                {
                    ChannelId = criteria.ChannelId
                });
        }
    }
}