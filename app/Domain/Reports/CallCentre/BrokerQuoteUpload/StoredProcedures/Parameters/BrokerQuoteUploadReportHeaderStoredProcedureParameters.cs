﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures.Parameters
{
    public class BrokerQuoteUploadReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ChannelId { get; set; }
    }
}