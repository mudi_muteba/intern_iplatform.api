﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures.Parameters
{
    public class BrokerQuoteUploadReportSummaryStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string ChannelIds { get; set; }
        public string InsurerIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}