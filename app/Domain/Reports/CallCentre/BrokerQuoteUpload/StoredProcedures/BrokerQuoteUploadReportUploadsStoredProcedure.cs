﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures
{
    public class BrokerQuoteUploadReportUploadsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_BrokerQuoteUpload_GetUploads";
            }
        }

        public BrokerQuoteUploadReportUploadsStoredProcedure() { }

        public BrokerQuoteUploadReportUploadsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new BrokerQuoteUploadReportSUploadsStoredProcedureParameters
                {
                    ChannelIds = criteria.ChannelIds,
                    InsurerIds = criteria.InsurerIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}