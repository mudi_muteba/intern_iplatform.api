﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.BrokerQuoteUpload.Queries.Filters;
using Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Queries
{
    public class GetBrokerQuoteUploadReportUploadsQuery : BaseStoredProcedureQuery<BrokerQuoteUploadReportUploads, BrokerQuoteUploadReportUploadsStoredProcedure>
    {
        public GetBrokerQuoteUploadReportUploadsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultBrokerQuoteUploadReportUploadsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<BrokerQuoteUploadReportUploads> Execute(IList<BrokerQuoteUploadReportUploads> query)
        {
            return query;
        }
    }
}
