﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Queries.Filters
{
    public class DefaultBrokerQuoteUploadReportSummaryFilters : IApplyDefaultFilters<BrokerQuoteUploadReportSummary>
    {
        public IQueryable<BrokerQuoteUploadReportSummary> Apply(ExecutionContext executionContext, IQueryable<BrokerQuoteUploadReportSummary> query)
        {
            return query;
        }
    }
}