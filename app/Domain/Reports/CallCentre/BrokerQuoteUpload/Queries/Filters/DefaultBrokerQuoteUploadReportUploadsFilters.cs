﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Queries.Filters
{
    public class DefaultBrokerQuoteUploadReportUploadsFilters : IApplyDefaultFilters<BrokerQuoteUploadReportUploads>
    {
        public IQueryable<BrokerQuoteUploadReportUploads> Apply(ExecutionContext executionContext, IQueryable<BrokerQuoteUploadReportUploads> query)
        {
            return query;
        }
    }
}