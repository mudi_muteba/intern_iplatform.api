﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Queries.Filters
{
    public class DefaultBrokerQuoteUploadReportHeaderFilters : IApplyDefaultFilters<BrokerQuoteUploadReportHeader>
    {
        public IQueryable<BrokerQuoteUploadReportHeader> Apply(ExecutionContext executionContext, IQueryable<BrokerQuoteUploadReportHeader> query)
        {
            return query;
        }
    }
}