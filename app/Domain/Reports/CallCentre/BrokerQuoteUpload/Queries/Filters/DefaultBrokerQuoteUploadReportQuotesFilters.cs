﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Queries.Filters
{
    public class DefaultBrokerQuoteUploadReportQuotesFilters : IApplyDefaultFilters<BrokerQuoteUploadReportQuotes>
    {
        public IQueryable<BrokerQuoteUploadReportQuotes> Apply(ExecutionContext executionContext, IQueryable<BrokerQuoteUploadReportQuotes> query)
        {
            return query;
        }
    }
}