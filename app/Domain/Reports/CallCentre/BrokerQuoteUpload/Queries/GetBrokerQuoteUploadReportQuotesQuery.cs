﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.BrokerQuoteUpload.Queries.Filters;
using Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Queries
{
    public class GetBrokerQuoteUploadReportQuotesQuery : BaseStoredProcedureQuery<BrokerQuoteUploadReportQuotes, BrokerQuoteUploadReportQuotesStoredProcedure>
    {
        public GetBrokerQuoteUploadReportQuotesQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultBrokerQuoteUploadReportQuotesFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<BrokerQuoteUploadReportQuotes> Execute(IList<BrokerQuoteUploadReportQuotes> query)
        {
            return query;
        }
    }
}
