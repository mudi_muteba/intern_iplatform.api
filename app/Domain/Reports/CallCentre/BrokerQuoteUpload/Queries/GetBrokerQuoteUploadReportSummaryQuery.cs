﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.BrokerQuoteUpload.Queries.Filters;
using Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Queries
{
    public class GetBrokerQuoteUploadReportSummaryQuery : BaseStoredProcedureQuery<BrokerQuoteUploadReportSummary, BrokerQuoteUploadReportSummaryStoredProcedure>
    {
        public GetBrokerQuoteUploadReportSummaryQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultBrokerQuoteUploadReportSummaryFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<BrokerQuoteUploadReportSummary> Execute(IList<BrokerQuoteUploadReportSummary> query)
        {
            return query;
        }
    }
}
