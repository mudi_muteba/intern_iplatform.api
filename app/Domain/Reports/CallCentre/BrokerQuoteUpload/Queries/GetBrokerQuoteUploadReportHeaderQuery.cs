﻿using System.Collections.Generic;

using MasterData.Authorisation;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures;
using Domain.Reports.CallCentre.BrokerQuoteUpload.Queries.Filters;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Queries
{
    public class GetBrokerQuoteUploadReportHeaderQuery : BaseStoredProcedureQuery<BrokerQuoteUploadReportHeader, BrokerQuoteUploadReportHeaderStoredProcedure>
    {
        public GetBrokerQuoteUploadReportHeaderQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultBrokerQuoteUploadReportHeaderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<BrokerQuoteUploadReportHeader> Execute(IList<BrokerQuoteUploadReportHeader> query)
        {
            return query;
        }
    }
}
