﻿using System;

using Domain.Base;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReportQuotes : Entity
    {
        public BrokerQuoteUploadReportQuotes() { }

        public BrokerQuoteUploadReportQuotes(object[] obj)
        {
            this.BrokerName = obj[0] != null ? (string) obj[0] : "";
            this.Campaign = obj[1] != null ? (string) obj[1] : "";
            this.CustomerName = obj[2] != null ? (string) obj[2] : "";
            this.CustomerId = obj[3] != null ? (string) obj[3] : "";
            this.QuoteNumber = obj[4] != null ? (string) obj[4] : "";
            this.ContactCentreAgent = obj[5] != null ? (string) obj[5] : "";
            this.QuoteCreateDate = obj[6] != null ? (DateTime) obj[6] : new DateTime(2000, 1, 1);
            this.QuotedPremium = obj[7] != null ? (decimal) obj[7] : 0;
            this.Status = obj[8] != null ? (string) obj[8] : "";
            this.Reason = obj[9] != null ? (string) obj[9] : "";
            this.BrokerFee = obj[10] != null ? (decimal) obj[10] : 0;
        }

        public virtual string BrokerName { get; set; }
        public virtual string Campaign { get; set; }
        public virtual string CustomerName { get; set; }
        public virtual string CustomerId { get; set; }
        public virtual string QuoteNumber { get; set; }
        public virtual string ContactCentreAgent { get; set; }
        public virtual DateTime QuoteCreateDate { get; set; }
        public virtual decimal QuotedPremium { get; set; }
        public virtual string Status { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal BrokerFee { get; set; }
    }
       
}
