﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReportContainer : Entity
    {
        public BrokerQuoteUploadReportContainer() { }

        public BrokerQuoteUploadReportContainer(BrokerQuoteUploadReportHeader header, List<BrokerQuoteUploadReportSummary> summary, List<BrokerQuoteUploadReportQuotes> quotes, List<BrokerQuoteUploadReportUploads> uploads)
        {
            this.Header = header;
            this.Summary = summary;
            this.Quotes = quotes;
            this.Uploads = uploads;
        }

        public virtual BrokerQuoteUploadReportHeader Header { get; set; }
        public virtual List<BrokerQuoteUploadReportSummary> Summary { get; set; }
        public virtual List<BrokerQuoteUploadReportQuotes> Quotes { get; set; }
        public virtual List<BrokerQuoteUploadReportUploads> Uploads { get; set; }
    }
}