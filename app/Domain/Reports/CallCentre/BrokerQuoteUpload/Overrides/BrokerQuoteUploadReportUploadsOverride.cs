﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Overrides
{
    public class BrokerQuoteUploadReportUploadsOverride : IAutoMappingOverride<BrokerQuoteUploadReportUploads>
    {
        public void Override(AutoMapping<BrokerQuoteUploadReportUploads> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
