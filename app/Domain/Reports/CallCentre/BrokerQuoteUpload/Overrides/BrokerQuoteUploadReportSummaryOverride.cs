﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Overrides
{
    public class BrokerQuoteUploadReportSummaryOverride : IAutoMappingOverride<BrokerQuoteUploadReportSummary>
    {
        public void Override(AutoMapping<BrokerQuoteUploadReportSummary> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
