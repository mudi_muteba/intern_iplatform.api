﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Overrides
{
    public class BrokerQuoteUploadReportQuotesOverride : IAutoMappingOverride<BrokerQuoteUploadReportQuotes>
    {
        public void Override(AutoMapping<BrokerQuoteUploadReportQuotes> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
