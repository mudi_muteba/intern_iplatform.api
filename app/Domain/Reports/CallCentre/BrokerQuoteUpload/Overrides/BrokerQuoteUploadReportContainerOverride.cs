﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Overrides
{
    public class BrokerQuoteUploadReportContainerOverride : IAutoMappingOverride<BrokerQuoteUploadReportContainer>
    {
        public void Override(AutoMapping<BrokerQuoteUploadReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
