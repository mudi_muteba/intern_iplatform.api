﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload.Overrides
{
    public class BrokerQuoteUploadReportHeaderOverride : IAutoMappingOverride<BrokerQuoteUploadReportHeader>
    {
        public void Override(AutoMapping<BrokerQuoteUploadReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
