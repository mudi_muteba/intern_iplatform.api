﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload;

namespace Domain.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReport : ReportGenerator
    {
        public BrokerQuoteUploadReport() : base() { }

        public virtual BrokerQuoteUploadReportContainerDto Get(string channelIds, string insurerIds, DateTime dateFrom, DateTime dateTo)
        {
            var criteria = new CallCentreReportCriteriaDto
            {
                ChannelIds = channelIds,
                InsurerIds = insurerIds,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var response = Connector.ReportManagement.CallCentreReportConnector.GetBrokerQuoteUploadsReport(criteria).Response;
            return response;
        }
    }
}
