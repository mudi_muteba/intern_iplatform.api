﻿using Domain.Base;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct
{
    public class AgentPerformanceByProductReportAgentQuoteStatistics : Entity
    {
        public AgentPerformanceByProductReportAgentQuoteStatistics() { }

        public AgentPerformanceByProductReportAgentQuoteStatistics(object[] obj)
        {
            this.Channel = obj[0] != null ? (string) obj[0] : "";
            this.Campaign = obj[1] != null ? (string) obj[1] : "";
            this.Agent = obj[2] != null ? (string) obj[2] : "";
            this.Insurer = obj[3] != null ? (string) obj[3] : "";
            this.Product = obj[4] != null ? (string) obj[4] : "";
            this.Leads = obj[5] != null ? (int) obj[5] : 0;
            this.Quotes = obj[6] != null ? (int) obj[6] : 0;
            this.QuotesPerLead = obj[7] != null ? (decimal) obj[7] : 0;
            this.SecondLevelUnderwriting = obj[8] != null ? (int) obj[8] : 0;
            this.Sales = obj[9] != null ? (int) obj[9] : 0;
            this.Closing = obj[10] != null ? (decimal) obj[10] : 0;
            this.InsurerClosing = obj[11] != null ? (decimal) obj[11] : 0;
            this.InsurerDropOff = obj[12] != null ? (decimal) obj[12] : 0;
            this.AveragePremiumPerAcceptedQuote = obj[13] != null ? (decimal) obj[13] : 0;
        }

        public virtual string Channel { get; set; }
        public virtual string Campaign { get; set; }
        public virtual string Agent { get; set; }
        public virtual string Insurer { get; set; }
        public virtual string Product { get; set; }

        public virtual int Leads { get; set; }
        public virtual int Quotes { get; set; }
        public virtual decimal QuotesPerLead { get; set; }
        public virtual int SecondLevelUnderwriting { get; set; }
        public virtual int Sales { get; set; }

        public virtual decimal Closing { get; set; }
        public virtual decimal InsurerClosing { get; set; }
        public virtual decimal InsurerDropOff { get; set; }
        public virtual decimal AveragePremiumPerAcceptedQuote { get; set; }
    }
       
}
