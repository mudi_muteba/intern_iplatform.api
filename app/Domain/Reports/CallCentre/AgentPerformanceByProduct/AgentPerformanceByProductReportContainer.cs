﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct
{
    public class AgentPerformanceByProductReportContainer : Entity
    {
        public AgentPerformanceByProductReportContainer() { }

        public AgentPerformanceByProductReportContainer(AgentPerformanceByProductReportHeader header, List<AgentPerformanceByProductReportAgentQuoteStatistics> statistics)
        {
            this.Header = header;
            this.Statistics = statistics;
        }

        public virtual AgentPerformanceByProductReportHeader Header { get; set; }
        public virtual List<AgentPerformanceByProductReportAgentQuoteStatistics> Statistics { get; set; }
    }
}
