﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.StoredProcedures.Parameters
{
    public class AgentPerformanceByProductReportHeaderStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int CampaignId { get; set; }
        public string CampaignIds { get; set; }
    }
}