﻿using System;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.StoredProcedures.Parameters
{
    public class AgentPerformanceByProductReportAgentQuoteStatisticsStoredProcedureParameters : IStoredProcedureCriteria
    {
        public string CampaignIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo {get; set; }
    }
}