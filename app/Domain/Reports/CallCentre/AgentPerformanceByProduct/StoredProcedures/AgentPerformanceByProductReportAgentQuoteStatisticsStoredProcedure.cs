﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.AgentPerformanceByProduct.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.StoredProcedures
{
    public class AgentPerformanceByProductReportAgentQuoteStatisticsStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics";
            }
        }

        public AgentPerformanceByProductReportAgentQuoteStatisticsStoredProcedure() { }

        public AgentPerformanceByProductReportAgentQuoteStatisticsStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AgentPerformanceByProductReportAgentQuoteStatisticsStoredProcedureParameters
                {
                    CampaignIds = criteria.CampaignIds,
                    DateFrom = criteria.DateFrom,
                    DateTo = criteria.DateTo
                });
        }
    }
}