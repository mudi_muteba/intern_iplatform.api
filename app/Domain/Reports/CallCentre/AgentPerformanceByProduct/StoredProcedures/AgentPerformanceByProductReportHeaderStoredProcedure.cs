﻿using Domain.Base.Repository.StoredProcedures;
using Domain.Reports.CallCentre.AgentPerformanceByProduct.StoredProcedures.Parameters;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.StoredProcedures
{
    public class AgentPerformanceByProductReportHeaderStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "Report_CallCentre_AgentPerformanceByProduct_Header";
            }
        }

        public AgentPerformanceByProductReportHeaderStoredProcedure() { }

        public AgentPerformanceByProductReportHeaderStoredProcedure(CallCentreReportCriteriaDto criteria) : base()
        {
            base.
                SetParameters(new AgentPerformanceByProductReportHeaderStoredProcedureParameters
                {
                    CampaignId = criteria.CampaignId,
                    CampaignIds = criteria.CampaignIds
                });
        }
    }
}