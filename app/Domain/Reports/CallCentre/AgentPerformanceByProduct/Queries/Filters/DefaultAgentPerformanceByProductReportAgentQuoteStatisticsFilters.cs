﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.Queries.Filters
{
    public class DefaultAgentPerformanceByProductReportAgentQuoteStatisticsFilters : IApplyDefaultFilters<AgentPerformanceByProductReportAgentQuoteStatistics>
    {
        public IQueryable<AgentPerformanceByProductReportAgentQuoteStatistics> Apply(ExecutionContext executionContext, IQueryable<AgentPerformanceByProductReportAgentQuoteStatistics> query)
        {
            return query;
        }
    }
}