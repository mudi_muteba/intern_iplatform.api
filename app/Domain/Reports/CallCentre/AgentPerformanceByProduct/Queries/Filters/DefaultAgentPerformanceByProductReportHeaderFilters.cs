﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.Queries.Filters
{
    public class DefaultAgentPerformanceByProductReportHeaderFilters : IApplyDefaultFilters<AgentPerformanceByProductReportHeader>
    {
        public IQueryable<AgentPerformanceByProductReportHeader> Apply(ExecutionContext executionContext, IQueryable<AgentPerformanceByProductReportHeader> query)
        {
            return query;
        }
    }
}