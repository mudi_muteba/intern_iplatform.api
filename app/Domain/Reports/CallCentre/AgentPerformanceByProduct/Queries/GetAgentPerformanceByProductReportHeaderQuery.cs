﻿using System.Collections.Generic;

using MasterData.Authorisation;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.AgentPerformanceByProduct.StoredProcedures;
using Domain.Reports.CallCentre.AgentPerformanceByProduct.Queries.Filters;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.Queries
{
    public class GetAgentPerformanceByProductReportHeaderQuery : BaseStoredProcedureQuery<AgentPerformanceByProductReportHeader, AgentPerformanceByProductReportHeaderStoredProcedure>
    {
        public GetAgentPerformanceByProductReportHeaderQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultAgentPerformanceByProductReportHeaderFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AgentPerformanceByProductReportHeader> Execute(IList<AgentPerformanceByProductReportHeader> query)
        {
            return query;
        }
    }
}
