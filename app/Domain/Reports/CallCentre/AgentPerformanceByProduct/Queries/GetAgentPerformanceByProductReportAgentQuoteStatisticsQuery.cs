﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.AgentPerformanceByProduct.StoredProcedures;
using Domain.Reports.CallCentre.AgentPerformanceByProduct.Queries.Filters;

using MasterData.Authorisation;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.Queries
{
    public class GetAgentPerformanceByProductReportAgentQuoteStatisticsQuery : BaseStoredProcedureQuery<AgentPerformanceByProductReportAgentQuoteStatistics, AgentPerformanceByProductReportAgentQuoteStatisticsStoredProcedure>
    {
        public GetAgentPerformanceByProductReportAgentQuoteStatisticsQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new DefaultAgentPerformanceByProductReportAgentQuoteStatisticsFilters())
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<AgentPerformanceByProductReportAgentQuoteStatistics> Execute(IList<AgentPerformanceByProductReportAgentQuoteStatistics> query)
        {
            return query;
        }
    }
}
