﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformanceByProduct;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.Mappings
{
    public class AgentPerformanceByProductReportContainerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AgentPerformanceByProductReportContainer, AgentPerformanceByProductReportContainerDto>();

            Mapper.CreateMap<QueryResult<AgentPerformanceByProductReportContainerDto>, POSTResponseDto<AgentPerformanceByProductReportContainerDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<AgentPerformanceByProductReportContainer>>, POSTResponseDto<IList<AgentPerformanceByProductReportContainerDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}