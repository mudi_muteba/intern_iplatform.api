﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Execution;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformanceByProduct;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.Mappings
{
    public class AgentPerformanceByProductReportAgentQuoteStatisticsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AgentPerformanceByProductReportAgentQuoteStatistics, AgentPerformanceByProductReportAgentQuoteStatisticsDto>();

            Mapper.CreateMap<QueryResult<AgentPerformanceByProductReportAgentQuoteStatisticsDto>, POSTResponseDto<AgentPerformanceByProductReportAgentQuoteStatisticsDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<IList<AgentPerformanceByProductReportAgentQuoteStatistics>>, POSTResponseDto<IList<AgentPerformanceByProductReportAgentQuoteStatisticsDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}