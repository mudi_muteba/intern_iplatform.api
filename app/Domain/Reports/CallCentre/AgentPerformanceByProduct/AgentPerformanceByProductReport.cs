﻿using System;

using Domain.Reports.Base;

using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformanceByProduct;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct
{
    public class AgentPerformanceByProductReport : ReportGenerator
    {
        public AgentPerformanceByProductReport() : base() { }

        public virtual AgentPerformanceByProductReportContainerDto Get(string campaignIds, DateTime dateFrom, DateTime dateTo)
        {
            var criteria = new CallCentreReportCriteriaDto
            {
                CampaignIds = campaignIds,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var response = Connector.ReportManagement.CallCentreReportConnector.GetAgentPerformanceByProductReport(criteria).Response;
            return response;
        }
    }
}
