﻿using System.Collections.Generic;

using Domain.Base;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct
{
    public class AgentPerformanceByProductReportHeader : Entity
    {
        public AgentPerformanceByProductReportHeader() { }

        public AgentPerformanceByProductReportHeader(object[] obj)
        {
            this.Insurer = obj[0] != null ? (string) obj[0] : "";
        }

        public virtual string Insurer { get; set; }
    }
       
}
