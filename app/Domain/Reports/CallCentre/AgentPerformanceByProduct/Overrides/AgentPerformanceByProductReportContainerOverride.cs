﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.Overrides
{
    public class AgentPerformanceByProductReportContainerOverride : IAutoMappingOverride<AgentPerformanceByProductReportContainer>
    {
        public void Override(AutoMapping<AgentPerformanceByProductReportContainer> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
