﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.Overrides
{
    public class AgentPerformanceByProductReportAgentQuoteStatisticsOverride : IAutoMappingOverride<AgentPerformanceByProductReportAgentQuoteStatistics>
    {
        public void Override(AutoMapping<AgentPerformanceByProductReportAgentQuoteStatistics> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
