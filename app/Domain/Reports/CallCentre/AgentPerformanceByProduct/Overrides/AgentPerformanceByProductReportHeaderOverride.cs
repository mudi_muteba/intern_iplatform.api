﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Reports.CallCentre.AgentPerformanceByProduct.Overrides
{
    public class AgentPerformanceByProductReportHeaderOverride : IAutoMappingOverride<AgentPerformanceByProductReportHeader>
    {
        public void Override(AutoMapping<AgentPerformanceByProductReportHeader> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
