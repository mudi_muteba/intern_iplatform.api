﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Sales;
using Domain.Tia.Handlers.PartiesSearch;
using iPlatform.Api.DTOs.Sales;
using iPlatform.Api.DTOs.Tia.Parties;
using MasterData.Authorisation;

namespace Domain.Tia.Handlers
{
    public class TiaPartiesSearchDtoHandler : BaseDtoHandler<TiaPartiesSearchDto, TiaPartySearchResultDto>
    {
        public TiaPartiesSearchDtoHandler(IProvideContext contextProvider) : base(contextProvider)
        {
        }
        
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint> { }; }
        }

        protected override void InternalHandle(TiaPartiesSearchDto dto, HandlerResult<TiaPartySearchResultDto> result)
        {
            var tiaResult = new TiaPartiesSearchContainer().Search(dto);

            var item = Mapper.Map<TiaPartySearchResultDto>(tiaResult.Parties.FirstOrDefault());

            result.Processed(item);
        }
    }
}
