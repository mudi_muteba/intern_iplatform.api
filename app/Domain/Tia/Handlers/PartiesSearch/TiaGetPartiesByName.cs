using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Tia.Parties;
using RestSharp;

namespace Domain.Tia.Handlers.PartiesSearch
{
    public class TiaGetPartiesByName : TiaBasePartiesSearch
    {
        protected override List<TiaPartySearchResult> GetParties(TiaPartiesSearchDto searchCriteria)
        {
            var queryResult = new TiaRequest().Request<List<TiaPartySearchResult>>(String.Format("/parties?name={0}", searchCriteria.Name.ToLower()),
                Method.GET);

            return queryResult;
        }
    }
}