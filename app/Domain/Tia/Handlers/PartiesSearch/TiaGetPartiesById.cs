using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Tia.Parties;
using RestSharp;

namespace Domain.Tia.Handlers.PartiesSearch
{
    public class TiaGetPartiesById : TiaBasePartiesSearch
    {
        protected override List<TiaPartySearchResult> GetParties(TiaPartiesSearchDto searchCriteria)
        {
            var list = new List<TiaPartySearchResult>();
            var queryResult = new TiaRequest().Request<TiaPartySearchResult>(String.Format("/parties/{0}", searchCriteria.Id),
                Method.GET);
            list.Add(queryResult);
            return list;
        }
    }
}