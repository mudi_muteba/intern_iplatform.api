using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Tia.Parties;
using Newtonsoft.Json;
using RestSharp;

namespace Domain.Tia.Handlers.PartiesSearch
{
    public abstract class TiaBasePartiesSearch
    {
        public List<TiaPartySearchResult> Parties { get; set; }


        public TiaBasePartiesSearch Get(TiaPartiesSearchDto searchCriteria)
        {
            Parties = this.GetParties(searchCriteria);

            Parties.ForEach(a=> a.SetContactDetail(GetContactDetail(a.id)));
            return this;
        }

        public dynamic GetContactDetail(string id)
        {
            var queryResult = new TiaRequest().Request(String.Format("/parties/{0}/contactDetails", id),
                Method.GET);

            return queryResult;

        }

        protected abstract List<TiaPartySearchResult> GetParties(TiaPartiesSearchDto searchCriteria);

    }

}