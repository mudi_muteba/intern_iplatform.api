using System.Collections.Generic;
using iPlatform.Api.DTOs.Tia.Parties;

namespace Domain.Tia.Handlers.PartiesSearch
{
    /// <summary>
    /// Decides which Tia search to apply based on the search Dto
    /// </summary>
    public class TiaPartiesSearchContainer
    {
        public TiaBasePartiesSearch Search(TiaPartiesSearchDto searchCriteria)
        {
            TiaBasePartiesSearch result = new TiaGetPartiesByName();

            if (!string.IsNullOrWhiteSpace(searchCriteria.Name))
                result = new TiaGetPartiesByName().Get(searchCriteria);

            if (searchCriteria.Id > 0)
                result = new TiaGetPartiesById().Get(searchCriteria);

            return result;
        }
    }
}