using Newtonsoft.Json;
using RestSharp;

namespace Domain.Tia.Handlers
{
    public class TiaRequest
    {
        protected RestClient _client;
        protected RestRequest _request;

        public TiaRequest()
        {
            _client = new RestClient("http://rest.cloud4.tia.dk/TIARest/api");
        }

        public TRetun Request<TRetun>(string request, Method method) where TRetun : new()
        {
            _request = new RestRequest(request, method);
            _request.AddHeader("X-TIA-User", "TIA");
            var queryResult = _client.Execute<TRetun>(_request).Data;

            return queryResult;


        }

        public dynamic Request(string request, Method method) 
        {
            _request = new RestRequest(request, method);
            _request.AddHeader("X-TIA-User", "TIA");
            var queryResult = _client.Execute(_request);

            var result = JsonConvert.DeserializeObject<dynamic>(queryResult.Content);

            return result;

        }


    }
}