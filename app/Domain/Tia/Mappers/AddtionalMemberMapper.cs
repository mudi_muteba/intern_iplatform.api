﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Domain.Party.ProposalDefinitions;
using iPlatform.Api.DTOs.AdditionalMembers;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Tia.Parties;
using MasterData;

namespace Domain.Tia.Mappers
{
    public class TiaMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<TiaPartySearchResult, TiaPartySearchResultDto>()
                .ForMember(t => t.AddressLine1, o => o.MapFrom(s => s.streetNo))
                .ForMember(t => t.AddressLine2, o => o.MapFrom(s => s.street))
                .ForMember(t => t.AddressLine3, o => o.MapFrom(s => s.city))
                .ForMember(t => t.AddressPostalCode, o => o.MapFrom(s => s.addressCode))
                .ForMember(t => t.ClientFullName, o => o.MapFrom(s => s.name))
                .ForMember(t => t.LastName, o => o.MapFrom(s => s.surname))
                .ForMember(t => t.FirstName, o => o.MapFrom(s => s.forename))
                ;

      
        }
    }
}