﻿using Domain.Base;
using Domain.Campaigns;
using Domain.Tags;
using System;

namespace Domain.Tags
{
    [Serializable]
    public class TagCampaignMap : Entity
    {
        public virtual Tag Tag { get; set; }
        public virtual Campaign Campaign { get; set; }
    }
}