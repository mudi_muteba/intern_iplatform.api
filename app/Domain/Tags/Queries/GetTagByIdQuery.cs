﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Tags.Queries
{
    public class GetTagByIdQuery : BaseQuery<Tag>
    {
        private int id;

        public GetTagByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Tag>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetTagByIdQuery WithId(int id)
        {
            this.id = id;
            return this;
        }

        protected internal override IQueryable<Tag> Execute(IQueryable<Tag> query)
        {
            return query.Where(x => x.Id == id);
        }
    }
}