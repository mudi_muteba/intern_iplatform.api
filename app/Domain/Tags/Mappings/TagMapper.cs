﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using Domain.Products.Fees;
using iPlatform.Api.DTOs.Tags;
using MasterData;
using Domain.Campaigns;

namespace Domain.Tags.Mappings
{
    public class TagMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Tag, TagDto>();

            Mapper.CreateMap<TagCampaignMap, TagCampaignDto>();
        }
    }
}