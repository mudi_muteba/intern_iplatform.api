using Domain.Products;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Tags.Overrides
{
    public class TagProductMapOverride : IAutoMappingOverride<TagProductMap>
    {
        public void Override(AutoMapping<TagProductMap> mapping)
        {
            mapping.References(x => x.Product, "ProductId");
            mapping.References(x => x.Tag, "TagId");
        }
    }
}