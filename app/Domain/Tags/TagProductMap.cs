﻿using Domain.Base;
using Domain.Products;

namespace Domain.Tags
{
    public class TagProductMap : Entity
    {
        public virtual Tag Tag { get; set; }
        public virtual ProductReference Product { get; set; }
    }
}