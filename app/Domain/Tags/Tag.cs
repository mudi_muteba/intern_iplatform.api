﻿using Domain.Base;

namespace Domain.Tags
{
    public class Tag : Entity
    {
        public Tag()
        {
        }

        public Tag(string name)
        {
            Name = name;
        }

        public virtual string Name { get; protected internal set; }
    }
}