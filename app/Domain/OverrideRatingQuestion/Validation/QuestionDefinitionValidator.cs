﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Validation;
using Domain.OverrideRatingQuestion.Queries;
using Domain.QuestionDefinitions;
using iPerson.Api.DTOs;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using ValidationMessages;

namespace Domain.OverrideRatingQuestion.Validation
{
    public class QuestionDefinitionValidator : IValidateDto<CreateOverrideRatingQuestionDto>, IValidateDto<EditOverrideRatingQuestionDto>
    {
        private readonly GetQuestionDefinitionByCoverDefinitionIdAndQuestionIdQuery _Query;

        public QuestionDefinitionValidator(GetQuestionDefinitionByCoverDefinitionIdAndQuestionIdQuery query)
        {
            _Query = query;
        }

        public void Validate(CreateOverrideRatingQuestionDto dto, ExecutionResult result)
        {
            IQueryable<QuestionDefinition> questionDefinitions =
                GetQuestionDefinitionByCoverDefinitionIdAndQuestionId(dto);

            if(!questionDefinitions.Any())
                result.AddValidationMessage(OverrideRatingQuestionValidationMessage.QuestionDefinitionIdNotFound);
        }

        public void Validate(EditOverrideRatingQuestionDto dto, ExecutionResult result)
        {
            IQueryable<QuestionDefinition> questionDefinitions =
                GetQuestionDefinitionByCoverDefinitionIdAndQuestionId(dto);

            if (!questionDefinitions.Any())
                result.AddValidationMessage(OverrideRatingQuestionValidationMessage.QuestionDefinitionIdNotFound);
        }

        private IQueryable<QuestionDefinition> GetQuestionDefinitionByCoverDefinitionIdAndQuestionId(
            CreateOverrideRatingQuestionDto dto)
        {
            return _Query.WithCoverDefinitionIdAndQuestionId(dto).ExecuteQuery();
        }

        private IQueryable<QuestionDefinition> GetQuestionDefinitionByCoverDefinitionIdAndQuestionId(
            EditOverrideRatingQuestionDto dto)
        {
            return _Query.WithCoverDefinitionIdAndQuestionId(dto).ExecuteQuery();
        }
    }
}
