﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Validation;
using Domain.OverrideRatingQuestion.Queries;
using Domain.Products;
using iPerson.Api.DTOs;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using ValidationMessages;

namespace Domain.OverrideRatingQuestion.Validation
{
    public class CoverDefinitionValidator : IValidateDto<CreateOverrideRatingQuestionDto>, IValidateDto<EditOverrideRatingQuestionDto>
    {
        private readonly GetCoverDefinitionByProductIdAndCoverIdQuery m_Query;

        public CoverDefinitionValidator(GetCoverDefinitionByProductIdAndCoverIdQuery query)
        {
            m_Query = query;
        }

        public void Validate(CreateOverrideRatingQuestionDto dto, ExecutionResult result)
        {
            Validate(dto.ProductId, dto.CoverId, result);
        }

        public void Validate(EditOverrideRatingQuestionDto dto, ExecutionResult result)
        {
            Validate(dto.ProductId, dto.CoverId, result);
        }

        private void Validate(int productId, int coverId, ExecutionResult result)
        {
            IQueryable<CoverDefinition> coverDefinitions = GetCoverDefinitionByProductIdAndCoverId(productId, coverId);
            if(!coverDefinitions.Any())
                result.AddValidationMessage(OverrideRatingQuestionValidationMessage.CoverDefinitionIdNotFound);
        }

        private IQueryable<CoverDefinition> GetCoverDefinitionByProductIdAndCoverId(int productId, int coverId)
        {
            return m_Query.WithProductIdAndCoverId(productId, coverId)
                .ExecuteQuery();
        }
    }
}
