﻿using AutoMapper;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition;

namespace Domain.OverrideRatingQuestion.Mappings
{
    public class OverrideRatingQuestionMapVapQuestionDefinitionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateOverrideRatingQuestionMapVapQuestionDefinitionDto, OverrideRatingQuestionMapVapQuestionDefinition>()
                .ForMember(d => d.MapVapQuestionDefinition, o => o.MapFrom(s => new MapVapQuestionDefinition() {Id = s.MapVapQuestionDefinitionId}));

            Mapper.CreateMap<EditOverrideRatingQuestionMapVapQuestionDefinitionDto, OverrideRatingQuestionMapVapQuestionDefinition>();

            Mapper.CreateMap<OverrideRatingQuestionMapVapQuestionDefinitionDto, OverrideRatingQuestionMapVapQuestionDefinition>()
                .ReverseMap();
        }
    }
}