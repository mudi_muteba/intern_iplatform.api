﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;

namespace Domain.OverrideRatingQuestion.Mappings
{
    public class OverrideRatingQuestionChannelMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateOverrideRatingQuestionChannelDto, OverrideRatingQuestionChannel>()
                .ForMember(c => c.Channel, o => o.MapFrom(s => new Channel(s.ChannelId)));

            Mapper.CreateMap<EditOverrideRatingQuestionChannelDto, OverrideRatingQuestionChannel>()
                .ForMember(c => c.Channel, o => o.MapFrom(s => new Channel(s.ChannelId)));

            Mapper.CreateMap<OverrideRatingQuestionChannelDto, OverrideRatingQuestionChannel>()
                .ReverseMap();            
        }
    }
}
