﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Products;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition;

namespace Domain.OverrideRatingQuestion.Mappings
{
    public class OverrideRatingQuestionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateOverrideRatingQuestionDto, OverrideRatingQuestion>()
                .ForMember(d => d.CoverDefinition, o => o.MapFrom(s => new CoverDefinition() { Id = s.CoverDefinitionId }))
                .ForMember(d => d.QuestionDefinition, o => o.MapFrom(s => new QuestionDefinition { Id = s.QuestionDefinitionId }));

            Mapper.CreateMap<EditOverrideRatingQuestionDto, OverrideRatingQuestion>()
                .ForMember(d => d.CoverDefinition, o => o.MapFrom(s => new CoverDefinition() { Id = s.CoverDefinitionId }))
                .ForMember(d => d.QuestionDefinition, o => o.MapFrom(s => new QuestionDefinition { Id = s.QuestionDefinitionId }))
                .ForMember(d => d.OverrideRatingQuestionChannels, o => o.Ignore())
                .ForMember(d => d.OverrideRatingQuestionMapVapQuestionDefinitions, o => o.Ignore());

            Mapper.CreateMap<OverrideRatingQuestion, OverrideRatingQuestionDto>()
                .ForMember(d => d.OverrideRatingQuestionChannels, o => o.MapFrom(s => s.OverrideRatingQuestionChannels))
                .ForMember(d => d.OverrideRatingQuestionMapVapQuestionDefinitions, o => o.MapFrom(s => s.OverrideRatingQuestionMapVapQuestionDefinitions));

            Mapper.CreateMap<OverrideRatingQuestion, ListOverrideRatingQuestionDto>()
                .ForMember(d => d.ProductName, o => o.MapFrom(s => s.CoverDefinition.Product.Name))
                .ForMember(d => d.CoverName, o => o.MapFrom(s => s.CoverDefinition.Cover.Name))
                .ForMember(d => d.QuestionGroupName, o => o.MapFrom(s => s.QuestionDefinition.Question.QuestionGroup.Name))
                .ForMember(d => d.QuestionName, o => o.MapFrom(s => s.QuestionDefinition.Question.Name))
                .ForMember(d => d.OverrideRatingQuestionChannels, o => o.MapFrom(s => s.OverrideRatingQuestionChannels))
                .ForMember(d => d.OverrideRatingQuestionMapVapQuestionDefinitions, o => o.MapFrom(s => s.OverrideRatingQuestionMapVapQuestionDefinitions));

            Mapper.CreateMap<IEnumerable<OverrideRatingQuestion>, ListResultDto<ListOverrideRatingQuestionDto>>()
                .ForMember(d => d.Results, o => o.MapFrom(s => s.ToList()));

            Mapper.CreateMap<PagedResults<OverrideRatingQuestion>, PagedResultDto<ListOverrideRatingQuestionDto>>();

            Mapper.CreateMap<OverrideRatingQuestionDto, OverrideRatingQuestionAnswerDto>()
                .ForMember(d => d.Cover, o => o.MapFrom(s => s.CoverDefinition.Cover ))
                .ForMember(d => d.OverrideQuestion, o => o.MapFrom(s => s.QuestionDefinition.Question ))
                .ForMember(d => d.ProductCode, o => o.MapFrom(s => s.CoverDefinition.Product.Code))
                .ForMember(d => d.OverrideValue, o => o.MapFrom(s => s.OverrideValue));
        }
    }
}
