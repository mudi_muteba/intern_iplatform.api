﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base;
using Domain.OverrideRatingQuestion.Maintainers;
using Domain.Products;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition;
using Infrastructure.NHibernate.Attributes;

namespace Domain.OverrideRatingQuestion
{
    public class OverrideRatingQuestion : EntityWithAudit
    {
        public virtual CoverDefinition CoverDefinition { get; set; }

        public virtual QuestionDefinition QuestionDefinition { get; set; }

        public virtual string OverrideValue { get; set; }

        public virtual IList<OverrideRatingQuestionChannel> OverrideRatingQuestionChannels { get; set; }

        public virtual IList<OverrideRatingQuestionMapVapQuestionDefinition> OverrideRatingQuestionMapVapQuestionDefinitions { get; set; }

        public static OverrideRatingQuestion Create(CreateOverrideRatingQuestionDto createOverrideRatingQuestionDto)
        {
            OverrideRatingQuestion overrideRatingQuestion = Mapper.Map<OverrideRatingQuestion>(createOverrideRatingQuestionDto);
            return overrideRatingQuestion;
        }

        public virtual void Update(EditOverrideRatingQuestionDto editOverrideRatingQuestionDto)
        {
            AllocateOverrideRatingQuestionChannels(editOverrideRatingQuestionDto.OverrideRatingQuestionChannels);
            AllocateOverrideRatingQuestionMapVapQuestionDefinitions(editOverrideRatingQuestionDto.OverrideRatingQuestionMapVapQuestionDefinitions);
            Mapper.Map(editOverrideRatingQuestionDto, this);
        }

        public virtual void Delete(DeleteOverrideRatingQuestionDto deleteOverrideRatingQuestionDto)
        {
            Delete();
        }

        public virtual void AllocateOverrideRatingQuestionChannels(List<EditOverrideRatingQuestionChannelDto> editOverrideRatingQuestionChannelDtos)
        {
            OverrideRatingQuestionChannelMaintainer maintainer = new OverrideRatingQuestionChannelMaintainer(this);
            maintainer.MergeChanges(editOverrideRatingQuestionChannelDtos.Select(o => o.ChannelId).ToList());
        }

        public virtual void AllocateOverrideRatingQuestionMapVapQuestionDefinitions(List<EditOverrideRatingQuestionMapVapQuestionDefinitionDto> editOverrideRatingQuestionMapVapQuestionDefinitionDtos)
        {
            OverrideRatingQuestionMapVapQuestionDefinitionMaintainer maintainer = new OverrideRatingQuestionMapVapQuestionDefinitionMaintainer(this);
            maintainer.MergeChanges(editOverrideRatingQuestionMapVapQuestionDefinitionDtos.Select(o => o.MapVapQuestionDefinitionId).ToList());
        }
    }
}
