﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Admin;

namespace Domain.OverrideRatingQuestion.Maintainers
{
    internal class OverrideRatingQuestionChannelMaintainer
    {
        private readonly OverrideRatingQuestion _OverrideRatingQuestion;

        public OverrideRatingQuestionChannelMaintainer(OverrideRatingQuestion overrideRatingQuestion)
        {
            _OverrideRatingQuestion = overrideRatingQuestion;
        }

        public void MergeChanges(List<int> changes)
        {
            foreach (int change in changes)
            {
                if (_OverrideRatingQuestion.OverrideRatingQuestionChannels.All(o => o.Channel.Id != change))
                {
                    _OverrideRatingQuestion.OverrideRatingQuestionChannels.Add(new OverrideRatingQuestionChannel(_OverrideRatingQuestion, new Channel(change)));
                }
            }

            foreach (OverrideRatingQuestionChannel original in _OverrideRatingQuestion.OverrideRatingQuestionChannels)
            {
                bool inChanges = changes.Any(c => c == original.Channel.Id);
                if(!inChanges)
                    original.Delete();
            }
        }
    }
}
