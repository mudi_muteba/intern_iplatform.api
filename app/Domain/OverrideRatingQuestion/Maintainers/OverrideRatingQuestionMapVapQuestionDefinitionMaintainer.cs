﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.QuestionDefinitions;

namespace Domain.OverrideRatingQuestion.Maintainers
{
    internal class OverrideRatingQuestionMapVapQuestionDefinitionMaintainer
    {
        private readonly OverrideRatingQuestion _OverrideRatingQuestion;

        public OverrideRatingQuestionMapVapQuestionDefinitionMaintainer(OverrideRatingQuestion overrideRatingQuestion)
        {
            _OverrideRatingQuestion = overrideRatingQuestion;
        }

        public void MergeChanges(List<int> changes)
        {
            foreach (int change in changes)
            {
                if (_OverrideRatingQuestion.OverrideRatingQuestionMapVapQuestionDefinitions.All(o => o.MapVapQuestionDefinition.Id != change))
                {
                    _OverrideRatingQuestion.OverrideRatingQuestionMapVapQuestionDefinitions.Add(
                        new OverrideRatingQuestionMapVapQuestionDefinition(
                            new MapVapQuestionDefinition() {Id = change}, _OverrideRatingQuestion));
                }
            }

            foreach (OverrideRatingQuestionMapVapQuestionDefinition original in _OverrideRatingQuestion.OverrideRatingQuestionMapVapQuestionDefinitions)
            {
                bool inChanges = changes.Any(c => c == original.MapVapQuestionDefinition.Id);
                if (!inChanges)
                    original.Delete();
            }
        }
    }
}
