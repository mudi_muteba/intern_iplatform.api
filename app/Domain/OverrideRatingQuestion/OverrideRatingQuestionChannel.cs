﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Admin;
using Domain.Base;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;

namespace Domain.OverrideRatingQuestion
{
    public class OverrideRatingQuestionChannel : Entity
    {
        public virtual Channel Channel { get; set; }

        public virtual OverrideRatingQuestion OverrideRatingQuestion { get; set; }

        public OverrideRatingQuestionChannel() { }

        public OverrideRatingQuestionChannel(OverrideRatingQuestion overrideRatingQuestion, Channel channel)
        {
            OverrideRatingQuestion = overrideRatingQuestion;
            Channel = channel;
        }
    }
}