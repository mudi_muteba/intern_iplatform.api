﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Products;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using MasterData;
using MasterData.Authorisation;

namespace Domain.OverrideRatingQuestion.Queries
{
    public class GetQuestionDefinitionByCoverDefinitionIdAndQuestionIdQuery : BaseQuery<QuestionDefinition>
    {
        private int _CoverDefinitionId;
        private int _QuestionId;
        private IRepository _Repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetQuestionDefinitionByCoverDefinitionIdAndQuestionIdQuery(IProvideContext contextProvider,
            IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<QuestionDefinition>())
        {
            _Repository = repository;
        }

        public GetQuestionDefinitionByCoverDefinitionIdAndQuestionIdQuery WithCoverDefinitionIdAndQuestionId(CreateOverrideRatingQuestionDto createOverrideRatingQuestionDto)
        {
            CoverDefinition coverDefinition = _Repository.GetAll<CoverDefinition>()
                .FirstOrDefault(cd => cd.Cover.Id == createOverrideRatingQuestionDto.CoverId &&
                                      cd.Product.Id == createOverrideRatingQuestionDto.ProductId);


            _CoverDefinitionId = coverDefinition.Id;
            _QuestionId = createOverrideRatingQuestionDto.QuestionId;
            return this;
        }

        public GetQuestionDefinitionByCoverDefinitionIdAndQuestionIdQuery WithCoverDefinitionIdAndQuestionId(EditOverrideRatingQuestionDto editOverrideRatingQuestionDto)
        {
            CoverDefinition coverDefinition = _Repository.GetAll<CoverDefinition>()
                .FirstOrDefault(cd => cd.Cover.Id == editOverrideRatingQuestionDto.CoverId &&
                                      cd.Product.Id == editOverrideRatingQuestionDto.ProductId);


            _CoverDefinitionId = coverDefinition.Id;
            _QuestionId = editOverrideRatingQuestionDto.QuestionId;
            return this;
        }

        protected internal override IQueryable<QuestionDefinition> Execute(IQueryable<QuestionDefinition> query)
        {
            Question question = new Questions().First(q => q.Id == _QuestionId);
            return query.Where(q => q.CoverDefinition.Id == _CoverDefinitionId &&
                                    q.Question == question &&
                                    !q.IsDeleted);
        }
    }
}
