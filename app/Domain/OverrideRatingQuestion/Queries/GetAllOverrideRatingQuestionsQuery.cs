﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.OverrideRatingQuestion.Queries
{
    public class GetAllOverrideRatingQuestionsQuery : BaseQuery<OverrideRatingQuestion>
    {
        public GetAllOverrideRatingQuestionsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultOverrideRatingQuestionFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected internal override IQueryable<OverrideRatingQuestion> Execute(IQueryable<OverrideRatingQuestion> query)
        {
            //TODO: Add ordering
            return query;
        }
    }
}
