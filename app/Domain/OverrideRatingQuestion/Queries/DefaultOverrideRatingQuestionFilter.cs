﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.OverrideRatingQuestion.Queries
{
    public class DefaultOverrideRatingQuestionFilter : IApplyDefaultFilters<OverrideRatingQuestion>
    {
        public IQueryable<OverrideRatingQuestion> Apply(ExecutionContext executionContext, IQueryable<OverrideRatingQuestion> query)
        {
            return query.Where(q => !q.IsDeleted);
        }
    }
}
