﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.Base;
using MasterData.Authorisation;

namespace Domain.OverrideRatingQuestion.Queries
{
    public class SearchOverrideRatingQuestionQuery : BaseQuery<OverrideRatingQuestion>, ISearchQuery<SearchOverrideRatingQuestionDto>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        private SearchOverrideRatingQuestionDto _SearchOverrideRatingQuestionDto;

        public SearchOverrideRatingQuestionQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultOverrideRatingQuestionFilter())
        {
        }

        public void WithCriteria(SearchOverrideRatingQuestionDto criteria)
        {
            _SearchOverrideRatingQuestionDto = criteria;
        }

        protected internal override IQueryable<OverrideRatingQuestion> Execute(IQueryable<OverrideRatingQuestion> query)
        {
            if (!String.IsNullOrEmpty(_SearchOverrideRatingQuestionDto.ProductName))
                query = query.Where(q => q.CoverDefinition.Product.Name.Contains(_SearchOverrideRatingQuestionDto.ProductName));
            if (!String.IsNullOrEmpty(_SearchOverrideRatingQuestionDto.CoverName))
                query = query.Where(q => q.CoverDefinition.Cover.Name.Contains(_SearchOverrideRatingQuestionDto.CoverName));

            query = AddOrdering(query);
            return query;
        }

        private IQueryable<OverrideRatingQuestion> AddOrdering(IQueryable<OverrideRatingQuestion> query)
        {
            query = new OrderByBuilder<OverrideRatingQuestion>(OrderByDefinition).Build(query, _SearchOverrideRatingQuestionDto.OrderBy);
            return query;
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("ProductName"), () => "CoverDefinition.Product.Name"},
                    {new OrderByField("CoverName"), () => "CoverDefinition.Cover.Name"}
                };
            }
        }
    }
}
