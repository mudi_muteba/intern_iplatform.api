﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Products;
using MasterData.Authorisation;

namespace Domain.OverrideRatingQuestion.Queries
{
    public class GetCoverDefinitionByProductIdAndCoverIdQuery : BaseQuery<CoverDefinition>
    {
        private int _ProductId;
        private int _CoverId;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetCoverDefinitionByProductIdAndCoverIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<CoverDefinition>())
        {
        }

        public GetCoverDefinitionByProductIdAndCoverIdQuery WithProductIdAndCoverId(int productId, int coverId)
        {
            _ProductId = productId;
            _CoverId = coverId;
            return this;
        }

        protected internal override IQueryable<CoverDefinition> Execute(IQueryable<CoverDefinition> query)
        {
            return query.Where(q => q.Product.Id == _ProductId &&
                                    q.Cover.Id == _CoverId &&
                                    !q.IsDeleted);
        }
    }
}
