﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;
using Domain.QuestionDefinitions;

namespace Domain.OverrideRatingQuestion
{
    public class OverrideRatingQuestionMapVapQuestionDefinition : Entity
    {
        public virtual MapVapQuestionDefinition MapVapQuestionDefinition { get; set; }

        public virtual OverrideRatingQuestion OverrideRatingQuestion { get; set; }

        public OverrideRatingQuestionMapVapQuestionDefinition()
        {
        }

        public OverrideRatingQuestionMapVapQuestionDefinition(MapVapQuestionDefinition mapVapQuestionDefinition,
            OverrideRatingQuestion overrideRatingQuestion)
        {
            MapVapQuestionDefinition = mapVapQuestionDefinition;
            OverrideRatingQuestion = overrideRatingQuestion;
        }
    }
}
