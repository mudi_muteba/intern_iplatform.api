﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using MasterData;
using MasterData.Authorisation;

namespace Domain.OverrideRatingQuestion.DtoHandlers
{
    public class CreateOverrideRatingQuestionDtoHandler : CreationDtoHandler<OverrideRatingQuestion, CreateOverrideRatingQuestionDto, int>
    {
        private readonly IExecutionPlan _ExecutionPlan;
        private readonly IRepository _Repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public CreateOverrideRatingQuestionDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan) : base(contextProvider, repository)
        {
            _ExecutionPlan = executionPlan;
            _Repository = repository;
        }

        protected override void EntitySaved(OverrideRatingQuestion entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override OverrideRatingQuestion HandleCreation(CreateOverrideRatingQuestionDto dto, HandlerResult<int> result)
        {
            CoverDefinition coverDefinition = _Repository.GetAll<CoverDefinition>()
                .FirstOrDefault(cd => cd.Product.Id == dto.ProductId && 
                    cd.Cover.Id == dto.CoverId);

            Question question = new Questions().First(q => q.Id == dto.QuestionId);
            QuestionDefinition questionDefinition = _Repository.GetAll<QuestionDefinition>()
                .FirstOrDefault(qd => qd.CoverDefinition.Id == coverDefinition.Id &&
                                      qd.Question == question);

            dto.CoverDefinitionId = coverDefinition.Id;
            dto.QuestionDefinitionId = questionDefinition.Id;

            OverrideRatingQuestion overrideRatingQuestion = OverrideRatingQuestion.Create(dto);
            result.Processed(overrideRatingQuestion.Id);
            return overrideRatingQuestion;
        }
    }
}
