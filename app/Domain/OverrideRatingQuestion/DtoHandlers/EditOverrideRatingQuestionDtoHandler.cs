﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using MasterData;
using MasterData.Authorisation;
using NVelocity.Runtime.Directive;

namespace Domain.OverrideRatingQuestion.DtoHandlers
{
    public class EditOverrideRatingQuestionDtoHandler : ExistingEntityDtoHandler<OverrideRatingQuestion, EditOverrideRatingQuestionDto, int>
    {
        private readonly IRepository _Repository;
        private readonly IExecutionPlan _ExecutionPLan;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public EditOverrideRatingQuestionDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPLan) : base(contextProvider, repository)
        {
            _Repository = repository;
            _ExecutionPLan = executionPLan;
        }

        protected override void HandleExistingEntityChange(OverrideRatingQuestion entity, EditOverrideRatingQuestionDto dto, HandlerResult<int> result)
        {
            CoverDefinition coverDefinition = _Repository.GetAll<CoverDefinition>()
                .FirstOrDefault(cd => cd.Product.Id == dto.ProductId &&
                    cd.Cover.Id == dto.CoverId);

            Question question = new Questions().First(q => q.Id == dto.QuestionId);
            QuestionDefinition questionDefinition = _Repository.GetAll<QuestionDefinition>()
                .FirstOrDefault(qd => qd.Question == question &&
                    qd.CoverDefinition.Id == coverDefinition.Id);

            dto.CoverDefinitionId = coverDefinition.Id;
            dto.QuestionDefinitionId = questionDefinition.Id;

            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
