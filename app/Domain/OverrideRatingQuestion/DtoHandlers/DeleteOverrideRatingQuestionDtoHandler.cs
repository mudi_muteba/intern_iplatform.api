﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using MasterData.Authorisation;

namespace Domain.OverrideRatingQuestion.DtoHandlers
{
    public class DeleteOverrideRatingQuestionDtoHandler : ExistingEntityDtoHandler<OverrideRatingQuestion, DeleteOverrideRatingQuestionDto, int>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public DeleteOverrideRatingQuestionDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        protected override void HandleExistingEntityChange(OverrideRatingQuestion entity, DeleteOverrideRatingQuestionDto dto, HandlerResult<int> result)
        {
            entity.Delete(dto);
            result.Processed(entity.Id);
        }
    }
}
