﻿using System;
using Domain.QuestionDefinitions;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.OverrideRatingQuestion.Overrides
{
    public class OverrideRatingQuestionOverride : IAutoMappingOverride<OverrideRatingQuestion>
    {
        public void Override(AutoMapping<OverrideRatingQuestion> mapping)
        {
            mapping.HasMany(x => x.OverrideRatingQuestionChannels).Cascade.SaveUpdate();
            
            mapping.HasMany(x => x.OverrideRatingQuestionMapVapQuestionDefinitions).Cascade.SaveUpdate()
                .Where(String.Format("(SELECT TOP 1 P.IsDeleted FROM {0} AS P WHERE P.Id = {0}Id) = 0", typeof(MapVapQuestionDefinition).Name)); //Ensure child items have not been deleted.
        }
    }


}