﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Campaigns.Queries;
using MasterData.Authorisation;

namespace Domain.DocumentManagement.Queries
{
    public class GetDocumentData : BaseQuery<Document>
    {

        public GetDocumentData(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Document>())
        {
        }

        private int id;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithId(int id)
        {
            this.id = id;
        }

        protected internal override IQueryable<Document> Execute(IQueryable<Document> query)
        {
            return query.Where(x => x.Id == id);
        }
    }
}