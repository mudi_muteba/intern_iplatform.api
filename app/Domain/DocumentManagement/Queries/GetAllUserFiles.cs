﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Campaigns.Queries;
using MasterData.Authorisation;

namespace Domain.DocumentManagement.Queries
{
    public class GetAllUserFiles : BaseQuery<UserDocument>
    {

        public GetAllUserFiles(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<UserDocument>())
        {
        }

        private int createdBy;
        private int partyId;
        private int id;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCreatedById(int createdBy)
        {
            this.createdBy = createdBy;
        }

        public void WithPartyId(int partyId)
        {
            this.partyId = partyId;
        }

        public void WithId(int id)
        {
            this.id = id;
        }


        protected internal override IQueryable<UserDocument> Execute(IQueryable<UserDocument> query)
        {
            if (id > 0)
                query = query.Where(x => x.Id == id);


            if (createdBy > 0)
                query = query.Where(x => x.CreatedBy.Id == createdBy);


            if (partyId > 0)
                query = query.Where(x => x.Party.Id == partyId);

            return query;
        }
    }


}