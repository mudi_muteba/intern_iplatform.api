﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.DocumentManagement;

namespace Domain.DocumentManagement
{
    public class Document : Entity
    {
        public Document()
        {
            DocumentData = new DocumentData();
        }
        public virtual string FileName { get; set; }
        public virtual string FileAssociation { get; set; }
        public virtual string Tags { get; set; }
        public virtual bool IsDocumentTrue { get; set; }
        public virtual bool NoOriginalDocument { get; set; }
        public virtual int ContentLength { get; set; }
        public virtual string ContentType { get; set; }
        public virtual DateTime? ValidFrom { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual DateTime? DateCreated { get; set; }
        public virtual DocumentData DocumentData { get; set; }

        public static Document Create(DocumentDto dto, DocumentData data)
        {
            var entity = Mapper.Map<Document>(dto);
            entity.DocumentData = data;
            entity.DateCreated = DateTime.Now.ToUniversalTime();

            return entity;
        }

        public virtual void Update(DocumentDto dto)
        {
            Mapper.Map(dto, this);
        }

        public virtual void Disable(DocumentDto dto)
        {
            Delete();
        }
    }
}