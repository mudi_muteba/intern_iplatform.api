﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.DocumentManagement;
using MasterData.Authorisation;

namespace Domain.DocumentManagement.Handler
{
    public class DeleteDocumentHandler : IHandleDto<DeleteDocumentDto, int>
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IProvideContext _context;
        private readonly IRepository _repository;

        public DeleteDocumentHandler(IExecutionPlan executionPlan, IProvideContext context, IRepository repository)
        {
            _executionPlan = executionPlan;
            _context = context;
            _repository = repository;
        }

        public void Handle(DeleteDocumentDto dto, HandlerResult<int> handlerResult)
        {
            int result = 0;
            var userDocument =
                _repository.GetAll<UserDocument>().FirstOrDefault(x => x.Id == dto.UserDocumentId);

            if (userDocument != null)
            {
                userDocument.Delete();
                _repository.Save(userDocument);
                result = userDocument.Id;
            }

            handlerResult.Processed(result);
        }

        public List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }
    }
}
