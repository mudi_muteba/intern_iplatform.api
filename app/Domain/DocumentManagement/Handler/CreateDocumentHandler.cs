﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.DocumentManagement;
using MasterData.Authorisation;

namespace Domain.DocumentManagement.Handler
{
    public class CreateDocumentHandler : CreationDtoHandler<Document, DocumentDto, int>
    {
        private DocumentData _document;

        public CreateDocumentHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }
        protected override void EntitySaved(Document entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        public CreateDocumentHandler WithDocumentData(int documentData)
        {
            _document = new DocumentData() {Id = documentData };
            return this;
        }


        protected override Document HandleCreation(DocumentDto dto, HandlerResult<int> result)
        {
            var entity = Document.Create(dto, _document);
            result.Processed(entity.Id);

            return entity;
        }
    }
}