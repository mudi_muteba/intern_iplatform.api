﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.DocumentManagement;
using MasterData.Authorisation;

namespace Domain.DocumentManagement.Handler
{
    public class CreateDocumentDataHandler : CreationDtoHandler<DocumentData, DocumentDto, int>
    {
        public CreateDocumentDataHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }
        protected override void EntitySaved(DocumentData entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override DocumentData HandleCreation(DocumentDto dto, HandlerResult<int> result)
        {
            var entity = DocumentData.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}