﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.DocumentManagement.Queries;
using iPlatform.Api.DTOs.DocumentManagement;
using MasterData.Authorisation;

namespace Domain.DocumentManagement.Handler
{
    public class DocumentManagementDtoHandler : IHandleDto<CreateDocumentDto, int>
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IProvideContext _context;
        private readonly IRepository _repository;

        public DocumentManagementDtoHandler(IExecutionPlan executionPlan, IProvideContext context, IRepository repository)
        {
            _executionPlan = executionPlan;
            _context = context;
            _repository = repository;
        }

        public void Handle(CreateDocumentDto dto, HandlerResult<int> handlerResult)
        {
            var docDto = Mapper.Map<DocumentDto>(dto);

            var createDocumentDataHandler = new CreateDocumentDataHandler(_context, _repository);
            var createDocumentHandler = new CreateDocumentHandler(_context, _repository);
            var createUserDocumentHandler = new CreateUserDocumentHandler(_context, _repository);


            createDocumentDataHandler.Handle(docDto, handlerResult);
            createDocumentHandler.WithDocumentData(handlerResult.Response).Handle(docDto, handlerResult);
            createUserDocumentHandler.WithDocument(handlerResult.Response).Handle(docDto, handlerResult);
        }

        public List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }
    }
}