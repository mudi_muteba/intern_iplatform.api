﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.DocumentManagement;
using MasterData.Authorisation;

namespace Domain.DocumentManagement.Handler
{
    public class CreateUserDocumentHandler : CreationDtoHandler<UserDocument, DocumentDto, int>
    {
        private Document _document;

        public CreateUserDocumentHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }
        protected override void EntitySaved(UserDocument entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        public CreateUserDocumentHandler WithDocument(int document)
        {
            _document = new Document {Id = document};
            return this;
        }

        protected override UserDocument HandleCreation(DocumentDto dto, HandlerResult<int> result)
        {

            var entity = UserDocument.Create(dto, _document);

            EntitySaved(entity, result);

            return entity;
        }
    }
}