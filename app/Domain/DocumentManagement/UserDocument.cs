﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.DocumentManagement;
using RabbitMQ.Client.Impl;

namespace Domain.DocumentManagement
{
    public class UserDocument : Entity
    {
       public virtual Party.Party CreatedBy { get; set; }
       public virtual Party.Party Party { get; set; }
       public virtual Document Document { get; set; }

        public static UserDocument Create(DocumentDto dto, Document document)
        {
            var entity = Mapper.Map<UserDocument>(dto);
            entity.Document = document;
            return entity;
        }

        public override void Delete()
        {
            IsDeleted = true;
        }
    }
}