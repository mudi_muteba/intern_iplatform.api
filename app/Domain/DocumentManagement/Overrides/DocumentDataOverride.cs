﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.DocumentManagement.Overrides
{
    public class DocumentDataOverride : IAutoMappingOverride<DocumentData>
    {
        public void Override(AutoMapping<DocumentData> mapping)
        {
            mapping.Map(x => x.Contents).Length(1000000);
        }
    }
}
