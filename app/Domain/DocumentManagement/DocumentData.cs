﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Domain.Base;
using Domain.Base.Compression;
using Domain.Base.Encryption;
using iPlatform.Api.DTOs.DocumentManagement;

namespace Domain.DocumentManagement
{
    public class DocumentData : Entity
    {
        public DocumentData() { }
        [MaxLength(10000)]
        public virtual string Contents { get; set; }

        public static DocumentData Create(DocumentDto dto)
        {
            var entity = new DocumentData();
            var comp = dto.InputStream.Compress();
            var encrypt = Encryption.EncryptObject(comp);
            entity.Contents = encrypt;

            return entity;
        }

    }
}