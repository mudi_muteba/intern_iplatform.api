﻿using System;
using System.Collections.Generic;
using System.IO;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Encryption;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.DocumentManagement;

namespace Domain.DocumentManagement.Mappings
{
    public class DocumentMappings : Profile
    {

        protected override void Configure()
        {


            //Map From DocDto
            Mapper.CreateMap<DocumentDto, DocumentData>()
                .ForMember(t => t.Contents, o => o.MapFrom(s => Encryption.EncryptObject(s.InputStream)));
           

            Mapper.CreateMap<DocumentDto, UserDocument>()
                .ForMember(t => t.CreatedBy, o => o.MapFrom(s => new Party.Party() { Id = s.CreatedById }))
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party.Party() { Id = s.PartyId }));

            Mapper.CreateMap<DocumentDto, Document>();

            Mapper.CreateMap<List<DocumentDto>, ListResultDto<DocumentDto>>()
               .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<PagedResults<Document>, PagedResultDto<DocumentDto>>();

            Mapper.CreateMap<List<Document>, ListResultDto<DocumentDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            //fMap rom Doc
            Mapper.CreateMap<Document, DocumentDto>();

            Mapper.CreateMap<Document, DocumentResultDto>()
                ;


            //From Doc data
            Mapper.CreateMap<DocumentData, DocumentDto>()
                .ForMember(t => t.LinkedDocumentId, o => o.MapFrom(s => s.Id));
            
            Mapper.CreateMap<DocumentData, DocumentResultDto>()
                .ForMember(t => t.ContentBytes, o => o.MapFrom(s=> Encryption.DecryptString<byte[]>(s.Contents)));

            //from UserDoc
            Mapper.CreateMap<UserDocument, DocumentDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                .ForMember(t => t.CreatedName, o => o.MapFrom(s => s.CreatedBy.DisplayName))
                .ForMember(t => t.CreatedById, o => o.MapFrom(s => s.CreatedBy.Id))
                .ForMember(t => t.ExpiryDate, o => o.MapFrom(s => s.Document.ExpiryDate))
                .ForMember(t => t.FileAssociation, o => o.MapFrom(s => s.Document.FileAssociation))
                .ForMember(t => t.IsDocumentTrue, o => o.MapFrom(s => s.Document.IsDocumentTrue))
                .ForMember(t => t.NoOriginalDocument, o => o.MapFrom(s => s.Document.NoOriginalDocument))
                .ForMember(t => t.Tags, o => o.MapFrom(s => s.Document.Tags))
                .ForMember(t => t.ValidFrom, o => o.MapFrom(s => s.Document.ValidFrom))
                .ForMember(t => t.LinkedDocumentId, o => o.MapFrom(s => s.Document.Id))
                .ForMember(t => t.FileName, o => o.MapFrom(s => s.Document.FileName))
                .ForMember(t => t.ContentLength, o => o.MapFrom(s => s.Document.ContentLength))
                .ForMember(t => t.ContentType, o => o.MapFrom(s => s.Document.ContentType))
                .ForMember(t => t.DateCreated, o => o.MapFrom(s => s.Document.DateCreated))
                ;



            Mapper.CreateMap<CreateDocumentDto, DocumentDto>();
        }
    }

}