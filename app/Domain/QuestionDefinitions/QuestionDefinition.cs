using System.Collections.Generic;
using Domain.Base;
using Domain.Campaigns;
using Domain.Products;
using Infrastructure.NHibernate.Attributes;
using MasterData;
using Shared.Extentions;

namespace Domain.QuestionDefinitions
{
    public class QuestionDefinition : EntityWithAudit
    {
        public QuestionDefinition()
        {
            Children = new List<QuestionDefinition>();
        }

        public virtual string DisplayName { get; protected internal set; }

        [DoNotMap]
        public virtual string DisplayNameTranslationKey
        {
            get
            {
                return string.Format("QUESTION_KEY_{0}_{1}", CoverDefinition.Cover.Name.RemoveAllSpaces().ToUpperInvariant(),
                    Question.Name.RemoveAllSpaces().ToUpperInvariant());
            }
        }

        [DoNotMap]
        public virtual string QuestionGroupTranslationKey
        {
            get
            {
                return string.Format("QUESTION_GROUP_{0}_{1}", CoverDefinition.Cover.Name.RemoveAllSpaces().ToUpperInvariant(),
                    Question.QuestionGroup.Name.RemoveAllSpaces().ToUpperInvariant());
            }
        }

        public virtual CoverDefinition CoverDefinition { get; protected internal set; }
        public virtual Question Question { get; protected internal set; }
        public virtual QuestionDefinition Parent { get; protected internal set; }
        public virtual IList<QuestionDefinition> Children { get; protected internal set; }
        public virtual QuestionDefinitionType QuestionDefinitionType { get; protected internal set; }
        public virtual QuestionDefinitionGroupType QuestionDefinitionGroupType { get; protected internal set; }
        public virtual int VisibleIndex { get; protected internal set; }
        public virtual int MasterId { get; protected internal set; }
        public virtual bool RequiredForQuote { get; protected internal set; }
        public virtual bool RatingFactor { get; protected internal set; }
        public virtual bool ReadOnly { get; protected internal set; }
        public virtual string ToolTip { get; protected internal set; }
        public virtual bool Hide { get; protected internal set; }
        public virtual int? LinkQuestionId { get; protected internal set; }
        public virtual QuestionGroup QuestionGroup { get; set; }

        [DoNotMap]
        public virtual string ToolTipTranslationKey
        {
            get
            {
                return string.Format("QUESTION_TOOLTIP_{0}_{1}", CoverDefinition.Cover.Name.RemoveAllSpaces().ToUpperInvariant(),
                    Question.Name.RemoveAllSpaces().ToUpperInvariant());
            }
        }

        [DoNotMap]
        public virtual Product Product
        {
            get { return CoverDefinition.Product; }
        }

        public virtual string DefaultValue { get; protected internal set; }
        public virtual string RegexPattern { get; protected internal set; }
        public virtual int GroupIndex { get; protected internal set; }

        public virtual void AddChildQuestion(QuestionDefinition questionDefinition)
        {
            questionDefinition.Parent = this;
            questionDefinition.CoverDefinition = CoverDefinition;

            Children.Add(questionDefinition);
        }

        public virtual QuestionDefinition CreateSubQuestion(QuestionDefinition subQuestion)
        {
            subQuestion.Parent = this;
            subQuestion.CoverDefinition = CoverDefinition;
            subQuestion.QuestionDefinitionType = QuestionDefinitionTypes.SubQuestion;
            Children.Add(subQuestion);

            return subQuestion;
        }

        public virtual QuestionDefinition CreateSubQuestion()
        {
            return CreateSubQuestion(new QuestionDefinition());
        }

        public virtual bool Matches(int coverId, int questionId)
        {
            return Question.Id == questionId && CoverDefinition.Cover.Id == coverId;
        }
    }
}