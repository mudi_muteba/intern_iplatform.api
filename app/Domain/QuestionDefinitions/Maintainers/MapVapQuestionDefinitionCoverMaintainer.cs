﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData;
using Domain.MapVapQuestionDefinitionCovers;

namespace Domain.QuestionDefinitions.Maintainers
{
    internal class MapVapQuestionDefinitionCoverMaintainer
    {
        private readonly MapVapQuestionDefinition _MapVapQuestionDefinition;

        public MapVapQuestionDefinitionCoverMaintainer(MapVapQuestionDefinition mapVapQuestionDefinition)
        {
            _MapVapQuestionDefinition = mapVapQuestionDefinition;
        }

        public void MergeChanges(List<int> changes)
        {
            foreach (int change in changes)
            {
                if (_MapVapQuestionDefinition.MapVapQuestionDefinitionCovers.All(m => m.Cover.Id != change))
                {
                    _MapVapQuestionDefinition.MapVapQuestionDefinitionCovers.Add(
                        new MapVapQuestionDefinitionCover(_MapVapQuestionDefinition,
                            new Cover())
                        { Id = change });
                }
            }

            foreach (MapVapQuestionDefinitionCover original in _MapVapQuestionDefinition.MapVapQuestionDefinitionCovers)
            {
                if (changes.Any(c => c == original.Cover.Id))
                    original.Delete();
            }
        }
    }
}
