﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.QuestionDefinitions.Overrides
{
    public class MapVapQuestionDefinitionOverride : IAutoMappingOverride<MapVapQuestionDefinition>
    {
        public void Override(AutoMapping<MapVapQuestionDefinition> mapping)
        {
            mapping.HasMany(m => m.MapVapQuestionDefinitionCovers).Cascade.SaveUpdate();
        }
    }
}