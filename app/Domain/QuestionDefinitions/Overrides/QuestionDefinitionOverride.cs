﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.QuestionDefinitions.Overrides
{
    public class QuestionDefinitionOverride : IAutoMappingOverride<QuestionDefinition>
    {
        public void Override(AutoMapping<QuestionDefinition> mapping)
        {
            mapping.References(x => x.CoverDefinition).LazyLoad(Laziness.NoProxy);
        }
    }
}