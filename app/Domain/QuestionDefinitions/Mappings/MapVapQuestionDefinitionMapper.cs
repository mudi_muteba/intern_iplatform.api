﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;

namespace Domain.QuestionDefinitions.Mappings
{
    public class MapVapQuestionDefinitionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<MapVapQuestionDefinitionDto, MapVapQuestionDefinition>();

            Mapper.CreateMap<CreateMapVapQuestionDefinitionDto, MapVapQuestionDefinition>()
                .ForMember(d => d.QuestionDefinition, o => o.MapFrom(s => Mapper.Map<int, QuestionDefinition>(s.QuestionDefinitionId)))
                .ForMember(d => d.Product, o => o.MapFrom(s => Mapper.Map<int, Product>(s.ProductId)))
                .ForMember(d => d.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)))
                .ForMember(d => d.IsDeleted, o => o.Ignore())
                .ForMember(d => d.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<EditMapVapQuestionDefinitionDto, MapVapQuestionDefinition>()
                .ForMember(d => d.Id, o => o.Ignore())
                .ForMember(d => d.QuestionDefinition, o => o.MapFrom(s => Mapper.Map<int, QuestionDefinition>(s.QuestionDefinitionId)))
                .ForMember(d => d.Product, o => o.MapFrom(s => Mapper.Map<int, Product>(s.ProductId)))
                .ForMember(d => d.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)))
                .ForMember(d => d.IsDeleted, o => o.Ignore())
                ;

            Mapper.CreateMap<MapVapQuestionDefinitionDto, MapVapQuestionDefinition>()
                .ReverseMap();

            Mapper.CreateMap<MapVapQuestionDefinition, ListMapVapQuestionDefinitionDto>()
                ;
            Mapper.CreateMap<List<MapVapQuestionDefinition>, ListResultDto<ListMapVapQuestionDefinitionDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<IEnumerable<MapVapQuestionDefinition>, ListResultDto<ListMapVapQuestionDefinitionDto>>()
                .ForMember(d => d.Results, o => o.MapFrom(s => s.ToList()));

            Mapper.CreateMap<PagedResults<MapVapQuestionDefinition>, PagedResultDto<ListMapVapQuestionDefinitionDto>>();
        }
    }
}