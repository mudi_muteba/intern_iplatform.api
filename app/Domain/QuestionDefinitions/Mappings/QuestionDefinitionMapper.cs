﻿using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;

namespace Domain.QuestionDefinitions.Mappings
{
    public class QuestionDefinitionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PagedResults<QuestionDefinition>, PagedResultDto<QuestionDefinitionDto>>();
            Mapper.CreateMap<List<QuestionDefinition>, ListResultDto<QuestionDefinitionDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<Question, QuestionDto>();
            Mapper.CreateMap<QuestionGroup, QuestionGroupDto>();
            Mapper.CreateMap<IEnumerable<Question>, ListResultDto<QuestionDto>>()
                .ForMember(d => d.Results, o => o.MapFrom(s => s.ToList()));
        }
    }
}