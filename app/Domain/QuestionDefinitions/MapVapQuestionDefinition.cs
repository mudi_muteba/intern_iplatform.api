﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin;
using Domain.Base;
using Domain.Products;
using Domain.QuestionDefinitions.Maintainers;
using iPlatform.Api.DTOs.MapVapQuestionDefinitionCover;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using Domain.MapVapQuestionDefinitionCovers;

namespace Domain.QuestionDefinitions
{
    public class MapVapQuestionDefinition : Entity
    {
        public virtual Product Product { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual QuestionDefinition QuestionDefinition { get; set; }
        public virtual decimal Premium { get; set; }
        public virtual bool Enabled { get; set; }
        public virtual string ExternalVapProductCode { get; set; }
        public virtual bool PerSectionVap { get; set; }
        private int _annualizedMultiplier = 12;
        public virtual int AnnualizedMultiplier
        {
            get { return _annualizedMultiplier; }
            set { _annualizedMultiplier = value; }
        }
        public virtual decimal SumInsured { get; set; }

        public virtual IList<MapVapQuestionDefinitionCover> MapVapQuestionDefinitionCovers { get; set; }

        public static MapVapQuestionDefinition Create(CreateMapVapQuestionDefinitionDto dto)
        {
            var entity = Mapper.Map<MapVapQuestionDefinition>(dto);
            return entity;
        }

        public virtual void Update(EditMapVapQuestionDefinitionDto dto)
        {
            AllocateMapVapQuestionDefinitionCovers(dto.MapVapQuestionDefinitionCovers);
            Mapper.Map(dto, this);
        }

        public virtual void AllocateMapVapQuestionDefinitionCovers(
            List<EditMapVapQuestionDefinitionCoverDto> editMapVapQuestionDefinitionCoverDtos)
        {
            MapVapQuestionDefinitionCoverMaintainer maintainer = new MapVapQuestionDefinitionCoverMaintainer(this);
            if(editMapVapQuestionDefinitionCoverDtos != null && editMapVapQuestionDefinitionCoverDtos.Any())
                maintainer.MergeChanges(editMapVapQuestionDefinitionCoverDtos.Select(x => x.CoverId).ToList());
        }
    }
}
