﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using MasterData.Authorisation;

namespace Domain.QuestionDefinitions.Handlers
{
    public class CreateMapVapQuestionDefinitionDtoHandler : CreationDtoHandler<MapVapQuestionDefinition, CreateMapVapQuestionDefinitionDto, int>
    {

        public CreateMapVapQuestionDefinitionDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(MapVapQuestionDefinition entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override MapVapQuestionDefinition HandleCreation(CreateMapVapQuestionDefinitionDto dto, HandlerResult<int> result)
        {
            var entity = MapVapQuestionDefinition.Create(dto);
            return entity;
        }

    }
}