﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using MasterData.Authorisation;

namespace Domain.QuestionDefinitions.Handlers
{
    public class DeleteMapVapQuestionDefinitionDtoHandler : ExistingEntityDtoHandler<MapVapQuestionDefinition, DeleteMapVapQuestionDefinitionDto, bool>
    {

        public DeleteMapVapQuestionDefinitionDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void HandleExistingEntityChange(MapVapQuestionDefinition entity, DeleteMapVapQuestionDefinitionDto dto, HandlerResult<bool> result)
        {
            entity.Delete();
            result.Processed(true);
        }
    }
}