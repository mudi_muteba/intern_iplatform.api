﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using MasterData.Authorisation;

namespace Domain.QuestionDefinitions.Handlers
{
    public class EditMapVapQuestionDefinitionDtoHandler : ExistingEntityDtoHandler<MapVapQuestionDefinition, EditMapVapQuestionDefinitionDto, int>
    {

        public EditMapVapQuestionDefinitionDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void HandleExistingEntityChange(MapVapQuestionDefinition entity, EditMapVapQuestionDefinitionDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}