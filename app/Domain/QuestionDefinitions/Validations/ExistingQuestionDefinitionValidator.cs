﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using Domain.QuestionDefinitions.Queries;
using ValidationMessages;

namespace Domain.QuestionDefinitions.Validation
{
    public class ExistingQuestionDefinitionValidator : 
        IValidateDto<EditMapVapQuestionDefinitionDto>, 
        IValidateDto<CreateMapVapQuestionDefinitionDto>
    {
        private readonly GetQuestionDefinitionsByIdQuery _query;

        public ExistingQuestionDefinitionValidator(GetQuestionDefinitionsByIdQuery query)
        {
            _query = query;
        }

        public void Validate(EditMapVapQuestionDefinitionDto dto, ExecutionResult result)
        {
            ValidateQuestion(dto.QuestionDefinitionId, result);
        }


        public void Validate(CreateMapVapQuestionDefinitionDto dto, ExecutionResult result)
        {
            ValidateQuestion(dto.QuestionDefinitionId, result);
        }

        private void ValidateQuestion(int questionId, ExecutionResult result)
        {
            var questions = _query.WithId(questionId).ExecuteQuery();

            if(!questions.Any())
                    result.AddValidationMessage(MapVapQuestionDefinitionValidationMessages.QuestionIdInvalid.AddParameters(new[] { questionId.ToString() }));

        }

    }
}