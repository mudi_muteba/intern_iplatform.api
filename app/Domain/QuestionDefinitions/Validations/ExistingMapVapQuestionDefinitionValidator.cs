﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using Domain.QuestionDefinitions.Queries;
using ValidationMessages;
using Domain.Base.Repository;
using Domain.QuestionDefinitions;

namespace Domain.QuestionDefinitions.Validation
{
    public class ExistingMapVapQuestionDefinitionValidator : 
        IValidateDto<EditMapVapQuestionDefinitionDto>, 
        IValidateDto<DeleteMapVapQuestionDefinitionDto>
    {
        private readonly IRepository _repository;

        public ExistingMapVapQuestionDefinitionValidator(IRepository repository)
        {
            _repository = repository;
        }

        public void Validate(EditMapVapQuestionDefinitionDto dto, ExecutionResult result)
        {
            ValidateMapVapQuestionDefinition(dto.Id, result);
        }


        public void Validate(DeleteMapVapQuestionDefinitionDto dto, ExecutionResult result)
        {
            ValidateMapVapQuestionDefinition(dto.Id, result);
        }

        private void ValidateMapVapQuestionDefinition(int id, ExecutionResult result)
        {
            var questions = _repository.GetById<MapVapQuestionDefinition>(id);

            if(questions == null)
                    result.AddValidationMessage(MapVapQuestionDefinitionValidationMessages.IdInvalid.AddParameters(new[] { id.ToString() }));

        }

    }
}