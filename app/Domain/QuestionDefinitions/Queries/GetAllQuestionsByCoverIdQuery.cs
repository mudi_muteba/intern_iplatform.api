﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.QuestionDefinitions.Queries
{
    public class GetAllQuestionsByCoverIdQuery : BaseQuery<QuestionDefinition>
    {
        public GetAllQuestionsByCoverIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<QuestionDefinition>())
        {
        }

        private int CoverId;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }


        public GetAllQuestionsByCoverIdQuery WithCoverId(int id)
        {
            CoverId = id;
            return this;
        }
        protected internal override IQueryable<QuestionDefinition> Execute(IQueryable<QuestionDefinition> query)
        {

            return query.Where(x => x.CoverDefinition.Cover.Id == CoverId);
        }
    }
}