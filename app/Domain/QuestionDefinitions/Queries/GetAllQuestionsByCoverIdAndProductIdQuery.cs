﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.QuestionDefinitions.Queries
{
    public class GetAllQuestionsByCoverIdAndProductIdQuery : BaseQuery<QuestionDefinition>
    {
        private int _CoverId;
        private int _ProductId;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetAllQuestionsByCoverIdAndProductIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<QuestionDefinition>())
        {
        }

        public GetAllQuestionsByCoverIdAndProductIdQuery WithCoverIdAndProductId(int coverId, int productId)
        {
            _CoverId = coverId;
            _ProductId = productId;

            return this;
        }

        protected internal override IQueryable<QuestionDefinition> Execute(IQueryable<QuestionDefinition> query)
        {
            return query.Where(q => q.CoverDefinition.Cover.Id == _CoverId &&
                                    q.CoverDefinition.Product.Id == _ProductId);
        }
    }
}
