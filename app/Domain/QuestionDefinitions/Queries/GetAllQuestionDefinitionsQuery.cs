﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.QuestionDefinitions.Queries
{
    public class GetAllQuestionDefinitionsQuery : BaseQuery<QuestionDefinition>
    {
        public GetAllQuestionDefinitionsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<QuestionDefinition>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<QuestionDefinition> Execute(IQueryable<QuestionDefinition> query)
        {
            return query;
        }
    }
}