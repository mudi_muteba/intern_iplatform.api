﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.QuestionDefinitions.Queries
{
    public class GetQuestionDefinitionsByIdQuery : BaseQuery<QuestionDefinition>
    {
        public GetQuestionDefinitionsByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<QuestionDefinition>())
        {
        }

        private int _id;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetQuestionDefinitionsByIdQuery WithId(int id)
        {
            _id = id;
            return this;
        }

        protected internal override IQueryable<QuestionDefinition> Execute(IQueryable<QuestionDefinition> query)
        {
            return query.Where(x => x.Id == _id);
        }
    }
}