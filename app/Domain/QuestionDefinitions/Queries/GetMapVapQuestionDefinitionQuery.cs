﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Party.Addresses;
using Domain.Party.Addresses.Queries;
using MasterData.Authorisation;

namespace Domain.QuestionDefinitions.Queries
{
    public class GetMapVapQuestionDefinitionQuery : BaseQuery<MapVapQuestionDefinition>
    {
        private int _productId;
        private int _channelId;
        private bool? _Enabled;

        public GetMapVapQuestionDefinitionQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<MapVapQuestionDefinition>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetMapVapQuestionDefinitionQuery WithCriteria(int productId, int channelId, bool? enabled = null)
        {
            _productId = productId;
            _channelId = channelId;
            _Enabled = enabled;

            return this;
        }

        protected internal override IQueryable<MapVapQuestionDefinition> Execute(IQueryable<MapVapQuestionDefinition> query)
        {
            if (_channelId > 0)
                query = query.Where(x => x.Channel.Id == _channelId);
            if (_productId > 0)
                query = query.Where(x => x.Product.Id == _productId);
            if (_Enabled != null)
                query = query.Where(q => q.Enabled == _Enabled.Value);

            return query;
        }
    }
}