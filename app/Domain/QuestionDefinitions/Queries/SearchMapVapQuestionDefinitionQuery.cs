﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using MasterData.Authorisation;

namespace Domain.QuestionDefinitions.Queries
{
    public class SearchMapVapQuestionDefinitionQuery : BaseQuery<MapVapQuestionDefinition>, ISearchQuery<SearchMapVapQuestionDefinitionDto>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        private SearchMapVapQuestionDefinitionDto _SearchMapVapQuestionDefinitionDto;

        public SearchMapVapQuestionDefinitionQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<MapVapQuestionDefinition>())
        {
        }

        public void WithCriteria(SearchMapVapQuestionDefinitionDto criteria)
        {
            _SearchMapVapQuestionDefinitionDto = criteria;
        }

        protected internal override IQueryable<MapVapQuestionDefinition> Execute(IQueryable<MapVapQuestionDefinition> query)
        {
            if (!String.IsNullOrEmpty(_SearchMapVapQuestionDefinitionDto.ChannelName))
                query = query.Where(q => q.Channel.Name.Contains(_SearchMapVapQuestionDefinitionDto.ChannelName));
            if (!String.IsNullOrEmpty(_SearchMapVapQuestionDefinitionDto.ProductName))
                query = query.Where(q => q.Product.Name.Contains(_SearchMapVapQuestionDefinitionDto.ProductName));

            query = AddOrdering(query);

            return query;
        }

        private IQueryable<MapVapQuestionDefinition> AddOrdering(IQueryable<MapVapQuestionDefinition> query)
        {
            query = new OrderByBuilder<MapVapQuestionDefinition>(OrderByDefinition).Build(query, _SearchMapVapQuestionDefinitionDto.OrderBy);
            return query;
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("ChannelName"), () => "Channel.Name"  },
                    {new OrderByField("ProductName"), () => "Product.Name"  }
                };
            }
        }
    }
}
