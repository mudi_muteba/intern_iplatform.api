﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;
using iPlatform.Api.DTOs.Upload;

namespace Domain.QuestionDefinitions
{
    public class MapQuestionDefinition : Entity
    {
        public virtual int ParentId { get; set; }
        public virtual int ChildId { get; set; }
    }
}
