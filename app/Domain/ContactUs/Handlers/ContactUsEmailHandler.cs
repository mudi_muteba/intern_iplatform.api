﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Admin.ChannelSettings;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Emailing;
using Domain.Emailing.Factory;
using Domain.Individuals.Queries;
using Domain.Organizations.Queries;
using Domain.Reports.Base;
using Domain.Reports.Base.Handlers;
using Domain.Reports.Base.Queries;
using Domain.Reports.Base.StoredProcedures;
using Domain.SignFlow.Handlers;
using Domain.Users;
using iPlatform.Api.DTOs.ContactUs;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.Base.Criteria;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;
using iPlatform.Api.DTOs.SignFlow;
using ToolBox.Communication.Interfaces;
using ToolBox.Templating.Interfaces;
using Workflow.Messages;

namespace Domain.ContactUs.Handlers
{
    public class ContactUsEmailHandler : BaseDtoHandler<ContactUsEmailDto, ReportEmailResponseDto>
    {
        private readonly IRepository repository;
        private readonly ITemplateEngine templateEngine;
        private readonly IProvideContext contextProvider;
        private readonly IExecutionPlan executionPlan;
        
        private readonly GetChannelById channelQuery;

        public ContactUsEmailHandler(IProvideContext _contextProvider, IRepository _repository, ITemplateEngine _templateEngine, IExecutionPlan _executionPlan) : base(_contextProvider) 
        {
            repository = _repository;
            templateEngine = _templateEngine;
            contextProvider = _contextProvider;
            executionPlan = _executionPlan;
            
            channelQuery = new GetChannelById(contextProvider, repository);
        }

        public override List<MasterData.Authorisation.RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<MasterData.Authorisation.RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(ContactUsEmailDto dto, HandlerResult<ReportEmailResponseDto> result)
        {
            var channel = channelQuery
                .WithChannelId(dto.ChannelId)
                .ExecuteQuery()
                .FirstOrDefault();
             
            if (channel == null || !channel.EmailCommunicationSettings.Any())
                return;

            var emailSettings = Mapper.Map<AppSettingsEmailSetting>(channel.EmailCommunicationSettings.FirstOrDefault());
            var enableInsurerTemplate = ConfigurationManager.AppSettings[@"Reports/Storage"].ToString() != "false";

            var userId = dto.Context.UserId;
            var userIndividual = repository.GetAll<UserIndividual>().Where(u=>u.User.Id==userId);
            var displaName = userIndividual.Select(x => x.Individual.Surname).FirstOrDefault() == " "
                ? " "
                : userIndividual.Select(x => x.Individual.Surname).FirstOrDefault();

            var mailRegards = displaName + " " + channel.Name;
            var usernamePlusName = dto.FromEmail;
            var insurerCode = channel.Code;

            var template = "ContactUsEmailTemplate";
            var cellPhone = string.IsNullOrEmpty(dto.CellPhone)
                            ? " " 
                            : string.Format("cell phone {0}", dto.CellPhone);

            var alternativeMail = string.IsNullOrEmpty(dto.AlternativeEmail)
                            ? " " 
                            : string.Format("alternative mail {0}", dto.AlternativeEmail);

            var personalDetails = (cellPhone == " " && alternativeMail == " ")
                        ? " " :
                        string.Format("Personal Details {0} {1}.", cellPhone, alternativeMail);

            var data = new Dictionary<string, string>
            {
                {"FirstName", "iPlatform"},
                {"EmailBody", dto.Body },
                {"PhoneNumber", emailSettings.DefaultContactNumber },
                {"FromName", mailRegards},
                {"PersonalDetails", personalDetails},
            };
            dto.Email = "support@iplatform.co.za";
            var communicationMessageData = new CommunicationMessageData(data);
            var emailCommunicationMessage = new EmailCommunicationMessage(template, communicationMessageData, dto.Email, dto.Subject, templateEngine, emailSettings);


            if (enableInsurerTemplate)
            {
                var logo = string.Format(@"{0}content\reports\logos\{1}.png", AppDomain.CurrentDomain.BaseDirectory, insurerCode);

                var footerLogo = new EmbeddedResource();
                footerLogo.Attribute = "footerLogo";
                footerLogo.Value = @"<img src=""cid:{0}"" title=""Logo"" />";
                footerLogo.LinkedResource = new LinkedResource(logo);

                emailCommunicationMessage.EmbeddedResources.Add(footerLogo);
            }
            var message = new MailMessageFactory().Create(emailCommunicationMessage, emailSettings.AddCustomFrom(dto.FromEmail), true, usernamePlusName);

                new SmtpClientAdapter(emailSettings).Send(message);

                result.Processed(new ReportEmailResponseDto
                {
                    Success = true,
                    Body = message.Body
                });
        }
    }
}
