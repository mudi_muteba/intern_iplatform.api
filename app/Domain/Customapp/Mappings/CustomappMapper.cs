﻿using AutoMapper;
using Domain.Individuals;
using iPlatform.Api.DTOs.CustomApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Customapp.Mappings
{
    public class CustomappMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Individual, RegisterDto>()
                .ForMember(d => d.LastName, o => o.MapFrom(s => s.Surname))
                .ForMember(d => d.IDNumber, o => o.MapFrom(s => s.IdentityNo))
                .ForMember(d => d.MobileNumber, o => o.MapFrom(s => s.ContactDetail.Cell))
                .ForMember(d => d.EmailAddress, o => o.MapFrom(s => s.ContactDetail.Email))
                .ForMember(d => d.Occupation, o => o.MapFrom(s => s.Occupation.Name))
                .ForMember(d => d.WorkNumber, o => o.MapFrom(s => s.ContactDetail.Work))
                .ForMember(d => d.HomePhone, o => o.MapFrom(s => s.ContactDetail.Home))
                //if there are multiple address associated to a n individual get the latest default address
                .ForMember(d => d.AddressLine1, o => o.MapFrom(s => s.Addresses.OrderByDescending(a => a.Id).FirstOrDefault(a => a.IsDefault).Line1)) 
                .ForMember(d => d.AddressLine2, o => o.MapFrom(s => s.Addresses.OrderByDescending(a => a.Id).FirstOrDefault(a => a.IsDefault).Line2))
                .ForMember(d => d.AddressLine3, o => o.MapFrom(s => s.Addresses.OrderByDescending(a => a.Id).FirstOrDefault(a => a.IsDefault).Line3))
                .ForMember(d => d.AddressPostalCode, o => o.MapFrom(s => s.Addresses.OrderByDescending(a => a.Id).FirstOrDefault(a => a.IsDefault).Code));

            Mapper.CreateMap<Individual, GetMemberDetailsDto>()
                .ForMember(d => d.LastName, o => o.MapFrom(s => s.Surname))
                .ForMember(d => d.IDNumber, o => o.MapFrom(s => s.IdentityNo))
                .ForMember(d => d.CellphoneNumber, o => o.MapFrom(s => s.ContactDetail.Cell))
                .ForMember(d => d.EmailAddress, o => o.MapFrom(s => s.ContactDetail.Email))
                .ForMember(d => d.Occupation, o => o.MapFrom(s => s.Occupation.Name))
                .ForMember(d => d.WorkNumber, o => o.MapFrom(s => s.ContactDetail.Work))
                .ForMember(d => d.HomeNumber, o => o.MapFrom(s => s.ContactDetail.Home))
                //if there are multiple address associated to a n individual get the latest default address
                .ForMember(d => d.AddressLine1, o => o.MapFrom(s => s.Addresses.OrderByDescending(a => a.Id).FirstOrDefault(a => a.IsDefault).Line1))
                .ForMember(d => d.AddressLine2, o => o.MapFrom(s => s.Addresses.OrderByDescending(a => a.Id).FirstOrDefault(a => a.IsDefault).Line2))
                .ForMember(d => d.AddressLine3, o => o.MapFrom(s => s.Addresses.OrderByDescending(a => a.Id).FirstOrDefault(a => a.IsDefault).Line3))
                .ForMember(d => d.AddressPostalCode, o => o.MapFrom(s => s.Addresses.OrderByDescending(a => a.Id).FirstOrDefault(a => a.IsDefault).Code));
        }
    }
}
