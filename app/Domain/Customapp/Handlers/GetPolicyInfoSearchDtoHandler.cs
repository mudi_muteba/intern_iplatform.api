﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Policies;
using Domain.Products;
using iPlatform.Api.DTOs.CustomApp;
using MasterData;
using MasterData.Authorisation;
using ValidationMessages.Individual;

namespace Domain.Customapp.Handlers
{
    public class GetPolicyInfoSearchDtoHandler : BaseDtoHandler<GetPolicyInfoSearchDto, List<GetPolicyInfoDto>>
    {
        private readonly IRepository _repository;
        public GetPolicyInfoSearchDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void InternalHandle(GetPolicyInfoSearchDto dto, HandlerResult<List<GetPolicyInfoDto>> result)
        {
            List<PolicyHeader> policyHeaders = new List<PolicyHeader>();
            if (!String.IsNullOrWhiteSpace(dto.prmPolicyNumber))
            {
                policyHeaders = _repository.GetAll<PolicyHeader>()
                                        .Where(ph => ph.PolicyNo == dto.prmPolicyNumber)
                                        .ToList();
            }
            else if (!String.IsNullOrWhiteSpace(dto.prmIdNumber) || !String.IsNullOrWhiteSpace(dto.prmCellNumber))
            {
                //Get policies by IdentityNo or cell number
                Individual individual = _repository.GetAll<Individual>()
                    .FirstOrDefault(i => i.IdentityNo == dto.prmIdNumber ||
                                         i.ContactDetail.Cell == dto.prmCellNumber);

                policyHeaders = _repository.GetAll<PolicyHeader>()
                    .Where(ph => ph.Party.Id == individual.Id)
                    .ToList();
            }

            if (policyHeaders.Count == 0)
            {
                result.AddValidationMessage(IndividualValidationMessages.UnknownIndividual().AddParameters(new[] {"PROVIDE AT LEAST AN ID NUMBER OR A POLICY NUMBER"}));
            }
            else
            {
                List<GetPolicyInfoDto> getPolicyInfoDtoList = new List<GetPolicyInfoDto>();
                foreach (PolicyHeader policyHeader in policyHeaders)
                {
                    if (policyHeader != null)
                    {
                        //if policy items do exist take the first 5
                        List<PolicyItem> policyItems = policyHeader.PolicyItems != null
                            ? policyHeader.PolicyItems.Take(5).ToList()
                            : new List<PolicyItem>();
                        GetPolicyInfoDto getPolicyInfoDto = new GetPolicyInfoDto()
                        {
                            Insurer = null,
                            Product = policyHeader.Product.Name,
                            PolicyNumber = policyHeader.PolicyNo,
                            AnnualRenewalDate = policyHeader.DateRenewal.ToString(),
                            OriginalInceptionDate = policyHeader.DateEffective.ToString(),
                            PolicyType = null,
                            PolicyStatus = policyHeader.PolicyStatus.Name,
                            TotalPayment = null,
                            CoverType = null,
                            Id = null,
                            RiskItemType = null,
                            ItemDescription1 = policyItems.Count > 0 ? policyItems[0].ItemDescription : "",
                            ItemDescription2 = policyItems.Count > 1 ? policyItems[1].ItemDescription : "",
                            ItemDescription3 = policyItems.Count > 2 ? policyItems[2].ItemDescription : "",
                            ItemDescription4 = policyItems.Count > 3 ? policyItems[3].ItemDescription : "",
                            ItemDescription5 = policyItems.Count > 4 ? policyItems[4].ItemDescription : "",
                            SumInsured = policyItems.Sum(pi => pi.SumInsured).ToString(),
                            RiskPayment = null,
                            ExcessValue = policyItems.Sum(p => p.ExcessBasic).ToString(),
                            IsMotor = false,
                            IsProperty = false,
                            IsVAP = false
                        };

                        getPolicyInfoDtoList.Add((getPolicyInfoDto));
                    }
                }

                if (getPolicyInfoDtoList.Count > 0)
                {
                    result.Processed(getPolicyInfoDtoList);
                }
            }
        }
    }
}
