﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Party.Addresses;
using iPlatform.Api.DTOs.CustomApp;
using MasterData;
using MasterData.Authorisation;
using ValidationMessages.Individual;

namespace Domain.Customapp.Handlers
{
    public class UpdateMemberDetailsSearchDtoHandler : BaseDtoHandler<UpdateMemberDetailsSearchDto, List<UpdateMemberDetailsDto>>
    {
        private readonly IRepository _repository;
        public UpdateMemberDetailsSearchDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void InternalHandle(UpdateMemberDetailsSearchDto dto, HandlerResult<List<UpdateMemberDetailsDto>> result)
        {
            Individual individual = _repository.GetAll<Individual>()
                                                .FirstOrDefault(i => i.IdentityNo == dto.prmIDNumber);

            if (individual != null)
            {
                //TODO: Find out best practice to update occupation                                
                individual.ContactDetail.Work = dto.prmUpdatedWorkNumber;
                individual.ContactDetail.Home = dto.prmUpdatedHomeNumber;
                individual.FirstName = dto.prmUpdatedFirstName;
                individual.Surname = dto.prmUpdatedSurname;
                individual.ContactDetail.Cell = dto.prmUpdatedCellphone;
                individual.ContactDetail.Email = dto.prmUpdatedEmailAddress;

                //if there is more than one default address get the latest one
                Address address = individual.Addresses.OrderByDescending(a => a.Id)
                                                    .FirstOrDefault(a => a.IsDefault &&
                                                         !a.IsDeleted &&
                                                         a.AddressType == AddressTypes.PhysicalAddress);
                if (address != null)
                {
                    address.Line1 = dto.prmUpdatedAddressLine1;
                    address.Line2 = dto.prmUpdatedAddressLine2;
                    address.Line3 = dto.prmUpdatedAddressLine3;
                    address.Code = dto.prmUpdatedPostalCode;
                    _repository.Save(address);
                }

                _repository.Save(individual);

                List<UpdateMemberDetailsDto> updateMemberDetailsDtoList = new List<UpdateMemberDetailsDto>();
                result.Processed(updateMemberDetailsDtoList);
            }
            else
            {
                result.AddValidationMessage(IndividualValidationMessages.UnknownIndividual().AddParameters(new[] { "500 INTERNAL SERVER ERROR" }));
            }
        }
    }
}
