﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Policies;
using iPlatform.Api.DTOs.CustomApp;
using MasterData.Authorisation;
using ValidationMessages.Individual;

namespace Domain.Customapp.Handlers
{
    public class RegisterSearchDtoHandler : BaseDtoHandler<RegisterSearchDto, List<RegisterDto>>
    {
        private readonly IRepository _repository;
        public RegisterSearchDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void InternalHandle(RegisterSearchDto dto, HandlerResult<List<RegisterDto>> result)
        {
            Individual individual = _repository.GetAll<Individual>()
                                            .FirstOrDefault(i => i.IdentityNo == dto.prmIDNumber ||
                                                i.FirstName == dto.prmFirstName ||
                                                i.Surname == dto.prmLastName ||
                                                i.ContactDetail.Email == dto.prmEmailAddress ||
                                                i.ContactDetail.Cell == dto.prmMobileNumber);

            if (individual == null)
            {
                //try find the individual by policy number
                PolicyHeader policyHeader = _repository.GetAll<PolicyHeader>()
                                                        .FirstOrDefault(ph => ph.PolicyNo == dto.prmPolicyNumber);
                if (policyHeader != null)
                {
                    individual = _repository.GetAll<Individual>()
                                            .FirstOrDefault(i => i.Id == policyHeader.Party.Id);
                }
            }

            if (individual != null)
            {
                List<RegisterDto> registerDtoList = new List<RegisterDto> {Mapper.Map<RegisterDto>(individual)};
                result.Processed(registerDtoList);
            }
            else
            {
                result.AddValidationMessage(IndividualValidationMessages.UnknownIndividual().AddParameters(new[] { "MEMBER NOT FOUND" }));
            }
        }
    }
}
