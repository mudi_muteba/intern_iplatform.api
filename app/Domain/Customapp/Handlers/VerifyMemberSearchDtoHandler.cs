﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Policies;
using iPlatform.Api.DTOs.CustomApp;
using MasterData;
using MasterData.Authorisation;
using ValidationMessages.Individual;

namespace Domain.Customapp.Handlers
{
    public class VerifyMemberSearchDtoHandler : BaseDtoHandler<VerifyMemberSearchDto, List<VerifyMemberDto>>
    {
        private readonly IRepository _repository;
        public VerifyMemberSearchDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void InternalHandle(VerifyMemberSearchDto dto, HandlerResult<List<VerifyMemberDto>> result)
        {
            bool isValid = false;
            //first try find the individual
            Individual individual = _repository.GetAll<Individual>()
                                                .FirstOrDefault(i => i.IdentityNo == dto.prmIDNumber ||
                                                    i.ContactDetail.Cell == dto.prmCellNumber);
            if (individual != null)
            {
                //if the individual exists try find the policy header
                PolicyHeader policyHeader = _repository.GetAll<PolicyHeader>()
                    .FirstOrDefault(p => p.Party.Id == individual.Id &&
                                         p.PolicyNo == dto.prmPolicyNumber &&
                                         p.PolicyStatus == PolicyStatuses.Active);

                //if an active policy is found verify the member
                if (policyHeader != null)
                {
                    isValid = true;
                }

                List<VerifyMemberDto> verifyMemberDtoList = new List<VerifyMemberDto>();
                verifyMemberDtoList.Add(new VerifyMemberDto() {IsValid = isValid});

                result.Processed(verifyMemberDtoList);
            }
            else
            {
                result.AddValidationMessage(IndividualValidationMessages.UnknownIndividual().AddParameters(new[] { "INVALID ID NUMBER SPECIFIED" }));
            }
        }
    }
}
