﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Tia.Handlers.PartiesSearch;
using iPlatform.Api.DTOs.CustomApp;
using iPlatform.Api.DTOs.Tia.Parties;
using MasterData.Authorisation;
using Domain.Individuals;
using ValidationMessages.Individual;

namespace Domain.Customapp.Handlers
{
    public class GetMemberDetailsSearchDtoHandler : BaseDtoHandler<GetMemberDetailsSearchDto, List<GetMemberDetailsDto>>
    {
        private readonly IRepository _repository;

        public GetMemberDetailsSearchDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint> { }; }
        }

        protected override void InternalHandle(GetMemberDetailsSearchDto dto, HandlerResult<List<GetMemberDetailsDto>> result)
        {
            Individual individual = _repository.GetAll<Individual>()
                                                .FirstOrDefault(i => i.IdentityNo == dto.prmIDNumber);

            if (individual != null)
            {
                List<GetMemberDetailsDto> getMemberDetailsDtoList = new List<GetMemberDetailsDto>();
                getMemberDetailsDtoList.Add(Mapper.Map<GetMemberDetailsDto>(individual));

                result.Processed(getMemberDetailsDtoList);
            }
            else
            {
                result.AddValidationMessage(IndividualValidationMessages.UnknownIndividual().AddParameters(new[] { "MEMBER DETAILS NOT FOUND" }));
            }
        }
    }
}
