﻿using Domain.Base.Repository.Customisation;
using Domain.Sales;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Sales.Overrides
{
    public class SalesTagOverride : IAutoMappingOverride<SalesTag>
    {
        public void Override(AutoMapping<SalesTag> mapping)
        {
        }
    }
}