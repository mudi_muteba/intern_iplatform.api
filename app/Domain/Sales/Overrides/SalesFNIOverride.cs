﻿using Domain.Base.Repository.Customisation;
using Domain.Sales;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Sales.Overrides
{
    public class SalesFNIOverride : IAutoMappingOverride<SalesFNI>
    {
        public void Override(AutoMapping<SalesFNI> mapping)
        {
            mapping.Map(x => x.PartyId).Nullable();
            mapping.Id(x => x.Id).GeneratedBy.Identity();
        }
    }
}