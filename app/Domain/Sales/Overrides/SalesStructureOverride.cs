﻿using Domain.Base.Repository.Customisation;
using Domain.Sales;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Sales.Overrides
{
    public class SalesStructureOverride : IAutoMappingOverride<SalesStructure>
    {
        public void Override(AutoMapping<SalesStructure> mapping)
        {
            mapping.References(x => x.BranchSalesTag);
            mapping.References(x => x.CompanySalesTag);
            mapping.References(x => x.GroupSalesTag);
            mapping.References(x => x.Channel);
        }
    }
}