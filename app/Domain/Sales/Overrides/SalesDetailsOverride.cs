﻿using Domain.Base.Repository.Customisation;
using Domain.Sales;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Sales.Overrides
{
    public class SalesDetailsOverride : IAutoMappingOverride<SalesDetails>
    {
        public void Override(AutoMapping<SalesDetails> mapping)
        {
            mapping.References(x => x.Agent).Cascade.None();
            mapping.References(x => x.Asset).Cascade.None();
            mapping.References(x => x.Lead).Cascade.None();
            mapping.References(x => x.SalesStructure).Cascade.None();
        }
    }
}