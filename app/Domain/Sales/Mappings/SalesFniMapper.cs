﻿using AutoMapper;
using Domain.Admin;
using iPlatform.Api.DTOs.Sales;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData;

namespace Domain.Sales.Mappings
{
    public class SalesFniMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateSalesFNIDto, SalesFNI>()
                .ForMember(d => d.EmployeeNumber, opt => opt.MapFrom(x => x.EmployeeNumber))
                .ForMember(d => d.IdNumber, opt => opt.MapFrom(x => x.FiidNumber))
                .ForMember(d => d.Username, opt => opt.MapFrom(x => x.UserName))
                .ForMember(d => d.Firstname, opt => opt.MapFrom(x => x.FiFirstName))
                .ForMember(d => d.Lastname, opt => opt.MapFrom(x => x.FiLastName))
                .ForMember(d => d.WorkTelephoneCode, opt => opt.MapFrom(x => x.UserWorkTelephoneCode))
                .ForMember(d => d.WorkTelephoneNumber, opt => opt.MapFrom(x => x.UserWorkTelephoneNumber))
                .ForMember(d => d.FaxCode, opt => opt.MapFrom(x => x.UserFaxCode))
                .ForMember(d => d.FaxNumber, opt => opt.MapFrom(x => x.UserFaxNumber))
                .ForMember(d => d.EmailAddress, opt => opt.MapFrom(x => x.UserEmailAddress))
                ;
            

            Mapper.CreateMap<ImportLeadDto, CreateSalesFNIDto>()
                .ForMember(d => d.UserName, opt => opt.MapFrom(x => x.SalesFniRequest.UserName))
                .ForMember(d => d.UserWorkTelephoneCode, opt => opt.MapFrom(x => x.SalesFniRequest.UserWorkTelephoneCode))
                .ForMember(d => d.UserWorkTelephoneNumber, opt => opt.MapFrom(x => x.SalesFniRequest.UserWorkTelephoneNumber))
                .ForMember(d => d.UserEmailAddress, opt => opt.MapFrom(x => x.SalesFniRequest.UserEmailAddress))
                .ForMember(d => d.UserFaxCode, opt => opt.MapFrom(x => x.SalesFniRequest.UserFaxCode))
                .ForMember(d => d.UserFaxNumber, opt => opt.MapFrom(x => x.SalesFniRequest.UserFaxNumber))
                .ForMember(d => d.EmployeeNumber, opt => opt.MapFrom(s => s.SalesFniRequest.EmployeeNumber))
                .ForMember(d => d.FiFirstName, opt => opt.MapFrom(s => s.SalesFniRequest.FiFirstName))
                .ForMember(d => d.FiLastName, opt => opt.MapFrom(s => s.SalesFniRequest.FiLastName))
                .ForMember(d => d.FiidNumber, opt => opt.MapFrom(s => s.SalesFniRequest.FiidNumber))
                .ForMember(d => d.DisplayName, opt => opt.MapFrom(s => s.SalesFniRequest.DisplayName))
                .ForMember(d => d.ChannelId, opt => opt.MapFrom(s => s.SalesFniRequest.ChannelId))
                .ForMember(d => d.DateCreated, opt => opt.MapFrom(s => s.SalesFniRequest.DateCreated))
                ;

            Mapper.CreateMap<ImportLeadDto, EditSalesFNIDto>()
                .ForMember(d => d.UserName, opt => opt.MapFrom(x => x.SalesFniRequest.UserName))
                .ForMember(d => d.UserWorkTelephoneCode, opt => opt.MapFrom(x => x.SalesFniRequest.UserWorkTelephoneCode))
                .ForMember(d => d.UserWorkTelephoneNumber, opt => opt.MapFrom(x => x.SalesFniRequest.UserWorkTelephoneNumber))
                .ForMember(d => d.UserEmailAddress, opt => opt.MapFrom(x => x.SalesFniRequest.UserEmailAddress))
                .ForMember(d => d.UserFaxCode, opt => opt.MapFrom(x => x.SalesFniRequest.UserFaxCode))
                .ForMember(d => d.UserFaxNumber, opt => opt.MapFrom(x => x.SalesFniRequest.UserFaxNumber))
                .ForMember(d => d.EmployeeNumber, opt => opt.MapFrom(s => s.SalesFniRequest.EmployeeNumber))
                .ForMember(d => d.FiFirstName, opt => opt.MapFrom(s => s.SalesFniRequest.FiFirstName))
                .ForMember(d => d.FiLastName, opt => opt.MapFrom(s => s.SalesFniRequest.FiLastName))
                .ForMember(d => d.FiidNumber, opt => opt.MapFrom(s => s.SalesFniRequest.FiidNumber))
                ;

            Mapper.CreateMap<CreateSalesFNIDto, CreateIndividualDto>()
                .ForMember(d => d.FirstName, opt => opt.MapFrom(x => x.FiFirstName))
                .ForMember(d => d.Surname, opt => opt.MapFrom(x => x.FiLastName))
                .ForMember(d => d.IdentityNo, opt => opt.MapFrom(x => x.FiidNumber))
                .ForMember(d => d.ContactDetail,
                    opt =>
                        opt.MapFrom(
                            x =>
                                new CreateContactDetailDto
                                {
                                    Work = x.UserWorkTelephoneNumber,
                                    Email = x.UserEmailAddress,
                                    Fax = x.UserFaxNumber
                                }));
        }
    }
}