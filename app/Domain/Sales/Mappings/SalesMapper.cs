﻿using AutoMapper;
using iPlatform.Api.DTOs.Sales;
using iPlatform.Api.DTOs.Leads;
using Domain.Admin;
using System.Collections.Generic;
using Domain.Party.Assets;
using Domain.Leads;
using iPlatform.Api.DTOs.Admin;

namespace Domain.Sales.Mappings
{
    public class SalesMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ImportLeadDto, ImportSalesStructureDto>()
                .ForMember(t => t.SystemMode, o => o.MapFrom(s => s.DealDto.SystemMode))
                .ForMember(t => t.CurrentDate, o => o.MapFrom(s => s.DealDto.CurrentDate))
                .ForMember(t => t.PolicyNumber, o => o.MapFrom(s => s.DealDto.PolicyNumber))
                .ForMember(t => t.BranchCode, o => o.MapFrom(s => s.DealDto.BranchCode))
                .ForMember(t => t.BranchName, o => o.MapFrom(s => s.DealDto.BranchName))
                .ForMember(t => t.FinanceCompanyCode, o => o.MapFrom(s => s.DealDto.FinanceCompanyCode))
                .ForMember(t => t.FinanceCompanyName, o => o.MapFrom(s => s.DealDto.FinanceCompanyName))
                .ForMember(t => t.GroupCode, o => o.MapFrom(s => s.DealDto.GroupCode))
                .ForMember(t => t.GroupName, o => o.MapFrom(s => s.DealDto.GroupName))
                .ForMember(t => t.CompanyCode, o => o.MapFrom(s => s.DealDto.CompanyCode))
                .ForMember(t => t.SaleFNIId, o => o.Ignore())
                .ForMember(t => t.LeadId, o => o.Ignore())
                .ForMember(d => d.Channel, o => o.MapFrom(s => new ChannelInfoDto() { Id = s.ChannelId }))
                ;


            Mapper.CreateMap<SalesStructureDto, SalesStructure>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Channel, o => o.MapFrom(s => s.Channel))
                .ForMember(t => t.BranchSalesTag, o => o.MapFrom(s => s.BranchSalesTag))
                .ForMember(t => t.CompanySalesTag, o => o.MapFrom(s => s.CompanySalesTag))
                .ForMember(t => t.GroupSalesTag, o => o.MapFrom(s => s.GroupSalesTag))
                ;

            Mapper.CreateMap<CreateSalesDetailsDto, List<SalesDetails>>()
                .AfterMap((s, d) =>
                {
                    s.SalesDetails.ForEach(t =>
                    {
                        var entity = new SalesDetails();
                        entity.Agent = Mapper.Map<int, SalesFNI>(s.SalesFNIId);
                        entity.Asset = Mapper.Map<int, Asset>(t.AssetId);
                        entity.SalesStructure = Mapper.Map<int, SalesStructure>(s.SaleStructureId);
                        entity.Lead = Mapper.Map<int, Lead>(s.LeadId);
                        entity.PolicyNumber = s.PolicyNumber;
                        entity.IdNumber = s.IdNumber;
                        entity.CurrentDate = s.CurrentDate;
                        entity.VehicleRetailPrice = t.VehicleRetailPrice;
                        d.Add(entity);
                    });
                });

            Mapper.CreateMap<SalesTag, SalesTagDto>()
                .ReverseMap();

            Mapper.CreateMap<SalesStructure, SalesStructureDto>();

            Mapper.CreateMap<SalesFNI, SalesFNIDto>()
                .ForMember(d => d.FiFirstName, o => o.MapFrom(s => s.Firstname))
                .ForMember(d => d.FiLastName, o => o.MapFrom(s => s.Lastname))
                .ForMember(d => d.UserEmailAddress, o => o.MapFrom(s => s.EmailAddress));

            Mapper.CreateMap<SalesDetails, SalesDetailDto>();
        }

    }
}
