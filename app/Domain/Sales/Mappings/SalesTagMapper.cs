﻿using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Sales;
using System.Collections.Generic;

namespace Domain.Sales.Mappings
{
    public class SalesTagMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateSalesTagDto, SalesTag>()
                .ForMember(d => d.Code, opt => opt.MapFrom(x => x.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(d => d.SalesTagType, opt => opt.MapFrom(x => x.SalesTagType));
            Mapper.CreateMap<SalesTag, SalesTagDto>();
            Mapper.CreateMap<List<SalesTag>, ListResultDto<SalesTagDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));
            Mapper.CreateMap<PagedResults<SalesTag>, PagedResultDto<SalesTagDto>>();

            Mapper.CreateMap<CreateSalesTagDto, SalesTagDto>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;
        }
    }
}
