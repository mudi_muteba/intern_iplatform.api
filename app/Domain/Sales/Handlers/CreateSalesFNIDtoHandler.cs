﻿using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Sales.Events;
using System.Linq;

using iPlatform.Api.DTOs.Sales;
using MasterData.Authorisation;

namespace Domain.Sales.Handlers
{
    public class CreateSalesFNIDtoHandler : CreationDtoHandler<SalesFNI, CreateSalesFNIDto, int>
    {
        private readonly IRepository _repository;
        public CreateSalesFNIDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void EntitySaved(SalesFNI entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override SalesFNI HandleCreation(CreateSalesFNIDto dto, HandlerResult<int> result)
        {
            var entity = _repository.GetAll<SalesFNI>().FirstOrDefault(x => x.EmployeeNumber == dto.EmployeeNumber || x.IdNumber == dto.FiidNumber);
            if (entity == null)
                entity = SalesFNI.Create(dto);
            else
                entity = entity.Update(dto);
            return entity;
        }
    }
}
