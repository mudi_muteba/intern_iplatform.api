﻿
using System.Linq;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Sales;
using MasterData.Authorisation;
using AutoMapper;

namespace Domain.Sales.Handlers
{
    public class CreateSalesStructuresDtoHandler : BaseDtoHandler<CreateSalesStructuresDto, int>
    {
        public CreateSalesStructuresDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(CreateSalesStructuresDto dto, HandlerResult<int> result)
        {
            var id = 0;
            var saleStructures = GetSaleStructures(dto);

            foreach (var salestructure in dto.SalesStructures)
            {
                var existingSaleStructure = saleStructures
                    .FirstOrDefault(x => x.CompanySalesTag.Id == salestructure.CompanySalesTag.Id && 
                    x.BranchSalesTag.Id == salestructure.BranchSalesTag.Id);

                if (existingSaleStructure != null)
                {
                    id = existingSaleStructure.Id;
                    continue;
                }

                var entity = Mapper.Map<SalesStructure>(salestructure);
                _repository.Save(entity);
                id = entity.Id;
            }
            result.Processed(id);
        }


        private List<SalesStructure> GetSaleStructures(CreateSalesStructuresDto dto)
        {
            var result = new List<SalesStructure>();
            if (dto.SalesStructures.Any())
                result = _repository.GetAll<SalesStructure>().Where(x => x.GroupSalesTag.Id == dto.SalesStructures.FirstOrDefault().GroupSalesTag.Id).ToList();
            return result;
        }
    }
}
