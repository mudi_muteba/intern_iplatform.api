﻿
using System.Linq;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Sales;
using MasterData.Authorisation;
using AutoMapper;

namespace Domain.Sales.Handlers
{
    public class CreateSalesDetailsDtoHandler : BaseDtoHandler<CreateSalesDetailsDto, bool>
    {
        public CreateSalesDetailsDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(CreateSalesDetailsDto dto, HandlerResult<bool> result)
        {
            var entities = Mapper.Map<List<SalesDetails>>(dto);
            foreach (var entity in entities)
            {
                _repository.Save(entity);
            }

            result.Processed(true);
        }
    }
}
