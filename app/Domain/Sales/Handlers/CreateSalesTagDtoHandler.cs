﻿using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Sales;
using MasterData.Authorisation;

namespace Domain.Sales.Handlers
{
    public class CreateSalesTagDtoHandler : CreationDtoHandler<SalesTag,CreateSalesTagDto, int>
    {
        public CreateSalesTagDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void EntitySaved(SalesTag entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override SalesTag HandleCreation(CreateSalesTagDto dto, HandlerResult<int> result)
        {
            return SalesTag.Create(dto);
        }
    }
}
