﻿using Domain.Base.Events;
using iPlatform.Api.DTOs.Sales;

namespace Domain.Sales.Events
{
    public class SalesFNICreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public CreateSalesFNIDto CreateSalesFniDto { get; set; }

        public SalesFNICreatedEvent(CreateSalesFNIDto createSalesFniDto)
        {
            CreateSalesFniDto = createSalesFniDto;
        }
    }
}
