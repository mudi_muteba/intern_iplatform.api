﻿using Domain.Base;
using Domain.Leads;
using Domain.Party.Assets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sales
{
    public class SalesDetails : Entity
    {
        public SalesDetails() { }
        public virtual string PolicyNumber { get; set; }
        public virtual string IdNumber { get; set; }
        public virtual Asset Asset { get; set; }
        public virtual string VehicleRetailPrice { get; set; }
        public virtual string CurrentDate { get; set; }
        public virtual SalesFNI Agent { get; set; }
        public virtual Lead Lead { get; set; }
        public virtual SalesStructure SalesStructure { get; set; }
    }
}
