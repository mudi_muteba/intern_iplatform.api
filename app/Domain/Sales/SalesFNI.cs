﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.Sales;

namespace Domain.Sales
{
    public class SalesFNI : Entity
    {
        public SalesFNI() { }

        public virtual string EmployeeNumber { get; set; }
        public virtual string IdNumber { get; set; }
        public virtual string Username { get; set; }
        public virtual string WorkTelephoneCode { get; set; }
        public virtual string WorkTelephoneNumber { get; set; }
        public virtual string FaxCode { get; set; }
        public virtual string FaxNumber { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual string Firstname { get; set; }
        public virtual string Lastname { get; set; }
        public virtual int PartyId { get; set; }
        public static SalesFNI Create(CreateSalesFNIDto createSalesFniDto)
        {
            var salesFni = Mapper.Map<SalesFNI>(createSalesFniDto);
            return salesFni;
        }

        public virtual SalesFNI Update(CreateSalesFNIDto createSalesFniDto)
        {
            var salesFni = Mapper.Map<CreateSalesFNIDto, SalesFNI>(createSalesFniDto, this);
            return salesFni;
        }
    }
}
