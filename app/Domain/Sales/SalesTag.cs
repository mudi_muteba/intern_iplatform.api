﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.Sales;
using MasterData;


namespace Domain.Sales
{
    public class SalesTag : Entity
    {
        public SalesTag() { }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual SalesTagType SalesTagType { get; set; }

        public static SalesTag Create(CreateSalesTagDto createSalesTagDto)
        {
            var salesTag = Mapper.Map<SalesTag>(createSalesTagDto);
            return salesTag;
        }
    }
}
