﻿using Domain.Base.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData.Authorisation;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Sales.Queries.Filters;

namespace Domain.Sales.Queries
{
    public class GetSalesDetailByLeadIdQuery : BaseQuery<SalesDetails>
    {
        private int m_LeadId;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetSalesDetailByLeadIdQuery(IProvideContext contextProvider, IRepository reposiory) 
            : base(contextProvider, reposiory, new DefaultSalesDetailFilter()) { }

        public GetSalesDetailByLeadIdQuery WithCriteria(int leadId)
        {
            m_LeadId = leadId;
            return this;
        }

        protected internal override IQueryable<SalesDetails> Execute(IQueryable<SalesDetails> query)
        {
            return query.Where(q => q.Lead.Id == m_LeadId);
        }
    }
}
