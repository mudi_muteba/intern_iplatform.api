﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Sales;

namespace Domain.Sales.Queries
{
    public class SearchSalesTagQuery : BaseQuery<SalesTag>, ISearchQuery<SalesTagSearchDto>
    {
        private SalesTagSearchDto criteria;

        public SearchSalesTagQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultSalesTagFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    //CampaignAuthorisation.List
                };
            }
        }

        public void WithCriteria(SalesTagSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<SalesTag> Execute(IQueryable<SalesTag> query)
        {

            if (!string.IsNullOrWhiteSpace(criteria.Code))
                query = query.Where(a => a.Code.StartsWith(criteria.Code));

            if (!string.IsNullOrWhiteSpace(criteria.Name))
                query = query.Where(a => a.Name.StartsWith(criteria.Name));

            return query;
        }
    }
}