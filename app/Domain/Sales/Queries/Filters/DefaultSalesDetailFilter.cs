﻿using Domain.Base.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;

namespace Domain.Sales.Queries.Filters
{
    public class DefaultSalesDetailFilter : IApplyDefaultFilters<SalesDetails>
    {
        public IQueryable<SalesDetails> Apply(ExecutionContext executionContext, IQueryable<SalesDetails> query)
        {
            return query.Where(q => !q.IsDeleted);
        }
    }
}
