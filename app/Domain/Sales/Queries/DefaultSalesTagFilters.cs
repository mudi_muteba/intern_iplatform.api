﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Sales.Queries
{
    public class DefaultSalesTagFilters : IApplyDefaultFilters<SalesTag>
    {
        public IQueryable<SalesTag> Apply(ExecutionContext executionContext, IQueryable<SalesTag> query)
        {
            return query
                .Where(c => c.IsDeleted == false);
        }
    }
}