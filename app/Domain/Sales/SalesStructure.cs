﻿using Domain.Admin;
using Domain.Base;


namespace Domain.Sales
{
    public class SalesStructure : EntityWithAudit
    {
        public SalesStructure() { }
        public virtual SalesTag BranchSalesTag { get; set; }
        public virtual SalesTag CompanySalesTag { get; set; }
        public virtual SalesTag GroupSalesTag { get; set; }
        public virtual int TagLeft { get; set; }
        public virtual int TagRight { get; set; }
        public virtual int Level { get; set; }
        public virtual Channel Channel {get; set;}
    }
}
