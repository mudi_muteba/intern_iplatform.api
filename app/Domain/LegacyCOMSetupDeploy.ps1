$package = $OctopusParameters["Octopus.Action[Deploy Api].Output.Package.InstallationDirectoryPath"]
"Installation Directory Path: $package"

$location = Join-Path $package "content\bin"
"Library Directory Path: $location"

Set-Location $location

$comAdmin = New-Object -comobject COMAdmin.COMAdminCatalog
$apps = $comAdmin.GetCollection("Applications")
$apps.Populate();

$LegacyLibrary = "cdvdll.dll"
$COMPackage = "CardinalLegacyCOMLibrary"
$COMLibrary = "CardinalLegacyCOMLibrary.dll"

$LogDirectory = "Log"
$LogDirectory = join-path $location $LogDirectory
"Log Path: $LogDirectory"
New-Item -ItemType Directory -Force -Path $LogDirectory

$LegacyLibraryPath = Get-Item $LegacyLibrary
"Legacy DLL Path: $LegacyLibraryPath"

$COMWrapperPath = Get-Item $COMLibrary
"COM Wrapper Path: $COMWrapperPath"

$SystemPath32 = "$env:SystemRoot\Sysnative"
"System32 Path: $SystemPath32"

$SystemPath64 = "$env:SystemRoot\SysWOW64"
"SysWOW64 Path: $SystemPath64"

$appExistCheckApp = $apps | Where-Object {$_.Name -eq $COMPackage}

if($appExistCheckApp)
{
	$index = 0

    foreach($app in $apps)
	{
        if ($app.Name -eq $COMPackage)
		{
            $apps.Remove($index)
            $apps.SaveChanges()
        }

        $index++
    }

	"$COMPackage removed"
}
else
{
	"Copying $LegacyLibraryPath to $SystemPath32"
	Copy-Item $LegacyLibraryPath $SystemPath32
	"$LegacyLibrary copied to $SystemPath32"

	"Copying $LegacyLibraryPath to $SystemPath64"
	Copy-Item $LegacyLibraryPath $SystemPath64
	"$LegacyLibrary copied to $SystemPath64"
}

$legacyApp = $apps.Add()

$legacyApp.Value("ApplicationAccessChecksEnabled") = 0
$legacyApp.Value("DumpPath") = $LogDirectory
$legacyApp.Value("DumpEnabled") = 1
$legacyApp.Value("DumpOnException") = 1
$legacyApp.Value("DumpOnFailfast") = 1
$legacyApp.Value("Identity") = "nt authority\networkservice"
$legacyApp.Value("IsEnabled") = 1
$legacyApp.Value("Name") = $COMPackage
$legacyApp.Value("Password") = ""
$legacyApp.Value("RunForever") = 1
	
$saveChangesResult = $apps.SaveChanges()
"$COMPackage COM+ application created"

$comAdmin.InstallComponent($COMPackage, $COMWrapperPath, $null, $null)
"$COMLibrary added to COM+ application $COMPackage"

$saveChangesResult = $apps.SaveChanges()