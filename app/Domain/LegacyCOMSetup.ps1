param($ProjectDir)

$location = $ProjectDir

if(!$ProjectDir)
{
	$location = $PSScriptRoot
    Set-Location $PSScriptRoot
    "Location: $PSScriptRoot"
}
else
{
	$location = $ProjectDir
    Set-Location $ProjectDir
    "Location: $ProjectDir"
}

$comAdmin = New-Object -comobject COMAdmin.COMAdminCatalog
$apps = $comAdmin.GetCollection("Applications")
$apps.Populate();

$COMPackage = "CardinalLegacyCOMLibrary"

$appExistCheckApp = $apps | Where-Object {$_.Name -eq $COMPackage}

$LegacyLibrary = "cdvdll.dll"
$COMLibrary = "CardinalLegacyCOMLibrary.dll"

$LegacyLibraryPath = Get-Item ..\..\libs\BankAccountValidator\$LegacyLibrary
"Legacy Library Path: $LegacyLibraryPath"

$COMWrapperPath = Get-Item ..\..\libs\BankAccountValidator\$COMLibrary

"COM Wrapper Path: $COMWrapperPath"

$LogDirectory = "Log"
$LogDirectory = join-path $location $LogDirectory
"Log Path: $LogDirectory"
New-Item -ItemType Directory -Force -Path $LogDirectory

$SystemPath32 = "$env:SystemRoot\Sysnative\"
$SystemPath64 = "$env:SystemRoot\SysWOW64\"

if($appExistCheckApp)
{
	"$COMPackage COM+ application already exists. Stopping application."

	$comAdmin.ShutdownApplication($COMPackage)

	$index = 0

    foreach($app in $apps)
	{
        if ($app.Name -eq $COMPackage)
		{
			"Stopped. Removing $COMPackage COM+ application"

            $apps.Remove($index)
            $apps.SaveChanges()
        }

        $index++
    }

	"$COMPackage removed"
}
else
{
	Copy-Item $LegacyLibraryPath $SystemPath32
	"$LegacyLibrary copied to $SystemPath32"

	Copy-Item $LegacyLibraryPath $SystemPath64
	"$LegacyLibrary copied to $SystemPath64"
}

$legacyApp = $apps.Add()

$legacyApp.Value("ApplicationAccessChecksEnabled") = 0
$legacyApp.Value("DumpPath") = $LogDirectory
$legacyApp.Value("DumpEnabled") = 1
$legacyApp.Value("DumpOnException") = 1
$legacyApp.Value("DumpOnFailfast") = 1
$legacyApp.Value("Identity") = "nt authority\networkservice"
$legacyApp.Value("IsEnabled") = 1
$legacyApp.Value("Name") = $COMPackage
$legacyApp.Value("Password") = ""
$legacyApp.Value("RunForever") = 1
	
$result = $apps.SaveChanges()

if($result = 1)
{
    "$COMPackage COM+ application created, installing COM wrapper libraries"
	    
    $comAdmin.InstallComponent($COMPackage, $COMWrapperPath, $null, $null)
    "$COMLibrary added to COM+ application $COMPackage"
}
else
{
    "Unable to create $COMPackage COM+ application."
}

$comAdmin.StartApplication($COMPackage)