﻿using Domain.Base.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData.Authorisation;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.ProductPricingStructure.Queries.Filters;

namespace Domain.ProductPricingStructure.Queries
{
    public class GetProductPricingStructureByChannelIdProductIdAndCoverIdQuery : BaseQuery<ProductPricingStructure>
    {
        private int m_ChannelId;
        private int m_ProductId;
        private int m_CoverId;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetProductPricingStructureByChannelIdProductIdAndCoverIdQuery(IProvideContext contextProvider, IRepository repository)
            :base(contextProvider,repository, new DefaultProductPricingStructureFilter()) { }

        public GetProductPricingStructureByChannelIdProductIdAndCoverIdQuery WIthCriteria(int channelId, int productId, int coverId)
        {
            m_ChannelId = channelId;
            m_ProductId = productId;
            m_CoverId = coverId;

            return this;
        }

        protected internal override IQueryable<ProductPricingStructure> Execute(IQueryable<ProductPricingStructure> query)
        {
            DateTime currentDate = DateTime.UtcNow;
            query.Where(q => q.Channel.Id == m_ChannelId &&
                q.Product.Id == m_ProductId &&
                q.Cover.Id == m_CoverId &&
                q.StartDate <= currentDate &&
                q.EndDate >= currentDate);

            return query;
        }
    }
}
