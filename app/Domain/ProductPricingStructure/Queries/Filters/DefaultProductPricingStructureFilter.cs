﻿using Domain.Base.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;

namespace Domain.ProductPricingStructure.Queries.Filters
{
    public class DefaultProductPricingStructureFilter : IApplyDefaultFilters<ProductPricingStructure>
    {
        public IQueryable<ProductPricingStructure> Apply(ExecutionContext executionContext, IQueryable<ProductPricingStructure> query)
        {
            return query.Where(q => !q.IsDeleted);
        }
    }
}
