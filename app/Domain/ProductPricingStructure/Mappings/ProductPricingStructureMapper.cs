﻿using AutoMapper;
using iPlatform.Api.DTOs.ProductPricingStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ProductPricingStructure.Mappings
{
    public class ProductPricingStructureMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ProductPricingStructure, ProductPricingStructureDto>();
        }
    }
}
