﻿using Domain.Admin;
using Domain.Base;
using Domain.Products;
using Infrastructure.NHibernate.Attributes;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ProductPricingStructure
{
    public class ProductPricingStructure : Entity
    {
        public ProductPricingStructure() { }

        public virtual Product Product { get; set; }

        public virtual Cover Cover { get; set; }

        public virtual DateTime StartDate { get; set; }

        public virtual DateTime EndDate { get; set; }

        public virtual Channel Channel { get; set; }

        public virtual decimal Monthly1YearDefault { get; set; }

        public virtual decimal Monthly1YearMin { get; set; }

        public virtual decimal Monthly1YearMax { get; set; }

        public virtual decimal Monthly2YearDefault { get; set; }

        public virtual decimal Monthly2YearMin { get; set; }

        public virtual decimal Monthly2YearMax { get; set; }

        public virtual decimal Monthly3YearDefault { get; set; }

        public virtual decimal Monthly3YearMin { get; set; }

        public virtual decimal Monthly3YearMax { get; set; }

        public virtual decimal Annual1YearDefault { get; set; }

        public virtual decimal Annual1YearMin { get; set; }

        public virtual decimal Annual1YearMax { get; set; }

        public virtual decimal Annual2YearDefault { get; set; }

        public virtual decimal Annual2YearMin { get; set; }

        public virtual decimal Annual2YearMax { get; set; }

        public virtual decimal Annual3YearDefault { get; set; }

        public virtual decimal Annual3YearMin { get; set; }

        public virtual decimal Annual3YearMax { get; set; }
    }
}
