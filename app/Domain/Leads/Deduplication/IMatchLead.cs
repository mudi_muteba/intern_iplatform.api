﻿using iPlatform.Api.DTOs.Leads;

namespace Domain.Leads.Deduplication
{
    internal interface IMatchLead
    {
        Lead Match(SubmitLeadDto leadDto);
    }
}
