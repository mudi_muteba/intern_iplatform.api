﻿using System;
using Common.Logging;
using iPlatform.Api.DTOs.Leads;
using Domain.Leads;
using Domain.Base.Repository;
using System.Linq;
using Domain.Individuals;

namespace Domain.Leads.Deduplication
{
    internal class IDNumberMatching : IMatchLead
    {
        private readonly IMatchLead nextMatchingRule;
        private readonly IRepository repository;
        private static readonly ILog log = LogManager.GetLogger<IDNumberMatching>();

        public IDNumberMatching(IRepository repository, IMatchLead nextMatchingRule = null)
        {
            this.nextMatchingRule = nextMatchingRule ?? new NullLeadMatching();
            this.repository = repository;
        }

        public Lead Match(SubmitLeadDto leadDto)
        {
            if (string.IsNullOrEmpty(leadDto.InsuredInfo.IDNumber))
            {
                log.InfoFormat("Not matching on ID number, ID number not available");
                return nextMatchingRule.Match(leadDto);
            }

            var match = repository.GetAll<Lead>()
                .FirstOrDefault(l => ((Individual)l.Party).IdentityNo == leadDto.InsuredInfo.IDNumber);

            if (match != null)
                return match;

            log.InfoFormat("No leads found with the same ID number.");
            return nextMatchingRule.Match(leadDto);
        }
    }
}
