﻿using Domain.Base.Repository;
using iPlatform.Api.DTOs.Leads;
using Common.Logging;
using System.Linq;

namespace Domain.Leads.Deduplication
{
    internal class ContactNumberMatching : IMatchLead
    {
        private readonly IMatchLead nextMatchingRule;
        private readonly IRepository repository;
        private static readonly ILog log = LogManager.GetLogger<ContactNumberMatching>();

        public ContactNumberMatching(IRepository repository, IMatchLead nextMatchingRule = null)
        {
            this.nextMatchingRule = nextMatchingRule ?? new NullLeadMatching();
            this.repository = repository;
        }

        public Lead Match(SubmitLeadDto leadDto)
        {
            if (!CanMatch(leadDto))
            {
                log.InfoFormat("Not matching on contact number as not enough information is available");
                return nextMatchingRule.Match(leadDto);
            }

            var match = repository
                .GetAll<Lead>()
                .FirstOrDefault(x => x.Party.ContactDetail.Cell == leadDto.InsuredInfo.ContactNumber ||
                                     x.Party.ContactDetail.Work == leadDto.InsuredInfo.ContactNumber ||
                                     x.Party.ContactDetail.Home == leadDto.InsuredInfo.ContactNumber ||
                                     x.Party.ContactDetail.Direct == leadDto.InsuredInfo.ContactNumber);

            if (match != null)
                return match;

            log.InfoFormat("Not lead matching contact number information");
            return nextMatchingRule.Match(leadDto);
        }

        private bool CanMatch(SubmitLeadDto leadDto)
        {
            return !string.IsNullOrEmpty(leadDto.InsuredInfo.ContactNumber);
        }
    }
}
