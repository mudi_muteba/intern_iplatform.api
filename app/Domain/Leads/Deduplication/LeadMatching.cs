﻿using Domain.Base.Repository;
using iPlatform.Api.DTOs.Leads;

namespace Domain.Leads.Deduplication
{
    internal class LeadMatching
    {
        private readonly IMatchLead matchers;

        public LeadMatching(IRepository repository)
        {
            matchers = new IDNumberMatching(repository,
                            new ContactNumberMatching(repository,
                                new EmailAddressMatching(repository)));
        }

        public Lead Match(SubmitLeadDto leadDto)
        {
            return matchers.Match(leadDto);
        }
    }
}
