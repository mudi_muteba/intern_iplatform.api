﻿using System;
using Common.Logging;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Leads;
using System.Linq;
namespace Domain.Leads.Deduplication
{
    internal class EmailAddressMatching : IMatchLead
    {
        private static readonly ILog log = LogManager.GetLogger<EmailAddressMatching>();
        private readonly IMatchLead nextMatchingRule;
        private readonly IRepository repository;

        public EmailAddressMatching(IRepository repository, IMatchLead nextMatchingRule = null)
        {
            this.nextMatchingRule = nextMatchingRule ?? new NullLeadMatching();
            this.repository = repository;
        }

        public Lead Match(SubmitLeadDto leadDto)
        {
            if (string.IsNullOrEmpty(leadDto.InsuredInfo.EmailAddress))
            {
                log.InfoFormat("Not matching on email address, no email address available");
                return nextMatchingRule.Match(leadDto);
            }

            var match = repository
                .GetAll<Lead>()
                .FirstOrDefault(x => x.Party.ContactDetail.Email == leadDto.InsuredInfo.EmailAddress);

            if (match != null)
                return match;

            log.InfoFormat("No match found based on email address");
            return nextMatchingRule.Match(leadDto);
        }
    }
}
