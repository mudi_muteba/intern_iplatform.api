﻿using System;
using iPlatform.Api.DTOs.Leads;
using Common.Logging;

namespace Domain.Leads.Deduplication
{
    internal class NullLeadMatching : IMatchLead
    {
        private static readonly ILog log = LogManager.GetLogger<NullLeadMatching>();

        public Lead Match(SubmitLeadDto leadDto)
        {
            var message = string.Format("No matching rule applied on incoming lead");
            message += string.Format("ID Number: {0}{1}", leadDto.InsuredInfo.IDNumber, Environment.NewLine);
            message += string.Format("Contact Number: {0}{1}", leadDto.InsuredInfo.ContactNumber, Environment.NewLine);
            message += string.Format("Email address: {0}{1}", leadDto.InsuredInfo.EmailAddress, Environment.NewLine);

            log.DebugFormat(message);

            return null;
        }
    }
}
