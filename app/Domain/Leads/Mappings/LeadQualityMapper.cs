﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Party.ContactDetails;
using iPerson.Api.DTOs.PersonInformation;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Quality;
using MasterData;
using System;

namespace Domain.Leads.Mappings
{
    public class LeadQualityMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateLeadQualityDto, LeadQuality>()
                .ForMember(x => x.Id, o => o.Ignore())
                .ForMember(x => x.DateCreated, o => o.MapFrom(s => DateTime.UtcNow));
                ;

            Mapper.CreateMap<EditLeadQualityDto, LeadQuality>()
                .ForMember(x => x.Id, o => o.Ignore())
                .ForMember(x => x.DateCreated, o => o.Ignore())
                ;

            Mapper.CreateMap<LeadQuality, LeadQualityDto>();

            Mapper.CreateMap<Lead, LeadQualityDto>()
                .ForMember(t => t.LeadId, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.Id, o => o.MapFrom(s => s.LeadQuality.Id))
                .ForMember(t => t.DateCreated, o => o.MapFrom(s => s.LeadQuality.DateCreated))
                .ForMember(t => t.WealthIndex, o => o.MapFrom(s => s.LeadQuality.WealthIndex))
                .ForMember(t => t.CreditGradeNonCPA, o => o.MapFrom(s => s.LeadQuality.CreditGradeNonCPA))
                .ForMember(t => t.CreditActiveNonCPA, o => o.MapFrom(s => s.LeadQuality.CreditActiveNonCPA))
                .ForMember(t => t.MosaicCPAGroupMerged, o => o.MapFrom(s => s.LeadQuality.MosaicCPAGroupMerged))
                .ForMember(t => t.DemLSM, o => o.MapFrom(s => s.LeadQuality.DemLSM))
                .ForMember(t => t.FASNonCPAGroupDescriptionShort, o => o.MapFrom(s => s.LeadQuality.FASNonCPAGroupDescriptionShort))
                .ForMember(t => t.IdentityNumber, o => o.Ignore());

            Mapper.CreateMap<PersonDemographicsDto, CreateLeadQualityDto>()
                .ForMember(t => t.MosaicCPAGroupMerged, o => o.MapFrom(s => s.MosaicCPAGroup))
                .ForMember(t => t.DemLSM, o => o.MapFrom(s => s.LSM))
                .ForMember(t => t.FASNonCPAGroupDescriptionShort, o => o.MapFrom(s => s.FASNonCPAGroupDescription))
                ;
            
        }
    }
}