﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Party.ContactDetails;
using iPlatform.Api.DTOs.Leads;
using MasterData;
using System;
using System.Linq;
using Castle.Core.Internal;
using Domain.Activities;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Individual;
using NHibernate.Proxy;

namespace Domain.Leads.Mappings
{
    public class LeadMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Lead, LeadBasicDto>()
                .ForMember(x => x.LeadStatus, o => o.MapFrom(a => a.ActivityStatus))
                .ForMember(x => x.Individual, o => o.Ignore());

            Mapper.CreateMap<Lead, LeadDto>()
                .ForMember(x => x.LeadStatus, o => o.MapFrom(a => a.ActivityStatus))
                .ForMember(t => t.LeadQualityId, o => o.MapFrom(s => s.LeadQuality.Id))
                .ForMember(t => t.CampaignLeadBuckets, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    d.CampaignLeadBuckets.AddRange(s.CampaignLeadBuckets.Select(Mapper.Map<ListCampaignLeadBucketDto>));
                })
                ;

            Mapper.CreateMap<Lead, ImportLeadDto>()
                ;

            Mapper.CreateMap<Lead, ListLeadAdvanceDto>()
                .ForMember(x => x.LeadStatus, o => o.MapFrom(a => a.ActivityStatus))
                .ForMember(x => x.HomeLossHistory, o => o.Ignore())
                .ForMember(x => x.MotorLossHistory, o => o.Ignore())
                .ForMember(x => x.Individual, o => o.Ignore());

            Mapper.CreateMap<Lead, ListLeadSimplifiedDto>()
                .ForMember(x => x.Individual, o => o.Ignore());

            Mapper.CreateMap<HandlerResult<LeadImportResponseDto>, POSTResponseDto<LeadImportResponseDto>>()
                .ForMember(x => x.Response, o => o.MapFrom(s => s.Response));


        }
    }


}