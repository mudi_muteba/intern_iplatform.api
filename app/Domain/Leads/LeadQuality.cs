using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Leads
{
    public class LeadQuality : Entity
    {
        public LeadQuality()
        {
            DateCreated = DateTime.UtcNow;
        }

        public virtual DateTime DateCreated { get; set; }

        public virtual string WealthIndex { get; set; }
        public virtual string CreditGradeNonCPA { get; set; }
        public virtual bool? CreditActiveNonCPA { get; set; }
        public virtual string MosaicCPAGroupMerged { get; set; }
        public virtual string DemLSM { get; set; }
        public virtual string FASNonCPAGroupDescriptionShort { get; set; }

        
    }

}
