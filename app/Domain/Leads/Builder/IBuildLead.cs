﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Leads;

namespace Domain.Leads.Builder
{
    public interface IBuildLead
    {
        void Build(SubmitLeadDto leadDto, Lead lead);
    }
}
