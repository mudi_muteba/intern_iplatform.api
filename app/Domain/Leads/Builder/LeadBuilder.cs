﻿using AutoMapper;
using Domain.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Individual;
using Domain.Individuals;
using NHibernate.Proxy;
using iPlatform.Api.DTOs.Leads;
using Domain.HistoryLoss.Home;
using Domain.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using Domain.HistoryLoss.Motor;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using Domain.Party.Relationships;
using iPlatform.Api.DTOs.Party.Contacts;
using Domain.Party.Contacts;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;

namespace Domain.Leads.Builder
{
    public static class LeadBuilder
    {
        public static ListLeadAdvanceDto Build(this Lead entity, IRepository repository)
        {
            var model = Mapper.Map<ListLeadAdvanceDto>(entity);

            if (entity != null)
            {
                var individual = entity.Party as INHibernateProxy;

                model.Individual = Mapper.Map<IndividualDto>(individual != null
                    ? individual.HibernateLazyInitializer.GetImplementation()
                    : entity.Party);

                model.HomeLossHistory =
                    repository.GetByPartyId<HomeLossHistory>(model.Individual.PartyId).Select(e => e.Build()).ToList();
                model.MotorLossHistory =
                    repository.GetByPartyId<MotorLossHistory>(model.Individual.PartyId).Select(e => e.Build()).ToList();
                model.LossHistoryQuestions =
                    repository.GetByPartyId<LossHistory>(model.Individual.PartyId).Select(e => e.Build()).FirstOrDefault();

                model.CorrespondencePreferences =
                    repository.GetByPartyId<PartyCorrespondencePreference>(model.Individual.PartyId)
                        .Select(e => e.Build()).ToList();

                model.Individual.Contacts =
                    repository.GetByPartyId<Relationship>(model.Individual.PartyId).Select(e => e.Build()).ToList();



            }
            return model;
        }

        public static ListLeadSimplifiedDto BuildSimplifiedLead(this Lead entity, IRepository repository)
        {
            var model = Mapper.Map<ListLeadSimplifiedDto>(entity);

            if (entity != null)
            {
                var individual = entity.Party as INHibernateProxy;
                model.Individual = Mapper.Map<IndividualSimplifiedDto>(individual != null ? individual.HibernateLazyInitializer.GetImplementation() : entity.Party);
            }

            return model;
        }


        private static ListHomeLossHistoryDto Build(this HomeLossHistory entity)
        {
            return Mapper.Map<ListHomeLossHistoryDto>(entity);
        }

        private static ListMotorLossHistoryDto Build(this MotorLossHistory entity)
        {
            return Mapper.Map<ListMotorLossHistoryDto>(entity);
        }

        private static LossHistoryDto Build(this LossHistory entity)
        {
            return Mapper.Map<LossHistoryDto>(entity);
        }

        private static PartyCorrespondencePreferenceDto Build(this PartyCorrespondencePreference entity)
        {
            return Mapper.Map<PartyCorrespondencePreferenceDto>(entity);
        }

        private static ContactDto Build(this Relationship entity)
        {
            var contact = entity.ChildParty as Contact;

            if (contact == null)
                return null;

            var contactDto =  Mapper.Map<ContactDto>(contact);
            contactDto.RelationshipType = entity.RelationshipType;
            return contactDto;
        }


        public static LeadBasicDto Build(this Lead entity)
        {
            if (entity == null)
                return null;

            var model = Mapper.Map<LeadBasicDto>(entity);

            var individual = entity.Party as INHibernateProxy;

            model.Individual = Mapper.Map<IndividualDto>(individual != null
                ? individual.HibernateLazyInitializer.GetImplementation()
                : entity.Party);
            return model;
        }
    }
}
