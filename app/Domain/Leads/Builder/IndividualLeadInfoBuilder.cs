﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Party.ContactDetails;
using Domain.Users;
using iPlatform.Api.DTOs.Domain;
using iPlatform.Api.DTOs.Leads;
using MasterData;

namespace Domain.Leads.Builder
{
    class IndividualLeadInfoBuilder : IBuildLead
    {
        private readonly IRepository _repository;
        public IndividualLeadInfoBuilder(IRepository repository)
        {
            _repository = repository;
        }

        public void Build(SubmitLeadDto leadDto, Lead lead)
        {
            //Get Channel
            lead.Party.Channel = GetUserChannel(leadDto);
            
            lead.Party.ContactDetail = new ContactDetail
            {
                Cell = string.IsNullOrEmpty(leadDto.InsuredInfo.ContactNumber) ? null : leadDto.InsuredInfo.ContactNumber,
                Email = string.IsNullOrEmpty(leadDto.InsuredInfo.EmailAddress) ? null : leadDto.InsuredInfo.EmailAddress
            };

            var individual = (lead.Party as Individual);

            lead.Party.DateCreated = DateTime.UtcNow;

            individual.Title = leadDto.InsuredInfo.Title ?? Titles.Mr;

            individual.FirstName = string.IsNullOrEmpty(leadDto.InsuredInfo.Firstname)
                                            ? FallbackFirstName(leadDto)
                                            : leadDto.InsuredInfo.Firstname;

            individual.Surname = leadDto.InsuredInfo.Surname ?? string.Empty;
            individual.MaritalStatus = leadDto.InsuredInfo.MaritalStatus ?? MaritalStatuses.Unknown;
            individual.Language = Languages.English;
            individual.IdentityNo = leadDto.InsuredInfo.IDNumber ?? string.Empty;

            var idNumber = new IdentityNumberInfo(leadDto.InsuredInfo.IDNumber);
            if (idNumber.IsValid)
            {
                individual.IdentityNo = idNumber.IdentityNumber;
                individual.Gender = idNumber.Gender;
                individual.DateOfBirth = idNumber.BirthDate;
            }
        }

        private string FallbackFirstName(SubmitLeadDto leadDto)
        {
            if (!string.IsNullOrEmpty(leadDto.InsuredInfo.ContactNumber))
                return leadDto.InsuredInfo.ContactNumber;

            if (!string.IsNullOrEmpty(leadDto.InsuredInfo.EmailAddress))
                return leadDto.InsuredInfo.EmailAddress;

            return string.Empty;
        }

        private Channel GetUserChannel(SubmitLeadDto leadDto)
        {
            var channels = _repository.GetAll<UserChannel>().Where(x => x.User.Id == Convert.ToInt32(leadDto.ExternalSystemInfo.CallCentreUserId));
            if(!channels.Any())
                channels = _repository.GetAll<UserChannel>().Where(x => x.User.Id == leadDto.Context.UserId);

            if (channels == null)
                throw new NullReferenceException("no channel found");

            var userChannel = channels.FirstOrDefault(x => x.Channel.IsDefault);
            
            return userChannel != null ? userChannel.Channel : channels.FirstOrDefault().Channel;
        }
    }
}
