﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Individuals;
using Domain.Users;
using iPlatform.Api.DTOs.Leads;

namespace Domain.Leads.Builder
{
    public class LeadFromDtoBuilder
    {
        private static readonly ILog log = LogManager.GetLogger<LeadFromDtoBuilder>();
        private readonly List<IBuildLead> builders = new List<IBuildLead>();

        public LeadFromDtoBuilder(IWindsorContainer container, IRepository repository)
        {
            builders.Add(new IndividualLeadInfoBuilder(repository));
            builders.Add(new LeadAddressBuilder(container, repository));
        }

        public Lead Build(SubmitLeadDto leadDto, Campaign campaign)
        {
            log.InfoFormat("Creating a new lead");

            var lead = new Lead
            {
                Party = new Individual()
            };

            foreach (var builder in builders)
            {
                builder.Build(leadDto, lead);
            }

            lead.CreatedLeadActivity(new User(1), campaign: campaign); // root user import

            return lead;
        }
    }
}
