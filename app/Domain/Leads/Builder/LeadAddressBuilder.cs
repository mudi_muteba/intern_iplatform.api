﻿using iPlatform.Api.DTOs.Leads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Domain.Base.Repository;
using Domain.Lookups.Addresses;
using Domain.Party.Addresses;
using iPlatform.Api.DTOs.Party.Address;
using MasterData;

namespace Domain.Leads.Builder
{
    public class LeadAddressBuilder: IBuildLead
    {
        public LeadAddressBuilder(IWindsorContainer container, IRepository repository)
        {
            _container = container;
            _repository = repository;
        }

        private readonly IWindsorContainer _container;
        private readonly IRepository _repository;

        public void Build(SubmitLeadDto leadDto, Lead lead)
        {
            if (!CanBuildAddress(leadDto))
                return;

            var addressDto = leadDto.Request.Policy.Persons.First().Addresses.First();
            var address = new CreateAddressDto()
            {
                StateProvince = new ToStateProvinceMapperFactory(_container, _repository).Create("ZA").GetStateProvinceByPostCode(addressDto.PostalCode),
                IsDefault = addressDto.DefaultAddress,
                Line1 = addressDto.Address1 ?? string.Empty,
                Line2 = addressDto.Address2 ?? string.Empty,
                Line3 = addressDto.City ?? addressDto.City ?? string.Empty,
                Line4 = addressDto.Suburb ?? addressDto.Address3 ?? string.Empty,
                Code = addressDto.PostalCode ?? string.Empty,
                AddressType = addressDto.AddressType ?? AddressTypes.PhysicalAddress,
                Latitude = "0",
                Longitude = "0",
            };
            address.SetContext(leadDto.Context);
            lead.Party.AddAddress(address);
        }

        private bool CanBuildAddress(SubmitLeadDto leadDto)
        {
            if (leadDto.Request == null)
                return false;

            if (leadDto.Request.Policy == null)
                return false;

            if (!leadDto.Request.Policy.Persons.Any())
                return false;

            if (!leadDto.Request.Policy.Persons.First().Addresses.Any())
                return false;

            return true;
        }
    }
}
