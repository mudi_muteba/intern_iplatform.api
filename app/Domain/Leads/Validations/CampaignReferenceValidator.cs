﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Leads;
using Domain.Campaigns.Queries;
using System.Linq;
using Domain.Campaigns;
using ValidationMessages.Campaigns;

namespace Domain.Leads.Validations
{
    public class CampaignReferenceValidator : IValidateDto<SubmitLeadDto>
    {

        private readonly GetCampaignByReferenceQuery query;

        public CampaignReferenceValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetCampaignByReferenceQuery(contextProvider, repository);
        }

        public void Validate(SubmitLeadDto dto, ExecutionResult result)
        {
            ValidateCampaigns(dto.CampaignReference, result);
        }

        private void ValidateCampaigns(string reference, ExecutionResult result)
        {

            var queryResult = GetCampaignByReference(reference);

            if (queryResult.Any())
                result.AddValidationMessage(CampaignValidationMessages.UnknownCampaignReference.AddParameters(new[] { reference }));
        }


        private IQueryable<CampaignReference> GetCampaignByReference(string reference)
        {
            return query.WithReference(reference).ExecuteQuery();
        }

    }
}
