using AutoMapper;
using Domain.Activities;
using Domain.Base;
using Domain.Campaigns;
using Domain.Party.ProposalHeaders;
using Domain.Party.Quotes;
using Domain.Users;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Internal;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Quality;
using Domain.Individuals;
using Domain.Admin;
using Domain.Party.CustomerSatisfactionSurveys;
using Infrastructure.NHibernate.Attributes;
using Domain.Imports.Leads.Dtos;
using Domain.Imports.Leads.Events;
using Domain.Base.Extentions;

namespace Domain.Leads
{
    public class Lead : EntityWithAudit, IEntityIncludeParty
    {
        public Lead()
        {
            CampaignLeadBuckets = new List<CampaignLeadBucket>();
            Activities = new List<LeadActivity>();
            LeadStatus = LeadStatuses.Created;
            IsDeleted = false;
        }


        public Lead(Party.Party party)
        {
            CampaignLeadBuckets = new List<CampaignLeadBucket>();
            Activities = new List<LeadActivity>();
            LeadStatus = LeadStatuses.Created;
            IsDeleted = false;
            this.Party = party;
        }
        public Lead(int id)
            : this()
        {
            Id = id;
            LeadStatus = LeadStatuses.Created;
            CampaignLeadBuckets = new List<CampaignLeadBucket>();
            Activities = new List<LeadActivity>();
        }

        public virtual Party.Party Party { get; set; }
        public virtual IList<LeadActivity> Activities { get; set; }
        public virtual LeadQuality LeadQuality { get; set; }
        public virtual string ExternalReference { get; set; }
        // This is not required in the new API: the status is deermined by finding the last activity in the ActivityStatus property
        public virtual LeadStatus LeadStatus { get; protected internal set; }
        public virtual IList<CampaignLeadBucket> CampaignLeadBuckets { get; set; }


        [DoNotMap]
        public virtual ActivityType ActivityStatus
        {
            get
            {
                var activity = Activities.OrderBy(a => a.DateCreated).LastOrDefault();
                if (activity != null) return activity.ActivityType;

                return null;
            }
        }

        public static Lead Create(CreateLeadDto dto)
        {
            var lead = Mapper.Map<Lead>(dto);
            return lead;
        }





        public virtual void SaveQuality(CreateLeadQualityDto dto)
        {
            this.LeadQuality = Mapper.Map<LeadQuality>(dto);
        }
        public virtual void EditQuality(EditLeadQualityDto dto)
        {
            Mapper.Map(dto, this.LeadQuality);
        }

        public virtual void AddLeadActivity(LeadActivity leadActivity)
        {
            leadActivity.Lead = this;
            Activities.Add(leadActivity);
        }

        protected bool Equals(Lead other)
        {
            return base.Equals(other) && Equals(Party, other.Party) && Equals(Activities, other.Activities) &&
                   Equals(LeadQuality, other.LeadQuality);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Lead)obj);
        }


        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ (Party != null ? Party.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Activities != null ? Activities.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LeadQuality != null ? LeadQuality.GetHashCode() : 0);
                return hashCode;
            }
        }

        public virtual void Loss(LossLeadActivityDto dto)
        {
            var activity = new LossLeadActivity(FindUser(dto.Context.UserId), Party);
            activity.AllocateToCampaign(new Campaign { Id = dto.CampaignId }, string.Empty);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;

            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
        }

        public virtual void Dead(DeadLeadActivityDto dto)
        {
            var activity = new DeadLeadActivity(FindUser(dto.Context.UserId), Party);
            activity.AllocateToCampaign(new Campaign { Id = dto.CampaignId }, string.Empty);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;

            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
        }


        public virtual void Created(int userId, Campaign campaign = null)
        {
            CreatedLeadActivity(FindUser(userId), campaign);
        }

        public virtual void Delay(DelayLeadActivityDto dto)
        {
            var activity = new DelayLeadActivity(FindUser(dto.Context.UserId), Party);
            activity.AllocateToCampaign(new Campaign { Id = dto.CampaignId }, string.Empty);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;


            RaiseEvent(new UpdateIndividualUploadDetailEvent(new IndividualUploadDetailEventDto()
            {
                LeadImportStatus = new LeadImportStatuses().FirstOrDefault(a => a.Name.ToLower().Equals("delay")),
                LeadImportReference = activity.Lead.ExternalReference.ToGuid(),
                Comment = dto.Comment,
                PartyId = Party.Id
            }));
        }

        public virtual void Imported(ImportedLeadActivityDto dto)
        {
            //update lead created leadActivity created date
            UpdateCreatedLeadActivity();
            var activity = new ImportedLeadActivity(FindUser(dto.Context.UserId), Party, null, dto.ImportType, dto.Existing);
            activity.AllocateToCampaign(new Campaign { Id = dto.CampaignId }, string.Empty);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;
            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
        }

        private void UpdateCreatedLeadActivity()
        {
            var activity = Activities.FirstOrDefault(x => x.ActivityType == ActivityTypes.Created);
            if(activity != null)
            {
                activity.DateCreated = DateTime.UtcNow;
            }
        }


        public virtual QuotedLeadActivity Quoted(User user, QuoteHeader quote, Campaign campaign = null)
        {
            var activity = new QuotedLeadActivity(user, Party, quote);
            activity.AllocateToCampaign(campaign, string.Empty);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;

            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
            return activity;
        }

        public virtual CreateLeadActivity CreatedLeadActivity(User user, Campaign campaign = null)
        {
            //Check if LeadActivity exists already
            LeadActivity createLeadActivity = campaign == null 
                ? Activities.FirstOrDefault(p => p.GetType() == typeof(CreateLeadActivity))
                : Activities.FirstOrDefault(p => p.GetType() == typeof(CreateLeadActivity) && p.Campaign.Id == campaign.Id);

            if (createLeadActivity != null)
                return createLeadActivity as CreateLeadActivity;

            var activity = new CreateLeadActivity(user, Party, campaign);
            activity.AllocateToCampaign(campaign);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;


            RaiseEvent(new UpdateIndividualUploadDetailEvent(new IndividualUploadDetailEventDto()
            {
                LeadImportStatus = new LeadImportStatuses().FirstOrDefault(a => a.Name.ToLower().Equals(activity.Lead.LeadStatus.Name.ToLower())),
                LeadImportReference = activity.Lead.ExternalReference.ToGuid(),
                Comment = "CreatedLeadActivity",
                PartyId = Party.Id
            }));
            return activity;
        }

        public virtual ProposalLeadActivity ProposalLeadActivity(User User, ProposalHeader proposal,
            Campaign campaign = null)
        {
            var activity = new ProposalLeadActivity(User, Party, proposal, campaign);
            activity.AllocateToCampaign(campaign);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;
            if (activity.Lead != null && Party != null)
            {
                RaiseEvent(new UpdateIndividualUploadDetailEvent(new IndividualUploadDetailEventDto()
                {
                    LeadImportStatus = new LeadImportStatuses().FirstOrDefault(a => a.Name.ToLower().Equals("proposal")),
                    LeadImportReference = activity.Lead.ExternalReference.ToGuid(),
                    Comment = "Proposal",
                    PartyId = Party.Id
                }));
            }
            return activity;
        }

        public virtual QuoteAcceptedLeadActivity QuoteAccepted(int quoteId, int UserId, Campaign campaign = null)
        {
            var activity = new QuoteAcceptedLeadActivity(FindUser(UserId), Party) { Quote = new Quote { Id = quoteId } };
            activity.AllocateToCampaign(campaign);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;

            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
            return activity;
        }

        public virtual void PolicyBindingCreated(PolicyBindingCreatedLeadActivityDto dto)
        {
            var activity = new PolicyBindingCreatedLeadActivity(FindUser(dto.UserId), Party, new Quote { Id = dto.QuoteId});
            activity.AllocateToCampaign(new Campaign { Id = dto.CampaignId }, string.Empty);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;

            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
        }

        public virtual PolicyBindingCompletedLeadActivity PolicyBindingCompleted(PolicyBindingCompletedLeadActivityDto dto, Quote quote)
        {
            var activity = new PolicyBindingCompletedLeadActivity(FindUser(dto.UserId), Party, quote, dto.RequestId, dto.Comments );
            activity.AllocateToCampaign(new Campaign { Id = dto.CampaignId }, string.Empty);
            AddLeadActivity(activity);
            LeadStatus = activity.GetImpliedStatus;

            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
            return activity;
        }

        private User FindUser(int? userid = null)
        {
            if (userid != null)
            {
                var user = Mapper.Map<int, User>((int)userid);

                if (user != null)
                    return user;
            }

            LeadActivity leaduser = null;

            if (Activities.Any())
                leaduser = Activities.FirstOrDefault(p => p.GetType() == typeof(CreateLeadActivity));

            if (leaduser != null)
                return leaduser.User;

            return null;
        }

        public virtual void Imported(Campaign campaign)
        {
            var activity = new ImportedLeadActivity(FindUser(), Party, campaign);

            AddLeadActivity(activity);
        }

        public virtual IEnumerable<QuotedLeadActivity> GetQuotes()
        {
            IEnumerable<QuotedLeadActivity> quoteActivities =
                Activities.Where(a => a is QuotedLeadActivity).Cast<QuotedLeadActivity>();

            return quoteActivities.Where(q => q.QuoteHeader.IsDeleted != true);
        }

        public virtual QuoteIntentToBuyLeadActivity QuoteIntentToBuy(int quoteId, Campaign campaign = null)
        {
            var activity = new QuoteIntentToBuyLeadActivity(FindUser(), Party) { Quote = new Quote { Id = quoteId }, Campaign = campaign };
            this.AddLeadActivity(activity);
            this.LeadStatus = activity.GetImpliedStatus;
            return activity;
        }

        public virtual SurveyRespondedLeadActivity SurveyResponded(int userId, CustomerSatisfactionSurvey survey, Campaign campaign = null)
        {
            var activity = new SurveyRespondedLeadActivity(new User(userId), Party, survey);
            this.AddLeadActivity(activity);
            this.LeadStatus = activity.GetImpliedStatus;
            return activity;
        }

        public virtual SentForUnderwritingLeadActivity SentForUnderwriting(int userId, Quote quote, Campaign campaign = null)
        {
            var activity = new SentForUnderwritingLeadActivity(new User(userId), Party, campaign, quote);
            this.AddLeadActivity(activity);
            this.LeadStatus = activity.GetImpliedStatus;

            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
            return activity;
        }


        public virtual RejectedAtUnderwritingLeadActivity RejectedAtUnderwriting(int userId, Quote quote, Campaign campaign = null)
        {
            var activity = new RejectedAtUnderwritingLeadActivity(new User(userId), Party, quote, campaign);
            this.AddLeadActivity(activity);
            this.LeadStatus = activity.GetImpliedStatus;

            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
            return activity;
        }

        public virtual SoldLeadActivity Sold(User user, Quote quote, Campaign campaign = null)
        {
            var activity = new SoldLeadActivity(user, Party, campaign, quote);
            this.AddLeadActivity(activity);
            this.LeadStatus = activity.GetImpliedStatus;

            ActivityEvents(activity.Lead.LeadStatus.Name.ToLower(), activity.Lead.ExternalReference.ToGuid());
            return activity;
        }

        public override void Delete()
        {
            Activities.ForEach(x => { x.Delete(); });
            Party.Delete();
            base.Delete();
        }

        protected virtual void ActivityEvents(string status,Guid externalRef)
        {
            RaiseEvent(new UpdateIndividualUploadDetailEvent(new IndividualUploadDetailEventDto()
            {
                LeadImportStatus = new LeadImportStatuses().FirstOrDefault(a => a.Name.ToLower().Equals(status)),
                LeadImportReference = externalRef,
                Comment = status,
                PartyId = Party.Id
            }));
        }
    }
}