﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Leads;

namespace Domain.Leads.Queries
{
    public class GetLeadDetailByIdQuery : BaseQuery<Lead>, ISearchQuery<GetLeadDetailedDto>
    {
        private GetLeadDetailedDto criteria;

        public GetLeadDetailByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Lead>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    LeadAuthorisation.List,
                };
            }
        }
        public void WithCriteria(GetLeadDetailedDto criteria)
        {
            this.criteria = criteria;
        }



        protected internal override IQueryable<Lead> Execute(IQueryable<Lead> query)
        {
            return query
                .Where(c => c.Id == criteria.Id);
        }
    }
}