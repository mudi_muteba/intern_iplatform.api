﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Leads;

namespace Domain.Leads.Queries
{
    public class GetLeadDetailByPartyIdQuery : BaseQuery<Lead>
    {

        public GetLeadDetailByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Lead>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    LeadAuthorisation.List,
                };
            }
        }
        public int partyId;
        public void WithPartyId(int partyId)
        {
            this.partyId = partyId;
        }



        protected internal override IQueryable<Lead> Execute(IQueryable<Lead> query)
        {
            return query
                .Where(c => c.Party.Id == partyId);
        }
    }
}