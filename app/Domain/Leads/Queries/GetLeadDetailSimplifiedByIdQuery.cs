﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Leads;
using MasterData.Authorisation;

namespace Domain.Leads.Queries
{
    public class GetLeadDetailSimplifiedByIdQuery : BaseQuery<Lead>, ISearchQuery<GetLeadDetailedDto>
    {
        private GetLeadDetailedDto criteria;

        public GetLeadDetailSimplifiedByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Lead>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    LeadAuthorisation.List,
                };
            }
        }
        public void WithCriteria(GetLeadDetailedDto criteria)
        {
            this.criteria = criteria;
        }



        protected internal override IQueryable<Lead> Execute(IQueryable<Lead> query)
        {
            return query
                .Where(c => c.Id == criteria.Id);
        }
    }
}
