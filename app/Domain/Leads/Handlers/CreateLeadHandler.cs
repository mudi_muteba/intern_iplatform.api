using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Leads;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Activities;
using AutoMapper;
using Domain.Admin;
using Domain.Campaigns;

namespace Domain.Leads.Handlers
{
    public class CreateLeadHandler : CreationDtoHandler<Lead, CreateLeadDto, int>
    {
        public CreateLeadHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; } 
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void EntitySaved(Lead entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Lead HandleCreation(CreateLeadDto dto, HandlerResult<int> result)
        {

            var lead = Lead.Create(dto);
            lead.Created(dto.Context.UserId, new Campaign{Id = dto.CampaignId});

            result.Processed(lead.Id);

            return lead;
        }
    }
}