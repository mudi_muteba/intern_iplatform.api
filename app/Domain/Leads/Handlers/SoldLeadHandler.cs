using System;
using System.Collections.Generic;
using Domain.AuditLeadlog.Events;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Policies;
using Domain.Party.Quotes;
using Domain.Users;
using MasterData;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.AuditLeadLog;
using Infrastructure.Configuration;
using Domain.Admin;

namespace Domain.Leads.Handlers
{
    public class SoldLeadHandler : ExistingEntityDtoHandler<Lead, SoldLeadActivityDto, int>
    {
        private readonly IRepository _repository;
        private readonly IProvideContext _contextProvider;

        public SoldLeadHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights        
		{
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void HandleExistingEntityChange(Lead entity, SoldLeadActivityDto dto, HandlerResult<int> result)
        {
            var policyHeader = _repository.GetById<PolicyHeader>(dto.PolicyHeaderId);

            if (policyHeader.LeadActivity.ActivityType == ActivityTypes.Sold)
            {
                result.Processed(0);
                return;
            }

            var quote = _repository.GetById<Quote>(policyHeader.QuoteId);
            var campaign = _repository.GetById<Campaign>(dto.CampaignId);
            var user = _repository.GetById<User>(dto.Context.UserId);
            var activity = entity.Sold(user, quote, campaign);
            var channel =_repository.GetById<Channel>(campaign.Channel.Id);

            RaiseEditAuditLeadLogEvent(entity, quote, channel.SystemId);
            policyHeader.SetActivity(activity);
           
            result.Processed(entity.Id);
        }

        private void RaiseEditAuditLeadLogEvent(Lead entity, Quote quote, Guid SystemId)
        {
            EditAuditLeadLogDto editAuditLeadLogDto = new EditAuditLeadLogDto()
            {
                LeadId = quote.QuoteHeader.Proposal.LeadActivity.Lead.Id,
                Timestamp = DateTime.UtcNow,
                IsAccepted = quote.IsAccepted,
                ProductId = quote.Product.Id,
                ProductName = quote.Product.Name,
                ProductCode = quote.Product.ProductCode,
                IsSold = true,
                SystemId = SystemId,
                EnvironmentType = new AdminConfigurationReader().EnvironmentTypeKey
            };
            entity.RaiseEvent(new EditAuditLeadLogEvent(editAuditLeadLogDto));
        }
    }
}