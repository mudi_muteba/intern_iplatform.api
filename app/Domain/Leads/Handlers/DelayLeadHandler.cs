using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Leads;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Activities;
using AutoMapper;
using MasterData;

namespace Domain.Leads.Handlers
{
    public class DelayLeadHandler : ExistingEntityDtoHandler<Lead, DelayLeadActivityDto, int>
    {
        private readonly IRepository _repository;

        public DelayLeadHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights        
		{
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void HandleExistingEntityChange(Lead entity, DelayLeadActivityDto dto, HandlerResult<int> result)
        {
            entity.Delay(dto);
            result.Processed(entity.Id);
        }
    }
}