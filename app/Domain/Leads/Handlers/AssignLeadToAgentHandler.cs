using System.Collections.Generic;
using System.Linq;

using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;

using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Leads.Campaign;

using MasterData.Authorisation;
using Domain.Users;
using Domain.Individuals;

namespace Domain.Leads.Handlers
{
    public class AssignLeadToAgentHandler : BaseDtoHandler<AssignLeadToAgentDto, NextLeadResponseDto>
    {
        private readonly IRepository m_Repository;
        private readonly IProvideContext m_ContextProvider;

        public AssignLeadToAgentHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            m_Repository = repository;
            m_ContextProvider = contextProvider;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void InternalHandle(AssignLeadToAgentDto dto, HandlerResult<NextLeadResponseDto> result)
        {
            var campaign = m_Repository.GetById<Campaign>(dto.Id);
            var lead = m_Repository.GetById<Lead>(dto.LeadId);
            //Get all leads which are not already on final stage

            var existingLeadBucket = m_Repository.GetAll<CampaignLeadBucket>().Where(x => x.Campaign.Id == campaign.Id && x.Lead.Id == lead.Id).ToList();
            
            if (!existingLeadBucket.Any())
            {
                var bucket = CampaignLeadBucket.Create(dto.Id, lead);
                m_Repository.Save(bucket);
                bucket.SetAsAllocated(dto.Context.UserId, GetEventDetails(bucket, campaign, dto, dto.Context.Username));
                m_Repository.Save(bucket);
            }
            else if (existingLeadBucket.Any() && dto.AgentId != 0)
            {
                var bucket = CampaignLeadBucket.Create(dto.Id, lead);
                m_Repository.Save(bucket);

                bool isAssigned = existingLeadBucket.Any(x => x.Agent != null);
                existingLeadBucket.Where(x => x.Agent != null)
                    .ToList()
                    .ForEach(campaignLeadBucket =>
                    {
                        campaignLeadBucket.Agent = null;
                        m_Repository.Save(campaignLeadBucket);
                    });

                UserIndividual userIndividual = m_Repository.GetAll<UserIndividual>()
                    .FirstOrDefault(ui => ui.Individual.Id == dto.AgentId);
                if (!isAssigned)
                {
                    bucket.SetAsAllocated(userIndividual.User.Id, GetEventDetails(bucket, campaign, dto, userIndividual.User.UserName));
                }
                else
                {
                    bucket.SetAsReallocated(userIndividual.User.Id, GetEventDetails(bucket, campaign, dto, userIndividual.User.UserName));
                }
                m_Repository.Save(bucket);
            }

            result.Processed(NextLeadResponseDto.NextLeads(lead.Id));

            return;
        }

        private CallCentreEventDetailsDto GetEventDetails(CampaignLeadBucket bucket, Campaign campaign, AssignLeadToAgentDto assignLeadToAgentDto, string agentName)
        {
            var dto = new CallCentreEventDetailsDto();
            var channel = m_Repository.GetById<Channel>(bucket.Lead.Party.Channel.Id);

            dto.Details.Add(new CallCentreStatusEventDetailDto("{CAMPAIGN}", campaign.Name));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{AGENT}", agentName));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{LEAD}", bucket.Lead.Party.DisplayName));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{SOURCE}", assignLeadToAgentDto.Context.Username));

            return dto;
        }
    }
}