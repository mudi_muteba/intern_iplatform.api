using System.Collections.Generic;
using System.Linq;
using Domain.Activities;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Users;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Leads.Campaign;
using MasterData;


namespace Domain.Leads.Handlers
{
    public class UpdateLeadStatusHandler : BaseDtoHandler<UpdateLeadStatusDto, bool>
    {
        private readonly IRepository _repository;

        public UpdateLeadStatusHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            _repository = repository;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights        
		{
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void InternalHandle(UpdateLeadStatusDto dto, HandlerResult<bool> result)
        {
            var bucket =
                _repository.GetAll<CampaignLeadBucket>()
                    .FirstOrDefault(x => x.Lead.Id == dto.Id && x.Campaign.Id == dto.CampaignId && !x.Lead.IsDeleted);

            if (bucket == null)
            {
                Log.ErrorFormat("No CampaignLeadBucket exist for LeadId: '{0}' and CampaignId '{1}'", dto.Id.ToString(), dto.CampaignId.ToString());
                result.Processed(false);
                return;
            }
            
            //Update EventTemplate
            var eventDetails = GetEventDetails(bucket, dto);
            //Update Status
            bucket.UpdateStatus(dto, eventDetails);

            _repository.Save(bucket);
            _repository.Save(bucket.Lead);

            result.Processed(true);
        }


        private CallCentreEventDetailsDto GetEventDetails(CampaignLeadBucket bucket, UpdateLeadStatusDto statusDto)
        {
            var dto = new CallCentreEventDetailsDto();
            var channel = _repository.GetById<Channel>(bucket.Lead.Party.Channel.Id);


            var contactNumber = string.IsNullOrEmpty(bucket.Lead.Party.ContactDetail.Cell)
                ? string.IsNullOrEmpty(bucket.Lead.Party.ContactDetail.Direct)
                ? bucket.Lead.Party.ContactDetail.Home
                : bucket.Lead.Party.ContactDetail.Direct
                : bucket.Lead.Party.ContactDetail.Cell;

            var quoteAcceptedLeadActivity =
                _repository.GetAll<QuoteAcceptedLeadActivity>().Where(x => x.Lead.Id == bucket.Lead.Id).OrderByDescending(n => n.Id).FirstOrDefault();
            var insurerName = string.Empty;

            if (quoteAcceptedLeadActivity != null && quoteAcceptedLeadActivity.Quote != null)
            insurerName = quoteAcceptedLeadActivity.Quote.Product.ProductOwner.TradingName;

            dto.Details.Add(new CallCentreStatusEventDetailDto("{CAMPAIGN}", bucket.Campaign.Name));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{AGENT}", statusDto.Context.Username));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{LEAD}", bucket.Lead.Party.DisplayName));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{DATE}", statusDto.CallBackDate.ToString("dd-MM-yyyy hh:mm tt")));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{SOURCE}", channel.Name));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{CONTACTNUMBER}", contactNumber));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{INSURER}", insurerName));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{MAXATTEMPTS}", bucket.Campaign.MaximumLeadCallbackLimit.ToString()));
            
            return dto;
        }
    }
}