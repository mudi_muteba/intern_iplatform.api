using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms.VisualStyles;
using AutoMapper;
using Domain.Activities;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Individuals;
using Domain.Users;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Campaign;
using MasterData;
using MasterData.Authorisation;
using ValidationMessages.Campaigns;


namespace Domain.Leads.Handlers
{
    public class GetNextLeadHandler : BaseDtoHandler<GetNextLeadDto, NextLeadResponseDto>
    {
        private readonly IRepository _repository;

        public GetNextLeadHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            _repository = repository;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights        
		{
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void InternalHandle(GetNextLeadDto dto, HandlerResult<NextLeadResponseDto> result)
        {

            var campaign = _repository.GetById<Campaign>(dto.Id);
            //Get all leads which are not already on final stage

            var userIndividual = _repository.GetAll<UserIndividual>();

            var buckets = _repository.GetAll<CampaignLeadBucket>().Where(x => x.Campaign.Id == dto.Id 
                    && !x.Lead.IsDeleted
                    && !x.Lead.Party.IsDeleted
                    && userIndividual.All(f => f.Individual.Id != x.Lead.Party.Id)
                    ).ToList();
            

            var bucket = buckets.Where(x => x.LeadCallCentreCode.IsFinalStatus == false);

            if (!bucket.Any())
            {
                LogNoLeadinBucket(result);
                return;
            }
          
            //check fifo or lifo
            bucket = campaign.UseLastInFirstOutCallScheme ? bucket.OrderBy(x => x.CreatedOn) : bucket.OrderByDescending(x => x.CreatedOn);

            //Assigned to this Agent
            var agentBucket = bucket.Where(x => x.Agent != null  && x.Agent.Id == dto.Context.UserId).OrderBy(x => x.CallbackDate).ToList();
           
            if (agentBucket.Any())
            {
                if (agentBucket.Count() >= campaign.MaximumLeadsPerAgent)
                {
                    LogMaximumLeadsExceeded(result);
                    return;
                }

                if (GetAgentNextLead(agentBucket, result))
                return;
            }

            //Get Unallocated Leads
            var leads = bucket.Where(x => x.LeadCallCentreCode == LeadCallCentreCodes.LeadNotAllocated);
            
            if (!leads.Any())
            {
                LogNoLeadinBucket(result);
                return;
            }

            var userParty = userIndividual.FirstOrDefault(x => x.User.Id == dto.Context.UserId);

            //no individual related to User, thus user is not an agent on campaign
            if(userParty == null)
            {
                //return first lead from bucket without assigning
                result.Processed(NextLeadResponseDto.NextLeads(leads.First().Lead.Id));
                return;
            }

            //if individual exist veify if user is a campaign agent
            if (campaign.Agents.All(x => x.Party.Id != userParty.Individual.Id))
            {
                //return first lead from bucket without assigning
                result.Processed(NextLeadResponseDto.NextLeads(leads.First().Lead.Id));
                return;
            }

            foreach (var lead in leads)
            {
                //verifying if lead has been in similar campaign && callbackhours are correct
                if (!CheckSimilarCampaign(lead) && CheckCallbackHours(lead)) continue;

                lead.SetAsAllocated(dto.Context.UserId, GetEventDetails(lead, dto));
                _repository.Save(lead);
                result.Processed(NextLeadResponseDto.NextLeads(lead.Lead.Id));
                return;
            }
        }

        private bool GetAgentNextLead(IEnumerable<CampaignLeadBucket> buckets, HandlerResult<NextLeadResponseDto> result)
        {
            var response = false;
            var date = DateTime.UtcNow;
            foreach (var bucket in buckets)
            {
                //validate Callback date
                if (bucket.CallbackDate > date) continue;
                result.Processed(NextLeadResponseDto.NextLeads(bucket.Lead.Id));
                response = true;
                break;
            }
            return response;
        }
        private CallCentreEventDetailsDto GetEventDetails(CampaignLeadBucket bucket, GetNextLeadDto getNextLeadDto)
        {
            var dto = new CallCentreEventDetailsDto();
            var channel = _repository.GetById<Channel>(bucket.Lead.Party.Channel.Id);

            dto.Details.Add(new CallCentreStatusEventDetailDto("{CAMPAIGN}", bucket.Campaign.Name));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{AGENT}", getNextLeadDto.Context.Username));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{LEAD}", bucket.Lead.Party.DisplayName));
            dto.Details.Add(new CallCentreStatusEventDetailDto("{SOURCE}", channel.Name));


            return dto;
        }
        private bool CheckSimilarCampaign(CampaignLeadBucket bucket)
        {
            if (bucket.Campaign.AllowSimilarCampaignLeadCallbacks) return true;

            return _repository.GetAll<CampaignSimilarCampaign>().Any(x => x.Campaign.Id == bucket.Campaign.Id);
        }
        private bool CheckCallbackHours(CampaignLeadBucket bucket)
        {
            if (bucket.Campaign.AllowLeadCallbacksOutsideCampaignHours) return true;

            var campaignHoursTo = bucket.Campaign.CampaignHoursTo.Contains(':')
                ? GetTimeInteger(bucket.Campaign.CampaignHoursTo)
                : Convert.ToDecimal(bucket.Campaign.CampaignHoursTo);

            var campaignHoursFrom = bucket.Campaign.CampaignHoursFrom.Contains(':')
                ? GetTimeInteger(bucket.Campaign.CampaignHoursFrom)
                : Convert.ToDecimal(bucket.Campaign.CampaignHoursFrom);

            var nowTime = GetTimeInteger(DateTime.UtcNow.ToString("HH:mm"));

            return nowTime >= campaignHoursTo && nowTime <= campaignHoursFrom;
        }

        private void LogNoLeadinBucket(HandlerResult<NextLeadResponseDto> result)
        {
            result.Processed(NextLeadResponseDto.NoLeads());
            result.AllErrorDTOMessages.Add(Mapper.Map<ResponseErrorMessage>(result.Response.Message));
            Log.ErrorFormat(result.Response.Message.DefaultMessage);
        }

        private void LogMaximumLeadsExceeded(HandlerResult<NextLeadResponseDto> result)
        {
            result.Processed(NextLeadResponseDto.MaximumLeadsExceeded());
            result.AllErrorDTOMessages.Add(Mapper.Map<ResponseErrorMessage>(result.Response.Message));
            Log.ErrorFormat(result.Response.Message.DefaultMessage);
        }

        private static decimal GetTimeInteger(string time)
        {
            var splitted = time.Split(':');
            var hour = splitted[0];
            var min = splitted[1];

            return Convert.ToDecimal(hour) + (Convert.ToDecimal(min) / 60);
        }
    }
}