﻿using Common.Logging;
using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Leads;
using System.Collections.Generic;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Leads.Extensions;
using iPlatform.Api.DTOs.Base;
using MasterData.Authorisation;

namespace Domain.Leads.Handlers
{
    public class ImportLeadHandler : BaseDtoHandler<SubmitLeadDto, LeadImportResponseDto>
    {
        private readonly ILog _log = LogManager.GetLogger<ImportLeadHandler>();
        private readonly IRepository _repository;
        private readonly IWindsorContainer _container;
        public ImportLeadHandler(IProvideContext contextProvider, IRepository repository, IWindsorContainer container)
            : base(contextProvider)
        {
            _repository = repository;
            _container = container;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.Import
                };
            }
        }

        protected override void InternalHandle(SubmitLeadDto dto, HandlerResult<LeadImportResponseDto> result)
        {
            var response = dto.ImportLead(_repository, _container);
            result.Processed(response);
        }
    }
}
