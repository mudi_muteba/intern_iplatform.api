using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Leads;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Activities;
using AutoMapper;
using Domain.Party.Quotes;
using MasterData;

namespace Domain.Leads.Handlers
{
    public class QuoteAcceptedLeadHandler : ExistingEntityDtoHandler<Lead, CreateQuoteAcceptedLeadActivityDto, int>
    {
        private readonly IRepository _repository;

        public QuoteAcceptedLeadHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights        
		{
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void HandleExistingEntityChange(Lead entity, CreateQuoteAcceptedLeadActivityDto dto, HandlerResult<int> result)
        {

            var activity = entity.QuoteAccepted(dto.QuoteId, dto.Context.UserId);
            entity.LeadStatus = activity.GetImpliedStatus;
            result.Processed(entity.Id);
        }
    }
}