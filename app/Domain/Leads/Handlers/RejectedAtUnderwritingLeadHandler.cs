using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Leads;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Activities;
using AutoMapper;
using MasterData;
using Domain.Campaigns;
using Domain.Party.ProposalHeaders;
using Domain.Policies;
using Domain.Party.Quotes;

namespace Domain.Leads.Handlers
{
    public class RejectedAtUnderwritingLeadHandler : ExistingEntityDtoHandler<Lead, RejectedAtUnderwritingLeadActivityDto, int>
    {
        private readonly IRepository _repository;

        public RejectedAtUnderwritingLeadHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights        
		{
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void HandleExistingEntityChange(Lead entity, RejectedAtUnderwritingLeadActivityDto dto, HandlerResult<int> result)
        {
            var policyHeader = _repository.GetById<PolicyHeader>(dto.PolicyHeaderId);
            var quote = _repository.GetById<Quote>(policyHeader.QuoteId);
            var campaign = _repository.GetById<Campaign>(dto.CampaignId);

            var activity = entity.RejectedAtUnderwriting(dto.Context.UserId, quote, campaign);

            policyHeader.SetActivity(activity);
            result.Processed(entity.Id);
        }
    }
}