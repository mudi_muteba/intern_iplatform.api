﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Imports.Leads;
using Domain.Users;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;

namespace Domain.Leads.Handlers
{
    public class GetSeritiImportNextLeadHandler : BaseDtoHandler<GetSeritiImportNextLeadDto, NextLeadResponseDto>
    {
        private readonly IRepository m_Repository;

        public GetSeritiImportNextLeadHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            m_Repository = repository;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void InternalHandle(GetSeritiImportNextLeadDto dto, HandlerResult<NextLeadResponseDto> result)
        {
            var individualUploadDetail =
                m_Repository.GetAll<IndividualUploadDetail>().OrderBy(x => x.DateCreated)
                    .Where(x => x.Campaign.Id == dto.Id
                    && x.LeadAssigned != true
                    && x.IsDeleted == false
                    && x.DuplicateFileRecordCount == 0).ToList();
  
            if (!individualUploadDetail.Any())
            {
                result.Processed(NextLeadResponseDto.NoSeritiLead());
                return;
            }

            var userAssignCheckCampaignLeadBucket =
                        m_Repository.GetAll<CampaignLeadBucket>()
                        .Where(x => x.Agent.Id == dto.UserId
                        && x.IsDeleted != true
                        && x.Lead.IsDeleted != true
                        && !x.Lead.Party.IsDeleted).ToList();

            foreach (var campaignlead in userAssignCheckCampaignLeadBucket)
            {
                if (campaignlead.LeadCallCentreCode.Id == 1)
                {
                    result.Processed(NextLeadResponseDto.UpdatePendingLead());
                    return;
                }
            }

            foreach (var availableSeritiLead in individualUploadDetail)
            {
                var leadId =
                    m_Repository.GetAll<Lead>()
                        .Where(x => x.ExternalReference.ToLower() == availableSeritiLead.LeadImportReference.ToString().ToLower()
                        && x.IsDeleted == false)
                        .Select(x => x.Id)
                        .FirstOrDefault();
                if (leadId != 0)
                {
                    var user = m_Repository.GetById<User>(dto.UserId);

                    var updateCampaignLeadBucket =
                        m_Repository.GetAll<CampaignLeadBucket>()
                            .FirstOrDefault(x => x.Lead.Id == leadId
                                                 && x.IsDeleted != true);

                    if (updateCampaignLeadBucket != null)
                    {
                        updateCampaignLeadBucket.UpdateAgent(user);
                        availableSeritiLead.UpdateAssigned();
                        result.Processed(NextLeadResponseDto.NextLeads(leadId));
                        return;
                    }
                }
                else
                {
                    availableSeritiLead.UpdateAssigned();
                }
            }
        }
    }
}