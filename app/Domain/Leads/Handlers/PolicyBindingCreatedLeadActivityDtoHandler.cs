using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Activities;

namespace Domain.Leads.Handlers
{
    public class PolicyBindingCreatedLeadActivityDtoHandler : ExistingEntityDtoHandler<Lead, PolicyBindingCreatedLeadActivityDto, int>
    {
        public PolicyBindingCreatedLeadActivityDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }


        public override List<RequiredAuthorisationPoint> RequiredRights        
		{
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }
        protected override void HandleExistingEntityChange(Lead entity, PolicyBindingCreatedLeadActivityDto dto, HandlerResult<int> result)
        {
            entity.PolicyBindingCreated(dto);
            result.Processed(entity.Id);
        }
    }
}