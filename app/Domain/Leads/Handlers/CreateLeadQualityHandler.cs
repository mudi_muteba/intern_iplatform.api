﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Quality;

namespace Domain.Leads.Handlers
{
    public class CreateLeadQualityHandler : ExistingEntityDtoHandler<Lead, CreateLeadQualityDto, int>
    {
        public CreateLeadQualityHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            
        }

        
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void HandleExistingEntityChange(Lead entity, CreateLeadQualityDto dto, HandlerResult<int> result)
        {
            entity.SaveQuality(dto);
        }
    }
}