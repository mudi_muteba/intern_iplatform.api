﻿using System;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Campaigns.Extensions;
using Domain.Leads.Builder;
using Domain.Leads.Deduplication;
using Domain.Users;
using iPlatform.Api.DTOs.Leads;
using Domain.Party.ProposalHeaders.Builders;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Leads.Campaign;
using MasterData;

namespace Domain.Leads.Extensions
{
    public static class LeadExtensions
    {

        public static LeadImportResponseDto ImportLead(this SubmitLeadDto leadDto, IRepository repository, IWindsorContainer container)
        {
            var campaign = leadDto.ResolveCampaign(repository);



            var lead = new LeadMatching(repository).Match(leadDto)
                       ?? new LeadFromDtoBuilder(container, repository).Build(leadDto, campaign);

            lead.Imported(campaign);

            repository.Save(lead);

            return new LeadImportResponseDto {LeadId =  lead.Id, PartyId = lead.Party.Id};
        }

        public static int ImportProposals(this SubmitLeadDto leadDto, IRepository repository, IExecutionPlan executionPlan, SystemChannels systemChannels)
        {
            var lead = new LeadMatching(repository).Match(leadDto);

            if (leadDto.CanCreateProposal() & lead != null)
            {
                //set context
                leadDto.Request.SetContext(leadDto.Context);

                var proposal = new ProposalBuilder(repository, executionPlan, systemChannels).ImportMultiQuoteProposal(leadDto.Request, lead);

                return proposal.Id;
            }

            return 0;
        }

        public static void UpdateEventTemplate(this LeadCallCentreCodeDto code, CallCentreEventDetailsDto eventDetails)
        {
            eventDetails.Details.ForEach(x => { code.EventTemplate = code.EventTemplate.Replace(x.Code, x.Description); });
        }
    }
}
