﻿using System;
using System.Linq;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Leads;

namespace Domain.Campaigns.Extensions
{
    public static class CampaignExtensions
    {
        public static Campaign ResolveCampaign(this SubmitLeadDto leadDto, IRepository repository)
        {
            Campaign campaign = null;

            if (!string.IsNullOrEmpty(leadDto.CampaignReference))
                campaign = repository.GetAll<Campaign>().FirstOrDefault(a => a.References.Any() 
                    && a.References.Count(r => r.Name == leadDto.CampaignReference) > 0);

            if (campaign == null)
                campaign = GetDefaultCampaign(repository);

            campaign.Used();

            return campaign;
        }
        private static Campaign GetDefaultCampaign(IRepository repository)
        {
            var defaultCampaign = repository
                .GetAll<Campaign>()
                .FirstOrDefault(c => c.DefaultCampaign);

            if (defaultCampaign != null)
                return defaultCampaign;

            var firstCampaign = repository.GetAll<Campaign>().OrderBy(c => c.Id).Take(1).FirstOrDefault();

            if (firstCampaign != null)
                return firstCampaign;

            var newCampaign = new Campaign()
            {
                Id = 0,
                Name = "Default campaign",
                EndDate = DateTime.MaxValue,
                DefaultCampaign = true
            };

            repository.Save(newCampaign);
            return newCampaign;
        }
    }
}
