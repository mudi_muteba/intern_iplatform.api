﻿using Domain.Activities;
using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;
using LinqToExcel.Extensions;

namespace Domain.Leads.Overrides
{
    public class LeadOverride : IAutoMappingOverride<Lead>
    {
        public void Override(AutoMapping<Lead> mapping)
        {
            mapping.References(x => x.Party).Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy);
            mapping.HasMany(x => x.Activities).Cascade.SaveUpdate();
            mapping.References(x => x.LeadQuality).Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy);
            mapping.HasMany(x => x.CampaignLeadBuckets); 
        }
    }
}