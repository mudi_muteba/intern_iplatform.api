﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using Domain.Products;
using Domain.Admin;
using Domain.RatingRuleHeaders;
using MasterData;

namespace Domain.RatingRuleHeaderCalculations
{
    public class RatingRuleHeaderCalculation : Entity
    {
        public virtual RatingRuleHeader RatingRuleHeader { get; set; }
        public virtual Product Product { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual Cover Cover { get; set; }

        public virtual Question Question { get; set; }
        public virtual string QuestionAnswer { get; set; }
        public virtual string From { get; set; }
        public virtual string To { get; set; }
        public virtual decimal Premium { get; set; }
        public virtual decimal Percentage { get; set; }
        public virtual int Rank { get; set; }
        public virtual RateCalculatorType RateCalculatorType { get; set; }
        public virtual bool IsPercentage { get; set; }
        public virtual decimal MinPremium { get; set; }
        public virtual decimal MaxPremium { get; set; }
        public virtual string WarningMessage { get; set; }
        public virtual string FailureMessage { get; set; }

        public static RatingRuleHeaderCalculation Create(CreateRatingRuleHeaderCalculationDto dto)
        {
            return Mapper.Map<RatingRuleHeaderCalculation>(dto);
        }

        public virtual void Update(EditRatingRuleHeaderCalculationDto dto)
        {
            Mapper.Map<RatingRuleHeaderCalculation>(dto);
        }

        public virtual void Delete(DeleteRatingRuleHeaderCalculationDto dto)
        {
            Delete();
        }

        public virtual void GetById()
        {

        }

    }
}
