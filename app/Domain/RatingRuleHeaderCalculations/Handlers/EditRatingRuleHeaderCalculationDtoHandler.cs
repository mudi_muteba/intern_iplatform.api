﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using MasterData.Authorisation;

namespace Domain.RatingRuleCalculations.Handlers
{
    public class EditRatingRuleHeaderCalculationDtoHandler : ExistingEntityDtoHandler<RatingRuleHeaderCalculations.RatingRuleHeaderCalculation, EditRatingRuleHeaderCalculationDto, int>
    {
        public EditRatingRuleHeaderCalculationDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(RatingRuleHeaderCalculations.RatingRuleHeaderCalculation entity, EditRatingRuleHeaderCalculationDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}