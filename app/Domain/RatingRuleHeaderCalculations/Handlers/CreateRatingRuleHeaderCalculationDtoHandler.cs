﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using MasterData.Authorisation;

namespace Domain.RatingRuleCalculations.Handlers
{
   public  class CreateRatingRuleHeaderCalculationDtoHandler : CreationDtoHandler<RatingRuleHeaderCalculations.RatingRuleHeaderCalculation, CreateRatingRuleHeaderCalculationDto, int>
    {
        public CreateRatingRuleHeaderCalculationDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(RatingRuleHeaderCalculations.RatingRuleHeaderCalculation entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override RatingRuleHeaderCalculations.RatingRuleHeaderCalculation HandleCreation(CreateRatingRuleHeaderCalculationDto dto, HandlerResult<int> result)
        {
            return RatingRuleHeaderCalculations.RatingRuleHeaderCalculation.Create(dto);
        }
    }
}
