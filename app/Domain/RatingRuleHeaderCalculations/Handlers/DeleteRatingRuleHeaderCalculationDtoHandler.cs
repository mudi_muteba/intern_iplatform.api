﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using MasterData.Authorisation;

namespace Domain.RatingRuleCalculations.Handlers
{
    public class DeleteRatingRuleHeaderCalculationDtoHandler : ExistingEntityDtoHandler<RatingRuleHeaderCalculations.RatingRuleHeaderCalculation, DeleteRatingRuleHeaderCalculationDto, int>
    {
        public DeleteRatingRuleHeaderCalculationDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(RatingRuleHeaderCalculations.RatingRuleHeaderCalculation entity, DeleteRatingRuleHeaderCalculationDto dto, HandlerResult<int> result)
        {
            entity.Delete();
            result.Processed(entity.Id);
        }
    }
}