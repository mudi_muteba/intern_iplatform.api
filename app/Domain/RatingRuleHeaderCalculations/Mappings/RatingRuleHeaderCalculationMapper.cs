﻿using AutoMapper;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Admin;
using Domain.Admin;

namespace Domain.RatingRuleHeaderCalculations.Mappings
{
    public class RatingRuleHeaderCalculationMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<RatingRuleHeaderCalculationDto, RatingRuleHeaderCalculationDto>().ReverseMap();
            Mapper.CreateMap<RatingRuleHeaderCalculation, RatingRuleHeaderCalculationDto>().ReverseMap();

            Mapper.CreateMap<Products.Product, ProductInfoDto>().ReverseMap();
            Mapper.CreateMap<Channel, ChannelInfoDto>().ReverseMap();

            Mapper.CreateMap<RatingRuleHeaderCalculation, CreateRatingRuleHeaderCalculationDto>()
               .ForMember(dest => dest.RatingRuleHeader, opt => opt.MapFrom(src => src.RatingRuleHeader))
               .ForMember(dest => dest.Product, opt => opt.MapFrom(src => src.Product))
               .ForMember(dest => dest.Channel, opt => opt.MapFrom(src => src.Channel))
               .ForMember(dest => dest.Cover, opt => opt.MapFrom(src => src.Cover))
               .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Question))
                .ReverseMap();

            Mapper.CreateMap<RatingRuleHeaderCalculation, EditRatingRuleHeaderCalculationDto>()
                 .ForMember(dest => dest.RatingRuleHeader, opt => opt.MapFrom(src => src.RatingRuleHeader))
               .ForMember(dest => dest.Product, opt => opt.MapFrom(src => src.Product))
               .ForMember(dest => dest.Channel, opt => opt.MapFrom(src => src.Channel))
               .ForMember(dest => dest.Cover, opt => opt.MapFrom(src => src.Cover))
               .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Question))
               .ReverseMap();
            Mapper.CreateMap<RatingRuleHeaderCalculation, DeleteRatingRuleHeaderCalculationDto>().ReverseMap();

            Mapper.CreateMap<List<RatingRuleHeaderCalculation>, PagedResultDto<ListRatingRuleHeaderCalculationDto>>();
            Mapper.CreateMap<PagedResults<RatingRuleHeaderCalculation>, PagedResultDto<ListRatingRuleHeaderCalculationDto>>();
            Mapper.CreateMap<List<RatingRuleHeaderCalculation>, ListResultDto<ListRatingRuleHeaderCalculationDto>>()
               .ForMember(t => t.Results, o => o.MapFrom(s => s));
            Mapper.CreateMap<IEnumerable<RatingRuleHeaderCalculation>, ListResultDto<ListRatingRuleHeaderCalculationDto>>()
                .ForMember(d => d.Results, o => o.MapFrom(s => s));
            Mapper.CreateMap<RatingRuleHeaderCalculation, ListRatingRuleHeaderCalculationDto>().ReverseMap();
        }
    }
}
