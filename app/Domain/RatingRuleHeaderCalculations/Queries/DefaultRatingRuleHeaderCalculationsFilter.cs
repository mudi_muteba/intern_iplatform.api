﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.RatingRuleHeaderCalculations.Queries
{
    public class DefaultRatingRuleHeaderCalculationsFilter : IApplyDefaultFilters<RatingRuleHeaderCalculation>
    {
        public IQueryable<RatingRuleHeaderCalculation> Apply(ExecutionContext executionContext, IQueryable<RatingRuleHeaderCalculation> query)
        {
            return query.Where(q => !q.IsDeleted);
        }
    }
}
