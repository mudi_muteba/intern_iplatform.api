﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.RatingRuleHeaderCalculations.Queries
{
    public class GetAllRatingRuleHeaderCalculationsQuery : BaseQuery<RatingRuleHeaderCalculation>
    {
        public GetAllRatingRuleHeaderCalculationsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultRatingRuleHeaderCalculationsFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected internal override IQueryable<RatingRuleHeaderCalculation> Execute(IQueryable<RatingRuleHeaderCalculation> query)
        {
            //TODO: Add ordering
            return query;
        }
    }
}
