﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.CoverLinks.Queries
{
    public class GetAllCoverLinksQuery : BaseQuery<CoverLink>
    {
        public GetAllCoverLinksQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<CoverLink>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        protected internal override IQueryable<CoverLink> Execute(IQueryable<CoverLink> query)
        {
            return query;
        }
    }
}