﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.CoverLinks;
using MasterData.Authorisation;

namespace Domain.CoverLinks.Queries
{
    public class SearchCoverLinksQuery : BaseQuery<CoverLink>, ISearchQuery<SearchCoverLinksDto>
    {
        public SearchCoverLinksQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<CoverLink>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }
        private SearchCoverLinksDto _criteria;

        public void WithCriteria(SearchCoverLinksDto criteria)
        {
            this._criteria = criteria;
        }

        protected internal override IQueryable<CoverLink> Execute(IQueryable<CoverLink> query)
        {
            if (!string.IsNullOrEmpty(_criteria.CoverDefinitionName))
                query = query.Where(x => x.CoverDefinition.DisplayName.StartsWith(_criteria.CoverDefinitionName));

            if (_criteria.CoverDefinitionId > 0)
                query = query.Where(x => x.CoverDefinition.Id == _criteria.CoverDefinitionId);
  

            if (_criteria.ChannelId > 0)
                query = query.Where(x => x.ChannelId == _criteria.ChannelId);

            if (_criteria.Id > 0)
                query = query.Where(x => x.Id == _criteria.Id);

            return query;
        }
    }
}