﻿using AutoMapper;
using Domain.Base;
using Domain.Products;
using iPlatform.Api.DTOs.CoverLinks;

namespace Domain.CoverLinks
{
    public class CoverLink : Entity
    {
        public virtual int ChannelId { get; set; }
        public virtual CoverDefinition CoverDefinition { get; set; }
        public virtual int VisibleIndex { get; set; }

        public static CoverLink Create(CreateCoverLinkDto createDto)
        {
            var entity = Mapper.Map<CoverLink>(createDto);
            return entity;
        }

        public virtual void Update(EditCoverLinkDto editDto)
        {
            Mapper.Map(editDto, this);
        }

    }
}