﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.CoverLinks;
using MasterData.Authorisation;

namespace Domain.CoverLinks.Handlers
{
    public class CreateCoverLinkEventHandler : CreationDtoHandler<CoverLink, CreateCoverLinkDto, int>
    {
        public CreateCoverLinkEventHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            this.repository = repository;
        }
        private readonly IRepository repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(CoverLink entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override CoverLink HandleCreation(CreateCoverLinkDto dto, HandlerResult<int> result)
        {
            var entity = CoverLink.Create(dto);

            return entity;
        }
    }
}