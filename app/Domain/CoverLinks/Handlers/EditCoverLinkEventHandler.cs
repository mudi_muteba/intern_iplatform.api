﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.CoverLinks;
using MasterData.Authorisation;

namespace Domain.CoverLinks.Handlers
{
    public class EditCoverLinkEventHandler : ExistingEntityDtoHandler<CoverLink, EditCoverLinkDto, int>
    {
        public EditCoverLinkEventHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(CoverLink entity, EditCoverLinkDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);

            result.Processed(entity.Id);
        }
    }
}