﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.CoverLinks;
using MasterData.Authorisation;

namespace Domain.CoverLinks.Handlers
{
    public class DeleteCoverLinkEventHandler : ExistingEntityDtoHandler<CoverLink, DeleteCoverLinkDto, int>
    {
        public DeleteCoverLinkEventHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(CoverLink entity, DeleteCoverLinkDto dto, HandlerResult<int> result)
        {

            entity.Delete();
            result.Processed(entity.Id);
        }
    }
}