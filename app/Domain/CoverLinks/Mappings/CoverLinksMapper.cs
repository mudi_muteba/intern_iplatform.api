﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Products;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.CoverLinks;

namespace Domain.CoverLinks.Mappings
{
    public class CoverLinksMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CoverLink, ListCoverLinkDto>()
                .ForMember(t => t.ChannelId, o => o.MapFrom(s => s.ChannelId))
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)))
                .ForMember(t => t.CoverDefinition, o => o.MapFrom(s => s.CoverDefinition))
                ;

            Mapper.CreateMap<CoverLink, BasicCoverLinkDto>()
             .ForMember(t => t.ChannelId, o => o.MapFrom(s => s.ChannelId))
             .ForMember(t => t.CoverDefinition, o => o.MapFrom(s => s.CoverDefinition));

            Mapper.CreateMap<CoverLink, ListResultDto<ListCoverLinkDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));



            Mapper.CreateMap<CoverLink, ListCoverLinkDto>();

            Mapper.CreateMap<List<CoverLink>, ListResultDto<ListCoverLinkDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;
            Mapper.CreateMap<PagedResults<CoverLink>, PagedResultDto<ListCoverLinkDto>>();

            Mapper.CreateMap<CreateCoverLinkDto, CoverLink>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.ChannelId, o => o.MapFrom(s => s.Channel.Id))
                .ForMember(t => t.VisibleIndex, o => o.MapFrom(s => s.VisibleIndex))
                .ForMember(t => t.CoverDefinition, o => o.MapFrom(s => new CoverDefinition {Id = s.CoverDefinition.Id}))
                .ForMember(t => t.ChannelTags, o => o.Ignore())
                ;

            Mapper.CreateMap<EditCoverLinkDto, CoverLink>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.ChannelId, o => o.MapFrom(s => s.Channel.Id))
                .ForMember(t => t.VisibleIndex, o => o.MapFrom(s => s.VisibleIndex))
                .ForMember(t => t.CoverDefinition, o => o.MapFrom(s => new CoverDefinition { Id = s.CoverDefinition.Id }))
                .ForMember(t => t.ChannelTags, o => o.Ignore())
                ;
        }
    }
}