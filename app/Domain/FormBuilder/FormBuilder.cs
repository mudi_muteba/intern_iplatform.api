﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.FormBuilder;

namespace Domain.FormBuilder
{
    public class FormBuilder : Entity
    {
        public FormBuilder()
        {
            FormBuilderDetails = new List<FormBuilderDetail>();
        }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual IList<FormBuilderDetail> FormBuilderDetails { get; set; }

        public static FormBuilder Create(CreateFormBuilderDto dto)
        {
            var formBuilder = Mapper.Map<FormBuilder>(dto);
            return formBuilder;
        }

        public virtual void Update(EditFormBuilderDto dto)
        {
            Mapper.Map(dto, this);
        }

        public static FormBuilder AddFormBuilderDetails(FormBuilder entity, List<FormBuilderDetailDto> formBuilderDetailsDto)
        {
            foreach (var formBuilderDetailDto in formBuilderDetailsDto)
            {
                entity.FormBuilderDetails.Add(new FormBuilderDetail
                {
                    Version = formBuilderDetailDto.Version,
                    ChangeComment = formBuilderDetailDto.ChangeComment,
                    HtmlForm = formBuilderDetailDto.HtmlForm,
                    FormStatus = formBuilderDetailDto.FormStatus,
                    FormType = formBuilderDetailDto.FormType
                });
            }

            return entity;
        }

        public virtual void Disable(DisableFormBuilderDto dto)
        {
            Delete();
        }
    }
}
