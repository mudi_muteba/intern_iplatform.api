﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.FormBuilder;
using MasterData;

namespace Domain.FormBuilder
{
    public class FormBuilderDetail : Entity
    {
        public FormBuilderDetail()
        {
        }

        public virtual double Version { get; set; }
        public virtual FormType FormType { get; set; }
        public virtual FormStatus FormStatus { get; set; }
        public virtual string HtmlForm { get; set; }
        public virtual string ChangeComment { get; set; }
        public virtual FormBuilder FormBuilder { get; set; }

        public static FormBuilderDetail Create(CreateFormBuilderDetailDto dto)
        {
            var formBuilderDetail = Mapper.Map<FormBuilderDetail>(dto);
            return formBuilderDetail;
        }

        public virtual void Update(EditFormBuilderDetailDto dto)
        {
            Mapper.Map(dto, this);
        }

        public virtual void Disable(DisableFormBuilderDetailDto dto)
        {
            Delete();
        }
    }
}

