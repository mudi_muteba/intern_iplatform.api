﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.FormBuilder.Overrides
{
    public class FormBuilderDetailOverrides : IAutoMappingOverride<FormBuilderDetail>
    {
        public void Override(AutoMapping<FormBuilderDetail> mapping)
        {
            mapping.Map(x => x.HtmlForm).CustomType("StringClob").CustomSqlType("text");
            mapping.References(x => x.FormBuilder);
        }
    }
}