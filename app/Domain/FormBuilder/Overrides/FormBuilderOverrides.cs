﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.FormBuilder.Overrides
{
    public class FormBuilderOverrides : IAutoMappingOverride<FormBuilder>
    {
        public void Override(AutoMapping<FormBuilder> mapping)
        {
            mapping.HasMany(x => x.FormBuilderDetails).Cascade.SaveUpdate();
        }
    }
}