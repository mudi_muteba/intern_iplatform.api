﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.FormBuilder;
using MasterData.Authorisation;

namespace Domain.FormBuilder.Handlers
{
    public class DisableFormBuilderDetailDtoHandler : ExistingEntityDtoHandler<FormBuilderDetail, DisableFormBuilderDetailDto, int>
    {
        public DisableFormBuilderDetailDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(FormBuilderDetail entity, DisableFormBuilderDetailDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}