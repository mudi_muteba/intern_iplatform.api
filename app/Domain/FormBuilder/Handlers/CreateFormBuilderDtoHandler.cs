﻿using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.FormBuilder;
using MasterData.Authorisation;

namespace Domain.FormBuilder.Handlers
{
    public class CreateFormBuilderDtoHandler : CreationDtoHandler<FormBuilder, CreateFormBuilderDto, int>
    {
        private IRepository m_Repository;
        public CreateFormBuilderDtoHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(FormBuilder entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override FormBuilder HandleCreation(CreateFormBuilderDto dto, HandlerResult<int> result)
        {
            var entity = FormBuilder.Create(dto);

            if (entity.FormBuilderDetails != null)
            {
                var version = 1.0;
                foreach (var formDetail in entity.FormBuilderDetails)
                {
                    formDetail.Version = version;
                    version = version + 0.1;

                    if (version - Math.Truncate(version) == 0.9)
                    {
                        version = Math.Truncate(version) + 1.0;
                    }
                }
            }

            result.Processed(entity.Id);

            return entity;
        }
    }
}