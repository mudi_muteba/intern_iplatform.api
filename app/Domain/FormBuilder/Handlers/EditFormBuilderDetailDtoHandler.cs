﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using FluentNHibernate.Conventions;
using iPlatform.Api.DTOs.FormBuilder;
using MasterData;
using MasterData.Authorisation;

namespace Domain.FormBuilder.Handlers
{
    public class EditFormBuilderDetailDtoHandler : ExistingEntityDtoHandler<FormBuilderDetail, EditFormBuilderDetailDto, int>
    {
        private IRepository m_Repository;

        public EditFormBuilderDetailDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(FormBuilderDetail entity, EditFormBuilderDetailDto dto, HandlerResult<int> result)
        {
            var form = m_Repository.GetById<FormBuilder>(dto.IdForm);

            if (form != null)
            {
                if (!form.FormBuilderDetails.IsAny())
                {
                    if (dto.FormStatus == FormStatuses.Live || dto.FormStatus.Code == "Live" || dto.FormStatus.Name == "Live" || dto.FormStatus.Id == 1)
                    {
                        // Set existing to draft
                        foreach (var item in form.FormBuilderDetails)
                        {
                            item.FormStatus = new FormStatuses().FirstOrDefault(x => x.Code == "Draft");
                            m_Repository.Save(item);
                        }
                    }
                }
            }

            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}