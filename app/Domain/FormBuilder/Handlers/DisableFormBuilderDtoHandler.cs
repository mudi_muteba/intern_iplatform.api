﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.FormBuilder;
using MasterData.Authorisation;

namespace Domain.FormBuilder.Handlers
{
    public class DisableFormBuilderDtoHandler : ExistingEntityDtoHandler<FormBuilder, DisableFormBuilderDto, int>
    {
        public DisableFormBuilderDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(FormBuilder entity, DisableFormBuilderDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}