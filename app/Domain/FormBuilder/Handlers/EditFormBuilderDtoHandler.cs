﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.FormBuilder;
using MasterData.Authorisation;

namespace Domain.FormBuilder.Handlers
{
    public class EditFormBuilderDtoHandler : ExistingEntityDtoHandler<FormBuilder, EditFormBuilderDto, int>
    {
        private IRepository m_Repository;

        public EditFormBuilderDtoHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(FormBuilder entity, EditFormBuilderDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}