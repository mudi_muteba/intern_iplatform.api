﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using FluentNHibernate.Conventions;
using iPlatform.Api.DTOs.FormBuilder;
using MasterData;
using MasterData.Authorisation;

namespace Domain.FormBuilder.Handlers
{
    public class CreateFormBuilderDetailDtoHandler : CreationDtoHandler<FormBuilderDetail, CreateFormBuilderDetailDto, int>
    {
        private IRepository m_Repository;
        public CreateFormBuilderDetailDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(FormBuilderDetail entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override FormBuilderDetail HandleCreation(CreateFormBuilderDetailDto dto, HandlerResult<int> result)
        {
            var entityFormBuilder = new FormBuilderDetail();
            var entity = m_Repository.GetById<FormBuilder>(dto.FormBuilderId);

            if (entity.Id > 0 && entity.Id == dto.FormBuilderId)
            {
                if (!entity.FormBuilderDetails.IsAny())
                {
                    if (dto.FormStatus == FormStatuses.Live || dto.FormStatus.Code == "Live" || dto.FormStatus.Name == "Live" || dto.FormStatus.Id == 1)
                    {
                        // Set existing to draft
                        foreach (var item in entity.FormBuilderDetails)
                        {
                            item.FormStatus = new FormStatuses().FirstOrDefault(x => x.Code == "Draft");
                            m_Repository.Save(item);
                        }
                    }
                }

                dto.Version = VersionManager(entity);
                entityFormBuilder = FormBuilderDetail.Create(dto);
            }

            return entityFormBuilder;
        }

        public double VersionManager(FormBuilder entity)
        {
            if (entity != null && entity.FormBuilderDetails.Any())
            {
                var latest = entity.FormBuilderDetails.Select(x => x.Version).Max();

                if (latest - Math.Truncate(latest) == 0.9)
                {
                    return Math.Truncate(latest) + 1.0;
                }
                return latest + 0.1;
            }
            return 1.0;
        }
    }
}