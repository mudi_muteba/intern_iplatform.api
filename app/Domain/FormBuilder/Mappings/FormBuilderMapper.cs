﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.FormBuilder;

namespace Domain.FormBuilder.Mappings
{
    public class FormBuilderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateFormBuilderDto, FormBuilder>()
                .ForMember(t => t.FormBuilderDetails, o => o.MapFrom(s => s.FormBuilderDetailsDto));

            Mapper.CreateMap<FormBuilderDetailDto, FormBuilderDetail>();

            Mapper.CreateMap<CreateFormBuilderDetailDto, FormBuilderDetail>()
                .ForMember(t => t.FormBuilder, o => o.MapFrom(s => new FormBuilder { Id = s.FormBuilderId}))
                ;

            Mapper.CreateMap<FormBuilder, FormBuilderDto>()
                .ForMember(t => t.FormBuilderDetails, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    foreach (var formdetail in s.FormBuilderDetails)
                    {
                        formdetail.Version = Math.Round(formdetail.Version, 1);
                        d.FormBuilderDetails.Add(Mapper.Map<FormBuilderDetailDto>(formdetail));
                    }
                });


            Mapper.CreateMap<FormBuilderDetail, FormBuilderDetailDto>();

            Mapper.CreateMap<List<FormBuilder>, ListResultDto<FormBuilderDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<PagedResults<FormBuilder>, PagedResultDto<FormBuilderDto>>();

            Mapper.CreateMap<EditFormBuilderDto, FormBuilder>()
                .ForMember(t => t.FormBuilderDetails, o => o.Ignore());

            Mapper.CreateMap<EditFormBuilderDetailDto, FormBuilderDetail>()
                .ForMember(t => t.Version, o => o.Ignore());
           
        }
    }
}