﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.FormBuilder.Queries
{
    public class GetAllFormBuilderQuery : BaseQuery<FormBuilder>
    {
        public GetAllFormBuilderQuery(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new NoDefaultFilters<FormBuilder>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<FormBuilder> Execute(IQueryable<FormBuilder> query)
        {
            return query;
        }
    }
}