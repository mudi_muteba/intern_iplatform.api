﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.FormBuilder;
using MasterData.Authorisation;

namespace Domain.FormBuilder.Queries
{
    public class SearchFormBuilderQuery : BaseQuery<FormBuilder>, ISearchQuery<SearchFormBuilderDto>
    {
        private SearchFormBuilderDto _criteria;

        public SearchFormBuilderQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<FormBuilder>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchFormBuilderDto criteria)
        {
            _criteria = criteria;
        }

        protected internal override IQueryable<FormBuilder> Execute(IQueryable<FormBuilder> query)
        {
            if (!string.IsNullOrEmpty(_criteria.Name))
            {
                query = query.Where(x => x.Name.StartsWith(_criteria.Name));
            }

            if (!string.IsNullOrEmpty(_criteria.Description))
            {
                query = query.Where(x => x.Description.StartsWith(_criteria.Description));
            }

            if (_criteria.FormStatusId > 0)
            {
                query = query.Where(x => x.FormBuilderDetails.Any(v => v.FormStatus.Id == _criteria.FormStatusId));
            }

            if (_criteria.FormTypeId > 0)
            {
                query = query.Where(x => x.FormBuilderDetails.Any(v => v.FormType.Id == _criteria.FormTypeId));
            }

            query = AddOrdering(query);

            return query;
        }

        private IQueryable<FormBuilder> AddOrdering(IQueryable<FormBuilder> query)
        {
            query = new OrderByBuilder<FormBuilder>(OrderByDefinition).Build(query, _criteria.OrderBy);

            return query;
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("Name"), () => "Name"},
                    {new OrderByField("Description"), () => "Description"}
                };
            }
        }
    }
}