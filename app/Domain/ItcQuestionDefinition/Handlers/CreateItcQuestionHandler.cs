﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Repository;
using MasterData.Authorisation;
using Domain.Base.Handlers;
using iPlatform.Api.DTOs.ItcQuestion;

namespace Domain.ItcQuestionDefinition.Handlers
{
    public class CreateItcQuestionHandler : CreationDtoHandler<ItcQuestionDefinition, CreateItcQuestionDto, int>
    {
        public CreateItcQuestionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {}

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void EntitySaved(ItcQuestionDefinition entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ItcQuestionDefinition HandleCreation(CreateItcQuestionDto dto, HandlerResult<int> result)
        {
            var entity = ItcQuestionDefinition.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}
