﻿using System;
using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.ItcQuestion;
using MasterData.Authorisation;

namespace Domain.ItcQuestionDefinition.Handlers
{
    public class UpdateItcQuestionHandler : ExistingEntityDtoHandler<ItcQuestionDefinition, UpdateItcQuestionDto, int>
    {
        public UpdateItcQuestionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {}

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void HandleExistingEntityChange(ItcQuestionDefinition entity, UpdateItcQuestionDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
