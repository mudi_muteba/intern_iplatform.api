﻿using System;
using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.ItcQuestion;
using MasterData.Authorisation;

namespace Domain.ItcQuestionDefinition.Handlers
{
    public class DeleteItcQuestionHandler : ExistingEntityDtoHandler<ItcQuestionDefinition, RemoveItcQuestionDto, int>
    {
        public DeleteItcQuestionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void HandleExistingEntityChange(ItcQuestionDefinition entity, RemoveItcQuestionDto dto, HandlerResult<int> result)
        {
            entity.DateUpdated = DateTime.UtcNow;
            entity.Delete();
            result.Processed(entity.Id);
        }
    }
}
