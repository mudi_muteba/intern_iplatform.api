﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.ItcQuestionDefinition.Overrides
{
    public class ItcQuestionDefinitionOverride: IAutoMappingOverride<ItcQuestionDefinition>
    {
        public void Override(AutoMapping<ItcQuestionDefinition> mapping)
        {
            mapping.Map(x => x.QuestionText).CustomType("StringClob").CustomSqlType("ntext");
        }
    }
}
