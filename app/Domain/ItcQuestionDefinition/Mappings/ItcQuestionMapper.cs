﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.ItcQuestion;

namespace Domain.ItcQuestionDefinition.Mappings
{
    public class ItcQuestionMapper: Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ItcQuestionDefinitionDto, ItcQuestionDefinition>()
                .ForMember(x => x.Id, o => o.Ignore())
                .ForMember(s => s.IsDeleted, t => t.MapFrom(x => !x.Active));
            
            Mapper.CreateMap<ItcQuestionDefinition, ItcQuestionDefinitionDto>()
                .ForMember(s => s.Active, e => e.MapFrom(i => !i.IsDeleted));
            
            Mapper.CreateMap<List<ItcQuestionDefinition>, ListResultDto<ItcQuestionDefinitionDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<CreateItcQuestionDto, ItcQuestionDefinition>()
                // .ForMember(s => s.DateCreated, o => o.MapFrom(s => DateTime.UtcNow))
                .ForMember(s => s.IsDeleted, t => t.MapFrom(x => !x.Active));

            Mapper.CreateMap<ItcQuestionDefinitionDto, UpdateItcQuestionDto>();

            Mapper.CreateMap<UpdateItcQuestionDto, ItcQuestionDefinition>()
                .ForMember(s => s.IsDeleted, t => t.MapFrom(x => !x.Active));
        }
    }
}
