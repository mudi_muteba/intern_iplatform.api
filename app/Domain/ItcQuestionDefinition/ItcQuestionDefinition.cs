﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.ItcQuestion;
using System;

namespace Domain.ItcQuestionDefinition
{
    public class ItcQuestionDefinition: Entity
    {
        public virtual int? ChannelId { get; protected internal set; }
        public virtual string QuestionText { get; protected internal set; }
        public virtual DateTime DateCreated { get; protected internal set; }
        public virtual DateTime? DateUpdated { get; protected internal set; }
        public virtual string Name { get; protected internal set; }

        public static ItcQuestionDefinition Create(CreateItcQuestionDto dto)
        {
            var entity = Mapper.Map<ItcQuestionDefinition>(dto);
            entity.DateUpdated = null;
            entity.DateCreated = DateTime.UtcNow;
            entity.IsDeleted = false;
            return entity;
        }

        public virtual void Update(UpdateItcQuestionDto dto)
        {
            dto.DateUpdated = DateTime.UtcNow;
            Mapper.Map(dto, this);
        }

        public virtual void Remove(RemoveItcQuestionDto dto)
        {
            DateUpdated = DateTime.UtcNow;
            Delete();
        }

        public ItcQuestionDefinition()
        {
                
        }

        public ItcQuestionDefinition(int id, string name, string questionText, DateTime createdOn, int? channelId, DateTime? updatedOn, bool isActive = true)
        {
            Id = id;
            Name = name;
            ChannelId = channelId;
            QuestionText = questionText;
            DateCreated = createdOn;
            DateUpdated = updatedOn;
            IsDeleted = !isActive;
        }
    }
}
