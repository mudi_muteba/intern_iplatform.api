﻿using System.Collections.Generic;
using System.Linq;

using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.ItcQuestionDefinition.Queries
{
    public class ItcQuestionByChannelCodeQuery : BaseQuery<ItcQuestionDefinition>
    {
        private string m_ChannelCode;
        private readonly IRepository m_Repository;

        public ItcQuestionByChannelCodeQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ItcQuestionDefinition>())
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.List
                };
            }
        }

        public ItcQuestionByChannelCodeQuery WithChannelCode(string channelCode)
        {
            m_ChannelCode = channelCode;
            return this;
        }

        protected internal override IQueryable<ItcQuestionDefinition> Execute(IQueryable<ItcQuestionDefinition> query)
        {
            var channel = m_Repository.GetAll<Channel>()
                .FirstOrDefault(q =>
                    !q.IsDeleted &&
                    q.Code == m_ChannelCode ||
                    q.ExternalReference == m_ChannelCode);

            var result = channel != null ? query.Where(q => !q.IsDeleted && q.ChannelId == channel.Id) : null;

            if (result == null || !result.Any())
            {
                result = query.Where(q => !q.IsDeleted && q.ChannelId == null);
            }

            return result;
        }
    }
}
