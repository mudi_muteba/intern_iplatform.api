﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.ItcQuestionDefinition.Queries
{
    public class ItcQuestionByIdQuery : BaseQuery<ItcQuestionDefinition>
    {
        private int m_Id;

        public ItcQuestionByIdQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new NoDefaultFilters<ItcQuestionDefinition>())
        {}

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    LeadAuthorisation.List,
                };
            }
        }

        public ItcQuestionByIdQuery WithCriteria(int id)
        {
            m_Id = id;
            return this;
        }

        protected internal override IQueryable<ItcQuestionDefinition> Execute(IQueryable<ItcQuestionDefinition> query)
        {
            var result = query.Where(q => !q.IsDeleted && q.Id == m_Id);

            if (!result.Any())
            {
                result = query.Where(q => !q.IsDeleted && q.ChannelId == null);
            }

            return result;
        }
    }
}
