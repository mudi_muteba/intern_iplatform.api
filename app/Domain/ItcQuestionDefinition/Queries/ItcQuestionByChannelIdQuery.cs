﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.ItcQuestionDefinition.Queries
{
    public class ItcQuestionByChannelIdQuery : BaseQuery<ItcQuestionDefinition>
    {
        private int? m_ChannelId;
        private readonly IRepository m_Repository;

        public ItcQuestionByChannelIdQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new NoDefaultFilters<ItcQuestionDefinition>())
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.List
                };
            }
        }

        public ItcQuestionByChannelIdQuery WithChannelId(int? channelId)
        {
            m_ChannelId = channelId;
            return this;
        }

        protected internal override IQueryable<ItcQuestionDefinition> Execute(IQueryable<ItcQuestionDefinition> query)
        {
            var channel = m_ChannelId.HasValue ? m_Repository.GetAll<Channel>()
                .FirstOrDefault(q => !q.IsDeleted && 
                                q.Id == m_ChannelId): null;

            var result = channel != null ? query.Where(q => !q.IsDeleted && q.ChannelId == channel.Id) : null;

            if (result == null || !result.Any())
            {
                result = query.Where(q => !q.IsDeleted && q.ChannelId == null);
            }

            return result;
        }
    }
}
