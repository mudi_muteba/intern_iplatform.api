﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.ItcQuestionDefinition.Queries
{
    public class CheckItcQuestionByChannelIdQuery : BaseQuery<ItcQuestionDefinition>
    {
        private int? m_channelId;
        private readonly IRepository m_repository;

        public CheckItcQuestionByChannelIdQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, new NoDefaultFilters<ItcQuestionDefinition>())
        {
            m_repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.List
                };
            }
        }

        public CheckItcQuestionByChannelIdQuery WithChannelId(int? channelId)
        {
            m_channelId = channelId;
            return this;
        }

        protected internal override IQueryable<ItcQuestionDefinition> Execute(IQueryable<ItcQuestionDefinition> query)
        {
            var channel = m_channelId.HasValue ? m_repository.GetAll<Channel>().FirstOrDefault(q => !q.IsDeleted && q.Id == m_channelId): null;

            var result = channel != null ? query.Where(q => !q.IsDeleted && q.ChannelId == channel.Id) : null;

            if (result == null)
            {
                result = query.Where(q => !q.IsDeleted && q.ChannelId == null);
            }

            return result;
        }
    }
}
