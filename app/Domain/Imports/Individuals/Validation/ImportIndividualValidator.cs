﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Individual;
using ValidationMessages.Individual.Imports;
using System.Collections.Generic;
using System;



namespace Domain.Imports.Individuals.Validation
{
    public class ImportIndividualValidator : IValidateDto<ImportIndividualDto>
    {

        public ImportIndividualValidator(IProvideContext contextProvider, IRepository repository)
        {
           
        }

        public void Validate(ImportIndividualDto dto, ExecutionResult result)
        {
            if (string.IsNullOrEmpty(dto.IdentityNo))
                result.AddValidationMessage(ImportIndividualValidationMessages.IDNumberRequired);

            if (dto.ChannelId == 0)
                result.AddValidationMessage(ImportIndividualValidationMessages.ChannelIdRequired);
    
            Guid guidvalue;
            if (string.IsNullOrEmpty(dto.ExternalReference))
                result.AddValidationMessage(ImportIndividualValidationMessages.ExternalReferenceRequired);
            else if(!Guid.TryParse(dto.ExternalReference, out guidvalue))
                result.AddValidationMessage(ImportIndividualValidationMessages.ExternalReferenceInvalid);


        }

    }
}