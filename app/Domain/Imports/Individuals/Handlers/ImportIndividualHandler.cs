﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Individual;
using MasterData.Authorisation;
using AutoMapper;

namespace Domain.Imports.Individuals.Handlers
{
    public class ImportIndividualHandler : BaseDtoHandler<ImportIndividualDto, Guid>
    {
        private readonly GetIndividualBySearch _query;

        public ImportIndividualHandler(IProvideContext contextProvider
            , GetIndividualBySearch query) : base(contextProvider)
        {
            _query = query;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Import
                };
            }
        }

        protected override void InternalHandle(ImportIndividualDto dto, HandlerResult<Guid> result)
        {
           var entity = _query
               .ByIdNumber(dto.IdentityNo)
               .ByExternalReference(dto.ExternalReference)
               .ByContactDetails(dto.ContactDetail)
               .ExecuteQuery().ToList();

            if(entity.Any())
            {
                var editIndividualDto = Mapper.Map<EditIndividualDto>(entity.FirstOrDefault());
                result.Processed(new Guid(editIndividualDto.ExternalReference));
            }
            else
            {
                var createIndividualDto = Mapper.Map<CreateIndividualDto>(dto);
                result.Processed(new Guid(createIndividualDto.ExternalReference));
            }
        }
    }
}