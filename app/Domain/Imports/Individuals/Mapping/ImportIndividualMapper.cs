﻿using System;
using AutoMapper;
using iPlatform.Api.DTOs.Individual;

namespace Domain.Imports.Individuals.Mapping
{
    public class ImportIndividualDtoMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ImportIndividualDto, CreateIndividualDto>()
                .ForMember(t => t.ExternalReference, o => o.MapFrom(s => 
                    string.IsNullOrEmpty(s.ExternalReference)? new Guid() : new Guid(s.ExternalReference)));
        }
    }
}