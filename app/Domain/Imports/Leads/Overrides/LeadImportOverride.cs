﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Imports.Leads.Overrides
{
    public class LeadImportOverride : IAutoMappingOverride<LeadImport>
    {
        public void Override(AutoMapping<LeadImport> mapping)
        {
            mapping.References(x => x.Campaign, "CampaignId");
            mapping.References(x => x.CreatedBy, "CreatedBy");
        }
    }
}