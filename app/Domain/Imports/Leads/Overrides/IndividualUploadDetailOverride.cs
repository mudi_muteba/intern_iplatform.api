﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Imports.Leads.Overrides
{
    public class IndividualUploadDetailOverride : IAutoMappingOverride<IndividualUploadDetail>
    {
        public void Override(AutoMapping<IndividualUploadDetail> mapping)
        {
            mapping.References(x => x.Campaign);
        }
    }
}