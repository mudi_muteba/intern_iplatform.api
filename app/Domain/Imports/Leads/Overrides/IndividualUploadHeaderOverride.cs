﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Imports.Leads.Overrides
{
    public class IndividualUploadHeaderOverride : IAutoMappingOverride<IndividualUploadHeader>
    {
        public void Override(AutoMapping<IndividualUploadHeader> mapping)
        {
            mapping.References(x => x.Campaign);
        }
    }
}