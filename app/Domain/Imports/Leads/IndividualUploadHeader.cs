using System;
using AutoMapper;
using Domain.Admin;
using Domain.Base;
using Domain.Campaigns;
using Domain.Users;
using iPlatform.Api.DTOs.Leads;
using MasterData;

namespace Domain.Imports.Leads
{
    public class IndividualUploadHeader : Entity
    {
        public virtual Guid LeadImportReference { get; protected set; }
        public virtual Channel Channel { get; protected set; }
        public virtual Campaign Campaign { get; set; }
        public virtual User CreatedBy { get; protected set; }
        public virtual DateTime DateCreated { get; protected set; }
        public virtual DateTime? DateUpdated { get; protected set; }
        public virtual string FileName { get; protected set; }
        public virtual int FileRecordCount { get; protected set; }
        public virtual int DuplicateFileRecordCount { get; protected set; }
        public virtual int? NewCount { get; protected set; }
        public virtual int ExistingCount { get; protected set; }
        public virtual int FailureCount { get; protected set; }
        public virtual MasterData.LeadImportStatus LeadImportStatus { get; protected set; }

        public static IndividualUploadHeader Create(CreateIndividualUploadHeaderDto dto)
        {
            return Mapper.Map<IndividualUploadHeader>(dto).Created();
        }

        public virtual IndividualUploadHeader UpdateFailureCount(int count)
        {
            FailureCount = count;
            return this;
        }

        public virtual IndividualUploadHeader Created()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateCreated;
            LeadImportStatus = LeadImportStatuses.Submitted;
            return this;
        }
    }
}