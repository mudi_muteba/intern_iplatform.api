using System;
using AutoMapper;
using Domain.Base;
using Domain.Campaigns;
using Domain.Users;
using iPlatform.Api.DTOs.Leads.Excel;

namespace Domain.Imports.Leads
{
    public class LeadImport : Entity
    {
        public virtual Guid LeadImportReference { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual string FileName { get; protected set; }
        public virtual Campaign Campaign { get; set; }
        public virtual User CreatedBy { get; protected set; }
        public virtual DateTime? DateCreated { get; protected set; }
        public virtual DateTime? DateUpdated { get; protected set; }
        public virtual DateTime? DateImported { get; protected set; }
        public virtual string Status { get; protected set; }
        public virtual int TotalRecords { get; set; }
        public virtual int TotalImported { get; set; }
        public virtual int TotalErrors { get; set; }

        public LeadImport() { }

        public static LeadImport Create(LeadImportExcelDto dto)
        {
            return Mapper.Map<LeadImport>(dto).Created();
        }

        public virtual LeadImport Created()
        {
            LeadImportReference = Guid.NewGuid();
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
            return this;
        }

    }
}