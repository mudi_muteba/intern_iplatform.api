using System;
using AutoMapper;
using Domain.Admin;
using Domain.Base;
using Domain.Campaigns;
using Domain.Imports.Leads.Dtos;
using Domain.Imports.Leads.Events;
using Domain.Users;
using iPlatform.Api.DTOs.Leads;
using MasterData;

namespace Domain.Imports.Leads
{
    public class IndividualUploadDetail : EntityWithAudit
    {
        public IndividualUploadDetail()
        {
            DateCreated = DateTime.UtcNow;
        }
        public virtual Guid LeadImportReference { get; protected set; }
        public virtual Channel Channel { get; protected set; }
        public virtual Campaign Campaign { get; set; }
        public virtual User CreatedBy { get; protected set; }
        public virtual DateTime DateCreated { get; protected set; }
        public virtual string FileName { get; protected set; }
        public virtual string ContactNumber { get; protected set; }
        public virtual bool NewIndividual { get; protected set; }
        public virtual int DuplicateFileRecordCount { get; protected set; }
        public virtual bool LeadAssigned { get; set; }
        public virtual MasterData.LeadImportStatus LeadImportStatus { get; protected set; }

        public static IndividualUploadDetail Create(IndividualUploadDetailDto dto)
        {
            var individualUploadDetail = Mapper.Map<IndividualUploadDetail>(dto).Submitted();
            return individualUploadDetail;
        }

        public virtual IndividualUploadDetail Submitted()
        {
            DateCreated = DateTime.UtcNow;
            LeadImportStatus = LeadImportStatuses.Submitted;
            return this;
        }


        public virtual IndividualUploadDetail Update(EditIndividualUploadDetailDto dto)
        {
            Mapper.Map(dto, this);

            if (dto.LeadImportStatus.Name.ToLower() == "success")
                return this;

            RaiseEvent(new UpdateLeadImportEvent(new IndividualUploadDetailEventDto()
            {
                LeadImportStatus = dto.LeadImportStatus,
                LeadImportReference = dto.LeadImportReference,
                Comment = dto.Comment,
                PartyId = dto.PartyId
            }));
        
            return this;
        }

        public virtual void UpdateAssigned()
        {
            LeadAssigned = true;
        }
    }
}
