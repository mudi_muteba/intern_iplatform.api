﻿using AutoMapper;
using Common.Logging;
using Domain.Imports.Leads.Workflow.Messages;
using Domain.Imports.Leads.Workflow.Metadata;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Leads;
using Workflow.Domain;
using Workflow.Messages;
using Infrastructure.Configuration;
using iPlatform.Enums;

namespace Domain.Imports.Leads.Consumers
{
    public class EditIndividualUploadDetailMessageConsumer : AbstractMessageConsumer<EditIndividualUploadDetailMessage>
    {
        private readonly ILog _log = LogManager.GetLogger<EditIndividualUploadDetailMessageConsumer>();
        private readonly IConnector _connector;
        public EditIndividualUploadDetailMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
        }

        public override void Consume(EditIndividualUploadDetailMessage message)
        {
            var metadata = message.Metadata as EditIndividualUploadDetailMetadata;
            if (metadata == null)
            {
                _log.WarnFormat("Unable to cast ITaskMetadata as {0}", typeof(EditIndividualUploadDetailMetadata).ToString());
                return;
            }

            var dto = Mapper.Map<EditIndividualUploadDetailDto>(metadata);
            var response = _connector.ImportManagement.Lead.EditIndividualUploadDetail(dto);
        }
    }
}