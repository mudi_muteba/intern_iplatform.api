﻿using Domain.Imports.Leads.Workflow.Messages;
using Domain.Imports.Leads.Workflow.Metadata;
using Infrastructure.Configuration;
using iPlatform.Api.DTOs.Base.Connector;
using MasterData;
using Workflow.Domain;
using Workflow.Messages;
using System.Linq;
using Domain.Imports.Leads.Helpers;
using AutoMapper;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Enums;

namespace Domain.Imports.Leads.Consumers
{
    public class EditLeadMessageConsumer : AbstractMessageConsumer<EditLeadMessage>
    {
        private readonly IConnector m_Connector;
        private readonly LeadImportHelper m_Builder;

        public EditLeadMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            m_Connector = connector;
            var token = m_Connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            m_Connector.SetToken(token);
            m_Builder = new LeadImportHelper(m_Connector);
        }

        public override void Consume(EditLeadMessage message)
        {
            var editMetadata = message.Metadata as EditLeadMetadata;
            if (editMetadata == null)
            {
                return;
            }

            var result = Execute(editMetadata);
            if (result)
            {
                message.AddSuccessMessage(new EditIndividualUploadDetailMessage(Mapper.Map<EditIndividualUploadDetailMetadata>(editMetadata.EditIndividualUploadDetailDto.Success())));
            }
            else
            {
                message.AddFailureMessage(new CreateIndividualUploadDetailMessage(Mapper.Map<EditIndividualUploadDetailMetadata>(editMetadata.EditIndividualUploadDetailDto.Failure())));
            } 
        }

        private bool Execute(EditLeadMetadata editMetadata)
        {
            var identityNo = editMetadata.EditIndividualDto.IdentityNo;
            var partyId = editMetadata.EditIndividualDto.Id;

            //p-cube call
            m_Builder.GetPersonInformation(editMetadata.Validate());

            //Update Individual
            var individual = m_Builder.EditIndividual(editMetadata.EditIndividualDto);

            if (!individual.IsSuccess)
            {
                return false;
            }
                

            //Get Lead
            var leadResponse = m_Connector.LeadManagement.Lead.GetByPartyId(partyId);
            if (!leadResponse.IsSuccess)
            {
                return false;
            }

            var leadId = leadResponse.Response.Id;

            //Set Lead as imported
            var leadimportResponse = m_Connector.LeadManagement.Lead.ImportedLeadActivity(new ImportedLeadActivityDto { Id = leadId, CampaignId = editMetadata.CampaignId, ImportType = ImportTypes.Seriti, Existing = true } );
            if (!leadimportResponse.IsSuccess)
            {
                return false;
            }


            //Quality
            m_Builder.ImportQuality(leadId, identityNo);

            //losshistory
            var createLossHistoryResponse = m_Builder.ImportLossHistory(partyId);
            if (!createLossHistoryResponse)
            {
                return false;
            }

            // Proposal
            var proposalResponse = m_Builder.ImportProposals(partyId, editMetadata.CampaignId, editMetadata.Items, editMetadata.EditIndividualDto.ChannelId);
            if (!proposalResponse.IsSuccess || proposalResponse.Errors.Any())
            {
                return false;
            }

            //Sales Info
            var salesResponse = m_Builder.ImportSalesInformation(editMetadata.ImportSalesStructureDto, editMetadata.CreateSalesFniDto, editMetadata.CreateSalesDetailsDto,
                            leadId, proposalResponse.Response);

            //CampaignBucket
            var bucketResponse = m_Builder.ImportLeadToCampaigns(leadId, editMetadata.CampaignId);
            if (!bucketResponse.IsSuccess && !bucketResponse.Response)
            {
                return false;
            }

            return true;
        }
    }
}