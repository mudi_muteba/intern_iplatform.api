﻿using Domain.Imports.Leads.Workflow.Messages;
using Domain.Imports.Leads.Workflow.Metadata;
using Infrastructure.Configuration;
using iPlatform.Api.DTOs.Base.Connector;
using Workflow.Domain;
using Workflow.Messages;
using System.Linq;
using AutoMapper;
using iPlatform.Api.DTOs.Activities;
using MasterData;
using Domain.Imports.Leads.Helpers;
using iPlatform.Enums;

namespace Domain.Imports.Leads.Consumers
{
    public class CreateLeadMessageConsumer : AbstractMessageConsumer<CreateLeadMessage>
    {
        private readonly IConnector m_Connector;
        private readonly LeadImportHelper m_Builder;

        public CreateLeadMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector) : base(router, executor)
        {
            m_Connector = connector;
            var token = m_Connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            m_Connector.SetToken(token);
            m_Builder = new LeadImportHelper(m_Connector);
        }

        public override void Consume(CreateLeadMessage message)
        {
            var createMetadata = message.Metadata as CreateLeadMetadata;
            if (createMetadata == null)
            {
                return;
            }

            var result = Execute(createMetadata);
            if (result)
            {
                message.AddSuccessMessage(new EditIndividualUploadDetailMessage(Mapper.Map<EditIndividualUploadDetailMetadata>(createMetadata.EditIndividualUploadDetailDto.Success())));
            }
            else
            {
                message.AddFailureMessage(new CreateIndividualUploadDetailMessage(Mapper.Map<EditIndividualUploadDetailMetadata>(createMetadata.EditIndividualUploadDetailDto.Failure())));
            }
        }

        private bool Execute (CreateLeadMetadata createMetadata)
        {
            //p-cube call
            m_Builder.GetPersonInformation(createMetadata.Validate());

            var identityNo = createMetadata.CreateIndividualDto.IdentityNo;

            //Individual Created
            var individual = m_Builder.CreateIndividual(createMetadata.CreateIndividualDto);
            if (!individual.IsSuccess)
            {
                return false;
            }

            var partyId = individual.Response;

            //Get Lead
            var leadResponse = m_Connector.LeadManagement.Lead.GetByPartyId(partyId);
            if (!leadResponse.IsSuccess)
            {
                return false;
            }

            var leadId = leadResponse.Response.Id;

            //Set Lead as imported
            var leadimportResponse = m_Connector.LeadManagement.Lead.ImportedLeadActivity(new ImportedLeadActivityDto { Id = leadId, CampaignId = createMetadata.CampaignId, ImportType = ImportTypes.Seriti });
            if (!leadimportResponse.IsSuccess)
            {
                return false;
            }

            //Addresses
            m_Builder.ImportAddresses(partyId, createMetadata.Addresses);

            //Quality
            m_Builder.ImportQuality(leadId, identityNo);
           
            //losshistory
            var createLossHistoryResponse = m_Builder.ImportLossHistory(partyId);
            if (!createLossHistoryResponse)
            {
                return false;
            }
                

            // Proposal
            var proposalResponse = m_Builder.ImportProposals(partyId, createMetadata.CampaignId, createMetadata.Items, createMetadata.CreateIndividualDto.ChannelId);
            if (!proposalResponse.IsSuccess || proposalResponse.Errors.Any())
            {
                return false;
            }
               
            //Sales Info
            var salesResponse = m_Builder.ImportSalesInformation(createMetadata.ImportSalesStructureDto, createMetadata.CreateSalesFniDto, createMetadata.CreateSalesDetailsDto,
                            leadId, proposalResponse.Response);

            //CampaignBucket
            var bucketResponse = m_Builder.ImportLeadToCampaigns(leadId, createMetadata.CampaignId);
            if (!bucketResponse.IsSuccess && !bucketResponse.Response)
            {
                return false;
            }
                
            return true;
        }
    }
}