﻿using System;
using System.Net;
using Common.Logging;
using Domain.AuditLeadlog;
using Domain.Imports.Leads.Dtos;
using Domain.Imports.Leads.Messages;
using Infrastructure.Configuration;
using RestSharp;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;
using Shared;
namespace Domain.Imports.Leads.Consumers
{
    public class UpdateLeadImportMessageConsumer : AbstractMessageConsumer<UpdateLeadImportMessage>
    {
        private readonly string _baseUrl;
        private readonly RestClient _client;
        private readonly string _token;
        private static readonly ILog Log = LogManager.GetLogger<UpdateLeadImportMessage>();
        public UpdateLeadImportMessageConsumer(IWorkflowRouter router,IWorkflowExecutor executor)
            :base(router,executor)
        {
            _baseUrl = new AdminConfigurationReader().BaseUrl;

            _client = new RestClient(_baseUrl);
            _token = new AuditLeadLogAuthentication().Authenticate().Token;
        }
  
        public override void Consume(UpdateLeadImportMessage message)
        {
            this.Info(() => string.Format("Logging to Lead import status: {0} by lead reference {1}", message.IndividualUploadDetailEventDto.LeadImportStatus,
             message.IndividualUploadDetailEventDto.LeadImportReference));
            try
            {
                IRestRequest request = CreateRequest<IndividualUploadDetailEventDto>("/import/lead/ipl", message.IndividualUploadDetailEventDto);
                Log.InfoFormat("Sending individual upload detail to iAdmin api on url {0}", _baseUrl);
                IRestResponse response = _client.Execute<IndividualUploadDetailEventDto>(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    this.Info(
                        () =>
                            string.Format("Successfully Lead Import Status: {0} by lead reference {1}",
                                message.IndividualUploadDetailEventDto.LeadImportStatus,
                                message.IndividualUploadDetailEventDto.LeadImportReference));
                }
                else
                {
                    this.Error(
                        () =>
                            string.Format("Logging to Lead Import Status: {0} by LeadId {1}",
                                message.IndividualUploadDetailEventDto.LeadImportStatus,
                                message.IndividualUploadDetailEventDto.LeadImportReference));
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Could not import lead reference number '{0}'. Exception details: {1}",
                    message.IndividualUploadDetailEventDto.LeadImportReference,

                    new ExceptionPrettyPrinter().Print(ex));
                Log.ErrorFormat("AuditLeadLog Transfer Error: {0}", error);
            }
        }
        
        #region [ Helpers ]
        private IRestRequest CreateRequest<T>(string queryString, T dto)
        {
            RestRequest request = new RestRequest(queryString)
            {
                Method = Method.PUT,
                RequestFormat = DataFormat.Json
            };
            request.AddParameter("Authorization", _token, ParameterType.HttpHeader);
            request.AddBody(dto);

            return request;
        }
        #endregion
    }
}
