﻿using System;
using System.Net;
using Common.Logging;
using Domain.AuditLeadlog;
using Domain.Imports.Leads.Dtos;
using Domain.Imports.Leads.Messages;
using Infrastructure.Configuration;
using RestSharp;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;
using Shared;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Enums;

namespace Domain.Imports.Leads.Consumers
{
    public class UpdateIndividualUploadDetailMessageConsumer: AbstractMessageConsumer<UpdateIndividualUploadDetailMessage>
    {
        private readonly IConnector _connector;

        public UpdateIndividualUploadDetailMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
        }

        public override void Consume(UpdateIndividualUploadDetailMessage message)
        {
            var dto = new EditIndividualUploadDetailDto
            {
                Comment = message.IndividualUploadDetailEventDto.Comment,
                LeadImportReference = message.IndividualUploadDetailEventDto.LeadImportReference,
                LeadImportStatus = message.IndividualUploadDetailEventDto.LeadImportStatus,
                PartyId = message.IndividualUploadDetailEventDto.PartyId
            };

            if (_connector != null)
            {
                this.Info(() => "Saving IndividualUploadDetail");

                var result = _connector.ImportManagement.Lead.EditIndividualUploadDetail(dto);
                if (result.IsSuccess)
                {
                    this.Info(
                        () =>
                            string.Format("Successful Saving IndividualUploadDetail {0}",
                                message.IndividualUploadDetailEventDto.LeadImportReference.ToString()));
                }
                else { 
                    this.Error(
                        () =>
                            string.Format("Error Saving IndividualUploadDetail for LeadImportReference {0}",
                                dto.LeadImportReference.ToString()));
        }
            }
            else
            {
                this.Error(
                    () =>
                        "The connector to set the token is null");
            }
        }
    }
}
