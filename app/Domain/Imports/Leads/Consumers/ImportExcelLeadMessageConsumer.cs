﻿using Common.Logging;
using Domain.Imports.Leads.Workflow.Messages;
using Domain.Imports.Leads.Workflow.Metadata;
using Infrastructure.Configuration;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Imports.Leads.Consumers
{
    public class ImportExcelLeadMessageConsumer : AbstractMessageConsumer<ImportExcelLeadMessage>
    {
        private readonly ILog _log;
        private readonly IConnector _connector;
        public ImportExcelLeadMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
            _log = LogManager.GetLogger(typeof(ImportExcelLeadMessageConsumer));
        }

        public override void Consume(ImportExcelLeadMessage message)
        {
            var createMetadata = message.Metadata as CreateLeadMetadata;
            if (createMetadata != null)
                _connector.IndividualManagement.Individuals.CreateIndividual(createMetadata.CreateIndividualDto);

            var editMetadata = message.Metadata as EditLeadMetadata;
            if (editMetadata != null)
                _connector.IndividualManagement.Individual(editMetadata.EditIndividualDto.Id).EditIndividual(editMetadata.EditIndividualDto);
        }
    }
}