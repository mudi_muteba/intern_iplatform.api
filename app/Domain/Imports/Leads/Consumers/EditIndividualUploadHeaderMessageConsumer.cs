﻿using Common.Logging;
using Domain.Imports.Leads.Workflow.Messages;
using Domain.Imports.Leads.Workflow.Metadata;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Leads;
using Workflow.Domain;
using Workflow.Messages;
using Infrastructure.Configuration;
using iPlatform.Enums;

namespace Domain.Imports.Leads.Consumers
{
    public class EditIndividualUploadHeaderMessageConsumer : AbstractMessageConsumer<EditIndividualUploadHeaderMessage>
    {
        private readonly ILog _log;
        private readonly IConnector _connector;
        public EditIndividualUploadHeaderMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
            _log = LogManager.GetLogger(GetType());
        }

        public override void Consume(EditIndividualUploadHeaderMessage message)
        {
            var metadata = message.Metadata as EditIndividualUploadHeaderMetadata;
            if (metadata == null)
            {
                _log.WarnFormat("Unable to cast ITaskMetadata as {0}", typeof(EditIndividualUploadHeaderMetadata).ToString());
                return;
            }

            var response = _connector.ImportManagement.Lead.EditIndividualUploadHeader(new EditIndividualUploadHeaderDto(metadata.LeadImportReference));
        }
    }
}