﻿using System.Net;
using Domain.Imports.Leads.Messages;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums;
using Infrastructure.Configuration;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Imports.Leads.Consumers
{
    public class SubmitLeadExecutionMessageConsumer : AbstractMessageConsumer<SubmitLeadExecutionMessage>
    {
        private readonly IConnector _connector;

        public SubmitLeadExecutionMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
        }

        public override void Consume(SubmitLeadExecutionMessage message)
        {
            var metaData = (SubmitLeadTaskMetaData) message.Metadata;
            //import lead
            var leadResponse = _connector.LeadManagement.Lead.ImportLead(metaData.SubmitLeadDto);
            if (leadResponse.StatusCode == HttpStatusCode.Created)
            {
                this.Info(() => string.Format("Lead imported  having Id : {0}", leadResponse.Response.LeadId));

                //import lead proposal
                var proposalResponse = _connector.ProposalManagement.Proposals.ImportProposals(metaData.SubmitLeadDto);
                if (proposalResponse.StatusCode == HttpStatusCode.Created)
                    this.Info(() => string.Format("Successfully imported Proposal : {0}", proposalResponse.Response));
                foreach (var error in leadResponse.Errors)
                    this.Error(() => string.Format("Import lead proposal Failure: {0}", error));
            }
            else
                foreach (var error in leadResponse.Errors)
                    this.Error(() => string.Format("Import Lead Failure: {0}", error));
        }
    }
}
