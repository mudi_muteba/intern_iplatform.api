﻿using System;
using Domain.Base.Events;
using Domain.Imports.Leads.Events;
using Domain.Imports.Leads.Messages;
using Workflow.Messages;

namespace Domain.Imports.Leads.EventHandlers
{
    public class UpdateLeadImportEventHandler : BaseEventHandler<UpdateLeadImportEvent>
    {
        private readonly IWorkflowRouter _router;
        public UpdateLeadImportEventHandler(IWorkflowRouter router)
        {
            _router = router;
        }
        public override void Handle(UpdateLeadImportEvent @event)
        {
            _router.Publish(new WorkflowRoutingMessage().AddMessage(
                new UpdateLeadImportMessage(@event.IndividualUploadDetailEventDto)));
        }
    }
}
