﻿using System;
using Domain.Base.Events;
using Domain.Imports.Leads.Events;
using Domain.Imports.Leads.Messages;
using Workflow.Messages;
using Domain.Base.Repository;
using Shared.Extentions;
using System.Linq;
namespace Domain.Imports.Leads.EventHandlers
{
    public class UpdateIndividualUploadDetailEventHandler : BaseEventHandler<UpdateIndividualUploadDetailEvent>
    {
        private readonly IWorkflowRouter _router;
        private readonly IRepository _repository;
        public UpdateIndividualUploadDetailEventHandler(IWorkflowRouter router, IRepository repository)
        {
            _router = router;
            _repository = repository;
        }
        public override void Handle(UpdateIndividualUploadDetailEvent @event)
        {
            //Guid is empty, not raise event
            if (@event.IndividualUploadDetailEventDto.LeadImportReference.IsEmpty())
                return;

            // guid does not match any record
            var exists = _repository.GetAll<IndividualUploadDetail>().Any(x => x.LeadImportReference.Equals(@event.IndividualUploadDetailEventDto.LeadImportReference));

            if(exists)
                _router.Publish(new WorkflowRoutingMessage().AddMessage(
                    new UpdateIndividualUploadDetailMessage(@event.IndividualUploadDetailEventDto)));
        }
    }
}
