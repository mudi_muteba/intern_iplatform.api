﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Events;
using Domain.Imports.Leads.Dtos;
using iPlatform.Api.DTOs.Leads;
using MasterData;

namespace Domain.Imports.Leads.Events
{
    public class UpdateIndividualUploadDetailEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public UpdateIndividualUploadDetailEvent(IndividualUploadDetailEventDto individualUploadDetailEventDto)
        {
            IndividualUploadDetailEventDto = individualUploadDetailEventDto;
        }

        public IndividualUploadDetailEventDto IndividualUploadDetailEventDto { get; set; }
    }
}
