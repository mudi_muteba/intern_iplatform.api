﻿using AutoMapper;
using Common.Logging;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Imports.Leads.Workflow.Metadata;
using Domain.Users;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Leads;

namespace Domain.Imports.Leads.Mappings
{
    public class IndividualUploadDetailMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<IndividualUploadDetailDto, IndividualUploadDetail>()
                .ForMember(d => d.Channel, opt => opt.MapFrom(x => Mapper.Map<int, Channel>(x.ChannelId)))
                .ForMember(d => d.Campaign, opt => opt.MapFrom(x => Mapper.Map<int, Campaign>(x.CampaignId)))
                .ForMember(d => d.CreatedBy, opt => opt.MapFrom(x => Mapper.Map<int, User>(x.Context.UserId)))
                .ForMember(d => d.LeadAssigned, opt => opt.MapFrom(x => false))
                .AfterMap((s, d) =>
                {
                    if (d == null)
                        LogManager.GetLogger(GetType())
                            .WarnFormat("Unable to map {0} to {1}", typeof(IndividualUploadDetailDto).ToString(), typeof(IndividualUploadDetail).ToString());
                });

            Mapper.CreateMap<IndividualUploadDetail, IndividualUploadDetailDto>();
            Mapper.CreateMap<PagedResults<IndividualUploadDetail>, PagedResultDto<IndividualUploadDetailDto>>();
            Mapper.CreateMap<CreateIndividualUploadDetailMetadata,IndividualUploadDetailDto > ();

            Mapper.CreateMap<IndividualUploadDetailDto, CreateIndividualUploadDetailMetadata>();
            Mapper.CreateMap<ImportLeadDto, IndividualUploadDetailDto>()
                .ForMember(t => t.ContactNumber, o => o.MapFrom(s => s.InsuredInfo.ContactNumber));

            Mapper.CreateMap<ImportLeadDto, IndividualUploadDetail>()
                .ForMember(t => t.ContactNumber, o => o.MapFrom(s => s.InsuredInfo.ContactNumber))
                .ForMember(d => d.Channel, opt => opt.MapFrom(x => Mapper.Map<int, Channel>(x.ChannelId)))
                .ForMember(d => d.Campaign, opt => opt.MapFrom(x => Mapper.Map<int, Campaign>(x.CampaignId)))
                .ForMember(d => d.CreatedBy, opt => opt.MapFrom(x => Mapper.Map<int, User>(x.Context.UserId)));

            
            Mapper.CreateMap<ImportLeadDto, EditIndividualUploadDetailDto>()
                .ForMember(t => t.DuplicateFileRecordCount, o => o.Ignore())
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.LeadImportReference, o => o.MapFrom(s => s.LeadImportReference))
                ;

            Mapper.CreateMap<EditIndividualUploadDetailDto, EditIndividualUploadDetailMetadata>();
            Mapper.CreateMap<EditIndividualUploadDetailMetadata, EditIndividualUploadDetailDto>();

            Mapper.CreateMap<EditIndividualUploadDetailDto, IndividualUploadDetail>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.LeadImportReference, o => o.Ignore())
                .ForMember(t => t.CreatedBy, o => o.Ignore())
                .ForMember(t => t.DateCreated, o => o.Ignore())
                .ForMember(t => t.FileName, o => o.Ignore())
                .ForMember(t => t.Campaign, o => o.Ignore())
                .ForMember(t => t.Channel, o => o.Ignore())
                .ForMember(t => t.ContactNumber, o => o.Ignore())
                ;

        }
    }
}