﻿using AutoMapper;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Occupation;
using iPlatform.Api.DTOs.Party.ContactDetail;
using iPlatform.Api.DTOs.Ratings.Request;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;


namespace Domain.Imports.Leads.Mappings
{
    public class LeadIndividualMapper : Profile
    {
        const string dateFormat = "dd MMM yyyy";
        CultureInfo provider = CultureInfo.InvariantCulture;

        protected override void Configure()
        {
            Mapper.CreateMap<ImportLeadDto, CreateIndividualDto>()
                .ForMember(d => d.Title, opt => opt.MapFrom(x => x.InsuredInfo.Title))
                .ForMember(d => d.FirstName, opt => opt.MapFrom(x => x.InsuredInfo.Firstname))
                .ForMember(d => d.Surname, opt => opt.MapFrom(x => x.InsuredInfo.Surname))
                .ForMember(d => d.IdentityNo, opt => opt.MapFrom(x => x.InsuredInfo.IDNumber))
                .ForMember(d => d.MaritalStatus, opt => opt.MapFrom(x => x.InsuredInfo.MaritalStatus))
                .ForMember(d => d.Gender, opt => opt.MapFrom(s => s.InsuredInfo.Gender))
                .ForMember(d => d.ChannelId, opt => opt.MapFrom(s => s.ChannelId))
                .ForMember(d => d.ContactDetail,
                    opt =>
                        opt.MapFrom(
                            x =>
                                new CreateContactDetailDto
                                {
                                    Cell = x.InsuredInfo.ContactNumber,
                                    Email = x.InsuredInfo.EmailAddress
                             
                                }))
                .ForMember(t => t.DateOfBirth, o => o.MapFrom(s => s.InsuredInfo.DateOfBirth != null ? DateTime.SpecifyKind(s.InsuredInfo.DateOfBirth.Value, DateTimeKind.Utc): s.InsuredInfo.DateOfBirth))
                .AfterMap((s,d) => {
                    RatingRequestPersonDto person = null;
                    if (s.Request != null)
                        person = s.Request.Policy.Persons.FirstOrDefault();

                    d.Campaigns = new List<CampaignInfoDto> { new CampaignInfoDto { Id = s.CampaignId } };
                    if(person != null)
                    {
                        d.DisplayName = person.PreferredName;
                        d.Occupation = person.OccupationId != 0 ? new OccupationDto { Id = person.OccupationId } : null;
                        d.ContactDetail.Direct = person.PhoneDirect;
                        d.ContactDetail.Fax = person.PhoneFax;
                        d.ContactDetail.Home = person.PhoneHome;
                        d.ContactDetail.Work = person.PhoneWork;
                        d.Language = person.Language;
                        d.LeadExternalReference = s.LeadImportReference.ToString();
                        d.DateOfBirth = person.DateOfBirth != null ? DateTime.SpecifyKind(person.DateOfBirth.Value, DateTimeKind.Utc): person.DateOfBirth; 
                        d.Title = person.Title;
                        d.Language = person.Language;
                        d.MaritalStatus = person.MaritalStatus;
                    }
                    
                });

            Mapper.CreateMap<ImportLeadDto, EditIndividualDto>()
                .ForMember(d => d.Title, opt => opt.MapFrom(x => x.InsuredInfo.Title))
                .ForMember(d => d.FirstName, opt => opt.MapFrom(x => x.InsuredInfo.Firstname))
                .ForMember(d => d.Surname, opt => opt.MapFrom(x => x.InsuredInfo.Surname))
                .ForMember(d => d.IdentityNo, opt => opt.MapFrom(x => x.InsuredInfo.IDNumber))
                .ForMember(d => d.MaritalStatus, opt => opt.MapFrom(x => x.InsuredInfo.MaritalStatus))
                .ForMember(d => d.ChannelId, opt => opt.MapFrom(s => s.ChannelId))
                .ForMember(d => d.ContactDetail,
                    opt =>
                        opt.MapFrom(
                            x =>
                                new ContactDetailDto
                                {
                                    Cell = x.InsuredInfo.ContactNumber,
                                    Email = x.InsuredInfo.EmailAddress
                                }))
                .ForMember(t => t.DateOfBirth, o => o.MapFrom(s => s.InsuredInfo.DateOfBirth != null ? DateTime.SpecifyKind(s.InsuredInfo.DateOfBirth.Value, DateTimeKind.Utc) : s.InsuredInfo.DateOfBirth))
                .AfterMap((s, d) => {
                    RatingRequestPersonDto person = null;
                    if (s.Request != null)
                        person = s.Request.Policy.Persons.FirstOrDefault();

                    d.Campaigns = new List<CampaignInfoDto> { new CampaignInfoDto { Id = s.CampaignId } };

                    if (person != null)
                    {
                        d.Occupation = person.OccupationId != 0 ? new OccupationDto { Id = person.OccupationId } : null;
                        d.ContactDetail.Direct = person.PhoneDirect;
                        d.ContactDetail.Fax = person.PhoneFax;
                        d.ContactDetail.Home = person.PhoneHome;
                        d.ContactDetail.Work = person.PhoneWork;
                        d.Language = person.Language;
                        d.DateOfBirth = person.DateOfBirth != null ? DateTime.SpecifyKind(person.DateOfBirth.Value, DateTimeKind.Utc) : person.DateOfBirth;
                        d.Title = person.Title;
                        d.Language = person.Language;
                        d.MaritalStatus = person.MaritalStatus;
                    }
                });
        }
    }
}