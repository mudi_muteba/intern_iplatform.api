﻿using AutoMapper;
using iPlatform.Api.DTOs.Leads;
using Workflow.Messages;

namespace Domain.Imports.Leads.Mappings
{
    public class LeadWorkflowMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ImportLeadDto, IWorkflowExecutionMessage>()
                .ConvertUsing<ITypeConverter<ImportLeadDto, IWorkflowExecutionMessage>>();
        }
    }
}