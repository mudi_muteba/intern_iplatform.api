﻿using AutoMapper;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Users;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Excel;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using System;
using System.Linq;
using iPlatform.Api.DTOs.Sales;
using Domain.Admin;

namespace Domain.Imports.Leads.Mappings
{
    public class LeadImportMapper : Profile
    {
        

        protected override void Configure()
        {
            Mapper.CreateMap<LeadImport, ListLeadImportDto>();

            Mapper.CreateMap<PagedResults<LeadImport>, PagedResultDto<ListLeadImportDto>>();
                

            Mapper.CreateMap<LeadImportExcelDto, LeadImport>()
                .ForMember(t => t.CreatedBy, o => o.MapFrom(s => new User(s.Context.UserId)))
                .ForMember(t => t.Campaign, o => o.MapFrom(s => new Campaign() { Id = s.CampaignId }))
                .ForMember(t => t.TotalRecords, o => o.MapFrom(s => s.Leads.Count()))
                ;

            Mapper.CreateMap<LeadImportDto, InsuredInfoDto>()
                .ForMember(t => t.Firstname, o => o.MapFrom(s =>s.Client.FirstName.ToLower()))
                .ForMember(t => t.Surname, o => o.MapFrom(s => s.Client.Lastname.ToLower()))
                .ForMember(t => t.IDNumber, o => o.MapFrom(s => s.Client.IDNumber.ToLower()))
                .ForMember(t => t.MaritalStatus, o => o.MapFrom(s => GetMaritalStatus(s.Client.clnMaritalStatus)))
                .ForMember(t => t.ContactNumber, o => o.MapFrom(s => s.Client.ContactNumber.ToLower()))
                .ForMember(t => t.EmailAddress, o => o.MapFrom(s => s.Client.EmailAddress.ToLower()))
                .ForMember(t => t.ITCConsent, o => o.MapFrom(s => false))
                .ForMember(t => t.Gender, o => o.MapFrom(s => GetGender(s.Client.Gender)))
                .ForMember(t => t.Title, o => o.MapFrom(s => GetTitle(s.Client.Title, s.Client.Gender, s.Client.clnMaritalStatus)))
                ;

            Mapper.CreateMap<LeadImportDto, SalesFNIDto>()
               .ForMember(t => t.UserName, o => o.MapFrom(s => s.User.UserName.ToLower()))
               .ForMember(t => t.UserWorkTelephoneCode, o => o.MapFrom(s => s.User.UserWorkTelephoneCode.ToLower()))
               .ForMember(t => t.UserWorkTelephoneNumber, o => o.MapFrom(s => s.User.UserWorkTelephoneNumber.ToLower()))
               .ForMember(t => t.UserEmailAddress, o => o.MapFrom(s => s.User.UserEmailAddress.ToLower()))
               .ForMember(t => t.UserFaxCode, o => o.MapFrom(s => s.User.UserFaxCode.ToLower()))
               .ForMember(t => t.UserFaxNumber, o => o.MapFrom(s => s.User.UserFaxNumber.ToLower()))
               .ForMember(t => t.EmployeeNumber, o => o.MapFrom(s => s.User.EmployeeNumber.ToLower()))
               .ForMember(t => t.FiFirstName, o => o.MapFrom(s => s.User.FIFirstName.ToLower()))
               .ForMember(t => t.FiLastName, o => o.MapFrom(s => s.User.FILastName.ToLower()))
               .ForMember(t => t.FiidNumber, o => o.MapFrom(s => s.User.FIIDNumber.ToLower()))
               ;

            Mapper.CreateMap<LeadImportDto, DealDto>()
               .ForMember(t => t.SystemMode, o => o.MapFrom(s => s.Deal.SystemMode.ToLower()))
               .ForMember(t => t.CurrentDate, o => o.MapFrom(s => s.Deal.CurrentDate.ToLower()))
               .ForMember(t => t.PolicyNumber, o => o.MapFrom(s => s.Deal.PolicyNumber.ToLower()))
               .ForMember(t => t.BranchCode, o => o.MapFrom(s => s.Deal.BranchCode))
               .ForMember(t => t.BranchName, o => o.MapFrom(s => s.Deal.BranchName.ToLower()))
               .ForMember(t => t.FinanceCompanyCode, o => o.MapFrom(s => s.Deal.FinanceCompanyCode))
               .ForMember(t => t.FinanceCompanyName, o => o.MapFrom(s => s.Deal.FinanceCompanyName.ToLower()))
               .ForMember(t => t.GroupCode, o => o.MapFrom(s => s.Deal.GroupCode))
               .ForMember(t => t.GroupName, o => o.MapFrom(s => s.Deal.GroupName.ToLower()))
               .ForMember(t => t.CompanyCode, o => o.MapFrom(s => s.Deal.CompanyCode))
               ;

            Mapper.CreateMap<LeadImportDto, RatingRequestDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => Guid.NewGuid()))
                .ForMember(t => t.Items, o => o.Ignore())
                .AfterMap((s,d) => {
                    d.Policy = Mapper.Map<RatingRequestPolicyDto>(s);
                });

            Mapper.CreateMap<LeadImportDto, RatingRequestPolicyDto>()
                .ForMember(t => t.PaymentPlan, o => o.MapFrom(s => PaymentPlans.Monthly))
                .ForMember(t => t.Persons, o => o.Ignore())
                ;

            Mapper.CreateMap<LeadImportDto, RatingRequestPersonDto>()
                .ForMember(t => t.FirstName, o => o.MapFrom(s => s.Client.FirstName.ToLower()))
                .ForMember(t => t.Surname, o => o.MapFrom(s => s.Client.Lastname.ToLower()))
                .ForMember(t => t.Initials, o => o.MapFrom(s => s.Client.Initials))
                .ForMember(t => t.MaritalStatus, o => o.MapFrom(s => GetMaritalStatus(s.Client.clnMaritalStatus)))
                .ForMember(t => t.IdNumber, o => o.MapFrom(s => s.Client.IDNumber))
                .ForMember(t => t.Email, o => o.MapFrom(s => s.Client.EmailAddress))
                .ForMember(t => t.PhoneCell, o => o.MapFrom(s => s.Client.MobileNumber))
                .ForMember(t => t.PhoneDirect, o => o.MapFrom(s => s.Client.HomeTelephoneNumber))
                .ForMember(t => t.PhoneWork, o => o.MapFrom(s => s.Client.WorkTelephoneNumber))
                .ForMember(t => t.Occupation, o => o.MapFrom(s => s.Client.empOccupation))
                .ForMember(t => t.DateOfBirth, o => o.MapFrom(s => Convert.ToDateTime(s.Client.BirthDate)))

                .ForMember(t => t.Language, o => o.MapFrom(s => 
                    new Languages().FirstOrDefault(x => String.Equals(x.Name, 
                        s.Client.clnCorrespondenceLanguage, StringComparison.CurrentCultureIgnoreCase))))

                .ForMember(t => t.Title, o => o.MapFrom(s => GetTitle(s.Client.Title, s.Client.Gender,
                                    s.Client.clnMaritalStatus)))

                .ForMember(t => t.PolicyHolder, o => o.MapFrom(s => false))
                .ForMember(t => t.Addresses, o => o.Ignore())
                ;

            Mapper.CreateMap<LeadImportDto, RatingRequestAddressDto>()
                .ForMember(t => t.Address1, o => o.MapFrom(s => s.Client.PhysicalAddress1))
                .ForMember(t => t.PostalCode, o => o.MapFrom(s => s.Client.PostCode))
                .ForMember(t => t.Suburb, o => o.MapFrom(s => string.IsNullOrEmpty(s.Client.Suburb)? s.Client.Suburb1: s.Client.Suburb))
                ;

            Mapper.CreateMap<ImportLeadDto, CreateSalesDetailsDto>()
                .ForMember(t => t.SalesDetails, o => o.Ignore())
                .ForMember(t => t.IdNumber, o => o.MapFrom(s => s.Request.Policy.Persons.FirstOrDefault().IdNumber))
                .ForMember(t => t.PolicyNumber, o => o.MapFrom(s => s.DealDto.PolicyNumber))
                .ForMember(t => t.CurrentDate, o => o.MapFrom(s => s.DealDto.CurrentDate))
                .ForMember(t => t.SalesFNIId, o => o.Ignore())
                .ForMember(t => t.LeadId, o => o.Ignore())
                .ForMember(t => t.SaleStructureId, o => o.Ignore())
                ;

        }
        private MaritalStatus GetMaritalStatus(string given)
        {
            MaritalStatus result =
                new MaritalStatuses().FirstOrDefault(
                    x => String.Equals(x.Name, given, StringComparison.CurrentCultureIgnoreCase));
            if (result == null)
            {
                result = MaritalStatuses.Unknown;
            }
            return result;
        }

        private Title GetTitle(string given, string givenGender, string givenMaritalStatus)
        {
            Title result =
                new Titles().FirstOrDefault(x => String.Equals(x.Name, given, StringComparison.CurrentCultureIgnoreCase));
            if (result == null)
            {
                Gender gender = GetGender(givenGender);
                MaritalStatus martitalStatus = GetMaritalStatus(givenMaritalStatus);
                if (gender != Genders.Unknown)
                {
                    if (gender.Equals(Genders.Male))
                        result = Titles.Mr;
                    else
                    {
                        result = Titles.Mrs;
                        if (martitalStatus.Equals(MaritalStatuses.Single))
                            result = Titles.Miss;
                    }
                }
            }
            return result;
        }

        private Gender GetGender(string given)
        {
            Gender result =
                new Genders().FirstOrDefault(
                    x => String.Equals(x.Name, given, StringComparison.CurrentCultureIgnoreCase));
            if (result == null)
                result = Genders.Unknown;
            return result;
        }

    }
} 