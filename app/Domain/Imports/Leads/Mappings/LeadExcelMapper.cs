﻿using AutoMapper;
using Domain.Imports.Leads.Dtos;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Ratings.Quoting;

namespace Domain.Imports.Leads.Mappings
{
    public class LeadExcelMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ExcelLeadRecord, ImportLeadDto>()
                .ForMember(d => d.InsuredInfo, opt => opt.MapFrom(x => Mapper.Map<ExcelLeadRecord, InsuredInfoDto>(x)));

            Mapper.CreateMap<ExcelLeadRecord, InsuredInfoDto>();
        }
    }
}