﻿using AutoMapper;
using Common.Logging;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Users;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Leads;

namespace Domain.Imports.Leads.Mappings
{
    public class IndividualUploadHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateIndividualUploadHeaderDto, IndividualUploadHeader>()
                .ForMember(d => d.Channel, opt => opt.MapFrom(x => Mapper.Map<int, Channel>(x.ChannelId)))
                .ForMember(d => d.Campaign, opt => opt.MapFrom(x => Mapper.Map<int, Campaign>(x.CampaignId)))
                .ForMember(d => d.CreatedBy, opt => opt.MapFrom(x => Mapper.Map<int, User>(x.UserId)))
                .ForMember(d => d.LeadImportStatus, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (d == null)
                        LogManager.GetLogger(GetType())
                            .WarnFormat("Unable to map {0} to {1}", typeof(CreateIndividualUploadHeaderDto).ToString(), typeof(IndividualUploadHeader).ToString());
                });

            Mapper.CreateMap<IndividualUploadHeader, CreateIndividualUploadHeaderDto>();
            Mapper.CreateMap<PagedResults<IndividualUploadHeader>, PagedResultDto<CreateIndividualUploadHeaderDto>>();
        }
    }
}