﻿using AutoMapper;
using Domain.Leads;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Excel;
using iPlatform.Api.DTOs.Party.ContactDetail;

namespace Domain.Imports.Leads.Mappings
{
    public class LeadMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Lead, ImportLeadDto>();

            Mapper.CreateMap<ImportLeadDto, Lead>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Activities, o => o.Ignore())
                ;

            Mapper.CreateMap<LeadImportExcelRowDto, CreateIndividualDto>()
                .ForMember(d => d.FirstName, opt => opt.MapFrom(x => x.Cell[0]))
                .ForMember(d => d.Surname, opt => opt.MapFrom(x => x.Cell[1]))
                .ForMember(d => d.IdentityNo, opt => opt.Ignore())
                .ForMember(d => d.MaritalStatus, opt => opt.Ignore())
                .ForMember(d => d.ContactDetail, opt => opt.MapFrom(x => new ContactDetailDto {Cell = x.Cell[2]}))
                .ForMember(d => d.DateOfBirth, opt => opt.Ignore());

            Mapper.CreateMap<CreateLeadDto, Lead>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Activities, o => o.Ignore())
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party.Party{ Id = s.PartyId}))
                ;

        }
    }
}