﻿using System.Linq;
using AutoMapper;
using Domain.Imports.Leads.Workflow.Messages;
using Domain.Imports.Leads.Workflow.Metadata;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.ContactDetail;
using Workflow.Messages;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Sales;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Ratings.Request;
using Domain.Base.Repository;
using Domain.Individuals;

namespace Domain.Imports.Leads.Mappings
{
    public class LeadMessageConverter : TypeConverter<ImportLeadDto, IWorkflowExecutionMessage>
    {
        private readonly GetIndividualBySearch m_IndividualQuery;
        private readonly IRepository m_Repository;
        public LeadMessageConverter(GetIndividualBySearch individualQuery, IRepository repository)
        {
            m_IndividualQuery = individualQuery;
            m_Repository = repository;
        }

        protected override IWorkflowExecutionMessage ConvertCore(ImportLeadDto source)
        {
            var individuals = m_Repository.GetAll<Individual>().Where(x => !x.IsDeleted && x.IdentityNo == source.InsuredInfo.IDNumber && x.Channel.Id == source.ChannelId).ToList();

            var addresses = new List<CreateAddressDto>();
            if (source.Request != null)
            {
                var person = source.Request.Policy.Persons.FirstOrDefault();
                addresses = Mapper.Map<List<RatingRequestAddressDto>, List<CreateAddressDto>>(person.Addresses);
            }

            var createDto = Mapper.Map<ImportLeadDto, CreateIndividualDto>(source);
            var editDto = Mapper.Map<ImportLeadDto, EditIndividualDto>(source);
            var createSalesFNIDto = Mapper.Map<ImportLeadDto, CreateSalesFNIDto>(source);
            var importSalesStructureDto = Mapper.Map<ImportSalesStructureDto>(source);
            var editIndividualUploadDetailDto = Mapper.Map<ImportLeadDto, EditIndividualUploadDetailDto>(source);
            var createSalesDetailsDto = Mapper.Map<CreateSalesDetailsDto>(source);

            editIndividualUploadDetailDto.NewIndividual = !individuals.Any();
            editDto.Id = editIndividualUploadDetailDto.NewIndividual ? 0 : individuals.First().Id;
            editIndividualUploadDetailDto.DuplicateFileRecordCount = editIndividualUploadDetailDto.NewIndividual ? 0 : 1;

            var message = editIndividualUploadDetailDto.NewIndividual ? 
                new CreateLeadMessage(new CreateLeadMetadata(source, createDto, addresses, createSalesFNIDto, 
                importSalesStructureDto, editIndividualUploadDetailDto, createSalesDetailsDto)) : 
                new EditLeadMessage(new EditLeadMetadata(source, editDto, addresses, createSalesFNIDto, 
                importSalesStructureDto, editIndividualUploadDetailDto, createSalesDetailsDto)) as WorkflowExecutionMessage;

            return message;
        }
    }
}