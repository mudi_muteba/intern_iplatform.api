﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using System;

namespace Domain.Imports.Leads.Queries
{
    public class GetLeadImportQuery : BaseQuery<LeadImport>
    {
        private Guid Reference;

        public GetLeadImportQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<LeadImport>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetLeadImportQuery ByReference(Guid reference)
        {
            this.Reference = reference;
            return this;
        }

        protected internal override IQueryable<LeadImport> Execute(IQueryable<LeadImport> query)
        {
            if (Guid.Empty == Reference)
                return query;

            return query
                .Where(c => c.LeadImportReference == Reference);
        }
    }
}