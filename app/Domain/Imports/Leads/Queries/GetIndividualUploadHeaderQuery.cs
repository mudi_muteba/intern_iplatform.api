﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Imports.Leads.Queries
{
    public class GetIndividualUploadHeaderQuery : BaseQuery<IndividualUploadHeader>
    {
        private Guid _reference;

        public GetIndividualUploadHeaderQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<IndividualUploadHeader>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetIndividualUploadHeaderQuery ByReference(Guid reference)
        {
            _reference = reference;
            return this;
        }

        protected internal override IQueryable<IndividualUploadHeader> Execute(IQueryable<IndividualUploadHeader> query)
        {
            if (Guid.Empty == _reference)
                return query;

            return query
                .Where(c => c.LeadImportReference == _reference);
        }
    }
}