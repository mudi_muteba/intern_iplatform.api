﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Imports.Leads.Queries
{
    public class GetIndividualUploadDetailQuery : BaseQuery<IndividualUploadDetail>
    {
        private Guid m_LeadImportReference;

        public GetIndividualUploadDetailQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<IndividualUploadDetail>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetIndividualUploadDetailQuery WithCriteria(Guid leadImportReference)
        {
            m_LeadImportReference = leadImportReference;
            return this;
        }

        protected internal override IQueryable<IndividualUploadDetail> Execute(IQueryable<IndividualUploadDetail> query)
        {
            return m_LeadImportReference == Guid.Empty
                ? query 
                : query.Where(c => c.LeadImportReference == m_LeadImportReference);
        }
    }
}