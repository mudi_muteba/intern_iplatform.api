﻿using LinqToExcel.Attributes;

namespace Domain.Imports.Leads.Dtos
{
    public class ExcelLeadRecord
    {
        public string Firstname { get; set; }
        public string Surname        { get; set; }
        public string ContactNumber { get; set; }
    }
}