﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData;

namespace Domain.Imports.Leads.Dtos
{
    public class IndividualUploadDetailEventDto
    {
        public LeadImportStatus LeadImportStatus { get; set; }
        public Guid LeadImportReference { get; set; }
        public string Comment { get; set; }
        public int PartyId{ get; set; }
    }
}