﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Leads;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Messages
{
    public class SubmitLeadTaskMetaData : ITaskMetadata
    {
        public SubmitLeadDto SubmitLeadDto { get; set; }


        public SubmitLeadTaskMetaData(SubmitLeadDto dto)
        {
            this.SubmitLeadDto = dto;
        }
    }
}
