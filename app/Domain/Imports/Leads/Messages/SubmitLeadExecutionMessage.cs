﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Messages
{
    public class SubmitLeadExecutionMessage : WorkflowExecutionMessage
    {
        public SubmitLeadExecutionMessage() { }

        public SubmitLeadExecutionMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }

        public SubmitLeadExecutionMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }

    }
}
