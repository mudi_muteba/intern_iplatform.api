﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Imports.Leads.Dtos;
using MasterData;
using Workflow.Messages;

namespace Domain.Imports.Leads.Messages
{
   public class UpdateIndividualUploadDetailMessage : WorkflowExecutionMessage
   {
       public IndividualUploadDetailEventDto IndividualUploadDetailEventDto { get; set; }

       public UpdateIndividualUploadDetailMessage(IndividualUploadDetailEventDto individualUploadDetailEventDto,
           WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
       {
           IndividualUploadDetailEventDto = individualUploadDetailEventDto;
       }
    }
}
