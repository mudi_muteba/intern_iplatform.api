﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.Base.Helper.xml;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.QuestionDefinitions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData.Authorisation;
using MasterData;
using System.Linq;
using System;
using System.Globalization;
using AutoMapper;
using Domain.QuestionDefinitions.Queries;
using Workflow.Messages;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Occupations;
using iPlatform.Api.DTOs.Sales;
using iPlatform.Api.DTOs.Lookups.Address;
using Domain.Admin;
using ValidationMessages.Leads;
using iPlatform.Api.DTOs.Leads.Import;
using Shared.Extentions;

namespace Domain.Imports.Leads.Handlers
{
    public class ImportSeritiLeadHandler : CreationDtoHandler<IndividualUploadDetail, LeadImportXMLDto, Guid>  
    {
        private readonly IWorkflowRouter _router;
        private readonly IRepository _repository;
        private readonly IExecutionPlan _executionPlan;
        readonly GetAllQuestionDefinitionsQuery _query;

        public ImportSeritiLeadHandler(IProvideContext contextProvider, IExecutionPlan executionPlan, GetAllQuestionDefinitionsQuery query, IWorkflowRouter router, IRepository repository)
            : base(contextProvider, repository)
        {
            _query = query;
            _router = router;
            _repository = repository;
            _executionPlan = executionPlan;
        }
        
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.Import
                };
            }
        }

        protected override void EntitySaved(IndividualUploadDetail entity, HandlerResult<Guid> result)
        {
            result.Processed(entity.LeadImportReference);
        }

        protected override IndividualUploadDetail HandleCreation(LeadImportXMLDto dto, HandlerResult<Guid> result)
        {
            var routingMessage = new WorkflowRoutingMessage();
            var importLeadDto = ParseData(dto);

            var entity = Mapper.Map<IndividualUploadDetail>(importLeadDto);

            _repository.Save(entity);
        
            routingMessage.AddMessage(Mapper.Map<ImportLeadDto, IWorkflowExecutionMessage>(importLeadDto));

            _router.Publish(routingMessage);

            return entity.Submitted();
        }

        public ImportLeadDto ParseData(LeadImportXMLDto dto)
        {
            var leadImportDto = XmlHelper.XmlToObject<LeadImportDto>(dto.XMLString);

            var insurerdInfo = Mapper.Map<InsuredInfoDto>(leadImportDto);
            var person = Mapper.Map<RatingRequestPersonDto>(leadImportDto);
            var userInfo = Mapper.Map<SalesFNIDto>(leadImportDto);
            var dealDto = Mapper.Map<DealDto>(leadImportDto);

            var address = GetAddress(leadImportDto);
            person.Addresses.Add(address);

            person.OccupationId = GetOccupation(leadImportDto.Client.empOccupation);
            var request = Mapper.Map<RatingRequestDto>(leadImportDto);
            request.Items.Add(new RatingRequestItemDto(GetAnswers(leadImportDto, _query.ExecuteQuery().ToList()).ToList()));
            request.Policy.Persons.Add(person);

            //if no channel is provided
            dto.ChannelId = GetChannelByCode(leadImportDto.Deal.CompanyCode);

            //if no campaign is provided
            if (dto.CampaignId == 0)
                dto.CampaignId = GetCampaign(leadImportDto.Campaign.Reference, dto.ChannelId).Id;

            userInfo = GetAdditionalUserDataDto(dto.ChannelId, userInfo);

            var importLeadDto = new ImportLeadDto
            {
                CampaignReference = leadImportDto.Campaign.Reference,
                InsuredInfo = insurerdInfo,
                Request = request,
                ChannelId = dto.ChannelId,
                CampaignId = dto.CampaignId,
                LeadImportReference = dto.Reference.IsEmpty() ? Guid.NewGuid() : dto.Reference,
                FileName = string.IsNullOrEmpty(dto.FileName) ? "seriti.xml" : dto.FileName,
                SalesFniRequest = userInfo,
                DealDto = dealDto,
            };

            importLeadDto.SetContext(dto.Context);
            return importLeadDto;
        }

        public SalesFNIDto GetAdditionalUserDataDto(int channelId, SalesFNIDto salesFni)
        {
            salesFni.ChannelId = channelId;
            salesFni.DisplayName = string.Concat(salesFni.FiFirstName, ",", salesFni.FiLastName);
            salesFni.DateCreated = DateTime.UtcNow;

            return salesFni;
        }

        public int GetOccupation(string occ)
        {
            if (occ.Contains('('))
                occ = occ.Split('(').FirstOrDefault();

            var occupations = _repository.GetAll<Occupation>();
            var occupation = occupations.FirstOrDefault(x => x.Name.ToUpper() == occ.ToUpper());
            if (occupation != null)
                return occupation.Id;
            return 0;
        }

        private Campaign GetCampaign(string campaignReference, int channelid)
        {
            Campaign campaign;
            if (string.IsNullOrEmpty(campaignReference))
                campaign= GetCampaignByChannelId(channelid);
            else
            {
                campaign = _repository.GetAll<CampaignReference>().FirstOrDefault(x => x.Reference == campaignReference).Campaign;
                if(campaign == null)
                    campaign = GetCampaignByChannelId(channelid);
            }
            return campaign;
        }

        private Campaign GetCampaignByChannelId(int channelid)
        {
            Campaign campaign;
            var campaigns = _repository.GetAll<Campaign>().Where(x => x.Channel.Id == channelid);
            if (campaigns.Any())
            {
                campaign = campaigns.FirstOrDefault(x => x.DefaultCampaign);
                if (campaign == null)
                    campaign = campaigns.First();
            }
            else
                campaign = _repository.GetAll<Campaign>().First();

            return campaign;
        }

        public IList<RatingRequestItemAnswerDto> GetAnswers(LeadImportDto lead, IList<QuestionDefinition> questions)
        {
            var answers = new List<RatingRequestItemAnswerDto>();

            if (lead.Driver != null)
            {
                QuestionDefinition questionDefintion =
                    questions.FirstOrDefault(x => x.Question.Equals(Questions.MainDriverIDNumber));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Driver.drvIDNumber))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Driver.drvIDNumber));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.MainDriverGender));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Driver.drvGender))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Driver.drvGender));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.MainDriverDateofBirth));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Driver.drvBirthDate))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Driver.drvBirthDate));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.RelationshipToInsured));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Driver.drvRelation))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Driver.drvRelation));
            }

            if (lead.Vehicle != null)
            {
                QuestionDefinition questionDefintion =
                    questions.FirstOrDefault(x => x.Question.Equals(Questions.VehicleExtrasValue));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleAccessoryTotal))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleAccessoryTotal));

                if (!string.IsNullOrEmpty(lead.Vehicle.VehicleRetailPrice))
                    answers.Add(ResolveAnswer(Questions.AIGVehicleValueRetail, lead.Vehicle.VehicleRetailPrice));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.SumInsured));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.TotalSumInsured))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.TotalSumInsured));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.ClassOfUse));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehiclePurpose))
                    answers.Add(ResolveAnswer(questionDefintion.Question, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(lead.Vehicle.VehiclePurpose.ToLower())));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.VehicleMMCode));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleCode))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleCode));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.VehicleMake));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleManufacturer))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleManufacturer));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.VehicleModel));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleModel))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleModel));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.VehicleType));
                if (questionDefintion != null && questionDefintion.Question != null)
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleBodyStyle));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.VehicleRegistrationNumber));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleRegistrationNumber))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleRegistrationNumber));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.VINNumber));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleVINNumber))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleVINNumber));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.EngineNumber));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleEngineNumber))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleEngineNumber));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.VehicleImmobiliser));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleImmobiliser))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleImmobiliser));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.VehicleGearlock));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleGearlock))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleGearlock));

                questionDefintion = questions.FirstOrDefault(x => x.Question.Equals(Questions.VehicleTrackingDevice));
                if (questionDefintion != null && questionDefintion.Question != null &&
                    !string.IsNullOrEmpty(lead.Vehicle.VehicleTrackingDevice))
                    answers.Add(ResolveAnswer(questionDefintion.Question, lead.Vehicle.VehicleTrackingDevice));

                DateTime firstRegistrationDate;
                if(DateTime.TryParse(lead.Vehicle.VehicleFirstRegistrationDate, out firstRegistrationDate))
                    answers.Add(ResolveAnswer(Questions.YearOfManufacture, firstRegistrationDate.Year.ToString()));

            }
            return answers;
        }

        private RatingRequestItemAnswerDto ResolveAnswer(Question question, object givenAnswer)
        {
            var item = new RatingRequestItemAnswerDto
            {
                Question = question,
                QuestionAnswer = null
            };

            if (question.QuestionType.Equals(QuestionTypes.Checkbox))
            {
                try
                {
                    bool bitAnswer;
                    Boolean.TryParse(givenAnswer.ToString(), out bitAnswer);
                    item.QuestionAnswer = bitAnswer.ToString(CultureInfo.InvariantCulture);
                }
                catch (Exception exception)
                {
                    Exception x = exception;
                }
            }
            if (question.QuestionType.Equals(QuestionTypes.Date))
            {
                try
                {
                    DateTime dateAnswer;
                    if (DateTime.TryParse(givenAnswer.ToString(), out dateAnswer))
                        item.QuestionAnswer = dateAnswer.ToString("yyyy/MM/dd hh:mm:ss");
                }
                catch (Exception exception)
                {
                    Exception x = exception;
                }
            }
            if (question.QuestionType.Equals(QuestionTypes.Textbox))
            {
                if (question.Equals(Questions.VehicleRegistrationNumber) || question.Equals(Questions.VehicleMake) ||
                    question.Equals(Questions.VehicleModel))
                    item.QuestionAnswer = givenAnswer.ToString().ToUpper();
                else
                    item.QuestionAnswer = givenAnswer.ToString().ToLower();
            }
            if (question.QuestionType.Equals(QuestionTypes.Dropdown))
            {
                IQuestionConverter converter = QuestionConverterFactory.GetConverter(question);
                item.QuestionAnswer = converter.Convert(question, givenAnswer);
            }
            return item;
        }

        private RatingRequestAddressDto GetAddress(LeadImportDto dto)
        {
            var address = Mapper.Map<RatingRequestAddressDto>(dto);

            if (!string.IsNullOrEmpty(address.PostalCode))
            {
                var requestDto = new GetProvinceByPostCodeDto(address.PostalCode, "ZA");

                var result = _executionPlan.Execute<GetProvinceByPostCodeDto, AddressLookupDto>(requestDto);

                if (result.Response.Id != 0)
                {
                    var firstOrDefault = new StateProvinces().FirstOrDefault(x => x.Id == result.Response.Id);
                    if (firstOrDefault != null)
                        address.Province = firstOrDefault.Name;
                }
            }
            return address;
        }

        private int GetChannelByCode(string code)
        {
            var channel = _repository.GetAll<Channel>().FirstOrDefault(x =>  x.Code.ToUpper() == code.ToUpper());
            return channel == null? 0: channel.Id;
        }

    }
}