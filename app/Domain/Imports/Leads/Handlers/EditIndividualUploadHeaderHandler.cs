﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Leads;
using MasterData.Authorisation;
using MasterData;

namespace Domain.Imports.Leads.Handlers
{
    public class EditIndividualUploadHeaderHandler : CreationDtoHandler<IndividualUploadHeader, EditIndividualUploadHeaderDto, int>
    {
        private readonly IRepository _repository;

        public EditIndividualUploadHeaderHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.Import
                };
            }
        }

        protected override void EntitySaved(IndividualUploadHeader entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override IndividualUploadHeader HandleCreation(EditIndividualUploadHeaderDto dto, HandlerResult<int> result)
        {
            var failureCount = _repository.GetAll<IndividualUploadDetail>().Count(x => x.LeadImportReference.Equals(dto.LeadImportReference) && x.LeadImportStatus == LeadImportStatuses.Failure);
            var header = _repository.GetAll<IndividualUploadHeader>().FirstOrDefault(x => x.LeadImportReference.Equals(dto.LeadImportReference));

            return header != null 
                ? header.UpdateFailureCount(failureCount) 
                : null;
        }
    }
}