using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using iPlatform.Api.DTOs.Leads;
using MasterData.Authorisation;
using Domain.Campaigns;
using AutoMapper;
using System;

namespace Domain.Imports.Leads.Handlers
{
    public class EditIndividualUploadDetailHandler : ExistingEntityDtoCustomHandler<IndividualUploadDetail, EditIndividualUploadDetailDto, Guid>
    {
        private readonly IRepository _repository;

        public EditIndividualUploadDetailHandler(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository)
        {
            _repository = repository;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights        
		{
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.Import
                };
            }
        }


        protected override IndividualUploadDetail GetEntity(EditIndividualUploadDetailDto dto)
        {
            return _repository.GetAll<IndividualUploadDetail>()
                .Where(x => x.LeadImportReference == dto.LeadImportReference).FirstOrDefault();
        }

        protected override void HandleExistingEntityChange(IndividualUploadDetail entity, EditIndividualUploadDetailDto dto,
            HandlerResult<Guid> result)
        {
            entity.Update(dto);
            result.Processed(entity.LeadImportReference);
        }
    }
}