﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using iPlatform.Api.DTOs.Leads;
using MasterData.Authorisation;

namespace Domain.Imports.Leads.Handlers
{
    public class CreateIndividualUploadHeaderHandler : CreationDtoHandler<IndividualUploadHeader, CreateIndividualUploadHeaderDto, int>
    {
        private readonly IRepository _repository;
        public CreateIndividualUploadHeaderHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.Import
                };
            }
        }

        protected override void EntitySaved(IndividualUploadHeader entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override IndividualUploadHeader HandleCreation(CreateIndividualUploadHeaderDto dto, HandlerResult<int> result)
        {
            var channelId = dto.ChannelId;
            if (channelId == 0)
            {
                var user = _repository.GetById<User>(dto.Context.UserId);
                var firstOrDefault = user.Channels.FirstOrDefault(a => a.IsDefault);
                if (firstOrDefault != null)
                    dto.ChannelId = firstOrDefault.Id;
            }

            var individualUploadHeader = IndividualUploadHeader.Create(dto);

            result.Processed(individualUploadHeader.Id);

            return individualUploadHeader;
        }
    }
}