﻿using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Leads;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;
using Domain.Imports.Leads.Messages;
using Workflow.Messages;

namespace Domain.Imports.Leads.Handlers
{
    public class SubmitLeadHandler : BaseDtoHandler<SubmitLeadDto, string>
    {
        private readonly ILog _log = LogManager.GetLogger<SubmitLeadHandler>();
        private readonly IWorkflowRouter _router;


        public SubmitLeadHandler(IProvideContext contextProvider, IWorkflowRouter router)
            : base(contextProvider)
        {
            _router = router;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.Import
                };
            }
        }
        protected override void InternalHandle(SubmitLeadDto dto, HandlerResult<string> result)
        {
            _log.InfoFormat("Submitting Lead with Lead External Reference : {0}", dto.ExternalSystemInfo.ExternalReference);
            Handle(dto);
            result.Processed(dto.ExternalSystemInfo.ExternalReference);
        }

        internal void Handle(SubmitLeadDto dto)
        {
            _log.Info("Publishing lead submission message");
            var routingMessage = new WorkflowRoutingMessage();
            routingMessage.AddMessage(new SubmitLeadExecutionMessage(new SubmitLeadTaskMetaData(dto)));
            _router.Publish(routingMessage);
        }
    }
}
