using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using iPlatform.Api.DTOs.Leads;
using MasterData.Authorisation;
using Domain.Campaigns;
using AutoMapper;

namespace Domain.Imports.Leads.Handlers
{
    public class CreateIndividualUploadDetailHandler : CreationDtoHandler<IndividualUploadDetail, IndividualUploadDetailDto, int>
    {
        private readonly IRepository _repository;

        public CreateIndividualUploadDetailHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            _repository = repository;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights        
		{
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.Import
                };
            }
        }


        protected override void EntitySaved(IndividualUploadDetail entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override IndividualUploadDetail HandleCreation(IndividualUploadDetailDto dto, HandlerResult<int> result)
        {
            var channelId = dto.ChannelId;
            if (channelId == 0)
            {
                var user = _repository.GetById<User>(dto.Context.UserId);
                var firstOrDefault = user.Channels.FirstOrDefault(a => a.IsDefault);
                if (firstOrDefault != null)
                    dto.ChannelId = firstOrDefault.Id;
            }

            var entity = IndividualUploadDetail.Create(dto);

            result.Processed(entity.Id);
            return entity;
        }

        private CampaignReference GetCampaignReference(int campaignId)
        {
            var campaign = Mapper.Map<int, Campaign>(campaignId);

            var campaignreference = _repository.GetAll<CampaignReference>().Where(x => x.Campaign.Id == campaignId).FirstOrDefault();
            if (campaignreference != null)
                return campaignreference;

            campaignreference = new CampaignReference { Campaign = campaign, Reference = "Upload" };
            _repository.Save(campaignreference);
            return campaignreference;
        }
    }
}