﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using Castle.Core.Internal;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Imports.Leads.Dtos;
using Domain.Imports.Leads.Workflow.Messages;
using Domain.Imports.Leads.Workflow.Metadata;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Excel;
using LinqToExcel;
using MasterData.Authorisation;
using MoreLinq;
using Workflow.Messages;
using MasterData;

namespace Domain.Imports.Leads.Handlers
{
    public class ImportLeadExcelHandler : BaseDtoHandler<LeadImportExcelDto, Guid>
    {
        private readonly ILog _log;
        private readonly IWorkflowRouter _router;
        private readonly Guid _leadImportReference = Guid.NewGuid();
        
        public ImportLeadExcelHandler(IProvideContext contextProvider, IWorkflowRouter router)
            : base(contextProvider)
        {
            _log = LogManager.GetLogger(GetType());
            _router = router;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    LeadAuthorisation.Import
                };
            }
        }


        protected override void InternalHandle(LeadImportExcelDto dto, HandlerResult<Guid> result)
        {
            Handle(dto);
        }

        internal void Handle(LeadImportExcelDto dto)
        {
            dto.UserId = dto.UserId == 0 ? dto.Context.UserId : dto.UserId;
            var excelImportMetadata = new ExcelImportMetadata(dto.CampaignId, dto.ChannelId, dto.UserId, dto.FileName, Convert.ToBase64String(dto.File), dto.Leads);

            var leadExcelRecords = ExcelLeadRecords(excelImportMetadata);
            leadExcelRecords = leadExcelRecords ?? Enumerable.Empty<ExcelLeadRecord>();
            var excelLeadRecords = leadExcelRecords as IList<ExcelLeadRecord> ?? leadExcelRecords.ToList();
            if (!excelLeadRecords.Any()) return;

            var leadDtos = LeadDtos(excelImportMetadata, excelLeadRecords);
            leadDtos = leadDtos ?? Enumerable.Empty<ImportLeadDto>();
            var leads = leadDtos as IList<ImportLeadDto> ?? leadDtos.ToList();
            if (!leads.Any()) return;

            _router.Publish(WorkflowRoutingMessage(excelImportMetadata, excelLeadRecords, leads));
        }

        internal WorkflowRoutingMessage WorkflowRoutingMessage(ExcelImportMetadata metadata, IEnumerable<ExcelLeadRecord> leadExcelRecords, IEnumerable<ImportLeadDto> leads)
        {
            var messages = WorkflowExecutionMessages(leads);
            var workflowExecutionMessages = messages as IList<IWorkflowExecutionMessage> ?? messages.ToList();
            var headerMessage = IndividualUploadHeaderMessage(metadata, leadExcelRecords, workflowExecutionMessages);

            var routingMessage = new WorkflowRoutingMessage();
            routingMessage.AddMessage(headerMessage);
            routingMessage.AddMessage(workflowExecutionMessages.ToArray());

            return routingMessage;
        }

        internal WorkflowExecutionMessage IndividualUploadHeaderMessage(ExcelImportMetadata metadata, IEnumerable<ExcelLeadRecord> leads, IEnumerable<IWorkflowExecutionMessage> detailMessages)
        {
            var excelLeadRecords = leads as IList<ExcelLeadRecord> ?? leads.ToList();
            var duplicates = DuplicatesExcelLeadRecords(excelLeadRecords);
            var workflowExecutionMessages = detailMessages as IList<IWorkflowExecutionMessage> ?? detailMessages.ToList();
            var newLeads = workflowExecutionMessages.Where(x => x.GetType() == typeof(CreateLeadMessage));
            var existingLeads = workflowExecutionMessages.Where(x => x.GetType() == typeof(EditLeadMessage));

            return new CreateIndividualUploadHeaderMessage(new CreateIndividualUploadHeaderMetadata(_leadImportReference, metadata.ChannelId, metadata.CampaignId, metadata.UserId, metadata.FileName, excelLeadRecords.Count(), newLeads.Count(), existingLeads.Count(), duplicates.Count, null, LeadImportStatuses.Submitted));
        }

        internal IEnumerable<IWorkflowExecutionMessage> WorkflowExecutionMessages(IEnumerable<ImportLeadDto> leads)
        {
            var workflowExecutionMessages = leads.Select(Mapper.Map<ImportLeadDto, IWorkflowExecutionMessage>);
            var executionMessages = workflowExecutionMessages as IList<IWorkflowExecutionMessage> ?? workflowExecutionMessages.ToList();

            var last = executionMessages.LastOrDefault();
            if (last == null) return executionMessages;

            var message = new EditIndividualUploadHeaderMessage(new EditIndividualUploadHeaderMetadata(_leadImportReference));
            last.AddSuccessMessage(message);
            last.AddFailureMessage(message);

            return executionMessages;
        }

        internal Dictionary<string, int> DuplicatesExcelLeadRecords(IEnumerable<ExcelLeadRecord> leads)
        {
            return leads.GroupBy(x => (x.ContactNumber + "").Trim())
                .Where(g => g.Count() > 1)
                .ToDictionary(x => x.Key, y => y.Count());
        }

        internal IEnumerable<ImportLeadDto> LeadDtos(ExcelImportMetadata metadata, IEnumerable<ExcelLeadRecord> leads)
        {
            var excelLeadRecords = leads as IList<ExcelLeadRecord> ?? leads.ToList();
            var duplicates = DuplicatesExcelLeadRecords(excelLeadRecords);
            var leadDtos = Mapper.Map<IEnumerable<ExcelLeadRecord>, IEnumerable<ImportLeadDto>>(excelLeadRecords.DistinctBy(x => x.ContactNumber));
            var dtos = leadDtos as IList<ImportLeadDto> ?? leadDtos.ToList();
            CollectionExtensions.ForEach(dtos, x =>
            {
                x.ChannelId = metadata.ChannelId;
                x.CampaignId = metadata.CampaignId;
                x.UserId = metadata.UserId;
                x.LeadImportReference = _leadImportReference;
                x.FileName = metadata.FileName;
                int value;
                if (x.InsuredInfo != null && duplicates.TryGetValue(x.InsuredInfo.ContactNumber, out value))
                    x.DuplicateFileRecordCount = value;
            });

            if (excelLeadRecords.Any() && !dtos.Any())
                _log.ErrorFormat("Excel lead records were not mapped to dtos for File: {0} ChannelId: {1}", metadata.FileName, metadata.ChannelId);

            return dtos;
        }

        internal IEnumerable<ExcelLeadRecord> ExcelLeadRecords(ExcelImportMetadata metadata)
        {
            var path = Path.Combine(Path.GetTempPath(), string.Format("{0}_{1}", Guid.NewGuid(), metadata.FileName));
            var binaryFile = Convert.FromBase64String(metadata.Base64);
            File.WriteAllBytes(path, binaryFile);
            List<ExcelLeadRecord> leads = new List<ExcelLeadRecord>();

            try
            {
                var excel = new ExcelQueryFactory(path);
                leads = excel.Worksheet<ExcelLeadRecord>().Select(x => x).ToList();
                if (!leads.Any())
                    _log.ErrorFormat("No leads were found for file: {0}, ChannelId: {1}", metadata.FileName, metadata.ChannelId);
            }
            catch
            {
                metadata.Leads.ForEach(x => {
                    if(x.Cell.Count() == 3)
                        leads.Add(new ExcelLeadRecord
                        { Firstname = x.Cell[0], Surname = x.Cell[1], ContactNumber = x.Cell[2] });
                });
            }
            return leads;
        }
    }
}