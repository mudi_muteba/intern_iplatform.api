﻿using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Sales;
using MasterData;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Imports.Leads.Helpers
{
    public static class CreateLeadMessageExtension
    {
        public static void SetAnswer(this List<ProposalQuestionDefinitionDto> questions, List<RatingRequestItemAnswerDto> answers)
        {

            var assetDescription = answers.GetAssetDescription();

            questions.ForEach(q =>
            {

                if (q.DisplayNameTranslationKey == "QUESTION_KEY_MOTOR_ASSET")
                {
                    q.AnswerDto.Answer = assetDescription;
                }

                var answer = answers.FirstOrDefault(a => a.Question.Id == q.Question.Id);

                if (answer != null)
                {
                    if (answer.QuestionAnswer is QuestionAnswer)
                    {
                        q.AnswerDto.Answer = ((QuestionAnswer)answer.QuestionAnswer).Id.ToString();
                    }
                    else
                    {
                        q.AnswerDto.Answer = answer.QuestionAnswer.ToString();
                    }
                }
            });

        }

        public static string GetAssetDescription(this List<RatingRequestItemAnswerDto> answer)
        {

            var make = answer.FirstOrDefault(x => x.Question.Id == 95).QuestionAnswer.ToString();
            var model = answer.FirstOrDefault(x => x.Question.Id == 96).QuestionAnswer.ToString();

            return string.Format("{0} {1}", make, model);
        }

        public static string GetVehicleRetailPrice(this List<RatingRequestItemAnswerDto> answers)
        {
            var answer = answers.FirstOrDefault(x => x.Question.Id == Questions.AIGVehicleValueRetail.Id);
            return answer != null ? answer.QuestionAnswer.ToString() : string.Empty;
        }

        public static CreateSalesDetailsDto AddAssets(this CreateSalesDetailsDto dto, List<SalesAssetDto> assets, int saleStructureId, int leadId, int saleFNIId)
        {
            dto.SalesDetails.AddRange(assets);
            dto.SaleStructureId = saleStructureId;
            dto.LeadId = leadId;
            dto.SalesFNIId = saleFNIId;
            return dto;
        }
    }
}