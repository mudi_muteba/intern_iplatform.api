﻿using System;
using AutoMapper;
using iPerson.Api.DTOs.PersonInformation;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Leads.Quality;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Sales;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using System.Net;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using Infrastructure.Configuration;
using Domain.Imports.Leads.Workflow.Metadata;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Admin;

namespace Domain.Imports.Leads.Helpers

{
    public class LeadImportHelper
    {
        private readonly IConnector m_Connector;
        private GETResponseDto<PersonInformationResponsesDto> m_PersonResponse;
        private ValidationImportLead m_Validation;
        public LeadImportHelper(IConnector connector)
        {
            m_Connector = connector;
        }

        #region pcube
        public void GetPersonInformation(ValidationImportLead validation)
        {
            m_Validation = validation;

            if (!validation.IsValidated() && !string.IsNullOrEmpty(validation.IdentityNumber))
            {
                m_PersonResponse = m_Connector.Lookups.People.GetByIdNumber(validation.IdentityNumber);
            }
                
        }

        private IndividualDto GetIndividualFromPcubed()
        {
	        if (m_PersonResponse.Response != null)
	        {
		        var results = m_PersonResponse.Response.Results;
		        if (results != null && results.Any() && results.FirstOrDefault() != null)
		        {
			        var personInformationCollectionDto = results.FirstOrDefault();
			        if (personInformationCollectionDto != null)
				        return Mapper.Map<PersonInformationResponseDto, IndividualDto>(personInformationCollectionDto.People
					        .FirstOrDefault());
		        }
	        }
	        return null;
		}
        #endregion
        #region Import Individual

        public POSTResponseDto<int> CreateIndividual(CreateIndividualDto createIndividualDto)
        {
            if (!m_Validation.IsValidated(ImportLeadValidationEnum.Email))
            {
                SetEmail(createIndividualDto);
            }
                

            if (!m_Validation.IsValidated(ImportLeadValidationEnum.Phone))
            {
                SetPhone(createIndividualDto);
            }

            return m_Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public PUTResponseDto<int> EditIndividual(EditIndividualDto editIndividualDto)
        {
            if (!m_Validation.IsValidated(ImportLeadValidationEnum.Email))
            {
                SetEmail(editIndividualDto);
            }
                

            if (!m_Validation.IsValidated(ImportLeadValidationEnum.Phone))
            {
                SetPhone(editIndividualDto);
            }
            return m_Connector.IndividualManagement.Individual(editIndividualDto.Id).EditIndividual(editIndividualDto);
        }

        private void SetEmail(CreateIndividualDto createIndividualDto)
        {
            var individualDto = GetIndividualFromPcubed();
            if (individualDto != null && !string.IsNullOrEmpty(individualDto.ContactDetail.Email))
            {
                createIndividualDto.ContactDetail.Email = individualDto.ContactDetail.Email;
            }
                
        }
        private void SetPhone(CreateIndividualDto createIndividualDto)
        {
            var individualDto = GetIndividualFromPcubed();
            if (individualDto != null)
            {
                if(!string.IsNullOrEmpty(individualDto.ContactDetail.Cell))
                {
                    createIndividualDto.ContactDetail.Cell = individualDto.ContactDetail.Cell;
                }
                    
                if(!string.IsNullOrEmpty(individualDto.ContactDetail.Direct))
                {
                    createIndividualDto.ContactDetail.Direct = individualDto.ContactDetail.Direct;
                }
                    
                if(!string.IsNullOrEmpty(individualDto.ContactDetail.Fax))
                {
                    createIndividualDto.ContactDetail.Fax = individualDto.ContactDetail.Fax;
                }
                    
                if(!string.IsNullOrEmpty(individualDto.ContactDetail.Home))
                {
                    createIndividualDto.ContactDetail.Home = individualDto.ContactDetail.Home;
                }
                    
                if(!string.IsNullOrEmpty(individualDto.ContactDetail.Work))
                {
                    createIndividualDto.ContactDetail.Work = individualDto.ContactDetail.Work;
                }
                    
            }
        }

        private void SetEmail(EditIndividualDto editIndividualDto)
        {
            var individualDto = GetIndividualFromPcubed();
            if (individualDto != null && !string.IsNullOrEmpty(individualDto.ContactDetail.Email))
            {
                editIndividualDto.ContactDetail.Email = individualDto.ContactDetail.Email;
            }
                
        }
        private void SetPhone(EditIndividualDto editIndividualDto)
        {
            var individualDto = GetIndividualFromPcubed();
            if (individualDto != null)
            {
                if (!string.IsNullOrEmpty(individualDto.ContactDetail.Cell))
                {
                    editIndividualDto.ContactDetail.Cell = individualDto.ContactDetail.Cell;
                }
                    
                if (!string.IsNullOrEmpty(individualDto.ContactDetail.Direct))
                {
                    editIndividualDto.ContactDetail.Direct = individualDto.ContactDetail.Direct;
                }
                    
                if (!string.IsNullOrEmpty(individualDto.ContactDetail.Fax))
                {
                    editIndividualDto.ContactDetail.Fax = individualDto.ContactDetail.Fax;
                }
                    
                if (!string.IsNullOrEmpty(individualDto.ContactDetail.Home))
                {
                    editIndividualDto.ContactDetail.Home = individualDto.ContactDetail.Home;
                }
                    
                if (!string.IsNullOrEmpty(individualDto.ContactDetail.Work))
                {
                    editIndividualDto.ContactDetail.Work = individualDto.ContactDetail.Work;
                }
                    
            }
        }

        #endregion
        #region ImportAddress
        public void ImportAddresses(int partyId, List<CreateAddressDto> addresses)
        {
            if(m_Validation.IsValidated(ImportLeadValidationEnum.Address))
            {
                addresses.ForEach(address =>
                {
                    address.PartyId = partyId;
                    m_Connector.IndividualManagement.Individual(partyId).CreateAddress(address);
                });
            }
            else if(m_PersonResponse != null &&m_PersonResponse.IsSuccess && m_PersonResponse.Response.Results.Any())
            {

                var individualDto = GetIndividualFromPcubed();

                if (individualDto != null)
                {
                    individualDto.Addresses.ForEach(x =>
                    {
                        var createAddressDto = Mapper.Map<CreateAddressDto>(x);
                        m_Connector.IndividualManagement.Individual(partyId).CreateAddress(createAddressDto);
                    });
                }
                    
            }
        }
        #endregion
        #region LossHistory
        public bool ImportLossHistory(int partyId)
        {
            var existResponse = m_Connector.LossHistoryManagement.LossHistories.GetByPartyId(partyId);

            if (existResponse.IsSuccess && !existResponse.Errors.Any() && existResponse.Response.Results.Any())
            {
                return true;
            }
                

            var createLossHistoryDto = new CreateLossHistoryDto
            {
                ContactId = 0,
                PartyId = partyId,
                CurrentlyInsuredId = 1,
                InterruptedPolicyClaim = false,
                UninterruptedPolicyNoClaimId = 0,
                UninterruptedPolicyId = 1,
                PreviousComprehensiveInsurance = false,
                InsurerCancel = false,
            };
            var response = m_Connector.LossHistoryManagement.LossHistories.CreateLossHisotry(createLossHistoryDto);

            if (!response.IsSuccess || response.Errors.Any())
            {
                return false;
            }
            return true;
        }
        #endregion
        #region Quality
        public void ImportQuality(int leadId, string identityNo)
        {
            var dto = new CreateLeadQualityDto ();

            //Validation success no calls to pcube
            if (m_Validation.IsValidated())
            {
                dto.CreditGradeNonCPA = "No History";
                dto.MosaicCPAGroupMerged = "No Record";
            }
            else if (m_PersonResponse != null && m_PersonResponse.IsSuccess && m_PersonResponse.Response.Results.Any())
            {
                dto = GetLeadQualityDto(m_PersonResponse.Response, leadId, identityNo);
            }
                

            m_Connector.LeadManagement.LeadQuality(leadId).SaveLeadQuality(dto);
        }
        private CreateLeadQualityDto GetLeadQualityDto(PersonInformationResponsesDto response, int leadId, string identityNo)
        {
            var demographicinfo = response.Results.FirstOrDefault().People.FirstOrDefault().DemographicsDto;
            var dto = Mapper.Map<CreateLeadQualityDto>(demographicinfo);
            dto.Id = leadId;
            dto.IdentityNumber = identityNo;
            return dto;
        }
        #endregion
        #region Proposal
        public POSTResponseDto<List<SalesAssetDto>> ImportProposals(int partyId, int campaignId, List<RatingRequestItemDto> assets, int channelId)
        {
            var result = new List<SalesAssetDto>();
            var proposalHeaderId = CreateProposalHeader(partyId, campaignId);

            if (proposalHeaderId == 0)
            {
                return new POSTResponseDto<List<SalesAssetDto>>(result, string.Empty);
            }
                

            foreach (var asset in assets)
            {
                // Motor proposal definition
                var proposalDefinitionId = CreateMotorProposalDefinition(partyId, proposalHeaderId, channelId);

                if (proposalDefinitionId == 0)
                {
                    continue;
                }
                    
                var proposalDefinitionDto = SaveProposalDefinitionAnswers(partyId, proposalHeaderId, proposalDefinitionId, asset.Answers);

                result.Add(new SalesAssetDto(proposalDefinitionDto.Response.Asset.Id, asset.Answers.GetVehicleRetailPrice()));
            }
            return new POSTResponseDto<List<SalesAssetDto>>(result, string.Empty);
        }
        public PUTResponseDto<ProposalDefinitionDto> CreateProposal(int partyId, int campaignId, List<RatingRequestItemAnswerDto> answers, int channelId)
        {
            var result = new PUTResponseDto<ProposalDefinitionDto>();
            result.StatusCode = HttpStatusCode.BadRequest;

            var proposalHeaderId = CreateProposalHeader(partyId, campaignId);

            if (proposalHeaderId == 0)
            {
                return result;
            }
                

            // Motor proposal definition
            var proposalDefinitionId = CreateMotorProposalDefinition(partyId, proposalHeaderId, channelId);
            if (proposalDefinitionId == 0)
            {
                return result;
            }
               

            // answers and assets
            return SaveProposalDefinitionAnswers(partyId, proposalHeaderId,
                proposalDefinitionId, answers);
        }
        private int CreateProposalHeader(int partyId, int campaignId)
        {
            return m_Connector.IndividualManagement.Individual(partyId)
                    .Proposals()
                    .CreateProposalHeader(new CreateProposalHeaderDto
                    {
                        PartyId = partyId,
                        Description = "This is a new proposal",
                        CampaignId = campaignId  // pass selected campaign
                    }).Response;
        }
        private int CreateMotorProposalDefinition(int partyId, int proposalId, int channelId)
        {
            var newDefinition = new CreateProposalDefinitionDto
            {
                ProposalHeaderId = proposalId,
                CoverId = Covers.Motor.Id,
                ProductId = GetMotorProductIdBasedOnConfig(),
                ChannelId = channelId
            };

            return m_Connector.IndividualManagement.Individual(partyId)
                .Proposal(proposalId)
                .CreateProposalDefinition(newDefinition).Response;
        }
        private PUTResponseDto<ProposalDefinitionDto> SaveProposalDefinitionAnswers(int partyId, int proposalHeaderId, int proposalDefinitionId, List<RatingRequestItemAnswerDto> answers)
        {
            var assetDescription = answers.GetAssetDescription();
            var proposalDefinitionResponse = m_Connector.IndividualManagement
                    .Individual(partyId)
                    .ProposalDefinitionQuestion(proposalDefinitionId)
                    .GetQuestionAnswer();

            var questions = proposalDefinitionResponse.Response.QuestionDefinitions;


            //Set Answers
            questions.SetAnswer(answers);

            List<ProposalDefinitionAnswerDto> proposalDefinitionDto = questions.Select(a => new ProposalDefinitionAnswerDto
            {
                Id = a.AnswerDto.Id,
                Answer = a.AnswerDto.Answer,
                QuestionType = a.Question.QuestionType
            }).ToList();

            var editDto = new EditProposalDefinitionDto
            {
                Id = proposalDefinitionId,
                Answers = proposalDefinitionDto,
                Description = assetDescription
            };

            var result = m_Connector.IndividualManagement.Individual(partyId)
                     .Proposal(proposalHeaderId)
                     .UpdateProposalDefinition(editDto);

            return result;
        }

        public int GetMotorProductIdBasedOnConfig()
        {
            var insurerCode = ConfigurationReader.Platform.InsurerCode;
            var productId = 0; //hardcoded as multiquote
            switch (insurerCode)
            {
                case "AIG":
                    productId = 46;
                    break;
                case "MF":
                    productId = 51;
                    break;
                case "UAP":
                    productId = 27;
                    break;
                case "KP":
                    productId = 58;
                    break;
                case "AUG":
                    productId = 57;
                    break;
                case "PSG":
                    productId = 27;
                    break;
                default:
                    productId = 27; //hardcoded as multiquote
                    break;
            }

            return productId;

        }
        #endregion
        #region Sales
        private POSTResponseDto<int> CreateSalesFNI(CreateSalesFNIDto createSalesFniDto)
        {
            var result = m_Connector.SalesManagement.Sales.CreateSalesFni(createSalesFniDto);
            return result;
        }
        public bool ImportSalesInformation(ImportSalesStructureDto salesStructureDto,
            CreateSalesFNIDto createSalesFniDto, CreateSalesDetailsDto createSalesDetailsDto, int leadId, List<SalesAssetDto> assets)
        {
            //Create Sales FNI
            var salesFNIResponse = CreateSalesFNI(createSalesFniDto);
            if (!salesFNIResponse.IsSuccess || salesFNIResponse.Errors.Any())
            {
                return false;
            }
                

            salesStructureDto.SaleFNIId = salesFNIResponse.Response;
            salesStructureDto.LeadId = leadId;

            //Sales Tags
            var tagList = CreateSalesTags(salesStructureDto);
            if (tagList.Count != 3) // check if all 3 tags were created
            {
                return false;
            }

            //Sales Structure
            var createSalesStructureDto = GenerateSalesStructureDto(tagList, salesStructureDto.Channel);


            //create Sales Structure
            var saleStructureResponse = m_Connector.SalesManagement.Sales.CreateSalesStructures(createSalesStructureDto);
            if (!saleStructureResponse.IsSuccess || saleStructureResponse.Errors.Any())
            {
                return false;
            }

            //Create Sales Details
            var result = m_Connector.SalesManagement.Sales.CreateSalesDetails(createSalesDetailsDto.AddAssets(assets, saleStructureResponse.Response, leadId, salesFNIResponse.Response));
            if (!result.IsSuccess || result.Errors.Any())
            {
                return false;
            }

            return true;
        }
        private List<SalesTagDto> CreateSalesTags(ImportSalesStructureDto dto)
        {
            var TagList = new List<SalesTagDto>();
            //Get or Create GroupSalesTag
            var GroupSalesTag = GetOrCreateSalesTag(new CreateSalesTagDto { Code = dto.GroupCode, Name = dto.GroupName, SalesTagType = SalesTagTypes.Group });
            if (GroupSalesTag == null)
            {
                return TagList;
            }

            TagList.Add(GroupSalesTag);
            //Get or Create CompanySalesTag
            var CompanySalesTag = GetOrCreateSalesTag(new CreateSalesTagDto { Code = dto.FinanceCompanyName, Name = dto.FinanceCompanyName, SalesTagType = SalesTagTypes.Finance });
            if (CompanySalesTag == null)
            {
                return TagList;
            }

            TagList.Add(CompanySalesTag);
            //Get or Create BranchSalesTag
            var BranchSalesTag = GetOrCreateSalesTag(new CreateSalesTagDto { Code = dto.BranchCode, Name = dto.BranchName, SalesTagType = SalesTagTypes.Branch });
            if (BranchSalesTag == null)
            {
                return TagList;
            }

            TagList.Add(BranchSalesTag);

            return TagList;
        }
        private SalesTagDto GetOrCreateSalesTag(CreateSalesTagDto dto)
        {
            var searchResult = m_Connector.SalesManagement.SalesTag.SearchSalesTags(new SalesTagSearchDto { Code = dto.Code });

            if (searchResult.IsSuccess && !searchResult.Errors.Any() && searchResult.Response.Results.Any()) 
            {
                return searchResult.Response.Results.First();
            }

            var result = m_Connector.SalesManagement.SalesTag.CreateSalesTag(dto);

            if (result.IsSuccess && !result.Errors.Any() && result.Response != 0)
            {
                var resultDto = Mapper.Map<CreateSalesTagDto, SalesTagDto>(dto, new SalesTagDto { Id = result.Response });
                return resultDto;
            }
                
            return null;
        }
        private CreateSalesStructuresDto GenerateSalesStructureDto(List<SalesTagDto> Tags, ChannelInfoDto channel)
        {
            var result = new CreateSalesStructuresDto { ChannelId = channel.Id };
            SalesTagDto companySalesTag = null;
            SalesTagDto groupTag = null;
            int level = 1;
            Tags.ForEach(x =>
            {

                //Checking if this is a parent Tag
                if (companySalesTag == null)
                {
                    companySalesTag = x;
                }


                //Checking if this is a group Tag
                if (groupTag == null)
                {
                    groupTag = x;
                }

                result.SalesStructures.Add(new SalesStructureDto
                {
                    GroupSalesTag = groupTag,
                    CompanySalesTag = companySalesTag,
                    BranchSalesTag = x,
                    Level = level,
                    Channel = channel,
                });

                companySalesTag = x;
                level++;
            });

            return result;
        }
        #endregion
        #region CampaignBucket
        public POSTResponseDto<bool> ImportLeadToCampaigns(int leadid, int CampaignId)
        {
            var dto = new AssignLeadCampaignsDto();
            dto.LeadId = leadid;
            dto.CampaignList.Add(CampaignId);
            var result = m_Connector.CampaignManagement.Campaigns.AssignLeadCampaigns(dto);
            return result;
        }
        #endregion

    }
}
                                   