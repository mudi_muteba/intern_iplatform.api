﻿using System;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Metadata
{
    public class EditIndividualUploadHeaderMetadata : ITaskMetadata
    {
        public Guid LeadImportReference { get; set; }

        public EditIndividualUploadHeaderMetadata() { }

        public EditIndividualUploadHeaderMetadata(Guid leadImportReference)
        {
            LeadImportReference = leadImportReference;
        }
    }
}