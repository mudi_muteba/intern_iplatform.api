﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Imports.Leads.Workflow.Metadata
{
    public enum ImportLeadValidationEnum
    {
        Email,
        Phone,
        Address
    }
}
