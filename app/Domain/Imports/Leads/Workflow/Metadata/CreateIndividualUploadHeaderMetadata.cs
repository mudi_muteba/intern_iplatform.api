﻿using MasterData;
using System;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Metadata
{
    public class CreateIndividualUploadHeaderMetadata : ITaskMetadata
    {
        public Guid LeadImportReference { get; set; }
        public int ChannelId { get; set; }
        public int CampaignId { get; set; }
        public int UserId { get; set; }
        public string FileName { get; set; }
        public int FileRecordCount { get; set; }
        public int NewCount { get; set; }
        public int DuplicateCount { get; set; }
        public int DuplicateFileRecordCount { get; set; }
        public int? FailureCount { get; set; }
        public LeadImportStatus Status { get; set; }

        public CreateIndividualUploadHeaderMetadata(Guid leadImportReference, int channelId, int campaignId, int userId, string fileName, int fileRecordCount, int newCount, int duplicateCount, int duplicateFileRecordCount, int? failureCount, LeadImportStatus status)
        {
            LeadImportReference = leadImportReference;
            ChannelId = channelId;
            CampaignId = campaignId;
            UserId = userId;
            FileName = fileName;
            FileRecordCount = fileRecordCount;
            NewCount = newCount;
            DuplicateCount = duplicateCount;
            DuplicateFileRecordCount = duplicateFileRecordCount;
            FailureCount = failureCount;
            Status = status;
        }
    }
}