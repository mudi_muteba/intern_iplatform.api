﻿using MasterData;
using System;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Metadata
{
    public class EditIndividualUploadDetailMetadata : ITaskMetadata
    {
        public Guid LeadImportReference { get; protected internal set; }
        public bool NewIndividual { get; protected internal set; }
        public int DuplicateFileRecordCount { get; protected internal set; }
        public LeadImportStatus LeadImportStatus { get; protected internal set; }

        public EditIndividualUploadDetailMetadata(Guid leadImportReference, bool newIndividual, int duplicateFileRecordCount, LeadImportStatus leadImportStatus)
        {
            LeadImportReference = leadImportReference;
            NewIndividual = newIndividual;
            DuplicateFileRecordCount = duplicateFileRecordCount;
            LeadImportStatus = leadImportStatus;
        }
    }
}