﻿using iPlatform.Api.DTOs.Leads.Excel;
using System.Collections.Generic;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Metadata
{
    public class ExcelImportMetadata : ITaskMetadata
    {
        public ExcelImportMetadata() { }
        public int CampaignId { get; set; }
        public int ChannelId { get; set; }
        public int UserId { get; set; }
        public string FileName { get; set; }
        public string Base64 { get; set; }
        public List<LeadImportExcelRowDto> Leads { get; set; }

        public ExcelImportMetadata(int campaignId, int channelId, int userId, string fileName, string base64, List<LeadImportExcelRowDto> leads)
        {
            CampaignId = campaignId;
            ChannelId = channelId;
            UserId = userId;
            FileName = fileName;
            Base64 = base64;
            Leads = leads;
        }
        public ExcelImportMetadata(int campaignId, int channelId, int userId, string fileName, string base64)
        {
            CampaignId = campaignId;
            ChannelId = channelId;
            UserId = userId;
            FileName = fileName;
            Base64 = base64;
        }
    }
}