﻿using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Ratings.Request;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Sales;
using Workflow.Messages.Plan.Tasks;
using Domain.Imports.Leads.Helpers;

namespace Domain.Imports.Leads.Workflow.Metadata
{
    public class CreateLeadMetadata : ITaskMetadata
    {
        public CreateIndividualDto CreateIndividualDto { get; set; }
        public List<CreateAddressDto> Addresses { get; set; }
        public List<RatingRequestItemDto> Items { get; set; }
        public int CampaignId { get; set; }
        public int ChannelId { get; set; }
        public CreateSalesFNIDto CreateSalesFniDto { get; set; }
        public ImportSalesStructureDto ImportSalesStructureDto { get; set; }
        public CreateSalesTagDto CreateSalesTagDto { get; set; }
        public EditIndividualUploadDetailDto EditIndividualUploadDetailDto { get; set; }
        public CreateSalesDetailsDto CreateSalesDetailsDto { get; set; }

        public CreateLeadMetadata()
        {
            Addresses = new List<CreateAddressDto>();
            Items = new List<RatingRequestItemDto>();
        }

        public CreateLeadMetadata(CreateIndividualDto createIndividualDto)
        {
            CreateIndividualDto = createIndividualDto;
            Addresses = new List<CreateAddressDto>();
            Items = new List<RatingRequestItemDto>();
        }
        public CreateLeadMetadata(ImportLeadDto source, CreateIndividualDto createIndividualDto,
            List<CreateAddressDto> addresses, CreateSalesFNIDto createSalesFniDto,
            ImportSalesStructureDto importSalesStructureDto, EditIndividualUploadDetailDto editIndividualUploadDetailDto,
            CreateSalesDetailsDto createSalesDetailsDto)
        {
            CampaignId = source.CampaignId;
            ChannelId = source.ChannelId;
            CreateIndividualDto = createIndividualDto;
            Addresses = addresses;
            Items = source.Request == null ? new List<RatingRequestItemDto>(): source.Request.Items;
            CreateSalesFniDto = createSalesFniDto;
            ImportSalesStructureDto = importSalesStructureDto;
            EditIndividualUploadDetailDto = editIndividualUploadDetailDto;
            CreateSalesDetailsDto = createSalesDetailsDto;
        }

        //Verify if all necessary info are available 
        public ValidationImportLead Validate()
        {
            var result = new ValidationImportLead(CreateIndividualDto.IdentityNo);

            //Email Address
            if (string.IsNullOrEmpty(CreateIndividualDto.ContactDetail.Email) && !new EmailRegexHelper().IsValidEmail(CreateIndividualDto.ContactDetail.Email))
                result.Validations.Add(ImportLeadValidationEnum.Email);

            //Phone
            if (string.IsNullOrEmpty(CreateIndividualDto.ContactDetail.Cell) && 
                string.IsNullOrEmpty(CreateIndividualDto.ContactDetail.Direct) && 
                string.IsNullOrEmpty(CreateIndividualDto.ContactDetail.Home))
                result.Validations.Add(ImportLeadValidationEnum.Phone);

            //Address
            if(Addresses.Count == 0)
                result.Validations.Add(ImportLeadValidationEnum.Address);

            return result;
        }
    }
}