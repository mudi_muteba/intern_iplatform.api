﻿using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Ratings.Request;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Sales;
using Workflow.Messages.Plan.Tasks;
using Domain.Imports.Leads.Helpers;

namespace Domain.Imports.Leads.Workflow.Metadata
{
    public class EditLeadMetadata : ITaskMetadata
    {
        public EditIndividualDto EditIndividualDto { get; set; }
        public List<CreateAddressDto> Addresses { get; set; }
        public List<RatingRequestItemDto> Items { get; set; }
        public int CampaignId { get; set; }
        public int ChannelId { get; set; }
        public CreateSalesFNIDto CreateSalesFniDto { get; set; }
        public ImportSalesStructureDto ImportSalesStructureDto { get; set; }
        public EditSalesTagDto EditSalesTagDto { get; set; }
        public EditIndividualUploadDetailDto EditIndividualUploadDetailDto { get; set; }
        public CreateSalesDetailsDto CreateSalesDetailsDto { get; set; }

        public EditLeadMetadata()
        {
            Addresses = new List<CreateAddressDto>();
            Items = new List<RatingRequestItemDto>();
        }


        public EditLeadMetadata(EditIndividualDto editIndividualDto)
        {
            EditIndividualDto = editIndividualDto;
        }

        public EditLeadMetadata(ImportLeadDto source, EditIndividualDto editIndividualDto, 
            List<CreateAddressDto> addresses, CreateSalesFNIDto createSalesFniDto, 
            ImportSalesStructureDto importSalesStructureDto, EditIndividualUploadDetailDto editIndividualUploadDetailDto,
            CreateSalesDetailsDto createSalesDetailsDto)
        {
            CampaignId = source.CampaignId;
            ChannelId = source.ChannelId;
            EditIndividualDto = editIndividualDto;
            Addresses = addresses;
            Items = source.Request == null? new List<RatingRequestItemDto>() :  source.Request.Items;
            CreateSalesFniDto = createSalesFniDto;
            ImportSalesStructureDto = importSalesStructureDto;
            EditIndividualUploadDetailDto = editIndividualUploadDetailDto;
            CreateSalesDetailsDto = createSalesDetailsDto;
        }


        //Verify if all necessary info are available 
        public ValidationImportLead Validate()
        {
            var result = new ValidationImportLead(EditIndividualDto.IdentityNo);

            //Email Address
            if (string.IsNullOrEmpty(EditIndividualDto.ContactDetail.Email) && !new EmailRegexHelper().IsValidEmail(EditIndividualDto.ContactDetail.Email))
                result.Validations.Add(ImportLeadValidationEnum.Email);

            //Phone
            if (string.IsNullOrEmpty(EditIndividualDto.ContactDetail.Cell) &&
                string.IsNullOrEmpty(EditIndividualDto.ContactDetail.Direct) &&
                string.IsNullOrEmpty(EditIndividualDto.ContactDetail.Home))
                result.Validations.Add(ImportLeadValidationEnum.Phone);

            return result;
        }
    }
}