﻿using System;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Metadata
{
    public class CreateIndividualUploadDetailMetadata : ITaskMetadata
    {
        public Guid LeadImportReference { get; protected internal set; }
        public int ChannelId { get; protected internal set; }
        public int UserId { get; protected internal set; }
        public int CampaignId { get; protected internal set; }
        public string FileName { get; protected internal set; }
        public string ContactNumber { get; protected internal set; }
        public bool NewIndividual { get; protected internal set; }
        public int DuplicateFileRecordCount { get; protected internal set; }
        public string LeadImportStatus { get; protected internal set; }

        public CreateIndividualUploadDetailMetadata(Guid leadImportReference, int channelId, int userId, int campaignId, string fileName, string contactNumber, bool newIndividual, int duplicateFileRecordCount, string leadImportStatus)
        {
            LeadImportReference = leadImportReference;
            ChannelId = channelId;
            UserId = userId;
            CampaignId = campaignId;
            FileName = fileName;
            ContactNumber = contactNumber;
            NewIndividual = newIndividual;
            DuplicateFileRecordCount = duplicateFileRecordCount;
            LeadImportStatus = leadImportStatus;
        }
    }
}