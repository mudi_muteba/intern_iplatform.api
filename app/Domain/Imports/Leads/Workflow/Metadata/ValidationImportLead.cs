﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Imports.Leads.Workflow.Metadata
{
    public class ValidationImportLead
    {

        public ValidationImportLead()
        {
            Validations = new List<ImportLeadValidationEnum>();
        }

        public ValidationImportLead(string idNumber)
        {
            IdentityNumber = idNumber;
            Validations = new List<ImportLeadValidationEnum>();
        }

        public IList<ImportLeadValidationEnum> Validations { get; set; }
        public string IdentityNumber { get; private set; }

        public bool IsValidated()
        {
            return !Validations.Any();
        }

        public bool IsValidated(ImportLeadValidationEnum validation)
        {
            if (!Validations.Any())
                return true;

            if (Validations.Any(x => x == validation))
                return false;

            return true;
        }

    }
}
