﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Messages
{
    public class CreateLeadMessage : WorkflowExecutionMessage
    {
        // Json.Net requires a default constructor
        public CreateLeadMessage() { }
        public CreateLeadMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }
        public CreateLeadMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}