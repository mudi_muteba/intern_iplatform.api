﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Messages
{
    public class EditLeadMessage : WorkflowExecutionMessage
    {
        // Json.Net requires a default constructor
        public EditLeadMessage() { }
        public EditLeadMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }
        public EditLeadMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}