﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Messages
{
    public class EditIndividualUploadDetailMessage : WorkflowExecutionMessage
    {
        public EditIndividualUploadDetailMessage() { }
        public EditIndividualUploadDetailMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }
        public EditIndividualUploadDetailMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}