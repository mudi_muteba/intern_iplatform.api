﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Messages
{
    public class ImportExcelLeadMessage : WorkflowExecutionMessage
    {
        // Json.Net requires a default constructor
        public ImportExcelLeadMessage() { }
        public ImportExcelLeadMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }
        public ImportExcelLeadMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}