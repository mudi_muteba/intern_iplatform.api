﻿using Domain.Imports.Leads.Workflow.Metadata;
using Workflow.Messages;

namespace Domain.Imports.Leads.Workflow.Messages
{
    public class CreateIndividualUploadHeaderMessage : WorkflowExecutionMessage
    {
        public CreateIndividualUploadHeaderMessage() { }
        public CreateIndividualUploadHeaderMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }
        public CreateIndividualUploadHeaderMessage(CreateIndividualUploadHeaderMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}