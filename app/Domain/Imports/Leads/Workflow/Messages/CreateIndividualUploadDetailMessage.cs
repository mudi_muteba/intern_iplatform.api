﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Imports.Leads.Workflow.Messages
{
    public class CreateIndividualUploadDetailMessage : WorkflowExecutionMessage
    {
        public CreateIndividualUploadDetailMessage() { }
        public CreateIndividualUploadDetailMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }
        public CreateIndividualUploadDetailMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}