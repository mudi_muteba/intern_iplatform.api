﻿using Domain.Imports.Leads.Workflow.Metadata;
using Workflow.Messages;

namespace Domain.Imports.Leads.Workflow.Messages
{
    public class EditIndividualUploadHeaderMessage : WorkflowExecutionMessage
    {
        public EditIndividualUploadHeaderMessage() { }
        public EditIndividualUploadHeaderMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }
        public EditIndividualUploadHeaderMessage(EditIndividualUploadHeaderMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}