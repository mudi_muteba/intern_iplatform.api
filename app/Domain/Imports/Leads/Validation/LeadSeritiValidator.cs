﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Leads;
using ValidationMessages.Individual;
using Domain.Campaigns.Queries;
using System.Collections.Generic;
using Domain.Campaigns;
using ValidationMessages.Campaigns;
using System.Xml.Schema;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using ValidationMessages.Leads;

namespace Domain.Imports.Leads.Validation
{
    public class LeadSeritiValidator : IValidateDto<LeadImportXMLDto>
    {


        public LeadSeritiValidator(IProvideContext contextProvider, IRepository repository)
        {

        }

        public void Validate(LeadImportXMLDto dto, ExecutionResult result)
        {
            string error = string.Empty;
            var _xml = XDocument.Parse(dto.XMLString);

            //http://msdn.microsoft.com/en-us/library/bb387037.aspx = Validate Using XSD
            // TODO: How to validate only certain elements not caring if there's "more"
            const string xsdMarkup = @"<xsd:schema xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
                                       <xsd:element name='ApplicationData'>
                                        <xsd:complexType>
                                         <xsd:sequence>
                                          <xsd:element name='Campaign' minOccurs='0' maxOccurs='1'/>
                                          <xsd:element name='Deal' minOccurs='0' maxOccurs='1'/>
                                          <xsd:element name='User' minOccurs='1' maxOccurs='1'/>
                                          <xsd:element name='Client' minOccurs='1' maxOccurs='1'/>
                                          <xsd:element name='Driver' minOccurs='0' maxOccurs='1'/>
                                          <xsd:element name='Vehicle' minOccurs='0' maxOccurs='1'/>
                                         </xsd:sequence>
                                        </xsd:complexType>
                                       </xsd:element>
                                      </xsd:schema>";

            var schemas = new XmlSchemaSet();
            schemas.Add("", XmlReader.Create(new StringReader(xsdMarkup)));
            _xml.Validate(schemas, (o, e) => { error = e.Message; });

            if (!string.IsNullOrEmpty(error))
                result.AddValidationMessage(LeadImportValidationMessages.InvalidXML);
        }

    }
}