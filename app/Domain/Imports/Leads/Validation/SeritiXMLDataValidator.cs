﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Leads;
using ValidationMessages.Individual;
using Domain.Campaigns.Queries;
using System.Collections.Generic;
using Domain.Campaigns;
using ValidationMessages.Campaigns;
using System.Xml.Schema;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using ValidationMessages.Leads;
using iPlatform.Api.DTOs.Base.Helper.xml;
using Domain.Admin;
using System;

namespace Domain.Imports.Leads.Validation
{
    public class SeritiXMLDataValidator : IValidateDto<LeadImportXMLDto>
    {


        public SeritiXMLDataValidator(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
        }
        private readonly IRepository _repository;
        public void Validate(LeadImportXMLDto dto, ExecutionResult result)
        {
            var leadImportDto = XmlHelper.XmlToObject<LeadImportDto>(dto.XMLString);

            if (leadImportDto == null)
                result.AddValidationMessage(LeadImportValidationMessages.InvalidFormat);
            else
            {
                if (leadImportDto.Client == null)
                    result.AddValidationMessage(LeadImportValidationMessages.InvalidClient);
                else
                {
                    if (string.IsNullOrEmpty(leadImportDto.Client.Lastname))
                        result.AddValidationMessage(LeadImportValidationMessages.InvalidClientLastName);
                    if (string.IsNullOrEmpty(leadImportDto.Client.FirstName))
                        result.AddValidationMessage(LeadImportValidationMessages.InvalidClientFirstName);
                    if (string.IsNullOrEmpty(leadImportDto.Client.HomeTelephoneNumber) &&
                         string.IsNullOrEmpty(leadImportDto.Client.WorkTelephoneNumber) &&
                         string.IsNullOrEmpty(leadImportDto.Client.MobileNumber))
                        result.AddValidationMessage(LeadImportValidationMessages.InvalidClientContact);
                }
            }
        }
    }
}