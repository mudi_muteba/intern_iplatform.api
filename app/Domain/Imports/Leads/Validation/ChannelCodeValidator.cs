﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Leads;
using ValidationMessages.Individual;
using Domain.Campaigns.Queries;
using System.Collections.Generic;
using Domain.Campaigns;
using ValidationMessages.Campaigns;
using System.Xml.Schema;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using ValidationMessages.Leads;
using iPlatform.Api.DTOs.Base.Helper.xml;
using Domain.Admin;
using System;

namespace Domain.Imports.Leads.Validation
{
    public class ChannelCodeValidator : IValidateDto<LeadImportXMLDto>
    {


        public ChannelCodeValidator(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
        }
        private readonly IRepository _repository;
        public void Validate(LeadImportXMLDto dto, ExecutionResult result)
        {
            var leadImportDto = XmlHelper.XmlToObject<LeadImportDto>(dto.XMLString);

            var channel = GetChannelByCode(leadImportDto.Deal.CompanyCode);

            if(channel == 0)
            result.AddValidationMessage(LeadImportValidationMessages.ChannelNotExist.AddParameters(new[] { leadImportDto.Deal.CompanyCode}));
        }

        private int GetChannelByCode(string code)
        {
            var channel = _repository.GetAll<Channel>().FirstOrDefault(x => x.Code == code);
            return channel == null ? 0 : channel.Id;
        }

    }
}