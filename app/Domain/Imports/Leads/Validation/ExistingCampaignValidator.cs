﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Leads.Excel;
using ValidationMessages.Individual;
using Domain.Campaigns.Queries;
using System.Collections.Generic;
using Domain.Campaigns;
using ValidationMessages.Campaigns;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.Imports.Leads.Validation
{
    public class ExistingCampaignValidator : IValidateDto<LeadImportExcelDto>, IValidateDto<SubmitPolicyBindingDto>
    {
        private readonly GetCampaignByIdQuery query;

        public ExistingCampaignValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetCampaignByIdQuery(contextProvider, repository);
        }

        public void Validate(LeadImportExcelDto dto, ExecutionResult result)
        {
            ValidateCampaigns(dto.CampaignId, result);
        }

        public void Validate(SubmitPolicyBindingDto dto, ExecutionResult result)
        {
            ValidateCampaigns(dto.CampaignId, result);
        }
        private void ValidateCampaigns(int Id, ExecutionResult result)
        {
            var queryResult = GetCampaignById(Id);

            if (queryResult.Count() <= 0)
            {
                result.AddValidationMessage(CampaignValidationMessages.UnknownCampaign.AddParameters(new[] { Id.ToString() }));
            }
        }

        private IQueryable<Campaign> GetCampaignById(int id)
        {
            return query.ById(id).ExecuteQuery();
        }
    }
}