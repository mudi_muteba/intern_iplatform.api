﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Leads.Excel;
using ValidationMessages.Individual;
using Domain.Campaigns.Queries;
using System.Collections.Generic;
using Domain.Campaigns;
using ValidationMessages.Campaigns;
using System.Xml.Schema;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using ValidationMessages.Leads;

namespace Domain.Imports.Leads.Validation
{
    public class LeadExcelValidator : IValidateDto<LeadImportExcelDto>
    {


        public LeadExcelValidator(IProvideContext contextProvider, IRepository repository)
        {

        }

        public void Validate(LeadImportExcelDto dto, ExecutionResult result)
        {

        }

    }
}