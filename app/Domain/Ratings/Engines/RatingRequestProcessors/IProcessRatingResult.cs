﻿using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Engines.RatingRequestProcessors
{
    public interface IProcessRatingResult
    {
        void Process(RatingRequestDto request, RatingResultDto result, RatingConfiguration ratingConfiguration);
    }
}