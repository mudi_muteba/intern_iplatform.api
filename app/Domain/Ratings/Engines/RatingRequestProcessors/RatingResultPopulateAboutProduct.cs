using System.Collections.Generic;
using System.Linq;
using Domain.Products.Queries;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Engines.RatingRequestProcessors
{
    public class RatingResultPopulateAboutProduct : IProcessRatingResult
    {
        private readonly GetAllocatedProductsByCodesQuery query;

        public RatingResultPopulateAboutProduct(GetAllocatedProductsByCodesQuery query)
        {
            this.query = query;
        }

        public void Process(RatingRequestDto request, RatingResultDto result, RatingConfiguration ratingConfiguration)
        {
            if (!ShouldProcess(result))
                return;

            query.WithCodes(result.Policies.Select(p => p.ProductCode));

            var results = query.ExecuteQuery();

            foreach (var product in results)
            {
                var matchingPolicy = result.Policies.FirstOrDefault(p => p.ProductCode == product.ProductCode);
                if (matchingPolicy == null)
                    continue;

                matchingPolicy.AboutProduct = new AboutProductDto()
                {
                    Name = product.Name,
                    Code = product.ProductCode,
                    ProductType = product.ProductType.Name,
                    Provider = new ProductProviderDto()
                    {
                        Id = product.ProductProvider.Id,
                        Code = product.ProductProvider.Code,
                        TradingName = product.ProductProvider.TradingName,
                        RegisteredName = product.ProductProvider.RegisteredName,
                        Description = product.ProductProvider.Description
                    },
                    Images = new List<ProductImageDto>()
                    {
                        new ProductImageDto()
                        {
                            Url = product.ImageName
                        }
                    }
                };
            }
        }

        private bool ShouldProcess(RatingResultDto result)
        {
            if (result == null)
                return false;

            if (result.Policies == null)
                return false;

            if (!result.Policies.Any())
                return false;

            return true;
        }
    }
}