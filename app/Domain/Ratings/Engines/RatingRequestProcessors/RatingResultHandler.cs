using System.Linq;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.Base.Repository;
using Domain.Products.Fees;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Engines.RatingRequestProcessors
{
    public class RatingResultHandler : IProcessRatingResult
    {
        private static readonly ILog log = LogManager.GetLogger<RatingResultHandler>();
        private readonly IRepository repository;

        public RatingResultHandler(IRepository repository)
        {
            this.repository = repository;
        }

        public void Process(RatingRequestDto request, RatingResultDto result, RatingConfiguration ratingConfiguration)
        {
            var paymentPlan = request.Policy.PaymentPlan;
            var fees = new FeeCalculator(repository, paymentPlan);

            foreach (var policy in result.Policies)
            {
                var calcSasria = UAPProducts(policy.ProductCode);
                
                log.InfoFormat("Processing Quote [{0}] ({1})", policy.ProductCode, paymentPlan.Name);
                if (policy.Items == null) continue;

                foreach (var item in policy.Items)
                {
                    item.Premium = PremiumCalculator.Calculate(policy.ProductCode, paymentPlan, item.Premium);

                    var sumInsured = 0m;
                    var requestItem =
                        request.Items.FirstOrDefault(x => x.Cover.Id == item.Cover.Id && x.AssetNo == item.AssetNo);

                    if (requestItem != null)
                        sumInsured = requestItem.Answers.GetSumInsured();

                    if (calcSasria)
                    {
                        item.Sasria = SasriaCalculator.Calculate(item.Cover, paymentPlan, sumInsured, item.Premium);
                        item.SasriaCalulated = item.Sasria;
                    }
                        
                    if (item.Premium > 0)
                    {
                        log.DebugFormat("Premium: {0:C} Sasria: {1:C} ({2} {3}) Sum Insured: {4:C0}",
                            item.Premium, item.Sasria, item.Cover.Name, item.AssetNo, sumInsured);
                    }
                    else
                    {
                        log.WarnFormat("Premium: {0:C} Sasria: {1:C} ({2} {3}) Sum Insured: {4:C0}",
                            item.Premium, item.Sasria, item.Cover.Name, item.AssetNo, sumInsured);
                    }
                    item.SumInsured = sumInsured;
                    
                }

                policy.Fees += fees.Calculate(policy.ProductCode, policy.Items.Sum(i => i.Premium), ratingConfiguration.ChannelId, request.Policy.Source);

                if (calcSasria)
                {
                    SasriaCalculator.Shortfall(policy, paymentPlan);
                }

                log.DebugFormat("Total Fees: {0,-15:C}", policy.Fees);
            }
        }

        private bool UAPProducts(string productCode)
        {
            //FCM - Added MOTORSERVICE And MOTORWAR to list
            if (productCode == "IWYZEMOTORC" || productCode == "IWYZEMOTORTPF" || productCode == "IWYZEMOTORTO" || productCode == "MOTORSERVICE" || productCode == "MOTORWAR")
                return false;
            return true;
        }
    }
}