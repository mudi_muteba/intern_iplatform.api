using System.Linq;
using Common.Logging;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Engines.RatingRequestProcessors
{
    public class RatingFailureHandler : IProcessRatingResult
    {
        private static readonly ILog log = LogManager.GetLogger<RatingFailureHandler>();

        public void Process(RatingRequestDto request, RatingResultDto result, RatingConfiguration ratingConfiguration)
        {
            if (result == null)
            {
                log.ErrorFormat("No result for rating request with Id '{0}'", request.Id.ToString());
                return;
            }

            if (result.State.GetStatus() == RatingResultStatus.Failure)
            {
                log.ErrorFormat("Result Failure for request with Id '{0}'", request.Id.ToString());

                foreach (var error in result.State.Errors)
                {
                    log.Error(error);
                }
            }

            foreach (var policy in result.Policies)
            {
                if (policy.State.GetStatus() == RatingResultStatus.Failure)
                {
                    log.ErrorFormat("Policy Failure for product {0} - {1}", policy.InsurerCode, policy.ProductCode);

                    foreach (var error in policy.State.Errors)
                    {
                        log.Error(error);
                    }
                }

                if(policy.Items!= null)
                foreach (var item in policy.Items.Where(item => item.State.GetStatus() == RatingResultStatus.Failure))
                {
                    log.ErrorFormat("Item Failure for asset no [{0}]", item.AssetNo);

                    foreach (var error in item.State.Errors)
                    {
                        log.Error(error);
                    }
                }
            }
        }
    }
}