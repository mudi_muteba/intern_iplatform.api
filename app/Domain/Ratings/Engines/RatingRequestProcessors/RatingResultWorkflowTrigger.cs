using System.Linq;
using Common.Logging;
using Domain.Admin;
using Domain.Admin.Queries;
using Domain.Base.Events;
using Domain.Ratings.Events;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Engines.RatingRequestProcessors
{
    public class RatingResultWorkflowTrigger : IProcessRatingResult
    {
        private static readonly ILog log = LogManager.GetLogger<RatingResultWorkflowTrigger>();
        private readonly IPublishEvents eventPublisher;
        private readonly GetChannelBySystemIdQuery query;

        public RatingResultWorkflowTrigger(IPublishEvents eventPublisher
            , GetChannelBySystemIdQuery query)
        {
            this.eventPublisher = eventPublisher;
            this.query = query;
        }

        public void Process(RatingRequestDto request, RatingResultDto result, RatingConfiguration ratingConfiguration)
        {
            var channel = query.WithSystemId(request.RatingContext.ChannelId).ExecuteQuery().FirstOrDefault();

            if (channel == null)
            {
                log.ErrorFormat("Could not find channel with external id '{0}'", request.RatingContext.ChannelId);
                return;
            }

            TriggerWorkflow(request, result, channel);
        }

        private void TriggerWorkflow(RatingRequestDto request, RatingResultDto result, ChannelReference channel)
        {
            var @event = new ExternalRatingRequestReceivedEvent(request, result, request.CreateAudit(), channel);

            eventPublisher.Publish(@event);
        }
    }
}