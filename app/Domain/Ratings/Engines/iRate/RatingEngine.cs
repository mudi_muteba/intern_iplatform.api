﻿using System.Collections.Generic;
using Domain.Ratings.Engines.RatingRequestProcessors;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Engines.iRate
{
    public class RatingEngine : IRatingEngine
    {
        private readonly IIRateProvider provider;

        public RatingEngine(IIRateProvider provider )
        {
            this.provider = provider;
        }

        public RatingResultDto PerformRating(RatingRequestDto request)
        {
            var response = provider.GetRates(request);


            return response;
        }

    }
}