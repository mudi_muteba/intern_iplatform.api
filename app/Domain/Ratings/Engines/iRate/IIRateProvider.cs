﻿using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Engines.iRate
{
    public interface IIRateProvider
    {
        RatingResultDto GetRates(RatingRequestDto request);
    }
}