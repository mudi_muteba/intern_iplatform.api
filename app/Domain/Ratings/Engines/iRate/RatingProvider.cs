﻿using System;
using Common.Logging;
using Infrastructure.Configuration;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using Newtonsoft.Json;
using RestSharp;
using Shared;

namespace Domain.Ratings.Engines.iRate
{
    public class RatingProvider : IIRateProvider
    {
        private static readonly ILog log = LogManager.GetLogger<RatingProvider>();
        private readonly string url;

        public RatingProvider()
        {
            url = ConfigurationReader.ExternalServices.iRateUrl;
        }

        public RatingResultDto GetRates(RatingRequestDto request)
        {
            var client = new RestClient(url);
            var query = new RestRequest("Rate", Method.POST)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new JsonSerializerRestSharp()
            };

            query.AddBody(request);

            IRestResponse response;

            try
            {
                log.InfoFormat("Calling iRate at: {0}", client.BaseUrl);

                response = client.Execute<RatingResultDto>(query);
            }
            catch (Exception ex)
            {
                var error = string.Format("Could not execute request with Id '{0}'. Exception details: {1}",
                    request.Id,
                    new ExceptionPrettyPrinter().Print(ex));

                var state = RatingResultState.CreateWithError(error);

                log.ErrorFormat("Returning Error result: {0}", error);

                return new RatingResultDto { State = state };
            }

            try
            {
                log.InfoFormat("Converting response from iRate with length: {0}", response.Content.Length);

                var result = JsonConvert.DeserializeObject<RatingResultDto>(response.Content);
                if (result == null)
                {
                    var error = string.Format("Invalid response received from irate for request with Id '{0}'. The response was {1}", request.Id, response.Content);
                    log.ErrorFormat(error);
                    return new RatingResultDto { State = RatingResultState.CreateWithError(error) };
                }

                log.InfoFormat("Returning result with status: {0}", result.State.GetStatus().ToString());

                return result;
            }
            catch (JsonReaderException jsonException)
            {
                var error =
                    string.Format(
                        "Could not de-serialize rating result for request with Id '{0}'. The exception was {1}",
                        request.Id,
                        new ExceptionPrettyPrinter().Print(jsonException));

                var state = RatingResultState.CreateWithError(error);

                log.ErrorFormat("Returning Error result: {0}", error);
                log.ErrorFormat("Unable to process iRate response: {0}", response.Content);

                return new RatingResultDto { State = state };
            }
        }
    }
}