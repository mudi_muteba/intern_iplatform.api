﻿using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Engines
{
    public interface IRatingEngine
    {
        RatingResultDto PerformRating(RatingRequestDto request);
    }
}