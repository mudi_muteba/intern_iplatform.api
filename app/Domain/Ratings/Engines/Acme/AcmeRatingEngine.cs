﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;

namespace Domain.Ratings.Engines.Acme
{
    public class AcmeRatingEngine //: IRatingEngine
    {
        public RatingResultDto PerformRating(RatingRequestDto request)
        {
            return new RatingResultDto()
            {
                Policies = new List<RatingResultPolicyDto>()
                {
                    new RatingResultPolicyDto()
                    {
                        State = new RatingResultState()
                        {
                            Errors = new List<string>(),
                            Warnings = new List<string>()
                        },
                        ProductCode = "MULMOT",
                        ExtraITCScore = "13",
                        Fees = 123.56m,
                        ITCScore = "2",
                        InsurerCode = "STM",
                        InsurerProductCode = string.Empty,
                        InsurerReference = "INSURER_REFERENCE",
                        Items = new List<RatingResultItemDto>()
                        {
                            new RatingResultItemDto()
                            {
                                AssetNo = 1,
                                Cover = Covers.Motor,
                                Excess = new RatingResultItemExcessDto()
                                {
                                    Basic = 1000.00m,
                                    Calculated = true
                                },
                                Premium = 700.89m,
                                PremiumWithFees = 935.12m,
                                Sasria = 2.00m,
                                State = new RatingResultState()
                                {
                                    Errors = new List<string>(),
                                    Warnings = new List<string>()
                                },
                                SumInsured = 123456.99m,
                                Fees = new ItemFeesDto()
                                {
                                    Fees = new List<ItemFeeDto>()
                                    {
                                        new ItemFeeDto()
                                        {
                                            Fee = 123.45m
                                        }
                                    }
                                }
                            }
                        }
                    },
                    new RatingResultPolicyDto()
                    {
                        State = new RatingResultState()
                        {
                            Errors = new List<string>(),
                            Warnings = new List<string>()
                        },
                        ProductCode = "DOTSURE",
                        ExtraITCScore = "13",
                        Fees = 123.56m,
                        ITCScore = "2",
                        InsurerCode = "LLOYDS",
                        InsurerProductCode = string.Empty,
                        InsurerReference = "INSURER_REFERENCE_DOTSURE",
                        Items = new List<RatingResultItemDto>()
                        {
                            new RatingResultItemDto()
                            {
                                AssetNo = 1,
                                Cover = Covers.Motor,
                                Excess = new RatingResultItemExcessDto()
                                {
                                    Basic = 1000.00m,
                                    Calculated = true
                                },
                                Premium = 128.89m,
                                PremiumWithFees = 635.12m,
                                Sasria = 2.00m,
                                State = new RatingResultState()
                                {
                                    Errors = new List<string>(),
                                    Warnings = new List<string>()
                                },
                                SumInsured = 123456.99m,
                                Fees = new ItemFeesDto()
                                {
                                    Fees = new List<ItemFeeDto>()
                                    {
                                        new ItemFeeDto()
                                        {
                                            Fee = 145.45m
                                        }
                                    }
                                }
                            }
                        }
                    },
                },
                State = new RatingResultState()
                {
                    Errors = new List<string>(),
                    Warnings = new List<string>()
                }
            };
        }
    }
}