using System.Linq;
using AutoMapper;
using Domain.Ratings.Events;
using Domain.Ratings.Workflow.Tasks;

namespace Domain.Ratings.Mappings
{
    public class RatingWorkflowMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ExternalRatingRequestReceivedEvent, CreateProposalFromExternalRatingRequestMessage>()
                .ConvertUsing(@event =>
                {
                    var channelId = @event.ChannelReferences.FirstOrDefault().Id;

                    return new CreateProposalFromExternalRatingRequestMessage(new ExternalRatingRequestCompletedMetadata(channelId,  @event.Request, @event.Result));
                });
        }
    }
}