﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Ratings.Response;
using iPlatform.Api.DTOs.Ratings.Quoting;

namespace Domain.Ratings.Mappings
{
    public class RatingsMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<HandlerResult<RatingResultDto>, POSTResponseDto<RatingResultDto>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.ResolveUsing(result =>
                {
                    var list = new List<ResponseErrorMessage>();

                    list.AddRange(result.Authorisation.DTOMessages);
                    list.AddRange(result.Execution.DTOMessages);
                    list.AddRange(result.Validation.DTOMessages);

                    return list;
                }))
                ;

           Mapper.CreateMap <HandlerResult<QuoteAcceptanceResponseDto>, POSTResponseDto<QuoteAcceptanceResponseDto>>()
               .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
                ;

        }
    }
}