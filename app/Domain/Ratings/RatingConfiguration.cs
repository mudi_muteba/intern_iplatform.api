using System;
using Domain.Base;

namespace Domain.Ratings
{
    public class RatingConfiguration : Entity
    {
        public RatingConfiguration()
        {
        }

        public RatingConfiguration(int channelId, Guid channelSystemId, int productId, string ratingPassword, string ratingAgentCode,
            string ratingBrokerCode, string ratingAuthCode, string ratingSchemeCode, string ratingToken,
            string ratingEnvironment, string ratingUserId, string ratingSubscriberCode, string ratingCompanyCode,
            string ratingUwCompanyCode, string ratingUwProductCode, bool itcCheckRequired, string itcSubscriberCode,
            string itcBranchNumber, string itcBatchNumber, string itcSecurityCode, string itcEnquirerContactName,
            string itcEnquirerContactPhoneNo, string itcEnvironment, string productCode, string insurerCode)
        {
            ChannelId = channelId;
            ChannelSystemId = channelSystemId;
            ProductId = productId;
            RatingPassword = ratingPassword;
            RatingAgentCode = ratingAgentCode;
            RatingBrokerCode = ratingBrokerCode;
            RatingAuthCode = ratingAuthCode;
            RatingSchemeCode = ratingSchemeCode;
            RatingToken = ratingToken;
            RatingEnvironment = ratingEnvironment;
            RatingUserId = ratingUserId;
            RatingSubscriberCode = ratingSubscriberCode;
            RatingCompanyCode = ratingCompanyCode;
            RatingUwCompanyCode = ratingUwCompanyCode;
            RatingUwProductCode = ratingUwProductCode;
            ITCCheckRequired = itcCheckRequired;
            ITCSubscriberCode = itcSubscriberCode;
            ITCBranchNumber = itcBranchNumber;
            ITCBatchNumber = itcBatchNumber;
            ITCSecurityCode = itcSecurityCode;
            ITCEnquirerContactName = itcEnquirerContactName;
            ITCEnquirerContactPhoneNo = itcEnquirerContactPhoneNo;
            ITCEnvironment = itcEnvironment;
            ProductCode = productCode;
            InsurerCode = insurerCode;
        }

        public virtual int ChannelId { get; set; }
        public virtual Guid ChannelSystemId { get; protected internal set; }
        public virtual int ProductId { get; protected internal set; }
        public virtual string RatingPassword { get; protected internal set; }
        public virtual string RatingAgentCode { get; protected internal set; }
        public virtual string RatingBrokerCode { get; protected internal set; }
        public virtual string RatingAuthCode { get; protected internal set; }
        public virtual string RatingSchemeCode { get; protected internal set; }
        public virtual string RatingToken { get; protected internal set; }
        public virtual string RatingEnvironment { get; protected internal set; }
        public virtual string RatingUserId { get; protected internal set; }
        public virtual string RatingSubscriberCode { get; protected internal set; }
        public virtual string RatingCompanyCode { get; protected internal set; }
        public virtual string RatingUwCompanyCode { get; protected internal set; }
        public virtual string RatingUwProductCode { get; protected internal set; }
        public virtual bool ITCCheckRequired { get; protected internal set; }
        public virtual string ITCSubscriberCode { get; protected internal set; }
        public virtual string ITCBranchNumber { get; protected internal set; }
        public virtual string ITCBatchNumber { get; protected internal set; }
        public virtual string ITCSecurityCode { get; protected internal set; }
        public virtual string ITCEnquirerContactName { get; protected internal set; }
        public virtual string ITCEnquirerContactPhoneNo { get; protected internal set; }
        public virtual string ITCEnvironment { get; protected internal set; }
        public virtual string ProductCode { get; protected internal set; }
        public virtual string InsurerCode { get; protected internal set; }
    }
}