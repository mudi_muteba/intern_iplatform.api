using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;

namespace Domain.Ratings
{
    public static class SasriaCalculator
    {
        public static decimal Calculate(Cover cover, PaymentPlan paymentPlan, decimal sumInsured, decimal premium)
        {
            // TODO
            // 1) Different Payment Plans
            // 2) Sasria effective dates
            // 3) Other sections

            if (cover.Id.Equals(Covers.Motor.Id))
                return 2;

            if (cover.Id.Equals(Covers.CaravanOrTrailer.Id))
                return 2;

            if (cover.Id.Equals(Covers.PersonalLegalLiability.Id))
                return 0;

            if (cover.Id.Equals(Covers.IdentityTheft.Id))
                return 0;

            if (cover.Id.Equals(Covers.DisasterCash.Id))
                return 0;

            if (cover.Id.Equals(Covers.AIGAssist.Id))
                return 0;

            if (cover.Id.Equals(Covers.MotorWarranty.Id))
                return 0;

            return (premium > 0 && sumInsured > 0 ? sumInsured * (0.0036m / 100) / 10 : 0);
        }

        public static void Shortfall(RatingResultPolicyDto policy, PaymentPlan paymentPlan)
        {
            var minSasria = 3.00m;

            // this list will change based on new covers being handled by iPlatform
            var sasriaF1Class = new List<Cover>()
            {
                Covers.AllRisk,
                Covers.Building,
                Covers.HouseOwners,
                Covers.Contents
            };

            var sasriaF1ClassItems = policy.Items
                .Where(i => sasriaF1Class.Any(c => c.Id == i.Cover.Id))
                .ToList();

            if (!sasriaF1ClassItems.Any())
                return;

            var currentSasria = sasriaF1ClassItems
                .Sum(i => i.SasriaCalulated);

            if (currentSasria > minSasria)
                return;

            var shortfallAllocation = (minSasria - currentSasria) / sasriaF1ClassItems.Count;

            foreach (var item in sasriaF1ClassItems)
            {
                item.SasriaShortfall = shortfallAllocation;
                item.Sasria = item.SasriaShortfall + item.SasriaCalulated;
            }

            var adjustedSasria = sasriaF1ClassItems
                .Sum(i => i.Sasria);

            var adjustableItem = sasriaF1ClassItems.First();
            adjustableItem.SasriaShortfall += (minSasria - adjustedSasria);
            adjustableItem.Sasria = adjustableItem.SasriaCalulated + adjustableItem.SasriaShortfall;
        }
    }
}