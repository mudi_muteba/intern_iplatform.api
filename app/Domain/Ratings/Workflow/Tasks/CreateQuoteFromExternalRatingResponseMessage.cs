﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Ratings.Workflow.Tasks
{
    public class CreateQuoteFromExternalRatingResponseMessage : WorkflowExecutionMessage
    {
        public CreateQuoteFromExternalRatingResponseMessage()
        {
        }

        public CreateQuoteFromExternalRatingResponseMessage(ITaskMetadata metadata) : base(metadata)
        {
        }
    }
}