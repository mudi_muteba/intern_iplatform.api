﻿using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Ratings.Workflow.Tasks
{
    public class ExternalRatingRequestCompletedMetadata : ITaskMetadata
    {
        public int ChannelId { get; set; }
        public RatingRequestDto Request { get; set; }
        public RatingResultDto Result { get; set; }

        public ExternalRatingRequestCompletedMetadata(int channelId, RatingRequestDto request, RatingResultDto result)
        {
            ChannelId = channelId;
            Request = request;
            Result = result;
        }
    }
}