﻿using Domain.Base.Workflow;
using iPlatform.Enums.Workflows;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Ratings.Workflow.Tasks
{
    [WorkflowExecutionMessageType(WorkflowMessageType.CreateProposalFromExternalRatingRequest)]
    public class CreateProposalFromExternalRatingRequestMessage : WorkflowExecutionMessage
    {
        public CreateProposalFromExternalRatingRequestMessage()
        {
        }

        public CreateProposalFromExternalRatingRequestMessage(ITaskMetadata metadata) : base(metadata)
        {
        }
    }
}