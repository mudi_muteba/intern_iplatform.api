﻿using System;
using Common.Logging;
using Domain.Ratings.Workflow.Consumers.Proposals;
using Domain.Ratings.Workflow.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Ratings.Workflow.Consumers
{
    public class CreateProposalFromExternalRatingRequestMessageConsumer :
        AbstractMessageConsumer<CreateProposalFromExternalRatingRequestMessage>
    {
        private static readonly ILog log = LogManager.GetLogger<CreateProposalFromExternalRatingRequestMessageConsumer>();
        private IConnector connector;

        public CreateProposalFromExternalRatingRequestMessageConsumer(IWorkflowRouter router
            , IWorkflowExecutor executor
            , IConnector connector
            )
            : base(router, executor)
        {
            this.connector = connector;
        }

        public override void Consume(CreateProposalFromExternalRatingRequestMessage message)
        {
            log.InfoFormat("Consuming CreateProposalFromExternalRatingRequestMessage message");

            var metadata = ConfirmMetadata(message);

            connector = connector.Reset(metadata.Request.Context.Token);

            var proposalHeaderId = CreateProposal(metadata);

            CreateQuote(metadata, proposalHeaderId);
        }

        private void CreateQuote(ExternalRatingRequestCompletedMetadata metadata, int proposalHeaderId)
        {
            var quote = new CreateQuoteFromRatingRequest(metadata, connector).Create();

            if (quote == 0)
            {
                ThrowException(string.Format("Failed to create quote for request {0} (with proposal {1})"
                    , metadata.Request.Id
                    , proposalHeaderId));
            }

            log.InfoFormat("Quote {0} created for proposal {1} and request {2}"
                , quote
                , proposalHeaderId
                , metadata.Request.Id);
        }

        private int CreateProposal(ExternalRatingRequestCompletedMetadata metadata)
        {
            var proposalCreation = new CreateProposalFromRatingRequest(metadata, connector);

            var proposalHeaderId = proposalCreation.Create();

            if (proposalHeaderId == 0)
            {
                ThrowException(string.Format("Failed to create a proposal for request {0}", metadata.Request.Id));
            }

            log.InfoFormat("Proposal {0} was created for request {1}", proposalHeaderId, metadata.Request.Id);
            return proposalHeaderId;
        }

        private ExternalRatingRequestCompletedMetadata ConfirmMetadata(CreateProposalFromExternalRatingRequestMessage message)
        {
            var metadata = message.Metadata as ExternalRatingRequestCompletedMetadata;

            if (metadata == null)
            {
                var errorMessage = string.Format(@"Received CreateProposalFromExternalRatingRequestMessage message. 
                                    Expected metadata of type 'ExternalRatingRequestCompletedMetadata', but received '{0}'", message.Metadata.GetType());

                ThrowException(errorMessage);
            }
            return metadata;
        }

        private void ThrowException(string message)
        {
            log.ErrorFormat(message);

            throw new Exception(message);
        }
    }
}