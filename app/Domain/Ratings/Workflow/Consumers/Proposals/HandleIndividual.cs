using System;
using System.Linq;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.ContactDetail;
using iPlatform.Api.DTOs.Ratings.Request;

namespace Domain.Ratings.Workflow.Consumers.Proposals
{
    internal class HandleIndividual
    {
        private static readonly ILog log = LogManager.GetLogger<HandleIndividual>();
        private readonly IConnector connector;
        private readonly int currentChannelId;
        private readonly RatingRequestDto request;

        public HandleIndividual(RatingRequestDto request
            , int currentChannelId
            , IConnector connector)
        {
            this.request = request;
            this.currentChannelId = currentChannelId;
            this.connector = connector;
        }

        public int Get()
        {
            var person = request.Policy.Persons.FirstOrDefault();

            ValidateRequest(person);

            var searchDto = new IndividualSearchDto()
            {
                IdNumber = person.IdNumber
            };

            var individualResponse = connector.IndividualManagement.Individuals.SearchIndividual(searchDto);

            if (!individualResponse.IsSuccess)
            {
                ThrowConnectorException(individualResponse
                    , string.Format("Failed to search for individual with id {0} for request {1}"
                        , person.IdNumber, request.Id));
            }

            if (!individualResponse.Response.Results.Any())
            {
                log.InfoFormat("No individual found for request {0} with id number {1}", request.Id, person.IdNumber);
                return CreateIndividual(person);
            }

            return individualResponse.Response.Results.FirstOrDefault().Id;
        }

        private void ValidateRequest(RatingRequestPersonDto person)
        {
            if (person == null)
            {
                ThrowException(string.Format("No person available on request {0}", request.Id));
            }

            if (string.IsNullOrWhiteSpace(person.IdNumber))
            {
                ThrowException(string.Format("No ID number found for request {0}", person.IdNumber));
            }
        }

        private int CreateIndividual(RatingRequestPersonDto person)
        {
            var createIndividualDto = new CreateIndividualDto()
            {
                ChannelId = currentChannelId,
                ContactDetail = new CreateContactDetailDto()
                {
                    Email = person.Email,
                    Work = person.PhoneWork,
                    Home = person.PhoneWork,
                    Cell = person.PhoneCell,
                },
                DateOfBirth = person.DateOfBirth,
                FirstName = person.FirstName,
                Surname = person.Surname,
                Title = person.Title,
                Language = person.Language,
                MaritalStatus = person.MaritalStatus,
                IdentityNo = person.IdNumber
            };

            var createResponse = connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

            if (!createResponse.IsSuccess)
            {
                ThrowConnectorException(createResponse, string.Format("Failed to create individual for request {0} with id {1}", request.Id, person.IdNumber));
            }

            return createResponse.Response;
        }

        private void ThrowConnectorException(BaseResponseDto response, string baseMessage)
        {
            var errorMessage = string.Format("{0}{1}{2}. More info: {3}"
                , baseMessage
                , request.Id
                , Environment.NewLine
                , response.PrintErrors());

            ThrowException(errorMessage);
        }

        private void ThrowException(string message)
        {
            log.ErrorFormat(message);

            throw new Exception(message);
        }
    }
}