using System;
using Common.Logging;
using Domain.Party.ProposalHeaders;
using Domain.Products;
using Domain.Ratings.Workflow.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Ratings.Request;

namespace Domain.Ratings.Workflow.Consumers.Proposals
{
    internal class CreateProposalFromRatingRequest
    {
        private readonly int channelId;
        private readonly IConnector connector;
        private static readonly ILog log = LogManager.GetLogger<CreateProposalFromRatingRequest>();
        private readonly RatingRequestDto request;

        public CreateProposalFromRatingRequest(ExternalRatingRequestCompletedMetadata metadata
            , IConnector connector)
        {
            this.channelId = metadata.ChannelId;
            this.connector = connector;
            this.request = metadata.Request;
        }

        public int Create()
        {
            var individualId = GetIndividual();

            var proposalHeaderId = CreateProposal(individualId);

            PopulateProposal(individualId, proposalHeaderId);

            return proposalHeaderId;
        }

        private void PopulateProposal(int individualId, int proposalHeaderId)
        {
            var builder = new ProposalDtoBuilder().For(request);

            foreach (var item in request.Items)
            {
                var productId = Product.MultiQuoteProductId;
                if (!string.IsNullOrWhiteSpace(item.ProductCode))
                {
                    var productResponse = connector.ProductManagement.Products.Get(item.ProductCode);

                    if (!productResponse.IsSuccess)
                    {
                        ThrowConnectorException(productResponse, string.Format("Failed to get product by code. Code was {0}", item.ProductCode));
                    }
                }

                var createProposalDefinition = builder.CreateProposalDefinitionDto(productId, proposalHeaderId, item);

                var definitionResponse = connector.IndividualManagement
                    .Individual(individualId)
                    .Proposal(proposalHeaderId)
                    .CreateProposalDefinition(createProposalDefinition);

                if (!definitionResponse.IsSuccess)
                {
                    ThrowConnectorException(definitionResponse, string.Format("Failed to create proposal definition for request {0} item {1}"
                        , request.Id
                        , item.AssetNo));
                }
            }
        }

        private int CreateProposal(int individualId)
        {
            var builder = new ProposalDtoBuilder();

            var proposalHeaderDto = builder
                .For(request)
                .CreateProposalHeaderDto(individualId);

            var createResponse = connector.IndividualManagement
                .Individual(individualId)
                .Proposals()
                .CreateProposalHeader(proposalHeaderDto);

            if (!createResponse.IsSuccess)
            {
                ThrowConnectorException(createResponse, string.Format("Failed to create proposal header for request {0}", request.Id));
            }

            return createResponse.Response;
        }


        private int GetIndividual()
        {
            var handler = new HandleIndividual(request, channelId, connector);

            return handler.Get();
        }

        private void ThrowConnectorException(BaseResponseDto response, string baseMessage)
        {
            var errorMessage = string.Format("{0}{1}{2}. More info: {3}"
                , baseMessage
                , request.Id
                , Environment.NewLine
                , response.PrintErrors());

            ThrowException(errorMessage);
        }

        private void ThrowException(string message)
        {
            log.ErrorFormat(message);

            throw new Exception(message);
        }

    }
}