using System;
using Common.Logging;
using Domain.Ratings.Workflow.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Workflow.Consumers.Proposals
{
    internal class CreateQuoteFromRatingRequest
    {
        private readonly IConnector connector;
        private readonly RatingResultDto result;
        private readonly RatingRequestDto request;
        private static readonly ILog log = LogManager.GetLogger<CreateQuoteFromRatingRequest>();

        public CreateQuoteFromRatingRequest(ExternalRatingRequestCompletedMetadata metadata
            , IConnector connector)
        {
            this.request = metadata.Request;
            this.result = metadata.Result;
            this.connector = connector;
        }

        public int Create()
        {
            result.RatingRequestId = request.Id;

            var creationResponse = connector.Ratings.CreateQuote(result);

            if (!creationResponse.IsSuccess)
            {
                ThrowConnectorException(creationResponse, string.Format("Failed to create quote for request {0}", request.Id));
            }

            return creationResponse.Response;
        }

        private void ThrowConnectorException(BaseResponseDto response, string baseMessage)
        {
            var errorMessage = string.Format("{0}{1}{2}. More info: {3}"
                , baseMessage
                , request.Id
                , Environment.NewLine
                , response.PrintErrors());

            ThrowException(errorMessage);
        }

        private void ThrowException(string message)
        {
            log.ErrorFormat(message);

            throw new Exception(message);
        }

    }
}