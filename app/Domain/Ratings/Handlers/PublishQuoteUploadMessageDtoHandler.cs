using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Admin.ChannelEvents;
using Domain.Admin.Queries;
using Domain.Admin.SettingsQuoteUploads;
using Domain.Admin.SettingsQuoteUploads.Queries;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Ratings.Events;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData.Authorisation;
using ValidationMessages.Channels;
using Common.Logging;

namespace Domain.Ratings.Handlers
{
    public class PublishQuoteUploadMessageDtoHandler : IHandleDto<PublishQuoteUploadMessageDto, QuoteAcceptanceResponseDto>
    {
        private readonly IProvideContext m_ContextProvider;
        private readonly GetChannelBySystemIdQuery m_ChannelQuery;
        private readonly GetChannelEventConfigurationQuery m_ChannelEventConfigurationQuery;
        private readonly GetSettingsQuoteAcceptanceQuery m_SettingsQuoteAcceptanceQuery;
        private readonly IPublishEvents m_EventPublisher;
        protected static readonly ILog m_Log = LogManager.GetLogger<PublishQuoteUploadMessageDtoHandler>();

        public PublishQuoteUploadMessageDtoHandler(
            IProvideContext contextProvider
            , GetChannelBySystemIdQuery channelQuery
            , GetChannelEventConfigurationQuery channelEventConfigurationQuery
            , GetSettingsQuoteAcceptanceQuery settingsQuoteAcceptanceQuery
            , IPublishEvents eventPublisher
            )
        {
            this.m_ContextProvider = contextProvider;
            this.m_ChannelQuery = channelQuery;
            this.m_ChannelEventConfigurationQuery = channelEventConfigurationQuery;
            this.m_SettingsQuoteAcceptanceQuery = settingsQuoteAcceptanceQuery;
            this.m_EventPublisher = eventPublisher;
        }

        public void Handle(PublishQuoteUploadMessageDto dto, HandlerResult<QuoteAcceptanceResponseDto> result)
        {
            var runningInChannelId = Allowed(dto);

            var channelEvents = GetChannelEventConfiguration(runningInChannelId, dto.Policy.ProductCode);

            AcceptQuote(runningInChannelId, dto, result, channelEvents);

            result.Processed(new QuoteAcceptanceResponseDto
            {
                QuoteReference = Guid.NewGuid()
            });
        }

        private void AcceptQuote(int channelId, PublishQuoteUploadMessageDto dto, HandlerResult<QuoteAcceptanceResponseDto> result, List<ChannelEvent> channelEvents)
        {
            var context = m_ContextProvider.Get();

            var channelReference = new Channel(channelId);
            channelReference.AddChannelConfiguration(channelEvents);

            var @event = new ExternalQuoteAcceptedWithUnderwritingEnabledEvent(
                dto,
                dto.Policy.ProductCode, 
                dto.Policy.InsurerCode, 
                context.CreateAudit(), 
                channelReference,
                GetSettingsQuoteUpload(dto)
                );

            m_EventPublisher.Publish(@event);
        }

        private List<ChannelEvent> GetChannelEventConfiguration(int channelId, string productCode)
        {
            var channelEvents = m_ChannelEventConfigurationQuery
                .ForChannel(channelId)
                .ForProduct(productCode)
                .EventName(typeof(ExternalQuoteAcceptedWithUnderwritingEnabledEvent).Name)
                .ExecuteQuery()
                .ToList();

            if (!channelEvents.Any())
            {
                throw new ValidationException(ChannelValidationMessages.NoChannelEventConfiguration(typeof(ExternalQuoteAcceptedWithUnderwritingEnabledEvent).Name));
            }

            return channelEvents;
        }

        private int Allowed(PublishQuoteUploadMessageDto dto)
        {
            var channelId = dto.Request.RatingContext.ChannelId;
            var matchingChannel = m_ChannelQuery.WithSystemId(channelId).ExecuteQuery().FirstOrDefault();

            if (matchingChannel == null)
            {
                throw new UnauthorisationException(RequiredRights);
            }
                

            var context = m_ContextProvider.Get();
            var allowed = context.Authorised(matchingChannel.Id, RequiredRights);
            if (!allowed)
            {
                throw new UnauthorisationException(RequiredRights);
            }
            return matchingChannel.Id;
        }

        public List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    SecondLevelUnderwritingAuthorisation.CanAcceptQuote
                };
            }
        }

        private List<SettingsQuoteUploadDto> GetSettingsQuoteUpload(PublishQuoteUploadMessageDto dto)
        {
            var settings = m_SettingsQuoteAcceptanceQuery
                                .WithChannelId(dto.ChannelInfo.Id)
                                .WithProductId(dto.Policy.ProductId)
                                .WithEnvironment(dto.Environment)
                                .IsPolicyBinding(false)
                                .ExecuteQuery().ToList();
            if (!settings.Any())
            {
                m_Log.ErrorFormat("No SettingsQuoteUpload found for Channel:{0} and Product:{1} and Environment:{2}", dto.ChannelInfo.Id, dto.Policy.ProductId, dto.Environment);
            }

            return GetSettingsQuoteUploadDto(settings);
        }

        private List<SettingsQuoteUploadDto> GetSettingsQuoteUploadDto(List<SettingsQuoteUpload> settings)
        {
            var settingsDto = new List<SettingsQuoteUploadDto>();

            settings.ForEach(x =>
            {
                settingsDto.Add(new SettingsQuoteUploadDto()
                {
                    SettingName = x.SettingName,
                    SettingValue = x.SettingValue                   
                });
            });

            //Adding SettingQuoteUpload m_Logging
            if (settingsDto.Any())
            {
                m_Log.InfoFormat("Using Settings below for Upload/Acceptance");
            }
                
            settingsDto.ForEach(x => {
                m_Log.InfoFormat("NAME/VALUE: {0} : {1}", x.SettingName, x.SettingValue);
            });
           
            return settingsDto;
        }

    }
}