﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin.Queries;
using Domain.Admin.SettingsiRates;
using Domain.Admin.SettingsiRateUser;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Ratings.Configuration;
using Domain.Ratings.Engines;
using Domain.Ratings.Engines.RatingRequestProcessors;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData.Authorisation;
using ValidationMessages.Ratings;

namespace Domain.Ratings.Handlers
{
    public class RatingRequestDtoHandler : IHandleDto<RatingRequestDto, RatingResultDto>
    {
        private readonly IProvideContext m_ContextProvider;
        private readonly IRatingEngine m_RatingEngine;
        private readonly GetRatingConfigurationByChannelQuery m_RatingSettingsQuery;
        private readonly GetRatingUserConfigurationByUser m_RatingUserSettingsQuery;
        private readonly IList<IProcessRatingResult> m_ResponseProcessors;

        public RatingRequestDtoHandler(
            IProvideContext contextProvider
            , IRatingEngine ratingEngine
            , GetRatingConfigurationByChannelQuery ratingSettingsQuery
            , IList<IProcessRatingResult> responseProcessors
            , GetRatingUserConfigurationByUser ratingUserSettingsQuery
            )
        {
            m_ContextProvider = contextProvider;
            m_RatingEngine = ratingEngine;
            m_RatingSettingsQuery = ratingSettingsQuery;
            m_ResponseProcessors = responseProcessors ?? new List<IProcessRatingResult>();
            m_RatingUserSettingsQuery = ratingUserSettingsQuery;
        }

        public void Handle(RatingRequestDto dto, HandlerResult<RatingResultDto> result)
        {
            var ratingConfiguration = GetRatingConfiguration(dto);

            if (ratingConfiguration.Any())
                Allowed(ratingConfiguration.FirstOrDefault());

            GetRatingUserConfiguration(dto);

            var response = ExecuteRating(dto, ratingConfiguration);

            ProcessResponse(dto, response, ratingConfiguration);

            result.Processed(response);
        }

        private RatingResultDto ExecuteRating(RatingRequestDto dto, List<RatingConfiguration> ratingConfiguration)
        {
            if (ratingConfiguration.Any())
                BuildContext(dto, ratingConfiguration);

            var ratingResultDto = m_RatingEngine.PerformRating(dto);

            return ratingResultDto;
        }

        private void ProcessResponse(RatingRequestDto request, RatingResultDto response, List<RatingConfiguration> ratingConfigurations)
        {
            var ratingConfiguration = ratingConfigurations.FirstOrDefault();

            foreach (var processor in m_ResponseProcessors)
            {
                processor.Process(request, response, ratingConfiguration);
            }
        }

        public List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    QuotingAuthorisation.CanQuote
                };
            }
        }

        private void BuildContext(RatingRequestDto request, List<RatingConfiguration> ratingConfiguration)
        {
            new RatingContextBuilder(request, ratingConfiguration)
                .Build();
        }

        private List<RatingConfiguration> GetRatingConfiguration(RatingRequestDto dto)
        {
            var ratingConfiguration =
                m_RatingSettingsQuery.ForChannel(dto.RatingContext.ChannelId).ExecuteQuery().ToList();
            return ratingConfiguration;
        }

        private void GetRatingUserConfiguration(RatingRequestDto dto)
        {
            var user = m_ContextProvider.Get().UserId;
            var ratingUserConfiguration = m_RatingUserSettingsQuery.ForUser(user).ExecuteQuery().ToList();
            dto.UserRatingSettings = Mapper.Map<List<SettingsiRateUser>, List<SettingsiRateUserDto>>(ratingUserConfiguration);
        }

        private void Allowed(RatingConfiguration ratingConfiguration)
        {
            var executionContext = m_ContextProvider.Get();

            var allowed = executionContext.Authorised(ratingConfiguration.ChannelId, RequiredRights);

            if (allowed)
                return;

            throw new UnauthorisationException(RequiredRights);
        }
    }
}