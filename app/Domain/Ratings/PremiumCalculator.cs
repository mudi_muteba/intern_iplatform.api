using MasterData;

namespace Domain.Ratings
{
    public static class PremiumCalculator
    {
        public static decimal Calculate(string productCode, PaymentPlan paymentPlan, decimal annualPremium)
        {
            var divisor = 12;
            if (paymentPlan.Id.Equals(PaymentPlans.Annual.Id)) divisor = 1;
            if (paymentPlan.Id.Equals(PaymentPlans.OnceOff.Id)) divisor = 1;
            if (paymentPlan.Id.Equals(PaymentPlans.BiAnnual.Id)) divisor = 2;
            if (paymentPlan.Id.Equals(PaymentPlans.Quarterly.Id)) divisor = 4;

            // Regent uses monthly premium as annual / 11
            if (paymentPlan.Id.Equals(PaymentPlans.Monthly.Id) && productCode == "REGPRD") divisor = 11;

            return annualPremium / divisor;
        }
    }
}