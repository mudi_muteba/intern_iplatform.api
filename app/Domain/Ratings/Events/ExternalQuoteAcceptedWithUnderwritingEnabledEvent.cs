﻿using System;
using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;

namespace Domain.Ratings.Events
{
    public class ExternalQuoteAcceptedWithUnderwritingEnabledEvent : BaseDomainEvent, IProductRelatedDomainEvent
    {
        public ExternalQuoteAcceptedWithUnderwritingEnabledEvent(PublishQuoteUploadMessageDto dto, string productCode, string insurerCode, EventAudit audit, ChannelReference channelReference, List<SettingsQuoteUploadDto> settingsQuoteUploads) 
            : base(audit, new List<ChannelReference>() { channelReference })
        {
            Dto = dto;
            ProductCode = productCode;
            InsurerCode = insurerCode;
            SettingsQuoteUploads = settingsQuoteUploads;
        }

        public PublishQuoteUploadMessageDto Dto { get; set; }
        public string ProductCode { get; set; }
        public string InsurerCode { get; set; }
        public List<SettingsQuoteUploadDto> SettingsQuoteUploads { get; set; }
    }
}