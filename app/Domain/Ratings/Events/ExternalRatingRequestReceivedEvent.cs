﻿using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Ratings.Events
{
    public class ExternalRatingRequestReceivedEvent : BaseDomainEvent, DelayedDomainEvent
    {
        public ExternalRatingRequestReceivedEvent(RatingRequestDto request
            , RatingResultDto result
            , EventAudit audit
            , ChannelReference channelReference) : base(audit, channelReference)
        {
            Request = request;
            Result = result;
        }

        public RatingRequestDto Request { get; set; }
        public RatingResultDto Result { get; set; }
    }
}