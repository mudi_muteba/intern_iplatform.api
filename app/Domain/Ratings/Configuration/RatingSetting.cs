﻿using System;
using Domain.Base;
using Domain.Products;

namespace Domain.Ratings.Configuration
{
    public class RatingSetting : Entity
    {
        public RatingSetting()
        {
        }

        public RatingSetting(Guid channelId, Product product)
        {
            ChannelId = channelId;
            Product = product;
        }

        public virtual Guid ChannelId { get; protected internal set; }
        public virtual Product Product { get; protected internal set; }
        public virtual string UserId { get; protected internal set; }
        public virtual string Password { get; protected internal set; }
        public virtual string AgentCode { get; protected internal set; }
        public virtual string BrokerCode { get; protected internal set; }
        public virtual string AuthCode { get; protected internal set; }
        public virtual string SchemeCode { get; protected internal set; }
        public virtual string Token { get; protected internal set; }
        public virtual string Environment { get; protected internal set; }
        public virtual string SubscriberCode { get; protected internal set; }
        public virtual string CompanyCode { get; protected internal set; }
        public virtual string UwCompanyCode { get; protected internal set; }
        public virtual string UwProductCode { get; protected internal set; }

        public virtual void AuthenticationDetails(string agentCode, string authCode, string brokerCode,
            string environment, string password, string schemeCode, string userId, string token, string subscriberCode,
            string companyCode, string uwCompanyCode, string uwProductCode)
        {
            AgentCode = agentCode;
            AuthCode = authCode;
            BrokerCode = brokerCode;
            Environment = environment;
            Password = password;
            SchemeCode = schemeCode;
            UserId = userId;
            Token = token;
            SubscriberCode = subscriberCode;
            CompanyCode = companyCode;
            UwCompanyCode = uwCompanyCode;
            UwProductCode = uwProductCode;
        }
    }
}