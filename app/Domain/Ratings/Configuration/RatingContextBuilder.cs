using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution.Exceptions;
using iPlatform.Api.DTOs.Ratings.Request;
using ValidationMessages;
using ValidationMessages.Ratings;

namespace Domain.Ratings.Configuration
{
    internal class RatingContextBuilder
    {
        private readonly List<RatingConfiguration> configuration;
        private readonly RatingRequestDto ratingRequestDto;

        public RatingContextBuilder(RatingRequestDto ratingRequestDto, List<RatingConfiguration> configuration)
        {
            this.ratingRequestDto = ratingRequestDto;
            this.configuration = configuration;
        }

        public void Build()
        {
            if (ratingRequestDto.RatingContext == null)
            {
                ratingRequestDto.RatingContext = new RatingContextDto();
            }

            ratingRequestDto.RatingContext.RatingEngines.Clear();

            foreach (var ratingConfiguration in configuration)
            {
                ratingRequestDto.RatingContext
                    .RatingEngines
                    .Add(CreateRatingSettingDto(ratingConfiguration));
            }

            var validation = new List<ValidationErrorMessage>();

            ValidateRatingContext(ratingRequestDto, validation);

            if (validation.Any())
            {
                throw new ValidationException(validation.ToArray());
            }

        }

        private void ValidateRatingContext(RatingRequestDto request, List<ValidationErrorMessage> list)
        {
            if (request.RatingContext == null)
            {
                list.Add(RatingRequestValidationMessages.NoRatingContext);
                return;
            }

            if (!request.RatingContext.RatingEngines.Any())
            {
                list.Add(RatingRequestValidationMessages.NoRatingEngines);
            }
        }


        private RatingSettingDto CreateRatingSettingDto(RatingConfiguration ratingConfiguration)
        {
            var ratingSetting = new RatingSettingDto()
            {
                Password = ratingConfiguration.RatingPassword,
                UserName = ratingConfiguration.RatingUserId,
                AuthCode = ratingConfiguration.RatingAuthCode,
                BrokerCode = ratingConfiguration.RatingBrokerCode,
                AgentCode = ratingConfiguration.RatingAgentCode,
                SchemeCode = ratingConfiguration.RatingSchemeCode,
                Token = ratingConfiguration.RatingToken,
                Environment = ratingConfiguration.RatingEnvironment,
                SubscriberCode = ratingConfiguration.RatingSubscriberCode,
                InsurerCode = ratingConfiguration.InsurerCode,
                ProductCode = ratingConfiguration.ProductCode,
                CompanyCode = ratingConfiguration.RatingCompanyCode,
                UwCompanyCode = ratingConfiguration.RatingUwCompanyCode,
                UwProductCode = ratingConfiguration.RatingUwProductCode,
                ITCSettings = new ITCSettingDto()
                {
                    BatchNumber = ratingConfiguration.ITCBatchNumber,
                    BranchNumber = ratingConfiguration.ITCBranchNumber,
                    CheckRequired = ratingConfiguration.ITCCheckRequired,
                    EnquirerContactName = ratingConfiguration.ITCEnquirerContactName,
                    EnquirerContactPhoneNo = ratingConfiguration.ITCEnquirerContactPhoneNo,
                    SecurityCode = ratingConfiguration.ITCSecurityCode,
                    SubscriberCode = ratingConfiguration.ITCSubscriberCode,
                    Environment = ratingConfiguration.ITCEnvironment
                }
            };

            return ratingSetting;
        }
    }
}