using System;
using Domain.Base;
using Domain.Products;

namespace Domain.Ratings.Configuration
{
    public class ITCSetting : EntityWithAudit
    {
        public ITCSetting()
        {
        }

        public ITCSetting(Guid channelId, Product product)
        {
            ChannelId = channelId;
            Product = product;
        }

        public ITCSetting(Guid channelId, Product product, bool checkRequired, string subscriberCode,
            string branchNumber, string batchNumber, string securityCode, string enquirerContactName,
            string enquirerContactPhoneNo, string environment)
        {
            ChannelId = channelId;
            Product = product;
            CheckRequired = checkRequired;
            SubscriberCode = subscriberCode;
            BranchNumber = branchNumber;
            BatchNumber = batchNumber;
            SecurityCode = securityCode;
            EnquirerContactName = enquirerContactName;
            EnquirerContactPhoneNo = enquirerContactPhoneNo;
            Environment = environment;
        }

        public virtual Guid ChannelId { get; protected internal set; }
        public virtual Product Product { get; protected internal set; }
        public virtual bool CheckRequired { get; protected internal set; }
        public virtual string SubscriberCode { get; protected internal set; }
        public virtual string BranchNumber { get; protected internal set; }
        public virtual string BatchNumber { get; protected internal set; }
        public virtual string SecurityCode { get; protected internal set; }
        public virtual string EnquirerContactName { get; protected internal set; }
        public virtual string EnquirerContactPhoneNo { get; protected internal set; }
        public virtual string Environment { get; protected internal set; }

        public virtual void Configuration(string batchNumber, string branchNumber, bool checkRequired,
            string enquirerContactName, string enquirerContactPhoneNo, string securityCode, string subscriberCode,
            string environment)
        {
            BatchNumber = batchNumber;
            BranchNumber = branchNumber;
            CheckRequired = checkRequired;
            EnquirerContactName = enquirerContactName;
            EnquirerContactPhoneNo = enquirerContactPhoneNo;
            SecurityCode = securityCode;
            SubscriberCode = subscriberCode;
            Environment = environment;
        }
    }
}