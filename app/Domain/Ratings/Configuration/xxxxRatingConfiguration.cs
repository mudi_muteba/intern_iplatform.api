using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;

namespace Domain.Ratings.Configuration
{
    public class xxxxRatingConfiguration
    {
        private readonly IList<ITCSetting> itcSettings;
        private readonly IList<RatingSetting> ratingSettings;
        private readonly IList<Channel> settings;

        public xxxxRatingConfiguration() : this(new List<Channel>(), new List<RatingSetting>(), new List<ITCSetting>())
        {
        }

        public xxxxRatingConfiguration(IList<Channel> settings, IList<RatingSetting> ratingSettings,
            IList<ITCSetting> itcSettings)
        {
            this.settings = settings ?? new List<Channel>();
            this.ratingSettings = ratingSettings ?? new List<RatingSetting>();
            this.itcSettings = itcSettings ?? new List<ITCSetting>();
        }

        // default or first activated
        public Guid GetChannelIdToUse()
        {
            var channelToUse = settings.FirstOrDefault(s => s.IsActive && s.IsDefault);

            if (channelToUse == null)
            {
                channelToUse = settings
                    .Where(s => s.IsActive)
                    .OrderBy(s => s.ActivatedOn)
                    .FirstOrDefault();
            }

            return channelToUse == null ? Guid.Empty : channelToUse.SystemId;
        }

        public IList<RatingSetting> GetRatingSettings(Guid channelId)
        {
            return ratingSettings
                .Where(rs => rs.ChannelId == channelId)
                .ToList();
        }

        public IList<ITCSetting> GetITCSettings(Guid channelId)
        {
            return itcSettings
                .Where(rs => rs.ChannelId == channelId)
                .ToList();
        }

        public bool IsActive(Guid channelId)
        {
            return settings.Any(s => s.SystemId.Equals(channelId) && s.IsActive);
        }
    }
}