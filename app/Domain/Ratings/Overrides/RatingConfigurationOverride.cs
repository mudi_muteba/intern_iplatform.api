﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Ratings.Overrides
{
    public class RatingConfigurationOverride : IAutoMappingOverride<RatingConfiguration>
    {
        public void Override(AutoMapping<RatingConfiguration> mapping)
        {
            mapping.ReadOnly();
            mapping.SchemaAction.None();
            mapping.Table("vw_rating_configuration");
        }
    }
}