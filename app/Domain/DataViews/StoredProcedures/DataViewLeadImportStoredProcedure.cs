﻿using AutoMapper;
using Domain.Base.Repository.StoredProcedures;
using Domain.DataViews.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.DataView.DataViewLeadImport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DataViews.StoredProcedures
{
    public class DataViewLeadImportStoredProcedure : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "DataView_LeadImport";
            }
        }

        public DataViewLeadImportStoredProcedure() { }

        public DataViewLeadImportStoredProcedure(SearchDataViewLeadImportDto searchDataViewLeadImportDto)
        {
            DataViewLeadImportStoredProcedureParameters parameters = Mapper.Map<DataViewLeadImportStoredProcedureParameters>(searchDataViewLeadImportDto);
            SetParameters(parameters);
        }
    }
}
