﻿using Domain.Base.Repository.StoredProcedures;
using System;

namespace Domain.DataViews.StoredProcedures.Parameters
{
    public class DataViewLeadImportStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int ChannelId { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }
    }
}
