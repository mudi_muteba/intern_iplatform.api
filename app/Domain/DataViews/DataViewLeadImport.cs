﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DataViews
{
    public class DataViewLeadImport : Entity
    {
        public DataViewLeadImport() { }

        public DataViewLeadImport(object[] obj)
        {
            LeadSystem = obj[0] != null ? (string)obj[0] : string.Empty;
            Channel = obj[1] != null ? (string)obj[1] : string.Empty;
            Campaign = obj[2] != null ? (string)obj[2] : string.Empty;
            LeadSource = obj[3] != null ? (string)obj[3] : string.Empty;
            LeadPassedBy = obj[4] != null ? (string)obj[4] : string.Empty;
            AgentName = obj[5] != null ? (string)obj[5] : string.Empty;
            if (obj[6] != null)
            {
                DateLeadImported = (DateTime)obj[6];
            }
            if (obj[7] != null)
            {
                DateOfSale = (DateTime)obj[7];
            }
            LeadReference = obj[8] != null ? (int)obj[8] : 0;
            Product = obj[9] != null ? (string)obj[9] : string.Empty;
            PolicyNo = obj[10] != null ? (string)obj[10] : string.Empty;
            Client = obj[11] != null ? (string)obj[11] : string.Empty;
            IDNumber = obj[12] != null ? (string)obj[12] : string.Empty;
            CellNumber = obj[13] != null ? (string)obj[13] : string.Empty;
            SoldPolicyPremium = obj[14] != null ? (decimal)obj[14] : 0;
            Asset = obj[15] != null ? (string)obj[15] : string.Empty;
            LeadType = obj[16] != null ? (string)obj[16] : string.Empty;
            Quoted = obj[17] != null ? (int)obj[17] == 1 : false;
            Sale = obj[18] != null ? (int)obj[18] == 1: false;
        }

        public virtual string LeadSystem { get; set; }

        public virtual string Channel { get; set; }

        public virtual string Campaign { get; set; }

        public virtual string LeadSource { get; set; }

        public virtual string LeadPassedBy { get; set; }

        public virtual string AgentName { get; set; }

        public virtual DateTime? DateLeadImported { get; set; }

        public virtual DateTime? DateOfSale { get; set; }

        public virtual int LeadReference { get; set; }

        public virtual string Product { get; set; }

        public virtual string PolicyNo { get; set; }

        public virtual string Client { get; set; }

        public virtual string IDNumber { get; set; }

        public virtual string CellNumber { get; set; }

        public virtual decimal SoldPolicyPremium { get; set; }

        public virtual string Asset { get; set; }

        public virtual string LeadType { get; set; }

        public virtual bool Quoted { get; set; }

        public virtual bool Sale { get; set; }
    }
}
