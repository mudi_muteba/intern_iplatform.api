﻿using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.DataViews.StoredProcedures;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;

namespace Domain.DataViews.Queries
{
    public class DataViewLeadImportQuery : BaseStoredProcedureQuery<DataViewLeadImport, DataViewLeadImportStoredProcedure>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public DataViewLeadImportQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<DataViewLeadImport>()) { }

        protected internal override IList<DataViewLeadImport> Execute(IList<DataViewLeadImport> storedProcedure)
        {
            return storedProcedure;
        }
    }
}
