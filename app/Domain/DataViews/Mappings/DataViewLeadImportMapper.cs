﻿using AutoMapper;
using Domain.DataViews.StoredProcedures.Parameters;
using iPlatform.Api.DTOs.DataView.DataViewLeadImport;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;

namespace Domain.DataViews.Mappings
{
    public class DataViewLeadImportMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SearchDataViewLeadImportDto, DataViewLeadImportStoredProcedureParameters>()
                .ForMember(d => d.ChannelId, o => o.MapFrom(s => s.Channel.Id));

            Mapper.CreateMap<DataViewLeadImport, DataViewLeadImportDto>();

            Mapper.CreateMap<QueryResult<IList<DataViewLeadImportDto>>, POSTResponseDto<IList<DataViewLeadImportDto>>>()
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;
        }
    }
}
