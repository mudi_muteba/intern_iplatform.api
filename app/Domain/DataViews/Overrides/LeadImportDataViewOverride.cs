﻿using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Automapping;

namespace Domain.DataViews.Overrides
{
    public class LeadImportDataViewOverride : IAutoMappingOverride<DataViewLeadImport>
    {
        public void Override(AutoMapping<DataViewLeadImport> mapping)
        {
            mapping.SchemaAction.None();
        }
    }
}
