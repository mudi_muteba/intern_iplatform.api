﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.Addresses;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalDefinitions.Builders.AssetsBuilder;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Party.ProposalDefinitionQuestions.Handlers
{
    public class SaveProposalDefinitionAnswersDtoHandler : 
        ExistingEntityDtoHandler<ProposalDefinition, SaveProposalDefinitionAnswersDto, bool>
    {
        public SaveProposalDefinitionAnswersDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _executionPlan = executionPlan;
        }

        private IExecutionPlan _executionPlan;
        private readonly IRepository _repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override ProposalDefinition GetEntityById(SaveProposalDefinitionAnswersDto dto)
        {
            var proposalDefinition = repository.GetById<ProposalDefinition>(dto.Id);
            return repository.UnProxy<ProposalDefinition>(proposalDefinition);
        }

        protected override void HandleExistingEntityChange(ProposalDefinition entity, SaveProposalDefinitionAnswersDto dto, HandlerResult<bool> result)
        {
            //Update QuestionAnswers First
            UpdateQuestionAnswers(dto);
            //Update definition if AIGAssist
            UpdateProposalDefinitionAigAssist(entity, dto);

            var assetList = AssetHelper.GetAssetList(_repository, entity.ProposalHeader.Party.Id);
            assetList.AssetExtras.AddRange(dto.AssetExtras);

            foreach (var item in dto.Answers)
            {
                var questionAnswer = entity.ProposalQuestionAnswers.FirstOrDefault(x => x.Id == item.Id);
                if (questionAnswer != null)
                    questionAnswer.Answer = item.Answer;
                else
                    Log.ErrorFormat("Unable to retrieve ProposalQuestionAnswer with id {0}", item.Id.ToString());
            }

            result.Processed(true);
        }

        private void UpdateQuestionAnswers(SaveProposalDefinitionAnswersDto dto)
        {
            var answerIds = dto.Answers.Select(y => y.Id).ToList();

            //Get the related entities once
            var proposalQuestionAnswers = _repository
                .GetAll<ProposalQuestionAnswer>()
                .Where(x => answerIds.Contains(x.Id))
                .ToList();

            //Update the related entities
            proposalQuestionAnswers.ForEach(x =>
            {
                var answerDto = dto.Answers.FirstOrDefault(y => y.Id == x.Id);
                if (answerDto != null)
                    x.Answer = answerDto.Answer;
                _repository.Save(x);
            });
        }

        private void UpdateProposalDefinitionAigAssist(ProposalDefinition entity, SaveProposalDefinitionAnswersDto dto)
        {
            if (entity.Cover.Id == Covers.AIGAssist.Id)
            {
                var proposalDefinitionAnswerDto = dto.Answers.FirstOrDefault();
                if (proposalDefinitionAnswerDto == null)
                    return;

                var AddressId = 0;
                var addressid = proposalDefinitionAnswerDto.Answer;
                if (!int.TryParse(addressid, out AddressId))
                    return;

                var address = _repository.GetById<Address>(AddressId);
                if (address == null)
                    return;

                entity.Description = address.Description;
            }
        }
    }
}