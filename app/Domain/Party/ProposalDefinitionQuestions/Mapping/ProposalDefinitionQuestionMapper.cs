﻿using System.Linq;
using AutoMapper;
using Domain.Individuals;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party;
using Domain.Party.ProposalDefinitions;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;

namespace Domain.Party.ProposalDefinitionQuestions.Mapping
{
    public class ProposalDefinitionQuestionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ProposalDefinition, ProposalDefinitionQuestionsDto>()
                .ForMember(t => t.ProposalDefinition, o => o.MapFrom(s => s))
                .ForMember(t => t.QuestionDefinitions, o => o.Ignore());

        }
    }
}