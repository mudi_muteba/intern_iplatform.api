﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace Domain.Party.Addresses.Mappings
{
    public class AddressMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<List<ListAddressDto>, ListResultDto<ListAddressDto>>();

            Mapper.CreateMap<List<Address>, ListResultDto<ListAddressDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<CreateAddressDto, Address>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Latitude, o => o.MapFrom(s => string.IsNullOrEmpty(s.Latitude) ? "0" : s.Latitude))
                .ForMember(t => t.Longitude, o => o.MapFrom(s => string.IsNullOrEmpty(s.Longitude) ? "0" : s.Longitude))
                ;


            Mapper.CreateMap<EditAddressDto, Address>();
            Mapper.CreateMap<Address, ListAddressDto>();
            Mapper.CreateMap<Address, AddressDto>();

            Mapper.CreateMap<RatingRequestAddressDto, CreateAddressDto>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Latitude, o => o.MapFrom(s => "0"))
                .ForMember(t => t.Longitude, o => o.MapFrom(s => "0"))
                .ForMember(t => t.Line1, o => o.MapFrom(s => s.Address1))
                .ForMember(t => t.Line2, o => o.MapFrom(s => s.Address2))
                .ForMember(t => t.Line3, o => o.MapFrom(s => s.Address3))
                .ForMember(t => t.Line4, o => o.MapFrom(s => s.Suburb))
                .ForMember(t => t.IsDefault, o => o.MapFrom(s => s.DefaultAddress))
                .ForMember(t => t.AddressType, o => o.MapFrom(s => s.AddressType != null ? s.AddressType : AddressTypes.PhysicalAddress))
                .ForMember(t => t.Code, o => o.MapFrom(s => s.PostalCode))
                .ForMember(t => t.Country, o => o.MapFrom(s => s.Country))
                .ForMember(t => t.Complex, o => o.Ignore())
                .ForMember(t => t.StateProvince, o => o.MapFrom(s => new StateProvinces().FirstOrDefault(a=>a.Name.Equals(s.Province)) ))
                ;

            Mapper.CreateMap<List<RatingRequestAddressDto>, List<CreateAddressDto>>()
                .AfterMap((s,d) => {
                    s.ForEach(x => d.Add(Mapper.Map<CreateAddressDto>(x)));
                });

            Mapper.CreateMap<Address, BasicAddressDto>()
                .ForMember(t => t.AddressType, o => o.MapFrom(s => s.AddressType.Name))
                .ForMember(t => t.StateProvince, o => o.MapFrom(s => s.StateProvince.Name))
                ;

            Mapper.CreateMap<List<Address>, List<BasicAddressDto>>()
                 .AfterMap((s, d) => {
                     s.ForEach(x => d.Add(Mapper.Map<BasicAddressDto>(x)));
                 });

            Mapper.CreateMap<AddressDto, CreateAddressDto>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;
            Mapper.CreateMap<AddressDto, EditAddressDto>();
        }
    }
}