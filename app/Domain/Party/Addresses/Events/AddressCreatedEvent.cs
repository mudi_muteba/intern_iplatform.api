﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Party.Addresses.Events
{
    public class AddressCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public Address Address { get; private set; }
        public int UserId { get; private set; }

        public AddressCreatedEvent(Address _address, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> { channelReferences })
        {
            Address = _address;
        }
    }
}