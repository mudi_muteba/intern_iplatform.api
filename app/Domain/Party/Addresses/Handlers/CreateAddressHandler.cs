﻿using System.Collections.Generic;
using Domain.Base;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Address;
using MasterData.Authorisation;

namespace Domain.Party.Addresses.Handlers
{
    public class CreateAddressHandler : CreationDtoHandler<Address, CreateAddressDto, int>
    {
        public CreateAddressHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    AddressAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(Address entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Address HandleCreation(CreateAddressDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Domain.Party.Party>(dto.PartyId);

            Address address = party.AddAddress(dto);

            result.Processed(address.Id);
            return address;
        }
    }
}