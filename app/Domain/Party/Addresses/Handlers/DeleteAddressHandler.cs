﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Address;
using MasterData.Authorisation;

namespace Domain.Party.Addresses.Handlers
{
    public class DeleteAddressHandler : CreationDtoHandler<Address, DeleteAddressDto, int>
    {
        public DeleteAddressHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    AddressAuthorisation.Delete
                };
            }
        }

        protected override void EntitySaved(Address entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Address HandleCreation(DeleteAddressDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Domain.Party.Party>(dto.PartyId);
            Address Address = party.DeleteAddress(dto);

            result.Processed(dto.Id);
            return Address;
        }
    }
}