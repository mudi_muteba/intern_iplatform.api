﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Address;
using MasterData.Authorisation;

namespace Domain.Party.Addresses.Handlers
{
    public class UpdateAddressHandler : ExistingEntityDtoHandler<Party, EditAddressDto, int>
    {
        public UpdateAddressHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    AddressAuthorisation.Edit
                };
            }
        }

        protected override Party GetEntityById(EditAddressDto dto)
        {
            return repository.GetById<Party>(dto.PartyId);
        }

        protected override void HandleExistingEntityChange(Party entity, EditAddressDto dto, HandlerResult<int> result)
        {
            entity.UpdateAddress(dto);
        }
    }
}