﻿using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Automapping;

namespace Domain.Party.Addresses.Overrides
{
    public class AddressOverride : IAutoMappingOverride<Address>
    {
        public void Override(AutoMapping<Address> mapping)
        {
            mapping.References(m => m.StateProvince).Nullable();
        }
    }
}
