﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Addresses.Queries
{
    public class GetAddressesByPartyIdQuery : BaseQuery<Address>
    {
        private int _partyId;

        public GetAddressesByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Address>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetAddressesByPartyIdQuery WithPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<Address> Execute(IQueryable<Address> query)
        {
            return Repository.GetAll<Address>().Where(a => a.Party.Id == _partyId);
        }
    }
}