﻿using System;
using AutoMapper;
using Domain.Base;
using Domain.Base.Events;
using Domain.Party.Addresses.Events;
using iPlatform.Api.DTOs.Party.Address;
using MasterData;

namespace Domain.Party.Addresses
{
    public class Address : EntityWithParty
    {
        private DateTime? dateFrom;
        private string description;
        public virtual int MasterId { get; set; }

        public virtual string Description
        {
            get
            {
                if (string.IsNullOrEmpty(description))
                {
                    description = CreateDescription();
                }

                return description;
            }
            set { description = value; }
        }

        public virtual string Line1 { get; set; }

        public virtual string Line2 { get; set; }

        public virtual string Line3 { get; set; }

        public virtual string Line4 { get; set; }

        public virtual string Code { get; set; }

        public virtual string Latitude { get; set; }

        public virtual string Longitude { get; set; }

        public virtual StateProvince StateProvince { get; set; }

        public virtual AddressType AddressType { get; set; }

        public virtual bool IsDefault { get; set; }

        public virtual bool IsComplex { get; set; }

        public virtual string Complex { get; set; }

        public virtual DateTime? DateFrom
        {
            get { return dateFrom ?? (dateFrom = DateTime.UtcNow); }
            set { dateFrom = value; }
        }

        public virtual DateTime? DateTo { get; set; }

        private string CreateDescription()
        {
            return string.Format("{0}, {1}, {2}", Line1, Line4, Code);
        }

        public virtual void Update(EditAddressDto dto)
        {
            Mapper.Map(dto, this);
            RaiseEvent(new AddressUpdatedEvent(this, dto.CreateAudit(), Party.Channel));
        }
    }
}