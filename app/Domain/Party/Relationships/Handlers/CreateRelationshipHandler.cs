﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Relationships;
using MasterData.Authorisation;

namespace Domain.Party.Relationships.Handlers
{
    public class CreateRelationshipHandler : CreationDtoHandler<Relationship, CreateRelationshipDto, int>
    {
        private readonly IRepository _repository;

        public CreateRelationshipHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void EntitySaved(Relationship entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Relationship HandleCreation(CreateRelationshipDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Party>(dto.PartyId);
            var childParty = _repository.GetById<Party>(dto.ChildPartyId);

            if (dto.ChannelId <= 0)
                dto.ChannelId = GetDefaultChannelId(dto.Context);

            var relationship = Relationship.Create(party, childParty, dto);
            
            result.Processed(relationship.Id);

            return relationship;
        }

        private int GetDefaultChannelId(DtoContext context)
        {
            var channelId = 0;
            var user = _repository.GetById<User>(context.UserId);
            var firstOrDefault = user.Channels.FirstOrDefault(a => a.IsDefault);
            if (firstOrDefault != null)
                channelId = firstOrDefault.Id;

            return channelId;
        }
    }
}