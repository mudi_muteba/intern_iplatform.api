﻿using Domain.Admin;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns.Events;
using Domain.Party.Relationships.Events;
using iPlatform.Api.DTOs.Party.Relationships;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Party.Relationships.Handlers
{
    public class DeleteRelationshipHandler : BaseDtoHandler<DeleteRelationshipDto, int>
    {
        public DeleteRelationshipHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            this.repository = repository;
        }
        private IRepository repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void InternalHandle(DeleteRelationshipDto dto, HandlerResult<int> result)
        {
            var entities = repository.GetAll<Relationship>().Where(x => x.ChildParty.Id == dto.Id);

            foreach (var entity in entities)
            {
                entity.Delete();
                entity.RaiseEvent(new RelationshipDeletedEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel() }));
                result.Processed(dto.Id);
            }
        }
    }
}