﻿using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns.Events;
using Domain.Users;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Relationships;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Party.Relationships.Handlers
{
    public class UpdateRelationshipHandler : ExistingEntityDtoHandler<Relationship, EditRelationshipDto, int>
    {
        public UpdateRelationshipHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(Relationship entity, EditRelationshipDto dto,
            HandlerResult<int> result)
        {
            entity.Party = repository.GetById<Party>(dto.PartyId);
            entity.ChildParty = repository.GetById<Party>(dto.ChildPartyId);

            if (dto.ChannelId <= 0)
                dto.ChannelId = GetDefaultChannelId(dto.Context);

            entity.Update(dto);
            result.Processed(entity.Id);
        }

        private int GetDefaultChannelId(DtoContext Context)
        {
            var channelId = 0;
            var user = repository.GetById<User>(Context.UserId);
            var firstOrDefault = user.Channels.FirstOrDefault(a => a.IsDefault);

            if (firstOrDefault != null)
                channelId = firstOrDefault.Id;

            return channelId;
        }
    }
}