﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Relationships.Queries
{
    public class GetRelationshipByPartyIdQuery : BaseQuery<Relationship>
    {
        private int PartyId;
        public GetRelationshipByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Relationship>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetRelationshipByPartyIdQuery WithPartyId(int partyId)
        {
            this.PartyId = partyId;
            return this;
        }
        

        protected internal override IQueryable<Relationship> Execute(IQueryable<Relationship> query)
        {

                query = query.Where(x => x.Party.Id == PartyId && !x.ChildParty.IsDeleted);


            return query;
        }
    }
}