﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Relationships.Queries
{
    public class GetExistingRelationshipQuery : BaseQuery<Relationship>
    {
        private int PartyId;
        private int ChildPartyId;
        private int Id;
        public GetExistingRelationshipQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Relationship>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetExistingRelationshipQuery WithPartyId(int partyId)
        {
            this.PartyId = partyId;
            return this;
        }

        public GetExistingRelationshipQuery WithChildPartyId(int childPartyId)
        {
            this.ChildPartyId = childPartyId;
            return this;
        }

        public GetExistingRelationshipQuery WithId(int Id)
        {
            this.Id = Id;
            return this;
        }

        protected internal override IQueryable<Relationship> Execute(IQueryable<Relationship> query)
        {
            if (Id > 0)
                query = query.Where(x => x.Id == Id);

            if (PartyId > 0)
                query = query.Where(x => x.Party.Id == PartyId);

            if (ChildPartyId> 0)
                query = query.Where(x => x.ChildParty.Id == ChildPartyId);

            return query;
        }
    }
}