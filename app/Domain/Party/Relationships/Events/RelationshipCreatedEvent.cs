﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Party.Relationships;

namespace Domain.Party.Relationships.Events
{
    public class RelationshipCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public CreateRelationshipDto Relationship { get; private set; }

        public RelationshipCreatedEvent(CreateRelationshipDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            Relationship = dto;
        }
    }
}