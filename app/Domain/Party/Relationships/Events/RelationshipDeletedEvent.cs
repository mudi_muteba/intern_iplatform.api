﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Party.Relationships;

namespace Domain.Party.Relationships.Events
{
    public class RelationshipDeletedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public DeleteRelationshipDto Relationship { get; private set; }

        public RelationshipDeletedEvent(DeleteRelationshipDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            Relationship = dto;
        }
    }
}