﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Party.Relationships;

namespace Domain.Party.Relationships.Events
{
    public class RelationshipUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public EditRelationshipDto Relationship { get; private set; }

        public RelationshipUpdatedEvent(EditRelationshipDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            Relationship = dto;
        }
    }
}