﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Party.Relationships.Overrides
{
    public class RelationshipOverride : IAutoMappingOverride<Relationship>
    {
        public void Override(AutoMapping<Relationship> mapping)
        {
            mapping.References(x => x.Party).Cascade.All();
            mapping.References(x => x.ChildParty).LazyLoad(Laziness.NoProxy);
            mapping.Id(x => x.Id).Unique();
        }
    }
}