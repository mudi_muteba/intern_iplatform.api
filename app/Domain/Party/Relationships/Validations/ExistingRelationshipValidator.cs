﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Party.Relationships.Queries;
using iPlatform.Api.DTOs.Party.Relationships;
using ValidationMessages;


namespace Domain.Party.Relationships.Validation
{
    public class ExistingRelationshipValidator : IValidateDto<CreateRelationshipDto>, IValidateDto<EditRelationshipDto>
    {

        public ExistingRelationshipValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetExistingRelationshipQuery(contextProvider, repository);
        }
        private readonly GetExistingRelationshipQuery query;
        public void Validate(CreateRelationshipDto dto, ExecutionResult result)
        {
            if (dto.PartyId > 0 && dto.ChildPartyId > 0)
            {
                var existingRelationship = GetExistingRelationship(dto.PartyId, dto.ChildPartyId);

                if (existingRelationship.Any())
                {
                    result.AddValidationMessage(RelationshipValidationMessages.ExistingRelationship.AddParameters(new[] { existingRelationship.FirstOrDefault().Id.ToString() }));
                }
            }
        }

        public void Validate(EditRelationshipDto dto, ExecutionResult result)
        {
            if (dto.Id > 0)
            {
                var existingRelationship = GetExistingRelationshipById(dto.Id);

                if (!existingRelationship.Any())
                {
                    result.AddValidationMessage(RelationshipValidationMessages.IdRequired);
                }
            }
        }

        private IQueryable<Relationship> GetExistingRelationship(int partyId, int ChildPartyId)
        {
            return query.WithPartyId(partyId)
                        .WithChildPartyId(ChildPartyId)
                        .ExecuteQuery();
        }

        private IQueryable<Relationship> GetExistingRelationshipById(int Id)
        {
            return query.WithId(Id)
                        .ExecuteQuery();
        }
    }
}