﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Party.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Party.Relationships;
using ValidationMessages;

namespace Domain.Party.Relationships.Validations
{
    public class RelationshipPartyValidator : IValidateDto<CreateRelationshipDto>, IValidateDto<EditRelationshipDto>
    {
        public RelationshipPartyValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetPartyByIdQuery(contextProvider, repository);
        }

        private readonly GetPartyByIdQuery query;

        public void Validate(CreateRelationshipDto dto, ExecutionResult result)
        {
            if (dto.PartyId > 0)
            {
                var party = GetPartyById(dto.PartyId);
                if (!party.Any())
                {
                    result.AddValidationMessage(RelationshipValidationMessages.InvalidPartyId.AddParameters(new[] { dto.PartyId.ToString() }));
                }
            }

            if (dto.ChildPartyId > 0)
            {
                var childparty = GetPartyById(dto.ChildPartyId);
                if (!childparty.Any())
                {
                    result.AddValidationMessage(RelationshipValidationMessages.InvalidChildPartyId.AddParameters(new[] { dto.ChildPartyId.ToString() }));
                }
            }
        }

        public void Validate(EditRelationshipDto dto, ExecutionResult result)
        {
            var party = GetPartyById(dto.PartyId);

            if (!party.Any())
            {
                result.AddValidationMessage(RelationshipValidationMessages.InvalidPartyId.AddParameters(new[] { dto.PartyId.ToString() }));
            }


            var childparty = GetPartyById(dto.ChildPartyId);

            if (!childparty.Any())
            {
                result.AddValidationMessage(RelationshipValidationMessages.InvalidChildPartyId.AddParameters(new[] { dto.ChildPartyId.ToString() }));
            }
        }

        public IQueryable<Party> GetPartyById(int PartyId)
        {
            return query.WithId(PartyId).ExecuteQuery();
        }

    }
}
