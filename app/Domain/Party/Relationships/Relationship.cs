﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Admin;
using Domain.Base;
using Domain.Base.Events;
using Domain.Party.Relationships.Events;
using iPlatform.Api.DTOs.Party.Relationships;
using MasterData;

namespace Domain.Party.Relationships
{
    public class Relationship : EntityWithAudit, IEntityIncludeParty
    {
        public virtual int MasterId { get; set; }
        public virtual Party Party { get; set; }
        public virtual Party ChildParty { get; set; }
        public virtual RelationshipType RelationshipType { get; set; }
        public virtual string Functions { get; set; }
        public virtual string Claims { get; set; }
        public virtual string Other { get; set; }

        public static Relationship Create(Party party, Party childParty, CreateRelationshipDto dto)
        {
            var relationship = Mapper.Map<Relationship>(dto);
            relationship.Party = party;
            relationship.ChildParty = childParty;

            relationship.RaiseEvent(new RelationshipCreatedEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(dto.ChannelId) }));
            return relationship;
        }

        public virtual void Update(EditRelationshipDto dto)
        {
            Mapper.Map(dto, this);
            RaiseEvent(new RelationshipUpdatedEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(dto.ChannelId) }));
        }

        public virtual void Delete(DeleteRelationshipDto dto)
        {
            Delete();
        }
    }
}
