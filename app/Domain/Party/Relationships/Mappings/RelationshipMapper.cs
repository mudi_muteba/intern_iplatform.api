﻿
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Relationships;
using MasterData;
using System;
using System.Collections.Generic;
using Domain.Individuals;
using Domain.Party.Contacts;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party;
using NHibernate.Proxy;

namespace Domain.Party.Relationships.Mappings
{
    public class RelationshipMapper : Profile
    {

        protected override void Configure()
        {
            Mapper.CreateMap<Relationship, RelationshipDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                .ForMember(t => t.ChildPartyId, o => o.MapFrom(s => s.ChildParty.Id))
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ChildParty, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    var partyprox = s.Party as INHibernateProxy;
                    if (partyprox == null)
                        d.Party = Mapper.Map<IndividualDto>(s.Party);
                    else{
                        var party = partyprox.HibernateLazyInitializer.GetImplementation();
                        d.Party = Mapper.Map<IndividualDto>(party);
                    }

                    var childPartyprox = s.ChildParty as INHibernateProxy;
                    if (childPartyprox == null)
                        d.ChildParty = Mapper.Map<IndividualDto>(s.ChildParty);
                    else{
                        var party = childPartyprox.HibernateLazyInitializer.GetImplementation();
                        d.ChildParty = Mapper.Map<IndividualDto>(party);
                    }


                });

            Mapper.CreateMap<List<Relationship>, ListResultDto<RelationshipDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
            ;
            

            Mapper.CreateMap<CreateRelationshipDto, Relationship>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.RelationshipType, o => o.MapFrom(t => new RelationshipType() { Id = t.RelationshipTypeId }))
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ChildParty, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                ;

            Mapper.CreateMap<EditRelationshipDto, Relationship>()
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.RelationshipType, o => o.MapFrom(t => new RelationshipType() { Id = t.RelationshipTypeId }))
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ChildParty, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                ;

            Mapper.CreateMap<Relationship,EditRelationshipDto>()
                .ForMember(t => t.ChannelId, o => o.Ignore())
                ;

            Mapper.CreateMap<Individual, IndividualDto>()
                
                ;

            Mapper.CreateMap<Contact, IndividualDto>()

                ;
        }
    }
}