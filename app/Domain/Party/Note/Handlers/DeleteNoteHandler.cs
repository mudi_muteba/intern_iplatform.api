﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Note;
using MasterData.Authorisation;

namespace Domain.Party.Note.Handlers
{
    public class DeleteNoteHandler : CreationDtoHandler<Note, DeleteNoteDto, int>
    {
        public DeleteNoteHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(Note entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Note HandleCreation(DeleteNoteDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Domain.Party.Party>(dto.PartyId);

            if (party == null)
                throw new MissingEntityException(typeof(Domain.Party.Party), dto.Id);

            Note note = party.DeleteNote(dto);

            result.Processed(dto.Id);
            return note;
        }
    }
}