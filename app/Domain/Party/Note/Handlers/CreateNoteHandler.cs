﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Note;
using MasterData.Authorisation;

namespace Domain.Party.Note.Handlers
{
    public class CreateNoteHandler : CreationDtoHandler<Note, CreateNoteDto, int>
    {
        public CreateNoteHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(Note entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Note HandleCreation(CreateNoteDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Domain.Party.Party>(dto.PartyId);
            if (party == null)
                throw new MissingEntityException(typeof (Domain.Party.Party), dto.Id);
            Note Note = party.AddNote(dto);

            result.Processed(Note.Id);
            return Note;
        }
    }
}