﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Party.Note;

namespace Domain.Party.Note.Validation
{
    public class NoteValidator : IValidateDto<CreateNoteDto>, IValidateDto<EditNoteDto>
    {
        private readonly IRepository _repository;
        public NoteValidator(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
        }

        public void Validate(CreateNoteDto dto, ExecutionResult result)
        {
        }

        public void Validate(EditNoteDto dto, ExecutionResult result)
        {
        }
    }
}