﻿using System;
using Domain.Base;

namespace Domain.Party.Note
{
    public class Note : EntityWithParty
    {
        public virtual DateTime DateCreated { get; set; }
        public virtual string Notes { get; set; }
        public virtual bool Confidential { get; set; }
        public virtual bool Warning { get; set; }
    }
}
