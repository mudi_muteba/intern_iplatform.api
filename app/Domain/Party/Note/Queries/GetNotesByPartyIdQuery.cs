﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Note.Queries
{
    public class GetNotesByPartyIdQuery : BaseQuery<Note>
    {
        private int _partyId;

        public GetNotesByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Note>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetNotesByPartyIdQuery WithPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<Note> Execute(IQueryable<Note> query)
        {
            return Repository.GetAll<Note>().Where(a => a.Party.Id == _partyId);
        }
    }
}