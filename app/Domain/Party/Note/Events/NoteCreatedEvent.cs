﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Party.Note.Events
{
    public class NoteCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public Note Note { get; private set; }
        public int UserId { get; private set; }

        public NoteCreatedEvent(Note _Note, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> { channelReferences })
        {
            Note = _Note;
        }
    }
}