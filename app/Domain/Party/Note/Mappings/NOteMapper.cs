﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Note;

namespace Domain.Party.Note.Mappings
{
    public class NoteMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<List<ListNoteDto>, ListResultDto<ListNoteDto>>();

            Mapper.CreateMap<List<Note>, ListResultDto<ListNoteDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<CreateNoteDto, Note>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore());

            Mapper.CreateMap<Note, ListNoteDto>();
            Mapper.CreateMap<Note, NoteDto>();
        }
    }
}