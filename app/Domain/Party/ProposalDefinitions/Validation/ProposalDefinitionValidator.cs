﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Party.ProposalDefinition;

namespace Domain.Party.ProposalDefinitions.Validation
{
    public class ProposalDefinitionValidator : IValidateDto<CreateProposalDefinitionDto>, IValidateDto<EditProposalDefinitionDto>
    {
        private readonly IRepository _repository;
        public ProposalDefinitionValidator(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
        }

        public void Validate(CreateProposalDefinitionDto dto, ExecutionResult result)
        {
        }

        public void Validate(EditProposalDefinitionDto dto, ExecutionResult result)
        {
        }
    }
}