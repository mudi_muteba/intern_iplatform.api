﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Party.ProposalDefinitions.Queries;
using Domain.Party.ProposalHeaders;
using FluentNHibernate.Conventions;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData;
using ValidationMessages.ProposalQuestionDefinition;

namespace Domain.Party.ProposalDefinitions.Validation
{
    public class EditProposalDefinitionDtoValidator : IValidateDto<EditProposalDefinitionDto>
    {
        private readonly IProvideContext _contextProvider;
        private readonly IRepository _repository;
        private readonly SystemChannels _systemChannels;

        public EditProposalDefinitionDtoValidator(IProvideContext contextProvider, IRepository repository,
            SystemChannels systemChannels)
        {
            _contextProvider = contextProvider;
            _repository = repository;
            _systemChannels = systemChannels;
            //query = new GetPartyByIdQuery(contextProvider, repository);
        }

        public void Validate(EditProposalDefinitionDto dto, ExecutionResult result)
        {
            if (_contextProvider.Get().ActiveChannelId != 5 )
                return;

            var proposalHeader = _repository.GetAll<ProposalHeader>().FirstOrDefault(a => a.Id == dto.ProposalHeaderId);

            if (proposalHeader == null)
                return;

            var allRiskR1Answers = new List<string>() {"11180","11181","11182","11183","11766"};

            if (!dto.Answers.Any(x => allRiskR1Answers.Contains(x.Answer)))
                return;

            var motorProposals = proposalHeader.ProposalDefinitions.Where(a => a.Cover.Id == Covers.Motor.Id && a.Id != dto.Id).ToList();
            var allRiskProposals = proposalHeader.ProposalDefinitions.Where(a => a.Cover.Id == Covers.AllRisk.Id && a.Id != dto.Id).ToList();
            var workingProposal = proposalHeader.ProposalDefinitions.First(a => a.Id == dto.Id);

            var motorCompCount =
                motorProposals.SelectMany(x => x.ProposalQuestionAnswers)
                    .Where(a => a.QuestionDefinition.Question.Id == Questions.TypeOfCoverMotor.Id).Count(a=>a.Answer.Equals("89"));

            var allRiskR1Count =
            allRiskProposals.SelectMany(x => x.ProposalQuestionAnswers)
                .Where(a => a.QuestionDefinition.Question.Id == Questions.KPAllRiskCategory.Id).Count(a =>  allRiskR1Answers.Contains(a.Answer));


            var newAllRiskCatAnswer = string.Empty;
            if (workingProposal.Cover.Id == Covers.AllRisk.Id)
            {
                int pqaid =workingProposal.ProposalQuestionAnswers.Where(
                    a => a.QuestionDefinition.Question.Id == Questions.KPAllRiskCategory.Id).Select(a=>a.Id).First();

                newAllRiskCatAnswer = dto.Answers.First(a => a.Id == pqaid).Answer;

                if (allRiskR1Answers.Contains(newAllRiskCatAnswer))
                    allRiskR1Count++;

            }

            if (allRiskR1Count > motorCompCount)
                    result.AddValidationMessage(
                        ProposalDefinitionValidationMessages.R1ItemCannotBeMoreThanMotorComprehensiveCovers);


            var allRiskR1Asnwers =
                allRiskProposals.SelectMany(x => x.ProposalQuestionAnswers)
                    .Where(a => a.QuestionDefinition.Question.Id == Questions.KPAllRiskCategory.Id).Select(a=> a.Answer).ToList<string>();

            allRiskR1Asnwers.Add(newAllRiskCatAnswer);

            if(allRiskR1Asnwers.GroupBy(x => x).Any(g => g.Count() > 1))
                    result.AddValidationMessage(
                        ProposalDefinitionValidationMessages.CannotAddDuplicateR1Item);

        }


    }
}