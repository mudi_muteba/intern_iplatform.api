﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;

namespace Domain.Party.ProposalDefinitions.Mappings
{
    public class ProposalDefinitionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<List<ListProposalDefinitionDto>, ListResultDto<ListProposalDefinitionDto>>();

            Mapper.CreateMap<List<ProposalDefinition>, ListResultDto<ListProposalDefinitionDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<CreateProposalDefinitionDto, ProposalDefinition>()
                .ForMember(t => t.Cover, o => o.MapFrom(d => new Covers().First(a => a.Id == d.CoverId)))
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore());

            Mapper.CreateMap<ProposalDefinition, ListProposalDefinitionDto>()
                .ForMember(t => t.LeadId, o => o.MapFrom(s => s.Party.Lead.Id))
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                .ForMember(t => t.SumInsured, o => o.MapFrom(s => s.SumInsured > 0 ? s.SumInsured: s.Asset.SumInsured ))
                 .ForMember(t => t.CoverDefinition, o => o.MapFrom(s => s.Product.CoverDefinitions.FirstOrDefault(x=>x.Cover.Id == s.Cover.Id)))
                ;

            Mapper.CreateMap<ProposalDefinition, ProposalDefinitionDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(d => d.Party.Id))
                .ForMember(t => t.IsHeaderQuoted, o => o.MapFrom(d => d.ProposalHeader.IsQuoted))
                .ForMember(t => t.PartyDisplayName, o => o.MapFrom(d => d.Party.DisplayName))
                .ForMember(t => t.LeadId, o => o.MapFrom(d => d.Party.Lead.Id))
                .ForMember(t => t.ProductId, o => o.MapFrom(d => d.Product.Id))
                .ForMember(t => t.Product, o => o.MapFrom(d => d.Product))
                .ForMember(t => t.SumInsured, o => o.MapFrom(s => s.SumInsured > 0 ? s.SumInsured : s.Asset.SumInsured));

            Mapper.CreateMap<ProposalQuestionAnswer, ProposalDefinitionAnswerDto>();
            Mapper.CreateMap<ProposalDefinitionAnswerDto, ProposalQuestionAnswer>();

            Mapper.CreateMap<QuestionAnswer, QuestionAnswerDto>();

            Mapper.CreateMap<QuestionDefinition, ProposalQuestionDefinitionDto>()
                
                .ForMember(t => t.QuestionGroup, 
                o => o.MapFrom(
                    d => d.QuestionGroup ?? d.Question.QuestionGroup
                    )
                )
                .ForMember(t => t.DisplayNameTranslationKey, o => o.MapFrom(d => d.DisplayNameTranslationKey))
                .ForMember(t => t.ProductId, o => o.MapFrom(d => d.CoverDefinition.Product.Id))
                .ForMember(t => t.LogoName, o => o.MapFrom(d => d.CoverDefinition.Product.ImageName))
                .ForMember(s => s.GroupType,
                    o => o.MapFrom(s => s.QuestionDefinitionGroupType))
                .ForMember(s => s.ParentQuestionName,
                    o => o.MapFrom(s => s.Parent.Question.Name))
                .ForMember(s => s.ParentId,
                    o => o.MapFrom(s => s.Parent.Id))
                .ForMember(s => s.Options,
                    o => o.MapFrom(s => new QuestionAnswers().Where(qa => qa.Question.Id == s.Question.Id)))
                    ;

            Mapper.CreateMap<UpdateProposalAnswerFromPolicyBindingDto, ProposalQuestionAnswer>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.QuestionDefinition, o => o.Ignore())
                .ForMember(t => t.ProposalDefinition, o => o.Ignore())
                .ForMember(t => t.QuestionType, o => o.Ignore())
                .ForMember(t => t.Answer, o => o.MapFrom(s => s.Answer))
                ;
        }
    }
}