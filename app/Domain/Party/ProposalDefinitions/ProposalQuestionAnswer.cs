﻿using AutoMapper;
using Domain.Base;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData;

namespace Domain.Party.ProposalDefinitions
{
    public class ProposalQuestionAnswer : Entity
    {
        public virtual ProposalDefinition ProposalDefinition { get; set; }
        public virtual QuestionDefinition QuestionDefinition { get; set; }
        public virtual string Answer { get; set; }
        public virtual QuestionType QuestionType { get; set; }

        public virtual void BelongsTo(ProposalDefinition proposalDefinition)
        {
            ProposalDefinition = proposalDefinition;
        }

        public virtual decimal ConvertToDecimal(decimal defaultValue = 0.00m)
        {
            var decimalValue = defaultValue;
            return decimal.TryParse(Answer, out defaultValue) ? decimalValue : defaultValue;
        }

        public virtual void AnswerThis(AnswerQuestionDefinitionDto answer)
        {
            Answer = answer == null ? string.Empty : answer.Answer.ToString();
            QuestionType = QuestionDefinition.Question.QuestionType;
        }

        public virtual void Update(UpdateProposalAnswerFromPolicyBindingDto dto)
        {
            Mapper.Map(dto, this);
        }
    }
}