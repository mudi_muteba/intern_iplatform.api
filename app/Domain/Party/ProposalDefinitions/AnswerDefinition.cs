﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;
using Domain.Products;
using MasterData;

namespace Domain.Party.ProposalDefinitions
{
    public class AnswerDefinition : Entity
    {
        public virtual Product Product { get; set; }
        public virtual QuestionAnswer QuestionAnswer { get; set; }
    }
}
