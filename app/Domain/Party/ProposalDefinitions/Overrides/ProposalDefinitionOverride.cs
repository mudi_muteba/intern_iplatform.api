﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Party.ProposalDefinitions.Overrides
{
    public class ProposalDefinitionOverride : IAutoMappingOverride<ProposalDefinition>
    {
        public void Override(AutoMapping<ProposalDefinition> mapping)
        {
            mapping.References(x => x.Asset).Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy);
            mapping.HasMany(x => x.ProposalQuestionAnswers).Cascade.SaveUpdate();
            mapping.IgnoreProperty(p => p.TotalQuestions);
            mapping.IgnoreProperty(p => p.TotalQuestionsAnswered);
        }
    }
}