﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;


namespace Domain.Party.ProposalDefinitions.Overrides
{
    public class ProposalQuestionAnswerOverride : IAutoMappingOverride<ProposalQuestionAnswer>
    {
        public void Override(AutoMapping<ProposalQuestionAnswer> mapping)
        {
            mapping.References(x => x.QuestionDefinition).Cascade.None().LazyLoad(Laziness.NoProxy);
        }
    }
}