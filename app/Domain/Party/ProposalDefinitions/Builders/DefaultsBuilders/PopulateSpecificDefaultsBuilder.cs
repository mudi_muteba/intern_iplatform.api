using System.Collections.Generic;
using System.Linq;
using Domain.Base.Repository;
using Domain.HistoryLoss.LossHistory;
using Domain.Individuals;
using Domain.Party.Addresses;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using MasterData;

namespace Domain.Party.ProposalDefinitions.Builders.DefaultsBuilders
{
    public class PopulateSpecificDefaultsBuilder : DefaultsBuilderBase
    {
        private List<ResponseErrorMessage> ResponseErrorMessages { get; set; }

        public PopulateSpecificDefaultsBuilder(SystemChannels systemChannels, IRepository repository)
            : base(systemChannels, repository)
        {
            ResponseErrorMessages = new List<ResponseErrorMessage>();
        }

        public override List<ResponseErrorMessage> Build(DefaultsBuilderDto dto)
        {
            Address defaultAddress =
                dto.NewDefinition.ProposalHeader.Party.Addresses.FirstOrDefault(a => a.IsDefault);

            if (defaultAddress != null)
            {
                ProposalQuestionAnswer riskAddress =
                    dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        a => a.QuestionDefinition.Question.Id == Questions.RiskAddress.Id);
                if (riskAddress != null) riskAddress.Answer = defaultAddress.Id.ToString();

                ProposalQuestionAnswer overNightAddress =
                    dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        a => a.QuestionDefinition.Question.Id == Questions.OvernightAddress.Id);

                if (overNightAddress != null) overNightAddress.Answer = defaultAddress.Id.ToString();

                ProposalQuestionAnswer postalCode =
                    dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        a => a.QuestionDefinition.Question.Id == Questions.PostalCode.Id);

                if (postalCode != null) postalCode.Answer = defaultAddress.Code;

                ProposalQuestionAnswer suburb =
                    dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        a => a.QuestionDefinition.Question.Id == Questions.Suburb.Id);

                if (suburb != null) suburb.Answer = defaultAddress.Line4;

                ProposalQuestionAnswer province =
                    dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        a => a.QuestionDefinition.Question.Id == Questions.Province.Id);

                if (province != null && postalCode != null)
                    province.Answer = GetProvinceFromPostalCode(postalCode.Answer).ToString();
            }

            var individual = _repository.GetById<Individual>(dto.NewDefinition.ProposalHeader.Party.Id);

            if (individual != null)
            {
                ProposalQuestionAnswer registeredOwnerIDNumber = dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault
                    (
                        a => a.QuestionDefinition.Question.Id == Questions.RegisteredOwnerIDNumber.Id);

                if (registeredOwnerIDNumber != null)
                    registeredOwnerIDNumber.Answer = individual.IdentityNo;

                ProposalQuestionAnswer previouslyInsured = dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    a => a.QuestionDefinition.Question.Id == Questions.PreviouslyInsured.Id);

                if (previouslyInsured != null)
                {
                    var lossHistory = _repository.GetByPartyId<LossHistory>(individual.Id).FirstOrDefault();

                    if (lossHistory != null)
                        previouslyInsured.Answer = (lossHistory.CurrentlyInsured.Id > 1) ? "True" : "False";
                }

                ProposalQuestionAnswer maritalStatus = dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                  a => a.QuestionDefinition.Question.Id == Questions.MainDriverMaritalStatus.Id);

                if (maritalStatus != null && individual.MaritalStatus != null)
                {
                    var firstOrDefault = new QuestionAnswers().FirstOrDefault(
                        a =>
                            a.Question.Id == maritalStatus.QuestionDefinition.Question.Id &&
                            individual.MaritalStatus.Name.Equals(a.Answer));
                    if (firstOrDefault != null)
                        maritalStatus.Answer =
                            firstOrDefault.Id.ToString();
                }


                ProposalQuestionAnswer mainDriverIDNumber = dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    a => a.QuestionDefinition.Question.Id == Questions.MainDriverIDNumber.Id);

                if (mainDriverIDNumber != null)
                    mainDriverIDNumber.Answer = individual.IdentityNo;

                ProposalQuestionAnswer mainDriverFirstName = dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    a => a.QuestionDefinition.Question.Id == Questions.MainDriverFirstName.Id);

                if (mainDriverFirstName != null)
                    mainDriverFirstName.Answer = individual.FirstName;

                ProposalQuestionAnswer mainDriverSurname = dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    a => a.QuestionDefinition.Question.Id == Questions.MainDriverSurname.Id);

                if (mainDriverSurname != null)
                    mainDriverSurname.Answer = individual.Surname;

                ProposalQuestionAnswer mainDriverTitle = dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    a => a.QuestionDefinition.Question.Id == Questions.MainDriverTitle.Id);

                if (mainDriverTitle != null && individual.Title != null)
                {
                    QuestionAnswer firstOrDefault = new QuestionAnswers().FirstOrDefault(
                        a =>
                            a.Question.Id == mainDriverTitle.QuestionDefinition.Question.Id &&
                            individual.Title.Name.Equals(a.Answer));
                    if (firstOrDefault != null)
                        mainDriverTitle.Answer =
                            firstOrDefault.Id.ToString();
                }
            }
            return ResponseErrorMessages;
        }
    }
}