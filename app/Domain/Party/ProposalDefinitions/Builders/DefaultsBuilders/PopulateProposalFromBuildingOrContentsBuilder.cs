using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Domain.Base.Repository;
using Domain.Party.Assets.AssetAddresses;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Infrastructure.Configuration;
using MasterData;
using Shared.Extentions;

namespace Domain.Party.ProposalDefinitions.Builders.DefaultsBuilders
{
    public class PopulateProposalFromBuildingOrContentsBuilder : DefaultsBuilderBase
    {
        private List<ResponseErrorMessage> ResponseErrorMessages { get; set; }

        private static readonly List<int> AllowedCovers = new List<int>
        {
            Covers.Contents.Id,
            Covers.Building.Id
        };

        public PopulateProposalFromBuildingOrContentsBuilder(SystemChannels systemChannels, IRepository repository)
            : base(systemChannels, repository)
        {
            ResponseErrorMessages = new List<ResponseErrorMessage>();
        }

        public override List<ResponseErrorMessage> Build(DefaultsBuilderDto dto)
        {
            if (!AllowedCovers.Contains(dto.NewDefinition.Cover.Id))
                return ResponseErrorMessages;

            if (dto.DefaultFromProposalId < 1)
                return ResponseErrorMessages;

            this.Info(
                () =>
                    string.Format("PopulateProposalFromBuilding: Check if channelId allowed {0}",
                        dto.NewDefinition.ProposalHeader.Party.Channel.Id));


            this.Info(
                () =>
                    string.Format("PopulateProposalFromBuilding: channelId allowed {0}",
                        dto.NewDefinition.ProposalHeader.Party.Channel.Id));

            var newCover = dto.NewDefinition.Cover;
            var oldProposal =
                dto.NewDefinition.ProposalHeader.ProposalDefinitions.LastOrDefault(
                    a => a.Id == dto.DefaultFromProposalId);

            if (oldProposal == null)
            {
                this.Info(
                    () =>
                        string.Format(
                            "PopulateProposalFromBuilding: did not find a contents or building proposal to copy from for proposal definitiion {0}",
                            dto.NewDefinition.Id));
                return ResponseErrorMessages;
            }


            //Find the risk address on old propolsal and check if a "new cover" with this address already exists 
            var riskAddressId =
                oldProposal.ProposalQuestionAnswers.Where(
                    a => a.QuestionDefinition.Question.Id == Questions.RiskAddress.Id)
                    .Select(a => a.Answer)
                    .First();

            var addressAlreadyAdded = dto.NewDefinition.ProposalHeader.ProposalDefinitions.Where(
                a => a.Cover == newCover && a.Id != dto.NewDefinition.Id)
                .ToList()
                .Any(
                    a =>
                        a.ProposalQuestionAnswers.Any(
                            s =>
                                s.QuestionDefinition.Question.Id == Questions.RiskAddress.Id &&
                                s.Answer.Equals(riskAddressId)));

            if (addressAlreadyAdded)
            {
                this.Info(
                    () =>
                        string.Format("PopulateProposalFromBuilding: alredy contains cover {0} with risk addressId {1}",
                            newCover.Name, riskAddressId));
                ResponseErrorMessages.Add(new ResponseErrorMessage("A cover with the Risk Address already exists","COVER_ALREADY_ADDED"));

                return ResponseErrorMessages;
            }

            //copy properties on the definition entity
            dto.NewDefinition.Description = oldProposal.Description;
            dto.NewDefinition.Asset = 
                new AssetAddress()
                {
                    Address = (oldProposal.Asset as AssetAddress).Address,
                    AssetNo = dto.NewDefinition.Id,
                    Description = oldProposal.Asset.Description,
                    Party = oldProposal.Party,
                    SumInsured = 0
                };

            UpdateByQuestionId(
                dto.NewDefinition.ProposalQuestionAnswers.ToList()
                , oldProposal.ProposalQuestionAnswers.ToList(), dto.NewDefinition.Id.ToString());


            // if copy from Buildings direction = 0 else Contents = 1
            var direction = 0;
            if (oldProposal.Cover.Id == Covers.Contents.Id)
                direction = 1;

            UpdateByMatchingIds(dto.NewDefinition.ProposalQuestionAnswers.ToList(),
                oldProposal.ProposalQuestionAnswers.ToList(), direction,
                dto.NewDefinition.ProposalHeader.Party.Channel.Id);

            //reset sumInSured .. we do not want to copy this one 
            var sumInsured = dto.NewDefinition.ProposalQuestionAnswers.FirstOrDefault(
                a => a.QuestionDefinition.Question.Id == Questions.SumInsured.Id);

            if (sumInsured != null)
                sumInsured.Answer = "0";

            return ResponseErrorMessages;
        }

        private void UpdateByMatchingIds(List<ProposalQuestionAnswer> newList, List<ProposalQuestionAnswer> oldList,
            int direction, int channelId)
        {
            var matchingQuestionIds = new List<Tuple<int, int>>
            {
                new Tuple<int, int>(404, 456), // Lightning conductor
                new Tuple<int, int>(410, 462),
                new Tuple<int, int>(420, 472),
                new Tuple<int, int>(428, 480),
                new Tuple<int, int>(396, 448),
                new Tuple<int, int>(398, 450),
                new Tuple<int, int>(386, 436),
                new Tuple<int, int>(416, 468),
                new Tuple<int, int>(402, 454),
                new Tuple<int, int>(402, 454),
                new Tuple<int, int>(182, 11)
            };
            var matchingOptionIds = new List<Tuple<int, int>>
            {
                new Tuple<int, int>(3257,3253),
                new Tuple<int, int>(3258,3254),
                new Tuple<int, int>(3259,3255),
                new Tuple<int, int>(3260,3256),
                new Tuple<int, int>(3264,3261),
                new Tuple<int, int>(3265,3262),
                new Tuple<int, int>(3266,3263)
            };

            foreach (var newQuestionAnswer in newList)
            {
                var matchingQuestionId = 0;

                var questionId = newQuestionAnswer.QuestionDefinition.Question.Id;

                if (GetMatchingId(direction, matchingQuestionIds, questionId, ref matchingQuestionId)) continue;

                var oldQuestionAnswer =
                    oldList.FirstOrDefault(a => a.QuestionDefinition.Question.Id == matchingQuestionId);

                if (oldQuestionAnswer == null)
                    continue;

                //no old answer to copy, new answer already answered
                if (string.IsNullOrWhiteSpace(oldQuestionAnswer.Answer)
                    || !string.IsNullOrWhiteSpace(newQuestionAnswer.Answer)
                    )
                    continue;


                if (newQuestionAnswer.QuestionDefinition.Question.QuestionType.Id == QuestionTypes.Dropdown.Id)
                {
                    var oldOptionId = 0;
                    var castResult = Int32.TryParse(oldQuestionAnswer.Answer, out oldOptionId);

                    if (!castResult)
                        continue;

                    var matchingAnswerId = 0;
                    if (GetMatchingId(direction, matchingOptionIds, oldOptionId, ref matchingAnswerId)) continue;
                    newQuestionAnswer.Answer = matchingAnswerId.ToString();
                }
                else
                    newQuestionAnswer.Answer = oldQuestionAnswer.Answer;

            }
        }

        private static bool GetMatchingId(int direction, List<Tuple<int, int>> listIds, int? questionId, ref int matchingQuestionId)
        {
            if (direction == 0)
            {
                var item = listIds.FirstOrDefault(a => a.Item2 == questionId);
                if (item != null)
                    matchingQuestionId = item.Item1;
                else
                    return true;
            }
            else
            {
                var item = listIds.FirstOrDefault(a => a.Item1 == questionId);
                if (item != null)
                    matchingQuestionId = item.Item2;
                else
                    return true;
            }
            return false;
        }
    }
}