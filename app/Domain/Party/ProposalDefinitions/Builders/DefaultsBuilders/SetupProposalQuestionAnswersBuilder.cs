using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Admin.SettingsiRates;
using Domain.Base.Repository;
using Domain.QuestionDefinitions;
using Domain.Users;
using FluentNHibernate.Conventions;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Shared.Extentions;

namespace Domain.Party.ProposalDefinitions.Builders.DefaultsBuilders
{
    public class SetupProposalQuestionAnswersBuilder : DefaultsBuilderBase
    {
        private List<ResponseErrorMessage> ResponseErrorMessages { get; set; }
        public SetupProposalQuestionAnswersBuilder(SystemChannels systemChannels, IRepository repository)
            : base(systemChannels, repository)
        {
            ResponseErrorMessages = new List<ResponseErrorMessage>();
        }

        public override List<ResponseErrorMessage> Build(DefaultsBuilderDto dto)
        {
            List<int> coverDefQuestionIds = _repository.GetAll<QuestionDefinition>().Where(a => a.CoverDefinition.Cover == dto.NewDefinition.Cover && a.CoverDefinition.Product == dto.NewDefinition.Product).Select(a => a.Id).Distinct().ToList();

            //Get the allowed products for the user channel
            IQueryable<int> userChannelAllowedProducts = _repository.GetAll<SettingsiRate>()
                .Where(s => s.Channel.Id == dto.ActiveChannelId && !s.IsDeleted)
                .Select(p => p.Product.Id);

            IQueryable<int> mappedQuestionIds = _repository.GetAll<MapQuestionDefinition>().Where(a => coverDefQuestionIds.Contains(a.ParentId)).Select(a => a.ChildId).Distinct();

            List<QuestionDefinition> questionDefinitions = _repository.GetAll<QuestionDefinition>().Where(
                a => (coverDefQuestionIds.Contains(a.Id) || mappedQuestionIds.Contains(a.Id)) 
                && (!userChannelAllowedProducts.Any() || userChannelAllowedProducts.Any(uc => a.CoverDefinition.Product.Id == uc) || dto.NewDefinition.Product.Id == a.CoverDefinition.Product.Id )) //If the userChannelAllowedProducts is empty dont enforce filter
                .ToList();

            if (dto.NewDefinition.ProposalHeader == null) return ResponseErrorMessages;

            foreach (var questionDefinition in questionDefinitions)
            {
                if (questionDefinition.Question == null)
                {
                    this.Error(
                        () =>
                            string.Format("Question Definition {0} does not have a Question associated", questionDefinition.Id));

                    continue;
                }

                var pqa = new ProposalQuestionAnswer
                {
                    ProposalDefinition = dto.NewDefinition,
                    QuestionDefinition = questionDefinition,
                    QuestionType = questionDefinition.Question.QuestionType
                };

                //set defaults from database
                if (!string.IsNullOrEmpty(questionDefinition.DefaultValue))
                    pqa.Answer = questionDefinition.DefaultValue;

                dto.NewDefinition.AddProposalQuestionAnswer(pqa);
            }


            return ResponseErrorMessages;
        }
    }
}