using System;
using System.Collections.Generic;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Party.ProposalDefinitions.Builders.DefaultsBuilders
{
    public class DefaultsBuilder
    {
        private readonly DefaultsBuilderDto _dto;
        private List<DefaultsBuilderBase> Builders { get; set; }
        private List<ResponseErrorMessage> ResponseErrorMessage { get; set; }


        public DefaultsBuilder(SystemChannels systemChannels, IRepository repository, DefaultsBuilderDto dto)
        {
            _dto = dto;
            Builders = new List<DefaultsBuilderBase>();
            Builders.Add(new SetupProposalQuestionAnswersBuilder(systemChannels, repository));
            Builders.Add(new PopulateSpecificDefaultsBuilder(systemChannels, repository));
            Builders.Add(new PopulateDriverInfoFromPolicyHolderBuilder(systemChannels, repository));
            Builders.Add(new PopulateProposalFromBuildingOrContentsBuilder(systemChannels, repository));
            Builders.Add(new PopulateVapBasedOnSettingsBuilder(systemChannels, repository));
            ResponseErrorMessage = new List<ResponseErrorMessage>();
        }

        public List<ResponseErrorMessage> Build()
        {
            foreach (var builder in Builders)
            {
                    var errors = builder.Build(_dto);
                    ResponseErrorMessage.AddRange(errors);
            }
            return ResponseErrorMessage;
        }
    }
}