﻿namespace Domain.Party.ProposalDefinitions.Builders.DefaultsBuilders
{
    public class DefaultsBuilderDto
    {
        public ProposalDefinition NewDefinition { get; set; }
        public int PropulatefromPrpoosal { get; set; }

        public int DefaultFromProposalId { get; set; }
        public int ActiveChannelId { get; set; }
    }
}