﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using MasterData;
using Shared.Extentions;

namespace Domain.Party.ProposalDefinitions.Builders.DefaultsBuilders
{
    public class PopulateDriverInfoFromPolicyHolderBuilder : DefaultsBuilderBase
    {
        private List<ResponseErrorMessage> ResponseErrorMessages { get; set; }
        private static readonly List<int> AllowedCovers = new List<int>
        {
            Covers.Motor.Id,
            Covers.CaravanOrTrailer.Id,
            Covers.Motorcycle.Id
        };

        public PopulateDriverInfoFromPolicyHolderBuilder(SystemChannels systemChannels, IRepository repository)
            : base(systemChannels, repository)
        {
            ResponseErrorMessages = new List<ResponseErrorMessage>();
        }

        public override List<ResponseErrorMessage> Build(DefaultsBuilderDto dto)
        {
            if (!AllowedCovers.Contains(dto.NewDefinition.Cover.Id))
                return ResponseErrorMessages;

            this.Info(
                () =>
                    string.Format("PopulateDriverInformation: Check if channelId allowed {0}",
                        dto.NewDefinition.ProposalHeader.Party.Channel.Id));

            this.Info(
                () =>
                    string.Format("PopulateDriverInformation: channelId allowed {0}",
                        dto.NewDefinition.ProposalHeader.Party.Channel.Id));

            Individual individual = null;
            individual = _repository.UnProxy<Individual>(dto.NewDefinition.ProposalHeader.Party);
            var identityNo = individual.IdentityNo;

            this.Info(() => string.Format("PopulateDriverInformation: Policy Holder IdentityNo {0}", identityNo));

            var proposalQuestionAnswers =
                dto.NewDefinition.ProposalHeader.ProposalDefinitions.Where(x => x.Id != dto.NewDefinition.Id)
                    .SelectMany(a => a.ProposalQuestionAnswers)
                    .ToList();
            var proposalQuestionAnswer =
                proposalQuestionAnswers.FirstOrDefault(a => a.Answer != null && a.Answer.Equals(identityNo));

            if (proposalQuestionAnswer == null)
            {
                this.Warn(
                    () =>
                        string.Format(
                            "PopulateDriverInformation: exit No propsal found using the main policy holder {0}",
                            identityNo));
                return ResponseErrorMessages;
            }

            var proposalDefinition = proposalQuestionAnswer.ProposalDefinition;
            this.Info(
                () =>
                    string.Format("PopulateDriverInformation: populate main driver info from proposal definition {0}",
                        proposalDefinition.Id));

            //Find all Question linked to parameter QuestionGroup
            var oldQuestionAnswers =
                proposalDefinition.ProposalQuestionAnswers.Where(
                    a => a.QuestionDefinition.Question.QuestionGroup == QuestionGroups.DriverInformation).ToList();

            var newQuestionAnswers =
                dto.NewDefinition.ProposalQuestionAnswers.Where(
                    a => a.QuestionDefinition.Question.QuestionGroup == QuestionGroups.DriverInformation).ToList();

            UpdateByQuestionDefinitionId(newQuestionAnswers, oldQuestionAnswers, identityNo);
            return ResponseErrorMessages;
        }
    }
}