using System.Collections.Generic;
using System.Linq;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using MasterData;
using Shared.Extentions;

namespace Domain.Party.ProposalDefinitions.Builders.DefaultsBuilders
{
    public abstract class DefaultsBuilderBase
    {
        internal readonly IRepository _repository;
        internal readonly SystemChannels _systemChannels;
        public abstract List<ResponseErrorMessage> Build(DefaultsBuilderDto dto);

        protected DefaultsBuilderBase(SystemChannels systemChannels, IRepository repository)
        {
            _systemChannels = systemChannels;
            _repository = repository;
        }

        internal static void UpdateByQuestionDefinitionId(List<ProposalQuestionAnswer> newQuestionAnswers,
            List<ProposalQuestionAnswer> oldQuestionAnswers, string identity)
        {
            var counter = 0;
            foreach (var questionAnswer in newQuestionAnswers)
            {
                var e =
                    oldQuestionAnswers.FirstOrDefault(
                        a => a.QuestionDefinition.Id == questionAnswer.QuestionDefinition.Id);

                if (e == null) continue;
                counter++;
                questionAnswer.Answer = e.Answer;
            }

            typeof (DefaultsBuilderBase).Info(
                () => string.Format("PopulateDriverInformation: Done {0} Mapped {1} of {2}", identity, counter,
                    oldQuestionAnswers.Count));
        }
        internal static void UpdateByQuestionId(List<ProposalQuestionAnswer> newQuestionAnswers,
            List<ProposalQuestionAnswer> oldQuestionAnswers, string identity)
        {
            var counter = 0;
            foreach (var questionAnswer in newQuestionAnswers)
            {
                var e =
                    oldQuestionAnswers.FirstOrDefault(
                        a => a.QuestionDefinition.Question.Id == questionAnswer.QuestionDefinition.Question.Id);

                if (e == null) continue;
                counter++;
                questionAnswer.Answer = e.Answer;
            }

            typeof (DefaultsBuilderBase).Info(
                () => string.Format("PopulateDriverInformation: Done {0} Mapped {1} of {2}", identity, counter,
                    oldQuestionAnswers.Count));
        }

        public static int GetProvinceFromPostalCode(string postalcode)
        {
            if (string.IsNullOrEmpty(postalcode)) return 0;
            int provinceId = 0;

            var province = string.Empty;

            var pcode = int.Parse(postalcode);

            if ((pcode >= 1 && pcode <= 299) | (pcode >= 1400 && pcode <= 2199)) province = StateProvinces.Gauteng.Name;
            else if (pcode >= 6500 && pcode <= 8099) province = StateProvinces.WesternCape.Name;
            else if (pcode >= 2900 && pcode <= 4730) province = StateProvinces.KwaZuluNatal.Name;
            else if ((pcode >= 1000 && pcode <= 1399) | (pcode >= 2200 && pcode <= 2499)) province = StateProvinces.Mpumalanga.Name;
            else if (pcode >= 9300 && pcode <= 9999) province = StateProvinces.FreeState.Name;
            else if ((pcode >= 300 && pcode <= 499) | (pcode >= 2500 && pcode <= 2899)) province = StateProvinces.NorthWest.Name;
            else if (pcode >= 8100 && pcode <= 8999) province = StateProvinces.NorthernCape.Name;
            else if (pcode >= 4731 && pcode <= 6499) province = StateProvinces.EasternCape.Name;
            else if (pcode >= 500 && pcode <= 999) province = StateProvinces.Limpopo.Name;

            var firstOrDefault = new QuestionAnswers().FirstOrDefault(
                a => a.Question.Id == Questions.Province.Id && a.Answer.Equals(province));
            if (firstOrDefault != null)
            {
                provinceId =
                    firstOrDefault.Id;
            }

            return provinceId;
        }
    }
}