using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Domain.QuestionDefinitions;
using MasterData;
using Domain.MapVapQuestionDefinitionCovers;

namespace Domain.Party.ProposalDefinitions.Builders.DefaultsBuilders
{
    public class PopulateVapBasedOnSettingsBuilder : DefaultsBuilderBase
    {
        private List<ResponseErrorMessage> ResponseErrorMessages { get; set; }
        private readonly IRepository repository;
        public PopulateVapBasedOnSettingsBuilder(SystemChannels systemChannels, IRepository repository)
            : base(systemChannels, repository)
        {
            this.repository = repository;
            ResponseErrorMessages = new List<ResponseErrorMessage>();
        }

        public override List<ResponseErrorMessage> Build(DefaultsBuilderDto dto)
        {
            var questions = GetMapVapQuestionDefinitions(dto);
            if (!questions.Any())
                return ResponseErrorMessages;

            questions.ForEach(x =>
            {
                var pqa = new ProposalQuestionAnswer
                {
                    ProposalDefinition = dto.NewDefinition,
                    QuestionDefinition = x.QuestionDefinition,
                    QuestionType = x.QuestionDefinition.Question.QuestionType
                };

                //set defaults from database
                if (!string.IsNullOrEmpty(x.QuestionDefinition.DefaultValue))
                    pqa.Answer = x.QuestionDefinition.DefaultValue;

                dto.NewDefinition.AddProposalQuestionAnswer(pqa);

            });
            return ResponseErrorMessages;
        }

        private List<MapVapQuestionDefinition> GetMapVapQuestionDefinitions(DefaultsBuilderDto dto)
        {
            Cover cover = new Covers().First(c => c.Id == dto.NewDefinition.Cover.Id);
            List<MapVapQuestionDefinition> result = _repository.GetAll<MapVapQuestionDefinitionCover>()
                .Where(x => x.Cover == cover)
                .Join(_repository.GetAll<MapVapQuestionDefinition>(),
                    o => o.MapVapQuestionDefinition.Id,
                    i => i.Id,
                    (o, i) => i)
                .Where(i => i.Channel.Id == dto.ActiveChannelId &&
                            i.Enabled &&
                            !i.IsDeleted)
                .ToList();

            List<MapVapQuestionDefinition> distinct = result.GroupBy(r => r.QuestionDefinition.Question.Id)
                .Select(r => r.First())
                .ToList();
            
            return distinct;
        }

    }
}