﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;

namespace Domain.Party.ProposalDefinitions.Builders.MapQuestions
{
    public class ResolveMapArgumentsDto
    {
        public ResolveMapArgumentsDto(ProposalDefinitionAnswerDto parent, ProposalDefinitionAnswerDto child, IList<ProposalQuestionAnswer> proposalQuestionAnswers)
        {
            Parent = parent;
            Child = child;
            ProposalQuestionAnswers = proposalQuestionAnswers;
        }

        public ProposalDefinitionAnswerDto Parent { get; set; }
        public ProposalDefinitionAnswerDto Child { get; set; }
        public IList<ProposalQuestionAnswer> ProposalQuestionAnswers { get; set; }
    }
}
