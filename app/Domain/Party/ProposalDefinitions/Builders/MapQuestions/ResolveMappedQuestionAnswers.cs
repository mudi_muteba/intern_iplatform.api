using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Repository;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData;
using Shared.Extentions;

namespace Domain.Party.ProposalDefinitions.Builders.MapQuestions
{
    public class ResolveMappedQuestionAnswers
    {
        private readonly Action<ResolveMapArgumentsDto> _dropdownType =
            dto =>
            {
                if (dto.Child.QuestionType.Id != dto.Parent.QuestionType.Id)
                    return;

                if (string.IsNullOrWhiteSpace(dto.Parent.Answer))
                    return;

                var parentSelectedOption = new QuestionAnswers().First(a => a.Id == int.Parse(dto.Parent.Answer));

                var childQuestionId =
                    dto.ProposalQuestionAnswers
                        .Where(a => a.Id == dto.Child.Id)
                        .Select(a => a.QuestionDefinition.Question.Id)
                        .First();

                var childOptions = new QuestionAnswers().Where(a => a.Question.Id == childQuestionId);

                var childSelectedOption =
                    childOptions.FirstOrDefault(a => a.Answer.ContainsAny(parentSelectedOption.Answer.Split(' ')));

                if (childSelectedOption == null)
                    return;

                dto.Child.Answer = childSelectedOption.Id.ToString();
            };

        private readonly IRepository _repository;

        private readonly Action<ResolveMapArgumentsDto> _textboxType =
            dto =>
            {
                if (dto.Child.QuestionType == dto.Parent.QuestionType)
                    dto.Child.Answer = dto.Parent.Answer;
            };

        public ResolveMappedQuestionAnswers(IRepository repository)
        {
            _repository = repository;
            _resolveMap = new Dictionary<int, Action<ResolveMapArgumentsDto>>();
        }

        private Dictionary<int, Action<ResolveMapArgumentsDto>> _resolveMap { get; set; }

        public void UpdateMappedQuestions(EditProposalDefinitionDto dto,
            IList<ProposalQuestionAnswer> proposalQuestionAnswers)
        {
            var qdIds = proposalQuestionAnswers.Select(a => a.QuestionDefinition.Id).ToList();

            var mapQuestionDefinition =
                _repository.GetAll<MapQuestionDefinition>().Where(a => qdIds.Contains(a.ParentId)).ToList().Distinct();

            if (!mapQuestionDefinition.Any()) return;

            _resolveMap.Add(QuestionTypes.Textbox.Id, _textboxType);
            _resolveMap.Add(QuestionTypes.Date.Id, _textboxType);
            _resolveMap.Add(QuestionTypes.Address.Id, _textboxType);
            _resolveMap.Add(QuestionTypes.Asset.Id, _textboxType);
            _resolveMap.Add(QuestionTypes.Checkbox.Id, _textboxType);
            _resolveMap.Add(QuestionTypes.Dropdown.Id, _dropdownType);

            // group by parentId so we have groups of children per parent
            var mapGroup = mapQuestionDefinition.GroupBy(a => a.ParentId);

            foreach (var map in mapGroup)
            {
                var childIds = map.ToList().Select(a => a.ChildId).ToList();

                var X = proposalQuestionAnswers.FirstOrDefault(a => a.QuestionDefinition.Id == map.Key);
                var parrentQaId =
                    proposalQuestionAnswers.Where(a => a.QuestionDefinition.Id == map.Key).Select(a => a.Id).First();
                var childrenQaId =
                    proposalQuestionAnswers.Where(a => childIds.Contains(a.QuestionDefinition.Id))
                        .Select(a => a.Id)
                        .ToList();

                var parent = dto.Answers.FirstOrDefault(y => y.Id == parrentQaId);
                var children = dto.Answers.Where(y => childrenQaId.Contains(y.Id)).ToList();

                children.ForEach(
                    a =>
                        _resolveMap[parent.QuestionType.Id].Invoke(new ResolveMapArgumentsDto(parent, a,
                            proposalQuestionAnswers)));
            }
        }
    }
}