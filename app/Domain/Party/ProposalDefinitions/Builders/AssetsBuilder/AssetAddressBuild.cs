﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Lookups.Addresses;
using Domain.Party.Addresses;
using Domain.Party.Assets;
using Domain.Party.Assets.AssetAddresses;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using MasterData;
using NHibernate.Proxy;

namespace Domain.Party.ProposalDefinitions.Builders.AssetsBuilder
{
    public class AssetAddressBuild : BuildBase, IBuildAsset
    {
        private static readonly ILog log = LogManager.GetLogger<AssetAddressBuild>();

        private static readonly List<int> AllowedCovers = new List<int>
        {
            Covers.Building.Id,
            Covers.BuildingAndContents.Id,
            Covers.Contents.Id,
            Covers.HouseOwners.Id,
            Covers.IdentityTheft.Id,
            Covers.FuneralCostsAddDeathBenefits.Id,
            Covers.PersonalLegalLiability.Id,
            Covers.DisasterCash.Id,
            Covers.AIGAssist.Id,
            Covers.PersonalAccident.Id,
            Covers.ComputerEquipment.Id,
            Covers.ExtendedPersonalLegalLiability.Id,
            Covers.Watercraft.Id,
            Covers.Geyser.Id,
            Covers.DisasterMortgage.Id,
            Covers.ApplianceRepair.Id,
            Covers.Kidnap.Id,
            Covers.LegalPlan.Id
        };

        private readonly IRepository _repository;
        private readonly SystemChannels _systemChannels;

        public AssetAddressBuild(IRepository repository, SystemChannels systemChannels)
        {
            _repository = repository;
            _systemChannels = systemChannels;
        }

        public Asset Build(ProposalDefinition proposalDefinition, AssetList assetList, DtoContext context)
        {
            if (!AllowedCovers.Contains(proposalDefinition.Cover.Id))
                return null;

            //do not execute from AIG
            if (_systemChannels.IsAllowed(proposalDefinition.Party.Channel.Id, new[] { "AIG" }))
                return null;

            Asset result = null;

            try
            {
                //find addressQuestion
                ProposalQuestionAnswer addressQuestion =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        a => a.QuestionDefinition.Question.Id == Questions.RiskAddress.Id);


                #region Validation

                if (addressQuestion == null || string.IsNullOrEmpty(addressQuestion.Answer))
                    return null;

                #endregion

                ProposalQuestionAnswer sumInsuredAnswer =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            (a.QuestionDefinition.Question.Id == Questions.AIGPersonalLiabilityPersonalLiabilityLimit.Id ||
                            a.QuestionDefinition.Question.Id == Questions.AIGIdentityTheftIDTheftValue.Id ||
                            a.QuestionDefinition.Question.Id == Questions.AIGDisasterCashCashLimit.Id ||
                            a.QuestionDefinition.Question.Id == Questions.SumAssured.Id ||
                            a.QuestionDefinition.Question.Id == Questions.SumInsured.Id)));

                decimal sumInsured = 0;

                if (sumInsuredAnswer != null)
                {
                    if (sumInsuredAnswer.QuestionDefinition.Question.Id == Questions.SumAssured.Id 
                        || sumInsuredAnswer.QuestionDefinition.Question.Id == Questions.SumInsured.Id)

                        sumInsured = (sumInsuredAnswer.QuestionType.Name.ToLower() == "dropdown")
                            ? GetDigitsFromQuestionAnswer(sumInsuredAnswer)
                            : SumInsured(sumInsuredAnswer);
                    else
                        sumInsured = GetDigitsFromQuestionAnswer(sumInsuredAnswer);
                }

                int addressId = int.TryParse(addressQuestion.Answer, out addressId) ? addressId : 0;

                //Existing Asset
                var asset = SaveExistingAsseProposalDef(addressId, addressQuestion, proposalDefinition, assetList, sumInsured);
                if (asset != null)
                    return asset;
                
                Address address = CreateOrGetExistingAddress(addressId, proposalDefinition);
                
                addressQuestion.Answer = address.Id.ToString();

                //New Asset
                result = SaveNewAssetProposalDef(proposalDefinition, address, assetList, sumInsured);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

  
        private void ManageExtras(Asset asset, IEnumerable<AssetExtrasDto> extras)
        {
            if (extras == null)
                return;

            IList<AssetExtrasDto> assetExtrasDtos = extras as IList<AssetExtrasDto> ?? extras.ToList();
            foreach (AssetExtras extra in asset.Extras)
            {
                AssetExtrasDto item = assetExtrasDtos.FirstOrDefault(a => a.Description == extra.Description);

                if (item == null)
                    extra.IsDeleted = true;
            }

            foreach (AssetExtrasDto extra in assetExtrasDtos)
            {
                if (extra.Id > 0)
                {
                }
                else
                {
                    var entity = Mapper.Map<AssetExtras>(extra);
                    var optionalExtra = new OptionalExtras().FirstOrDefault(x => x.Description == entity.Description);
                    if (optionalExtra != null)
                        entity.AccessoryType = optionalExtra.AccessoryType;
                    asset.Extras.Add(entity);
                }
            }
        }

        private decimal GetDigitsFromQuestionAnswer(ProposalQuestionAnswer proposalQuestionAnswer)
        {
            decimal answerOuput = 0;
            int answerId = 0;

            try
            {
                if (Int32.TryParse(proposalQuestionAnswer.Answer, out answerId))
                {
                    Regex digitsOnly = new Regex(@"[^0-9.]");
                    var answer = new QuestionAnswers().FirstOrDefault(x => x.Question.Id == proposalQuestionAnswer.QuestionDefinition.Question.Id && x.Id == answerId);

                    answerOuput = decimal.Parse(digitsOnly.Replace(answer.Answer, ""));
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return answerOuput;
        }

        private Asset SaveExistingAsseProposalDef(int addressId, ProposalQuestionAnswer addressQuestion,
            ProposalDefinition proposalDefinition, AssetList assetList,decimal sumInsured)
        {
            var criteriaDetail = new AssetInternalCriteria();
            Func<IList<Asset>, AssetInternalCriteria, Asset> criteriaFunction =
                (x, criteria) =>
                    x.FirstOrDefault(
                        a =>
                        {
                            var proxy = a as INHibernateProxy;
                            if (proxy != null)
                                a = (AssetAddress)proxy.HibernateLazyInitializer.GetImplementation();

                            return (a as AssetAddress).Address.Id == criteria.AddressId && (a as AssetAddress).AssetNo == criteriaDetail.AssetId;
                        });

            
            criteriaDetail.AddressId = addressId > 0 ? addressId : 0;
            criteriaDetail.AssetId = proposalDefinition.Id;

            var asset = assetList.GetAsset<AssetAddress>(criteriaFunction, criteriaDetail);

            if (asset == null)
                return null;

            ManageExtras(asset, assetList.AssetExtras);
            addressQuestion.Answer = addressId.ToString();
            if (proposalDefinition.Cover.Id != Covers.AIGAssist.Id)
                asset.SumInsured = sumInsured;
    
            _repository.Save(asset);
            log.DebugFormat("Adding asset {0} to proposalDefinition {1}", asset.Id, proposalDefinition.Id);
            proposalDefinition.Asset = asset;
            proposalDefinition.SumInsured = sumInsured;
            _repository.Save(proposalDefinition);
            return asset;
        }

        private Address CreateOrGetExistingAddress(int addressId, ProposalDefinition proposalDefinition )
        {
            if(addressId > 0)
                return  _repository.GetAll<Address>().FirstOrDefault(a => a.Id == addressId);


            //Try create address
            ProposalQuestionAnswer address1 =
                proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    (a =>
                        a.QuestionDefinition != null &&
                        a.QuestionDefinition.Question.Id == Questions.RiskAddress.Id));

            ProposalQuestionAnswer suburb =
                proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    (a =>
                        a.QuestionDefinition != null &&
                        a.QuestionDefinition.Question.Id == Questions.Suburb.Id));

            ProposalQuestionAnswer postalCode =
                proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    (a =>
                        a.QuestionDefinition != null &&
                        a.QuestionDefinition.Question.Id == Questions.PostalCode.Id));

            if (address1 == null || address1.Answer == "" || suburb == null || suburb.Answer == "" ||
                postalCode == null || postalCode.Answer == "")
            {
                log.InfoFormat("Unable to create Address Asset: Missing answers");
                return null;
            }

            var address =
                _repository.GetAll<Address>()
                    .FirstOrDefault(
                        x => x.Line1.ToLower().Equals(address1.Answer.ToLower()) && x.Code.Equals(postalCode.Answer));
            
            if(address !=  null)
                return address;


            var mapper = new ZAStateProvinceMapper(_repository);
            var newAddress = new Address
            {
                MasterId = 0,
                Line1 = address1.Answer,
                Line2 = "",
                Line3 = "",
                Line4 = suburb.Answer,
                Code = postalCode.Answer,
                StateProvince = mapper.GetStateProvinceByPostCode(postalCode.Answer),
                AddressType = AddressTypes.PhysicalAddress,
                Complex = "",
                IsComplex = false,
                IsDefault = false,
                Party = proposalDefinition.Party
            };

            _repository.Save(newAddress);
            address1.Answer = newAddress.Id.ToString();

            return newAddress;;
        }

        private Asset SaveNewAssetProposalDef(ProposalDefinition proposalDefinition, Address address, AssetList assetList, decimal sumInsured)
        {
            var asset = new AssetAddress
            {
                Party = proposalDefinition.Party,
                Address = address,
                Description = address.Description,
                AssetNo = proposalDefinition.Id, // no assetno yet
                SumInsured = sumInsured
            };
            ManageExtras(asset, assetList.AssetExtras);
            _repository.Save(asset);

            //Save Id to proposal def
            log.DebugFormat("Adding asset {0} to proposalDefinition {1}", asset.Id, proposalDefinition.Id);
            proposalDefinition.Asset = asset;

            if (proposalDefinition.ProposalHeader != null)
            {
                proposalDefinition.SumInsured = sumInsured;
                _repository.Save(proposalDefinition);
            }
            return asset;
        }


    
    }
}