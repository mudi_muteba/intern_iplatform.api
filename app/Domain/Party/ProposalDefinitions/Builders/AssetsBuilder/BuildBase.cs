﻿using System;
using System.Globalization;
using System.Linq;
using Domain.Base.Repository;
using Domain.Products;
using Domain.QuestionDefinitions;
using MasterData;
using Raven.Abstractions.Json;

namespace Domain.Party.ProposalDefinitions.Builders.AssetsBuilder
{
    public abstract class BuildBase
    {
        protected static ProposalQuestionAnswer SetAssetQuestion(IRepository repository, ProposalDefinition proposalDefinition)
        {
            ProposalQuestionAnswer assetQuestion =
                proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(a => a.QuestionDefinition.Question.Id == Questions.Asset.Id);


            if (assetQuestion == null)
            {
                var coverDefinitions = repository.GetAll<CoverDefinition>().ToList();
                var coverDefinitionId = coverDefinitions
                    .Where(
                        a => a.Product.Id == proposalDefinition.Product.Id
                        && a.Cover.Id == proposalDefinition.Cover.Id)
                    .Select(a => a.Id)
                    .FirstOrDefault();

                var assetQuestionDefinition = repository.GetAll<QuestionDefinition>()
                    .FirstOrDefault(
                        a => a.CoverDefinition.Id == coverDefinitionId && a.Question.Id == Questions.Asset.Id);

                var questionAnswer = new ProposalQuestionAnswer
                {
                    QuestionDefinition = assetQuestionDefinition,
                    Answer = string.Empty,
                    ProposalDefinition = proposalDefinition,
                };

                proposalDefinition.AddProposalQuestionAnswer(questionAnswer);
            }
            return assetQuestion;
        }


        internal VehicleType ResolveVehicleType(ProposalQuestionAnswer vehicleTypeAnswer)
        {
            if (vehicleTypeAnswer == null || string.IsNullOrWhiteSpace(vehicleTypeAnswer.Answer))
                return null;

            int value;
            var testValue = Int32.TryParse(vehicleTypeAnswer.Answer, out value);

            if (testValue == false)
                return null;


            return
                new VehicleTypes().FirstOrDefault(
                    a => vehicleTypeAnswer != null && a.Id == int.Parse(vehicleTypeAnswer.Answer));

        }

        internal decimal SumInsured(ProposalQuestionAnswer sumInsuredAnswer)
        {
            if (sumInsuredAnswer != null && sumInsuredAnswer.Answer != null &&
                string.IsNullOrWhiteSpace(sumInsuredAnswer.Answer))
                sumInsuredAnswer.Answer = "0";

            decimal sumInsured = 0;

            if (sumInsuredAnswer != null && sumInsuredAnswer.Answer != null)
                sumInsured = decimal.Parse(sumInsuredAnswer.Answer, CultureInfo.InvariantCulture);
            return sumInsured;
        }
    }
}