﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Party.Assets;
using Domain.Party.Assets.AssetRiskItems;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using MasterData;
using NHibernate.Proxy;

namespace Domain.Party.ProposalDefinitions.Builders.AssetsBuilder
{

    public class AssetRiskItemBuild : BuildBase, IBuildAsset
    {
        private static readonly ILog log = LogManager.GetLogger<AssetRiskItemBuild>();

        private static readonly List<int> AllowedCovers = new List<int>
        {
            Covers.AllRisk.Id
        };

        private readonly IRepository _repository;

        public AssetRiskItemBuild(IRepository repository)
        {
            _repository = repository;
        }


        public Asset Build(ProposalDefinition proposalDefinition, AssetList assetList, DtoContext context)
        {

            if (!AllowedCovers.Contains(proposalDefinition.Cover.Id))
                return null;

            Asset result = null;

            if (proposalDefinition == null)
                throw new Exception("proposalDefinition is null");

            if (proposalDefinition.ProposalQuestionAnswers == null)
                throw new Exception("ProposalQuestionAnswer is null");

            try
            {
                var assetQuestion = SetAssetQuestion(_repository, proposalDefinition);

                var serialIMEINumber =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            (a.QuestionDefinition.Question.Id == MasterData.Questions.SerialIMEINumber.Id
                            || a.QuestionDefinition.Question.Id == MasterData.Questions.MFAllRiskSerialIMEINumber.Id
                            || a.QuestionDefinition.Question.Id == MasterData.Questions.AIGAllRisksSerialNumber.Id
                            )));

                var allRiskCategory =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            (a.QuestionDefinition.Question.Id == MasterData.Questions.AllRiskCategory.Id
                            || a.QuestionDefinition.Question.Id == MasterData.Questions.MFAllRiskRiskCategory.Id
                            || a.QuestionDefinition.Question.Id == MasterData.Questions.AIGAllRisksCategory.Id
                            || a.QuestionDefinition.Question.Id == MasterData.Questions.KPAllRiskCategory.Id
                            )));

                var description =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            (a.QuestionDefinition.Question.Id == MasterData.Questions.ItemDescription.Id
                            || a.QuestionDefinition.Question.Id == MasterData.Questions.MFAllRiskItemDescription.Id
                            || a.QuestionDefinition.Question.Id == MasterData.Questions.AIGAllRisksItemDescription.Id
                            )));

                var assetAnswer =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a => a.QuestionDefinition != null && a.QuestionDefinition.Question.Id == MasterData.Questions.Asset
                            .Id));

                ProposalQuestionAnswer sumInsuredAnswer =
                   proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                       (a =>
                           a.QuestionDefinition != null &&
                           (a.QuestionDefinition.Question.Id == MasterData.Questions.SumInsured.Id)));

                decimal sumInsured = SumInsured(sumInsuredAnswer);

                var criteriaDetail = new AssetInternalCriteria();

                var serialNumber = string.Empty;
                int assetId = 0;
                if (serialIMEINumber != null)
                    serialNumber = serialIMEINumber.Answer;
                if (assetAnswer != null && !string.IsNullOrEmpty(assetAnswer.Answer))
                {
                    bool parsed = int.TryParse(assetAnswer.Answer, out assetId);
                    if (!parsed)
                    {
                        assetId = 0;
                    }
                }

                if (!string.IsNullOrEmpty(serialNumber))
                    criteriaDetail.SerialIMEINumber = serialNumber;
                if (assetId > 0)
                    criteriaDetail.AssetId = assetId;

                AssetRiskItem asset = proposalDefinition.Asset as AssetRiskItem;


                var categoryName = "Category not set";
                if (!string.IsNullOrWhiteSpace(allRiskCategory.Answer)) // assuming allRiskCategory.Answer will always be an int
                {
                    var firstOrDefault = new QuestionAnswers().Where(a => a.Question.Id == allRiskCategory.QuestionDefinition.Question.Id)
                        .ToList().FirstOrDefault(x => x.Id == int.Parse(allRiskCategory.Answer));
                    if (firstOrDefault != null)
                    {
                        categoryName =
                            firstOrDefault.Answer;
                    }
                }

                var allRiskDescription = string.Format("{0} {1} {2}"
                    , description == null ? string.Empty : description.Answer
                    , categoryName
                    , serialIMEINumber == null ? string.Empty : (string.IsNullOrWhiteSpace(serialIMEINumber.Answer) ? string.Empty : string.Format("- {0}", serialIMEINumber.Answer)));

                int n;
                bool isNumeric = int.TryParse(allRiskCategory.Answer, out n);

                if (asset != null)
                {
                    asset.SerialIMEINumber = serialNumber;
                    asset.Description = allRiskDescription;
                    asset.AllRiskCategory = isNumeric ? n : 0;

                    assetQuestion.Answer = asset.Id.ToString();
                    ManageExtras(asset, assetList.AssetExtras);
                    asset.SumInsured = sumInsured;
                    _repository.Save(asset) ;
                    proposalDefinition.Asset = asset;
                    proposalDefinition.SumInsured = sumInsured;
                    proposalDefinition.Description = asset.Description;
                    _repository.Save(proposalDefinition);
                    return asset;
                }


                if (string.IsNullOrWhiteSpace(allRiskDescription))
                {
                    log.InfoFormat("Unable to create Vehicle Asset: Missing questions");
                    return null;
                }

                asset = new AssetRiskItem
                {
                    Party = proposalDefinition.Party,
                    SerialIMEINumber = serialNumber,
                    Description = allRiskDescription,
                    AllRiskCategory = isNumeric ? n : 0,
                    AssetNo = proposalDefinition.Id,
                    SumInsured = sumInsured
                };

                ManageExtras(asset, assetList.AssetExtras);
                _repository.Save(asset);
                assetQuestion.Answer = asset.Id.ToString();
                proposalDefinition.Asset = asset;

                if (proposalDefinition.ProposalHeader != null)
                    proposalDefinition.SumInsured = sumInsured;
                _repository.Save(proposalDefinition);

                result = asset;
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }

        private static void ManageExtras(Asset asset, IEnumerable<AssetExtrasDto> extras)
        {
            if (extras == null)
                return;

            IList<AssetExtrasDto> assetExtrasDtos = extras as IList<AssetExtrasDto> ?? extras.ToList();
            foreach (AssetExtras extra in asset.Extras)
            {
                AssetExtrasDto item = assetExtrasDtos.FirstOrDefault(a => a.Description == extra.Description);

                if (item == null)
                    extra.IsDeleted = true;
            }

            foreach (AssetExtrasDto extra in assetExtrasDtos)
            {
                if (extra.Id > 0)
                {
                    asset.Extras.FirstOrDefault(a => a.Id == extra.Id).MapFromDto(extra);
                }
                else
                {
                    var entity = Mapper.Map<AssetExtras>(extra);
                    var optionalExtra = new OptionalExtras().FirstOrDefault(x => x.Description == entity.Description);
                    if (optionalExtra != null)
                        entity.AccessoryType = optionalExtra.AccessoryType;
                    asset.Extras.Add(entity);
                }
            }
        }
    }
}



        