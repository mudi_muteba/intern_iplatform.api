﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Lookups.Addresses;
using Domain.Party.Addresses;
using Domain.Party.Assets;
using Domain.Party.Assets.AssetAddresses;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using MasterData;
using NHibernate.Proxy;

namespace Domain.Party.ProposalDefinitions.Builders.AssetsBuilder
{
    public class AigAssetAddressBuild : BuildBase, IBuildAsset
    {
        private static readonly ILog log = LogManager.GetLogger<AssetAddressBuild>();

        private static readonly List<int> AllowedCovers = new List<int>
        {
            Covers.Building.Id,
            Covers.BuildingAndContents.Id,
            Covers.Contents.Id,
            Covers.HouseOwners.Id,
            Covers.IdentityTheft.Id,
            Covers.FuneralCostsAddDeathBenefits.Id,
            Covers.PersonalLegalLiability.Id,
            Covers.DisasterCash.Id,
            Covers.AIGAssist.Id,
        };

        private readonly IRepository _repository;
        private readonly SystemChannels _systemChannels;

        public AigAssetAddressBuild(IRepository repository, SystemChannels systemChannels)
        {
            _repository = repository;
            _systemChannels = systemChannels;
        }

        public Asset Build(ProposalDefinition proposalDefinition, AssetList assetList, DtoContext context)
        {
            if (!AllowedCovers.Contains(proposalDefinition.Cover.Id))
                return null;

            //execute only for AIG from AIG
            if (!_systemChannels.IsAllowed(proposalDefinition.Party.Channel.Id, new[] { "AIG" }))
                return null;


            Asset result = null;

            try
            {
                //find addressQuestion
                ProposalQuestionAnswer addressQuestion =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        a => a.QuestionDefinition.Question.Id == Questions.RiskAddress.Id);


                #region Validation

                if (addressQuestion == null || string.IsNullOrEmpty(addressQuestion.Answer))
                    return null;

                #endregion

                ProposalQuestionAnswer sumInsuredAnswer =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            (a.QuestionDefinition.Question.Id == Questions.AIGPersonalLiabilityPersonalLiabilityLimit.Id ||
                            a.QuestionDefinition.Question.Id == Questions.AIGIdentityTheftIDTheftValue.Id ||
                            a.QuestionDefinition.Question.Id == Questions.AIGDisasterCashCashLimit.Id ||
                            a.QuestionDefinition.Question.Id == Questions.SumAssured.Id ||
                            a.QuestionDefinition.Question.Id == Questions.SumInsured.Id)));

                decimal sumInsured = 0;

                if (sumInsuredAnswer != null)
                {
                    if (sumInsuredAnswer.QuestionDefinition.Question.Id == Questions.SumAssured.Id
                        || sumInsuredAnswer.QuestionDefinition.Question.Id == Questions.SumInsured.Id)

                        sumInsured = (sumInsuredAnswer.QuestionType.Name.ToLower() == "dropdown")
                            ? GetDigitsFromQuestionAnswer(sumInsuredAnswer)
                            : SumInsured(sumInsuredAnswer);
                    else
                        sumInsured = GetDigitsFromQuestionAnswer(sumInsuredAnswer);
                }

                int addressId = int.TryParse(addressQuestion.Answer, out addressId) ? addressId : 0;
                Address address = CreateOrGetExistingAddress(addressId, proposalDefinition);


                if (proposalDefinition.Asset == null)
                    SaveNewAssetProposalDef(proposalDefinition, address, assetList, sumInsured);
                else
                    proposalDefinition.Asset = SaveExistingAsseProposalDef(address, addressQuestion, proposalDefinition, assetList, sumInsured);

                addressQuestion.Answer = address.Id.ToString();

                //New Asset
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        private void ManageExtras(Asset asset, IEnumerable<AssetExtrasDto> extras)
        {
            if (extras == null)
                return;

            IList<AssetExtrasDto> assetExtrasDtos = extras as IList<AssetExtrasDto> ?? extras.ToList();
            foreach (AssetExtras extra in asset.Extras)
            {
                AssetExtrasDto item = assetExtrasDtos.FirstOrDefault(a => a.Description == extra.Description);

                if (item == null)
                    extra.IsDeleted = true;
            }

            foreach (AssetExtrasDto extra in assetExtrasDtos)
            {
                if (extra.Id > 0)
                {
                }
                else
                {
                    var entity = Mapper.Map<AssetExtras>(extra);
                    var optionalExtra = new OptionalExtras().FirstOrDefault(x => x.Description == entity.Description);
                    if (optionalExtra != null)
                        entity.AccessoryType = optionalExtra.AccessoryType;
                    asset.Extras.Add(entity);
                }
            }
        }

        private decimal GetDigitsFromQuestionAnswer(ProposalQuestionAnswer proposalQuestionAnswer)
        {
            decimal answerOuput = 0;
            int answerId = 0;

            try
            {
                if (Int32.TryParse(proposalQuestionAnswer.Answer, out answerId))
                {
                    Regex digitsOnly = new Regex(@"[^0-9.]");
                    var answer = new QuestionAnswers().FirstOrDefault(x => x.Question.Id == proposalQuestionAnswer.QuestionDefinition.Question.Id && x.Id == answerId);

                    answerOuput = decimal.Parse(digitsOnly.Replace(answer.Answer, ""));
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return answerOuput;
        }

        private Asset SaveExistingAsseProposalDef(Address address, ProposalQuestionAnswer addressQuestion,
            ProposalDefinition proposalDefinition, AssetList assetList, decimal sumInsured)
        {
            AssetAddress asset = proposalDefinition.Asset as AssetAddress;
            asset.Address = address;

            asset.Party = proposalDefinition.Party;
            asset.Address = address;
            asset.Description = address.Description;
            asset.AssetNo = proposalDefinition.Id; // no assetno yet
            asset.SumInsured = sumInsured;
            addressQuestion.Answer = address.Id.ToString();


            ManageExtras(asset, assetList.AssetExtras);
            if (proposalDefinition.Cover.Id != Covers.AIGAssist.Id)
                asset.SumInsured = sumInsured;

            _repository.Save(asset);
            log.DebugFormat("Adding asset {0} to proposalDefinition {1}", asset.Id, proposalDefinition.Id);
            proposalDefinition.SumInsured = sumInsured;
            _repository.Save(proposalDefinition);
            return asset;
        }

        private Address CreateOrGetExistingAddress(int addressId, ProposalDefinition proposalDefinition)
        {
            if (addressId > 0)
                return _repository.GetAll<Address>().FirstOrDefault(a => a.Id == addressId);


            //Try create address
            ProposalQuestionAnswer address1 =
                proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    (a =>
                        a.QuestionDefinition != null &&
                        a.QuestionDefinition.Question.Id == Questions.RiskAddress.Id));

            ProposalQuestionAnswer suburb =
                proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    (a =>
                        a.QuestionDefinition != null &&
                        a.QuestionDefinition.Question.Id == Questions.Suburb.Id));

            ProposalQuestionAnswer postalCode =
                proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                    (a =>
                        a.QuestionDefinition != null &&
                        a.QuestionDefinition.Question.Id == Questions.PostalCode.Id));

            if (address1 == null || address1.Answer == "" || suburb == null || suburb.Answer == "" ||
                postalCode == null || postalCode.Answer == "")
            {
                log.InfoFormat("Unable to create Address Asset: Missing answers");
                return null;
            }

            var address =
                _repository.GetAll<Address>()
                    .FirstOrDefault(
                        x => x.Line1.ToLower().Equals(address1.Answer.ToLower()) && x.Code.Equals(postalCode.Answer));

            if (address != null)
                return address;


            var mapper = new ZAStateProvinceMapper(_repository);
            var newAddress = new Address
            {
                MasterId = 0,
                Line1 = address1.Answer,
                Line2 = "",
                Line3 = "",
                Line4 = suburb.Answer,
                Code = postalCode.Answer,
                StateProvince = mapper.GetStateProvinceByPostCode(postalCode.Answer),
                AddressType = AddressTypes.PhysicalAddress,
                Complex = "",
                IsComplex = false,
                IsDefault = false,
                Party = proposalDefinition.Party
            };

            _repository.Save(newAddress);
            address1.Answer = newAddress.Id.ToString();

            return newAddress; ;
        }

        private Asset SaveNewAssetProposalDef(ProposalDefinition proposalDefinition, Address address, AssetList assetList, decimal sumInsured)
        {
            var asset = new AssetAddress
            {
                Party = proposalDefinition.Party,
                Address = address,
                Description = address.Description,
                AssetNo = proposalDefinition.Id, // no assetno yet
                SumInsured = sumInsured
            };
            ManageExtras(asset, assetList.AssetExtras);
            _repository.Save(asset);

            //Save Id to proposal def
            log.DebugFormat("Adding asset {0} to proposalDefinition {1}", asset.Id, proposalDefinition.Id);
            proposalDefinition.Asset = asset;

            if (proposalDefinition.ProposalHeader != null)
            {
                proposalDefinition.SumInsured = sumInsured;
                _repository.Save(proposalDefinition);
            }
            return asset;
        }



    }
}