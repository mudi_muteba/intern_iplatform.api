﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Party.Assets;
using Domain.Party.Assets.AssetVehicles;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using MasterData;

namespace Domain.Party.ProposalDefinitions.Builders.AssetsBuilder
{
    public class AssetVehicleBuild : BuildBase, IBuildAsset
    {
        private static readonly ILog log = LogManager.GetLogger<AssetVehicleBuild>();

        private static readonly List<int> AllowedCovers = new List<int>
        {
            Covers.Motor.Id,
            Covers.Windscreen.Id,
            Covers.TouchUp.Id,
            Covers.ExcessProtect.Id,
            Covers.TyreProtect.Id,
            Covers.TyreRim.Id,
            Covers.MotorWarranty.Id,
            Covers.CaravanOrTrailer.Id,
            Covers.MotorExternal.Id,
            Covers.Motorcycle.Id,
            Covers.ServicePlan.Id,
            Covers.ExcessEraser.Id,
            Covers.CosmeticPlan.Id,
            Covers.PowerTrainWarranty.Id
        };

        private static readonly List<Question> VehicleTypes = new List<Question>
        {
            Questions.VehicleType,
            Questions.AIGMotorTypeOfVehicle
        };

        private readonly IRepository _repository;

        public AssetVehicleBuild(IRepository repository)
        {
            _repository = repository;
        }

        public Asset Build(ProposalDefinition proposalDefinition, AssetList assetList, DtoContext context)
        {
            if (!AllowedCovers.Contains(proposalDefinition.Cover.Id))
                return null;


            Func<IList<Asset>, AssetInternalCriteria, Asset> criteriaFunction =
                (x, criteria) =>
                    x.FirstOrDefault(
                        a => a.Id == criteria.AssetId);

            Asset result = null;

            try
            {
                //find assetQuestion
                var assetQuestion = SetAssetQuestion(_repository, proposalDefinition);

                #region Get Questions

                var vehicleRegistrationNumber =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            a.QuestionDefinition.Question.Id == Questions.VehicleRegistrationNumber.Id));
                var vehicleMMCode =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            a.QuestionDefinition.Question.Id == Questions.VehicleMMCode.Id));
                var vehicleMake =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            a.QuestionDefinition.Question.Id == Questions.VehicleMake.Id));
                var vehicleModel =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            a.QuestionDefinition.Question.Id == Questions.VehicleModel.Id));
                var yearOfManufacture =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            a.QuestionDefinition.Question.Id == Questions.YearOfManufacture.Id));

                var caravanTrailerType =
                  proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                       (a =>
                           a.QuestionDefinition != null &&
                           a.QuestionDefinition.Question.Id == Questions.KPTrailerCaravanType.Id));

                // resolve the vehicle type contained in the VehicleTypes list
                var vehicleType =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            VehicleTypes.Contains(a.QuestionDefinition.Question)
                            ));


                var assetAnswer =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a => a.QuestionDefinition != null && a.QuestionDefinition.Question.Id == Questions.Asset
                            .Id));

                var sumInsuredAnswer =
                    proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(
                        (a =>
                            a.QuestionDefinition != null &&
                            (a.QuestionDefinition.Question.Id == Questions.SumInsured.Id)));

                #endregion

                var sumInsured = SumInsured(sumInsuredAnswer);

                var resolvedVehicleType = ResolveVehicleType(vehicleType);


                var criteriaDetail = new AssetInternalCriteria();

                var regNumber = vehicleRegistrationNumber != null ? vehicleRegistrationNumber.Answer : string.Empty;

                var assetId = 0;
                if (assetAnswer != null && !string.IsNullOrEmpty(assetAnswer.Answer))
                    assetId = int.TryParse(assetAnswer.Answer, out assetId) ? assetId : 0;

                #region Update existing asset

                //Set Criteria & Get Asset
                criteriaDetail.VehicleRegistrationNumber = !string.IsNullOrEmpty(regNumber) ? regNumber : string.Empty;
                criteriaDetail.AssetId = assetId > 0 ? assetId : 0;
                var asset = assetList.GetAsset<AssetVehicle>(criteriaFunction, criteriaDetail);

                if (asset != null)
                {
                    ManageExtras(asset, assetList.AssetExtras);
                    asset.SumInsured = sumInsured;
                    _repository.Save(asset);
                    assetQuestion.Answer = asset.Id.ToString();
                    //Save Id to proposal def
                    log.DebugFormat("Adding asset {0} to proposalDefinition {1}", assetId, proposalDefinition.Id);
                    proposalDefinition.Asset = asset;
                    proposalDefinition.SumInsured = sumInsured;
                    return asset;
                }

                #endregion

                #region Validation

                if (vehicleMMCode == null || vehicleMake == null ||
                    vehicleModel == null || yearOfManufacture == null)
                {
                    log.InfoFormat("Unable to create Vehicle Asset: Missing questions");
                    return null;
                }

                int n;
                var isNumeric = int.TryParse(yearOfManufacture.Answer, out n);
                if (!isNumeric)
                {
                    log.InfoFormat("Unable to create Vehicle Asset: yearOfManufacture is not numeric");
                    return null;
                }

                var registrationNumber = string.Empty;

                if (vehicleRegistrationNumber != null && vehicleRegistrationNumber.Answer != null)
                    registrationNumber = vehicleRegistrationNumber.Answer;

                #endregion

                #region New Asset

                string proposalDescription = "";
                string descriptionType = "";
                if (caravanTrailerType != null)
                {
                    descriptionType = new QuestionAnswers().FirstOrDefault(x => x.Id == Convert.ToInt32(caravanTrailerType.Answer)).Answer;
                }
                if (proposalDefinition.Cover.Id == 62)
                {
                    proposalDescription = string.Format("{0}, {1}",
                        descriptionType,
                        yearOfManufacture.Answer);
                }
                else
                {
                    proposalDescription = string.Format("{0}, {1}, {2}, {3}",
                        vehicleMake.Answer,
                        vehicleModel.Answer,
                        yearOfManufacture.Answer,
                        registrationNumber);
                }


                asset = new AssetVehicle
                {
                    AssetNo = proposalDefinition.Id, //no assetno yet
                    Party = proposalDefinition.Party,
                    VehicleMMCode = vehicleMMCode.Answer,
                    VehicleMake = vehicleMake.Answer,
                    VehicleModel = vehicleModel.Answer,
                    VehicleRegistrationNumber = registrationNumber,
                    YearOfManufacture = int.Parse(yearOfManufacture.Answer),
                    VehicleType = resolvedVehicleType,
                    SumInsured = sumInsured,
                    Description = proposalDescription
                };

                ManageExtras(asset, assetList.AssetExtras);
                _repository.Save(asset);

                log.DebugFormat("Adding asset {0} to proposalDefinition {1}", assetId, proposalDefinition.Id);
                proposalDefinition.Asset = asset;

                if (proposalDefinition.ProposalHeader != null)
                    proposalDefinition.SumInsured = sumInsured;
                _repository.Save(proposalDefinition);

                assetQuestion.Answer = asset.Id.ToString();
                result = asset;

                #endregion
            }
            catch (Exception e)
            {
                log.ErrorFormat("Unable to add asset {0}", e.Message);
                throw;
            }

            return result;
        }

        private void ManageExtras(Asset asset, IEnumerable<AssetExtrasDto> extras)
        {
            if (extras == null)
                return;

            var assetExtrasDtos = extras as IList<AssetExtrasDto> ?? extras.ToList();
            foreach (var extra in asset.Extras)
            {
                var item = assetExtrasDtos.FirstOrDefault(a => a.Description == extra.Description);

                if (item == null)
                    extra.IsDeleted = true;
            }

            foreach (var extra in assetExtrasDtos)
            {
                var entity = _repository.GetById<AssetExtras>(extra.Id) ?? Mapper.Map<AssetExtras>(extra);
                entity.Selected = extra.Selected;
                entity.Description = extra.Description;
                if (extra.Id == 0)
                {
                    var optionalExtra = new OptionalExtras().FirstOrDefault(x => x.Description == entity.Description);
                    if (optionalExtra != null)
                        entity.AccessoryType = optionalExtra.AccessoryType;
                }
                asset.AddAssetExtras(entity);
            }
        }
    }
}