﻿using System.Collections.Generic;
using Domain.Base.Repository;
using Domain.Party.Assets;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;

namespace Domain.Party.ProposalDefinitions.Builders.AssetsBuilder
{
   public interface IBuildAsset
    {
       Asset Build(ProposalDefinition proposalDefinition, AssetList assetList, DtoContext context);

    }
}
