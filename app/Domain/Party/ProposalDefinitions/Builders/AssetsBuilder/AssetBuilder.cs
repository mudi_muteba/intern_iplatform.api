﻿using System;
using System.Collections.Generic;
using Domain.Base.Repository;
using Domain.Party.Assets;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using RabbitMQ.Client.Framing.Impl;
using Common.Logging;
using iPlatform.Api.DTOs.Admin;

namespace Domain.Party.ProposalDefinitions.Builders.AssetsBuilder
{
    public class AssetBuilder
    {
        private readonly SystemChannels _systemChannels;
        private readonly List<IBuildAsset> _assetBuilders;

        public AssetBuilder(IRepository repository, SystemChannels systemChannels)
        {
            _systemChannels = systemChannels;
            _assetBuilders = new List<IBuildAsset>();
            
            _assetBuilders.Add(new AssetVehicleBuild(repository));
            _assetBuilders.Add(new AssetRiskItemBuild(repository));
            _assetBuilders.Add(new AigAssetAddressBuild(repository, systemChannels));
            _assetBuilders.Add(new AssetAddressBuild(repository, systemChannels));
        }

        public Asset Build(ProposalDefinition definition, AssetList assetList, DtoContext context)
        {
            Asset asset = null;
            foreach (IBuildAsset assetBuilder in _assetBuilders)
            {
                asset = assetBuilder.Build(definition, assetList, context);
                if (asset != null)
                    break;
            }
            return asset;
        }
    }





}