﻿using Domain.Party.Assets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;

namespace Domain.Party.ProposalDefinitions.Builders.AssetsBuilder
{
    public static class AssetHelper
    {
        public static AssetList GetAssetList(IRepository repository, int partyId)
        {
            var assetList = new AssetList();

            var assets = repository.GetAll<Asset>().Where(a => a.Party.Id == partyId && a.IsDeleted != true).ToList();
            assetList.AddRange(assets);

            return assetList;
        }

        public static AssetExtras MapFromDto(this AssetExtras assetVehicleExtras, AssetExtrasDto model)
        {
            try
            {
                if (model == null)
                    return null;

                assetVehicleExtras.AccessoryType = model.AccessoryType;
                assetVehicleExtras.Description = model.Description;
                assetVehicleExtras.IsUserDefined = model.IsUserDefined;
                assetVehicleExtras.Selected = model.Selected;
                assetVehicleExtras.Value = model.Value;

                return assetVehicleExtras;
            }
            catch (Exception e)
            {
                var log = LogManager.GetLogger<AssetExtrasDto>();
                log.ErrorFormat("{0},Error message: {1}", "MapFromDtoToEntity", e.Message);
                throw;
            }
        }
    }
}
