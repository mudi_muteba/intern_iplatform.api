using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Party.Contacts;
using Domain.Party.ProposalDefinitions.Builders.OptionsBuilders;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;
using MoreLinq;
using NHibernate.Proxy;

namespace Domain.Party.ProposalDefinitions.Builders
{
    public class ProposalDefinitionBuilderDto
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(ProposalDefinitionBuilderDto));
        private readonly IRepository _repository;

        public ProposalDefinitionBuilderDto(IRepository repository)
        {
            _repository = repository;
        }

        public ProposalDefinitionDto Build(ProposalDefinition definition)
        {
            if (definition == null)
            {
                log.ErrorFormat("Proposal Definition not found");
                return null;
            }

            var model = Mapper.Map<ProposalDefinitionDto>(definition);
            var qdList = new List<ProposalQuestionDefinitionDto>();
            var party = definition.ProposalHeader.Party;
            var proxy = party as INHibernateProxy;

            if (proxy != null)
                party = (Individual)proxy.HibernateLazyInitializer.GetImplementation();



            //check marital status in full details
            var inidividualLead = _repository.GetAll<Individual>().Single(p => p.Lead.Party.Id == party.Id);

            if (inidividualLead.MaritalStatus == null && definition.Cover.Id == 218)
            {
                var proposalMaritalStatus = definition[Questions.MainDriverMaritalStatus.Id];
                if (!string.IsNullOrEmpty(proposalMaritalStatus.Answer))
                {
                    var maritalId = new QuestionAnswers().FirstOrDefault(x => x.Id == int.Parse(proposalMaritalStatus.Answer));
                    if (maritalId != null) inidividualLead.UpdateMaritalStatus(maritalId.Answer);
                }
            }
            //TODO: Hibernate mapping issue
            var mainDriverIdNumber = definition[Questions.MainDriverIDNumber.Id];

            if (mainDriverIdNumber == null || string.IsNullOrWhiteSpace(mainDriverIdNumber.Answer))
                model.MainDriverPartyId = definition.Party.Id;
            else
            {
                Contact contact = null;
                var contactIds = definition.Party.Relationships.Select(a => a.ChildParty.Id).ToList();
                if (contactIds.Any())
                    contact =
                        _repository.GetAll<Contact>()
                            .FirstOrDefault(
                                a => contactIds.Contains(a.Id) && a.IdentityNo.Equals(mainDriverIdNumber.Answer));

                model.MainDriverPartyId = contact != null ? contact.Id : definition.Party.Id;
            }

            //Map the question definition and Answers to DTO's
            foreach (var proposalQuestionAnswer in definition.ProposalQuestionAnswers)
            {
                //if this is null its because some deleleted te question definition and forgot about the proposalQuestionAnswer
                if (proposalQuestionAnswer.QuestionDefinition == null)
                    continue;

                var answer = Mapper.Map<ProposalDefinitionAnswerDto>(proposalQuestionAnswer);
                var qdDto = Mapper.Map<ProposalQuestionDefinitionDto>(proposalQuestionAnswer.QuestionDefinition);

                var titleObj = ((Individual)party).Title;
                var title = titleObj == null ? "" : titleObj.Name;

                qdDto.ToolTip = qdDto.ToolTip
                    .Replace("{Title}", title)
                    .Replace("{Name}", ((Individual)party).DisplayName)
                    ;

                qdDto.AnswerDto = answer;
                qdList.Add(qdDto);
            }

            //Group the questions 
            var groups = qdList.GroupBy(a => a.Question.QuestionGroup.Id).ToList();
            var questionGroups = new QuestionGroups();

            foreach (var proposalQuestionDefinitionDtos in groups)
            {
                var item = questionGroups.First(a => a.Id == proposalQuestionDefinitionDtos.Key);

                model.QuestionGroups.Add(new QuestionGroupDto
                {
                    Id = item.Id,
                    Name = item.Name,
                    VisibleIndex = item.VisibleIndex,
                    QuestionDefinitions = proposalQuestionDefinitionDtos.ToList()
                });
            }

            //Re index vap products for group Valueaddedproducts
            var visibleIndexCounter = 0;
            var vapGroup = model.QuestionGroups.FirstOrDefault(a => a.Id == QuestionGroups.Valueaddedproducts.Id);

            if (vapGroup != null)
            {
                //find parrent question for each VAP product mapped to the main product
                var mainIds = vapGroup.QuestionDefinitions.Where(a => a.ProductId == definition.Product.Id).Select(a => a.Id).ToList();
                var mappedVapQuestions = _repository.GetAll<MapQuestionDefinition>().Where(a => mainIds.Contains(a.ParentId)).ToList();
                //group by parent question to re index
                var vapProductgroups = mappedVapQuestions.GroupBy(a => a.ParentId).ToList();

                foreach (var group in vapProductgroups)
                {
                    vapGroup.QuestionDefinitions.First(a => a.Id == group.Key).VisibleIndex =
                        visibleIndexCounter++;
                    foreach (var mapQuestionDefinition in group)
                    {
                        var question = vapGroup.QuestionDefinitions.FirstOrDefault(a => a.Id == mapQuestionDefinition.ChildId);
                        if (question != null) question.VisibleIndex = visibleIndexCounter++;
                    }
                }
            }

            //FCM adding cover level wording info to response
            model.CoverLevelWording = "";
            if (definition.Product.CoverDefinitions.Any())
            {
                var coverwordinglevel = definition.Product.CoverDefinitions.FirstOrDefault(a => a.CoverLevelWording != null && a.CoverLevelWording.Any());
                if (coverwordinglevel != null)
                    if (coverwordinglevel.CoverLevelWording.Any()) model.CoverLevelWording = coverwordinglevel.CoverLevelWording;

            }

            new OptionsBuilder(definition, qdList, _repository).Build();

            return model;
        }

        public ProposalDefinitionQuestionsDto BuildPDefinitionQuestions(ProposalDefinition proposalDefinition)
        {
            var model = new ProposalDefinitionQuestionsDto();
            model.ProposalDefinition = Mapper.Map<ProposalDefinitionDto>(proposalDefinition);
            var qdList = new List<ProposalQuestionDefinitionDto>();

            foreach (var proposalQuestionAnswer in proposalDefinition.ProposalQuestionAnswers)
            {
                var answer = Mapper.Map<ProposalDefinitionAnswerDto>(proposalQuestionAnswer);
                var qdDto = Mapper.Map<ProposalQuestionDefinitionDto>(proposalQuestionAnswer.QuestionDefinition);
                qdDto.AnswerDto = answer;
                qdList.Add(qdDto);
            }

            new OptionsBuilder(proposalDefinition, qdList, _repository).Build();
            model.QuestionDefinitions = qdList;

            return model;
        }
    }
}