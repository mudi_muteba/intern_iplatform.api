﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Party.Contacts;
using Domain.Party.Relationships;
using Domain.Products;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;

namespace Domain.Party.ProposalDefinitions.Builders.OptionsBuilders
{
    internal class BuildByQuestionId : IOptionsBuilder
    {
        private readonly ProposalDefinition _proposalDefinition;
        private readonly List<ProposalQuestionDefinitionDto> _questionDefinitions;
        private readonly IRepository _repository;

        internal BuildByQuestionId(ProposalDefinition proposalDefinition,
            List<ProposalQuestionDefinitionDto> questionDefinitions, IRepository repository)
        {
            _proposalDefinition = proposalDefinition;
            _questionDefinitions = questionDefinitions;
            _repository = repository;
        }

        public bool Build()
        {
            List<ProposalQuestionDefinitionDto> yearOfManufacture =
                _questionDefinitions.Where(a => a.Question.Id == Questions.YearOfManufacture.Id).ToList();
            FindYearOfManufactureOptions(yearOfManufacture);

            List<ProposalQuestionDefinitionDto> idNumberQuestions =
                _questionDefinitions.Where(a => a.Question.Id ==
                                                Questions.MainDriverIDNumber.Id
                                                || a.Question.Id == Questions.RegisteredOwnerIDNumber.Id
                                                || a.Question.Id == Questions.IDNumber.Id)
                    .ToList();

            FindIdNumberQuestions(idNumberQuestions);

            // TODO: Uncomment when Individual Contacts has been added 
            //FindIdNumberQuestionsFromRelationships(idNumberQuestions);


            List<ProposalQuestionDefinitionDto> valueaddedproduct =
                _questionDefinitions.Where(a => a.Question.Id == Questions.Valueaddedproduct.Id).ToList();
            FindValueAddedProductOptions(valueaddedproduct);


            return true;
        }


        private void FindValueAddedProductOptions(IEnumerable<ProposalQuestionDefinitionDto> questionDefinitions)
        {
            Product product = _proposalDefinition.Product;

            if (product == null)
                return;

            foreach (ProposalQuestionDefinitionDto proposalQuestionDefinitionDto in questionDefinitions)
            {
                List<AnswerDefinition> listAnswerDefinition = _repository.GetAll<AnswerDefinition>()
                    .Where(a => a.Product.Id == product.Id).ToList();

                foreach (AnswerDefinition a in listAnswerDefinition)
                {
                    proposalQuestionDefinitionDto.Options.Add(new QuestionAnswerDto
                    {
                        Id = a.QuestionAnswer.Id.ToString(),
                        Name = a.QuestionAnswer.Name,
                        Answer = a.QuestionAnswer.Answer,
                        VisibleIndex = a.QuestionAnswer.VisibleIndex
                    });
                }
            }
        }


        private void FindYearOfManufactureOptions(IEnumerable<ProposalQuestionDefinitionDto> questionDefinitions)
        {
            int start = DateTime.UtcNow.Year - 35;
            foreach (ProposalQuestionDefinitionDto proposalQuestionDefinitionDto in questionDefinitions)
            {
                for (int i = DateTime.UtcNow.Year; i > start; i--)
                    proposalQuestionDefinitionDto.Options.Add(new QuestionAnswerDto
                    {
                        Id = i.ToString(),
                        Name = i.ToString(),
                        Answer = i.ToString(),
                        VisibleIndex = i
                    });
            }
        }


        private void FindIdNumberQuestions(IEnumerable<ProposalQuestionDefinitionDto> questionDefinitions)
        {
            var answers = new List<QuestionAnswerDto>();
            var policyHolder = _repository.GetById<Individual>(_proposalDefinition.Party.Id);

            if (policyHolder.IdentityNo.IsNullOrEmpty())
                return;

            //Add Policy Holder
            answers.Add(new QuestionAnswerDto()
            {
                Id = policyHolder.IdentityNo,
                Name = policyHolder.IdentityNo,
                Answer = GetIdDisplayValue(policyHolder),
                VisibleIndex = 0
            });

            //Get Contacts
            var contacts = _repository.GetAll<Relationship>()
                .Where(x => x.Party.Id == _proposalDefinition.Party.Id)
                .ToList();

            //Add Contacts
            answers.AddRange(contacts.Select(contact => contact.ChildParty).OfType<Contact>().Select(temp => new QuestionAnswerDto()
            {
                Id = temp.IdentityNo, Name = temp.IdentityNo, Answer = temp.FirstName + " " + temp.Surname, VisibleIndex = 0
            }));

            foreach (var proposalQuestionDefinitionDto in questionDefinitions)
            {
                proposalQuestionDefinitionDto.Options = answers;
            }
        }

        private void FindIdNumberQuestionsFromRelationships(
            IEnumerable<ProposalQuestionDefinitionDto> questionDefinitions)
        {
            IList<Relationship> relationships = (_proposalDefinition.Party as Individual).Relationships;


            foreach (ProposalQuestionDefinitionDto proposalQuestionDefinitionDto in questionDefinitions)
            {
                foreach (Relationship relationship in relationships)
                {
                    string partyIdentityNo = string.Empty;

                    if ((relationship.Party as Individual) != null)
                        partyIdentityNo = (relationship.Party as Individual).IdentityNo;


                    // TODO: Uncomment when Individual Contacts has been added 
                    //                    if (string.IsNullOrEmpty(partyIdentityNo) && (relationship.Party as Contact) != null)
                    //                        partyIdentityNo = (relationship.Party as Contact).IdentityNo;

                    if (string.IsNullOrEmpty(partyIdentityNo))
                        continue;

                    proposalQuestionDefinitionDto.Options.Add(new QuestionAnswerDto
                    {
                        Id = partyIdentityNo,
                        Name = partyIdentityNo,
                        Answer = partyIdentityNo,
                        VisibleIndex = 0
                    });
                }
            }
        }

        private string GetIdDisplayValue(Individual policyHolder)
        {
            return policyHolder.FirstName + " " + policyHolder.Surname;
        }
    }
}