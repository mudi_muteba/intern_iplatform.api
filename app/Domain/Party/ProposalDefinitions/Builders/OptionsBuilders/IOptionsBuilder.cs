﻿namespace Domain.Party.ProposalDefinitions.Builders.OptionsBuilders
{
    internal interface IOptionsBuilder
    {
        bool Build();
    }
}
