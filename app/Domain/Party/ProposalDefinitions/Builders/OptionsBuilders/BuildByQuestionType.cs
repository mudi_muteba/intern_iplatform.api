﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Repository;
using Domain.Party.Assets.AssetRiskItems;
using Domain.Party.Assets.AssetVehicles;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;

namespace Domain.Party.ProposalDefinitions.Builders.OptionsBuilders
{
    internal class BuildByQuestionType : IOptionsBuilder
    {
        private readonly ProposalDefinition _proposalDefinition;
        private readonly List<ProposalQuestionDefinitionDto> _questionDefinitions;

        internal BuildByQuestionType(ProposalDefinition proposalDefinition,
            List<ProposalQuestionDefinitionDto> questionDefinitions, IRepository repository)
        {
            _proposalDefinition = proposalDefinition;
            _questionDefinitions = questionDefinitions;
        }

        public bool Build()
        {
            List<ProposalQuestionDefinitionDto> addressDefinitions =
                _questionDefinitions.Where(a => a.Question.QuestionType == QuestionTypes.Address).ToList();
            FindAddressOptions(addressDefinitions);

            List<ProposalQuestionDefinitionDto> assetDefinitions =
                _questionDefinitions.Where(a => a.Question.QuestionType == QuestionTypes.Asset).ToList();
            FindAssetOptions(assetDefinitions);

            return true;
        }

        private void FindAddressOptions(IEnumerable<ProposalQuestionDefinitionDto> questionDefinitions)
        {
            if (questionDefinitions == null)
                return;

            List<QuestionAnswerDto> list =
                _proposalDefinition.Party.Addresses.Select(
                    a => new QuestionAnswerDto
                    {
                        Id = a.Id.ToString(),
                        Name = a.Description,
                        Answer = a.Description,
                    }).ToList();

            foreach (ProposalQuestionDefinitionDto definition in questionDefinitions)
                definition.Options = list;
        }

        private void FindAssetOptions(IEnumerable<ProposalQuestionDefinitionDto> questionDefinitions)
        {
            if (questionDefinitions != null)
            {

                var riskAssetQuestions = questionDefinitions.Where(a => a.GroupType == QuestionDefinitionGroupTypes.AllRiskAsset).ToList();
                var vehicleAssetQuestions = questionDefinitions.Where(a => a.GroupType == QuestionDefinitionGroupTypes.Vehicleasset).ToList();

                var assetRiskItems = _proposalDefinition.Party.Assets.Where(p => p.GetType().ToString() == typeof(AssetRiskItem).ToString()).ToList();
                var assetVehicleItems = _proposalDefinition.Party.Assets.Where(p => p.GetType().ToString() == typeof(AssetVehicle).ToString()).ToList();


                List<QuestionAnswerDto> riskOptions = assetRiskItems.Select(a => new QuestionAnswerDto
                {
                    Id = a.Id.ToString(),
                    Name = a.Description,
                    Answer = a.Description
                }).ToList();

                List<QuestionAnswerDto> vehilceOptions = assetVehicleItems.GroupBy(x => x.Description).Select(a => new QuestionAnswerDto
                {
                    Id = a.Last().Id.ToString(),
                    Name = a.Last().Description,
                    Answer = a.Last().Description
                }).ToList();

                foreach (var dto in vehicleAssetQuestions)
                    dto.Options = vehilceOptions;

                foreach (var dto in riskAssetQuestions)
                    dto.Options = riskOptions;
              
         }
        }
    }
}