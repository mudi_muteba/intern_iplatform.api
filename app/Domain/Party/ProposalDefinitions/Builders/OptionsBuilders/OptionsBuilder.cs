﻿using System.Collections.Generic;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;

namespace Domain.Party.ProposalDefinitions.Builders.OptionsBuilders
{
    public class OptionsBuilder
    {
        private readonly List<IOptionsBuilder> _builders;

        public OptionsBuilder(ProposalDefinition proposalDefinition,
            List<ProposalQuestionDefinitionDto> questionDefinitions, IRepository repository)
        {
            _builders = new List<IOptionsBuilder>();
            _builders.Add(new BuildByQuestionType(proposalDefinition, questionDefinitions, repository));
            _builders.Add(new BuildByQuestionId(proposalDefinition, questionDefinitions, repository));
        }

        public void Build()
        {
            foreach (IOptionsBuilder optionsBuilder in _builders)
                optionsBuilder.Build();
        }
    }
}