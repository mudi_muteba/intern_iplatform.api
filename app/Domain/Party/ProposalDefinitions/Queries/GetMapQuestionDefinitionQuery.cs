﻿
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Party.Note;
using Domain.Products;
using Domain.QuestionDefinitions;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Party.ProposalDefinitions.Queries
{
    /// <summary>
    /// This class is used to query question ddefinition and include any linked questions used for a multi quote
    /// </summary>
    public class GetMapQuestionDefinitionQuery : BaseQuery<QuestionDefinition>
    {
        private Product _product;
        private Cover _cover;

        public GetMapQuestionDefinitionQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultQuestionDefinitionFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProposalAuthorisation.List
                };
            }
        }


        public GetMapQuestionDefinitionQuery WithProduct(Product product)
        {
            _product = product;
            return this;
        }
        public GetMapQuestionDefinitionQuery WithCover(Cover cover)
        {
            _cover = cover;
            return this;
        }

        protected internal override IQueryable<QuestionDefinition> Execute(IQueryable<QuestionDefinition> query)
        {
            return query;
        }
    }
}