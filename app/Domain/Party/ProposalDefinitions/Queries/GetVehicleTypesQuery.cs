﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Party.ProposalDefinitions.Queries
{
    /// <summary>
    /// This class is used to query question ddefinition and include any linked questions used for a multi quote
    /// </summary>
    public class GetVehicleTypesQuery : BaseQuery<ProposalQuestionAnswer>
    {
        private ContainsVehicleTypesDto _dto;

        public GetVehicleTypesQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProposalQuestionAnswer>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.List
                };
            }
        }

        public GetVehicleTypesQuery WithCriteria(ContainsVehicleTypesDto dto)
        {
            _dto = dto;
            return this;
        }

        protected internal override IQueryable<ProposalQuestionAnswer> Execute(IQueryable<ProposalQuestionAnswer> query)
        {
            var proposalHeader = Repository.GetAll<ProposalHeader>().Where(a => a.Id == _dto.ProposalId);
            var proposalQuestionAnswers =
                Repository.GetAll<ProposalQuestionAnswer>().Where(a => a.ProposalDefinition.ProposalHeader == proposalHeader
                && a.ProposalDefinition.Party.Id == _dto.IndividualId
                && a.QuestionDefinition.Question == Questions.AIGMotorTypeOfVehicle
                && _dto.VehicleTypes.Contains(a.Answer)
                );

            return proposalQuestionAnswers;
        }
    }
}