﻿
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.ProposalDefinitions.Queries
{
    public class GetProposalDefinitionByIdQuery : BaseQuery<ProposalDefinition>
    {
        private int _id;

        public GetProposalDefinitionByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProposalDefinition>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.List
                };
            }
        }

        public GetProposalDefinitionByIdQuery WithId(int id)
        {
            _id = id;
            return this;
        }

        protected internal override IQueryable<ProposalDefinition> Execute(IQueryable<ProposalDefinition> query)
        {
            return Repository.GetAll<ProposalDefinition>().Where(a => a.Id == _id);
        }
    }
}