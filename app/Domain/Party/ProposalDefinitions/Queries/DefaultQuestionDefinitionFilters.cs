using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.QuestionDefinitions;

namespace Domain.Party.ProposalDefinitions.Queries
{
    public class DefaultQuestionDefinitionFilters : IApplyDefaultFilters<QuestionDefinition>
    {
        public IQueryable<QuestionDefinition> Apply(ExecutionContext executionContext, IQueryable<QuestionDefinition> query)
        {
            return query;

        }
    }
}