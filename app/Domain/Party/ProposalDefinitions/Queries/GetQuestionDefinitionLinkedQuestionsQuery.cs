﻿
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Party.Note;
using Domain.Products;
using Domain.QuestionDefinitions;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Party.ProposalDefinitions.Queries
{
    /// <summary>
    /// This class is used to query question ddefinition and include any linked questions used for a multi quote
    /// </summary>
    public class GetQuestionDefinitionLinkedQuestionsQuery : BaseQuery<QuestionDefinition>
    {
        private readonly IRepository _repository;
        private Product _product;
        private Cover _cover;

        public GetQuestionDefinitionLinkedQuestionsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultQuestionDefinitionFilters())
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProposalAuthorisation.List
                };
            }
        }


        public GetQuestionDefinitionLinkedQuestionsQuery WithProduct(Product product)
        {
            _product = product;
            return this;
        }
        public GetQuestionDefinitionLinkedQuestionsQuery WithCover(Cover cover)
        {
            _cover = cover;
            return this;
        }

        protected internal override IQueryable<QuestionDefinition> Execute(IQueryable<QuestionDefinition> query)
        {
            IQueryable<int?> linkedQuestionIds = _repository.GetAll<QuestionDefinition>().Where(a => a.CoverDefinition.Cover == _cover && a.CoverDefinition.Product == _product && a.LinkQuestionId != null).Select(a => a.LinkQuestionId).Distinct();
            return  _repository.GetAll<QuestionDefinition>().Where(
                a => (a.CoverDefinition.Cover == _cover
                    && a.CoverDefinition.Product == _product) || linkedQuestionIds.Contains(a.Id));
        }
    }
}