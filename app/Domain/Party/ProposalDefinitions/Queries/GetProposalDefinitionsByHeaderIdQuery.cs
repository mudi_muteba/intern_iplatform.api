﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.ProposalDefinitions.Queries
{
    public class GetProposalDefinitionsByHeaderIdQuery : BaseQuery<ProposalDefinition>
    {
        private int _headerId;

        public GetProposalDefinitionsByHeaderIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProposalDefinition>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.List
                };
            }
        }

        public GetProposalDefinitionsByHeaderIdQuery WithHeaderId(int id)
        {
            _headerId = id;
            return this;
        }

        protected internal override IQueryable<ProposalDefinition> Execute(IQueryable<ProposalDefinition> query)
        {
            return Repository.GetAll<ProposalDefinition>().Where(a => a.ProposalHeader.Id == _headerId);
        }
    }
}