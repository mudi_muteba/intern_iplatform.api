﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Common.Logging;
using Domain.Base;
using Domain.Party.Assets;
using Domain.Party.ProposalHeaders;
using Domain.Products;
using Domain.QuestionDefinitions;
using Domain.Users;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData;

namespace Domain.Party.ProposalDefinitions
{
    public class ProposalDefinition : EntityWithParty
    {
        // Just to keep Hibernate happy 
        private int _fakeTotalQuestions;
        private int _fakeTotalQuestionsAnswered;
        private static readonly ILog log = LogManager.GetLogger<ProposalDefinition>();

        public ProposalDefinition()
        {
            _fakeTotalQuestionsAnswered = 0;
            _fakeTotalQuestions = 0;
            ProposalQuestionAnswers = new List<ProposalQuestionAnswer>();
            Created = DateTime.UtcNow;
        }

        public virtual Asset Asset { get; set; }
        public virtual ProposalHeader ProposalHeader { get; set; }
        public virtual Cover Cover { get; set; }
        public virtual Product Product { get; set; }
        public virtual decimal SumInsured { get; set; }
        public virtual bool? IsVap { get; set; }
        public virtual DateTime? Created { get; set; }
        public virtual DateTime? Modified { get; set; }
        public virtual string Description { get; set; }
        public virtual User User { get; protected internal set; }

        public virtual IList<ProposalQuestionAnswer> ProposalQuestionAnswers { get; set; }

        public virtual void SetUser(User user)
        {
            User = user;
        }

        public virtual ProposalQuestionAnswer this[int id]
        {
            get
            {
                if (!ProposalQuestionAnswers.Any())
                    return null;

                var item = ProposalQuestionAnswers.FirstOrDefault(a => a.QuestionDefinition != null && a.QuestionDefinition.Question.Id == id);
                return item;
            }
        }
        public virtual int TotalQuestions
        {
            get { return ProposalQuestionAnswers.Count(); }
            set { _fakeTotalQuestions = value; }
        }

        public virtual int TotalQuestionsAnswered
        {
            get { return ProposalQuestionAnswers.Count(a => !string.IsNullOrEmpty(a.Answer)); }
            set { _fakeTotalQuestionsAnswered = value; }
        }

        public virtual void AddProposalQuestionAnswer(ProposalQuestionAnswer proposalQuestionAnswer)
        {
            proposalQuestionAnswer.ProposalDefinition = this;
            ProposalQuestionAnswers.Add(proposalQuestionAnswer);
        }

        public virtual void EditProposal(EditProposalDefinitionDto dto)
        {
            Description = dto.Description;
            Modified = DateTime.UtcNow;

            foreach (var answer in dto.Answers)
            {
                var entity = ProposalQuestionAnswers.FirstOrDefault(a => a.Id == answer.Id);
                if (entity != null)
                    entity.Answer = answer.Answer;
            }

            //TODO: resolve suminsured 
            SumInsured = GetSumInsured(dto);
        }

        private decimal GetSumInsured(EditProposalDefinitionDto dto)
        {
            var entity = ProposalQuestionAnswers.FirstOrDefault(x => x.QuestionDefinition.Question.Id == Questions.SumInsured.Id);
            var suminsured = "0";

            if(entity != null)
                suminsured = entity.Answer ?? suminsured;

            var numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.NumberDecimalSeparator = ".";

            return !string.IsNullOrWhiteSpace(suminsured) ? decimal.Parse(suminsured, numberFormatInfo) : 0;
        }

        public virtual void HasAnswers(IList<ProposalQuestionAnswer> answers)
        {
            foreach (var answer in answers)
            {
                answer.BelongsTo(this);

                ProposalQuestionAnswers.Add(answer);
            }
        }

        public virtual void Answer(AnswerQuestionDefinitionDto answer)
        {
            var matchingProposalQuestionAnswer = ProposalQuestionAnswers.FirstOrDefault(pqa => pqa.QuestionDefinition.Question.Id == answer.QuestionId);
            if (matchingProposalQuestionAnswer == null)
            {
                var question = new Questions().FirstOrDefault(q => q.Id == answer.QuestionId);
                if (question == null) // invalid question response
                    return;

                matchingProposalQuestionAnswer = new ProposalQuestionAnswer()
                {
                    ProposalDefinition = this,
                    QuestionDefinition = GetQuestionDefinition(question.Id),
                };

                if (matchingProposalQuestionAnswer.QuestionDefinition == null)
                    return;
            }

            matchingProposalQuestionAnswer.AnswerThis(answer);
        }

        private QuestionDefinition GetQuestionDefinition(int questionId)
        {
            var questionAnswer = this.ProposalQuestionAnswers
                .FirstOrDefault(pqa => pqa.QuestionDefinition.Matches(Cover.Id, questionId));

            if(questionAnswer == null)
            {
                log.WarnFormat("Failed to find a question definition for question with id {0} on proposal definition {1}"
                    , questionId
                    , Id);

                return null;
            }
            return questionAnswer.QuestionDefinition;
        }

        public virtual ProposalQuestionAnswer GetQuestionResponse(Question question)
        {
            return ProposalQuestionAnswers.FirstOrDefault(qa => qa.QuestionDefinition.Question.Id == question.Id);
        }
        public virtual bool IsQuestionAnswered(Question question)
        {
            var matchingQuestion = ProposalQuestionAnswers.FirstOrDefault(pqa => pqa.QuestionDefinition.Question.Id == question.Id);

            if (matchingQuestion == null)
                return false;

            return !string.IsNullOrWhiteSpace(matchingQuestion.Answer);
        }

        public virtual bool IsQuestionAnsweredWith(Question question, string value)
        {
            var matchingQuestion = ProposalQuestionAnswers.FirstOrDefault(pqa => pqa.QuestionDefinition.Question.Id == question.Id);
            if (matchingQuestion == null)
                return false;

            value = value ?? string.Empty;

            return value.Equals(matchingQuestion.Answer, StringComparison.InvariantCultureIgnoreCase);
        }

        public virtual void BelongsTo(ProposalHeader proposalHeader)
        {
            ProposalHeader = proposalHeader;
        }
    }
}