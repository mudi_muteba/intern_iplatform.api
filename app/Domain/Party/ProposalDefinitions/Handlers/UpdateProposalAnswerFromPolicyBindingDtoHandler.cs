﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;


namespace Domain.Party.ProposalDefinitions.Handlers
{
    public class UpdateProposalAnswerFromPolicyBindingDtoHandler : BaseDtoHandler<UpdateProposalAnswerFromPolicyBindingDto, bool>
    {
        private readonly IRepository m_Repository;
        private readonly IExecutionPlan m_ExecutionPlan;
        public UpdateProposalAnswerFromPolicyBindingDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            m_Repository = repository;
            m_ExecutionPlan = executionPlan;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(UpdateProposalAnswerFromPolicyBindingDto dto, HandlerResult<bool> result)
        {

            ProposalQuestionAnswer proposalQuestionAnswer = m_Repository.GetAll<ProposalQuestionAnswer>().FirstOrDefault(x => x.QuestionDefinition.Id == dto.QuestionDefinitionId && x.ProposalDefinition.Id == dto.ProposalDefinitionId);
            if(proposalQuestionAnswer != null)
            {
                proposalQuestionAnswer.Update(dto);
                m_Repository.Save(proposalQuestionAnswer);
            }

            result.Processed(true);
        }
    }
}

