﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.HistoryLoss.LossHistory;
using Domain.Individuals;
using Domain.Party.Addresses;
using Domain.Party.ProposalDefinitions.Builders.DefaultsBuilders;
using Domain.Party.ProposalDefinitions.Queries;
using Domain.Party.ProposalHeaders;
using Domain.Products;
using Domain.Products.Queries;
using Domain.QuestionDefinitions;
using Domain.SalesForce.Events;
using Domain.Users;
using FluentNHibernate.Utils;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData;
using MasterData.Authorisation;
using MoreLinq;
using NHibernate.Proxy;

namespace Domain.Party.ProposalDefinitions.Handlers
{
    public class CreateProposalDefinitionDtoHandler :
        AssociatedDtoHandler<ProposalHeader, CreateProposalDefinitionDto, int, ProposalDefinition>
    {
        private readonly IProvideContext m_ContextProvider;
        private readonly SystemChannels m_SystemChannels;
        private IRepository m_Repository { get; set; }
        private IExecutionPlan m_ExecutionPlan { get; set; }
        private List<ResponseErrorMessage> ResponseErrorMessage { get; set; }

        public CreateProposalDefinitionDtoHandler(IProvideContext contextProvider, IRepository repository, SystemChannels systemChannels, IExecutionPlan executionPlan)
            : base(contextProvider, repository)
        {
            m_ContextProvider = contextProvider;
            m_SystemChannels = systemChannels;
            m_Repository = repository;
            m_ExecutionPlan = executionPlan;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.Create
                };
            }
        }


        protected override void Processed(ProposalDefinition associatedEntity, HandlerResult<int> result)
        {
            result.Processed(associatedEntity.Id);
        }

        protected override ProposalDefinition Associate(ProposalHeader parentEntity, CreateProposalDefinitionDto dto,
            HandlerResult<int> result)
        {
            var product = GetProduct(dto.ProductId);

            var currentUser = m_Repository.GetAll<User>().First(a => a.Id == dto.Context.UserId);
            var proposalDefinition = parentEntity.AddProposalDefinition(dto, product, currentUser);

            AddAutomaticCovers(product, currentUser, parentEntity, dto);

            var defaultsBuilder = new DefaultsBuilder(m_SystemChannels, m_Repository, new DefaultsBuilderDto
            {
                NewDefinition = proposalDefinition,
                DefaultFromProposalId = dto.DefaultFromProposalId,
                ActiveChannelId = dto.ChannelId > 0 ? dto.ChannelId : m_ContextProvider.Get().ActiveChannelId
            });


            result.AddErrors(defaultsBuilder.Build());

            //raise events
            RaiseEvents(proposalDefinition);

            if (!result.AllErrorDTOMessages.Any())
            {
                return proposalDefinition;
            }

            proposalDefinition.IsDeleted = true;
            parentEntity.ProposalDefinitions.Remove(proposalDefinition);
            return null;

        }

        private Product GetProduct(int productId)
        {
            var product = m_Repository.GetById<Product>(productId);

            if (product == null)
            {
                throw new MissingEntityException(typeof(Product), productId);
            }
            return product;
        }

        private void RaiseEvents(ProposalDefinition proposalDefinition)
        {
            var source = string.Empty;
            if (m_ContextProvider.Get().RequestHeaders.ContainsKey("source"))
            {
                source = m_ContextProvider.Get().RequestHeaders["source"].FirstOrDefault();
            }


            if (source.Equals("aig-widget"))
            {
                var cmpid = proposalDefinition.ProposalHeader.CmpidSource;

                var individual =
                    m_Repository.GetAll<Individual>().FirstOrDefault(a => a.Id == proposalDefinition.Party.Id);

                proposalDefinition.RaiseEvent(new SalesForceEntryEvent("Risk Level", individual, cmpid, null, "", proposalDefinition.ProposalHeader.Id));
            }
        }

        protected override int GetParentId(CreateProposalDefinitionDto dto)
        {
            return dto.ProposalHeaderId;
        }

        /// <summary>
        /// Checks if a product has covers that should automatically be added to the proposal header.
        /// These automatic covers are either linked to the whole product or a specific cover on the product
        /// eg. Miway Motor Automatically Add personal Liability
        /// </summary>
        /// <param name="product">product to check for automatic products</param>
        /// <param name="currentUser">user creating the product</param>
        /// <param name="parentEntity">the proposal Definition</param>
        /// <param name="dto"></param>
        private void AddAutomaticCovers(Product product, User currentUser, ProposalHeader parentEntity, CreateProposalDefinitionDto dto)
        {
            try
            {
                var queryResult = m_ExecutionPlan
                    .Query<GetAutomaticCoversByProductIdQuery, ProductAutomaticCovers, List<ProductAutomaticCovers>>()
                    .Configure(q => q.WithProductId(product.Id))
                    .OnSuccess(r => r.ToList())
                    .Execute();

                var automaticProductcovers = queryResult.Response;
                foreach (var automaticProductCover in automaticProductcovers)
                {
                    //If automatic cover has not been added
                    if (parentEntity.HasDefinitionWithCover(automaticProductCover.CoverDefinition.Cover.Id))
                    { continue; }

                    //if cover is null add autocover to proposal OR if check if this specific cover requires autocover and add to proposal
                    if (automaticProductCover.Cover == null || automaticProductCover.Cover.Id == dto.CoverId)
                    {
                        var automaticProposalDefinition = dto.Clone() as CreateProposalDefinitionDto;
                        if (automaticProposalDefinition != null)
                        {
                            automaticProposalDefinition.CoverId = automaticProductCover.CoverDefinition.Cover.Id;
                            parentEntity.AddProposalDefinition(automaticProposalDefinition, GetProduct(automaticProductCover.Product.Id), currentUser);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.ErrorFormat("Error adding automatic covers {0}", e.Message);
            }
        }
    }
}