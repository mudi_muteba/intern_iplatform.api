﻿using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions.Builders;
using Domain.Party.ProposalDefinitions.Events;
using Domain.Users;

namespace Domain.Party.ProposalDefinitions.Handlers
{
    public class ProposalDefinitionUpdatedEventHandler : BaseEventHandler<ProposalDefinitionUpdatedEvent>
    {
        public ProposalDefinitionUpdatedEventHandler(IRepository repository, IProvideContext contextProvider)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        private IRepository _repository { get; set; }
        private IProvideContext _contextProvider { get; set; }


        public override void Handle(ProposalDefinitionUpdatedEvent @event)
        {
        }
    }
}