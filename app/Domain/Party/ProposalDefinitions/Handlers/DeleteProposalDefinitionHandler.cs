﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData.Authorisation;
using Workflow.Messages.Plan.Printers;

namespace Domain.Party.ProposalDefinitions.Handlers
{
    public class DeleteProposalDefinitionHandler :
        AssociatedDtoHandler<ProposalHeader, DeleteProposalDefinitionDto, DeleteProposalDefinitionResponseDto, ProposalHeader>
    {
        public DeleteProposalDefinitionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.Delete
                };
            }
        }

        protected override void Processed(ProposalHeader associatedEntity, HandlerResult<DeleteProposalDefinitionResponseDto> result)
        {
            if (associatedEntity.Party == null)
            Log.ErrorFormat("Party for Proposal Header is null");

            var response = new DeleteProposalDefinitionResponseDto()
            {
                PartyId = associatedEntity.Party.Id,
                ProposalHeaderId = associatedEntity.Id
            };

            result.Processed(response);
        }

        protected override ProposalHeader Associate(ProposalHeader parentEntity, DeleteProposalDefinitionDto dto,
            HandlerResult<DeleteProposalDefinitionResponseDto> result)
        {
            parentEntity.DeleteProposalDefinition(dto);

            return parentEntity;
        }

        protected override int GetParentId(DeleteProposalDefinitionDto dto)
        {
            return dto.ProposalHeaderId;
        }
    }
}