﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions.Builders;
using Domain.Party.ProposalDefinitions.Builders.AssetsBuilder;
using Domain.Party.ProposalDefinitions.Builders.MapQuestions;
using Domain.Party.ProposalDefinitions.Events;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData.Authorisation;
using NHibernate;

namespace Domain.Party.ProposalDefinitions.Handlers
{
    public class EditProposalDefinitionDtoHandler :
        ExistingEntityDtoHandler<ProposalDefinition, EditProposalDefinitionDto, ProposalDefinitionDto>
    {
        private readonly IRepository _repository;
        private readonly SystemChannels _systemChannels;
        private readonly ISession _session;

        public EditProposalDefinitionDtoHandler(IProvideContext contextProvider,
            IRepository repository,
             SystemChannels systemChannels,
            ISession session)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _systemChannels = systemChannels;
            _session = session;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.Edit
                };
            }
        }

        protected override ProposalDefinition GetEntityById(EditProposalDefinitionDto dto)
        {
            var proposalDefinition = repository.GetById<ProposalDefinition>(dto.Id);
            return proposalDefinition;
        }

        protected override void HandleExistingEntityChange(ProposalDefinition entity, EditProposalDefinitionDto dto,
            HandlerResult<ProposalDefinitionDto> result)
        {
            //update mapped answers
            entity.EditProposal(dto);

            var assetList = AssetHelper.GetAssetList(_repository, entity.ProposalHeader.Party.Id);

            if (dto.AssetExtras.Any())
                assetList.AssetExtras.AddRange(dto.AssetExtras);

            var asset = new AssetBuilder(_repository,_systemChannels).Build(entity, assetList, dto.Context);

            if (asset != null)
                entity.Description = asset.Description;

            //TODO update asset
            var response = new ProposalDefinitionBuilderDto(repository).Build(entity);

            result.Processed(response);

            entity.RaiseEvent(new ProposalDefinitionUpdatedEvent(entity, dto, dto.CreateAudit(), entity.Party.Channel));
        }
    }
}