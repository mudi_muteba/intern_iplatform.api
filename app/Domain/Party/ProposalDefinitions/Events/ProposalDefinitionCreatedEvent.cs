﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Party.Note;

namespace Domain.Party.ProposalDefinitions.Events
{
    public class ProposalDefinitionCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public ProposalDefinition ProposalDefinition { get; private set; }
        public int UserId { get; private set; }

        public ProposalDefinitionCreatedEvent(ProposalDefinition _ProposalDefinition, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> { channelReferences })
        {
            ProposalDefinition = _ProposalDefinition;
        }
    }
}