﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Party.Note;
using iPlatform.Api.DTOs.Party.ProposalDefinition;

namespace Domain.Party.ProposalDefinitions.Events
{
    public class ProposalDefinitionUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public ProposalDefinition ProposalDefinition { get; private set; }
        public EditProposalDefinitionDto EditProposalDefinitionDto { get; private set; }
        public int UserId { get; private set; }

        public ProposalDefinitionUpdatedEvent(ProposalDefinition proposalDefinition, EditProposalDefinitionDto dto, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> { channelReferences })
        {
            ProposalDefinition = proposalDefinition;
            EditProposalDefinitionDto = dto;
        }
    }
}