﻿using System;
using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.AdditionalMembers;
using MasterData;

namespace Domain.Party.ProposalDefinitions
{
    public class AdditionalMembers : Entity
    {
        public AdditionalMembers()
        {

        }

        public virtual MemberRelationship MemberRelationship { get; set; }
        public virtual string Initials { get; set; }
        public virtual string Surname { get; set; }
        public virtual string IdNumber { get; set; }
        public virtual bool IsStudent { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual DateTime DateOnCover { get; set; }
        public virtual string SumInsured { get; set; }
        public virtual ProposalDefinition ProposalDefinition { get; set; }
        public virtual DateTime DateOfBirth { get; set; }


        public static AdditionalMembers Create(CreateAdditionalMemberDto dto)
        {
            var additionalMember = Mapper.Map<AdditionalMembers>(dto);

            return additionalMember;
        }

        public virtual void Update(EditAdditionalMemberDto editAdditionalMemberDto)
        {
            Mapper.Map(editAdditionalMemberDto, this);

        }

        public virtual void Disable()
        {
            this.Delete();
        }

    }
}
