﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Payments;
using MasterData.Authorisation;
using NHibernate;

namespace Domain.Party.Payments.Handlers
{
    public class CreatePaymentDetailsDtoHandler : CreationDtoHandler<PaymentDetails, CreatePaymentDetailsDto, int>
    {
        private readonly ISession _session;

        public CreatePaymentDetailsDtoHandler(IProvideContext contextProvider, IRepository repository, ISession session)
            : base(contextProvider, repository)
        {
            _session = session;
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PaymentDetailsAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(PaymentDetails entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override PaymentDetails HandleCreation(CreatePaymentDetailsDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Party>(dto.PartyId);
            var paymentDetail = party.AddPaymentDetail(dto);

            result.Processed(paymentDetail.Id);
            _repository.Save(paymentDetail);
            _session.Flush();
            return paymentDetail;
        }
    }
}