﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Bank;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.Payments;

namespace Domain.Party.Payments.Handlers
{
    public class UpdatePaymentDetailsHandler : ExistingEntityDtoHandler<Party, EditPaymentDetailsDto, int>
    {
        public UpdatePaymentDetailsHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PaymentDetailsAuthorisation.Edit
                };
            }
        }

        protected override Party GetEntityById(EditPaymentDetailsDto dto)
        {
            return repository.GetById<Party>(dto.PartyId);
        }

        protected override void HandleExistingEntityChange(Party entity, EditPaymentDetailsDto dto,
            HandlerResult<int> result)
        {
            entity.UpdatePaymentDetail(dto);
        }
    }
}