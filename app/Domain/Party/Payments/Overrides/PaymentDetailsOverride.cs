﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;
using NHibernate.Properties;

namespace Domain.Party.Payments.Overrides
{
    public class PaymentDetailsOverride : IAutoMappingOverride<PaymentDetails>
    {
        public void Override(AutoMapping<PaymentDetails> mapping)
        {
            mapping.References(x => x.BankDetails).Nullable();
            mapping.References(x => x.Party).Cascade.None();
            mapping.References(x => x.Quote).Cascade.None();
        }
    }
}