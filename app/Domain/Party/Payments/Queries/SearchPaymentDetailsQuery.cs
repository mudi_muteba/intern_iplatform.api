﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Banks;
using MasterData.Authorisation;
using Domain.Party.Payments;
using iPlatform.Api.DTOs.Party.Payments;

namespace Domain.Party.Payments.Queries
{
    public class SearchPaymentDetailsQuery : BaseQuery<PaymentDetails>, ISearchQuery<PaymentDetailSearchDto>
    {
        private PaymentDetailSearchDto criteria;

        public SearchPaymentDetailsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PaymentDetails>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    PaymentDetailsAuthorisation.List
                };
            }
        }

        public void WithCriteria(PaymentDetailSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<PaymentDetails> Execute(IQueryable<PaymentDetails> query)
        {
            if (criteria.PartyId.HasValue)
                query = query.Where(a => a.Party.Id == criteria.PartyId);

            return query;
        }
    }
}