﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Banks;
using MasterData.Authorisation;


namespace Domain.Party.Payments.Queries
{
    public class GetPaymentDetailByIdQuery : BaseQuery<PaymentDetails>
    {
        private int partyId;
        private int id;

        public GetPaymentDetailByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PaymentDetails>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    PaymentDetailsAuthorisation.List
                };
            }
        }

        public void WithId(int id)
        {
            this.id = id;
        }

        public void WithPartyId(int partyId)
        {
            this.partyId = partyId;
        }

        protected internal override IQueryable<PaymentDetails> Execute(IQueryable<PaymentDetails> query)
        {
            return query.Where(a => a.Party.Id == partyId && a.Id == id);
        }
    }
}