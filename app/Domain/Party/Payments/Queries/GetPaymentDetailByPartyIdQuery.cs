﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Payments.Queries
{
    public class GetPaymentDetailByPartyIdQuery : BaseQuery<PaymentDetails>
    {
        private int _partyId;

        public GetPaymentDetailByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PaymentDetails>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    PaymentDetailsAuthorisation.List
                };
            }
        }

        public GetPaymentDetailByPartyIdQuery WithPartyId(int id)
        {
            this._partyId = id;
            return this;
        }

        protected internal override IQueryable<PaymentDetails> Execute(IQueryable<PaymentDetails> query)
        {
            return query.Where(a => a.Party.Id == _partyId);
        }
    }
}