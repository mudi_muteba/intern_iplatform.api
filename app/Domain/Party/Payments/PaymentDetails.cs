﻿using AutoMapper;
using Domain.Base;
using Domain.Party.Bank;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.Party.Payments;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Party.Payments
{
    public class PaymentDetails : Entity, IEntityIncludeParty
    {
        public virtual BankDetails BankDetails { get; set; }
        public virtual DateTime? DebitOrderDate { get; set; }
        public virtual Quote Quote { get; set; }
        public virtual DateTime InceptionDate { get; set; }
        public virtual MethodOfPayment MethodOfPayment { get; set; }
        public virtual PaymentPlan PaymentPlan { get; set; }
        public virtual Party Party { get; set; }
        public virtual DateTime TermEndDate { get; set; }
        public virtual DateTime? TakeOnStrikeDate { get; set; }
        public virtual void Update(EditPaymentDetailsDto dto)
        {
            Mapper.Map(dto, this);
        }
    }
}
