﻿
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using System;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Payments;
using System.Collections.Generic;
using Domain.Party.Quotes;
using Domain.Party.Bank;


namespace Domain.Party.Payments.Mappings
{
    public class PaymentDetailsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PaymentDetails, PaymentDetailsDto>();
            Mapper.CreateMap<PagedResults<PaymentDetails>, PagedResultDto<PaymentDetailsDto>>();

            Mapper.CreateMap<List<PaymentDetails>, ListResultDto<PaymentDetailsDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<CreatePaymentDetailsDto, PaymentDetails>()
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.MapFrom(s => new Quote { Id = s.QuoteId }))
                .ForMember(t => t.BankDetails, o => o.MapFrom(s => s.BankDetailsId == 0 ? null : new BankDetails { Id = s.BankDetailsId }))
                ;

            Mapper.CreateMap<EditPaymentDetailsDto, PaymentDetails>()
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.MapFrom(s => new Quote { Id = s.QuoteId }))
                .ForMember(t => t.BankDetails, o => o.MapFrom(s => s.BankDetailsId == 0 ? null : new BankDetails { Id = s.BankDetailsId }))
                ;
        }
    }
}