﻿using Domain.Base;

namespace Domain.Party.ContactDetails
{
    public class ContactDetail : Entity
    {
        public virtual string Work { get; set; }

        public virtual string Direct { get; set; }

        public virtual string Cell { get; set; }

        public virtual string Home { get; set; }

        public virtual string Fax { get; set; }

        public virtual string Email { get; set; }

        public virtual string Website { get; set; }

        public virtual string Facebook { get; set; }

        public virtual string Twitter { get; set; }

        public virtual string LinkedIn { get; set; }

        public virtual string Skype { get; set; }
    }
}