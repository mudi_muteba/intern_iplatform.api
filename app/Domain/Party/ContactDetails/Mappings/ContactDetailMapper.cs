﻿using AutoMapper;
using iPlatform.Api.DTOs.Party.ContactDetail;

namespace Domain.Party.ContactDetails.Mappings
{
    public class ContactDetailMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateContactDetailDto, ContactDetail>()
                .ForMember(t => t.Id, o => o.Ignore());

            Mapper.CreateMap<ContactDetail, ContactDetailDto>();
        }
    }
}