﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Campaigns.Queries;
using Domain.Leads;
using Domain.Users;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Party.ProposalHeaders.Handlers
{
    public class CreateProposalHeaderDtoHandler : CreationDtoHandler<ProposalHeader, CreateProposalHeaderDto, int>
    {
        private IProvideContext _contextProvider;

        public CreateProposalHeaderDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(ProposalHeader entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ProposalHeader HandleCreation(CreateProposalHeaderDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Party>(dto.PartyId);
            if (party == null)
                throw new MissingEntityException(typeof(Party), dto.Id);

            var user = _repository.GetById<User>(dto.Context.UserId);

            var campaignRef = dto.CampaignId > 0
                ? _repository.GetAll<CampaignReference>().FirstOrDefault(x => x.Campaign.Id == dto.CampaignId)
                : null;

            campaignRef = campaignRef ??
                          new CampaignReference(_repository.GetById<Campaign>(dto.CampaignId), string.Empty);

            var proposalHeader = party.AddProposalHeader(dto, user, campaignRef);

            var lead = _repository.GetAll<Lead>().FirstOrDefault(x => x.Party.Id == dto.PartyId && x.IsDeleted == false);
            _repository.Save(lead);


            result.Processed(proposalHeader.Id);
            return proposalHeader;
        }
    }
}