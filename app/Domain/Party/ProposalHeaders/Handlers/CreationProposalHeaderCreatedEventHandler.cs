﻿using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.ProposalHeaders.Events;
using Domain.Users;

namespace Domain.Party.ProposalHeaders.Handlers
{
    public class CreationProposalHeaderCreatedEventHandler : BaseEventHandler<ProposalHeaderCreatedEvent>
    {
        public CreationProposalHeaderCreatedEventHandler(IRepository repository, IProvideContext contextProvider)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        private IRepository _repository { get; set; }
        private IProvideContext _contextProvider { get; set; }


        public override void Handle(ProposalHeaderCreatedEvent @event)
        {
        }
    }
}