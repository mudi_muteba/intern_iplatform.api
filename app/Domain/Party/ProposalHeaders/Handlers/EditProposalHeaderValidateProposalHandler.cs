﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using MasterData.Authorisation;

namespace Domain.Party.ProposalHeaders.Handlers
{
    public class EditProposalHeaderShouldRequoteHandler : ExistingEntityDtoHandler<ProposalHeader, EditProposalHeaderShouldQuoteDto, int>
    {
        public EditProposalHeaderShouldRequoteHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(ProposalHeader entity, EditProposalHeaderShouldQuoteDto dto, HandlerResult<int> result)
        {
            var obj = repository.GetById<ProposalHeader>(dto.Id);
            obj.ShouldReQuote = dto.ShouldReQuote;

            entity.Update(obj);
            result.Processed(entity.Id);
        }
    }
}
