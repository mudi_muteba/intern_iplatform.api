﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using MasterData.Authorisation;
using MasterData;

namespace Domain.Party.ProposalHeaders.Handlers
{
    public class DeleteProposalHeaderHandler : ExistingEntityDtoHandler<ProposalHeader, DeleteProposalHeaderDto, int>
    {
        public DeleteProposalHeaderHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.Delete
                };
            }
        }


        protected override void HandleExistingEntityChange(ProposalHeader entity, DeleteProposalHeaderDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Party>(dto.PartyId);

            if (party == null)
                throw new MissingEntityException(typeof(Party), dto.Id);

            party.DeleteProposalHeader(dto);
            result.Processed(dto.Id);
        }
    }
}