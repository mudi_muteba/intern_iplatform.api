﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using Domain.Base.Repository;

namespace Domain.Party.ProposalHeaders.Mappings
{
    public class ProposalHeaderMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<List<ListProposalHeaderDto>, ListResultDto<ListProposalHeaderDto>>();

            Mapper.CreateMap<List<ProposalHeader>, ListResultDto<ListProposalHeaderDto>>()
                .ForMember(t => t.Results, o=> o.MapFrom(s => s));

            Mapper.CreateMap<CreateProposalHeaderDto, ProposalHeader>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore());

            Mapper.CreateMap<ProposalHeader, ListProposalHeaderDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                .ForMember(t => t.AgentId, o => o.MapFrom(s => s.LeadActivity.User.Id))
                .ForMember(t => t.AgentName,
                    o => o.MapFrom(
                            s =>
                                s.LeadActivity.User.Individuals.Any()
                                    ? s.LeadActivity.User.Individuals.FirstOrDefault().DisplayName
                                    : s.LeadActivity.User.UserName))

                ;

            Mapper.CreateMap<ProposalHeader, ProposalHeaderDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                .ForMember(t => t.AgentId, o => o.MapFrom(s => s.LeadActivity.User.Id))
                .ForMember(t => t.AgentName,
                    o => o.MapFrom(
                            s =>
                                s.LeadActivity.User.Individuals.Any()
                                    ? s.LeadActivity.User.Individuals.FirstOrDefault().DisplayName
                                    : s.LeadActivity.User.UserName))
              ;

            Mapper.CreateMap<ProposalHeader, ProposalHeaderDetailedDto>()
                .ForMember(t => t.ProposalDefinitions, o => o.MapFrom(s => s.ProposalDefinitions.Where(x => x.IsDeleted == false)))
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                .ForMember(t => t.AgentId, o => o.MapFrom(s => s.LeadActivity.User.Id))
                .ForMember(t => t.AgentName,
                    o => o.MapFrom(
                            s =>
                                s.LeadActivity.User.Individuals.Any()
                                    ? s.LeadActivity.User.Individuals.FirstOrDefault().DisplayName
                                    : s.LeadActivity.User.UserName))
              ;

        }
    }
}