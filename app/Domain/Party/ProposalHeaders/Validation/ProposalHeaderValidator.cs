﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Party.ProposalHeaders;

namespace Domain.Party.ProposalHeaders.Validation
{
    public class ProposalHeaderValidator : IValidateDto<CreateProposalHeaderDto>, IValidateDto<EditProposalHeaderDto>
    {
        private readonly IRepository _repository;
        public ProposalHeaderValidator(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
        }

        public void Validate(CreateProposalHeaderDto dto, ExecutionResult result)
        {
        }

        public void Validate(EditProposalHeaderDto dto, ExecutionResult result)
        {
        }
    }
}