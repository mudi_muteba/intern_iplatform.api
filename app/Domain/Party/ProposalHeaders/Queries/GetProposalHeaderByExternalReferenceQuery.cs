using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.ProposalHeaders.Queries
{
    public class GetProposalHeaderByExternalReferenceQuery : BaseQuery<ProposalHeader>
    {
        private string _externalReference;

        public GetProposalHeaderByExternalReferenceQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProposalHeader>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.List
                };
            }
        }

        public GetProposalHeaderByExternalReferenceQuery WithExternalReference(string externalReference)
        {
            _externalReference = externalReference;
            return this;
        }

        protected internal override IQueryable<ProposalHeader> Execute(IQueryable<ProposalHeader> query)
        {
            return Repository.GetAll<ProposalHeader>().Where(ph => ph.ExternalReference == _externalReference);
        }
    }
}