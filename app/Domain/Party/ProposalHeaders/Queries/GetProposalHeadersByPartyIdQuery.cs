﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Party.Note;
using MasterData.Authorisation;
using Domain.Party.Quotes;
using Domain.Party.Quotes.Queries;

namespace Domain.Party.ProposalHeaders.Queries
{
    public class GetProposalHeadersByPartyIdQuery : BaseQuery<ProposalHeader>
    {
        private int _partyId;

        public GetProposalHeadersByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProposalHeader>())
        {
            this.repository = repository;
        }

        private readonly IRepository repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProposalAuthorisation.List
                };
            }
        }

        public GetProposalHeadersByPartyIdQuery WithPartyId(int id)
        {
            this._partyId = id;
            return this;
        }

        protected internal override IQueryable<ProposalHeader> Execute(IQueryable<ProposalHeader> query)
        {
           var results =  repository.GetAll<ProposalHeader>().Where(a => a.Party.Id == _partyId);
            return results;
        }
    }
}