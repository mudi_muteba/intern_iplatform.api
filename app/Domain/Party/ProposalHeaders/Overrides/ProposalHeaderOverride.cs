﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.ProposalHeaders.Overrides
{
    public class ProposalHeaderOverride : IAutoMappingOverride<ProposalHeader>
    {
        public void Override(AutoMapping<ProposalHeader> mapping)
        {
            mapping.HasMany(x => x.ProposalDefinitions).Cascade.SaveUpdate();
            mapping.References(x => x.LeadActivity).Cascade.SaveUpdate();
        }
    }
}