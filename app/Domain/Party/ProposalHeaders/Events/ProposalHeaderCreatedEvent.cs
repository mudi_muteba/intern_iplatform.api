﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Party.Note;

namespace Domain.Party.ProposalHeaders.Events
{
    public class ProposalHeaderCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public ProposalHeader ProposalHeader { get; private set; }
        public int UserId { get; private set; }

        public ProposalHeaderCreatedEvent(ProposalHeader _ProposalHeader, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> { channelReferences })
        {
            ProposalHeader = _ProposalHeader;
        }
    }
}