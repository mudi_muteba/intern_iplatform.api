﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.Logging;
using Domain.Activities;
using Domain.Base;
using Domain.Base.Events;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Extentions;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Imports.Leads.Dtos;
using Domain.Imports.Leads.Events;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalDefinitions.Events;
using Domain.Party.Quotes;
using Domain.Products;
using Domain.Users;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;

namespace Domain.Party.ProposalHeaders
{
    public class ProposalHeader : EntityWithParty
    {
        protected readonly ILog log = LogManager.GetLogger(typeof(ProposalHeader));

        public ProposalHeader()
        {
            ProposalDefinitions = new List<ProposalDefinition>();
            ShouldReQuote = false;
            ValidateProposals = false;
        }

        public ProposalHeader(string description)
        {
            ProposalDefinitions = new List<ProposalDefinition>();
            RatingDate = DateTime.UtcNow;
            CreatedDate = DateTime.UtcNow;
            Description = description;
        }

        public virtual string Description { get; protected internal set; }
        public virtual ProposalLeadActivity LeadActivity { get; protected internal set; }
        public virtual IList<ProposalDefinition> ProposalDefinitions { get; protected internal set; }
        public virtual DateTime? CreatedDate { get; protected internal set; }
        public virtual DateTime? ModifiedDate { get; set; }
        public virtual DateTime? RatingDate { get; set; }
        public virtual string ExternalReference { get; protected internal set; }
        public virtual bool IsQuoted { get; protected internal set; }
        public virtual bool IsIntentToBuy { get; set; }
        //AIG rules
        public virtual bool ShouldReQuote { get; set; }
        public virtual bool ValidateProposals { get; set; }

        //AIG rules
        public virtual string ExternalClientReference { get; set; }
        public virtual string Source { get; set; }
        public virtual string CmpidSource { get; set; }


        public virtual ProposalLeadActivity Created(User user, Party party, CampaignReference campaignReference = null)
        {
            CreatedDate = DateTime.UtcNow;

            Campaign campaign = null;
            ;
            if (campaignReference != null)
                campaign = campaignReference.Campaign;

            var activity = new ProposalLeadActivity(user, party, this, campaign);
            if (activity.Lead != null && party != null)
            {
                RaiseEvent(new UpdateIndividualUploadDetailEvent(new IndividualUploadDetailEventDto()
                {
                    LeadImportStatus = new LeadImportStatuses().FirstOrDefault(a => a.Name.ToLower().Equals("proposal")),
                    LeadImportReference = activity.Lead.ExternalReference.ToGuid(),
                    Comment = "Proposal",
                    PartyId = Party.Id
                }));
            }
            if (campaignReference != null)
                activity.AllocateToCampaign(campaignReference.Campaign, campaignReference.Reference);

            LeadActivity = activity;
            return activity;
        }

        public virtual void MarkAsQuoted()
        {
            IsQuoted = true;
        }

        public virtual ProposalDefinition AddProposalDefinition(CreateProposalDefinitionDto dto, Product product,
            User currentUser)
        {
            var proposalDefinition = Mapper.Map<ProposalDefinition>(dto);
            proposalDefinition.Product = product;
            proposalDefinition.SetUser(currentUser);

            if (Party == null)
                log.ErrorFormat("PROPOSAL HEADER Party is NULL, unable to add proposal definition for header {0}",
                    this.Id);

            proposalDefinition.Party = Party;

            if (proposalDefinition.ProposalHeader == null)
                proposalDefinition.ProposalHeader = this;

            ProposalDefinitions.Add(proposalDefinition);

            return proposalDefinition;
        }

        public virtual decimal VoluntaryExcess(int assetNumber)
        {
            var match = GetQuestionResponse(assetNumber, Questions.VoluntaryExcess);
            int questionId;
            if (match == null || !int.TryParse(match.Answer, out questionId)) return 0.0m;
            var number = 0.00m;
            var result = new QuestionAnswers().FirstOrDefault(x => x.Id == questionId);
            if (result == null) return number;
            decimal.TryParse(result.Answer, out number);
            return number;
        }

        public virtual ProposalQuestionAnswer GetQuestionResponse(int assetNumber, Question question)
        {
            var match = ProposalDefinitions.FirstOrDefault(d => d.Id == assetNumber);

            return match == null ? null : match.GetQuestionResponse(question);
        }

        public virtual ProposalDefinition DeleteProposalDefinition(DeleteProposalDefinitionDto dto)
        {
            ProposalDefinition entity = ProposalDefinitions.FirstOrDefault(a => a.Id == dto.Id);
            if (entity == null)
                throw new MissingEntityException(typeof (ProposalDefinition), dto.Id);
            entity.IsDeleted = true;

            return entity;
        }

        public virtual QuoteHeader CreateQuote(RatingResultDto ratingResultDto, List<Product> allocatedProducts,
            IRepository repository)
        {
            var header = QuoteHeader.Create(this, ratingResultDto);
            var reader = new RatingResultDtoQuoteReader(ratingResultDto);
            this.MarkAsQuoted();
            var coverdefinitions = repository.GetAll<CoverDefinition>().ToList();
            var policies = reader.GetValidPolicies(allocatedProducts);
            foreach (var policy in policies)
            {
                var currentProduct = allocatedProducts.FirstOrDefault(p => p.ProductCode == policy.ProductCode);
                var quote = header.CreateQuote(currentProduct, policy);
                var items = reader.GetValidItems(currentProduct, policy.Items, coverdefinitions);
                foreach (var item in items)
                {
                    quote.CreateItem(this, item.Item2, item.Item1);
                }
            }

            return header;
        }

        public virtual void HasDefinitions(List<ProposalDefinition> definitions)
        {
            foreach (var definition in definitions)
            {
                ProposalDefinitions.Add(definition);
                definition.BelongsTo(this);
            }
        }

        public virtual bool HasDefinitionWithCover(int coverId)
        {
           return ProposalDefinitions.Any(x => x.Cover.Id == coverId);
        }

        public virtual void Update(ProposalHeader dto)
        {
            Mapper.Map(dto, this);
        }
    }
}
