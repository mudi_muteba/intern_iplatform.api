using System.Collections.Generic;
using System.Linq;
using Domain.Leads;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Request;
using Shared;

namespace Domain.Party.ProposalHeaders
{
    public class ProposalDtoBuilder
    {
        private RatingRequestDto request;

        public ProposalDtoBuilder For(RatingRequestDto ratingRequest)
        {
            this.request = ratingRequest;
            return this;
        }

        public CreateProposalHeaderDto CreateProposalHeaderDto(int partyId)
        {
            return new ProposalHeaderBuilder().WithParty(partyId).Create(request);
        }

        public CreateProposalDefinitionDto CreateProposalDefinitionDto(int productId, int proposalHeaderId, RatingRequestItemDto item)
        {
            return new CreateProposalDefinitionDto()
            {
                CoverId = item.Cover.Id,
                ProductId = productId,
                ProposalHeaderId = proposalHeaderId
            };
        }
    }

    public class ProposalHeaderBuilder
    {
        private int partyId;

        public ProposalHeaderBuilder WithParty(int partyId)
        {
            this.partyId = partyId;
            return this;
        }

        public CreateProposalHeaderDto Create(RatingRequestDto request)
        {
            var dto = new CreateProposalHeaderDto()
            {
                CreatedDate = SystemTime.Now(),
                PartyId = partyId,
                RatingDate = SystemTime.Now(),
                ModifiedDate = SystemTime.Now(),
                ExternalReference = request.Id.ToString()
            };

            dto.SetContext(request.Context);

            return dto;
        }
    }
}