﻿using iPlatform.Api.DTOs.Ratings.Request;
using Domain.Leads;
using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Assets;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalDefinitions.Builders.AssetsBuilder;
using Newtonsoft.Json;
using Domain.QuestionDefinitions;
using Domain.Products;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using MasterData;

namespace Domain.Party.ProposalHeaders.Builders
{
    public class ProposalBuilder
    {
        private readonly ILog log = LogManager.GetLogger<ProposalBuilder>();
        private readonly IRepository _repository;
        private readonly IExecutionPlan _executionPlan;
        private readonly SystemChannels _systemChannels;

        public ProposalBuilder(IRepository repository, IExecutionPlan executionPlan, SystemChannels systemChannels)
        {
            _repository = repository;
            _executionPlan = executionPlan;
            _systemChannels = systemChannels;
        }

        public ProposalHeader ImportMultiQuoteProposal(RatingRequestDto request, Lead lead)
        {
            if (request == null || lead == null)
            {
                throw new Exception("Either request or lead is null. Can not create proposal");
            }

            if (!request.Policy.Persons.Any())
            {
                log.WarnFormat("Can not create proposal since there are no people associated with the request.");
                return null;
            }

            if (!request.Items.Any())
            {
                log.WarnFormat("Can not create proposal since no items have been associated with the request.");
                return null;
            }

            
            //Create ProposalHeader
            var createProposalHeaderDto = new ProposalDtoBuilder().For(request).CreateProposalHeaderDto(lead.Party.Id);
            var header = _executionPlan.Execute<CreateProposalHeaderDto, ProposalHeader>(createProposalHeaderDto).Response;

            //Build ProposalDefintions
            var definitions = BuildProposalDefinitions(request, header, lead);

            header.HasDefinitions(definitions);

            _repository.Save(header);

            return header;
        }



        private List<ProposalDefinition> BuildProposalDefinitions(RatingRequestDto request, ProposalHeader header, Lead lead)
        {
            var definitions = new List<ProposalDefinition>();

            Product multiQuoteProduct = _repository.GetById<Product>(Product.MultiQuoteProductId);

            List<QuestionDefinition> existingQuestionDefinitions = _repository.GetAll<QuestionDefinition>()
                .Where(x => x.CoverDefinition.Cover != null)
                .ToList();


            var assetNoCounter = 0;
            foreach (RatingRequestItemDto item in request.Items)
            {
                if (item.AssetNo == 0)
                    item.AssetNo = assetNoCounter++;

                string currentProductCode = string.IsNullOrEmpty(item.ProductCode)
                    ? multiQuoteProduct.ProductCode
                    : item.ProductCode;

                Product product = string.IsNullOrEmpty(item.ProductCode)
                    ? multiQuoteProduct
                    : _repository.GetAll<Product>().FirstOrDefault(x => x.ProductCode == currentProductCode);


                if (string.IsNullOrEmpty(item.ProductCode))
                {
                    log.ErrorFormat(
                        "No product found for item. The product code was '{0}'. Using multiquote product '{1}'",
                        item.ProductCode, currentProductCode);
                }

                RatingRequestItemDto item1 = item;
                string currentCode = currentProductCode;

                IEnumerable<ProposalQuestionAnswer> answers = item.Answers.Select(qa =>
                {
                    string answer = qa.QuestionAnswer == null ? string.Empty : qa.QuestionAnswer.ToString();

                    if (qa.Question.QuestionType.Id == QuestionTypes.Dropdown.Id)
                    {
                        var questionAnswer = qa.QuestionAnswer as QuestionAnswer;

                        if (questionAnswer == null)
                        {
                            try
                            {
                                questionAnswer =
                                    JsonConvert.DeserializeObject<QuestionAnswer>(qa.QuestionAnswer.ToString());
                            }
                            catch (Exception)
                            {

                            }
                        }

                        answer = questionAnswer == null ? string.Empty : questionAnswer.Id.ToString();

                    }
                    QuestionDefinition matchingQuestionDefinition = existingQuestionDefinitions
                       .FirstOrDefault(q => q.CoverDefinition.Cover.Name == item1.Cover.Name &&
                                            q.Question.Id == qa.Question.Id && q.CoverDefinition.Product.ProductCode == currentProductCode);

                    if (matchingQuestionDefinition == null)
                    {
                        log.WarnFormat(
                            "No question definition found for question '{0} ({1})' on cover '{2}' for product '{3}'",
                            qa.Question.Name,
                            qa.Question.Id,
                            item1.Cover.Name,
                            currentCode);
                    }

                    return new ProposalQuestionAnswer
                    {
                        Answer = answer,
                        QuestionType = qa.Question.QuestionType,
                        QuestionDefinition = matchingQuestionDefinition
                    };

                }).Where(a => a.QuestionDefinition != null);

                var proposalDefinition = new ProposalDefinition
                {
                    Cover = item.Cover,
                    Product = product,
                    Party = lead.Party,
                    ProposalHeader = header
                };

                proposalDefinition.HasAnswers(answers.ToList());

                var assetList = new AssetList();

                List<Asset> assets = lead.Party.Assets.ToList();
                assetList.AddRange(assets);

                //Build quote item
                var asset = new AssetBuilder(_repository,_systemChannels).Build(proposalDefinition, assetList, request.Context);
    
                definitions.Add(proposalDefinition);

                if (item.Cover.Id == Covers.FuneralCostsAddDeathBenefits.Id)
                {
                    //TODO Save Funeral members

                }
            }
            return definitions;
        }
    }
}
