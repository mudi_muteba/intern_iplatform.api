﻿using System;
using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions.Builders;
using iPlatform.Api.DTOs.Party.ProposalHeaders;

namespace Domain.Party.ProposalHeaders.Builders
{
    public static class ProposalHeaderBuilder
    {
        public static ListProposalHeaderDto BuildList(this ProposalHeader proposalHeader, IRepository repository)
        {
            var model = Mapper.Map<ListProposalHeaderDto>(proposalHeader);
            if (proposalHeader != null)
                model.IsQuotable = proposalHeader.ProposalDefinitions.Any(x => x.Asset != null && x.Asset.Id > 0);
            return model;
        }

        public static ProposalHeaderDto Build(this ProposalHeader proposalHeader, IRepository repository)
        {
            var model = Mapper.Map<ProposalHeaderDto>(proposalHeader);
            if (proposalHeader != null)
            {
                var proposalDefinition = proposalHeader.ProposalDefinitions.FirstOrDefault(x => x.Asset != null && x.Asset.Id > 0);
                model.IsQuotable = proposalDefinition != null;
                model.QuoteExpirationDays = model.IsQuotable ? proposalDefinition.Product.QuoteExpiration : 0;
                var dateDiffernce = DateTime.UtcNow - model.RatingDate.Value;
                if (dateDiffernce != null)
                    model.QuoteExpired = dateDiffernce.Value.TotalDays > model.QuoteExpirationDays;
            }

            return model;
        }

        public static ProposalHeaderDetailedDto BuildDetailed(this ProposalHeader proposalHeader, IRepository repository)
        {
            var model = Mapper.Map<ProposalHeaderDetailedDto>(proposalHeader);

            foreach (var definition in proposalHeader.ProposalDefinitions)
            {
                model.ProposalDefinitions.Add(new ProposalDefinitionBuilderDto(repository).Build(definition));
            }

            return model;
        }
    }
}
