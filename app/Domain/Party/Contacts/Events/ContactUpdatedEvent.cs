﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Party.Contacts;
using iPlatform.Api.DTOs.Party.Contacts;

namespace Domain.Party.Contacts.Events
{
    public class ContactUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public ContactUpdatedEvent(ContactDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            Contact = dto;
        }

        public ContactDto Contact { get; private set; }
        public int UserId { get; private set; }
    }
}