﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Party.Contacts;
using iPlatform.Api.DTOs.Party.Contacts;
using MasterData;

namespace Domain.Party.Contacts.Events
{
    public class ContactCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public int UserId { get; private set; }
        public ContactDto Contact { get; private set; }
        public ContactCreatedEvent(ContactDto dto, EventAudit audit, List<ChannelReference> channelReferences)
            : base(audit,  channelReferences )
        {
            Contact = dto;
        }
    }
}