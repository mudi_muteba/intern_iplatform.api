﻿using System;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Party.Contacts.Events;
using Domain.Party.Relationships;
using iPlatform.Api.DTOs.Party.Contacts;
using MasterData;
using System.Collections.Generic;
using Domain.Occupations;

namespace Domain.Party.Contacts
{
    public class Contact : Party
    {
        private string _firstName;
        private string _surname;

        public Contact()
        {
            base.PartyType = PartyTypes.Contact;
        }

        public virtual Title Title { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual Occupation Occupation { get; set; }
        public virtual string ContactType { get; set; }

        public virtual string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                base.DisplayName = string.Format("{0}, {1}", _surname, _firstName);
            }
        }

        public virtual string MiddleName { get; set; }

        public virtual string Surname
        {
            get { return _surname; }
            set
            {
                _surname = value;
                base.DisplayName = string.Format("{0}, {1}", _surname, _firstName);
            }
        }

        public virtual DateTime? DateOfBirth { get; set; }

        public virtual string IdentityNo { get; set; }

        public virtual string PassportNo { get; set; }

        public virtual MaritalStatus MaritalStatus { get; set; }


        public virtual MarriageType MarriageType { get; set; }
        public virtual DateTime? DateOfMarriage { get; set; }

        
        public virtual Language Language { get; set; }

        public virtual void SetDisplayName()
        {
            base.DisplayName = string.Format("{0}, {1}", _surname, _firstName);
        }

        public static Contact Create(CreateContactDto createContactDto)
        {
            var contact = Mapper.Map<Contact>(createContactDto);

            return contact;
        }


        public static Contact Create(CreateContactDto createContactDto, Party party, ChannelReference channel)
        {
            var contact = Mapper.Map<Contact>(createContactDto);
            contact.Channel = party.Channel;
            return contact;
        }

        public virtual void Update(EditContactDto editContactDto)
        {
            Mapper.Map(editContactDto, this);
            SetDisplayName();
            RaiseEvent(new ContactUpdatedEvent(Mapper.Map<ContactDto>(this), editContactDto.CreateAudit(), new List<ChannelReference> { new Channel(this.Channel.Id) }));
        }
    }
}