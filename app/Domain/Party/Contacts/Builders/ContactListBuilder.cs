﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Party.Relationships;
using iPlatform.Api.DTOs.Party.Contacts;

namespace Domain.Party.Contacts.Builders
{
    public class ContactListBuilder
    {
        public List<ListContactDto> Build(IList<Relationship> relationship)
        {
            var list = new List<ListContactDto>();

            foreach (Relationship item in relationship)
            {
                if (item.ChildParty != null)
                {
                    if (item.ChildParty.GetType() != typeof(Contact))
                        continue;

                    var contactDto = Mapper.Map<ListContactDto>(item.ChildParty);
                    contactDto.RelationshipType = item.RelationshipType;
                    list.Add(contactDto);
                }
            }

            return list;
        }
    }
}