﻿using AutoMapper;
using Domain.Party.Relationships;
using iPlatform.Api.DTOs.Party.Contacts;

namespace Domain.Party.Contacts.Builders
{
    public class ContactBuilder
    {
        public ContactDto Build(Relationship relationship)
        {
            if (relationship.ChildParty.GetType() != typeof (Contact))
            {
                return null;
            }

            var contactDto = Mapper.Map<ContactDto>(relationship.ChildParty);
            contactDto.RelationshipType = relationship.RelationshipType;


            return contactDto;
        }
    }
}