﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Party.Relationships;
using MasterData.Authorisation;

namespace Domain.Party.Contacts.Queries
{
    public class GetContactByPartyIdQuery : BaseQuery<Contact>
    {
        private readonly IRepository _repository;

        private int _partyId;
        private int _contactId;

        public GetContactByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Contact>())
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetContactByPartyIdQuery WithContactId(int id)
        {
            _contactId = id;
            return this;
        }

        public GetContactByPartyIdQuery WithPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<Contact> Execute(IQueryable<Contact> query)
        {
            if (_contactId > 0 && _partyId == 0)
                return query.Where(x => x.Id == _contactId);

            return _repository.GetAll<Relationship>()
                .Where(a => a.Party.Id == _partyId)
                .Select(a => a.ChildParty as Contact);
        }
    }
}