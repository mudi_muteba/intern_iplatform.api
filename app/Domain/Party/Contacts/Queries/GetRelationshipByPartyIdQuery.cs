﻿
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Party.Relationships;
using MasterData.Authorisation;

namespace Domain.Party.Contacts.Queries
{
    public class GetRelationshipByPartyIdQuery : BaseQuery<Relationship>
    {
        private readonly IRepository _repository
            ;

        private int _partyId;

        public GetRelationshipByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Relationship>())
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetRelationshipByPartyIdQuery WithPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<Relationship> Execute(IQueryable<Relationship> query)
        {
            var relationships = _repository.GetAll<Relationship>().Where(x => x.Party.Id == _partyId && !x.ChildParty.IsDeleted);
            return relationships;
        }
    }
}