﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Admin;
using Domain.Occupations;
using Domain.Party.Contacts;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Contacts;

namespace Domain.Party.Contacts.Mappings
{
    public class ContactsMapper : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<Contact, ContactDto>()
                .ForMember(t => t.Events, o => o.MapFrom(s => s.Audit))
                .ForMember(t => t.Occupation, o => o.MapFrom(s => s.Occupation))
                ;

            Mapper.CreateMap<List<ListContactDto>, ListResultDto<ListContactDto>>();

            Mapper.CreateMap<List<ListContactDto>, ListResultDto<ListContactDto>>();

            Mapper.CreateMap<CreateContactDto, Contact>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Channel, o => o.MapFrom(s => s.ChannelId > 0 ? new Channel(s.ChannelId) : null))
                .ForMember(t => t.Occupation, o => o.MapFrom(s => new Occupation {Id = s.Occupation.Id}));

            Mapper.CreateMap<Contact, ListContactDto>();
            Mapper.CreateMap<EditContactDto, Contact>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Occupation, o => o.MapFrom(s => new Occupation { Id = s.Occupation.Id }));
        
        }
    }
}