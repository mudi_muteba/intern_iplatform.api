﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.Contacts.Events;
using iPlatform.Api.DTOs.Party.Contacts;
using MasterData.Authorisation;
using AutoMapper;

namespace Domain.Party.Contacts.Handlers
{
    public class CreateContactHandler : CreationDtoHandler<Contact, CreateContactDto, int>
    {
        public CreateContactHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(Contact entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Contact HandleCreation(CreateContactDto dto, HandlerResult<int> result)
        {
            //TODO: Get this from the token, it will be used for validation as well
            var channelList = ExecutionContext.Channels;

            var party = _repository.GetById<Party>(dto.PartyId);

            if (party == null)
                throw new MissingEntityException(typeof(Party),dto.PartyId);

            if (!channelList.Any())
                throw new MissingEntityException(typeof(Channel), 0);

            var channel = channelList.Any(x => x == party.Channel.Id) ? party.Channel : new Channel(channelList.First());

            Contact contact = Contact.Create(dto,party, channel);
            _repository.Save(contact);

            party.AddRelationship(contact, dto.RelationshipType);

            _repository.Save(party);

            contact.RaiseEvent(new ContactCreatedEvent(Mapper.Map<ContactDto>(contact), dto.CreateAudit(), new List<ChannelReference> { new Channel(channel.Id) }));
            result.Processed(contact.Id);
            return contact;
        }
    }
}