﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Contacts;
using MasterData.Authorisation;

namespace Domain.Party.Contacts.Handlers
{
    public class DeleteContactHandler : ExistingEntityDtoHandler<Party, DeleteContactDto, int>
    {
        public DeleteContactHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            Repository = repository;
        }

        private IRepository Repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override Party GetEntityById(DeleteContactDto dto)
        {
            return repository.GetById<Party>(dto.Id);
        }

        protected override void HandleExistingEntityChange(Party entity, DeleteContactDto dto,
            HandlerResult<int> result)
        {
            entity.Delete();
            result.Processed(dto.Id);
        }
    }
}