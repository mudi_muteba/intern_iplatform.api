﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Contacts;
using MasterData.Authorisation;

namespace Domain.Party.Contacts.Handlers
{
    public class UpdateContactHandler : ExistingEntityDtoHandler<Contact, EditContactDto, int>
    {
        public UpdateContactHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Edit
                };
            }
        }

        protected override Contact GetEntityById(EditContactDto dto)
        {
            return repository.GetById<Contact>(dto.Id);
        }

        protected override void HandleExistingEntityChange(Contact entity, EditContactDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);


            var party = _repository.GetById<Party>(dto.PartyId);

            if (party == null)
                throw new MissingEntityException(typeof(Party), dto.PartyId);

            party.UpdateRelationship(entity, dto.RelationshipType);
            _repository.Save(party);
            
            result.Processed(entity.Id);

        }
    }
}