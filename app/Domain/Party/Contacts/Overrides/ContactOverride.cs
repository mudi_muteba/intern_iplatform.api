﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Contacts.Overrides
{
    public class ContactOverride : IAutoMappingOverride<Contact>
    {
        public void Override(AutoMapping<Contact> mapping)
        {
        }
    }
}