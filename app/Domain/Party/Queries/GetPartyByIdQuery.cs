﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Queries
{
    public class GetPartyByIdQuery : BaseQuery<Party>
    {
        private int id;

        public GetPartyByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Party>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetPartyByIdQuery WithId(int id)
        {
            this.id = id;
            return this;
        }

        protected internal override IQueryable<Party> Execute(IQueryable<Party> query)
        {
            return query
                .Where(c => c.Id == id);
        }
   
    }
}