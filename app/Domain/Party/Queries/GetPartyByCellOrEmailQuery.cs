﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Leads;
using MasterData.Authorisation;

namespace Domain.Party.Queries
{
    public class GetPartyByCellOrEmailQuery : BaseQuery<Lead>
    {
        private string cellNumber;
        private string emailAddress;

        public GetPartyByCellOrEmailQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Lead>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetPartyByCellOrEmailQuery WithCellorEmail(string cellNumber, string emailAddress)
        {
            this.cellNumber = cellNumber;
            this.emailAddress = emailAddress;
            return this;
        }

        protected internal override IQueryable<Lead> Execute(IQueryable<Lead> query)
        {

            return
                query.Where(
                    x =>
                        x.Party.ContactDetail.Cell.Contains(cellNumber.Substring(2))  || x.Party.ContactDetail.Email == emailAddress);

        }
   
    }
}