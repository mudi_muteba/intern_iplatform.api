﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Queries
{
    public class GetPartyByEmailQuery : BaseQuery<Party>
    {
        private string email;

        public GetPartyByEmailQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultPartyFilters())
        {}

        public override List<AuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<AuthorisationPoint>
                {
                    IndividualAuthorisation.List
                };
            }
        }

        public GetPartyByEmailQuery ByEmail(string email)
        {
            this.email = email;
            return this;
        }

        protected internal override IQueryable<Party> Execute(IQueryable<Party> query)
        {
            return query
                .Where(c => c.ContactDetail.Email == email);
        }
    }
}