﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base;
using Domain.Campaigns;
using Domain.Individuals;


namespace Domain.Party
{
    public partial class PartyCampaign : Entity
    {
        public virtual Campaign Campaign { get; set; }

        public virtual Party Party { get; set; }
    }
}