﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Overrides
{
    public class PartyCampaignOverride : IAutoMappingOverride<PartyCampaign>
    {
        public void Override(AutoMapping<PartyCampaign> mapping)
        {
            
        }
    }
}