﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Overrides
{
    public class PartyOverride : IAutoMappingOverride<Party>
    {
        public void Override(AutoMapping<Party> mapping)
        {
            mapping.Map(x => x.DisplayName).Length(550);
            mapping.References(x => x.ContactDetail).Cascade.SaveUpdate();
            mapping.HasOne(x => x.Lead).PropertyRef(a=>a.Party).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Addresses).Cascade.SaveUpdate();
            mapping.HasMany(x => x.BankDetails).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Notes).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Relationships).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Assets).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Activities).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Campaigns).Cascade.SaveUpdate();
            mapping.HasMany(x => x.CorrespondencePreferences).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Correspondence).Cascade.SaveUpdate();
        }
    }
}