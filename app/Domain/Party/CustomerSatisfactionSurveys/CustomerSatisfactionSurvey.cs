﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base;
using Domain.Party.Relationships;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using MasterData;

namespace Domain.Party.CustomerSatisfactionSurveys
{
    public class CustomerSatisfactionSurvey : EntityWithParty
    {
        public CustomerSatisfactionSurvey()
        {
            Answers = new List<CustomerSatisfactionSurveyAnswers>();
        }
        public virtual string Event { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual IList<CustomerSatisfactionSurveyAnswers> Answers { get; protected set; }

        public virtual void AddAnswers(List<CustomerSatisfactionSurveyAnswersDto> answers)
        {
            foreach(var answer in answers)
            {
                var entity = Mapper.Map<CustomerSatisfactionSurveyAnswers>(answer);
                entity.CustomerSatisfactionSurvey = this;
                Answers.Add(entity);
            }
        }


        public static CustomerSatisfactionSurvey Create(Party party, CreateCustomerSatisfactionSurveyDto dto)
        {
            var entity = Mapper.Map<CustomerSatisfactionSurvey>(dto);
            entity.Party = party;

            entity.AddAnswers(dto.Answers);
            return entity;
        }
    }
}
