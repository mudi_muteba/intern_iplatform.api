﻿using Domain.Party.CorrespondencePreferences;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.CustomerSatisfactionSurveys.Overrides
{
    public class CustomerSatisfactionSurveyOverride : IAutoMappingOverride<CustomerSatisfactionSurvey>
    {
        public void Override(AutoMapping<CustomerSatisfactionSurvey> mapping)
        {
            mapping.References(x => x.Party);
            mapping.HasMany(x => x.Answers).Cascade.SaveUpdate();
        }
    }
}
