﻿using Domain.Party.CorrespondencePreferences;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.CustomerSatisfactionSurveys.Overrides
{
    public class CustomerSatisfactionSurveyAnswersOverride : IAutoMappingOverride<CustomerSatisfactionSurveyAnswers>
    {
        public void Override(AutoMapping<CustomerSatisfactionSurveyAnswers> mapping)
        {
            mapping.References(x => x.CustomerSatisfactionSurvey);
        }
    }
}
