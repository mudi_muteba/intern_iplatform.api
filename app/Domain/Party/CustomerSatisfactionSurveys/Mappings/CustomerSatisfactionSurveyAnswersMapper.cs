﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;

namespace Domain.Party.CustomerSatisfactionSurveys.Mappings
{
    public class CustomerSatisfactionSurveyAnswersMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CustomerSatisfactionSurveyAnswersDto, CustomerSatisfactionSurveyAnswers>()
                .ForMember(t => t.CustomerSatisfactionSurvey, o => o.Ignore())
                ;

            Mapper.CreateMap<CustomerSatisfactionSurveyAnswers, CustomerSatisfactionSurveyAnswersDto>()
                .ForMember(t => t.CustomerSatisfactionSurveyId, o => o.MapFrom(s => s.CustomerSatisfactionSurvey.Id))
                .ForMember(t => t.Answer, o => o.MapFrom(s => s.Answer))
                ;

            Mapper.CreateMap<List<CustomerSatisfactionSurveyAnswers>, List<CustomerSatisfactionSurveyAnswersDto>>()

                ;
        }
    }
}