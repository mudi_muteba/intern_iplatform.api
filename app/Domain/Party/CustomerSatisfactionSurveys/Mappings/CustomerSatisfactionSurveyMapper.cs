﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using MoreLinq;

namespace Domain.Party.CustomerSatisfactionSurveys.Mappings
{
    public class CustomerSatisfactionSurveyMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateCustomerSatisfactionSurveyDto, CustomerSatisfactionSurvey>()
                .ForMember(t => t.CreatedOn, o => o.MapFrom(s => DateTime.UtcNow))
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party{Id = s.PartyId}))
                .ForMember(t => t.Event, o => o.MapFrom(s => s.Event))
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.Answers, o => o.Ignore())
                ;

            Mapper.CreateMap<CustomerSatisfactionSurvey, CustomerSatisfactionSurveyDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                .ForMember(t => t.Event, o => o.MapFrom(s => s.Event))
                .ForMember(t => t.Answers, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    d.Answers.AddRange(s.Answers.Select(Mapper.Map<CustomerSatisfactionSurveyAnswersDto>));
                });
        }
    }
}