﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Leads;
using Domain.Party.Queries;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using MasterData.Authorisation;
using NHibernate.Proxy;
using Workflow.Messages;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Shared;

namespace Domain.Party.CustomerSatisfactionSurveys.Handlers
{
    public class CustomerSatisfactionSurveyRequestDtoHandler : BaseDtoHandler<CustomerSatisfactionSurveyRequestDto, bool>
    {

        private readonly IWorkflowRouter _router;
        public CustomerSatisfactionSurveyRequestDtoHandler(IProvideContext contextProvider, IWorkflowRouter router) : base(contextProvider)
        {
            _router = router;
        }
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void InternalHandle(CustomerSatisfactionSurveyRequestDto dto, HandlerResult<bool> result)
        {
            _router.Publish(new CustomerSatisfactionSurveyMessage(new CustomerSatisfactionSurveyMetadata(dto)));
        }
    }
}