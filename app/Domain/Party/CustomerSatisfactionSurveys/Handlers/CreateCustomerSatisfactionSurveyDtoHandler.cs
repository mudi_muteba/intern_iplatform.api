﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Leads;
using Domain.Party.Queries;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using MasterData.Authorisation;
using NHibernate.Proxy;

namespace Domain.Party.CustomerSatisfactionSurveys.Handlers
{
    public class CreateCustomerSatisfactionSurveyDtoHandler : CreationDtoHandler<CustomerSatisfactionSurvey, CreateCustomerSatisfactionSurveyDto, int>
    {
        

        public CreateCustomerSatisfactionSurveyDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _query = new GetPartyByCellOrEmailQuery(contextProvider, repository);
        }

        private IRepository _repository { get; set; }
        private readonly GetPartyByCellOrEmailQuery _query;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void EntitySaved(CustomerSatisfactionSurvey entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override CustomerSatisfactionSurvey HandleCreation(CreateCustomerSatisfactionSurveyDto dto, HandlerResult<int> result)
        {
            var leads = _query.WithCellorEmail(dto.CellNumber, dto.EmailAddress).ExecuteQuery();
            if (leads == null)
            {
                Log.ErrorFormat("No Lead match found for CellNumber :'{0}' and EmailAddress: '{1}'", dto.CellNumber, dto.EmailAddress);
                return null;
            }

            var quoteuploadLog =
                    _repository.GetAll<QuoteUploadLog>().Where(x => x.InsurerReference == dto.PolicyNumber);

            Quote quote = quoteuploadLog.Any() ? _repository.GetById<Quote>(quoteuploadLog.First().Quote.Id) : null;

            if (quote == null)
            {
                Log.ErrorFormat("No quote match found for reference {0}", dto.PolicyNumber);
                return null;
            }

            var lead = leads.FirstOrDefault();

            if (leads.Count() > 1)
            {
                Log.WarnFormat("More than one lead found for CellNumber :'{0}' and EmailAddress: '{1}'", dto.CellNumber, dto.EmailAddress);
                Log.InfoFormat("Looking for lead via quoteuploadlog using PolicyNumber '{0}'", dto.PolicyNumber);

                if (quote != null)
                {
                    var quoteLead = quote.QuoteHeader.Proposal.LeadActivity.Lead;

                    var leadproxy = quoteLead as INHibernateProxy;

                    if (leadproxy != null)
                        lead = leadproxy.HibernateLazyInitializer.GetImplementation() as Lead;

                }
                else
                {
                    Log.WarnFormat("No QuoteUploadLog found using PolicyNumber '{0}'", dto.PolicyNumber);
                    Log.InfoFormat("Using first Lead among the List Leads '{0}'", lead.Id.ToString());
                }
            }

            dto.PartyId = lead.Party.Id;
            var entity = CustomerSatisfactionSurvey.Create(lead.Party, dto);
            result.Processed(entity.Id);

            Log.InfoFormat("Saving surveyrespondedleadactivity with LeadId '{0}'", lead.Id.ToString());
            lead.SurveyResponded(dto.Context.UserId,  entity, quote.QuoteHeader.Proposal.LeadActivity.Campaign);
            _repository.Save(lead);

            return entity;
        }
    }
}