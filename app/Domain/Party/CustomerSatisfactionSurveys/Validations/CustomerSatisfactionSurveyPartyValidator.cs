﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Leads;
using Domain.Party.Queries;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using ValidationMessages;

namespace Domain.Party.CustomerSatisfactionSurveys.Validations
{
    public class CustomerSatisfactionSurveyPartyValidator : IValidateDto<CreateCustomerSatisfactionSurveyDto>
    {
        private readonly GetPartyByCellOrEmailQuery query;

        public CustomerSatisfactionSurveyPartyValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetPartyByCellOrEmailQuery(contextProvider, repository);
        }

        public void Validate(CreateCustomerSatisfactionSurveyDto dto, ExecutionResult result)
        {
            ValidateParty(dto.CellNumber, dto.EmailAddress, result);
        }


        private void ValidateParty(string cellNumber, string emailAddress, ExecutionResult result)
        {
            var queryResult = GetPartyByCellOrEmailQuery(cellNumber, emailAddress);

            if (!queryResult.Any())
            {
                result.AddValidationMessage(CustomerSatisfactionSurveyValidationMessages.InvalidCellNumberOrEmailAddress.AddParameters(new[] { cellNumber, emailAddress }));
            }
        }

        private IQueryable<Lead> GetPartyByCellOrEmailQuery(string cellNumber, string emailAddress)
        {
            return query.WithCellorEmail(cellNumber, emailAddress).ExecuteQuery();
        }
    }
}