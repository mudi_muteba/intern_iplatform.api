﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;

namespace Domain.Party.CustomerSatisfactionSurveys
{
    public class CustomerSatisfactionSurveyAnswers : Entity
    {
        public CustomerSatisfactionSurveyAnswers() { }
        public virtual CustomerSatisfactionSurvey CustomerSatisfactionSurvey { get; set; }
        public virtual string Question { get; set; }
        public virtual string Answer { get; set; }

    }
}
