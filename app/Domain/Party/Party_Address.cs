﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Events;
using Domain.Base.Execution.Exceptions;
using Domain.Party.Addresses;
using Domain.Party.Addresses.Events;
using iPlatform.Api.DTOs.Party.Address;

namespace Domain.Party
{
    public partial class Party
    {
        public virtual IList<Address> Addresses { get; protected internal set; }

        public virtual Address AddAddress(CreateAddressDto dto)
        {
            var address = Mapper.Map<Address>(dto);
            address.RaiseEvent(new AddressCreatedEvent(address, dto.CreateAudit(), Channel));

            if (address.Party == null)
                address.Party = this;
            Addresses.Add(address);
            return address;
        }

        public virtual Address DeleteAddress(DeleteAddressDto dto)
        {
            Address address = Addresses.FirstOrDefault(a => a.Id == dto.Id);

            if (address == null)
                throw new MissingEntityException(typeof (Address), dto.Id);

            address.IsDeleted = true;
            return address;
        }

        public virtual void UpdateAddress(EditAddressDto dto)
        {
            Address address = Addresses.FirstOrDefault(a => a.Id == dto.Id);

            if (address == null)
            {
                throw new MissingEntityException(typeof (Address), dto.Id);
            }

            address.Update(dto);
        }
    }
}