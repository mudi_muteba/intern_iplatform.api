﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using MasterData.Authorisation;

namespace Domain.Party.Assets.AssetRiskItems.Handlers
{
    public class CreateAssetRiskItemHandler : CreationDtoHandler<AssetRiskItem, CreateAssetRiskItemDto, int>
    {
        public CreateAssetRiskItemHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(AssetRiskItem entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override AssetRiskItem HandleCreation(CreateAssetRiskItemDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Party>(dto.PartyId);
            AssetRiskItem AssetRiskItem = party.AddAssetRiskItem(dto);

            result.Processed(AssetRiskItem.Id);
            return AssetRiskItem;
        }
    }
}