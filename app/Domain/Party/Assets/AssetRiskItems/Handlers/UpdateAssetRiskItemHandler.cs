﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using MasterData.Authorisation;

namespace Domain.Party.Assets.AssetRiskItems.Handlers
{
    public class UpdateAssetRiskItemHandler : ExistingEntityDtoHandler<Party, EditAssetRiskItemDto, int>
    {
        public UpdateAssetRiskItemHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Edit
                };
            }
        }

        protected override Party GetEntityById(EditAssetRiskItemDto dto)
        {
            return repository.GetById<Party>(dto.PartyId);
        }

        protected override void HandleExistingEntityChange(Party entity, EditAssetRiskItemDto dto,
            HandlerResult<int> result)
        {
            entity.UpdateAssetRiskItem(dto);
        }
    }
}