﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using MasterData.Authorisation;

namespace Domain.Party.Assets.AssetRiskItems.Handlers
{
    public class DeleteAssetRiskItemHandler : ExistingEntityDtoHandler<Party, DeleteAssetRiskItemDto, int>
    {
        public DeleteAssetRiskItemHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            Repository = repository;
        }

        private IRepository Repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Delete
                };
            }
        }

        protected override Party GetEntityById(DeleteAssetRiskItemDto dto)
        {
            return repository.GetById<Party>(dto.PartyId);
        }

        protected override void HandleExistingEntityChange(Party entity, DeleteAssetRiskItemDto dto,
            HandlerResult<int> result)
        {
            var party = Repository.GetById<Party>(dto.PartyId);
            party.DeleteAssetRiskItem(dto);

            result.Processed(dto.Id);
        }
    }
}