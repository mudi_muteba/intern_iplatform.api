﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Party.Assets.AssetRiskItems.Events
{
    public class AssetRiskItemUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public AssetRiskItemUpdatedEvent(AssetRiskItem _AssetRiskItem, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> {channelReferences})
        {
            AssetRiskItem = _AssetRiskItem;
        }

        public AssetRiskItem AssetRiskItem { get; private set; }
        public int UserId { get; private set; }
    }
}