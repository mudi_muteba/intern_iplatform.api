﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Party.Assets.AssetRiskItems.Events
{
    public class AssetRiskItemCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public AssetRiskItem AssetRiskItem { get; private set; }
        public int UserId { get; private set; }

        public AssetRiskItemCreatedEvent(AssetRiskItem _AssetRiskItem, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> { channelReferences })
        {
            AssetRiskItem = _AssetRiskItem;
        }
    }
}