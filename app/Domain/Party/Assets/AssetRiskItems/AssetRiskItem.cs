using System;
using AutoMapper;
using Domain.Base.Events;
using Domain.Party.Assets.AssetRiskItems.Events;
using Domain.Party.Assets.AssetVehicles;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;

namespace Domain.Party.Assets.AssetRiskItems
{
    [Serializable]
    public class AssetRiskItem : Asset
    {
        public virtual int? AllRiskCategory { get; set; }
        public virtual string SerialIMEINumber { get; set; }

        public override Asset NullAsset()
        {
            return new AssetVehicle
            {
                Description = "Asset not found"
            };
        }

        public virtual void Update(EditAssetRiskItemDto dto)
        {
            Mapper.Map(dto, this);
            RaiseEvent(new AssetRiskItemUpdatedEvent(this, dto.CreateAudit(), Party.Channel));
        }
    }
}