﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Assets.AssetRiskItems.Queries
{
    public class GetAssetRiskItemByPartyIdQuery : BaseQuery<AssetRiskItem>
    {
        private int _partyId;

        public GetAssetRiskItemByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<AssetRiskItem>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetAssetRiskItemByPartyIdQuery WithPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<AssetRiskItem> Execute(IQueryable<AssetRiskItem> query)
        {
            return Repository.GetAll<AssetRiskItem>().Where(a => a.Party.Id == _partyId);
        }
    }
}