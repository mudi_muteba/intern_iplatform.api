﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;

namespace Domain.Party.Assets.AssetRiskItems.Mappings
{
    public class AssetRiskItemMapper : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<AssetRiskItem, AssetRiskItemDto>()
                .ForMember(t => t. Events, o => o.MapFrom(s => s.Audit));

            Mapper.CreateMap<List<ListAssetRiskItemDto>, ListResultDto<ListAssetRiskItemDto>>();

            Mapper.CreateMap<List<AssetRiskItem>, ListResultDto<AssetRiskItemDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<List<AssetRiskItem>, ListResultDto<ListAssetRiskItemDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s=> s));

            Mapper.CreateMap<CreateAssetRiskItemDto, AssetRiskItem>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore());

            Mapper.CreateMap<AssetRiskItem, ListAssetRiskItemDto>();
            Mapper.CreateMap<AssetRiskItem, AssetRiskItemDto>();
            Mapper.CreateMap<EditAssetRiskItemDto, AssetRiskItem>();
        
        }
    }
}