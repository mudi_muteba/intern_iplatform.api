﻿using Domain.Base;
using MasterData;

namespace Domain.Party.Assets
{
    public class AssetExtras : Entity
    {
        public virtual Asset Asset { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal Value { get; set; }
        public virtual bool IsUserDefined { get; set; }
        public virtual bool Selected { get; set; }
        public virtual AccessoryType AccessoryType { get; set; }

    }
}
