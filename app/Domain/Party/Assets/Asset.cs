using System;
using System.Collections.Generic;
using Domain.Base;

namespace Domain.Party.Assets
{
    [Serializable]
    public class Asset : EntityWithParty
    {
        public Asset()
        {
            Extras = new List<AssetExtras>();
        }

        public virtual string Description { get; set; }
        public virtual decimal SumInsured { get; set; }

        public virtual IList<AssetExtras> Extras { get; internal protected set; }
        public virtual int AssetNo { get; set; }
        public virtual void AddAssetExtras(AssetExtras extra)
        {
            extra.Asset = this;
            Extras.Add(extra);
        }

        public virtual Asset NullAsset()
        {
            return new Asset();
        }

    }
}