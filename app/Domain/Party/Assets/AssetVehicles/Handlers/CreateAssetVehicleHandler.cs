﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using MasterData.Authorisation;

namespace Domain.Party.Assets.AssetVehicles.Handlers
{
    public class CreateAssetVehicleHandler : CreationDtoHandler<AssetVehicle, CreateAssetVehicleDto, int>
    {
        public CreateAssetVehicleHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(AssetVehicle entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override AssetVehicle HandleCreation(CreateAssetVehicleDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Party>(dto.PartyId);
            AssetVehicle AssetVehicle = party.AddAssetVehicle(dto);

            result.Processed(AssetVehicle.Id);
            return AssetVehicle;
        }
    }
}