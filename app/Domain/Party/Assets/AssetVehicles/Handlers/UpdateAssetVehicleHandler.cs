﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using MasterData.Authorisation;

namespace Domain.Party.Assets.AssetVehicles.Handlers
{
    public class UpdateAssetVehicleHandler : ExistingEntityDtoHandler<Party, EditAssetVehicleDto, int>
    {
        public UpdateAssetVehicleHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Edit
                };
            }
        }

        protected override Party GetEntityById(EditAssetVehicleDto dto)
        {
            return repository.GetById<Party>(dto.PartyId);
        }

        protected override void HandleExistingEntityChange(Party entity, EditAssetVehicleDto dto,
            HandlerResult<int> result)
        {
            entity.UpdateAssetVehicle(dto);
        }
    }
}