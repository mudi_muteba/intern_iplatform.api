﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using MasterData.Authorisation;

namespace Domain.Party.Assets.AssetVehicles.Handlers
{
    public class DeleteAssetVehicleHandler : ExistingEntityDtoHandler<Party, DeleteAssetVehicleDto, int>
    {
        public DeleteAssetVehicleHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            Repository = repository;
        }

        private IRepository Repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    IndividualAuthorisation.Delete
                };
            }
        }

        protected override Party GetEntityById(DeleteAssetVehicleDto dto)
        {
            return repository.GetById<Party>(dto.PartyId);
        }

        protected override void HandleExistingEntityChange(Party entity, DeleteAssetVehicleDto dto,
            HandlerResult<int> result)
        {
            var party = Repository.GetById<Party>(dto.PartyId);
            party.DeleteAssetVehicle(dto);

            result.Processed(dto.Id);
        }
    }
}