﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;

namespace Domain.Party.Assets.AssetVehicles.Mappings
{
    public class AssetVehicleMapper : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<AssetVehicle, AssetVehicleDto>()
                .ForMember(t => t.Extras, o => o.MapFrom(s => s.Extras))
                .ForMember(t => t. Events, o => o.MapFrom(s => s.Audit));

            Mapper.CreateMap<List<ListAssetVehicleDto>, ListResultDto<ListAssetVehicleDto>>();
            Mapper.CreateMap<List<AssetVehicle>, ListResultDto<ListAssetVehicleDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<CreateAssetVehicleDto, AssetVehicle>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore());

            Mapper.CreateMap<AssetVehicle, ListAssetVehicleDto>();
            Mapper.CreateMap<EditAssetVehicleDto, AssetVehicle>();
        
        }
    }
}