﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Party.Assets.AssetVehicles.Events
{
    public class AssetVehicleUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public AssetVehicleUpdatedEvent(AssetVehicle _AssetVehicle, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> {channelReferences})
        {
            AssetVehicle = _AssetVehicle;
        }

        public AssetVehicle AssetVehicle { get; private set; }
        public int UserId { get; private set; }
    }
}