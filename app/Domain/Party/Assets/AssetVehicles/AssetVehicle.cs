using System;
using AutoMapper;
using Domain.Base.Events;
using Domain.Party.Assets.AssetVehicles.Events;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using MasterData;

namespace Domain.Party.Assets.AssetVehicles
{
    public class AssetVehicle : Asset
    {
        public virtual int? YearOfManufacture { get; set; }
        public virtual string VehicleMake { get; set; }
        public virtual string VehicleModel { get; set; }
        public virtual string VehicleRegistrationNumber { get; set; }
        public virtual string VehicleMMCode { get; set; }
        public virtual VehicleType VehicleType { get; set; }


        public virtual void SetDescription()
        {
            Description =
                string.Format("{0}, {1}, {2}, {3}",
                    VehicleMake,
                    VehicleModel,
                    YearOfManufacture,
                    VehicleRegistrationNumber);
        }


        public override Asset NullAsset()
        {
            return new AssetVehicle
            {
                Description = "Asset not found"
            };
        }

        public virtual void Update(EditAssetVehicleDto dto)
        {
            Mapper.Map(dto, this);
            RaiseEvent(new AssetVehicleUpdatedEvent(this, dto.CreateAudit(), Party.Channel));
        }
    }
}