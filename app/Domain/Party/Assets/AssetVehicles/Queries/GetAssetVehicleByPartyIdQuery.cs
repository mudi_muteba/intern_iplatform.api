﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Assets.AssetVehicles.Queries
{
    public class GetAssetVehicleByPartyIdQuery : BaseQuery<AssetVehicle>
    {
        private int _partyId;

        public GetAssetVehicleByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<AssetVehicle>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetAssetVehicleByPartyIdQuery WithPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<AssetVehicle> Execute(IQueryable<AssetVehicle> query)
        {
            return Repository.GetAll<AssetVehicle>().Where(a => a.Party.Id == _partyId);
        }
    }
}