﻿using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using NHibernate.Proxy;

namespace Domain.Party.Assets
{
    public class AssetList
    {
        private List<Asset> _assets;

        public AssetList()
        {
            _assets = new List<Asset>();
            AssetExtras = new List<AssetExtrasDto>();
        }

        public List<AssetExtrasDto> AssetExtras { get; set; }

        public void AddRange(List<Asset> assets)
        {
            _assets.AddRange(assets);
        }

        public Asset GetAsset<T>(Func<IList< Asset>, AssetInternalCriteria, Asset> function, AssetInternalCriteria criteria) where T : class
        {
            var list = _assets.Where(p =>
            {
                if (p.GetType() == typeof (T))
                    return true;
                
                var assetProxy = p as INHibernateProxy;
                if (assetProxy == null) return false;

                var asset = assetProxy.HibernateLazyInitializer.GetImplementation();
                return asset.GetType() == typeof (T);

            }).ToList();
            return function(list, criteria);
        }
    }

    public class AssetInternalCriteria
    {
        public string SerialIMEINumber { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public int AssetId { get; set; }
        public int AddressId { get; set; }
    }
}
