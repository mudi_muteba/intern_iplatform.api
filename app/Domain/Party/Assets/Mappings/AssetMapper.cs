﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Party.Assets;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;

namespace Domain.Party.Assets.Mappings
{
    public class AssetMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Asset, AssetDto>();
            Mapper.CreateMap<AssetExtras, AssetExtrasDto>().ReverseMap();
        }
    }
}