﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Party.Assets.AssetAddresses;
using Domain.Party.Assets.AssetRiskItems;
using Domain.Party.Assets.AssetVehicles;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Party.Assets.Overrides
{

    public class AssetOverride : IAutoMappingOverride<Asset>
    {
        public void Override(AutoMapping<Asset> mapping)
        {
            mapping.HasMany(x => x.Extras).Cascade.SaveUpdate();
            mapping.Not.LazyLoad();
        }
    }


    public class AssetAddressOverride : IAutoMappingOverride<AssetAddress>
    {
        public void Override(AutoMapping<AssetAddress> mapping)
        {
            mapping.Not.LazyLoad();
        }
    }

    public class AssetVehicleOverride : IAutoMappingOverride<AssetVehicle>
    {
        public void Override(AutoMapping<AssetVehicle> mapping)
        {
            mapping.Not.LazyLoad();
        }
    }

    public class AssetRiskItemOverride : IAutoMappingOverride<AssetRiskItem>
    {
        public void Override(AutoMapping<AssetRiskItem> mapping)
        {
            mapping.Not.LazyLoad();
        }
    }
}
