﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Party.Addresses;

namespace Domain.Party.Assets.AssetAddresses
{
    public class AssetAddress : Asset
    {
        public virtual Address Address { get; set; }

        public override Asset NullAsset()
        {
            return new AssetAddress()
            {
                Description = "Asset not found"
            };
        }
    }
}
