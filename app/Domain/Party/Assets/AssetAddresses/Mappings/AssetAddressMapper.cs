﻿using AutoMapper;
using iPlatform.Api.DTOs.Party.Assets.AssetAddresses;

namespace Domain.Party.Assets.AssetAddresses.Mappings
{
    public class AssetAddressMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateAssetAddressDto, AssetAddress>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore());
        }
    }
}