﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Party.Assets.AssetRiskItems;

namespace Domain.Party.Assets.AssetAddresses.Events
{
    public class AssetAddressCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public AssetAddress AssetAddress { get; private set; }
        public int UserId { get; private set; }

        public AssetAddressCreatedEvent(AssetAddress _AssetAddress, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> { channelReferences })
        {
            AssetAddress = _AssetAddress;
        }
    }
}