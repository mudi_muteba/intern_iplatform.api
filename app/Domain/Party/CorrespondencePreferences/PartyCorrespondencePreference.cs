﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Party.CorrespondencePreferences
{
    public class PartyCorrespondencePreference : Entity, IEntityIncludeParty
    {
        public PartyCorrespondencePreference()
        {

        }

        public PartyCorrespondencePreference(CorrespondenceType correspondenceType, Party party)
        {
            this.Party = party;
            this.CorrespondenceType = correspondenceType;
        }

        public virtual CorrespondenceType CorrespondenceType { get; protected set; }
        public virtual bool Email { get; set; }
        public virtual bool Telephone { get; set; }
        public virtual bool SMS { get; set; }
        public virtual bool Post { get; set; }
        public virtual Party Party { get; set; }

        public virtual void Update(PartyCorrespondencePreferenceDto dto)
        {
            Mapper.Map(dto, this);
        }
    }
}
