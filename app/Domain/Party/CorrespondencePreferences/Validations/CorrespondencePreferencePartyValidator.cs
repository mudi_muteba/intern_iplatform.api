﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using ValidationMessages;
using Domain.Individuals;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;



namespace Domain.Party.CorrespondencePreferences.Validation
{
    public class CorrespondencePreferencePartyValidator : IValidateDto<EditCorrespondencePreferenceDto>
    {

        public CorrespondencePreferencePartyValidator(IProvideContext contextProvider, IRepository repository)
        {
        }

        public void Validate(EditCorrespondencePreferenceDto dto, ExecutionResult result)
        {
            ValidateParty((int)dto.PartyId, result);
        }

        public void ValidateParty(int PartyId, ExecutionResult result)
        {
            var party = Mapper.Map<int, Party>(PartyId);

            if(party == null)
                result.AddValidationMessage(CorrespondencePreferencesValidationMessages.PartyIdInvalid.AddParameters(new[] { PartyId.ToString() }));
        }

    }
}