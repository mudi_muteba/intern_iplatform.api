﻿using AutoMapper;
using Domain.Individuals;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using System.Collections.Generic;

namespace Domain.Party.PartyCorrespondencePreferences.Mapping
{
    public class PartyCorrespondencePreferenceMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PartyCorrespondencePreference, PartyCorrespondencePreferenceDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                ;
            Mapper.CreateMap<EditCorrespondencePreferenceDto, PartyCorrespondencePreference>();

            Mapper.CreateMap<List<PartyCorrespondencePreference>, ListResultDto<PartyCorrespondencePreferenceDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<PartyCorrespondencePreferenceDto, PartyCorrespondencePreference>()
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.Id, o => o.Ignore());

                ;
        }
    }
}