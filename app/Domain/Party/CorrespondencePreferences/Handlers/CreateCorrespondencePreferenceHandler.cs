﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;

namespace Domain.Party.CorrespondencePreferences.Handlers
{
    public class CreateCorrespondencePreferenceHandler : CreationDtoHandler<Party, EditCorrespondencePreferenceDto, int>
    {
        public CreateCorrespondencePreferenceHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void EntitySaved(Party entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Party HandleCreation(EditCorrespondencePreferenceDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Party>((int)dto.PartyId);

            party.RemoveCorrespondencePref(dto);

            foreach(var record in dto.CorrespondencePreferences)
            {
                party.AddCorrespondencePref(record);
            }

            result.Processed(party.Id);
            return party;
        }
    }
}