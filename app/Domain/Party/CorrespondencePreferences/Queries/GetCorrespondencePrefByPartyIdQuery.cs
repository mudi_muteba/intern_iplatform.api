﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.CorrespondencePreferences.Queries
{
    public class GetCorrespondencePrefByPartyIdQuery : BaseQuery<PartyCorrespondencePreference>
    {
        private int _partyId;

        public GetCorrespondencePrefByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PartyCorrespondencePreference>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    CorrespondenceReferenceAuthorisation.List,
                };
            }
        }

        public GetCorrespondencePrefByPartyIdQuery WithPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<PartyCorrespondencePreference> Execute(IQueryable<PartyCorrespondencePreference> query)
        {
            return Repository.GetAll<PartyCorrespondencePreference>().Where(a => a.Party.Id == _partyId);
        }
    }
}