﻿using Domain.Base.Repository.Customisation;
using Domain.Party.CorrespondencePreferences;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;
using NHibernate.Properties;

namespace Domain.Party.CorrespondencePreferences.Overrides
{
    public class PartyCorrespondencePreferenceOverride : IAutoMappingOverride<PartyCorrespondencePreference>
    {
        public void Override(AutoMapping<PartyCorrespondencePreference> mapping)
        {
            mapping.References(x => x.Party);
        }
    }
}
