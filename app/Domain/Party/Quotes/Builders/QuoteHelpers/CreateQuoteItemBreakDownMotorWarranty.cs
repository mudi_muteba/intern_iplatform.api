﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;

namespace Domain.Party.Quotes.Builders.QuoteHelpers
{
    public class CreateQuoteItemBreakDownMotorWarranty : CreateQuoteItemBreakDownBase
    {
        private string _motorWarranty;

        public CreateQuoteItemBreakDownMotorWarranty(ProposalHeader proposalHeader, ProposalDefinition definition)
            : base(proposalHeader, definition)
        {
        }

        public void BreakDown(RatingResultPolicyDto policy,
            QuoteItem quoteItem)
        {
            if (quoteItem.CoverDefinition.Cover.Id != MasterData.Covers.MotorWarranty.Id)
                return;

            _motorWarranty = GetAnswer(MasterData.Questions.ExtendedWarrantyVehicleCategory);
            if (string.IsNullOrEmpty(_motorWarranty)) //Motor
                return;

            Process(policy.ExternalRatingResult, quoteItem);

            PopulateBreakDown(quoteItem);
        }

        public void Process(string externalRatingRequest, QuoteItem quoteItem)
        {

            var motorWarrantyDescription = new QuestionAnswers().FirstOrDefault(a => a.Id == Int32.Parse(_motorWarranty));
            if (motorWarrantyDescription == null)
                return;
            
                AddMember("motor warranty", "0", "0", motorWarrantyDescription.Answer, "0", false, false, "MotorWarranty");
        }
    }
}