﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Xml.Linq;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Party.Quotes.Builders.QuoteHelpers
{
    public class CreateQuoteItemBreakDownBuildings : CreateQuoteItemBreakDownBase
    {
        public CreateQuoteItemBreakDownBuildings(ProposalHeader proposalHeader, ProposalDefinition definition)
            : base(proposalHeader, definition)
        {
        }

        public void BreakDown(RatingResultPolicyDto policy,
            QuoteItem quoteItem)
        {
            if (quoteItem.CoverDefinition.Cover.Id != MasterData.Covers.Building.Id)
                return;

            AddBuildingsVap(policy.ExternalRatingResult, quoteItem);

            PopulateBreakDown(quoteItem);
        }

        public void AddBuildingsVap(string externalRatingRequest, QuoteItem quoteItem)
        {
            var aigSubsidenceLandslip = GetAnswer(MasterData.Questions.AIGSubsidenceLandslip);
            var aigAccidentalDamage = GetAnswer(MasterData.Questions.AIGAccidentalDamage);
            var aigWaterPumpingMachinery = GetAnswer(MasterData.Questions.AIGWaterPumpingMachinery);
            var sumInsured = GetAnswer(MasterData.Questions.SumInsured);
            

            var root = XDocument.Parse(externalRatingRequest).Root;

            List<string> listNodes = new List<string>();
            ExpandoObject xmlContent = new ExpandoObject();
            ExpandoObjectHelper.Parse(xmlContent, root, listNodes);

            dynamic buildings = ((dynamic)xmlContent).QRaterAssemblyRs.QRaterRs.Policy.Buildings;
            buildings = GetQraterResponseFromList(buildings);

            Dictionary<string, string> funeralDictionary = new Dictionary<string, string>();

            foreach (KeyValuePair<string, object> pair in buildings)
                funeralDictionary.Add(pair.Key, pair.Value.ToString());


            var premiumTotal = getValueFromQRater(funeralDictionary, "PremiumTotal");
            var premiumRisk = getValueFromQRater(funeralDictionary, "PremiumRisk");
            var dDiscount = getValueFromQRater(funeralDictionary, "DiscretionaryDiscount");

            //Discretionary Discount
            if (quoteItem.Fees != null)
                quoteItem.DiscretionaryDiscount = Decimal.Parse((string.IsNullOrWhiteSpace(dDiscount) ? "0" : dDiscount), CultureInfo.InvariantCulture);

            if (string.IsNullOrWhiteSpace(premiumRisk))
                return;

            var sumValue = Decimal.Parse(premiumRisk, CultureInfo.InvariantCulture);

            AddMember("Standard ", "0", "0", sumInsured, sumValue.ToString(CultureInfo.InvariantCulture), false, false, "Standard");


            var keys = new List<Tuple<string, string, string>>
            {
                new Tuple<string,string,string>("PremiumSubsidenceAndLandslip","Subsidence And Landslip",aigSubsidenceLandslip),
                new Tuple<string,string,string>("PremiumAccidentalDamage","Accidental Damage",aigAccidentalDamage),
                new Tuple<string,string,string>("PremiumWaterMachinery","Water Pumping Machinery",aigWaterPumpingMachinery),

            };

            BuildDtoFromKeys(keys, funeralDictionary);

            var sasriaValue = quoteItem.Fees.Sasria > 0 ? "Yes" : "No";
            AddMember("Sasria", "0", "0", sasriaValue, quoteItem.Fees.Sasria.ToString(CultureInfo.InvariantCulture), false, false, "Sasria");


        }
    }
}