﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Xml.Linq;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Party.Quotes.Builders.QuoteHelpers
{
    public class CreateQuoteItemBreakDownContents : CreateQuoteItemBreakDownBase
    {
        public CreateQuoteItemBreakDownContents(ProposalHeader proposalHeader, ProposalDefinition definition)
            : base(proposalHeader, definition)
        {
        }

        public void BreakDown(RatingResultPolicyDto policy,
            QuoteItem quoteItem)
        {
            if (quoteItem.CoverDefinition.Cover.Id != MasterData.Covers.Contents.Id)
                return;

            Process(policy.ExternalRatingResult, quoteItem);

            PopulateBreakDown(quoteItem);
        }

        public void Process(string externalRatingRequest, QuoteItem quoteItem)
        {
            var aigExcludeTheftCover = GetAnswer(MasterData.Questions.AIGExcludeTheftCover);
            var aigHouseholdersSubsidenceLandslip = GetAnswer(MasterData.Questions.AIGHouseholdersSubsidenceLandslip);
            var aigAccidentalDamage = GetAnswer(MasterData.Questions.AIGAccidentalDamage);
            var sumInsured = GetAnswer(MasterData.Questions.SumInsured);


            var root = XDocument.Parse(externalRatingRequest).Root;

            List<string> listNodes = new List<string>();
            ExpandoObject xmlContent = new ExpandoObject();
            ExpandoObjectHelper.Parse(xmlContent, root, listNodes);

            dynamic contents = ((dynamic)xmlContent).QRaterAssemblyRs.QRaterRs.Policy.Contents;
            contents = GetQraterResponseFromList(contents);

            Dictionary<string, string> funeralDictionary = new Dictionary<string, string>();

            foreach (KeyValuePair<string, object> pair in contents)
                funeralDictionary.Add(pair.Key, pair.Value.ToString());

            var premiumTotal = getValueFromQRater(funeralDictionary, "PremiumTotal");
            var premiumRisk = getValueFromQRater(funeralDictionary, "PremiumRisk");
            var dDiscount = getValueFromQRater(funeralDictionary, "DiscretionaryDiscount");

            //Discretionary Discount
            quoteItem.DiscretionaryDiscount = Decimal.Parse((string.IsNullOrWhiteSpace(dDiscount) ? "0" : dDiscount), CultureInfo.InvariantCulture);

            if (string.IsNullOrWhiteSpace(premiumRisk))
                return;

            var sumValue = Decimal.Parse(premiumRisk, CultureInfo.InvariantCulture);

            if (aigExcludeTheftCover.ToLower() == "no")
                AddMember("Standard including theft", "0", "0", sumInsured, sumValue.ToString(CultureInfo.InvariantCulture), false, false, "StandardIncludingTheft");
            else
                AddMember("Standard excluding theft", "0", "0", sumInsured, sumValue.ToString(CultureInfo.InvariantCulture), false, false, "StandardExcludingTheft");


            var keys = new List<Tuple<string, string, string>>
            {
                new Tuple<string,string,string>("PremiumSubsidenceAndLandslip","Subsidence And Landslip",aigHouseholdersSubsidenceLandslip),
                new Tuple<string,string,string>("PremiumAccidentalDamage","Accidental Damage",aigAccidentalDamage),
            };

            BuildDtoFromKeys(keys, funeralDictionary);

            var sasriaValue = quoteItem.Fees.Sasria > 0 ? "Yes" : "No";
            AddMember("Sasria", "0", "0", sasriaValue, quoteItem.Fees.Sasria.ToString(CultureInfo.InvariantCulture), false, false, "Sasria");

        }
    }
}
