using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Xml.Linq;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Party.Quotes.Builders.QuoteHelpers
{
    public class CreateQuoteItemBreakDownMotorCycle : CreateQuoteItemBreakDownBase
    {
        public CreateQuoteItemBreakDownMotorCycle(ProposalHeader proposalHeader, ProposalDefinition definition)
            : base(proposalHeader, definition)
        {
        }

        public void BreakDown(RatingResultPolicyDto policy,
            QuoteItem quoteItem)
        {
            if (quoteItem.CoverDefinition.Cover.Id != MasterData.Covers.Motor.Id)
                return;

            var vehicleType = GetAnswer(MasterData.Questions.AIGMotorTypeOfVehicle);
            if (!vehicleType.ToLower().Equals("3344")) //MotorCycle
                return;

            Process(policy.ExternalRatingResult, quoteItem);

            PopulateBreakDown(quoteItem);

            var sasriaValue = quoteItem.Fees.Sasria > 0 ? "Yes" : "No";
            AddMember("Sasria", "0", "0", sasriaValue, quoteItem.Fees.Sasria.ToString(CultureInfo.InvariantCulture), false, false, "Sasria");

        }

        public void Process(string externalRatingRequest, QuoteItem quoteItem)
        {
            var vehicleType = GetAnswer(MasterData.Questions.AIGMotorTypeOfVehicle);
            if (!vehicleType.ToLower().Equals("3344")) //Motorcycle
                return;

            var aigCoverType = GetAnswer(MasterData.Questions.AIGCoverType);
            var aigValuationMethod = GetAnswer(MasterData.Questions.AIGValuationMethod);

            var root = XDocument.Parse(externalRatingRequest).Root;

            var listNodes = new List<string>();
            var xmlContent = new ExpandoObject();
            ExpandoObjectHelper.Parse(xmlContent, root, listNodes);

            dynamic motor = ((dynamic)xmlContent).QRaterAssemblyRs.QRaterRs.Policy.Motorcycle;

            motor = GetQraterResponseFromList(motor);

            var funeralDictionary = new Dictionary<string, string>();

            foreach (KeyValuePair<string, object> pair in motor)
                funeralDictionary.Add(pair.Key, pair.Value.ToString());
            var premium = getValueFromQRater(funeralDictionary, "PremiumRisk");
            var sumValue = Decimal.Parse(premium, CultureInfo.InvariantCulture);
            AddMember("Comprehensive", "0", "0", aigValuationMethod, sumValue.ToString(CultureInfo.InvariantCulture), false, false, "Comprehensive");

            var sasriaValue = quoteItem.Fees.Sasria > 0 ? "Yes" : "No";
            AddMember("Sasria", "0", "0", sasriaValue, quoteItem.Fees.Sasria.ToString(CultureInfo.InvariantCulture), false, false, "Sasria");
        }
    }
}