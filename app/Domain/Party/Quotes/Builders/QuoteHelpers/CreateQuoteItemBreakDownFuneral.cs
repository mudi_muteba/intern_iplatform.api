﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Xml.Linq;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Party.Quotes.Builders.QuoteHelpers
{
    public class CreateQuoteItemBreakDownFuneral : CreateQuoteItemBreakDownBase
    {
        private IRepository _repository;

        public CreateQuoteItemBreakDownFuneral(ProposalHeader proposalHeader, ProposalDefinition definition) : base(proposalHeader, definition)
        {
        }

        public CreateQuoteItemBreakDownFuneral WithRepository(IRepository repository)
        {
            _repository = repository;
            return this;
        }

        public void BreakDown(RatingResultPolicyDto policy,
            QuoteItem quoteItem)
        {
            if (!policy.ProductCode.Equals("VMSAFuneral"))
                return;

            var definition = GetDefinition(quoteItem);
            if (definition == null)
                return;
            AddMembers(policy.ExternalRatingResult);

            //TODO: try to find the members of the applicable QuestionDefinition
            var proposalDefinitionsIds = _proposalHeader.ProposalDefinitions.Select(a => a.Id).ToList();

            var members =
                _repository.GetAll<AdditionalMembers>()
                    .Where(a => proposalDefinitionsIds.Contains(a.ProposalDefinition.Id))
                    .ToList();

            members.ForEach(additionalMember =>
                quoteItem.AddBreakDown(new QuoteItemBreakDown(quoteItem
                    , string.Empty
                    , additionalMember.Id.ToString()
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , string.Format("{0} {1}", additionalMember.Initials, additionalMember.Surname)
                    , additionalMember.MemberRelationship.Name
                    , additionalMember.Id
                    , false
                    , false,"").MapXmlMember(Members)));

            //Add Main member

            // resolve main member name
            var policyHolderName = string.Empty;
            var firstOrDefault = _proposalHeader.ProposalDefinitions.FirstOrDefault();
            if (firstOrDefault != null)
                policyHolderName = firstOrDefault.Party.DisplayName;

            var mainMember = Members.FirstOrDefault(a => a.IsMainMember);
            if (mainMember != null)
                quoteItem.AddBreakDown(new QuoteItemBreakDown(quoteItem
                    , string.Empty
                    , mainMember.Reference
                    , mainMember.Risk
                    , mainMember.Markup
                    , ""
                    , mainMember.Total
                    , policyHolderName
                    , "Main Member"
                    , 0, false, true, "MainMember"));
        }

        public void AddMembers(string externalRatingRequest)
        {
            var root = XDocument.Parse(externalRatingRequest).Root;

            List<string> listNodes = new List<string>();
            ExpandoObject xmlContent = new ExpandoObject();
            ExpandoObjectHelper.Parse(xmlContent, root, listNodes);

            dynamic funeral = ((dynamic)xmlContent).QRaterAssemblyRs.QRaterRs.Policy.Funeral;

            Dictionary<string, string> funeralDictionary = new Dictionary<string, string>();

            foreach (KeyValuePair<string, object> pair in funeral)
                funeralDictionary.Add(pair.Key, pair.Value.ToString());


            for (int i = 0; i < 30; i++)
            {
                var reference = getValueFromQRater(funeralDictionary, string.Format("ExtendedFamilyMember{0}ReferenceOUT", i));
                var risk = getValueFromQRater(funeralDictionary, string.Format("PremiumExtendedFamily{0}Risk", i));
                var markup = getValueFromQRater(funeralDictionary, string.Format("PremiumExtendedFamily{0}Markup", i));
                var total = getValueFromQRater(funeralDictionary, string.Format("PremiumExtendedFamily{0}Total", i));
                AddMember(reference, risk, markup,"", total, false, false, string.Format("ExtendedFamilyMember{0}ReferenceOUT", i));

                reference = getValueFromQRater(funeralDictionary, string.Format("Child{0}ReferenceOUT", i));
                risk = getValueFromQRater(funeralDictionary, string.Format("Child{0}RiskPremium", i));
                markup = getValueFromQRater(funeralDictionary, string.Format("Child{0}MarkupPremium", i));
                total = getValueFromQRater(funeralDictionary, string.Format("PremiumChild{0}Total", i));
                AddMember(reference, risk, markup,"", total, true, false, string.Format("Child{0}ReferenceOUT", i));
            }

            var mainmemberReference = "mainMember";
            var mainmemberRisk = getValueFromQRater(funeralDictionary, "PremiumBasicRisk");
            var mainmemberMarkup = getValueFromQRater(funeralDictionary, "PremiumBasicMarkup");
            var mainmemberTotal = getValueFromQRater(funeralDictionary, "PremiumTotalBasic");
            AddMember(mainmemberReference, mainmemberRisk, mainmemberMarkup,"", mainmemberTotal, false, true, "MainMember");

        }
    }
}