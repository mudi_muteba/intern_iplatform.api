using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Party.Quotes.Builders.QuoteHelpers
{
    public class CreateQuoteItemBreakDownIdentityTheft : CreateQuoteItemBreakDownBase
    {
        public CreateQuoteItemBreakDownIdentityTheft(ProposalHeader proposalHeader, ProposalDefinition definition)
            : base(proposalHeader, definition)
        {
        }

        public void BreakDown(RatingResultPolicyDto policy,
            QuoteItem quoteItem)
        {
            if (quoteItem.CoverDefinition.Cover.Id != MasterData.Covers.IdentityTheft.Id)
                return;

            Process(policy.ExternalRatingResult, quoteItem);

            PopulateBreakDown(quoteItem);
        }

        public void Process(string externalRatingRequest, QuoteItem quoteItem)
        {

        }
    }
}