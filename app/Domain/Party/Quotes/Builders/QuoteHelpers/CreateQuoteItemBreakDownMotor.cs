﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Party.Quotes.Builders.QuoteHelpers
{
    public class CreateQuoteItemBreakDownMotor : CreateQuoteItemBreakDownBase
    {
        public CreateQuoteItemBreakDownMotor(ProposalHeader proposalHeader, ProposalDefinition definition)
            : base(proposalHeader, definition)
        {
        }

        public void BreakDown(RatingResultPolicyDto policy,
            QuoteItem quoteItem)
        {
            if (quoteItem.CoverDefinition.Cover.Id != MasterData.Covers.Motor.Id)
                return;

            var vehicleType = GetAnswer(MasterData.Questions.AIGMotorTypeOfVehicle);
            if (!vehicleType.ToLower().Equals("3340")) //Motor
                return;

            Process(policy.ExternalRatingResult, quoteItem);

            PopulateBreakDown(quoteItem);
        }


        public void Process(string externalRatingRequest, QuoteItem quoteItem)
        {

            var aigCreditShortfall = GetAnswer(MasterData.Questions.AIGCreditShortfall);
            var aigScratchAndDent = GetAnswer(MasterData.Questions.AIGScratchAndDent);
            var aigTyreAndRim = GetAnswer(MasterData.Questions.AIGTyreAndRim);
            var aigCarHire = GetAnswer(MasterData.Questions.AIGCarHire);
            var vehicleExtrasValue = GetAnswer(MasterData.Questions.VehicleExtrasValue);
            var aigCoverType = GetAnswer(MasterData.Questions.AIGCoverType);
            var aigValuationMethod = GetAnswer(MasterData.Questions.AIGValuationMethod);

  
            //
            //VIRGINTOTAL
            //VIRGINTPFTH

            var root = XDocument.Parse(externalRatingRequest).Root;

            var listNodes = new List<string>();
            var xmlContent = new ExpandoObject();
            ExpandoObjectHelper.Parse(xmlContent, root, listNodes);

            dynamic motor = ((dynamic)xmlContent).QRaterAssemblyRs.QRaterRs.Policy.MotorVehicle;

            motor = GetQraterResponseFromList(motor);

            var funeralDictionary = new Dictionary<string, string>();
            var optionsDictionary = new Dictionary<string, string>();

            foreach (KeyValuePair<string, object> pair in motor)
                funeralDictionary.Add(pair.Key, pair.Value.ToString());

            dynamic options = null;
            var isPropertyExist = false;
            try
            {
                var x = motor.Options.Option[0];
                isPropertyExist = true;

                    var keys = new List<Tuple<string, dynamic>>
                    {
                        new Tuple<string, dynamic>(motor.Options.Option[0].Option1Value, motor.Options.Option[0]),
                        new Tuple<string,dynamic>(motor.Options.Option[1].Option1Value,motor.Options.Option[1]),
                        new Tuple<string,dynamic>(motor.Options.Option[2].Option1Value,motor.Options.Option[2])
                    };


                if (quoteItem.Quote.Product.ProductCode == "VIRGINTOTAL")
                {
                    options = keys.First(a => a.Item1 == "TOTAL").Item2;
                    aigCoverType = "Total loss";

                }
                if (quoteItem.Quote.Product.ProductCode == "VIRGINTPFTH")
                {
                    options = keys.First(a => a.Item1 == "TPFTH").Item2;
                    aigCoverType = "Third Party, Fire And Theft";
                }

                if (options == null)
                {
                    options = keys.First(a => a.Item1 == "COMP").Item2;
                    aigCoverType = "Comprehensive";
                }


            }
            catch (Exception)
            {
                isPropertyExist = false;
            }

            if (isPropertyExist)
                foreach (KeyValuePair<string, object> pair in options)
                    optionsDictionary.Add(pair.Key, pair.Value.ToString());
            else
                optionsDictionary = funeralDictionary;

            var vapsTotal = GetVapTotals(optionsDictionary);
            var basicPremium = GetBasicPremium(optionsDictionary);
            var totalExtras = TotalExtras(optionsDictionary);

            var tariffPremiumTotalPremium = TariffPremiumTotalPremium(optionsDictionary);
            basicPremium = tariffPremiumTotalPremium - (vapsTotal + totalExtras);
            quoteItem.Fees.Premium = basicPremium + totalExtras; // when not comprehensive

            if (aigCoverType.ToLower().Equals("comprehensive"))
            {
                quoteItem.Fees.Premium = basicPremium + totalExtras + vapsTotal;
            }



            AddMember(aigCoverType, "0", "0", aigValuationMethod,
                (basicPremium).ToString(CultureInfo.InvariantCulture), false, false, "aigCoverType");

            if (aigCoverType.ToLower().Equals("comprehensive"))
                GetVapBreakDown(aigCreditShortfall, aigScratchAndDent, aigTyreAndRim, aigCarHire, optionsDictionary);

            //Add Extras
            if (totalExtras > 0)
                AddMember("Extras", "0", "0", vehicleExtrasValue, totalExtras.ToString(CultureInfo.InvariantCulture),
                    false, false, "Extras");
            //Add Sasria
            var sasriaValue = quoteItem.Fees.Sasria > 0 ? "Yes" : "No";
            AddMember("Sasria", "0", "0", sasriaValue, quoteItem.Fees.Sasria.ToString(CultureInfo.InvariantCulture),
                false, false, "Sasria");
        }

        private void GetVapBreakDown(string aigCreditShortfall, string aigScratchAndDent, string aigTyreAndRim,
            string aigCarHire, Dictionary<string, string> funeralDictionary)
        {

            var keys = new List<Tuple<string, string, string>>
            {
                new Tuple<string, string, string>("BenefitsExtentionsCreditshortfallextension", "Credit Shortfall",
                    aigCreditShortfall),
                new Tuple<string, string, string>("ValueAddOnsScratchanddent", "Scratch and Dent", aigScratchAndDent),
                new Tuple<string, string, string>("ValueAddOnsTyreandRim", "Tyre and Rim", aigTyreAndRim),
                new Tuple<string, string, string>("ValueAddOnsCarHire", "Car Hire", aigCarHire),

                new Tuple<string, string, string>("BenefitsExtentionsEmergencytravelexpenses", "Emergency travel expenses", "Yes"),
                new Tuple<string, string, string>("BasicFeaturesInspection", "Features Inspection", "Yes"),
            };


            BuildDtoFromKeys(keys, funeralDictionary);
        }

        private decimal TotalExtras(Dictionary<string, string> funeralDictionary)
        {
            var extrasKeys = new List<string>
            {
                "AccessoriesSoundEquipment",
                "AccessoriesMagWheels",
                "AccessoriesSunRoof",
                "AccessoriesXenonLights",
                "AccessoriesBullTowbars",
                "AccessoriesBodyKit",
                "AccessoriesAntiSmashandGrab",
                "AccessoriesOther"
            };

            var totalExtras = SumQRaterValues(extrasKeys, funeralDictionary);
            return totalExtras;
        }

        private decimal GetVapTotals(Dictionary<string, string> funeralDictionary)
        {
            var vapKeys = new List<string>
            {
                "BenefitsExtentionsEmergencytravelexpenses",
                "BenefitsExtentionsCreditshortfallextension",
                "BasicFeaturesInspection",
                "ValueAddOnsScratchanddent",
                "ValueAddOnsTyreandRim",
                "ValueAddOnsCarHire"
            };

            return SumQRaterValues(vapKeys, funeralDictionary);
        }

        private decimal GetBasicPremium(Dictionary<string, string> funeralDictionary)
        {
            var vapKeys = new List<string>
            {
                "MainPerilsAccident",
                "MainPerilsTheft",
                "MainPerilsHijack",
                "MainPerilsWindscreen",
                "MainPerilsWeatherandElementsFloodOther",
                "MainPerilsWeatherandElementsHail",
                "MainPerilsFireandExplosion",
                "MainPerilsThirdPartyLiabilityPropertyDamage",
                "ValueAddOnsTrackingDevice",
                "CashBackRiskPremium",
                "CashBackBasicFeatures",
                "CashBackValueAddOns"
            };

            return SumQRaterValues(vapKeys, funeralDictionary);
        }

        private decimal TariffPremiumTotalPremium(Dictionary<string, string> funeralDictionary)
        {
            var keys = new List<string>
            {
                "TariffPremiumTotalPremium",
            };

            var total = SumQRaterValues(keys, funeralDictionary);
            return total;
        } 
   
    }
}