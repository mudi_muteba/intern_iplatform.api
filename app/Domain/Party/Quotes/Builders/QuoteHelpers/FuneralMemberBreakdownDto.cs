﻿namespace Domain.Party.Quotes.Builders.QuoteHelpers
{
    public class FuneralMemberBreakdownDto
    {
        public string Reference { get; set; }
        public string Risk { get; set; }
        public string Markup { get; set; }
        public string QuestionAnswer { get; set; }
        public string Total { get; set; }
        public bool IsChild { get; set; }
        public bool IsMainMember { get; set; }
        public string ExternalId { get; set; }
    }
}