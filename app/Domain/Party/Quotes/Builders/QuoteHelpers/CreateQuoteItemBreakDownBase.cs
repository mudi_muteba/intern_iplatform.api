﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using MasterData;

namespace Domain.Party.Quotes.Builders.QuoteHelpers
{
    public abstract class CreateQuoteItemBreakDownBase
    {
        protected ProposalDefinition _definition;
        protected ProposalHeader _proposalHeader;
        protected List<FuneralMemberBreakdownDto> Members;

        protected CreateQuoteItemBreakDownBase(ProposalHeader proposalHeader, ProposalDefinition definition)
        {
            _proposalHeader = proposalHeader;
            _definition = definition;
            Members = new List<FuneralMemberBreakdownDto>();
        }

        protected ProposalDefinition GetDefinition(QuoteItem quoteItem)
        {
            var proposalDefinition =
                _proposalHeader.ProposalDefinitions.FirstOrDefault(a => a.Asset.AssetNo == quoteItem.Asset.AssetNumber
                                                                        &&
                                                                        a.Cover.Id == quoteItem.CoverDefinition.Cover.Id);
            return proposalDefinition;
        }

        protected void PopulateBreakDown(QuoteItem quoteItem)
        {
            Members.ForEach(vap =>
                quoteItem.AddBreakDown(new QuoteItemBreakDown(
                    quoteItem
                    , vap.Reference
                    , vap.Reference
                    , "0"
                    , "0"
                    , vap.QuestionAnswer
                    , vap.Total
                    , vap.Reference
                    , ""
                    , 0
                    , false
                    , false,vap.ExternalId, vap.Total)));


            //Add the main cover Item to the breakdown
            var assetDescription = string.Format("{0} : {1}", quoteItem.CoverDefinition.Cover.Name, quoteItem.Asset.Description);
            var coverItemTotal = Math.Round(quoteItem.Fees.Premium + quoteItem.Fees.Sasria,2) ;
                quoteItem.AddBreakDown(new QuoteItemBreakDown(
                    quoteItem
                    , assetDescription
                    , ""
                    , "0"
                    , "0"
                    , ""
                    , coverItemTotal.ToString(CultureInfo.InvariantCulture)
                    , ""
                    , ""
                    , 0
                    , false
                    , false,"CoverItem", coverItemTotal.ToString(CultureInfo.InvariantCulture)));



        }

        protected string GetAnswer(Question question)
        {
             var proposalQuestion = _definition.ProposalQuestionAnswers.FirstOrDefault(
                    a => a.QuestionDefinition.Question.Id == question.Id);

            if (proposalQuestion == null)
                return String.Empty;

            if (question.QuestionType.Id == QuestionTypes.Checkbox.Id)
            {
                return proposalQuestion.Answer.ToLower() == "true" ? "Yes" : "No";
            }

            if (question.QuestionType.Id == QuestionTypes.Dropdown.Id)
            {
                int optionId = 0;
                var success = Int32.TryParse(proposalQuestion.Answer, out optionId);
                if (!success)
                    return "No";

                var questionAnswer = new QuestionAnswers().FirstOrDefault(a => a.Id == optionId);

                if (questionAnswer == null)
                    return "No";

                if (question.Id == MasterData.Questions.AIGCarHire.Id)
                    return questionAnswer.Answer == "30" ? "Yes" : "No";
                if (question.Id == MasterData.Questions.AIGWaterPumpingMachinery.Id
                    ||
                    question.Id == MasterData.Questions.AIGAccidentalDamage.Id
                    )
                    return String.Format("Yes, {0}", questionAnswer.Answer);

                if (question.Id == MasterData.Questions.AIGCoverType.Id
                    ||
                    question.Id == MasterData.Questions.AIGValuationMethod.Id
                    )
                    return questionAnswer.Answer;
            }

            //If text answer
            return proposalQuestion.Answer;
        }

        protected void BuildDtoFromKeys(List<Tuple<string, string, string>> keys, Dictionary<string, string> dictionary)
        {
            foreach (var key in keys)
            {
                var total = getValueFromQRater(dictionary, key.Item1);

                if (total.Equals("0") || total.Equals("0.0") || total.Equals("0.00") || total.Equals(""))
                    continue;

                AddMember(key.Item2, "0", "0", key.Item3, total, false, false, key.Item1);
            }
        }

        protected void AddMember(string reference, string risk, string markup, string questionAnswer, string total, bool isChild, bool isMainMember,string externalId)
        {
            if (string.IsNullOrWhiteSpace(reference))
                return;

            Members.Add(new FuneralMemberBreakdownDto
            {
                Reference = reference,
                Markup = markup,
                Risk = risk,
                QuestionAnswer = questionAnswer,
                Total = total,
                IsChild = isChild,
                IsMainMember = isMainMember,
                ExternalId = externalId
            });
        }

        protected Func<Dictionary<string, string>, string, string> getValueFromQRater = (dictionary, key) =>
        {
            if (dictionary.ContainsKey(key))
                return dictionary[key];
            return String.Empty;
        };
        protected dynamic GetQraterResponseFromList(dynamic obj)
        {
            if (obj is IList)
                foreach (var item in obj)
                    if (item.Id == _definition.Id.ToString())
                    {
                        obj = item;
                        break;
                    }
            return obj;
        }

        protected decimal SumQRaterValues(List<string> extrasKeys, Dictionary<string, string> funeralDictionary)
        {
            var sum = extrasKeys.Sum(y =>
            {
                var val = getValueFromQRater(funeralDictionary, y);
                Decimal x;
                var success = Decimal.TryParse(val, NumberStyles.Any, CultureInfo.InvariantCulture, out x);
                if (!success)
                    return 0;
                return x;
            });
            return sum;
        }
    }

}