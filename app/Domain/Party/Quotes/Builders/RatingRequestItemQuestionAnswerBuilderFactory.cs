﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Party.ProposalDefinitions;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace Domain.Party.Quotes.Builders
{
    internal class RatingRequestItemQuestionAnswerBuilderFactory
    {
        private readonly Dictionary<Func<ProposalQuestionAnswer, bool>, Func<ProposalQuestionAnswer, RatingRequestItemQuestionAnswerBuilder>> mapping =
            new Dictionary<Func<ProposalQuestionAnswer, bool>, Func<ProposalQuestionAnswer, RatingRequestItemQuestionAnswerBuilder>>();

        public RatingRequestItemQuestionAnswerBuilderFactory()
        {
            mapping.Add(qa => qa.QuestionType.Id == QuestionTypes.Address.Id, qa => new AddressQuestionTypeBuilder(qa));
            mapping.Add(qa => qa.QuestionType.Id == QuestionTypes.Asset.Id, qa => new AssetQuestionTypeBuilder(qa));

            mapping.Add(qa => qa.QuestionType.Id == QuestionTypes.Checkbox.Id, qa => new DefaultQuestionTypeBuilder(qa));
            mapping.Add(qa => qa.QuestionType.Id == QuestionTypes.Textbox.Id, qa => new DefaultQuestionTypeBuilder(qa));
            mapping.Add(qa => qa.QuestionType.Id == QuestionTypes.Date.Id, qa => new DefaultQuestionTypeBuilder(qa));

            mapping.Add(qa => qa.QuestionType.Id == QuestionTypes.Dropdown.Id &&
                    qa.QuestionDefinition.Question.Id == Questions.YearOfManufacture.Id, qa => new YearOfManufactureQuestionTypeBuilder(qa));

            mapping.Add(qa => qa.QuestionType.Id == QuestionTypes.Dropdown.Id, qa => new DropdownQuestionTypeBuilder(qa));
        }

        public RatingRequestItemAnswerDto Create(ProposalQuestionAnswer questionAnswer)
        {
            var match = mapping.FirstOrDefault(q => q.Key(questionAnswer));

            if (match.Value == null)
                throw new Exception(string.Format("No question reader for question type {0}", questionAnswer.QuestionType.Id));

            return match.Value(questionAnswer)
                .Build();
        }
    }

    internal class YearOfManufactureQuestionTypeBuilder : RatingRequestItemQuestionAnswerBuilder
    {
        public YearOfManufactureQuestionTypeBuilder(ProposalQuestionAnswer proposalQuestionAnswer)
            : base(proposalQuestionAnswer)
        {
        }

        protected override void InternalBuild(RatingRequestItemAnswerDto questionAnswer)
        {
            questionAnswer.QuestionAnswer = proposalQuestionAnswer.Answer;
            questionAnswer.Question.QuestionType = QuestionTypes.Textbox;
        }
    }

    internal class DropdownQuestionTypeBuilder : RatingRequestItemQuestionAnswerBuilder
    {
        public DropdownQuestionTypeBuilder(ProposalQuestionAnswer proposalQuestionAnswer)
            : base(proposalQuestionAnswer)
        {
        }

        protected override void InternalBuild(RatingRequestItemAnswerDto questionAnswer)
        {
            int selectedId;
            if (!int.TryParse(proposalQuestionAnswer.Answer, out selectedId))
                return;

            var possibleAnswers = GetPossibleAnswers();

            questionAnswer.QuestionAnswer = possibleAnswers.FirstOrDefault(pa => pa.Id == selectedId);
        }

        private IEnumerable<QuestionAnswer> GetPossibleAnswers()
        {
            Func<QuestionAnswer, bool> matchOnQuestion = qa => qa.Question.Id == proposalQuestionAnswer.QuestionDefinition.Question.Id;
            return new QuestionAnswers().Where(matchOnQuestion);
        }
    }

    internal class AssetQuestionTypeBuilder : RatingRequestItemQuestionAnswerBuilder
    {
        public AssetQuestionTypeBuilder(ProposalQuestionAnswer proposalQuestionAnswer)
            : base(proposalQuestionAnswer)
        {
        }

        protected override void InternalBuild(RatingRequestItemAnswerDto questionAnswer)
        {
            questionAnswer.QuestionAnswer = proposalQuestionAnswer.Answer;
        }
    }

    internal class DefaultQuestionTypeBuilder : RatingRequestItemQuestionAnswerBuilder
    {
        public DefaultQuestionTypeBuilder(ProposalQuestionAnswer proposalQuestionAnswer)
            : base(proposalQuestionAnswer)
        {
        }

        protected override void InternalBuild(RatingRequestItemAnswerDto questionAnswer)
        {
            questionAnswer.QuestionAnswer = proposalQuestionAnswer.Answer;
        }
    }

    internal class AddressQuestionTypeBuilder : RatingRequestItemQuestionAnswerBuilder
    {
        public AddressQuestionTypeBuilder(ProposalQuestionAnswer proposalQuestionAnswer)
            : base(proposalQuestionAnswer)
        {
        }

        protected override void InternalBuild(RatingRequestItemAnswerDto questionAnswer)
        {
            questionAnswer.QuestionAnswer = proposalQuestionAnswer.Answer;
        }
    }

    internal abstract class RatingRequestItemQuestionAnswerBuilder
    {
        protected readonly ProposalQuestionAnswer proposalQuestionAnswer;

        protected RatingRequestItemQuestionAnswerBuilder(ProposalQuestionAnswer proposalQuestionAnswer)
        {
            this.proposalQuestionAnswer = proposalQuestionAnswer;
        }

        public RatingRequestItemAnswerDto Build()
        {
            var questionAnswer = new RatingRequestItemAnswerDto()
            {
                Question = proposalQuestionAnswer.QuestionDefinition.Question
            };

            InternalBuild(questionAnswer);

            return questionAnswer;
        }

        protected abstract void InternalBuild(RatingRequestItemAnswerDto questionAnswer);
    }
}