﻿using System;
using System.Linq;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using Domain.Products;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using NHibernate.Proxy;
using Domain.QuestionDefinitions;

namespace Domain.Party.Quotes.Builders
{
    public static class QuoteHeaderBuilder
    {
        private static SystemChannels _systemChannels;

        public static QuoteHeader GetQuoteHeader(CreateQuoteDto dto, IExecutionPlan executionPlan, IRepository repository, ExecutionResult result, SystemChannels systemChannels,int activeChannelId)
        {
            _systemChannels = systemChannels;

            //delete all previous quotes 
            var oldHeaders = repository.GetAll<QuoteHeader>().Where(a => a.Proposal.Id == dto.ProposalHeaderId && a.IsRerated == false).ToList();
            oldHeaders.ForEach(a => a.Rerate());

            var proposal = repository.GetById<ProposalHeader>(dto.ProposalHeaderId);

            if (proposal == null)
                throw new Exception(string.Format("Proposal header for id {0} cannot be found", dto.ProposalHeaderId));

            //Check if the quoteDate has expired then set to now
            var proposalDefinition = proposal.ProposalDefinitions.FirstOrDefault();
            var productQuoteExpiration = proposalDefinition != null ? proposalDefinition.Product.QuoteExpiration : 0;
            proposal.RatingDate = CheckRatingDate(proposal.RatingDate, productQuoteExpiration);

            var channel = repository.GetById<Channel>(activeChannelId > 0 ? activeChannelId : proposal.Party.Channel.Id); //ActiveChannelId

            var ratingRequest = new RatingRequestDtoBuilder(repository).Build(proposal, dto.Source, proposal.RatingDate, channel);

            if (channel == null || channel.SystemId == Guid.Empty)
                throw new Exception(
                    string.Format(
                        "There is no channel system id associated with ID {0} to create a quote for proposal header with ID {1}",
                        dto.ChannelId, dto.ProposalHeaderId));

            ratingRequest.RatingContext.ChannelId = channel.SystemId;

            var ratingResult = executionPlan.Execute<RatingRequestDto, RatingResultDto>(ratingRequest);
            ratingResult.AllErrorDTOMessages.ForEach(result.AddError);
            proposal.MarkAsQuoted();
            //this is done here cause not casting in builder
            //not possible to cast a NHibernate proxy base class to instance of derived class
            AddPersons(proposal.Party, ratingRequest, repository);

            if (ratingResult.Response == null)
                return null;

            var quoteHeader = GenerateQuote(ratingRequest.Id, proposal, ratingResult.Response, activeChannelId, repository);

            return quoteHeader;
        }

        public static QuoteHeader GenerateQuote(Guid requestId, ProposalHeader proposal, RatingResultDto ratings, int channelId,
            IRepository repository)
        {
            var products = ratings.Policies.Select(p => p.ProductCode).ToList();


            var workingProducts = repository.GetAll<Product>()
                .Where(x => !x.IsDeleted && products.Contains(x.ProductCode))
                .ToList();

            var mapvapSettings = repository.GetAll<MapVapQuestionDefinition>()
                .Where(x => !x.IsDeleted && 
                    products.Contains(x.Product.ProductCode) && 
                    x.Channel.Id == channelId && 
                    x.Enabled)
                .ToList();

            var builder = new QuoteBuilder(workingProducts, repository, _systemChannels, mapvapSettings);

            var header = builder.Build(requestId, proposal, ratings);

            return header;
        }

        private static void AddPersons(Party party, RatingRequestDto request, IRepository repository)
        {
            var individual = new Individual();

            var partyProxy = party as INHibernateProxy;
            if (partyProxy != null)
            {
                var result = partyProxy.HibernateLazyInitializer.GetImplementation();

                if (result.GetType().Name == "Contact")

                    individual = repository.GetById<Individual>(party.Id) ?? new Individual();
                else
                    individual = repository.UnProxy<Individual>(party) ?? new Individual();
            }

            foreach (var person in request.Policy.Persons)
            {
                person.PolicyHolder = true;
                person.FirstName = individual.FirstName;
                person.Surname = individual.Surname;
                person.DateOfBirth = individual.DateOfBirth;
                person.IdNumber = individual.IdentityNo;
                person.MaritalStatus = individual.MaritalStatus ?? MaritalStatuses.Unknown;
                person.Language = individual.Language ?? Languages.English;
                person.Initials = !string.IsNullOrEmpty(individual.FirstName)
                    ? individual.FirstName.Substring(0, 1)
                    : string.Empty;
                person.PhoneCell = individual.ContactDetail.Cell;
                person.PhoneDirect = individual.ContactDetail.Direct;
                person.PhoneFax = individual.ContactDetail.Fax;
                person.PhoneWork = individual.ContactDetail.Work;
            }
        }


        private static DateTime CheckRatingDate(DateTime? ratingDate, int daysTillExpirations)
        {
            var tmpRatingDate = ratingDate ?? DateTime.UtcNow;

            if (daysTillExpirations == 0)
                return tmpRatingDate;

            var dateDifference = (DateTime.UtcNow - tmpRatingDate).TotalDays;
            if (dateDifference > daysTillExpirations)
                return DateTime.UtcNow;

            return tmpRatingDate;
        }
    }
}