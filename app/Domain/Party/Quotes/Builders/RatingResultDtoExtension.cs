﻿using iPlatform.Api.DTOs.Ratings.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Party.Quotes.Builders
{
    public static class RatingResultDtoExtension
    {
        public static List<string> GetErrors(this RatingResultDto dto)
        {
            var errors = new List<string>();

            dto.Policies.ForEach(x => x.Items.ForEach(i => errors.AddRange(i.State.Errors)));

            return errors;
        }

        public static List<string> GetWarnings(this RatingResultDto dto)
        {
            var warnings = new List<string>();

            dto.Policies.ForEach(x => x.Items.ForEach(i => warnings.AddRange(i.State.Warnings)));

            return warnings;
        }
    }
}
