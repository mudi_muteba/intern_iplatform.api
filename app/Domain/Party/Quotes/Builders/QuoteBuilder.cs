﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Castle.Core.Internal;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using Domain.Party.Quotes.Builders.QuoteHelpers;
using Domain.Products;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using Domain.QuestionDefinitions;
using NHibernate.Mapping.ByCode;
using Domain.Party.Assets;

namespace Domain.Party.Quotes.Builders
{
    internal class QuoteBuilder
    {
        private readonly List<Product> products;
        private static readonly ILog log = LogManager.GetLogger<QuoteBuilder>();
        private readonly IRepository _repository;
        private readonly SystemChannels _systemChannels;
        private ProposalHeader _proposalHeader;
        private readonly List<MapVapQuestionDefinition> _mapvapSettings;
        public QuoteBuilder(List<Product> products, IRepository repository, SystemChannels systemChannels, List<MapVapQuestionDefinition> mapvapSettings)
        {
            this.products = products ?? new List<Product>();
            _repository = repository;
            _systemChannels = systemChannels;
            _mapvapSettings = mapvapSettings;
        }

        public QuoteHeader Build(Guid requestId, ProposalHeader proposal, RatingResultDto ratings)
        {
            _proposalHeader = proposal;
            var header = CreateQuoteHeader(proposal, requestId, ratings);

            var coversDefinitions = _repository.GetAll<CoverDefinition>().ToList();
            var reader = new RatingResultDtoQuoteReader(ratings);
            var policies = reader.GetValidPolicies(products);
            foreach (var policy in policies)
            {
                var currentProduct = products.FirstOrDefault(p => p.ProductCode.Equals(policy.ProductCode, StringComparison.InvariantCultureIgnoreCase));
                var quote = CreateQuote(header, currentProduct, policy);
                var items = reader.GetValidItems(currentProduct, policy.Items, coversDefinitions);

                foreach (var item in items)
                {

                    var percent = 0m;
                    var discount = 0m;

                    var quoteItem = CreateQuoteItem(quote, item.Item2, item.Item1, discount, percent);
                    var definition = GetDefinition(quoteItem);

                    if (item.Item1.IsVap)
                    {
                        GetVapQuoteItem(quote, item.Item1, quoteItem);
                        continue;
                    }

                    if (_systemChannels.IsAllowed(_proposalHeader.Party.Channel.Id, new[] { "AIG" }))
                    {
                        new CreateQuoteItemBreakDownFuneral(proposal, definition).WithRepository(_repository).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownMotor(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownBuildings(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownContents(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownTrailer(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownCaravan(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownMotorCycle(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownAllRisk(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownPersonalLegalLiability(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownIdentityTheft(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownDisasterCash(proposal, definition).BreakDown(policy, quoteItem);
                        new CreateQuoteItemBreakDownAigAssist(proposal, definition).BreakDown(policy, quoteItem);
                    }

                    if (_systemChannels.IsAllowed(_proposalHeader.Party.Channel.Id, new[] { "TELESURE", "IP" }))
                    {
                        new CreateQuoteItemBreakDownMotorWarranty(proposal, definition).BreakDown(policy, quoteItem);
                    }

                    quote.AddItem(quoteItem);

                    // IPL-624 Automatic Vaps must do it here.
                    IncludeVapQuoteItems(quote, definition, quoteItem);
                }

                header.AddQuote(quote);
            }

            return header;
        }


        private static QuoteHeader CreateQuoteHeader(ProposalHeader proposal, Guid requestId, RatingResultDto ratings)
        {
            var header = new QuoteHeader(requestId.ToString(), proposal, ratings.State.Errors, ratings.State.Warnings);
            return header;
        }

        private QuoteItem CreateQuoteItem(Quote quote, CoverDefinition currentCover,
            RatingResultItemDto item, decimal discount, decimal percent)
        {
            if (item.AssetNo < 1)
            {
                log.ErrorFormat("internal asset number not set Proposal Header: {0}, InsurerReference: {1}", _proposalHeader.Id,
                    quote.InsurerReference);
            }


            //retrieve the item description
            var proposalItem = _proposalHeader.ProposalDefinitions.FirstOrDefault(x => x.Asset != null && x.Id == item.AssetNo);

            if (proposalItem == null)
                proposalItem = _proposalHeader.ProposalDefinitions.FirstOrDefault(x => x.Asset != null);

            return new QuoteItem(quote, item.SumInsuredDescription, proposalItem == null ? string.Empty : proposalItem.Description,
                currentCover,
                new QuoteItemAsset(item.AssetNo, item.SumInsured, proposalItem == null ? string.Empty : proposalItem.Asset.Description),
                new QuoteItemFees(item.Premium, item.Premium, item.Sasria, item.SasriaCalulated,
                    item.SasriaShortfall, discount, percent, item.DiscountedDifference),
                new QuoteItemExcess(item.Excess.Calculated, item.Excess.Basic, item.Excess.Description,
                    item.Premium > 0 ? _proposalHeader.VoluntaryExcess(item.AssetNo) : 0.00m),
                item.State.Errors,
                item.State.Warnings,
                 item.IsVap,
                 item.VapQuestionId, 
                 item.Clauses);
        }

        private static Quote CreateQuote(QuoteHeader quoteHeader, Product currentProduct, RatingResultPolicyDto policy)
        {
            var xmlDoc = new XmlDocument();

            if (!string.IsNullOrEmpty(policy.ExternalRatingResult))
                xmlDoc.LoadXml(policy.ExternalRatingResult);

            var quote = new Quote(quoteHeader, currentProduct, policy.Fees, policy.InsurerReference,
                policy.ITCScore,
                policy.ExtraITCScore, policy.State.Errors, policy.State.Warnings)
            {
                ExternalRatingResult = xmlDoc.OuterXml,
                ExternalRatingRequest = policy.ExternalRatingRequest == null ? string.Empty : policy.ExternalRatingRequest
            };

            return quote;
        }
        private ProposalDefinition GetDefinition(QuoteItem quoteItem)
        {
            var proposalDefinition =
                _proposalHeader.ProposalDefinitions.FirstOrDefault(a => a.Cover.Id == quoteItem.CoverDefinition.Cover.Id);
            return proposalDefinition;
        }

        //Add Quote Item Based on MapVapQuestionDefinition
        private void IncludeVapQuoteItems(Quote quote, ProposalDefinition definition, QuoteItem quoteItem)
        {
            var vapitems = _mapvapSettings.Where(x => x.Product.Id == quote.Product.Id).ToList();

            if (definition != null)
            {
                var questionAnswers = definition.ProposalQuestionAnswers
                           .Where(p => p.QuestionDefinition.Question.QuestionGroup == QuestionGroups.Valueaddedproducts)
                           .ToList();

                questionAnswers.Where(qa => String.Equals(qa.Answer, "true", StringComparison.InvariantCultureIgnoreCase))
                    .ForEach(qa =>
                    {
                        MapVapQuestionDefinition vap = vapitems.FirstOrDefault(mvqd => mvqd.QuestionDefinition.Question.Id == qa.QuestionDefinition.Question.Id);

                        if (vap != null && //Ensure we have a VAP
                            ((vap.PerSectionVap && !quote.Items.Any(i => i.MapVapQuestionDefinitionId == vap.Id && i.ParentCoverId == definition.Cover.Id)) || //Only add the VAP if the VAP is per section and has not been added to the list yet
                             !vap.PerSectionVap)) //Add the VAP if it is per item
                        {

                            QuoteItem quoteitem = new QuoteItem()
                            {
                                Quote = quote,
                                CoverDefinition = vap.QuestionDefinition.CoverDefinition,
                                Description = vap.QuestionDefinition.DisplayName,
                                Fees = new QuoteItemFees
                                {
                                    Premium = vap.Premium
                                },
                                Excess = new QuoteItemExcess(),
                                Asset = new QuoteItemAsset(),
                                IsVap = true,
                                MapVapQuestionDefinitionId = vap.Id,
                                ParentCoverId = definition.Cover.Id,
                            };

                            quoteitem.QuoteItemState = new QuoteItemState(quoteitem);
                            quote.AddItem(quoteitem);
                        }
                    });
            }
        }

        private void GetVapQuoteItem(Quote quote, RatingResultItemDto item, QuoteItem quoteItem)
        {

            QuoteItem quoteitem2 = new QuoteItem()
            {
                Quote = quote,
                CoverDefinition = quoteItem.CoverDefinition,
                Description = string.Format("{0} {1}", quoteItem.Description, item.Description),
                Fees = new QuoteItemFees
                {
                    Premium = item.Premium
                },
                Excess = new QuoteItemExcess(),
                Asset = quoteItem.Asset,
                IsVap = true,
                ParentCoverId = quoteItem.CoverDefinition.Cover.Id,
                VapQuestionId = item.VapQuestionId,
                SumInsuredDescription = quoteItem.SumInsuredDescription
            };
            quoteitem2.QuoteItemState = new QuoteItemState(quoteitem2);
            quote.AddItem(quoteitem2);
        }
    }
}
