﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.Logging;
using Domain.Admin;
using Domain.Individuals;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using NHibernate.Proxy;
using Domain.Base.Repository;
using Domain.Party.Assets;
using Domain.Party.Relationships;
using Domain.HistoryLoss.Home;
using Domain.HistoryLoss.LossHistory;
using Domain.HistoryLoss.Motor;
using Domain.OverrideRatingQuestion;
using Domain.Party.Contacts;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using Domain.Components.ComponentDefinitions;
using Domain.Components.ComponentQuestionAnswers;
using Domain.Components.ComponentQuestionDefinitions;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;

namespace Domain.Party.Quotes.Builders
{
    public class RatingRequestDtoBuilder
    {
        public RatingRequestDtoBuilder(IRepository repository)
        {
            this.m_Repository = repository;
        }
        
        private readonly IRepository m_Repository;
        protected static readonly ILog Log = LogManager.GetLogger<RatingRequestDtoBuilder>();
        private static readonly ILog log = LogManager.GetLogger<RatingRequestDtoBuilder>();

        public IEnumerable<RatingRequestPersonDto> CreatePersons(Party party)
        {
            var person = new RatingRequestPersonDto { PolicyHolder = true };

            if (party.PartyType == PartyTypes.Individual)
            {
                var individual = party as Individual;

                if (individual == null)
                {
                    var personIndividual = party as INHibernateProxy;
                    if (personIndividual != null)
                        individual = personIndividual.HibernateLazyInitializer.GetImplementation() as Individual;
                }

                person.FirstName = individual.FirstName;
                person.Surname = individual.Surname;
                person.DateOfBirth = individual.DateOfBirth;
                person.IdNumber = individual.IdentityNo;
                person.MaritalStatus = individual.MaritalStatus ?? MaritalStatuses.Unknown;
                person.Language = individual.Language ?? Languages.English;
                person.Initials = individual.FirstName.Length > 0 ? individual.FirstName.Substring(0, 1) : string.Empty;
                person.PhoneCell = individual.ContactDetail.Cell;
                person.PhoneDirect = individual.ContactDetail.Direct;
                person.PhoneFax = individual.ContactDetail.Fax;
                person.PhoneWork = individual.ContactDetail.Work;
                person.PhoneHome = individual.ContactDetail.Home;
                person.Judgement = individual.AnyJudgements;
                person.JudgementReason = individual.AnyJudgementsReason;
                person.SequestratedReason = individual.BeenSequestratedOrLiquidatedReason;
                person.Sequestrated = individual.BeenSequestratedOrLiquidated;
                person.Administration = individual.UnderAdministrationOrDebtReview;
                person.AdministrationReason = individual.UnderAdministrationOrDebtReviewReason;
                person.ExternalReference = individual.ExternalReference;
                person.Email = individual.ContactDetail.Email;
                person.Gender = individual.Gender;
                person.Occupation = individual.Occupation != null ? individual.Occupation.Name : String.Empty;
                person.Title = individual.Title;
                person.DriverHomeLoss = GetHomeLosses(party.Id, individual.IdentityNo);
                person.DriverLossHistory = GetLossHistory(party.Id, individual.IdentityNo, true);
                person.DriverMotorLoss = GetMotorLosses(party.Id, individual.IdentityNo);
            }
            else
            {
                person.Surname = party.DisplayName;
            }

            person.Addresses.AddRange(CreateAddresses(party));

            List<PartyCorrespondencePreference> correspondenceList = m_Repository.GetAll<PartyCorrespondencePreference>()
                        .Where(a => a.Party.Id == party.Id).ToList();

            person.CommunicationPreferences.AddRange(Mapper.Map<List<PartyCorrespondencePreferenceDto>>(correspondenceList));

            return new List<RatingRequestPersonDto>()
            {
                person
            };
        }

        private static IEnumerable<RatingRequestAddressDto> CreateAddresses(Party party)
        {
            var addresses = party.Addresses.Where(a => a.IsDeleted != true).ToList();

            return addresses.Select(address => new RatingRequestAddressDto
            {
                Address1 = address.Line1,
                Address2 = address.Line2,
                Address3 = address.Line3,
                Address4 = address.Line4,
                Complex = address.Complex,
                Province = address.StateProvince != null ? address.StateProvince.Name : "",
                City = address.Line3,
                Suburb = address.Line4,
                AddressType = address.AddressType ?? AddressTypes.PhysicalAddress,
                DefaultAddress = address.IsDefault,
                PostalCode = address.Code,
                ExternalAddressID = address.Id.ToString(),
                Country = address.StateProvince != null ? address.StateProvince.Country.Name : "",
                Latitude = address.Latitude,
                Longitude = address.Longitude
            });
        }


        public IEnumerable<RatingRequestPersonDto> RatingRequestPersonDtos(Party party, IEnumerable<ProposalDefinition> proposalDefinitions)
        {
            var list = new List<RatingRequestPersonDto>();

            List<string> idNumbers;
            var contacts = GetMainDriverContact(out idNumbers, party.Id, proposalDefinitions);

            log.DebugFormat("processing {0} main driver contacts ", idNumbers.Count);

            foreach (var id in idNumbers)
            {
                if (string.IsNullOrEmpty(id))
                    continue;

                var contact = contacts.FirstOrDefault(a => a.IdentityNo == id);

                if (contact == null)
                    continue;

                var person = new RatingRequestPersonDto { PolicyHolder = false };

                person.Surname = contact.Surname;
                person.FirstName = contact.FirstName;
                person.Title = contact.Title;
                person.IdNumber = contact.IdentityNo;
                person.DateOfBirth = contact.DateOfBirth;
                person.MaritalStatus = contact.MaritalStatus ?? MaritalStatuses.Unknown;
                person.Language = contact.Language ?? Languages.English;
                person.Initials = contact.FirstName.Length > 0 ?
                                            contact.FirstName.Substring(0, 1) : string.Empty;

                if (contact.ContactDetail != null)
                {
                    person.PhoneWork = contact.ContactDetail.Work;
                    person.PhoneDirect = contact.ContactDetail.Direct;
                    person.PhoneCell = contact.ContactDetail.Cell;
                    person.PhoneFax = contact.ContactDetail.Fax;
                    person.Email = contact.ContactDetail.Email;
                }


                person.Addresses.AddRange(CreateAddresses(party));

                person.DriverHomeLoss = GetHomeLosses(party.Id, contact.IdentityNo);
                person.DriverLossHistory = GetLossHistory(party.Id, contact.IdentityNo, false);
                person.DriverMotorLoss = GetMotorLosses(party.Id, contact.IdentityNo);


                list.Add(person);
                log.DebugFormat("main driver contact{0} added", contact.Id);
            }
            return list;
        }


        private List<Contact> GetMainDriverContact(out List<string> idNumbers, int partyId, IEnumerable<ProposalDefinition> proposalDefinitions)
        {
            var questionMainDriverIdNumber = MasterData.Questions.MainDriverIDNumber;

            idNumbers = proposalDefinitions
               .SelectMany(a => a.ProposalQuestionAnswers)
               .Where(a => a.QuestionDefinition.Question.Equals(questionMainDriverIdNumber))
               .Select(a => a.Answer).ToList();

            var contacts = m_Repository.GetAll<Relationship>()
               .Where(a => a.Party.Id == partyId)
               .Select(a => a.ChildParty as Contact).ToList();

            return contacts;
        }


        public RatingRequestDto Build(ProposalHeader proposal, string source = "", DateTime? ratingDate = null,
            Channel channel = null, bool addMapVapSections = false)

        {
            var request = new RatingRequestDto
            {
                Id = Guid.NewGuid(),
                Source = source,
                PlatformId = Guid.NewGuid()
            };

            request.Policy.Source = proposal.Source;
            var persons = CreatePersons(proposal.Party);

            var ratingRequestPersons = RatingRequestPersonDtos(proposal.Party,
                proposal.ProposalDefinitions.Where(a => !a.IsDeleted).ToList());

            request.Policy.Persons.AddRange(persons);
            //contacts for main driver
            request.Policy.Persons.AddRange(ratingRequestPersons);

            var proposalDefinitions = proposal.ProposalDefinitions.Where(a => a.IsDeleted != true).ToList();

            //TODO:
            //            request.Policy.Persons.AddRange(GetContacts(proposalDefinitions, party.Id));



            var items = new List<RatingRequestItemDto>();

            var overrideRatingQuestions = m_Repository.GetAll<OverrideRatingQuestionChannel>()
                .Where(x => x.Channel.Id == channel.Id &&
                            !x.IsDeleted)
                .Join(m_Repository.GetAll<OverrideRatingQuestion.OverrideRatingQuestion>(),
                    o => o.OverrideRatingQuestion.Id,
                    i => i.Id,
                    (o, i) => i)
                .Where(i => !i.IsDeleted)
                .ToList();

            var overrideRatingQuestionsDto = Mapper.Map<List<OverrideRatingQuestionDto>>(overrideRatingQuestions);

            var overrideRatingQuestionAnswerDto = Mapper.Map<List<OverrideRatingQuestionAnswerDto>>(overrideRatingQuestionsDto);

            request.OverrideRatingQuestionAnswer = overrideRatingQuestionAnswerDto;

            var policyHolder = persons.FirstOrDefault(p => p.PolicyHolder) ?? persons.FirstOrDefault();
            request.ClientLossHistory = GetLossHistory(proposal.Party.Id, policyHolder.IdNumber, true);

            foreach (var definition in proposalDefinitions)
            {
                var requestItem = new RatingRequestItemDto()
                {
                    //TODO: need to confirm why some proposal item has empty assets..
                    //This breaks the quote to proposal link
                    AssetNo = definition.Asset != null && definition.Asset.AssetNo > 0 ? definition.Asset.AssetNo : definition.Id,
                    Cover = definition.Cover,
                    RatingDate = ratingDate ?? DateTime.UtcNow,
                    ProductCode = definition.Product != null ? definition.Product.ProductCode : null,
                    IsVap = definition.IsVap ?? false,
                    LinkedProducts = new RatingRequestLinkedProductsDtoBuilder(m_Repository).ListLinkedProducts(definition.Product.ProductCode.ToString())
                };

                AppendAnswers(definition, requestItem);
                AppendFuneralMembers(definition, requestItem);
                AppendExtras(definition, requestItem);
                //Build contact/party loss history
                AppendDriverLoss(definition, requestItem);

                //Previous LH builders are legacy and use the multiple tables approach by AIG
                //only populate the RatinglosshistoryDto per definition main driver (
                //WARNING this may include the Policy holder as a main driver on proposal definition
                AppendComponentLossHistory(definition, requestItem);
                items.Add(requestItem);
            }

            request.Items.AddRange(items);

            //Build Policy holder's Loss History

            AppendComponentPolicyHolderLossHistory(proposal.LeadActivity.Lead.Party.Id, request, persons.FirstOrDefault(x => x.PolicyHolder).IdNumber);
            return request;
        }

        private void AppendMapVapSections(List<MapVapQuestionDefinitionDto> mapVaps, RatingRequestItemDto requestItem)
        {
            foreach (var vap in mapVaps)
            {
                foreach (var answer in requestItem.Answers)
                {
                    if (vap.QuestionDefinition.Question.Id == answer.Question.Id)
                    {
                        var didUserTickYes = false;

                        if (Boolean.TryParse(answer.QuestionAnswer.ToString(), out didUserTickYes))
                        {
                            if (didUserTickYes)
                            {
                                requestItem.MapVapQuestionDefinition.Add(vap);
                            }
                        }
                    }
                }
            }
        }

        private void AppendAnswers(ProposalDefinition definition, RatingRequestItemDto requestItem)
        {
            var requestItemBuilder = new RatingRequestItemQuestionAnswerBuilderFactory();

            foreach (var questionAnswer in definition.ProposalQuestionAnswers)
            {
                //cater for empty answer in boolean
                if (questionAnswer.QuestionDefinition != null && questionAnswer.QuestionDefinition.Question.QuestionType == QuestionTypes.Checkbox)
                {
                    questionAnswer.Answer = questionAnswer.Answer.ConvertToBoolString();
                }

                log.InfoFormat("Adding question '{0}' with answer '{1}' to rating request",
                    questionAnswer.QuestionDefinition.Question.Name,
                    questionAnswer.Answer);

                var requestItemAnswer = requestItemBuilder.Create(questionAnswer);
                requestItem.Answers.Add(requestItemAnswer);
            }
        }

        private void AppendFuneralMembers(ProposalDefinition definition, RatingRequestItemDto requestItem)
        {
            List<AdditionalMembers> members = m_Repository.GetAll<AdditionalMembers>()
                    .Where(a => a.ProposalDefinition.Id == definition.Id && a.IsDeleted != true)
                    .ToList();

            foreach (AdditionalMembers item in members)
            {
                var funeralMember = new RatingRequestItemFuneralDto
                {
                    MemberIdentifier = item.Id.ToString(),
                    MemberIdNumber = item.IdNumber,
                };

                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AIGFuneralRelationship,
                    QuestionAnswer = item.MemberRelationship.Name
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberDateOnCover,
                    QuestionAnswer = item.DateOnCover
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberIdNumber,
                    QuestionAnswer = item.IdNumber
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberInitials,
                    QuestionAnswer = item.Initials
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberStudent,
                    QuestionAnswer = item.IsStudent
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberSurname,
                    QuestionAnswer = item.Surname
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberDateOfBirth,
                    QuestionAnswer = item.DateOfBirth
                });

                requestItem.FuneralMembers.Add(funeralMember);
            }
        }

        private static void AppendExtras(ProposalDefinition definition, RatingRequestItemDto requestItem)
        {
            if (definition.Asset == null)
                return;

            foreach (AssetExtras assetExtra in definition.Asset.Extras.Where(a => a.Selected).ToList())
            {
                var item = new RatingRequestItemAccessoriesDto();

                var extra = new RatingRequestItemAccessoriesAnswerDto
                {
                    Question = Questions.AccessoriesCategory,
                    QuestionAnswer = assetExtra.AccessoryType == null ? null : assetExtra.AccessoryType.Answer
                };

                item.AccessoryAnswers.Add(extra);

                item.AccessoryAnswers.Add(new RatingRequestItemAccessoriesAnswerDto
                {
                    Question = Questions.AccessoriesDescription,
                    QuestionAnswer = assetExtra.Description
                });

                item.AccessoryAnswers.Add(new RatingRequestItemAccessoriesAnswerDto
                {
                    Question = Questions.AccessoriesSuminsured,
                    QuestionAnswer = assetExtra.Value
                });

                item.AccessoryAnswers.Add(new RatingRequestItemAccessoriesAnswerDto
                {
                    Question = Questions.AccessoriesIsFactoryFitted,
                    QuestionAnswer = !assetExtra.IsUserDefined
                });

                requestItem.Accessories.Add(item);
            }
        }

        #region Component Builders
        private void AppendComponentPolicyHolderLossHistory(int partyId, RatingRequestDto request, string idNumber)
        {
            var ratingLossHistoryDto = new RatingLossHistoryDto()
            {
                PartyId = partyId,
                IdentityNumber = idNumber,
                IsPolicyHolder = true,
                PartyType = PartyTypes.Individual,
                Items = ComponentAnswerBuilder(partyId)
            };
        }

        private void AppendComponentLossHistory(ProposalDefinition definition, RatingRequestItemDto requestItem)
        {
            var ratingLossHistoryDto = new RatingLossHistoryDto();

            //Set IdentityNumber
            ratingLossHistoryDto.IdentityNumber = definition.ProposalQuestionAnswers.Where(a => a.QuestionDefinition.Question == Questions.MainDriverIDNumber)
                    .Select(a => a.Answer)
                    .FirstOrDefault();
            if (ratingLossHistoryDto.IdentityNumber == null) return;

            //Set Party ID of idNumber
            var individual = m_Repository.GetAll<Individual>()
                .Where(x => x.IdentityNo == ratingLossHistoryDto.IdentityNumber)
                .FirstOrDefault();

            if (individual != null)
            {
                ratingLossHistoryDto.PartyId = individual.Id;
            }
            else
            {
                ratingLossHistoryDto.PartyId = m_Repository.GetAll<Contact>()
                    .Where(x => x.IdentityNo == ratingLossHistoryDto.IdentityNumber)
                    .FirstOrDefault().Id;
            }

            //Set PartyType
            ratingLossHistoryDto.PartyType = m_Repository.GetById<Party>(ratingLossHistoryDto.PartyId).PartyType;

            //Set IsPolicyHolder 
            ratingLossHistoryDto.IsPolicyHolder = (ratingLossHistoryDto.PartyId == definition.Party.Id) ? true : false;

            ratingLossHistoryDto.Items = ComponentAnswerBuilder(ratingLossHistoryDto.PartyId);

            requestItem.LossHistoryComponent.Add(ratingLossHistoryDto);
        }

        private List<RatingComponentItemDto> ComponentAnswerBuilder(int partyId)
        {
            var ratingComponentItems = new List<RatingComponentItemDto>();
            //Get all definitions per partyId
            var componentDefinitionList = m_Repository.GetAll<ComponentDefinition>().Where(x => x.PartyId == partyId)
                .ToList();

            foreach (var item in componentDefinitionList)
            {
                var Answers = m_Repository.GetAll<ComponentQuestionAnswer>()
                    .Where(x => x.ComponentDefinitionId == item.Id);

                foreach (var answer in Answers)
                {
                    var questionDefinition = m_Repository.GetById<ComponentQuestionDefinition>(answer.ComponentQuestionDefinitionId);
                    var question = new ComponentQuestions().FirstOrDefault(x => x.Id == questionDefinition.Id);

                    if (question != null)
                        ratingComponentItems.Add(new RatingComponentItemDto()
                        {
                            ComponentDefinitionId = answer.ComponentDefinitionId,
                            Question = question.Name,
                            Answer = answer.Answer,
                            QuestionGroup = question.ComponentQuestionGroup,
                            QuestionType = question.QuestionType
                        });
                }
            }

            return ratingComponentItems;
        }
        #endregion

        //LEGACY CODE only support for AIG
        #region Loss History
        /// <summary>
        /// Builds loss history for the Policy holder (Client) only
        /// </summary>
        /// <param name="partyId"></param>
        /// <param name="request"></param>
        /// <param name="idNumber"></param>
        private void AppendPolicyHolderLossHistory(int partyId, RatingRequestDto request, string idNumber)
        {
            request.ClientLossHome = GetHomeLosses(partyId, idNumber);
            request.ClientLossMotor = GetMotorLosses(partyId, idNumber);
            request.ClientLossHistory = GetLossHistory(partyId, idNumber, true);
        }

        /// <summary>
        /// Builds loss history on id and PartyId only applicable for motor proposal
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="requestItem"></param>
        public void AppendDriverLoss(ProposalDefinition definition, RatingRequestItemDto requestItem)
        {
            var isPolicyHolder = true;
            var partyId = 0;

            var mainDriverIdentityNumber =
                definition.ProposalQuestionAnswers.Where(a => a.QuestionDefinition.Question == Questions.MainDriverIDNumber)
                    .Select(a => a.Answer)
                    .FirstOrDefault();

            if (string.IsNullOrWhiteSpace(mainDriverIdentityNumber))
                return;

            //because definition.Party does not have policy holder's identity number
            var policyHolder = m_Repository.GetById<Individual>(definition.Party.Id);

            if (mainDriverIdentityNumber != policyHolder.IdentityNo)
            {
                var contacts = definition.Party.Relationships.Select(relationship => m_Repository.GetById<Contact>(relationship.ChildParty.Id)).ToList();

                var selectedContact = contacts.FirstOrDefault(x => x.IdentityNo == mainDriverIdentityNumber);

                if (selectedContact != null)
                {
                    partyId = selectedContact.Id;
                    isPolicyHolder = false;
                }
            }
            else
            {
                partyId = policyHolder.Id;
            }

            requestItem.DriverHomeLoss = GetHomeLosses(partyId, mainDriverIdentityNumber);
            requestItem.DriverMotorLoss = GetMotorLosses(partyId, mainDriverIdentityNumber);
            requestItem.DriverLossHistory = GetLossHistory(partyId, mainDriverIdentityNumber, isPolicyHolder);
        }

        public List<RatingRequestHomeLossDto> GetHomeLosses(int id, string idNumber)
        {
            var homeLosses = m_Repository.GetAll<HomeLossHistory>().Where(a => a.Party.Id == id && a.IsDeleted == false).ToList();

            var result = homeLosses.Select(item => new RatingRequestHomeLossDto
            {
                IdNumber = idNumber,
                CurrentlyInsured = item.CurrentlyInsured,
                DateOfLoss = item.DateOfLoss,
                HomeClaimAmount = item.HomeClaimAmount,
                HomeClaimLocation = item.HomeClaimLocation,
                HomeTypeOfLoss = item.HomeTypeOfLoss,
                InsurerCancel = item.InsurerCancel,
                UninterruptedPolicy = item.UninterruptedPolicy,
                Why = item.Why
            }).ToList();

            return result;
        }
        public List<RatingRequestMotorLossDto> GetMotorLosses(int contactId, string idNumber)
        {
            var motorLosses = m_Repository.GetAll<MotorLossHistory>().Where(a => a.Party.Id == contactId && a.IsDeleted == false).ToList();

            var result = motorLosses.Select(item => new RatingRequestMotorLossDto
            {
                IdNumber = idNumber,
                DateOfLoss = item.DateOfLoss,
                InsurerCancel = item.InsurerCancel,
                UninterruptedPolicy = item.UninterruptedPolicy,
                Why = item.Why,
                CurrentlyInsured = item.CurrentlyInsured,
                MotorClaimAmount = item.MotorClaimAmount,
                MotorCurrentTypeOfCover = item.MotorCurrentTypeOfCover,
                MotorTypeOfLoss = item.MotorTypeOfLoss,
                MotorUninterruptedPolicyNoClaim = item.MotorUninterruptedPolicyNoClaim,
                PreviousComprehensiveInsurance = item.PreviousComprehensiveInsurance
            }).ToList();

            return result;
        }
        public List<RatingRequestLossHistoryDto> GetLossHistory(int id, string idNumber, bool isPolicyHolder)
        {
            List<LossHistory> lossHistories;

            if (isPolicyHolder)
            {
                lossHistories =
                    m_Repository.GetAll<LossHistory>()
                        .Where(x => x.Party.Id == id && x.IsDeleted == false && x.ContactId == 0)
                        .ToList();
            }
            else
            {
                lossHistories =
                    m_Repository.GetAll<LossHistory>()
                        .Where(x => x.ContactId == id && x.IsDeleted == false)
                        .ToList();
            }

            var result = lossHistories.Select(item => new RatingRequestLossHistoryDto
            {
                IdNumber = idNumber,
                CurrentlyInsured = item.CurrentlyInsured,
                InsurerCancel = item.InsurerCancel,
                InterruptedPolicyClaim = item.InterruptedPolicyClaim,
                PreviousComprehensiveInsurance = item.PreviousComprehensiveInsurance,
                UninterruptedPolicy = item.UninterruptedPolicy,
                MotorUninterruptedPolicyNoClaimId = item.MotorUninterruptedPolicyNoClaim,
                MissedPremiumLast6Months = item.MissedPremiumLast6Months
            }).ToList();

            return result;
        }
        #endregion
        //LEGACY CODE
    }

    public static class StringExtensions
    {
        public static string ConvertToBoolString(this string value)
        {
            bool b;
            int intvalue = 0;

            if (Int32.TryParse(value, out intvalue))
                b = intvalue == 1;
            else
                bool.TryParse(value, out b);

            return b.ToString();
        }
    }

    //FCM - Extending class function, added this extentsion part so as not to disturb orginal classes above

    public class RatingRequestLinkedProductsDtoBuilder : RatingRequestDtoBuilder
    {
        private readonly IRepository m_Repository;
        private static readonly ILog log = LogManager.GetLogger<RatingRequestDtoBuilder>();

        public RatingRequestLinkedProductsDtoBuilder(IRepository repository) : base(repository)
        {
            this.m_Repository = repository;
        }

        public List<RatingLinkedProductsDTO> ListLinkedProducts(string productcode)
        {

            List<RatingLinkedProductsDTO> LinkedProductList = new List<RatingLinkedProductsDTO>();
            RatingLinkedProductsDTO LinkedProduct;

            try
            {
                if (!string.IsNullOrEmpty(productcode))
                {
                    var ProductList = m_Repository.GetAll<Domain.Products.Product>()
                            .Where(x => x.ProductCode.Contains(productcode) && !x.ProductCode.Equals(productcode)).ToList();

                    if (ProductList != null)
                        foreach (var item in ProductList)
                        {
                            LinkedProduct = new RatingLinkedProductsDTO(item.Id, item.ProductCode.ToString(), item.Name);
                            LinkedProductList.Add(LinkedProduct);
                        }
                }
            }
            catch (Exception e)
            {
                log.ErrorFormat("Error retrieving product links ", e.Message);
            }
            return LinkedProductList;

        }
    }
}
