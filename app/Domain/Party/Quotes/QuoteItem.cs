﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base;
using Domain.Base.Extentions;
using Domain.Products;
using Infrastructure.NHibernate.Attributes;

namespace Domain.Party.Quotes
{
    public class QuoteItem : Entity
    {
        public QuoteItem()
        {
            Fees = new QuoteItemFees();
            Excess = new QuoteItemExcess();
            IsVap = false;
        }

        public QuoteItem(Quote quote, string insuredDescription, string description, CoverDefinition cover, QuoteItemAsset asset, QuoteItemFees fees, QuoteItemExcess excess, List<string> errors, List<string> warnings, bool isVap = false, int vapQuestionId = 0, List<string> clauses = null)
        {
            Quote = quote;
            CoverDefinition = cover;
            Asset = asset;
            Fees = fees;
            Excess = excess;
            SumInsuredDescription = insuredDescription;
            Description = description;
            QuoteItemBreakDowns = new List<QuoteItemBreakDown>();
            QuoteItemState = new QuoteItemState(this);
            QuoteItemState.AddErrors(errors);
            QuoteItemState.AddWarnings(warnings);
            IsVap = isVap;
            VapQuestionId = vapQuestionId;
            if (clauses != null && clauses.Any())
            { SaveClauses(clauses); }
        }

        public virtual IList<QuoteItemBreakDown> QuoteItemBreakDowns { get; protected internal set; }
        public virtual QuoteItemState QuoteItemState { get; protected internal set; }
        public virtual Quote Quote { get; set; }
        public virtual CoverDefinition CoverDefinition { get; protected internal set; }
        public virtual QuoteItemFees Fees { get; protected internal set; }
        public virtual QuoteItemExcess Excess { get; protected internal set; }
        public virtual QuoteItemAsset Asset { get; protected internal set; }
        public virtual string Description { get; protected internal set; }
        public virtual string SumInsuredDescription { get; protected internal set; }
        public virtual decimal DiscretionaryDiscount { get; set; }
        public virtual bool IsVap { get; set; }
        public virtual int VapQuestionId { get; set; }
        public virtual string Clauses { get; set; }


        #region [ Quote Breakdown HACK ]
        //Used for the quote breakdown
        //We need to check if the VAP is per section and if it is we must not add it again
        //We need the "Parent" items cover to determine if the VAP can be added to the breakdown again

        [DoNotMap]
        public virtual int MapVapQuestionDefinitionId { get; set; }

        [DoNotMap]
        public virtual int ParentCoverId { get; set; }

        #endregion

        public virtual QuoteItemBreakDown AddBreakDown(QuoteItemBreakDown breakDown)
        {
            QuoteItemBreakDowns.Add(breakDown);

            return breakDown;
        }

        public virtual void SaveClauses(List<string> clauseList)
        {
            var csvList = string.Join("|", clauseList.ToArray());
            Clauses = csvList.TrimFirst(10000);
        }

        public virtual List<string> GetClauses()
        {
            var clauses = new List<string>();
            try
            {
                if (Clauses != null && Clauses.Any())
                { clauses = Clauses.Split('|').Select(i => i).ToList(); }
            }
            catch (Exception)
            {
                // ignored
            }
            return clauses;
        }
    }
}