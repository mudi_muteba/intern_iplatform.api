﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;

using MasterData.Authorisation;
using iPlatform.Api.DTOs.Ratings.Quoting;
using System.Linq;
using Domain.Activities;

namespace Domain.Party.Quotes.Handlers
{
    public class DeleteQuoteAcceptDtoHandler : ExistingEntityDtoHandler<Quote,DeleteQuoteAcceptDto, bool>
    {
        private readonly IRepository _repository;

        public DeleteQuoteAcceptDtoHandler(IProvideContext contextProvider, IRepository repository)
            :base(contextProvider, repository)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(Quote entity, DeleteQuoteAcceptDto dto, HandlerResult<bool> result)
        {
            entity.NotAccepted();

            var quoteAcceptedActivity = _repository.GetAll<QuoteAcceptedLeadActivity>().Where(x => x.Quote.Id == dto.Id).FirstOrDefault();
            if(quoteAcceptedActivity != null)
            {
                quoteAcceptedActivity.Delete();
                _repository.Save(quoteAcceptedActivity);
            }

            var underWritingActivity = _repository.GetAll<SentForUnderwritingLeadActivity>().Where(x => x.Quote.Id == dto.Id).FirstOrDefault();
            if (underWritingActivity != null)
            {
                underWritingActivity.Delete();
                _repository.Save(underWritingActivity);
            }
            result.Processed(true);
        }
    }
}