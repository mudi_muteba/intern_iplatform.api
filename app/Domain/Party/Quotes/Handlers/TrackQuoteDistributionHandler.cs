using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Quotes;
using MasterData.Authorisation;

namespace Domain.Party.Quotes.Handlers
{
    public class TrackQuoteDistributionHandler : ExistingEntityDtoHandler<QuoteHeader, TrackQuoteDistributionDto, int>
    {
        public TrackQuoteDistributionHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void HandleExistingEntityChange(QuoteHeader entity, TrackQuoteDistributionDto dto, HandlerResult<int> result)
        {
            entity.TrackDistribution(dto);

            result.Processed(entity.Id);
        }
    }
}