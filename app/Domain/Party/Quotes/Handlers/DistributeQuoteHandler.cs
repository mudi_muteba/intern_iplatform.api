using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.Quotes.Queries;
using iPlatform.Api.DTOs.Party.Quotes;
using MasterData.Authorisation;

namespace Domain.Party.Quotes.Handlers
{
    public class DistributeQuoteHandler : BaseDtoHandler<DistributeQuoteDto, Guid>
    {
        private readonly GetQuoteByExternalReference quoteQuery;
        private readonly GetChannelById channelQuery;
        private readonly IRepository repository;

        public DistributeQuoteHandler(IProvideContext contextProvider
            , GetQuoteByExternalReference quoteQuery
            , GetChannelById channelQuery
            , IRepository repository) : base(contextProvider)
        {
            this.quoteQuery = quoteQuery;
            this.channelQuery = channelQuery;
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    QuotingAuthorisation.CanQuote
                };
            }
        }

        protected override void InternalHandle(DistributeQuoteDto dto, HandlerResult<Guid> result)
        {
            var quote = quoteQuery.WithExternalReference(dto.ExternalReference).ExecuteQuery().FirstOrDefault();
            var channel = channelQuery.WithChannelId(quote.Proposal.Party.Channel.Id).ExecuteQuery().FirstOrDefault();

            quote.Distribute(dto, channel);

            repository.Save(quote);
        }
    }
}