﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.Quotes.Builders;
using Domain.Party.Quotes.Events;
using Domain.Ratings.Engines;
using Domain.SalesForce.Events;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData.Authorisation;

namespace Domain.Party.Quotes.Handlers
{
    public class CreateQuoteHandler : IHandleDto<CreateQuoteDto, QuoteHeaderDto>
    {
        private readonly IRatingEngine _engine;
        private readonly SystemChannels _systemChannels;
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;
        private readonly IProvideContext _contextProvider;

        public CreateQuoteHandler(IExecutionPlan executionPlan, IRepository repository, IRatingEngine engine,SystemChannels systemChannels,IProvideContext contextProvider)
        {
            _executionPlan = executionPlan;
            _repository = repository;
            _engine = engine;
            _systemChannels = systemChannels;
            _contextProvider = contextProvider;
        }

        public List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProposalAuthorisation.Create
                };
            }
        }

        public void Handle(CreateQuoteDto dto, HandlerResult<QuoteHeaderDto> result)
        {
            var quoteHeader = QuoteHeaderBuilder.GetQuoteHeader(dto, _executionPlan, _repository, result, _systemChannels,_contextProvider.Get().ActiveChannelId);
            var response = Mapper.Map<QuoteHeader, QuoteHeaderDto>(quoteHeader);

            quoteHeader.RaiseEvent(new QuoteCreatedEvent(dto.IndividualId, quoteHeader, dto.Context.UserId));

            result.Processed(response);
        }
    }
}