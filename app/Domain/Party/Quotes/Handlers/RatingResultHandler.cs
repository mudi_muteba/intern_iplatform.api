﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.ProposalHeaders;
using Domain.Party.ProposalHeaders.Queries;
using Domain.Party.Quotes.Queries;
using Domain.Products.Queries;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData.Authorisation;
using ValidationMessages.Ratings;

namespace Domain.Party.Quotes.Handlers
{
    public class RatingResultHandler : IHandleDto<RatingResultDto, int>
    {
        private readonly GetAllAllocatedProductsAsFullProductQuery productQuery;
        private readonly GetQuoteByExternalReference quoteQuery;
        private readonly GetProposalHeaderByExternalReferenceQuery proposalQuery;
        private readonly IRepository repository;

        public RatingResultHandler(GetProposalHeaderByExternalReferenceQuery proposalQuery
            , GetAllAllocatedProductsAsFullProductQuery productQuery
            , GetQuoteByExternalReference quoteQuery
            , IRepository repository)
        {
            this.proposalQuery = proposalQuery;
            this.productQuery = productQuery;
            this.quoteQuery = quoteQuery;
            this.repository = repository;
        }

        public List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    QuotingAuthorisation.CanQuote
                };
            }
        }

        public void Handle(RatingResultDto dto, HandlerResult<int> result)
        {
            var proposal = proposalQuery.WithExternalReference(dto.RatingRequestId.ToString()).ExecuteQuery().FirstOrDefault();

            if (proposal == null)
            {
                result.AddValidationMessage(QuoteCreationValidationMessages.NoProposalFoundForTheRatingResult);
                result.Processed(0);

                return;
            }

            var quote = CreateQuote(proposal, dto);

            result.Processed(quote.Id);
        }

        private QuoteHeader CreateQuote(ProposalHeader proposal, RatingResultDto dto)
        {
            var allocatedProducts = productQuery.ExecuteQuery().ToList();

            DeleteExistingQuotes(dto);

            var quote = proposal.CreateQuote(dto
                , allocatedProducts
                , repository);

            repository.Save(quote);

            return quote;
        }

        private void DeleteExistingQuotes(RatingResultDto dto)
        {
            var existingQuotes = quoteQuery.WithExternalReference(dto.RatingRequestId.ToString()).ExecuteQuery().ToList();

            if (!existingQuotes.Any())
                return;

            foreach (var existingQuote in existingQuotes)
            {
                existingQuote.Delete();
                repository.Save(existingQuote);
            }
        }
    }
}