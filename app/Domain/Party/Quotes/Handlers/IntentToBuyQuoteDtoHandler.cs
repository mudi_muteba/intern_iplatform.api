using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.SalesForce.Events;
using iPlatform.Api.DTOs.Quotes;
using MasterData.Authorisation;

namespace Domain.Party.Quotes.Handlers
{
    public class IntentToBuyQuoteDtoHandler : ExistingEntityDtoHandler<Quote, IntentToBuyQuoteDto, int>
    {
        private readonly IProvideContext _contextProvider;
        private readonly IRepository _repository;

        public IntentToBuyQuoteDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _contextProvider = contextProvider;
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void HandleExistingEntityChange(Quote entity, IntentToBuyQuoteDto dto,
            HandlerResult<int> result)
        {
            var source = string.Empty;
            if (_contextProvider.Get().RequestHeaders.ContainsKey("source"))
                source = _contextProvider.Get().RequestHeaders["source"].FirstOrDefault();

            if (!source.Equals("aig-widget")) return;

            entity.QuoteHeader.Proposal.LeadActivity.Lead.QuoteIntentToBuy(entity.Id);
            entity.QuoteHeader.Proposal.IsIntentToBuy = true;
            result.Processed(entity.Id);

            var individual =
                _repository.GetAll<Individual>().FirstOrDefault(a => a.Id == entity.QuoteHeader.Proposal.Party.Id);

            var cmpid = entity.QuoteHeader.Proposal.CmpidSource;

            entity.RaiseEvent(new SalesForceEntryEvent("Intent to Buy", individual, cmpid, entity));
        }
    }
}