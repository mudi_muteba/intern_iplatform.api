﻿
using System.Linq;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Activities;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Ratings.Quoting;


namespace Domain.Party.Quotes.Handlers
{
    public class DeletePolicyBindingCompletedDtoHandler : ExistingEntityDtoHandler<Quote, DeletePolicyBindingCompletedDto, bool>
    {
        private readonly IRepository m_Repository;

        public DeletePolicyBindingCompletedDtoHandler(IProvideContext contextProvider, IRepository repository)
            :base(contextProvider, repository)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(Quote entity, DeletePolicyBindingCompletedDto dto, HandlerResult<bool> result)
        {
            entity.NotAccepted();

            var activity = m_Repository.GetAll<PolicyBindingCompletedLeadActivity>().Where(x => x.Quote.Id == dto.Id && x.RequestId == dto.RequestId).FirstOrDefault();
            if(activity != null)
            {
                activity.Delete();
                m_Repository.Save(activity);
            }

            result.Processed(true);
        }
    }
}