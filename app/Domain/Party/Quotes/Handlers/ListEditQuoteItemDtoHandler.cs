﻿using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Quotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Quotes;
using MasterData.Authorisation;
using Domain.Users.Discounts;
using log4net;

namespace Domain.Party.Quotes.Handlers
{
    public class ListEditQuoteItemDtoHandler : BaseDtoHandler<ListEditQuoteItemDto, QuoteDiscountResultDto>
    {
        private static readonly ILog m_Log = LogManager.GetLogger(typeof(ListEditQuoteItemDtoHandler));
        private readonly IRepository m_Repository;

        public ListEditQuoteItemDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void InternalHandle(ListEditQuoteItemDto dto, HandlerResult<QuoteDiscountResultDto> result)
        {

            var quoteDiscountResult = new QuoteDiscountResultDto();
            bool success = false;
            //Use the error count to return an error message to the user.
            int errorCount = 0;

            foreach (EditQuoteItemDto editQuoteItemDto in dto.QuoteItems)
            {
                UserDiscountCoverDefinition userDiscountCoverDefinition = m_Repository.GetAll<UserDiscountCoverDefinition>()
                    .FirstOrDefault(u => u.User.Id == dto.UserId &&
                        u.CoverDefinition.Id == editQuoteItemDto.CoverDefinitionId);

                if (userDiscountCoverDefinition != null &&
                    (userDiscountCoverDefinition.Discount > 0 || userDiscountCoverDefinition.Load > 0))
                {
                    //Get the actual quote item
                    QuoteItem quoteItem = m_Repository.GetAll<QuoteItem>()
                        .FirstOrDefault(qi => qi.Quote.Id == editQuoteItemDto.QuoteId &&
                            qi.Asset.AssetNumber == editQuoteItemDto.AssetNo);
                    if (quoteItem != null)
                    {
                        //Check if the user is applying discount or load
                        bool isLoading = false;
                        if (editQuoteItemDto.Percentage < 0)
                        {
                            isLoading = true;
                        }

                        //Calculate the difference.
                        decimal difference = quoteItem.Fees.OriginalPremium * (editQuoteItemDto.Percentage / 100);
                        //Calculate the total discount per product
                        decimal totalDiscount = 0;
                        dto.QuoteItems.ForEach(qi =>
                            {
                                QuoteItem item = m_Repository.GetAll<QuoteItem>()
                                    .FirstOrDefault(t => t.Quote.Id == qi.QuoteId &&
                                        t.Asset.AssetNumber == qi.AssetNo &&
                                        t.CoverDefinition.Product.Id == userDiscountCoverDefinition.CoverDefinition.Product.Id);
                                if (item != null)
                                {
                                    totalDiscount += item.Fees.OriginalPremium * (qi.Percentage / 100);
                                }
                            });

                        //Business rules
                        bool applyDiscount = (!isLoading && editQuoteItemDto.Percentage <= userDiscountCoverDefinition.Discount); //Ensure discount can be applied
                        bool applyLoading = (isLoading && Math.Abs(editQuoteItemDto.Percentage) <= userDiscountCoverDefinition.Load); //Ensure loading can be applied
                        bool discountLessThanMaxItem = (userDiscountCoverDefinition.ItemMaxDiscount > 0 && (difference - 0.01m) <= userDiscountCoverDefinition.ItemMaxDiscount); //Ensure the difference is not greater than the max item discount
                        bool totalDiscountLessThanMaxQuote = (userDiscountCoverDefinition.QuoteMaxDiscount > 0 && (totalDiscount - 0.01m) <= userDiscountCoverDefinition.QuoteMaxDiscount); //Ensure the difference is not greater than the max quote discount


                        var storeDiscounts = true;

                        var messageString = new StringBuilder();
                        messageString.AppendFormat("Errors Saving Discounts for {0}: {1}", quoteItem.CoverDefinition.Cover.Name, quoteItem.Asset.Description);
                        if (!(applyDiscount || applyLoading))
                        {
                            if (!isLoading)
                            {
                                messageString.AppendFormat("Discount cannot be applied because Percentage ({0}) greater than Discount allowed({1})", editQuoteItemDto.Percentage, userDiscountCoverDefinition.Discount);
                            }
                            else
                            {
                                messageString.AppendFormat("Loading cannot be applied because Percentage ({0}) greater than Load allowed({1})", editQuoteItemDto.Percentage, userDiscountCoverDefinition.Load);
                            }
                            storeDiscounts = false;
                        }

                        if (!discountLessThanMaxItem)
                        {
                            if (userDiscountCoverDefinition.ItemMaxDiscount <= 0)
                            {
                                messageString.AppendFormat("The Max Item Discount must be greater than 0 ({0})", userDiscountCoverDefinition.ItemMaxDiscount);
                            }
                            else
                            {
                                messageString.AppendFormat("The Premium Difference ({0}) is greater than the Max Item Discount ({1})", difference, userDiscountCoverDefinition.ItemMaxDiscount);
                            }
                            storeDiscounts = false;
                        }

                        if (!totalDiscountLessThanMaxQuote)
                        {
                            if (userDiscountCoverDefinition.QuoteMaxDiscount <= 0)
                            {
                                messageString.AppendFormat("The Quote Max Discount amount must be greater than 0.");
                            }
                            else
                            {
                                messageString.AppendFormat("The Total Discount ({0}) is greater than the Quote Max Discount amount ({1})", totalDiscount, userDiscountCoverDefinition.QuoteMaxDiscount);
                            }
                            storeDiscounts = false;
                        }

                        if (storeDiscounts)
                        {
                            quoteItem.Fees.Premium = quoteItem.Fees.OriginalPremium - difference;
                            quoteItem.Fees.DiscountedPercentage = editQuoteItemDto.Percentage;
                            quoteItem.Fees.DiscountedDifference = difference;
                            m_Repository.Save(quoteItem);
                            success = true;
                        }
                        else
                        {
                            errorCount++;
                            quoteDiscountResult.Message += messageString.ToString();
                            m_Log.Warn(string.Format("Discount could not be applied to QuoteItem due to rule failure. QuoteItemId: {0}, Percentage: {1}.", quoteItem.Id, editQuoteItemDto.Percentage));
                        }
                    }
                    else
                    {
                        errorCount++;
                        var message = string.Format("Could not get QuoteItem by QuoteId: {0} and AssetNumber: {1}.", editQuoteItemDto.QuoteId, editQuoteItemDto.AssetNo);
                        quoteDiscountResult.Message += message;
                        m_Log.Error(message);
                    }
                }
                else
                {
                    errorCount++;
                    var message = string.Format("Could not get QuoteItem by QuoteId: {0} and AssetNumber: {1}.", editQuoteItemDto.QuoteId, editQuoteItemDto.AssetNo);
                    quoteDiscountResult.Message += message;
                    m_Log.Warn(message);
                }
            }

            if (errorCount > 0)
                success = false;

            quoteDiscountResult.IsSuccess = success;
            result.Processed(quoteDiscountResult);
        }
    }
}
