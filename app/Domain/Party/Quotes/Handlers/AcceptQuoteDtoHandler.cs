using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Activities;
using Domain.Admin;
using Domain.Admin.Queries;
using Domain.Admin.SettingsiRateUser;
using Domain.AuditLeadlog.Events;
using Domain.Base.Culture;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Party.Contacts;
using Domain.Party.CorrespondencePreferences;
using Domain.Party.Payments;
using Domain.Party.ProposalHeaders;
using Domain.Party.Quotes.Builders;
using Domain.SecondLevel.Answers;
using Domain.Users;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.AuditLeadLog;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.QuoteAcceptance;
using iPlatform.Api.DTOs.Ratings.AdditionalInfo;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using Infrastructure.Configuration;
using MasterData;
using MasterData.Authorisation;
using NHibernate.Proxy;
using Shared.Extentions;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Policy;

namespace Domain.Party.Quotes.Handlers
{
    public class AcceptQuoteDtoHandler : ExistingEntityDtoHandler<Quote, AcceptQuoteDto, QuoteAcceptanceResponseDto>
    {
        public AcceptQuoteDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider, repository)
        {
            m_Repository = repository;
            m_ExecutionPlan = executionPlan;
            m_Context = contextProvider;
        }

        private readonly IRepository m_Repository;
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IProvideContext m_Context;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void HandleExistingEntityChange(Quote entity, AcceptQuoteDto dto, HandlerResult<QuoteAcceptanceResponseDto> result)
        {
            Log.InfoFormat("Loading AcceptQuoteHandler");

            var quoteAcceptanceDto = GetQuoteDto(entity, dto);
            quoteAcceptanceDto.SetContext(dto.Context);
            quoteAcceptanceDto.SetNumberFormat(dto.Context.GetCultureParam()); // used for quote acceptance email notification fees, premiums currency symbol

            //Used for cimsconnect to reply to for policyno/quoteno
            quoteAcceptanceDto.ApiUrl = ConfigurationReader.Connector.BaseUrl;

            Log.Info("STARTING AcceptQuote");

            if (dto.BrokerUserId != 0)
            {
                var brokerUser = m_Repository.GetById<User>(dto.BrokerUserId);

                entity.BrokerUserId = brokerUser.Id;
                entity.BrokerUserExternalReference = brokerUser.ExternalReference;

                quoteAcceptanceDto.BrokerUserId = entity.BrokerUserId;
                quoteAcceptanceDto.BrokerUserExternalReference = entity.BrokerUserExternalReference;
            }

            if (dto.AccountExecutiveUserId != 0)
            {
                var accountExecutiveUser = m_Repository.GetById<User>(dto.AccountExecutiveUserId);

                entity.AccountExecutiveUserId = accountExecutiveUser.Id;
                entity.AccountExecutiveExternalReference = accountExecutiveUser.ExternalReference;

                quoteAcceptanceDto.AccountExecutiveUserId = entity.AccountExecutiveUserId;
                quoteAcceptanceDto.AccountExecutiveExternalReference = entity.AccountExecutiveExternalReference;
            }

            if (dto.ChannelId != 0)
            {
                var channel = m_Repository.GetById<Channel>(dto.ChannelId);
                entity.ChannelId = channel.Id;
                entity.ChannelExternalReference = channel.ExternalReference;

                quoteAcceptanceDto.Country = channel.Country;//SmartUK add country for channel

                quoteAcceptanceDto.ChannelId = entity.ChannelId;
                //this gets overwritten in the LeadFactory in IDS
                quoteAcceptanceDto.ChannelExternalReference = entity.ChannelExternalReference;

            }
            //Create PolicyHeader for CimsConnect to update with PolicyNo
            var createPolicyWhenQuoteAcceptedDtoResponse = m_ExecutionPlan.Execute<CreatePolicyWhenQuoteAcceptedDto, int>(
                new CreatePolicyWhenQuoteAcceptedDto()
                {
                    PolicyNo = "Pending",
                    Reference = Guid.NewGuid(),
                    QuoteId = entity.Id,
                    QuoteAcceptedLeadActivityId = quoteAcceptanceDto.QuoteAcceptedLeadActivityId
                }).CreatePOSTResponse();
            if (createPolicyWhenQuoteAcceptedDtoResponse.IsSuccess)
            {
                Log.Info(String.Format("DONE PolicyHeader for Quote {0}", entity.Id));
            }
            else
            {
                result.AddErrors(createPolicyWhenQuoteAcceptedDtoResponse.Errors);
                return;
            }

            var acceptanceResponse = m_ExecutionPlan.Execute<PublishQuoteUploadMessageDto, QuoteAcceptanceResponseDto>(quoteAcceptanceDto);

            if (acceptanceResponse.AllErrorDTOMessages != null)
            {
                if (acceptanceResponse.AllErrorDTOMessages.Count > 0)
                {
                    foreach (var error in acceptanceResponse.AllErrorDTOMessages)
                    {
                        Log.Error(string.Format("AcceptanceResponse Error: {0}", error.DefaultMessage));
                    }
                }
            }


            Log.Info("DONE AcceptQuote");

            if (result.Completed)
            {
                var activity = entity.QuoteHeader.Proposal.LeadActivity;

                var activityProxy = activity as INHibernateProxy;

                if (activityProxy != null)
                    activity = (ProposalLeadActivity)activityProxy.HibernateLazyInitializer.GetImplementation();

                //Creating Quote Accepted Lead Activity
                var lead = entity.QuoteHeader.Proposal.LeadActivity.Lead;
                LeadActivity policyActivity = lead.QuoteAccepted(entity.Id, ExecutionContext.UserId, activity.Campaign);
                m_Repository.Save(lead);



                entity.Accept();

                //PSG Changes
                if (bool.Parse(ConfigurationReader.Platform.UnderwritingLeadStatus))
                {
                    policyActivity = lead.SentForUnderwriting(dto.Context.UserId, entity, activity.Campaign);
                    m_Repository.Save(lead);
                }

                RaiseEditAuditLeadLogEvent(entity, dto.ChannelId == 0 ? ExecutionContext.ActiveChannelId : dto.ChannelId);
            }
            result.Processed(acceptanceResponse.Response);
        }

        private PublishQuoteUploadMessageDto GetQuoteDto(Quote quote, AcceptQuoteDto dto)
        {

            var quoteDto = new PublishQuoteUploadMessageDto();
            {
                try
                {
                    quoteDto.Id = quote.Id;
                    quoteDto.QuoteAcceptanceEnvironment = ConfigurationReader.Platform.QuoteAcceptanceEnvironment;
                    quoteDto.WorkflowEnvironment = ConfigurationReader.Platform.WorkflowEnvironment;
                    quoteDto.Environment = ConfigurationReader.Platform.iPlatformEnvironment;

                    //ECHO TCF configs
                    quoteDto.EchoTCF = new EchoTCFDto(ConfigurationReader.EchoTCF.BaseUrl, ConfigurationReader.EchoTCF.Token, ConfigurationReader.EchoTCF.ByPass);
                    quoteDto.AgentDetail = new AgentDetailDto(ExecutionContext.UserId, ExecutionContext.Username, ExecutionContext.UserFullname) { Comments = dto.Comments };
                    //Agent Info who accepted the quote

                    var source = quote.QuoteHeader.Proposal.Source;
                    if (string.IsNullOrWhiteSpace(source))
                        source = "iplatform";

                    quoteDto.Policy = new RatingResultPolicyDto
                    {
                        ProductId = quote.Product.Id,
                        Source = source,
                        InsurerCode = quote.Product.ProductOwner.Code,
                        InsurerName = quote.Product.ProductOwner.RegisteredName,
                        ProductCode = quote.Product.ProductCode,
                        ProductName = quote.Product.Name,
                        ExtraITCScore = quote.ExtraITCScore,
                        ITCScore = quote.ITCScore,
                        InsurerReference = quote.InsurerReference,
                        ExternalRatingResult = quote.ExternalRatingResult,
                        ExternalRatingRequest = quote.ExternalRatingRequest,
                        Fees = quote.Fees,
                        EffectiveDate = DateTime.UtcNow.Date, //need to correct for endorsements etc
                        CreatePolicy = true, //needs to be updated when dev for IPL-545 is done
                        Items = new List<RatingResultItemDto>()
                    };

                    Log.InfoFormat("{0} DONE for quote {1}", "RatingResultPolicyDto", quote.Id);

                    List<SecondLevelQuestionSavedAnswer> allSecondLevelResponsesForQuote =
                        m_Repository.GetAll<SecondLevelQuestionSavedAnswer>()
                        .Where(sla => sla.Quote.Id == quote.Id)
                        .ToList() ?? new List<SecondLevelQuestionSavedAnswer>();

                    foreach (QuoteItem item in quote.Items)
                    {
                        quoteDto.Policy.Items.Add(Mapper.Map<RatingResultItemDto>(item));

                        var x = new QuoteItemDto
                        {
                            Premium = item.Fees.Premium,
                            DiscountedPremium = item.Fees.DiscountedPremium,
                            DiscountedPercentage = item.Fees.DiscountedPercentage,
                            Sasria = item.Fees.Sasria,
                            SasriaShortfall = item.Fees.SasriaShortfall,
                            SasriaCalulated = item.Fees.CalculatedSasria,
                            SumInsured = item.Asset.SumInsured,
                            SumInsuredDescription = item.SumInsuredDescription,
                            AssetNumber = item.Asset.AssetNumber,
                            ExcessBasic = item.Excess.Basic,
                            ExcessCalculated = item.Excess.IsCalculated,
                            ExcessDescription = item.Excess.Description,
                            Cover = item.CoverDefinition.Cover,
                        };

                        x.SecondLevelResponses = new List<SecondLevelQuestionResponseDto>
                            (
                            allSecondLevelResponsesForQuote
                                .Where(sla => sla.QuoteItem != null && sla.QuoteItem.Id == item.Id)
                                .Select(sla => new SecondLevelQuestionResponseDto
                                {
                                    Question = sla.SecondLevelQuestion,
                                    Answer = sla.Answer
                                })
                            );

                        quoteDto.Items.Add(x);

                    }

                    Log.InfoFormat("{0} DONE for quote {1}", "List<RatingResultItemDto>", quote.Id);

                    LeadActivity leadActivity = quote.QuoteHeader.Proposal.LeadActivity;

                    quoteDto.LeadId = leadActivity.Lead.Id;

                    if (leadActivity.Lead.Party.PartyType == PartyTypes.Individual)
                    {
                        var party = leadActivity.Lead.Party as Individual;

                        if (party == null)
                        {
                            var individual = leadActivity.Lead.Party as INHibernateProxy;
                            if (individual != null)
                                party = individual.HibernateLazyInitializer.GetImplementation() as Individual;
                        }

                        quoteDto.InsuredInfo = Mapper.Map<InsuredInfoDto>(party);


                        List<PartyCorrespondencePreference> savedPreferences =
                            m_Repository.GetAll<PartyCorrespondencePreference>()
                                .Where(a => a.Party.Id == party.Id)
                                .ToList();

                        foreach (PartyCorrespondencePreference pref in savedPreferences)
                        {
                            quoteDto.InsuredInfo.CommunicationPreferences.Add(Mapper.Map<PartyCorrespondencePreferenceDto>(pref));
                        }
                    }

                    Log.InfoFormat("{0} DONE for quote {1}", "InsuredInfoDto", quote.Id);

                    PaymentDetails paymentDetails = m_Repository.GetAll<PaymentDetails>().FirstOrDefault(a => a.Quote.Id == quote.Id);

                    quoteDto.PaymentDetailsInfo = Mapper.Map<PaymentDetailsInfoDto>(paymentDetails);


                    Log.InfoFormat("{0} DONE for quote {1}", "PaymentDetails", quote.Id);

                    ProposalHeader header =
                        repository.GetAll<ProposalHeader>().FirstOrDefault(a => a.LeadActivity.Id == leadActivity.Id);
                    Log.InfoFormat("{0} DONE Lead Activity Id {1}", "ProposalHeader", leadActivity.Id);

                    var channel = repository.GetById<Channel>(dto.ChannelId);

                    quoteDto.Request = new RatingRequestDtoBuilder(repository).Build(quote.QuoteHeader.Proposal, String.Empty, quote.QuoteHeader.Proposal.RatingDate, channel, true);

                    GetRatingUserConfiguration(quoteDto.Request);

                    //BUILD THE MapVapQuestionDefinition here because DEWALD E FRAKED UP !!!!!
                    var mapvapSettingsDto = Mapper.Map<List<MapVapQuestionDefinitionDto>>(repository.GetAll<MapVapQuestionDefinition>()
                            .Where(x => x.IsDeleted != true
                                        && x.Product.ProductCode == quoteDto.Policy.ProductCode
                                        && x.Channel.Id == channel.Id
                                        && x.Enabled).ToList());

                    foreach (var vap in mapvapSettingsDto)
                    {
                        foreach (var item in quoteDto.Request.Items)
                        {
                            foreach (var answer in item.Answers)
                            {
                                if (vap.QuestionDefinition.Question.Id == answer.Question.Id)
                                {
                                    var didUserTickYes = false;

                                    if (Boolean.TryParse(answer.QuestionAnswer.ToString(), out didUserTickYes))
                                    {
                                        if (didUserTickYes)
                                        {
                                            item.MapVapQuestionDefinition.Add(vap);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //pass existing external reference as rating requestid
                    if (new Guid(quote.QuoteHeader.ExternalReference) != Guid.Empty)
                        quoteDto.Request.Id = Guid.Parse(quote.QuoteHeader.ExternalReference);


                    quoteDto.Request.RatingContext = new RatingContextDto() { ChannelId = channel.SystemId };

                    Log.InfoFormat("{0} DONE for quote {1}", "header", quote.Id);

                    quoteDto.Request.PlatformId = quote.PlatformId;

                    //get user info used for echo tcf
                    var userDisplayName = string.Empty;

                    var userIndividual = m_Repository.GetAll<UserIndividual>().FirstOrDefault(x => x.User.Id == ExecutionContext.UserId);
                    userDisplayName = userIndividual != null ? userIndividual.Individual.DisplayName : string.Empty;

                    if (userIndividual == null)
                    {
                        var contact = m_Repository.GetAll<Contact>().FirstOrDefault(x => x.ContactDetail.Email == ExecutionContext.Username);
                        userDisplayName = contact != null ? contact.DisplayName : string.Empty;
                    }

                    quoteDto.CampaignInfo.AgentName = string.IsNullOrEmpty(userDisplayName) ? ExecutionContext.Username : userDisplayName;

                    //get campaign
                    var partyCampaign = m_Repository.GetAll<PartyCampaign>().FirstOrDefault(x => x.Party.Id == quote.QuoteHeader.Proposal.Party.Id);
                    quoteDto.CampaignInfo.CampaignReference = partyCampaign != null
                        ? partyCampaign.Campaign.Reference
                        : String.Empty;


                    //Get ChannelInfo
                    if (channel != null)
                        quoteDto.ChannelInfo = Mapper.Map<ChannelDto>(channel);

                }
                catch (Exception e)
                {
                    Log.ErrorFormat("Unable to GetRatingQuoteDto for Quote {0}, {1}, {2}", quote.Id, e.Message,
                        e.InnerException);
                    throw;
                }
            }
            Log.InfoFormat("Return QuoteDto");
            return quoteDto;
        }

        private void RaiseEditAuditLeadLogEvent(Quote quote, int channelId)
        {
            var channel = m_Repository.GetById<Channel>(channelId);
            EditAuditLeadLogDto editAuditLeadLogDto = new EditAuditLeadLogDto()
            {
                LeadId = quote.QuoteHeader.Proposal.LeadActivity.Lead.Id,
                Timestamp = DateTime.UtcNow,
                IsAccepted = quote.IsAccepted,
                ProductId = quote.Product.Id,
                ProductName = quote.Product.Name,
                ProductCode = quote.Product.ProductCode,
                SystemId = channel.SystemId,
                EnvironmentType = new AdminConfigurationReader().EnvironmentTypeKey
            };
            quote.RaiseEvent(new EditAuditLeadLogEvent(editAuditLeadLogDto));
        }

        private void GetRatingUserConfiguration(RatingRequestDto dto)
        {
            var ratingsUserSettingsQuery = new GetRatingUserConfigurationByUser(m_Context, m_Repository);
            var user = m_Context.Get().UserId;
            var ratingUserConfiguration = ratingsUserSettingsQuery.ForUser(user).ExecuteQuery().ToList();
            dto.UserRatingSettings = Mapper.Map<List<SettingsiRateUser>, List<SettingsiRateUserDto>>(ratingUserConfiguration);
        }
    }

}