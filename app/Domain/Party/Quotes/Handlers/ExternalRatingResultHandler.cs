﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using AutoMapper;
using Domain.Admin;
using Domain.Admin.ChannelEvents;
using Domain.Admin.Queries;
using Domain.Admin.SettingsQuoteUploads;
using Domain.Admin.SettingsQuoteUploads.Queries;
using Domain.Base.Culture;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using Domain.Ratings.Events;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.QuoteAcceptance;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using Infrastructure.Configuration;
using MasterData.Authorisation;
using ValidationMessages.Channels;

namespace Domain.Party.Quotes.Handlers
{
    public class ExternalRatingResultHandler : BaseDtoHandler<RatingResultDto, bool>
    {
        private readonly GetChannelEventConfigurationQuery m_ChannelEventConfigurationQuery;
        private readonly GetSettingsQuoteAcceptanceQuery m_SettingsQuoteAcceptanceQuery;
        private readonly IRepository m_Repository;
        private readonly IPublishEvents m_EventPublisher;

        public ExternalRatingResultHandler(
             IRepository repository, IProvideContext contextProvider, IPublishEvents eventPublisher, GetChannelEventConfigurationQuery channelEventConfigurationQuery, GetSettingsQuoteAcceptanceQuery settingsQuoteAcceptanceQuery) : base(contextProvider)
        {

            this.m_Repository = repository;
            this.m_EventPublisher = eventPublisher;
            this.m_ChannelEventConfigurationQuery = channelEventConfigurationQuery;
            this.m_SettingsQuoteAcceptanceQuery = settingsQuoteAcceptanceQuery;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    QuotingAuthorisation.CanQuote
                };
            }
        }

        protected override void InternalHandle(RatingResultDto dto, HandlerResult<bool> result)
        {
            AcceptQuote(dto);

            result.Processed(true);
        }

        private void AcceptQuote(RatingResultDto dto)
        {
            var quoteAcceptanceDto = GetQuoteDto(dto);
            quoteAcceptanceDto.SetContext(dto.Context);
            quoteAcceptanceDto.SetNumberFormat(dto.Context.GetCultureParam());
            // used for quote acceptance email notification fees, premiums currency symbol

            //Used for cimsconnect to reply to for policyno/quoteno
            quoteAcceptanceDto.ApiUrl = ConfigurationReader.Connector.BaseUrl;
            var channelReference = new Channel(ExecutionContext.ActiveChannelId);
            var policy = dto.Policies.FirstOrDefault();

            var channelEvents = GetChannelEventConfiguration(ExecutionContext.ActiveChannelId, policy.ProductCode);
            channelReference.AddChannelConfiguration(channelEvents);


            var @event = new ExternalQuoteAcceptedWithUnderwritingEnabledEvent(
                 quoteAcceptanceDto,
                 quoteAcceptanceDto.Policy.ProductCode,
                 quoteAcceptanceDto.Policy.InsurerCode,
                 ExecutionContext.CreateAudit(),
                 channelReference,
                 GetSettingsQuoteUpload(quoteAcceptanceDto)
                 );

            m_EventPublisher.Publish(@event);


            Log.Info("DONE Accept External Quote");
        }

        private List<ChannelEvent> GetChannelEventConfiguration(int channelId, string productCode)
        {

            var channelEvents = m_ChannelEventConfigurationQuery
                .ForChannel(channelId)
                .ForProduct(productCode)
                .EventName(typeof(ExternalQuoteAcceptedWithUnderwritingEnabledEvent).Name)
                .ExecuteQuery()
                .ToList();

            if (!channelEvents.Any())
            {
                throw new ValidationException(ChannelValidationMessages.NoChannelEventConfiguration(typeof(ExternalQuoteAcceptedWithUnderwritingEnabledEvent).Name));
            }

            return channelEvents;
        }

        private PublishQuoteUploadMessageDto GetQuoteDto(RatingResultDto dto)
        {
            var policy = dto.Policies.FirstOrDefault();

            var productid = policy.ProductCode;


            var product = m_Repository.GetAll<Product>().FirstOrDefault(p => p.ProductCode == productid);

            Quote quote = CreateQuote(new QuoteHeader(), product, policy);
            var quoteDto = new PublishQuoteUploadMessageDto();
            {
                try
                {
                    quoteDto.Id = quote.Id;
                    quoteDto.QuoteAcceptanceEnvironment = ConfigurationReader.Platform.QuoteAcceptanceEnvironment;
                    quoteDto.WorkflowEnvironment = ConfigurationReader.Platform.WorkflowEnvironment;
                    quoteDto.Environment = ConfigurationReader.Platform.iPlatformEnvironment;

                    quoteDto.Request.Policy.Persons = new List<RatingRequestPersonDto>()
                    {
                        new RatingRequestPersonDto() { PolicyHolder = true, Initials = "", Surname = "", IdNumber = ""}
                    };
                    //ECHO TCF configs
                    quoteDto.EchoTCF = new EchoTCFDto(ConfigurationReader.EchoTCF.BaseUrl, ConfigurationReader.EchoTCF.Token, ConfigurationReader.EchoTCF.ByPass);

                    quoteDto.AgentDetail = new AgentDetailDto(ExecutionContext.UserId, ExecutionContext.Username, ExecutionContext.UserFullname) { Comments = "" };

                    var source = "External Quote";

                    quoteDto.Policy = new RatingResultPolicyDto
                    {
                        ProductId = quote.Product.Id,
                        Source = source,
                        InsurerCode = quote.Product.ProductOwner.Code,
                        InsurerName = quote.Product.ProductOwner.RegisteredName,
                        ProductCode = quote.Product.ProductCode,
                        ProductName = quote.Product.Name,
                        ExtraITCScore = quote.ExtraITCScore,
                        ITCScore = quote.ITCScore,
                        InsurerReference = quote.InsurerReference,
                        ExternalRatingResult = quote.ExternalRatingResult,
                        ExternalRatingRequest = quote.ExternalRatingRequest,
                        Fees = quote.Fees,
                        EffectiveDate = DateTime.UtcNow.Date, 
                        CreatePolicy = true,
                        Items = new List<RatingResultItemDto>()
                    };

                    var channel = m_Repository.GetById<Channel>(ExecutionContext.ActiveChannelId);


                    quoteDto.Request.RatingContext = new RatingContextDto() { ChannelId = channel.SystemId };


                    quoteDto.Request.PlatformId = quote.PlatformId;


                    quoteDto.CampaignInfo.AgentName = ExecutionContext.Username;

                    quoteDto.ChannelInfo = Mapper.Map<ChannelDto>(channel);

                }
                catch (Exception e)
                {
                    Log.ErrorFormat("Unable to GetRatingQuoteDto for Quote {0}, {1}, {2}", quote.Id, e.Message,
                        e.InnerException);
                    throw;
                }
            }
            Log.InfoFormat("Return QuoteDto");
            return quoteDto;
        }

        private static Quote CreateQuote(QuoteHeader quoteHeader, Product currentProduct, RatingResultPolicyDto policy)
        {
            var xmlDoc = new XmlDocument();

            if (!string.IsNullOrEmpty(policy.ExternalRatingResult))
            {
                xmlDoc.LoadXml(policy.ExternalRatingResult);
            }

            var quote = new Quote(quoteHeader, currentProduct, policy.Fees, policy.InsurerReference,
                policy.ITCScore,
                policy.ExtraITCScore, policy.State.Errors, policy.State.Warnings)
            {
                ExternalRatingResult = xmlDoc.OuterXml,
                ExternalRatingRequest = policy.ExternalRatingRequest
            };

            return quote;
        }

        private List<SettingsQuoteUploadDto> GetSettingsQuoteUpload(PublishQuoteUploadMessageDto dto)
        {
            var settings = m_SettingsQuoteAcceptanceQuery
                                .WithChannelId(dto.ChannelInfo.Id)
                                .WithProductId(dto.Policy.ProductId)
                                .WithEnvironment(dto.Environment)
                                .IsPolicyBinding(false)
                                .ExecuteQuery().ToList();
            if (!settings.Any())
            {
                Log.ErrorFormat("No SettingsQuoteUpload found for Channel:{0} and Product:{1} and Environment:{2}", dto.ChannelInfo.Id, dto.Policy.ProductId, dto.Environment);
            }

            return GetSettingsQuoteUploadDto(settings);
        }

        private List<SettingsQuoteUploadDto> GetSettingsQuoteUploadDto(List<SettingsQuoteUpload> settings)
        {
            var settingsDto = new List<SettingsQuoteUploadDto>();

            settings.ForEach(x =>
            {
                settingsDto.Add(new SettingsQuoteUploadDto()
                {
                    SettingName = x.SettingName,
                    SettingValue = x.SettingValue
                });
            });

            //Adding SettingQuoteUpload Logging
            if (settingsDto.Any())
            {
                Log.InfoFormat("Using Settings below for Upload/Acceptance");
            }

            settingsDto.ForEach(x =>
            {
                Log.InfoFormat("NAME/VALUE: {0} : {1}", x.SettingName, x.SettingValue);
            });

            return settingsDto;
        }
    }


}