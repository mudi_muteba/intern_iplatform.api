﻿using System;
using Domain.Base.Events;
using Domain.Base.Repository;
using Domain.Leads;
using Domain.Campaigns;

namespace Domain.Party.Quotes.Events
{
    public class SentforUnderwritingEventHandler : BaseEventHandler<SentforUnderwritingEvent>
    {
        public SentforUnderwritingEventHandler(IRepository repository)
        {
            _repository = repository;
        }
        private readonly IRepository _repository;
        public override void Handle(SentforUnderwritingEvent @event)
        {
        }
    }
}