﻿namespace Domain.Party.Quotes
{
    public class QuoteItemAsset
    {
        public QuoteItemAsset()
        {
        }

        public QuoteItemAsset(int assetNumber, decimal sumInsurered, string description)
        {
            AssetNumber = assetNumber;
            SumInsured = sumInsurered;
            Description = description;
        }

        public string Description { get; protected internal set; }
        public int AssetNumber { get; protected internal set; }
        public decimal SumInsured { get; protected internal set; }
    }
}