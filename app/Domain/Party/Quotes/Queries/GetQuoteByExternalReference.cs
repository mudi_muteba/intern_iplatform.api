﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Quotes.Queries
{
    public class GetQuoteByExternalReference : BaseQuery<QuoteHeader>
    {
        private string externalReference;

        public GetQuoteByExternalReference(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<QuoteHeader>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    QuotingAuthorisation.CanQuote
                };
            }
        }

        public GetQuoteByExternalReference WithExternalReference(string externalReference)
        {
            this.externalReference = externalReference;
            return this;
        }

        protected internal override IQueryable<QuoteHeader> Execute(IQueryable<QuoteHeader> query)
        {
            return query
                .Where(q => q.ExternalReference == externalReference);
        }
    }
}