﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Party.Quotes.Queries
{
    public static class QuoteQueries
    {
        public static bool HasQuotesForProposalId(this IQueryable<QuoteHeader> quotes,
            int proposalId)
        {
            //TODO investigate how to make this query smaller 
            return quotes
                .Any(q => q.Proposal.Id == proposalId);
        }

    }
}
