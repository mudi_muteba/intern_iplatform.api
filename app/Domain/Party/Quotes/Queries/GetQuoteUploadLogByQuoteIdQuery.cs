﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Quotes.Queries
{
    public class GetQuoteUploadLogByQuoteIdQuery : BaseQuery<QuoteUploadLog>
    {
        private int quoteId;

        public GetQuoteUploadLogByQuoteIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<QuoteUploadLog>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetQuoteUploadLogByQuoteIdQuery WithQuoteId(int quoteId)
        {
            this.quoteId = quoteId;
            return this;
        }

        protected internal override IQueryable<QuoteUploadLog> Execute(IQueryable<QuoteUploadLog> query)
        {
            return query
                .Where(q => q.Quote.Id == quoteId);
        }
    }
}