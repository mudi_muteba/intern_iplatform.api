﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Quotes.Queries
{
    public class GetQuoteByIdQuery : BaseQuery<QuoteHeader>
    {
        private int quoteId;

        public GetQuoteByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<QuoteHeader>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetQuoteByIdQuery WithQuoteId(int quoteId)
        {
            this.quoteId = quoteId;
            return this;
        }

        protected internal override IQueryable<QuoteHeader> Execute(IQueryable<QuoteHeader> query)
        {
            return query.Where(q => q.Id == quoteId);
        }
    }
}