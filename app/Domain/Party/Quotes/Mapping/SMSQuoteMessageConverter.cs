﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Common.Logging;

using Domain.Admin.Channels.SMSCommunicationSettings.Queries;

using Domain.Party.Quotes.Events;
using Domain.Party.Quotes.Workflow.Messages;
using Domain.Party.Quotes.Workflow.Tasks;

using iPlatform.OutboundCommunication.SMS;

namespace Domain.Party.Quotes.Mapping
{
    public class SMSQuoteMessageConverter : TypeConverter<QuoteDistributionViaSMSRequestedEvent, SMSQuoteMessage>
    {
        private readonly ILog _log;
        private readonly GetSMSCommunicationSettingsQuery _query;

        public SMSQuoteMessageConverter(GetSMSCommunicationSettingsQuery query)
        {
            _log = LogManager.GetLogger(GetType());
            _query = query;
        }

        protected override SMSQuoteMessage ConvertCore(QuoteDistributionViaSMSRequestedEvent @event)
        {
            var settings = _query.WithChannelId(@event.ChannelId).ExecuteQuery().ToList();
            if (!settings.Any())
                _log.WarnFormat("Not communication settings found for channel {0}", @event.ChannelId);

            var providers = new List<SMSProviderConfiguration>(settings.Select(s => new SMSProviderConfiguration(s.SMSProviderName, s.SMSUserName, s.SMSPassword, s.SMSUrl)));
            var metadata = new DistributeQuoteViaSMSTaskMetadata(providers
                , @event.QuoteId
                , @event.ExternalReference
                , @event.CellPhoneNumber
                , @event.Quotes);

            return new SMSQuoteMessage(metadata);
        }
    }
}