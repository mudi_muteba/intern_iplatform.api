﻿using System;
using System.Text;
using AutoMapper;
using Domain.Products;
using iPlatform.Api.DTOs.Ratings.Quoting;

namespace Domain.Party.Quotes.Mapping
{
    public class QuoteUploadLogMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateQuoteUploadLogDto, QuoteUploadLog>()
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product {Id = s.ProductId}))
                .ForMember(t => t.Quote, o => o.MapFrom(s => new Quote { Id = s.QuoteId} ))
                .ForMember(t => t.DateCreated, o => o.MapFrom(s => DateTime.UtcNow))
                .ForMember(t => t.QuoteUploadObject, o => o.MapFrom(s => Encoding.UTF8.GetBytes(s.QuoteUploadObject)))
                ;
        }
    }
}