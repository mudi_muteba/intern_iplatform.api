﻿using System;
using System.Collections.Generic;
using System.Globalization;
using AutoMapper;
using Domain.Individuals;
using Domain.Party.Payments;
using Domain.Products;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Ratings.AdditionalInfo;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Response;
using QuoteDto = iPlatform.Api.DTOs.Party.Quotes.QuoteDto;
using QuoteItemDto = iPlatform.Api.DTOs.Party.Quotes.QuoteItemDto;

namespace Domain.Party.Quotes.Mapping
{
    public class QuoteMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<QuoteHeader, QuoteHeaderDto>()
                .ForMember(t => t.ProposalId, o => o.MapFrom(s => s.Proposal.Id));

            Mapper.CreateMap<Quote, QuoteDto>()
                .ForMember(t => t.Product, o => o.MapFrom(s => new AboutProductDto
                {
                    Name = s.Product.Name,
                    Code = s.Product.ProductCode,
                    ProductType = s.Product.ProductType.Name,
                    Provider = new ProductProviderDto
                    {
                        Code = s.Product.ProductProvider.Code,
                        Id = s.Product.ProductProvider.Id,
                        RegisteredName = s.Product.ProductProvider.RegisteredName,
                        Description = s.Product.ProductProvider.Description,
                        TradingName = s.Product.ProductProvider.TradingName
                    },
                    Images = new List<ProductImageDto>
                    {
                        new ProductImageDto
                        {
                            Url = s.Product.ImageName
                        }
                    }
                }))
                .ForMember(t => t.Fees, o => o.MapFrom(s => new MoneyDto(s.Fees)))
                ;
            Mapper.CreateMap<QuoteItemBreakDown, QuoteItemBreakDownDto>()
                .ForMember(t => t.ExternalId,
                    o => o.MapFrom(s => s.ExternalId))
                .ForMember(t => t.Markup,
                    o => o.MapFrom(s => new MoneyDto(Math.Round(Convert.ToDecimal(s.Markup, CultureInfo.InvariantCulture), 2))))
                .ForMember(t => t.Risk,
                    o => o.MapFrom(s => new MoneyDto(Math.Round(Convert.ToDecimal(s.Risk, CultureInfo.InvariantCulture), 2))))
                .ForMember(t => t.Total,
                    o => o.MapFrom(s => new MoneyDto(Math.Round(Convert.ToDecimal(s.Total, CultureInfo.InvariantCulture),2) )));


            Mapper.CreateMap<QuoteState, QuoteStateDto>();
            Mapper.CreateMap<QuoteStateEntry, QuoteStateEntryDto>();
            Mapper.CreateMap<QuoteItem, QuoteItemDto>()
                .ForMember(t => t.Fees, o => o.MapFrom(s => s.Fees))
                .ForMember(t => t.IsVap, o => o.MapFrom(s => s.IsVap))
                .ForMember(t => t.Clauses, o => o.MapFrom(s => s.GetClauses()))
                ;
            Mapper.CreateMap<ProductBenefit, ProductBenefitDto>();
            Mapper.CreateMap<QuoteDistribution, QuoteDistributionDto>();

            Mapper.CreateMap<CoverDefinition, QuoteCoverDefinitionDto>();
            Mapper.CreateMap<QuoteItemState, QuoteItemStateDto>();

            Mapper.CreateMap<QuoteItemFees, QuoteItemFeesDto>()
                .ForMember(t => t.OriginalPremium, o => o.MapFrom(s => new MoneyDto(s.OriginalPremium)))
                .ForMember(t => t.Premium, o => o.MapFrom(s => new MoneyDto(s.Premium)))
                .ForMember(t => t.Sasria, o => o.MapFrom(s => new MoneyDto(s.Sasria)))
                ;
            Mapper.CreateMap<QuoteItemExcess, QuoteItemExcessDto>()
                .ForMember(t => t.Basic, o => o.MapFrom(s => new MoneyDto(s.Basic)))
                .ForMember(t => t.VoluntaryExcess, o => o.MapFrom(s => new MoneyDto(s.VoluntaryExcess)))
                ;
            Mapper.CreateMap<QuoteItemAsset, QuoteItemAssetDto>()
                .ForMember(t => t.SumInsured, o => o.MapFrom(s => new MoneyDto(s.SumInsured)))
                ;
            Mapper.CreateMap<QuoteItemStateEntry, QuoteItemStateEntryDto>();
            Mapper.CreateMap<QuoteHeaderState, QuoteHeaderStateDto>();
            Mapper.CreateMap<QuoteHeaderStateEntry, QuoteHeaderStateEntryDto>();
            Mapper.CreateMap<QuoteAcceptance, PublishQuoteUploadMessageDto>();
            Mapper.CreateMap<QuoteAcceptance, QuoteAcceptedDto>();

            //Ratings

            Mapper.CreateMap<QuoteItem, RatingResultItemDto>()
                .ForMember(t => t.AssetNo, o => o.MapFrom(s => s.Asset.AssetNumber))
                .ForMember(t => t.Cover, o => o.MapFrom(s => s.CoverDefinition.Cover))
                .ForMember(t => t.Excess, o => o.Ignore())
                .ForMember(t => t.Fees, o => o.Ignore())
                .ForMember(t => t.Premium, o => o.MapFrom(s => s.Fees.Premium))
                .ForMember(t => t.DiscountedPremium, o => o.MapFrom(s => s.Fees.DiscountedPremium))
                .ForMember(t => t.DiscountedPercentage, o => o.MapFrom(s => s.Fees.DiscountedPercentage))
                .ForMember(t => t.DiscountedDifference, o => o.MapFrom(s => s.Fees.DiscountedDifference))
                .ForMember(t => t.Sasria, o => o.MapFrom(s => s.Fees.Sasria))
                .ForMember(t => t.SasriaShortfall, o => o.MapFrom(s => s.Fees.SasriaShortfall))
                .ForMember(t => t.SasriaCalulated, o => o.MapFrom(s => s.Fees.CalculatedSasria))
                .ForMember(t => t.SumInsured, o => o.MapFrom(s => s.Asset.SumInsured))
                .ForMember(t => t.VapQuestionId, o => o.MapFrom(s => s.VapQuestionId))
                .ForMember(t => t.SumInsuredDescription, o => o.MapFrom(s => s.SumInsuredDescription))
                .AfterMap((s, t) =>
                {
                    t.Excess = new RatingResultItemExcessDto
                    {
                        Basic = s.Excess.Basic,
                        Calculated = s.Excess.IsCalculated,
                        Description = s.Excess.Description
                    };
                });

            Mapper.CreateMap<Individual, InsuredInfoDto>()
                .ForMember(t => t.ContactNumber,
                    o => o.MapFrom(s => s.ContactDetail != null ? s.ContactDetail.Cell : string.Empty))
                .ForMember(t => t.EmailAddress,
                    o => o.MapFrom(s => s.ContactDetail != null ? s.ContactDetail.Email : string.Empty))
                .ForMember(t => t.IDNumber, o => o.MapFrom(s => s.IdentityNo))
                .ForMember(t => t.ITCConsent, o => o.MapFrom(s => true)) //TO DO
                ;

            Mapper.CreateMap<PaymentDetails, PaymentDetailsInfoDto>()
                .ForMember(t => t.DebitOrderDate, o => o.MapFrom(s =>s.DebitOrderDate))
                .ForMember(t => t.InceptionDate, o => o.MapFrom(s => new DateTimeDto(s.InceptionDate)))
                .ForMember(t => t.MethodOfPayment, o => o.MapFrom(s => s.MethodOfPayment))
                .ForMember(t => t.PaymentPlan, o => o.MapFrom(s => s.PaymentPlan))
                .ForMember(t => t.BankAccHolder, o => o.MapFrom(s => s.BankDetails == null ? String.Empty : s.BankDetails.BankAccHolder))
                .ForMember(t => t.Bank, o => o.MapFrom(s => s.BankDetails == null ? String.Empty : s.BankDetails.Bank))
                .ForMember(t => t.BankBranch, o => o.MapFrom(s => s.BankDetails == null ? String.Empty : s.BankDetails.BankBranch))
                .ForMember(t => t.BankBranchCode, o => o.MapFrom(s => s.BankDetails == null ? String.Empty : s.BankDetails.BankBranchCode))
                .ForMember(t => t.AccountNo, o => o.MapFrom(s => s.BankDetails == null ? String.Empty : s.BankDetails.AccountNo))
                .ForMember(t => t.TypeAccount, o => o.MapFrom(s => s.BankDetails == null ? String.Empty : s.BankDetails.TypeAccount))
                .ForMember(t => t.TermEndDate, o => o.MapFrom(s => new DateTimeDto(s.TermEndDate)))
                .ForMember(t => t.TakeOnStrikeDate, o => o.MapFrom(s => s.TakeOnStrikeDate));


            Mapper.CreateMap<PublishQuoteUploadMessageDto, iPlatform.Api.DTOs.Ratings.Quoting.QuoteDto>()
                .AfterMap((s, d) => { d.Request.Id = s.Request.PlatformId; })
                ;
        }
    }
}