﻿using AutoMapper;
using Domain.Party.Quotes.Events;
using Domain.Party.Quotes.Workflow.Messages;

namespace Domain.Party.Quotes.Mapping
{
    public class WorkflowMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<QuoteDistributionViaSMSRequestedEvent, SMSQuoteMessage>()
                .ConvertUsing<ITypeConverter<QuoteDistributionViaSMSRequestedEvent, SMSQuoteMessage>>();
        }
    }
}