﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base;

namespace Domain.Party.Quotes
{
    public class QuoteState : Entity
    {
        protected QuoteState()
        {
            Entries = new List<QuoteStateEntry>();
        }

        public QuoteState(Quote quote) : this()
        {
            Quote = quote;
        }

        public virtual IList<QuoteStateEntry> Entries { get; protected internal set; }

        public virtual bool Warnings
        {
            get { return Entries.Any(e => !e.IsError); }
            protected internal set { }
        }

        public virtual bool Errors
        {
            get { return Entries.Any(e => e.IsError); }
            protected internal set { }
        }

        public virtual Quote Quote { get; protected internal set; }

        public virtual void AddError(string error)
        {
            Entries.Add(QuoteStateEntry.CreateError(this, error));
        }

        public virtual void AddErrors(IEnumerable<string> errors)
        {
            foreach (var error in errors)
            {
                AddError(error);
            }
        }

        public virtual void AddWarning(string warning)
        {
            Entries.Add(QuoteStateEntry.CreateWarning(this, warning));
        }

        public virtual void AddWarnings(IEnumerable<string> warnings)
        {
            foreach (var warning in warnings)
            {
                AddWarning(warning);
            }
        }
    }
}