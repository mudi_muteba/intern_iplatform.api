﻿using System;

namespace Domain.Party.Quotes
{
    public class QuoteAcceptance
    {
        public QuoteAcceptance()
        {
            AcceptedOn = null;
        }

        private QuoteAcceptance(DateTime acceptedOn, int acceptedBy)
        {
            AcceptedOn = acceptedOn;
            AcceptedBy = acceptedBy;
            IsAccepted = true;
        }

        public virtual DateTime? AcceptedOn { get; protected internal set; }
        public virtual bool IsAccepted { get; protected internal set; }
        public virtual int AcceptedBy { get; protected internal set; }

        // accepted by should be user
        public static QuoteAcceptance Accepted(int acceptedBy)
        {
            return new QuoteAcceptance(DateTime.UtcNow, acceptedBy);
        }

        public static QuoteAcceptance NotAccepted()
        {
            return new QuoteAcceptance();
        }
    }
}