﻿using Domain.Base;

namespace Domain.Party.Quotes
{
    public class QuoteHeaderStateEntry : Entity
    {
        public QuoteHeaderStateEntry()
        {
        }

        private QuoteHeaderStateEntry(QuoteHeaderState quoteHeaderState, string message, bool isError)
        {
            QuoteHeaderState = quoteHeaderState;
            Message = message;
            IsError = isError;
        }

        public virtual QuoteHeaderState QuoteHeaderState { get; protected internal set; }
        public virtual string Message { get; protected internal set; }
        public virtual bool IsError { get; protected internal set; }

        public static QuoteHeaderStateEntry CreateError(QuoteHeaderState quoteHeader, string errorMessage)
        {
            return new QuoteHeaderStateEntry(quoteHeader, errorMessage, true);
        }

        public static QuoteHeaderStateEntry CreateWarning(QuoteHeaderState quoteHeader, string warningMessage)
        {
            return new QuoteHeaderStateEntry(quoteHeader, warningMessage, false);
        }
    }
}