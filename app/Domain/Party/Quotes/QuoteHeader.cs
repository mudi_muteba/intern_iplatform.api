﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base;
using Domain.Base.Events;
using Domain.Party.ProposalHeaders;
using Domain.Party.Quotes.Events;
using Domain.Products;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Party.Quotes
{
    public class QuoteHeader : EntityWithProposal
    {
        public QuoteHeader()
        {
            Quotes = new List<Quote>();
            State = new QuoteHeaderState();
        }

        public QuoteHeader(string externalReference, ProposalHeader proposal, IEnumerable<string> errors = null, IEnumerable<string> warnings = null)
        {
            Quotes = new List<Quote>();
            CreatedAt = DateTime.UtcNow;
            ModifiedAt = DateTime.UtcNow;
            Proposal = proposal;
            Acceptance = QuoteAcceptance.NotAccepted();
            ExternalReference = externalReference;

            State = new QuoteHeaderState(this);
            State.AddErrors(errors ?? new List<string>());
            State.AddWarnings(warnings ?? new List<string>());
            IsRerated = false;
        }

        public virtual IList<Quote> Quotes { get; protected internal set; }
        public virtual QuoteHeaderState State { get; protected internal set; }
        public virtual DateTime CreatedAt { get; protected internal set; }
        public virtual DateTime ModifiedAt { get; protected internal set; }

        public virtual string ExternalReference { get; protected internal set; }

        public virtual QuoteAcceptance Acceptance { get; protected internal set; }

        public virtual string ExternalClientReference { get; set; }
        public virtual bool IsRerated { get; set; }

        public virtual void AcceptQuote()
        {
            Acceptance = QuoteAcceptance.Accepted(1);
        }
        public virtual void NotAccepted()
        {
            Acceptance = QuoteAcceptance.NotAccepted();
        }
        public virtual void AddQuote(Quote quote)
        {
            quote.BelongsTo(this);
            Quotes.Add(quote);
        }

        public override void Delete()
        {
            IsDeleted = true;
            ModifiedAt = DateTime.UtcNow;

            foreach (var quote in Quotes)
                quote.Delete();

            base.Delete();
        }

        public virtual void Rerate()
        {
            IsRerated = true;
            ModifiedAt = DateTime.UtcNow;
        }

        public static QuoteHeader Create(ProposalHeader proposal, RatingResultDto ratingResultDto)
        {
            return new QuoteHeader(ratingResultDto.RatingRequestId.ToString(), proposal);
        }
        public static QuoteHeader CreateQuoteHeader(Guid requestId, ProposalHeader proposal, RatingResultDto ratings)
        {
            return new QuoteHeader(requestId.ToString(), proposal, ratings.State.Errors, ratings.State.Warnings);
        }

        public virtual Quote CreateQuote(Product product, RatingResultPolicyDto policy)
        {
            var quote = new Quote(this, product, policy.Fees, policy.InsurerReference, policy.ITCScore,
                policy.ExtraITCScore, policy.State.Errors, policy.State.Warnings);

            Quotes.Add(quote);

            return quote;
        }

        public virtual bool HasQuoteFor(string productCode)
        {
            return Quotes.Any(q => q.Product.ProductCode.Equals(productCode, StringComparison.InvariantCultureIgnoreCase));
        }

        public virtual void Distribute(DistributeQuoteDto distributeQuoteDto, Channel channel)
        {
            if (distributeQuoteDto.Via == DistributeQuoteVia.Unknown)
                return;

            var toDistribute = new List<DistributedQuoteEvent>();

            foreach (var productCode in distributeQuoteDto.ProductCodes)
            {
                var quote = Quotes.FirstOrDefault(q => q.Product.ProductCode.Equals(productCode, StringComparison.InvariantCultureIgnoreCase));
                if(quote == null)
                    continue;
                
                toDistribute.Add(new DistributedQuoteEvent(quote.Product.ProductProvider.Code
                    , quote.Product.ProductProvider.TradingName
                    , quote.Product.ProductCode
                    , quote.Product.Name
                    , quote.TotalPremium));
            }

            if(distributeQuoteDto.Via == DistributeQuoteVia.SMS)
            {
                var cellPhoneNumber = string.IsNullOrWhiteSpace(distributeQuoteDto.Contact)
                    ? Proposal.Party.ContactDetail.Cell
                    : distributeQuoteDto.Contact;

                var @event = new QuoteDistributionViaSMSRequestedEvent(Proposal.Party.Channel.Id
                    , Id
                    , ExternalReference
                    , cellPhoneNumber
                    , toDistribute
                    , distributeQuoteDto.CreateAudit()
                    , channel);

                RaiseEvent(@event);

                return;
            }
        }

        public virtual void TrackDistribution(TrackQuoteDistributionDto trackQuoteDistributionDto)
        {
            var quote = Quotes.FirstOrDefault(q => q.Product.ProductCode.Equals(trackQuoteDistributionDto.ProductCode, StringComparison.InvariantCultureIgnoreCase));

            if(quote == null)
                return;

            quote.TrackDistribution(trackQuoteDistributionDto);
        }
    }
}