namespace Domain.Party.Quotes
{
    public class QuoteItemExcess
    {
        public QuoteItemExcess()
        {
        }

        public QuoteItemExcess(bool isCalculated, decimal basic, string description, decimal voluntaryExcess)
        {
            IsCalculated = isCalculated;
            Basic = basic;
            Description = description;
            VoluntaryExcess = voluntaryExcess;
        }

        public virtual bool IsCalculated { get; protected internal set; }
        public virtual decimal Basic { get; protected internal set; }
        public virtual string Description { get; protected internal set; }
        public virtual decimal VoluntaryExcess { get; protected internal set; }
    }
}