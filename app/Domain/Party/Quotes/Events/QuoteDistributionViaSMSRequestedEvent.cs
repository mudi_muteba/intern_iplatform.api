﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Party.Quotes.Events
{
    public class QuoteDistributionViaSMSRequestedEvent : BaseDomainEvent
    {
        protected QuoteDistributionViaSMSRequestedEvent()
        {
        }

        public QuoteDistributionViaSMSRequestedEvent(int channelId
            , int quoteId
            , string externalReference
            , string cellPhoneNumber
            , List<DistributedQuoteEvent> quotes
            , EventAudit audit
            , ChannelReference channelReference) : base(audit, channelReference)
        {
            ChannelId = channelId;
            QuoteId = quoteId;
            ExternalReference = externalReference;
            CellPhoneNumber = cellPhoneNumber;
            Quotes = quotes ?? new List<DistributedQuoteEvent>();
        }

        public int ChannelId { get; set; }
        public int QuoteId { get; set; }
        public string ExternalReference { get; set; }
        public string CellPhoneNumber { get; set; }
        public List<DistributedQuoteEvent> Quotes { get; set; }
    }
}