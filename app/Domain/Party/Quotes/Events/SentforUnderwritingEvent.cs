﻿using Domain.Admin;
using Domain.Base.Events;
using Domain.Campaigns;
using System.Collections.Generic;

namespace Domain.Party.Quotes.Events
{
    public class SentforUnderwritingEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public SentforUnderwritingEvent()
        {
        }

        public SentforUnderwritingEvent(int leadId, int userId, Campaign campaign,  EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            LeadId = leadId;
            UserId = userId;
            Campaign = campaign;
        }

        public int LeadId { get; set; }
        public int UserId { get; set; }
        public Campaign Campaign { get; set; }

    }
}