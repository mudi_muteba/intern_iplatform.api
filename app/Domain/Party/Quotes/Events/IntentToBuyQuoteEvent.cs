﻿using Domain.Base.Events;

namespace Domain.Party.Quotes.Events
{
    public class IntentToBuyQuoteEvent : BaseDomainEvent , ExpressDomainEvent
    {
        public readonly int PartyId;
        public readonly Quote Quote;
        public readonly int UserId;

        public IntentToBuyQuoteEvent()
        {
        }

        public IntentToBuyQuoteEvent(int partyId, Quote quote, int userId)
        {
            PartyId = partyId;
            Quote = quote;
            UserId = userId;
        }
    }
}