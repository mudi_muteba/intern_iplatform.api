﻿namespace Domain.Party.Quotes.Events
{
    public class DistributedQuoteEvent
    {
        public DistributedQuoteEvent()
        {
        }

        public DistributedQuoteEvent(string insurerCode, string insurerName, string productCode, string productName, decimal premium)
        {
            InsurerCode = insurerCode;
            InsurerName = insurerName;
            ProductCode = productCode;
            ProductName = productName;
            Premium = premium;
        }

        public string InsurerCode { get; set; }
        public string InsurerName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal Premium { get; set; }
    }
}