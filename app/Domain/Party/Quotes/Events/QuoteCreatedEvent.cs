﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Party.Quotes.Events
{
    public class QuoteCreatedEvent : BaseDomainEvent ,ExpressDomainEvent
    {
        public readonly int PartyId;
        public readonly QuoteHeader QuoteHeader;
        public readonly int UserId;

        public QuoteCreatedEvent()
        {
        }

        public QuoteCreatedEvent(int partyId, QuoteHeader quoteHeader, int userId)
        {
            PartyId = partyId;
            QuoteHeader = quoteHeader;
            UserId = userId;
        }
    }
}