﻿using Infrastructure.NHibernate.Attributes;

namespace Domain.Party.Quotes
{
    public class QuoteItemFees
    {
        public QuoteItemFees()
        {
        }

        public QuoteItemFees(decimal premium, decimal sasria)
        {
            Premium = premium;
            Sasria = sasria;
        }

        public QuoteItemFees(decimal originalPremium, decimal premium, decimal sasria, decimal calculatedSasria, decimal sasriaShortfall, decimal discountedPremium, decimal discountedPercentage,decimal discountedDifference)
        {
            OriginalPremium = originalPremium;
            Premium = premium;
            Sasria = sasria;
            CalculatedSasria = calculatedSasria;
            SasriaShortfall = sasriaShortfall;
            DiscountedPremium = discountedPremium;
            DiscountedPercentage = discountedPercentage;
            DiscountedDifference = discountedDifference;
        }

        public virtual decimal OriginalPremium { get; protected internal set; }
        public virtual decimal Premium { get; protected internal set; }
        public virtual decimal Sasria { get; protected internal set; }
        public virtual decimal DiscountedPremium { get; protected internal set; }
        public virtual decimal DiscountedPercentage { get; protected internal set; }
        public virtual decimal DiscountedDifference { get; protected internal set; }
        public virtual decimal CalculatedSasria { get; protected internal set; }
        public virtual decimal SasriaShortfall { get; protected internal set; }


        [DoNotMap]
        public virtual decimal TotalPremium
        {
            get { return Premium + Sasria; }
        }
    }
}