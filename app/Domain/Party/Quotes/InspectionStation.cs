﻿using Domain.Base;

namespace Domain.Party.Quotes
{
    public class InspectionStation : Entity
    {

        public virtual string Name { get; set; }

        public virtual string WorkingHours { get; set; }

        public virtual string PhoneWork { get; set; }

        public virtual string Address1 { get; set; }

        public virtual string Address2 { get; set; }

        public virtual string Address3 { get; set; }

        public virtual string Suburb { get; set; }

        public virtual string PostalCode { get; set; }

        public virtual string Email { get; set; }

        public virtual string Notes { get; set; }

        public virtual Quote Quote { get; set; }


    }
}
