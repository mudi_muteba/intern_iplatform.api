﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Validation;
using Domain.Party.Quotes.Queries;
using iPlatform.Api.DTOs.Party.Quotes;
using ValidationMessages.Ratings;

namespace Domain.Party.Quotes.Validators
{
    public class DistributeQuoteValidator : IValidateDto<DistributeQuoteDto>
    {
        private readonly GetQuoteByExternalReference query;

        public DistributeQuoteValidator(GetQuoteByExternalReference query)
        {
            this.query = query;
        }

        public void Validate(DistributeQuoteDto dto, ExecutionResult result)
        {
            var matchingQuotes = query
                .WithExternalReference(dto.ExternalReference)
                .ExecuteQuery()
                .ToList();

            if (!matchingQuotes.Any())
            {
                result.AddValidationMessage(QuoteDistributionValidationMessages.UnknownExternalReference);
            }

            var quote = matchingQuotes.FirstOrDefault();

            foreach (var productCode in dto.ProductCodes.Where(productCode => !quote.HasQuoteFor(productCode)))
            {
                result.AddValidationMessage(QuoteDistributionValidationMessages.ProductNotQuoted(productCode));
            }

            ValidateSMSDistribution(dto, quote, result);
        }

        private void ValidateSMSDistribution(DistributeQuoteDto dto, QuoteHeader quote, ExecutionResult result)
        {
            if (dto.Via != DistributeQuoteVia.SMS)
                return;

            if (!string.IsNullOrWhiteSpace(dto.Contact))
                return;

            var cellphoneNumber = quote.Proposal.Party.ContactDetail.Cell;

            if (string.IsNullOrWhiteSpace(cellphoneNumber))
            {
                result.AddValidationMessage(QuoteDistributionValidationMessages.NoCellPhoneNumberProvided);
            }
        }
    }
}