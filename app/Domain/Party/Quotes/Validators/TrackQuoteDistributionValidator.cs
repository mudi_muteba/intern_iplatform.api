﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Validation;
using Domain.Party.Quotes.Queries;
using iPlatform.Api.DTOs.Party.Quotes;
using ValidationMessages.Ratings;

namespace Domain.Party.Quotes.Validators
{
    public class TrackQuoteDistributionValidator : IValidateDto<TrackQuoteDistributionDto>
    {
        private readonly GetQuoteByIdQuery query;

        public TrackQuoteDistributionValidator(GetQuoteByIdQuery query)
        {
            this.query = query;
        }

        public void Validate(TrackQuoteDistributionDto dto, ExecutionResult result)
        {
            var quote = query.WithQuoteId(dto.QuoteId).ExecuteQuery().FirstOrDefault();

            if (quote == null)
            {
                result.AddValidationMessage(QuoteDistributionTrackingValidationMessages.NoQuoteFound(dto.QuoteId));
            }

            if(quote != null)
            {
                if (!quote.HasQuoteFor(dto.ProductCode))
                {
                    result.AddValidationMessage(QuoteDistributionTrackingValidationMessages.ProductNotQuoted(dto.ProductCode, dto.QuoteId));
                }
            }
        }
    }
}