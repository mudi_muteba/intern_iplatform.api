﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Activities;
using ValidationMessages.Ratings;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.Party.Quotes.Validators
{
    public class QuoteValidator : IValidateDto<CreateQuoteAcceptedLeadActivityDto>, IValidateDto<GetPolicyBindingByQuoteDto>, IValidateDto<SubmitPolicyBindingDto>
    {

        public QuoteValidator(IRepository repository)
        {
            this.m_Repository = repository;
        }

        private readonly IRepository m_Repository;
        public void Validate(CreateQuoteAcceptedLeadActivityDto dto, ExecutionResult result)
        {
            ValidateQuote(dto.QuoteId, result);
        }

        private void ValidateQuote(int QuoteId, ExecutionResult result)
        {
            var quote = m_Repository.GetById<Quote>(QuoteId);

            if (quote == null)
            {
                result.AddValidationMessage(QuoteAcceptanceValidationMessages.InvalidQuoteId.AddParameters(new []{QuoteId.ToString()}));
            }
        }

        public void Validate(GetPolicyBindingByQuoteDto dto, ExecutionResult result)
        {
            ValidateQuote(dto.QuoteId, result);
        }

        public void Validate(SubmitPolicyBindingDto dto, ExecutionResult result)
        {
            ValidateQuote(dto.QuoteId, result);
        }
    }
}