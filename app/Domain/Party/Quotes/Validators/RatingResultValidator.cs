﻿using System;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Validation;
using Domain.Party.ProposalHeaders.Queries;
using iPlatform.Api.DTOs.Ratings.Response;
using ValidationMessages.Ratings;

namespace Domain.Party.Quotes.Validators
{
    public class RatingResultValidator : IValidateDto<RatingResultDto>
    {
        private readonly GetProposalHeaderByExternalReferenceQuery query;

        public RatingResultValidator(GetProposalHeaderByExternalReferenceQuery query)
        {
            this.query = query;
        }

        public void Validate(RatingResultDto dto, ExecutionResult result)
        {
            if (dto.RatingRequestId == Guid.Empty)
            {
                return;
            }

            var matches = query
                .WithExternalReference(dto.RatingRequestId.ToString())
                .ExecuteQuery()
                .ToList();


            if (!matches.Any())
            {
                result.AddValidationMessage(QuoteCreationValidationMessages.NoProposalFoundForTheRatingResult);
            }
        }
    }
}