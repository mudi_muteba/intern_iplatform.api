﻿using System;
using Domain.Base;
using Shared;

namespace Domain.Party.Quotes
{
    public class QuoteDistribution : Entity
    {
        protected QuoteDistribution()
        {
        }

        public QuoteDistribution(Quote quote
            , string distributionMethod
            , string providerName
            , string providerReference)
        {
            Quote = quote;
            DistributionMethod = distributionMethod;
            ProviderName = providerName;
            ProviderReference = providerReference;
            DateTracked = SystemTime.Now();
        }

        public virtual Quote Quote { get; protected internal set; }
        public virtual string DistributionMethod { get; protected internal set; }
        public virtual string ProviderName { get; protected internal set; }
        public virtual string ProviderReference { get; protected internal set; }
        public virtual DateTime DateTracked { get; protected internal set; }
    }
}