using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Products;
using FluentNHibernate.Conventions;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Domain.Party.Quotes
{
    internal class RatingResultDtoQuoteReader
    {
        private static readonly ILog log = LogManager.GetLogger<RatingResultDtoQuoteReader>();
        private readonly RatingResultDto ratings;

        public RatingResultDtoQuoteReader(RatingResultDto ratings)
        {
            this.ratings = ratings;
        }

        public IEnumerable<RatingResultPolicyDto> GetValidPolicies(List<Product> workingProducts)
        {
            var list = new List<RatingResultPolicyDto>();
            foreach (var policy in ratings.Policies)
            {
                var currentProducts = from p in workingProducts where p.ProductCode == policy.ProductCode
                                      select p;

                if (!currentProducts.Any())
                {
                    log.WarnFormat("Could not find a product with product code '{0}'", policy.ProductCode);
                    continue;
                }
                list.Add(policy);
                //yield removed due to performance issues.
                //all inner foreach loop queries to db removed
            }
            return list;
        }

        public IEnumerable<Tuple<RatingResultItemDto, CoverDefinition>> GetValidItems(Product currentProduct,
            IEnumerable<RatingResultItemDto> items, List<CoverDefinition> coversDefinitions)
        {

            List<Tuple<RatingResultItemDto, CoverDefinition>> list = new  List<Tuple<RatingResultItemDto, CoverDefinition>>();

            foreach (var item in items)
            {
                var currentCovers = from cover in coversDefinitions
                                    where cover.Cover.Id == item.Cover.Id && cover.Product.Id == currentProduct.Id
                                    select cover;


                if (!currentCovers.Any())
                {
                    log.WarnFormat("Missing cover '{0}' from product code '{1}'", item.Cover.Name, currentProduct.ProductCode);
                    continue;
                }

                list.Add(new Tuple<RatingResultItemDto, CoverDefinition>(item, currentCovers.FirstOrDefault()));
                // yield removed due to performance issues.
                //all inner foreach loop queries to db removed
            }
            return list;
        }
    }
}