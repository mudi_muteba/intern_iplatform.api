﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using Domain.Admin;
using Domain.Base;
using Domain.Base.Events;
using Domain.Individuals.Events;
using Domain.Party.Quotes.Builders.QuoteHelpers;
using iPlatform.Api.DTOs.QuoteBreakdown;

namespace Domain.Party.Quotes
{
    public class QuoteItemBreakDown : Entity
    {
        public QuoteItemBreakDown()
        {
        }

        public QuoteItemBreakDown(QuoteItem quoteItem,
            string description,
            string reference,
            string risk,
            string markup,
            string questionAnswer,
            string total,
            string name,
            string relationship,
            int additionalMembersId,
            bool isChild,
            bool isMainMember,
            string externalId,
            string finalPremium = "0"
            )
        {
            QuoteItem = quoteItem;
            Description = description;
            Reference = reference;
            Risk = risk;
            Markup = markup;
            QuestionAnswer = questionAnswer;
            Total = total;
            IsChild = isChild;
            Relationship = relationship;
            Name = name;
            IsMainMember = isMainMember;
            AdditionalMembersId = additionalMembersId;
            ExternalId = externalId;
            AmountAdj = "0";
            PercentageAdj = "0";
            FinalPremium = finalPremium;
        }

        public virtual QuoteItem QuoteItem { get; protected internal set; }
        public virtual string Description { get; protected internal set; }
        public virtual string Name { get; protected internal set; }
        public virtual string Relationship { get; protected internal set; }
        public virtual string Reference { get; protected internal set; }
        public virtual string Risk { get; protected internal set; }
        public virtual string Markup { get; protected internal set; }
        public virtual string QuestionAnswer { get; protected internal set; }

        public virtual string Total { get; protected internal set; }
        public virtual bool IsChild { get; protected internal set; }
        public virtual bool IsMainMember { get; protected internal set; }
        public virtual int AdditionalMembersId { get; protected internal set; }
        public virtual string FinalPremium { get; protected internal set; }
        public virtual string PercentageAdj { get; protected internal set; }
        public virtual string AmountAdj { get; protected internal set; }
        public virtual string ExternalId { get; protected internal set; }


        public virtual QuoteItemBreakDown MapXmlMember(List<FuneralMemberBreakdownDto> xmlMembers)
        {
            var member = xmlMembers.FirstOrDefault(a => a.Reference.Equals(Reference));
            if (member == null) return this;
            Risk = member.Risk;
            Markup = member.Markup;

            decimal total = 0;
            decimal.TryParse(member.Total, out total);

            Total = total.ToString(CultureInfo.InvariantCulture);
            FinalPremium = total.ToString(CultureInfo.InvariantCulture);

            return this;
        }

        public virtual void Update(EditBreakDownItemDto quoteBreakdownDto)
        {
            Id = quoteBreakdownDto.Id;
            PercentageAdj = quoteBreakdownDto.PercentageAdj;
            FinalPremium = quoteBreakdownDto.FinalPremium;
            AmountAdj = quoteBreakdownDto.AmountAdj;
        }
    }
}
