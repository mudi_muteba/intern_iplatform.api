﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base;

namespace Domain.Party.Quotes
{
    public class QuoteItemState : Entity
    {
        protected QuoteItemState()
        {
            Entries = new List<QuoteItemStateEntry>();
        }

        public QuoteItemState(QuoteItem quoteItem) : this()
        {
            QuoteItem = quoteItem;
        }

        public virtual QuoteItem QuoteItem { get; protected internal set; }

        public virtual bool Warnings
        {
            get { return Entries.Any(e => !e.IsError); }
            protected internal set { }
        }

        public virtual bool Errors
        {
            get { return Entries.Any(e => e.IsError); }
            protected internal set { }
        }

        public virtual IList<QuoteItemStateEntry> Entries { get; protected internal set; }

        public virtual void AddError(string error)
        {
            Entries.Add(QuoteItemStateEntry.CreateError(this, error));
        }

        public virtual void AddErrors(IEnumerable<string> errors)
        {
            foreach (var error in errors)
            {
                AddError(error);
            }
        }

        public virtual void AddWarning(string warning)
        {
            Entries.Add(QuoteItemStateEntry.CreateWarning(this, warning));
        }

        public virtual void AddWarnings(IEnumerable<string> warnings)
        {
            foreach (var warning in warnings)
            {
                AddWarning(warning);
            }
        }
    }
}