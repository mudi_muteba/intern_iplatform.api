﻿using Domain.Base;

namespace Domain.Party.Quotes
{
    public class QuoteItemStateEntry : Entity
    {
        public QuoteItemStateEntry()
        {
        }

        private QuoteItemStateEntry(QuoteItemState quoteItemState, string message, bool isError)
        {
            QuoteItemState = quoteItemState;
            Message = message;
            IsError = isError;
        }

        public virtual QuoteItemState QuoteItemState { get; protected internal set; }
        public virtual string Message { get; protected internal set; }
        public virtual bool IsError { get; protected internal set; }

        public static QuoteItemStateEntry CreateError(QuoteItemState quoteItem, string errorMessage)
        {
            return new QuoteItemStateEntry(quoteItem, errorMessage, true);
        }

        public static QuoteItemStateEntry CreateWarning(QuoteItemState quoteItem, string warningMessage)
        {
            return new QuoteItemStateEntry(quoteItem, warningMessage, false);
        }
    }
}