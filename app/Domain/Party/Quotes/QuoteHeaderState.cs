using System.Collections.Generic;
using System.Linq;
using Domain.Base;

namespace Domain.Party.Quotes
{
    public class QuoteHeaderState : Entity
    {
        public QuoteHeaderState()
        {
            Entries = new List<QuoteHeaderStateEntry>();
        }

        public QuoteHeaderState(QuoteHeader quote) : this()
        {
            QuoteHeader = quote;
        }

        public virtual QuoteHeader QuoteHeader { get; protected internal set; }

        public virtual bool Warnings
        {
            get { return Entries.Any(e => !e.IsError); }
            protected internal set { }
        }

        public virtual bool Errors
        {
            get { return Entries.Any(e => e.IsError); }
            protected internal set { }
        }

        public virtual IList<QuoteHeaderStateEntry> Entries { get; protected internal set; }

        public virtual void AddError(string error)
        {
            Entries.Add(QuoteHeaderStateEntry.CreateError(this, error));
        }

        public virtual void AddErrors(IEnumerable<string> errors)
        {
            foreach (var error in errors)
            {
                AddError(error);
            }
        }

        public virtual void AddWarning(string warning)
        {
            Entries.Add(QuoteHeaderStateEntry.CreateWarning(this, warning));
        }

        public virtual void AddWarnings(IEnumerable<string> warnings)
        {
            foreach (var warning in warnings)
            {
                AddWarning(warning);
            }
        }
    }
}