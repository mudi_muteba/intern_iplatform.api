﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base;
using Domain.Party.ProposalHeaders;
using Domain.Products;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Response;
using Infrastructure.NHibernate.Attributes;
using iPlatform.Api.DTOs.PolicyBindings;
using Domain.PolicyBindings.Events;

namespace Domain.Party.Quotes
{
    public class Quote : EntityWithAudit
    {
        public Quote()
        {
            CreatedAt = DateTime.UtcNow;
            ModifiedAt = DateTime.UtcNow;
            Items = new List<QuoteItem>();
            QuoteDistribution = new List<QuoteDistribution>();
            QuoteUploadLogs = new List<QuoteUploadLog>();
        }

        public Quote(QuoteHeader header, Product product, decimal fees, string insurerReference, string itcScore,
            string extraItcScore, IEnumerable<string> errors, IEnumerable<string> warnings) : this()
        {
            QuoteDistribution = new List<QuoteDistribution>();
            Items = new List<QuoteItem>();
            QuoteHeader = header;
            Product = product;
            Fees = fees;
            InsurerReference = insurerReference;
            ITCScore = itcScore;
            ExtraITCScore = extraItcScore;
            State = new QuoteState(this);
            PlatformId = Guid.NewGuid();
            State.AddErrors(errors);
            State.AddWarnings(warnings);
        }

        public virtual DateTime CreatedAt { get; protected internal set; }
        public virtual DateTime ModifiedAt { get; protected internal set; }
        public virtual QuoteHeader QuoteHeader { get; protected internal set; }
        public virtual Product Product { get; protected internal set; }
        public virtual QuoteState State { get; protected internal set; }
        public virtual decimal Fees { get; protected internal set; }
        public virtual bool IsAccepted { get; protected internal set; }
        public virtual DateTime? AcceptedOn { get; protected internal set; }
        public virtual IList<QuoteItem> Items { get; protected internal set; }
        public virtual IList<QuoteDistribution> QuoteDistribution { get; protected internal set; }
        public virtual string InsurerReference { get; protected internal set; }
        public virtual string ITCScore { get; protected internal set; }
        public virtual string ExtraITCScore { get; protected internal set; }
        public virtual string ExternalRatingResult { get; set; }
        public virtual string ExternalRatingRequest { get; set; }
        public virtual Guid PlatformId { get; set; }
        public virtual IList<QuoteUploadLog> QuoteUploadLogs { get; set; }

        public virtual int BrokerUserId { get; set; }
        public virtual string BrokerUserExternalReference { get; set; }
        public virtual int AccountExecutiveUserId { get; set; }
        public virtual string AccountExecutiveExternalReference { get; set; }
        public virtual int ChannelId { get; set; }
        public virtual string ChannelExternalReference { get; set; }



        public virtual void Accept()
        {
            IsAccepted = true;
            AcceptedOn = DateTime.UtcNow;
            QuoteHeader.AcceptQuote();
        }

        public virtual void NotAccepted()
        {
            IsAccepted = false;
            AcceptedOn = null;
            QuoteHeader.NotAccepted();
        }

        [DoNotMap]
        public virtual decimal TotalPremium
        {
            get { return Items.Sum(i => i.Fees.TotalPremium); }
        }

        [DoNotMap]
        public virtual decimal OriginalTotalPremium
        {
            get { return Items.Sum(i => i.Fees.OriginalPremium); }
        }

        public override void Delete()
        {
            IsDeleted = true;
            ModifiedAt = DateTime.UtcNow;

            foreach (var item in Items)
            {
                item.Delete();
            }

            base.Delete();
        }

        public virtual void AddItem(QuoteItem item)
        {
            ModifiedAt = DateTime.UtcNow;
            Items.Add(item);
        }

        public virtual void BelongsTo(QuoteHeader header)
        {
            QuoteHeader = header;
        }

        public virtual QuoteItem CreateItem(ProposalHeader proposal, CoverDefinition cover, RatingResultItemDto item)
        {

            var proposalItem = proposal.ProposalDefinitions.FirstOrDefault(x => x.Asset.AssetNo == item.AssetNo);

            var quoteItem = new QuoteItem(this, item.SumInsuredDescription, proposalItem != null ? proposalItem.Description : string.Empty,
                cover,
                new QuoteItemAsset(item.AssetNo, item.SumInsured, proposalItem.Asset.Description),
                new QuoteItemFees(item.Premium, item.Sasria),
                new QuoteItemExcess(item.Excess.Calculated, item.Excess.Basic, item.Excess.Description, proposal.VoluntaryExcess(item.AssetNo)),
                item.State.Errors,
                item.State.Warnings, 
                clauses: item.Clauses);

            Items.Add(quoteItem);

            return quoteItem;
        }

        public virtual void TrackDistribution(TrackQuoteDistributionDto trackQuoteDistributionDto)
        {
            QuoteDistribution.Add(new QuoteDistribution(this
                , trackQuoteDistributionDto.Via.ToString()
                , trackQuoteDistributionDto.Provider
                , trackQuoteDistributionDto.ProviderReference));
        }


        public virtual void PolicyBind(PolicyBindingDetailsDto dto, int campaignId, int userId, int channelId)
        {
            RaiseEvent(new PolicyBindingCreatedEvent
            {
                LeadId = dto.LeadId,
                PartyId = dto.PartyId,
                QuoteId = dto.QuoteId,
                UserId = userId,
                ChannelId = channelId,
                CampaignId = campaignId
            });
        }
    }
}