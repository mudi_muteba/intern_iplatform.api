﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;

namespace Domain.Party.Quotes
{
    public class ItemExcess : Entity
    {
        public virtual bool Calculated { get; set; }

        public virtual decimal Basic { get; set; }

        public virtual string Description { get; set; }

        public virtual decimal PremiumDifference { get; set; }

        public virtual decimal Premium { get; set; }

        public virtual string ExcessCode { get; set; }

        public virtual QuoteItem QuoteItem { get; set; }
        
    }
}
