﻿using Domain.Base.Workflow;
using iPlatform.Enums.Workflows;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Party.Quotes.Workflow.Messages
{
    [WorkflowExecutionMessageType(WorkflowMessageType.Sms)]
    public class SMSQuoteMessage : WorkflowExecutionMessage
    {
        public SMSQuoteMessage() { } //Required by AutoMapper
        public SMSQuoteMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }
        public SMSQuoteMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}