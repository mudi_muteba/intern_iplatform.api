﻿using System.Collections.Generic;
using Domain.Party.Quotes.Workflow.Messages;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Party.Quotes.Workflow.Tasks
{
    /// <summary>
    /// No longer needed because of changes made to workflow tasks
    /// </summary>
    public class DistributeQuoteViaSMSTask
    {
        
    }
}