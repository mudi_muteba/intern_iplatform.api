using System.Collections.Generic;
using Domain.Party.Quotes.Events;
using iPlatform.OutboundCommunication.SMS;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Party.Quotes.Workflow.Tasks
{
    public class DistributeQuoteViaSMSTaskMetadata : ITaskMetadata
    {
        public DistributeQuoteViaSMSTaskMetadata()
        {
            Providers = new List<SMSProviderConfiguration>();
        }

        public DistributeQuoteViaSMSTaskMetadata(List<SMSProviderConfiguration> providers
            , int quoteId
            , string externalReference
            , string cellPhoneNumber
            , List<DistributedQuoteEvent> quotes)
        {
            Providers = providers ?? new List<SMSProviderConfiguration>();
            QuoteId = quoteId;
            ExternalReference = externalReference;
            CellPhoneNumber = cellPhoneNumber;
            Quotes = quotes;
        }

        public List<SMSProviderConfiguration> Providers { get; set; }
        public int QuoteId { get; set; }
        public string ExternalReference { get; set; }
        public string CellPhoneNumber { get; set; }
        public List<DistributedQuoteEvent> Quotes { get; set; }
    }
}