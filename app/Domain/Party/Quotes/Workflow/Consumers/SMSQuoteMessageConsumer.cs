﻿using Common.Logging;
using Domain.Party.Quotes.Workflow.Messages;
using Domain.Party.Quotes.Workflow.Tasks;
using iPlatform.OutboundCommunication.SMS;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Party.Quotes.Workflow.Consumers
{
    public class SMSQuoteMessageConsumer : AbstractMessageConsumer<SMSQuoteMessage>
    {
        private readonly ICreateSMSSenders factory;
        private static readonly ILog log = LogManager.GetLogger<SMSQuoteMessageConsumer>();

        public SMSQuoteMessageConsumer(IWorkflowRouter router
            , IWorkflowExecutor executor
            , ICreateSMSSenders factory) : base(router, executor)
        {
            this.factory = factory;
        }

        public override void Consume(SMSQuoteMessage message)
        {
            var metadata = message.Metadata as DistributeQuoteViaSMSTaskMetadata;

            foreach (var provider in metadata.Providers)
            {
                var sender = factory.Create(provider);

                log.InfoFormat("Sending an SMS for quote {0} ({1}) using {2}. Configuration is username: {3} pointing at {4}"
                    , metadata.QuoteId
                    , metadata.ExternalReference
                    , provider.ProviderName
                    , provider.UserName
                    , provider.Url);

                SendSMS(sender, metadata, provider);
            }
        }

        private void SendSMS(ISendSMS sender, DistributeQuoteViaSMSTaskMetadata metadata, SMSProviderConfiguration provider)
        {
            foreach (var quote in metadata.Quotes)
            {
                var reference = string.Format("QUOTE_{0}_{1}", metadata.QuoteId, quote.ProductCode);
                var message = new SMSMessage(metadata.CellPhoneNumber, "Some random text", reference);

                var result = sender.Send(provider, message);

                log.InfoFormat("{0} sending result was {1} for reference {2}"
                    , provider
                    , result
                    , reference);
            }
        }
    }
}