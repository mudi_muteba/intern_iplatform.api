﻿using System.Collections.Generic;
using Domain.Base.Workflow;
using Domain.Party.Quotes.Events;

namespace Domain.Party.Quotes.Workflow.Factories
{
    public class CreateDistributionViaSMSTaskKey : IWorkflowFactoryKey
    {
        public CreateDistributionViaSMSTaskKey()
        {
            Quotes = new List<DistributedQuoteEvent>();
        }

        public int ChannelId { get; set; }
        public string CellPhoneNumber { get; set; }
        public int QuoteId { get; set; }
        public string ExternalReference { get; set; }
        public List<DistributedQuoteEvent> Quotes { get; set; }
    }
}