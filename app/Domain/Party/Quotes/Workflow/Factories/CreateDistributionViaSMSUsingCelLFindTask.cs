﻿using System.Collections.Generic;
using System.Linq;

using Common.Logging;

using Domain.Admin.Channels.SMSCommunicationSettings.Queries;

using Domain.Base.Workflow;

using Domain.Party.Quotes.Workflow.Messages;
using Domain.Party.Quotes.Workflow.Tasks;

using iPlatform.OutboundCommunication.SMS;

using Workflow.Messages;


namespace Domain.Party.Quotes.Workflow.Factories
{
    public class CreateDistributionViaSMSUsingCellFindTask : AbstractWorkflowTaskFactory<CreateDistributionViaSMSTaskKey>
    {
        private readonly GetSMSCommunicationSettingsQuery query;
        private static readonly ILog log = LogManager.GetLogger<CreateDistributionViaSMSUsingCellFindTask>();

        public CreateDistributionViaSMSUsingCellFindTask(GetSMSCommunicationSettingsQuery query)
        {
            this.query = query;
        }

        public override IWorkflowRoutingMessage Create(CreateDistributionViaSMSTaskKey command)
        {
            var settings = query.WithChannelId(command.ChannelId).ExecuteQuery().ToList();

            if (!settings.Any())
            {
                log.WarnFormat("Not communication settings found for channel {0}", command.ChannelId);
            }

            var providers = new List<SMSProviderConfiguration>(settings.Select(s => new SMSProviderConfiguration(s.SMSProviderName, s.SMSUserName, s.SMSPassword, s.SMSUrl)));

            var metadata = new DistributeQuoteViaSMSTaskMetadata(providers
                , command.QuoteId
                , command.ExternalReference
                , command.CellPhoneNumber
                , command.Quotes);
            return new WorkflowRoutingMessage().AddMessage(new SMSQuoteMessage(metadata));
        }
    }
}