using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteItemStateOverride : IAutoMappingOverride<QuoteItemState>
    {
        public void Override(AutoMapping<QuoteItemState> mapping)
        {
            mapping.Table(mapping.GetType().GetGenericArguments()[0].Name);
            mapping.HasOne<QuoteItem>(x => x.QuoteItem).Cascade.All();
            mapping.HasMany<QuoteItemStateEntry>(x => x.Entries)
                .Cascade.SaveUpdate()
                .KeyColumn("QuoteItemStateId")
                .AsBag();  
        }
    }
}