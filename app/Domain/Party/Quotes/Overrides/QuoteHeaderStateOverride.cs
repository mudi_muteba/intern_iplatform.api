﻿ using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteHeaderStateOverride : IAutoMappingOverride<QuoteHeaderState>
    {
        public void Override(AutoMapping<QuoteHeaderState> mapping)
        {
            mapping.Table(mapping.GetType().GetGenericArguments()[0].Name);
            mapping.References<QuoteHeader>(x => x.QuoteHeader).Cascade.SaveUpdate();
            mapping.HasMany<QuoteHeaderStateEntry>(x => x.Entries)
                .Cascade.SaveUpdate()
                .KeyColumn("QuoteHeaderStateId")
                .AsBag();
        }
    }
}