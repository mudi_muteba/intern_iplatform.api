﻿using Domain.Party.ProposalHeaders;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.OverridesOrganType
{
    public class QuoteHeaderOverride : IAutoMappingOverride<QuoteHeader>
    {
        public void Override(AutoMapping<QuoteHeader> mapping)
        {
            mapping.Component(x => x.Acceptance, part =>
            {
                part.Map(c => c.AcceptedBy);
                part.Map(c => c.AcceptedOn);
                part.Map(c => c.IsAccepted);
            });

            mapping.References<ProposalHeader>(c => c.Proposal, "ProposalHeaderId");

            mapping.HasOne(x => x.State).PropertyRef(a => a.QuoteHeader).Cascade.All();
            mapping.HasMany<Quote>(x => x.Quotes)
                .Cascade.SaveUpdate()
                .KeyColumn("QuoteHeaderId")
                .AsBag();
        }
    }
}