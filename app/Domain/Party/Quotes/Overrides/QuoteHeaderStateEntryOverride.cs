﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteHeaderStateEntryOverride : IAutoMappingOverride<QuoteHeaderStateEntry>
    {
        public void Override(AutoMapping<QuoteHeaderStateEntry> mapping)
        {
            mapping.Table(mapping.GetType().GetGenericArguments()[0].Name);
            mapping.References<QuoteHeaderState>(x => x.QuoteHeaderState);
        }
    }
}