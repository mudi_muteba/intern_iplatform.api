using System.Collections.Generic;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteItemOverride : IAutoMappingOverride<QuoteItem>
    {
        public void Override(AutoMapping<QuoteItem> mapping)
        {
            mapping.Component(x => x.Fees);

            mapping.Component(x => x.Excess, part =>
            {
                part.Map(c => c.Basic, "ExcessBasic");
                part.Map(c => c.IsCalculated, "ExcessCalculated");
                part.Map(c => c.Description, "ExcessDescription").Length(10000);
                part.Map(c => c.VoluntaryExcess, "VoluntaryExcess");

            });

            mapping.Component(x => x.Asset, part =>
            {
                part.Map(c => c.AssetNumber);
                part.Map(c => c.SumInsured);
            });

            mapping.Map(x => x.Clauses).Length(10000);
            mapping.References<Quote>(x => x.Quote).Cascade.All();
            mapping.References<QuoteItemState>(x => x.QuoteItemState).Cascade.All();
            mapping.HasMany<QuoteItemBreakDown>(x => x.QuoteItemBreakDowns).Cascade.All();
        }
    }
}