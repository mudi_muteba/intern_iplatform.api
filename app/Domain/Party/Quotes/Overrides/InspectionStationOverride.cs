using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class InspectionStationOverride : IAutoMappingOverride<InspectionStation>
    {
        public void Override(AutoMapping<InspectionStation> mapping)
        {

            mapping.Map(a => a.Email).Length(100);
            mapping.Map(a => a.WorkingHours).Length(50);
            mapping.Map(a => a.PhoneWork).Length(50);
            mapping.Map(a => a.Address1).Length(100);
            mapping.Map(a => a.Address2).Length(100);
            mapping.Map(a => a.Address3).Length(100);
            mapping.Map(a => a.Suburb).Length(100);
            mapping.Map(a => a.PostalCode).Length(100);
            mapping.Map(a => a.Name).Length(100);
            mapping.Map(a => a.Notes).Length(100);

        }
    }
}