﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteItemStateEntryOverride : IAutoMappingOverride<QuoteItemStateEntry>
    {
        public void Override(AutoMapping<QuoteItemStateEntry> mapping)
        {
            mapping.Table(mapping.GetType().GetGenericArguments()[0].Name);
			mapping.References<QuoteItemState>(x => x.QuoteItemState);
        }
    }
}
