using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class ItemExcessOverride : IAutoMappingOverride<ItemExcess>
    {
        public void Override(AutoMapping<ItemExcess> mapping)
        {

            mapping.Map(a => a.Description).Length(100);
            mapping.Map(a => a.ExcessCode).Length(100);

        }
    }
}