using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteDistributionOverride : IAutoMappingOverride<QuoteDistribution>
    {
        public void Override(AutoMapping<QuoteDistribution> mapping)
        {
            mapping.HasOne<Quote>(quote => quote.Quote).Cascade.All();
        }
    }
}