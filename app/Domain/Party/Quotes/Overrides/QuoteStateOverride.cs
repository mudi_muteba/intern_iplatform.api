﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteStateOverride : IAutoMappingOverride<QuoteState>
    {
        public void Override(AutoMapping<QuoteState> mapping)
        {
            mapping.Table(mapping.GetType().GetGenericArguments()[0].Name);
            mapping.References<Quote>(x => x.Quote).Cascade.All();
            mapping.HasMany<QuoteStateEntry>(x => x.Entries)
                .Cascade.SaveUpdate()
                .KeyColumn("QuoteStateId")
                .AsBag();
        }
    }
}