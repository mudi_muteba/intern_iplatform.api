using System;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteUploadLogOverride : IAutoMappingOverride<QuoteUploadLog>
    {
        public void Override(AutoMapping<QuoteUploadLog> mapping)
        {
            mapping.References<Quote>(x => x.Quote);
            mapping.Map(x => x.QuoteUploadObject).Length(int.MaxValue);
        }
    }
}