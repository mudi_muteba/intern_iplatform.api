using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteStateEntryOverride : IAutoMappingOverride<QuoteStateEntry>
    {
        public void Override(AutoMapping<QuoteStateEntry> mapping)
        {
            mapping.Table(mapping.GetType().GetGenericArguments()[0].Name);
        }
    }
}