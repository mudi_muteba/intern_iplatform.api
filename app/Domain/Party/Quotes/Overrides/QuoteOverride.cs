using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Quotes.Overrides
{
    public class QuoteOverride : IAutoMappingOverride<Quote>
    {
        public void Override(AutoMapping<Quote> mapping)
        {
            mapping.References(quote => quote.QuoteHeader).Cascade.SaveUpdate();
            mapping.HasMany<QuoteItem>(x => x.Items).Cascade.All();
            mapping.HasMany<QuoteDistribution>(x => x.QuoteDistribution).Cascade.All();
            mapping.HasOne(x => x.State).PropertyRef(a => a.Quote).Cascade.All();
            mapping.HasMany<QuoteUploadLog>(x => x.QuoteUploadLogs);
            mapping.Map(x => x.ExternalRatingResult).CustomType("StringClob").CustomSqlType("ntext");
            mapping.Map(x => x.ExternalRatingRequest).CustomType("StringClob").CustomSqlType("ntext");
        }
    }
}