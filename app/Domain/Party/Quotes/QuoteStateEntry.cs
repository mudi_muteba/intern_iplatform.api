﻿using Domain.Base;

namespace Domain.Party.Quotes
{
    public class QuoteStateEntry : Entity
    {
        public QuoteStateEntry()
        {
        }

        private QuoteStateEntry(QuoteState quoteState, string message, bool isError)
        {
            QuoteState = quoteState;
            Message = message;
            IsError = isError;
        }

        public virtual QuoteState QuoteState { get; protected internal set; }
        public virtual string Message { get; protected internal set; }
        public virtual bool IsError { get; protected internal set; }

        public static QuoteStateEntry CreateError(QuoteState quote, string errorMessage)
        {
            return new QuoteStateEntry(quote, errorMessage, true);
        }

        public static QuoteStateEntry CreateWarning(QuoteState quote, string warningMessage)
        {
            return new QuoteStateEntry(quote, warningMessage, false);
        }
    }
}