﻿using System;
using AutoMapper;
using Domain.Base;
using Domain.Products;
using iPlatform.Api.DTOs.Ratings.Quoting;

namespace Domain.Party.Quotes
{
    public class QuoteUploadLog : Entity
    {
        public virtual Guid RequestId { get; set; }
        public virtual Product Product { get; set; }
        public virtual string InsurerReference { get; set; }
        public virtual decimal Premium { get; set; }
        public virtual decimal Fees { get; set; }
        public virtual decimal Sasria { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual string Message { get; set; }
        public virtual Quote Quote { get; set; }
        public virtual byte[] QuoteUploadObject { get; set; }

        public static QuoteUploadLog Create(CreateQuoteUploadLogDto dto)
        {
            return Mapper.Map<QuoteUploadLog>(dto);
        }
    }
}
