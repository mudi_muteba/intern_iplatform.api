﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Activities;
using Domain.Base;
using Domain.Base.Events;
using Domain.Base.Execution.Exceptions;
using Domain.Campaigns;
using Domain.Campaigns.Queries;
using Domain.Party.Addresses;
using Domain.Party.Assets;
using Domain.Party.Assets.AssetAddresses;
using Domain.Party.Assets.AssetAddresses.Events;
using Domain.Party.Assets.AssetRiskItems;
using Domain.Party.Assets.AssetRiskItems.Events;
using Domain.Party.Assets.AssetVehicles;
using Domain.Party.Assets.AssetVehicles.Events;
using Domain.Party.Bank;
using Domain.Party.Bank.Events;
using Domain.Party.ContactDetails;
using Domain.Party.Contacts;
using Domain.Party.Note.Events;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalHeaders;
using Domain.Party.ProposalHeaders.Events;
using Domain.Party.Relationships;
using Domain.Users;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.Assets.AssetAddresses;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using iPlatform.Api.DTOs.Party.Bank;
using iPlatform.Api.DTOs.Party.Note;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using MasterData;
using Domain.Leads;
using Domain.Party.Correspondence;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using Domain.Party.Payments;
using iPlatform.Api.DTOs.Party.Payments;
using iPlatform.Api.DTOs.Party.Relationships;
using iPlatform.Api.DTOs.Party.Correspondence;

namespace Domain.Party
{
    public partial class Party : ChannelEntity
    {
        public Party()
        {
            Addresses = new List<Address>();
            BankDetails = new List<BankDetails>();
            Relationships = new List<Relationship>();
            Notes = new List<Note.Note>();
            Assets = new List<Asset>();
            Activities = new List<LeadActivity>();
            ProposalHeaders = new List<ProposalHeader>();
            Campaigns = new List<PartyCampaign>();
            ContactDetail = new ContactDetail();
            Correspondence = new List<PartyCorrespondence>();
        }

        public virtual string ExternalReference { get; set; }
        public virtual PartyType PartyType { get; protected set; }
        public virtual IList<BankDetails> BankDetails { get; protected set; }
        public virtual IList<Relationship> Relationships { get; protected set; }
        public virtual IList<Note.Note> Notes { get; set; }
        public virtual IList<LeadActivity> Activities { get; set; }
        public virtual IList<ProposalHeader> ProposalHeaders { get; set; }
        public virtual IList<PartyCorrespondencePreference> CorrespondencePreferences { get; set; }
        public virtual IList<PaymentDetails> PaymentDetails { get; set; }
        public virtual IList<PartyCampaign> Campaigns { get; set; }
        public virtual IList<Asset> Assets { get; set; }
        public virtual IList<PartyCorrespondence> Correspondence { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual DateTime? DateCreated { get; set; }
        public virtual DateTime? DateUpdated { get; set; }
        public virtual int MasterId { get; set; }
        public virtual ContactDetail ContactDetail { get; set; }
        public virtual Lead Lead { get; set; }


        public virtual string GetPrimaryContactNumber()
        {
            if (ContactDetail == null)
                return string.Empty;

            if (!string.IsNullOrEmpty(ContactDetail.Cell))
                return ContactDetail.Cell;
            if (!string.IsNullOrEmpty(ContactDetail.Home))
                return ContactDetail.Home;
            if (!string.IsNullOrEmpty(ContactDetail.Work))
                return ContactDetail.Work;
            if (!string.IsNullOrEmpty(ContactDetail.Direct))
                return ContactDetail.Direct;
            return string.Empty;
        }

        #region Relationship
        public virtual void AddRelationship(Contact contact, RelationshipType type)
        {
            contact.SetDisplayName();
            var relationship = new Relationship
            {
                Party = this,
                ChildParty = contact,
                RelationshipType = type
            };

            Relationships.Add(relationship);
        }

        public virtual void UpdateRelationship(Contact contact, RelationshipType type)
        {
            Relationship relationship = Relationships.FirstOrDefault(a => a.ChildParty.Id == contact.Id);

            if (relationship == null)
                throw new MissingEntityException(typeof(Relationship), contact.Id);

            var dto = Mapper.Map<EditRelationshipDto>(relationship);
            dto.RelationshipTypeId = type.Id;
            dto.ChannelId = Channel.Id;
            relationship.Update(dto);
        }
        #endregion

        #region Note

        public virtual Note.Note AddNote(CreateNoteDto dto)
        {
            var note = Mapper.Map<Note.Note>(dto);
            note.DateCreated = DateTime.UtcNow;
            note.RaiseEvent(new NoteCreatedEvent(note, dto.CreateAudit(), Channel));

            if (note.Party == null)
                note.Party = this;
            Notes.Add(note);
            return note;
        }

        public virtual Note.Note DeleteNote(DeleteNoteDto dto)
        {
            Note.Note note = Notes.FirstOrDefault(a => a.Id == dto.Id);

            if (note == null)
                throw new MissingEntityException(typeof(Note.Note), dto.Id);

            note.IsDeleted = true;
            return note;
        }

        #endregion

        #region Bank Details

        public virtual void UpdateBankDetails(EditBankDetailsDto dto)
        {
            BankDetails bankDetails = BankDetails.FirstOrDefault(a => a.Id == dto.Id);

            if (bankDetails == null)
            {
                throw new MissingEntityException(typeof(BankDetails), dto.Id);
            }

            if (bankDetails.Bank.Equals("No UK Bank"))
            {
                bankDetails.TypeAccount = "";
                bankDetails.Bank = "";
                bankDetails.BankBranch = "";
            }

            bankDetails.Update(dto);
        }

        public virtual BankDetails AddBankDetails(CreateBankDetailsDto dto)
        {
            var bankDetails = Mapper.Map<BankDetails>(dto);
            if (bankDetails.Bank.Equals("No UK Bank"))
            {
                bankDetails.TypeAccount = "";
                bankDetails.Bank = "";
                bankDetails.BankBranch = "";
            }
            bankDetails.RaiseEvent(new BankDetailsCreatedEvent(bankDetails, dto.CreateAudit(), Channel));

            if (bankDetails.Party == null)
                bankDetails.Party = this;
            BankDetails.Add(bankDetails);
            return bankDetails;
        }

        public virtual BankDetails DeleteBankDetails(DeleteBankDetailsDto dto)
        {
            BankDetails bankDetails = BankDetails.FirstOrDefault(a => a.Id == dto.Id || a.AccountNo == dto.AccountNo);

            if (bankDetails == null)
                throw new MissingEntityException(typeof(BankDetails), dto.Id);

            bankDetails.Delete();
            return bankDetails;
        }

        #endregion

        #region Activities

        public virtual void Created(string Source, User user, Campaign campaign = null)
        {
            if (Source == IndividualSourceEnum.API.ToString())
                HandleApiLeadActivity(user, campaign);
            else
                HandleWebLeadActivity(user, campaign);
        }



        private CreateLeadActivity HandleWebLeadActivity(User user, Campaign campaign = null)
        {
            LeadActivity createLeadActivity = Activities.FirstOrDefault(p => p.GetType() == typeof(CreateLeadActivity));

            if (createLeadActivity != null)
                return createLeadActivity as CreateLeadActivity;

            var activity = new CreateLeadActivity(user, this, campaign);

            activity.AllocateToCampaign(campaign);
            AddActivity(activity);
            return activity;
        }

        private ImportedLeadActivity HandleApiLeadActivity(User user, Campaign campaign = null)
        {
            var activity = new ImportedLeadActivity(user, this, null);
            activity.AllocateToCampaign(campaign);
            AddActivity(activity);
            return activity;
        }

        public virtual void Updated(string Source, User user, Campaign campaign = null)
        {
            if (Source == IndividualSourceEnum.API.ToString())
                HandleApiLeadActivity(user, campaign);
        }

        protected virtual void AddActivity(LeadActivity leadActivity)
        {
            Activities.Add(leadActivity);
        }

        #endregion

        #region Proposal

        public virtual ProposalHeader AddProposalHeader(CreateProposalHeaderDto dto, User user, CampaignReference campaign = null)
        {
            var proposalHeader = Mapper.Map<ProposalHeader>(dto);

            if (proposalHeader.Party == null)
                proposalHeader.Party = this;

            ProposalHeaders.Add(proposalHeader);


            proposalHeader.Created(user, this, campaign);
            proposalHeader.RaiseEvent(new ProposalHeaderCreatedEvent(proposalHeader, dto.CreateAudit(), Channel));

            return proposalHeader;
        }

        public virtual void DeleteProposalHeader(DeleteProposalHeaderDto dto)
        {
            ProposalHeader proposalHeader = ProposalHeaders.FirstOrDefault(a => a.Id == dto.Id);
            int count = ProposalHeaders.Count(a => a.Id != dto.Id);
            if (proposalHeader == null)
                throw new MissingEntityException(typeof(ProposalHeader), dto.Id);

            proposalHeader.Delete();
            proposalHeader.LeadActivity.Delete();
            foreach (ProposalDefinition proposalDefinition in proposalHeader.ProposalDefinitions)
            {
                proposalDefinition.Delete();
            }
        }



        #endregion

        #region Asset Vehicle

        public virtual void UpdateAssetVehicle(EditAssetVehicleDto dto)
        {
            var asset = Assets.FirstOrDefault(a => a.Id == dto.Id) as AssetVehicle;

            if (asset == null)
            {
                throw new MissingEntityException(typeof(AssetVehicle), dto.Id);
            }

            asset.Update(dto);
        }

        public virtual AssetVehicle DeleteAssetVehicle(DeleteAssetVehicleDto dto)
        {
            var asset = Assets.FirstOrDefault(a => a.Id == dto.Id) as AssetVehicle;


            if (asset == null)
                throw new MissingEntityException(typeof(AssetVehicle), dto.Id);

            asset.IsDeleted = true;
            return asset;
        }


        public virtual AssetVehicle AddAssetVehicle(CreateAssetVehicleDto dto)
        {
            var asset = Mapper.Map<AssetVehicle>(dto);
            asset.SetDescription();
            asset.RaiseEvent(new AssetVehicleCreatedEvent(asset, dto.CreateAudit(), Channel));

            if (asset.Party == null)
                asset.Party = this;
            Assets.Add(asset);
            return asset;
        }

        #endregion

        #region Asset RiskItem

        public virtual void UpdateAssetRiskItem(EditAssetRiskItemDto dto)
        {
            var asset = Assets.FirstOrDefault(a => a.Id == dto.Id) as AssetRiskItem;

            if (asset == null)
            {
                throw new MissingEntityException(typeof(AssetRiskItem), dto.Id);
            }

            asset.Update(dto);
        }

        public virtual AssetRiskItem DeleteAssetRiskItem(DeleteAssetRiskItemDto dto)
        {
            var asset = Assets.FirstOrDefault(a => a.Id == dto.Id) as AssetRiskItem;


            if (asset == null)
                throw new MissingEntityException(typeof(AssetRiskItem), dto.Id);

            asset.IsDeleted = true;
            return asset;
        }


        public virtual AssetRiskItem AddAssetRiskItem(CreateAssetRiskItemDto dto)
        {
            var asset = Mapper.Map<AssetRiskItem>(dto);
            asset.RaiseEvent(new AssetRiskItemCreatedEvent(asset, dto.CreateAudit(), Channel));

            if (asset.Party == null)
                asset.Party = this;
            Assets.Add(asset);
            return asset;
        }

        public virtual AssetAddress AddAssetAddress(CreateAssetAddressDto dto)
        {
            var asset = Mapper.Map<AssetAddress>(dto);
            var address = this.Addresses.First(a => a.Id == dto.AddressId);
            asset.Description = address.Description;
            asset.Address = address;

            asset.RaiseEvent(new AssetAddressCreatedEvent(asset, dto.CreateAudit(), Channel));

            if (asset.Party == null)
                asset.Party = this;
            Assets.Add(asset);
            return asset;
        }

        #endregion

        #region Correspondence
        public virtual void AddUpdateCorrespondence(CreatePartyCorrespondenceDto dto)
        {
            var entity = this.Correspondence.FirstOrDefault(x => x.Id == dto.PartyCorrespondenceId);

            if (entity == null)
                CreateCorrespondence(dto);
            else
                entity.Update(dto);

        }

        public virtual void CreateCorrespondence(CreatePartyCorrespondenceDto dto)
        {
            var entity = Mapper.Map<PartyCorrespondence>(dto);

            if (entity.Party == null)
                entity.Party = this;

            foreach (var attachment in dto.Attachments)
            {
                entity.AddAttachment(attachment);
            }

            Correspondence.Add(entity);
        }
        #endregion

        #region Correspondence Preferences
        public virtual void AddCorrespondencePref(PartyCorrespondencePreferenceDto dto)
        {
            var correspondenceUpdate = CorrespondencePreferences.FirstOrDefault(a => a.CorrespondenceType.Id == dto.CorrespondenceType.Id
                && a.Party.Id == dto.PartyId);

            if (correspondenceUpdate != null)
            {
                correspondenceUpdate.Update(dto);
            }
            else
            {
                var correspondencePref = Mapper.Map<PartyCorrespondencePreference>(dto);

                if (correspondencePref.Party == null)
                    correspondencePref.Party = this;
                CorrespondencePreferences.Add(correspondencePref);
            }
        }

        public virtual void RemoveCorrespondencePref(EditCorrespondencePreferenceDto dto)
        {
            var CPtoDelete = CorrespondencePreferences.Where(x => dto.CorrespondencePreferences.All(c => c.CorrespondenceType.Id != x.CorrespondenceType.Id));

            foreach (var CP in CPtoDelete)
            {
                CP.Delete();
            }
        }

        #endregion

        #region Payment Details
        public virtual PaymentDetails AddPaymentDetail(CreatePaymentDetailsDto dto)
        {
            var paymentDetail = Mapper.Map<PaymentDetails>(dto);

            if (paymentDetail.Party == null)
                paymentDetail.Party = this;
            PaymentDetails.Add(paymentDetail);

            return paymentDetail;
        }

        public virtual void UpdatePaymentDetail(EditPaymentDetailsDto dto)
        {
            var paymentDetail = PaymentDetails.FirstOrDefault(a => a.Id == dto.Id);

            if (paymentDetail == null)
            {
                throw new MissingEntityException(typeof(PaymentDetails), dto.Id);
            }

            paymentDetail.Update(dto);
        }
        #endregion
    }
}