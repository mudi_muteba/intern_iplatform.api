﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Correspondence.Overrides
{
    public class PartyCorrespondenceOverride : IAutoMappingOverride<PartyCorrespondence>
    {
        public void Override(AutoMapping<PartyCorrespondence> mapping)
        {
            mapping.References<Party>(x => x.To).Column("To");
            mapping.References<Party>(x => x.From).Column("From");
            mapping.HasMany<PartyCorrespondenceAttachment>(x => x.Attachments).Cascade.All();
            mapping.Map(x => x.Body).Length(10000);
        }
    }
}