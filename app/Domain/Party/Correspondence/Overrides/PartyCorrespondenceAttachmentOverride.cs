﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Party.Correspondence.Overrides
{
    public class PartyCorrespondenceAttachmentOverride : IAutoMappingOverride<PartyCorrespondenceAttachment>
    {
        public void Override(AutoMapping<PartyCorrespondenceAttachment> mapping)
        {
            
        }
    }
}