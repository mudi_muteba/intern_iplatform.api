﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Correspondence.Queries
{
    public class GetCorrespondenceByPartyIdQuery : BaseQuery<PartyCorrespondence>
    {
        private int _partyId;

        public GetCorrespondenceByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PartyCorrespondence>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    CorrespondenceReferenceAuthorisation.List,
                };
            }
        }

        public GetCorrespondenceByPartyIdQuery WithPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<PartyCorrespondence> Execute(IQueryable<PartyCorrespondence> query)
        {
            return Repository.GetAll<PartyCorrespondence>().Where(a => a.Party.Id == _partyId);
        }
    }
}