﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;

using MasterData.Authorisation;

using iPlatform.Api.DTOs.Party.Correspondence;
using AutoMapper;

namespace Domain.Party.Correspondence.Handlers
{
    public class CreateCorrespondenceHandler : CreationDtoHandler<Party, CreatePartyCorrespondenceDto, int>
    {
        public CreateCorrespondenceHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override Party HandleCreation(CreatePartyCorrespondenceDto dto, HandlerResult<int> result)
        {
            var entity = _repository.GetById<Party>(dto.PartyId);
            entity.AddUpdateCorrespondence(dto);
            return entity;
        }

        protected override void EntitySaved(Party entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }
    }
}