﻿using System;
using System.Collections.Generic;

using AutoMapper;

using Domain.Party.Correspondence;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Correspondence;

namespace Domain.Party.PartyCorrespondencePreferences.Mapping
{
    public class PartyCorrespondenceAttachmentMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreatePartyCorrespondenceAttachmentDto, PartyCorrespondenceAttachment>()
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.CreatedOn, o => o.MapFrom(s => DateTime.UtcNow))
                ;

            Mapper.CreateMap<PartyCorrespondenceAttachmentDto, PartyCorrespondenceAttachment>()
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.PartyCorrespondence, o => o.Ignore())
                .ForMember(t => t.CreatedOn, o => o.Ignore())
                ;

            Mapper.CreateMap<CreatePartyCorrespondenceAttachmentDto, PartyCorrespondenceAttachmentDto>()
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.PartyCorrespondenceId, o => o.Ignore())
                .ForMember(t => t.CreatedOn, o => o.Ignore())
                .ForMember(t => t.Id , o => o.MapFrom(s=>  s.PartyCorrespondenceAttachmentId))

                ;

            Mapper.CreateMap<PartyCorrespondenceAttachment, PartyCorrespondenceAttachmentDto>()
                .ForMember(t => t.PartyCorrespondenceId, o => o.MapFrom(s => s.PartyCorrespondence.Id))
                ;
        }
    }
}