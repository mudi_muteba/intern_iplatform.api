﻿using System;
using System.Collections.Generic;

using AutoMapper;

using Domain.Party.Correspondence;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Party.Correspondence;
using RestSharp.Contrib;

namespace Domain.Party.PartyCorrespondencePreferences.Mapping
{
    public class PartyCorrespondenceMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PartyCorrespondence, PartyCorrespondenceDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                .ForMember(t => t.Body, o => o.MapFrom(s => HttpUtility.HtmlEncode(s.Body)))
                ;

            Mapper.CreateMap<CreatePartyCorrespondenceDto, PartyCorrespondence>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.CreatedOn, o => o.MapFrom(s => DateTime.UtcNow))
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Attachments, o => o.Ignore())
                ;

            Mapper.CreateMap<List<PartyCorrespondence>, ListResultDto<PartyCorrespondenceDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<PartyCorrespondenceDto, PartyCorrespondence>()
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Attachments, o => o.Ignore())
                .ForMember(t => t.CreatedOn, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.To, o => o.MapFrom(s => new Party { Id = s.To.Id }))
                .ForMember(t => t.From, o => o.MapFrom(s => new Party { Id = s.From.Id }))
                ;

            Mapper.CreateMap<CreatePartyCorrespondenceDto, PartyCorrespondenceDto>()
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Id, o => o.MapFrom(s => s.PartyCorrespondenceId))
                .ForMember(t => t.Attachments, o => o.Ignore())
                .ForMember(t => t.To, o => o.MapFrom(s => new PartyDto { Id = s.To }))
                .ForMember(t => t.From, o => o.MapFrom(s => new PartyDto { Id = s.From }))
                ;
        }
    }
}