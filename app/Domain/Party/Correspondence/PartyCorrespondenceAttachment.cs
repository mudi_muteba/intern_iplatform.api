﻿using System;

using AutoMapper;

using Domain.Base;

using iPlatform.Api.DTOs.Party.Correspondence;

namespace Domain.Party.Correspondence
{
    public class PartyCorrespondenceAttachment : EntityWithPartyCorrespondence
    {
        public PartyCorrespondenceAttachment()
        {
            CreatedOn = DateTime.UtcNow;
        }

        public virtual string FileName { get; set; }
        public virtual string FileUri { get; set; }
        public virtual DateTime CreatedOn { get; set; }

        public static PartyCorrespondenceAttachment Create(CreatePartyCorrespondenceAttachmentDto createPartyCorrespondenceAttachmentDto)
        {
            var partyCorrespondenceAttachment = Mapper.Map<PartyCorrespondenceAttachment>(createPartyCorrespondenceAttachmentDto);
            return partyCorrespondenceAttachment;
        }

        public virtual void Update(CreatePartyCorrespondenceAttachmentDto dto)
        {
            var dto2 = Mapper.Map<PartyCorrespondenceAttachmentDto>(dto);
            Mapper.Map(dto2, this);
        }
    }
}