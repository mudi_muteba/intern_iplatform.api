﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

using Domain.Base;
using Domain.Individuals;
using iPlatform.Api.DTOs.Party.Correspondence;

namespace Domain.Party.Correspondence
{
    public class PartyCorrespondence : EntityWithParty
    {
        public PartyCorrespondence()
        {
            CreatedOn = DateTime.UtcNow;
            Attachments =  new List<PartyCorrespondenceAttachment>();
        }

        public virtual string Description { get; set; }

        public virtual string Body { get; set; }
        public virtual DateTime CreatedOn { get;  set; }
        public virtual Party From { get; set; }
        public virtual Party To { get; set; }
        public virtual string EmailRecipient { get; set; }

        public virtual IList<PartyCorrespondenceAttachment> Attachments { get; set; }

        public static PartyCorrespondence Create(CreatePartyCorrespondenceDto createPartyCorrespondenceDto)
        {
            var partyCorrespondence = Mapper.Map<PartyCorrespondence>(createPartyCorrespondenceDto);
            return partyCorrespondence;
        }

        public virtual void Update(CreatePartyCorrespondenceDto dto)
        {
            var dto2 = Mapper.Map<PartyCorrespondenceDto>(dto);
            Mapper.Map(dto2, this);
            RemoveAttachment(dto);

            foreach (var attachment in dto.Attachments)
            {
                var entity = Attachments.FirstOrDefault(x => x.Id == attachment.PartyCorrespondenceAttachmentId);
                if (entity == null)
                    AddAttachment(attachment);
                else
                    entity.Update(attachment);
            }
        }

        public virtual void AddAttachment(CreatePartyCorrespondenceAttachmentDto dto)
        {
            var attachment = PartyCorrespondenceAttachment.Create(dto);
            attachment.PartyCorrespondence = this;
            this.Attachments.Add(attachment);
        }

        public virtual void RemoveAttachment(CreatePartyCorrespondenceDto dto)
        {
            var toDelete = Attachments.Where(x => dto.Attachments.Any(c => c.PartyCorrespondenceAttachmentId != x.Id));

            foreach (var attachment in toDelete)
            {
                attachment.Delete();
            }
        }

    }
}