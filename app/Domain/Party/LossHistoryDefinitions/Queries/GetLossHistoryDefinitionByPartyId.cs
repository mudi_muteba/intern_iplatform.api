﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;


namespace Domain.Party.LossHistoryDefinitions.Queries
{
    public class GetLossHistoryDefinitionByPartyId : BaseQuery<LossHistoryDefinition>
    {
        public GetLossHistoryDefinitionByPartyId(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<LossHistoryDefinition>())
        {
        }

        public int partyId;
        public void WithPartyId(int partyId)
        {
            this.partyId = partyId;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<LossHistoryDefinition> Execute(IQueryable<LossHistoryDefinition> query)
        {
            return query
                .Where(c => c.Id == partyId);
        }
    }
}
