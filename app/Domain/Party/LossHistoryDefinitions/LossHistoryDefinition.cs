﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.Party.LossHistoryDefinitions;

namespace Domain.Party.LossHistoryDefinitions
{
    public class LossHistoryDefinition : Entity
    {
        public virtual int PartyId { get; set; }
        public virtual int PartyTypeId { get; set; }
        public virtual DateTime Created { get; set; }

        public LossHistoryDefinition()
        {

        }

        public static LossHistoryDefinition Create(CreateLossHistoryDefinitionDto dto)
        {
            var lossHistoryDefinition = Mapper.Map<LossHistoryDefinition>(dto);
            return lossHistoryDefinition;
        }

        public virtual void Disable()
        {
            this.Delete();
        }
    }
}
