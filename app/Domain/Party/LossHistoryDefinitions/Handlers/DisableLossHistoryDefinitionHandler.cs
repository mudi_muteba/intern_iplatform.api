﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.LossHistoryDefinitions;
using MasterData.Authorisation;

namespace Domain.Party.LossHistoryDefinitions.Handlers
{
    public class DisableLossHistoryDefinitionHandler : ExistingEntityDtoHandler<LossHistoryDefinition, DisableLossHistoryDefinitionDto, int>
    {
        public DisableLossHistoryDefinitionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(LossHistoryDefinition entity, DisableLossHistoryDefinitionDto dto, HandlerResult<int> result)
        {
            entity.Disable();
            result.Processed(entity.Id);
        }
    }
}
