﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.LossHistoryDefinitions;
using MasterData.Authorisation;

namespace Domain.Party.LossHistoryDefinitions.Handlers
{
    public class CreateLossHistoryDefinitionHandler : CreationDtoHandler<LossHistoryDefinition, CreateLossHistoryDefinitionDto, int>
    {
        public CreateLossHistoryDefinitionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(LossHistoryDefinition entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override LossHistoryDefinition HandleCreation(CreateLossHistoryDefinitionDto dto, HandlerResult<int> result)
        {
            var lossHistory = LossHistoryDefinition.Create(dto);
            return lossHistory;
        }

    }
}
