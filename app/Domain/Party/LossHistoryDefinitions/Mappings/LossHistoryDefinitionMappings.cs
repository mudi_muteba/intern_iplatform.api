﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using iPlatform.Api.DTOs.Party.LossHistoryDefinitions;

namespace Domain.Party.LossHistoryDefinitions.Mappings
{
    public class LossHistoryDefinitionMappings : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<LossHistoryDefinition, LossHistoryDefinitionDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.PartyId))
                .ForMember(t => t.PartyTypeId, o => o.MapFrom(s => s.PartyTypeId))
                ;
        }
    }
}
