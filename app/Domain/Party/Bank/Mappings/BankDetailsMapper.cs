﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Bank;

namespace Domain.Party.Bank.Mappings
{
    public class BankDetailsMapper : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<BankDetails, BankDetailsDto>()
                .ForMember(t => t. Events, o => o.MapFrom(s => s.Audit));

            Mapper.CreateMap<List<ListBankDetailsDto>, ListResultDto<ListBankDetailsDto>>();

            Mapper.CreateMap<List<BankDetails>, ListResultDto<ListBankDetailsDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<CreateBankDetailsDto, BankDetails>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore());

            Mapper.CreateMap<BankDetails, ListBankDetailsDto>();
            Mapper.CreateMap<BankDetails, BankDetailsDto>();
            Mapper.CreateMap<EditBankDetailsDto, BankDetails>();
        
        }
    }
}