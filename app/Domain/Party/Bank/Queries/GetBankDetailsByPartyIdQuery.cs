﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.Bank.Queries
{
    public class GetBankDetailsByPartyIdQuery : BaseQuery<BankDetails>
    {
        private int _partyId;

        public GetBankDetailsByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<BankDetails>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetBankDetailsByPartyIdQuery WithPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<BankDetails> Execute(IQueryable<BankDetails> query)
        {
            return Repository.GetAll<BankDetails>().Where(a => a.Party.Id == _partyId);
        }
    }
}