﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Bank;
using MasterData.Authorisation;

namespace Domain.Party.Bank.Handlers
{
    public class DeleteBankDetailsHandler : CreationDtoHandler<BankDetails, DeleteBankDetailsDto, int>
    {
        public DeleteBankDetailsHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    BankDetailsAuthorisation.Delete
                };
            }
        }

        protected override void EntitySaved(BankDetails entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override BankDetails HandleCreation(DeleteBankDetailsDto dto, HandlerResult<int> result)
        {
            var party = _repository.GetById<Domain.Party.Party>(dto.PartyId);
            BankDetails BankDetails = party.DeleteBankDetails(dto);

            result.Processed(dto.Id);
            return BankDetails;
        }
    }
}