﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Bank;
using MasterData.Authorisation;

namespace Domain.Party.Bank.Handlers
{
    public class UpdateBankDetailsHandler : ExistingEntityDtoHandler<Party, EditBankDetailsDto, int>
    {
        public UpdateBankDetailsHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    BankDetailsAuthorisation.Delete
                };
            }
        }

        protected override Party GetEntityById(EditBankDetailsDto dto)
        {
            return repository.GetById<Party>((int)dto.PartyId);
        }

        protected override void HandleExistingEntityChange(Party entity, EditBankDetailsDto dto,
            HandlerResult<int> result)
        {
            entity.UpdateBankDetails(dto);
        }
    }
}