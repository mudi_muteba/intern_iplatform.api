﻿using AutoMapper;
using Domain.Base;
using Domain.Base.Events;
using Domain.Party.Bank.Events;
using iPlatform.Api.DTOs.Party.Bank;

namespace Domain.Party.Bank
{
    public class BankDetails : EntityWithParty
    {
        public virtual string BankAccHolder { get; set; }

        public virtual string Bank { get; set; }

        public virtual string BankBranch { get; set; }

        public virtual string BankBranchCode { get; set; }

        public virtual string AccountNo { get; set; }

        public virtual string TypeAccount { get; set; }

        public virtual void Update(EditBankDetailsDto dto)
        {
            Mapper.Map(dto, this);
            RaiseEvent(new BankDetailsUpdatedEvent(this, dto.CreateAudit(), Party.Channel));
        }
    }
}