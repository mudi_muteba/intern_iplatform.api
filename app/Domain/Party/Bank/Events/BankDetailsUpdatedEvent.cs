﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Party.Bank.Events
{
    public class BankDetailsUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public BankDetailsUpdatedEvent(BankDetails _BankDetails, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> {channelReferences})
        {
            BankDetails = _BankDetails;
        }

        public BankDetails BankDetails { get; private set; }
        public int UserId { get; private set; }
    }
}