﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Party.Contacts;
using Domain.Party.Contacts.Queries;
using Domain.Party.Relationships;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Party.Contacts;
using MasterData;
using MasterData.Authorisation;


namespace Domain.Party.RegisteredOwnerContacts
{
    public class GetRegOwnerContactsByPartyIdQuery : BaseQuery<Contact>
    {
        private readonly IRepository _repository;
        private int _partyId;
        public GetRegOwnerContactsByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
                : base(contextProvider, repository, new NoDefaultFilters<Contact>())
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        public GetRegOwnerContactsByPartyIdQuery WithPartyId(int partyId)
        {
            _partyId = partyId;
            return this;
        }

        protected internal override IQueryable<Contact> Execute(IQueryable<Contact> query)
        {
            var result = _repository.GetAll<Relationship>()
                .Where(a => a.Party.Id == _partyId)
                .Select(a => a.ChildParty as Contact).ToList().AsQueryable();
            var regOwnerContacts = result.Where(a => a.ContactType == "regOwnerContact");
            
            return regOwnerContacts;
        }
    }
}