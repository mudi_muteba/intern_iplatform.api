﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;
using AutoMapper;
using iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions;
using MasterData;

namespace Domain.Party.LossHistoryQuestionDefinitions
{
    public class LossHistoryQuestionDefinition : Entity
    {
        public virtual string DisplayName { get; set; }
        public virtual int LossHistoryQuestionDefinitionId { get; set; }
        public virtual string ClientCode { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual bool RequierdForQuote { get; set; }
        public virtual bool RatingFactor { get; set; }
        public virtual bool IsReadOnly { get; set; }
        public virtual string ToolTip { get; set; }
        public virtual string DefaultValue { get; set; }
        public virtual string RegexPattern { get; set; }
        public virtual int LossHistoryQuestionGroupId { get; set; }
        public virtual int QuestionTypeId { get; set; }

        public LossHistoryQuestionDefinition()
        {

        }

        public static LossHistoryQuestionDefinition Create(CreateLossHistoryQuestionDefinitionDto dto)
        {
            var lhqd = Mapper.Map<LossHistoryQuestionDefinition>(dto);
            return lhqd;
        }
    }
}
