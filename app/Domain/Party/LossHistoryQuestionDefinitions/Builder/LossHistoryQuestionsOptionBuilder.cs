﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions.Builders;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions;

namespace Domain.Party.LossHistoryQuestionDefinitions.Builder
{
    public class LossHistoryQuestionsOptionBuilder
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(ProposalDefinitionBuilderDto));
        private readonly IRepository _repository;

        public LossHistoryQuestionsOptionBuilder(IRepository repository)
        {
            _repository = repository;
        }

        public ListResultDto<LossHistoryQuestionDefinitionAnswersDto> BuilDefinitionAnswersDtos(List<LossHistoryQuestionDefinition> questionDefinitionDto)
        {
            if (questionDefinitionDto == null || questionDefinitionDto.Count == 0)
                return new ListResultDto<LossHistoryQuestionDefinitionAnswersDto>();

            var result = new ListResultDto<LossHistoryQuestionDefinitionAnswersDto>();

            foreach (var questionDefinition in questionDefinitionDto)
            {
                var mdQueryResult = new MasterData.LossHistoryQuestionAnswers().Where(x => x.Question.Id == questionDefinition.Id)
                    .OrderByDescending(a => a.VisibleIndex)
                    .ToList();

                var dto = new LossHistoryQuestionDefinitionAnswersDto
                {
                    DefinitionDto =
                        Mapper.Map<LossHistoryQuestionDefinition, LossHistoryQuestionDefinitionDto>(questionDefinition),
                    Answers = mdQueryResult
                };

                result.Results.Add(dto);
            }

            return result;
        }
    }
}
