﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions;

namespace Domain.Party.LossHistoryQuestionDefinitions.Mappings
{
    public class LossHistoryQuestionDefinitionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateLossHistoryQuestionDefinitionDto, LossHistoryQuestionDefinition>()
                ;

            Mapper.CreateMap<LossHistoryQuestionDefinitionDto, LossHistoryQuestionDefinition>()
                ;

            Mapper.CreateMap<LossHistoryQuestionDefinition, LossHistoryQuestionDefinitionDto>()
                ;

            Mapper.CreateMap<List<LossHistoryQuestionDefinition>, ListResultDto<LossHistoryQuestionDefinitionDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));
            ;

            Mapper.CreateMap<List<LossHistoryQuestionDefinitionDto>, ListResultDto<LossHistoryQuestionDefinition>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
            ;

            Mapper.CreateMap<List<LossHistoryQuestionDefinition>, ListResultDto<LossHistoryQuestionDefinition>>()
               .ForMember(t => t.Results, o => o.MapFrom(s => s));
            ;

            Mapper.CreateMap<PagedResults<LossHistoryQuestionDefinition>, PagedResultDto<LossHistoryQuestionDefinitionDto>>();
        }
    }
}
