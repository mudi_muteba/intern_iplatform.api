﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions;
using MasterData.Authorisation;

namespace Domain.Party.LossHistoryQuestionDefinitions.Queries
{
    public class SearchQuestionDefinitionsByCriteriaQuery : BaseQuery<LossHistoryQuestionDefinition>,
        ISearchQuery<LossHistoryQuestionDefinitionSearchDto>
    {
        private LossHistoryQuestionDefinitionSearchDto Criteria;

        public SearchQuestionDefinitionsByCriteriaQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<LossHistoryQuestionDefinition>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(LossHistoryQuestionDefinitionSearchDto criteria)
        {
            this.Criteria = criteria;
        }

        protected internal override IQueryable<LossHistoryQuestionDefinition> Execute(
            IQueryable<LossHistoryQuestionDefinition> query)
        {

            if (string.IsNullOrEmpty(Criteria.ClientCode)) return null;

            if (Criteria.QuestionTypeId > 0)
            {
                query = query.Where(
                    x => x.ClientCode == Criteria.ClientCode && x.QuestionTypeId == Criteria.QuestionTypeId)
                    .OrderByDescending(x => x.VisibleIndex);
                return query;
            }

            query = query.Where(x => x.ClientCode == Criteria.ClientCode)
                .OrderByDescending(x => x.VisibleIndex);

            return query;
        }
    }
}

