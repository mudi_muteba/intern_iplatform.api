﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Party.LossHistoryQuestionDefinitions.Queries
{
    public class GetQuestionDefinitionByIdQuery : BaseQuery<LossHistoryQuestionDefinition>
    {
        public GetQuestionDefinitionByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<LossHistoryQuestionDefinition>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public int id;
        public void WithGuid(int id)
        {
            this.id = id;
        }

        protected internal override IQueryable<LossHistoryQuestionDefinition> Execute(IQueryable<LossHistoryQuestionDefinition> query)
        {
            return query
                .Where(x => x.Id == id);
        }
    }
}
