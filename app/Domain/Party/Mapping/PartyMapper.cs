﻿using System.Linq;
using AutoMapper;
using Domain.Individuals;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party;

namespace Domain.Party.Mapping
{
    public class PartyMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Party, PartyDto>()
                .ForMember(t => t.Events, o => o.MapFrom(s => s.Audit));
            Mapper.CreateMap<Party, BasicIndividualDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Id));
            Mapper.CreateMap<Party, IndividualDto>()
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.LeadId, o => o.MapFrom(s => s.Lead.Id));
        }
    }
}