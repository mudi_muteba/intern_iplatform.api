﻿using System;
using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base;

namespace Domain.AuditLogs.Events
{
    public class AuditLogEvent : ExpressDomainEvent, IDomainEvent
    {
        public string ChannelIds { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string AuditEntryType { get; set; }
        public string AuditEntryDescription { get; set; }
        public string EntityName { get; set; }
        public int EntityId { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime Timestamp { get; set; }

        public AuditLogEvent(DtoContext dtoContext, string auditEntryType, string auditEntryDescription, string entityFullName, int entityId, DateTime timestamp, string fieldName = "", string oldValue = "", string newValue = "")
        {
            ChannelIds = "";
            UserId = dtoContext.UserId;
            UserName = dtoContext.Username;
            AuditEntryType = auditEntryType;
            AuditEntryDescription = auditEntryDescription;
            EntityName = entityFullName;
            EntityId = entityId;
            FieldName = fieldName;
            OldValue = oldValue;
            NewValue = newValue;
            Timestamp = timestamp;
        }

        public AuditLogEvent(ExecutionContext executionContext, string auditEntryType, string auditEntryDescription, string entityFullName, int entityId, DateTime timestamp, string fieldName = "", string oldValue = "", string newValue = "")
        {
            ChannelIds = executionContext.ChannelIds;
            UserId = executionContext.UserId;
            UserName = executionContext.Username;
            AuditEntryType = auditEntryType;
            AuditEntryDescription = auditEntryDescription;
            EntityName = entityFullName;
            EntityId = entityId;
            FieldName = fieldName;
            OldValue = oldValue;
            NewValue = newValue;
            Timestamp = timestamp;
        }

        public EventAudit Audit { get; private set; }
        public bool IsVersioned { get; private set; }
        public List<ChannelReference> ChannelReferences { get; private set; }
    }
}
