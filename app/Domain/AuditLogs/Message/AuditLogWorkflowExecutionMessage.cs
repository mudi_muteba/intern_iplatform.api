﻿using iPlatform.Api.DTOs.Audit;
using Workflow.Messages;

namespace Domain.AuditLogs.Message
{
    public class AuditLogWorkflowExecutionMessage : WorkflowExecutionMessage
    {
        public CreateAuditLogDto CreateAuditLogDto { get; set; }

        public AuditLogWorkflowExecutionMessage() { }

        public AuditLogWorkflowExecutionMessage(CreateAuditLogDto createAuditLogDto,
            WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
            CreateAuditLogDto = createAuditLogDto;
        }
    }
}
