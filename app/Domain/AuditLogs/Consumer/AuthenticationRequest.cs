﻿using iPlatform.Api.DTOs.Users.Authentication;
using Infrastructure.Configuration;

namespace Domain.AuditLogs.Consumer
{
    public static class AuthenticationRequest
    {
        public static AuthenticationRequestDto GetRequest()
        {
            return new AuthenticationRequestDto
            {
                Email = ConfigurationReader.Connections.iPlatform,
                Password = ConfigurationReader.Connections.iPlatformKey,
                IPAddress = "12.0.0.1",
                System = "iPlatform.Workflow.Engine"
            };
        }
    }
}
