﻿using System;
using System.Net;
using Domain.Admin;
using Domain.AuditLogs.Message;
using Domain.AuditUserLog.Message;
using iPlatform.Api.DTOs.AuditUserLog;
using iPlatform.Api.DTOs.Base.Connector;
using Infrastructure.Configuration;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.AuditLogs.Consumer
{
    public class AuditLogWorkflowExecutionMessageConsumer : AbstractMessageConsumer<AuditLogWorkflowExecutionMessage>
    {
        private readonly IConnector _connector;
        private readonly IWorkflowRouter _router;

        public AuditLogWorkflowExecutionMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
            _router = router;
        }

        public override void Consume(AuditLogWorkflowExecutionMessage message)
        {
            this.Info(() => string.Format("Logging to Audit: {0} by UserId {1}", message.CreateAuditLogDto, message.CreateAuditLogDto.UserId));

            var response = _connector.AuditManagement.Audit.Save(message.CreateAuditLogDto);

            if (response.StatusCode == HttpStatusCode.OK)
                this.Info(() => string.Format("Successfully Logged to Audit: {0} by UserId {1}", message.CreateAuditLogDto, message.CreateAuditLogDto.UserId));
            else
                this.Error(() => string.Format("Logging to Audit: {0} by UserId {1} - {2}", message.CreateAuditLogDto, message.CreateAuditLogDto.UserId, response.Errors[0].DefaultMessage));

            PublishAuditUserLogMessage(message);
        }

        private void PublishAuditUserLogMessage(AuditLogWorkflowExecutionMessage message)
        {
            if (message.CreateAuditLogDto.AuditEntryDescription == "User authenticated successfully")
            {
                AuditUserLogDto createAuditUserLogDto = new AuditUserLogDto()
                {
                    RequestUrl = message.CreateAuditLogDto.RequestUrl,
                    Timestamp = DateTime.UtcNow,
                    Username = message.CreateAuditLogDto.UserName,
                    Event = message.CreateAuditLogDto.AuditEntryDescription,
                    SystemId = message.CreateAuditLogDto.ActiveChannelSystemId,
                    EnvironmentType = new AdminConfigurationReader().EnvironmentTypeKey
                };

                _router.Publish(
                    new WorkflowRoutingMessage().AddMessage(
                        new AuditUserLogMessage(createAuditUserLogDto)));
            }
        }
    }
}