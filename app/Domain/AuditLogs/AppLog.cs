﻿using Domain.Base;

namespace Domain.AuditLogs
{
    public class AppLog : Entity
    {
        public virtual string Date { get; protected set; }
        public virtual string Thread { get; protected set; }
        public virtual string Level { get; protected set; }
        public virtual string Logger { get; protected set; }
        public virtual string Message { get; protected set; }
        public virtual string Exception { get; protected set; }
        public virtual string RequestId { get; set; }
        public virtual string UserId { get; set; }
        public virtual string ActiveChannelSystemId { get; set; }
        public virtual string RequestMethod { get; set; }
        public virtual string RequestUrl { get; set; }
    }
}