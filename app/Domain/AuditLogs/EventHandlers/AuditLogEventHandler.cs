﻿using Domain.AuditLogs.Events;
using Domain.AuditLogs.Message;
using Domain.Base.Events;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Audit;
using Workflow.Messages;

namespace Domain.AuditLogs.EventHandlers
{
    public class AuditLogEventHandler : BaseEventHandler<AuditLogEvent>
    {
        private readonly IWorkflowRouter _router;
        private readonly IProvideContext _contextProvider;

        public AuditLogEventHandler(IWorkflowRouter router, IProvideContext contextProvider)
        {
            _router = router;
            _contextProvider = contextProvider;
        }

        public override void Handle(AuditLogEvent @event)
        {
            var auditLogDto = new CreateAuditLogDto(_contextProvider.Get(), @event.AuditEntryType, @event.AuditEntryDescription, @event.EntityName,
                @event.EntityId, @event.Timestamp, @event.FieldName, @event.OldValue, @event.NewValue);
            _router.Publish(new WorkflowRoutingMessage().AddMessage(new AuditLogWorkflowExecutionMessage(auditLogDto)));
        }
    }
}
