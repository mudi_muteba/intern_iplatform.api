﻿using System;
using Domain.Base;
using Domain.Base.Execution;
using Domain.Base.Extentions;
using Infrastructure.NHibernate.Attributes;

namespace Domain.AuditLogs
{
    public class AuditLog : Entity
    {
        [DoNotMap]
        public static int MaxFieldLength = 500;

        public virtual string UserAgent { get; protected set; }
        public virtual string Referrer { get; protected set; }
        public virtual Guid RequestId { get; protected set; }
        public virtual string RequestType { get; protected set; }
        public virtual string RequestIp { get; protected set; }
        public virtual DateTime? RequestDate { get; protected set; }
        public virtual string RequestMethod { get; protected set; }
        public virtual string RequestUrl { get; protected set; }
        public virtual string ChannelIds { get; protected set; }
        public virtual int ActiveChannelId { get; protected set; }
        public virtual int UserId { get; protected set; }
        public virtual string UserName { get; protected set; }
        public virtual string AuditEntryType { get; protected set; }
        public virtual string AuditEntryDescription { get; protected set; }
        public virtual string EntityName { get; protected set; }
        public virtual int EntityId { get; protected set; }
        public virtual string FieldName { get; protected set; }
        public virtual string OldValue { get; protected set; }
        public virtual string NewValue { get; protected set; }
        public virtual DateTime Timestamp { get; protected set; }

        public AuditLog() {}

        public AuditLog(ExecutionContext executionContext, string auditEntryType, string auditEntryDescription, string entityFullName, int entityId, DateTime timestamp, string fieldName = "", string oldValue = "", string newValue = "")
        {
            if (executionContext != null)
            {
                UserAgent = executionContext.UserAgent.TrimFirst(MaxFieldLength);
                Referrer = executionContext.Referrer.TrimFirst(MaxFieldLength);
                RequestId = executionContext.RequestId;
                RequestType = executionContext.RequestType;
                RequestIp = executionContext.RequestIp;
                RequestDate = executionContext.RequestDate;
                RequestMethod = executionContext.RequestMethod;
                RequestUrl = executionContext.RequestUrl.TrimFirst(MaxFieldLength);
                ChannelIds = executionContext.ChannelIds;
                ActiveChannelId = executionContext.ActiveChannelId;
                UserId = executionContext.UserId;
                UserName = executionContext.Username;
            }
            
            AuditEntryType = auditEntryType;
            AuditEntryDescription = auditEntryDescription.TrimFirst(MaxFieldLength);
            EntityName = entityFullName.TrimFirst(MaxFieldLength);
            EntityId = entityId;
            FieldName = fieldName;
            OldValue = oldValue.TrimFirst(MaxFieldLength);
            NewValue = newValue.TrimFirst(MaxFieldLength);
            Timestamp = timestamp;
        }

        public AuditLog(string userAgent, string referrer, Guid requestId, string requestType, string requestIp, DateTime? requestDate, string requestMethod, string requestUrl, string channelIds, int activeChannelId, int userId, string userName, string auditEntryType, string auditEntryDescription, string entityName, int entityId, string fieldName, string oldValue, string newValue, DateTime timestamp)
        {
            UserAgent = userAgent;
            Referrer = referrer;
            RequestId = requestId;
            RequestType = requestType;
            RequestIp = requestIp;
            RequestDate = requestDate;
            RequestMethod = requestMethod;
            RequestUrl = requestUrl;
            ChannelIds = channelIds;
            ActiveChannelId = activeChannelId;
            UserId = userId;
            UserName = userName;
            AuditEntryType = auditEntryType;
            AuditEntryDescription = auditEntryDescription;
            EntityName = entityName;
            EntityId = entityId;
            FieldName = fieldName;
            OldValue = oldValue;
            NewValue = newValue;
            Timestamp = timestamp;
        }
    }
}