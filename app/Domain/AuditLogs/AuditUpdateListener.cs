﻿using System;
using Domain.Base.Execution;
using Microsoft.Practices.ServiceLocation;
using NHibernate;
using NHibernate.Event;

namespace Domain.AuditLogs
{
    public class AuditUpdateListener : IPostInsertEventListener, IPostUpdateEventListener
    {
        private static string GetStringValueFromStateArray(object[] stateArray, int position)
        {
            var value = stateArray[position];
            return value == null || value.ToString() == string.Empty
                ? string.Empty
                : value.ToString();
        }

        public void OnPostInsert(PostInsertEvent @event)
        {
            if (@event.Entity is AuditLog)
                return;

            var session = @event.Session.GetSession(EntityMode.Poco);
            session.Save(new AuditLog(ServiceLocator.Current.GetInstance<IProvideContext>().Get(), "Insert", string.Empty, @event.Entity.GetType().FullName, (int) @event.Id, DateTime.UtcNow));
            session.Flush();
        }

        public void OnPostUpdate(PostUpdateEvent @event)
        {
            if (@event.Entity is AuditLog)
                return;

            var entityFullName = @event.Entity.GetType().FullName;
            if (@event.OldState == null)
                throw new ArgumentNullException("No old state available for entity type '" + entityFullName + "'. Make sure you're loading it into Session before modifying and saving it.");

            var session = @event.Session.GetSession(EntityMode.Poco);

            var dirtyFieldIndexes = @event.Persister.FindDirty(@event.State, @event.OldState, @event.Entity, @event.Session);
            foreach (var dirtyFieldIndex in dirtyFieldIndexes)
            {
                var oldValue = GetStringValueFromStateArray(@event.OldState, dirtyFieldIndex);
                var newValue = GetStringValueFromStateArray(@event.State, dirtyFieldIndex);
                if (oldValue == newValue)
                    continue;

                session.Save(new AuditLog(ServiceLocator.Current.GetInstance<IProvideContext>().Get(), "Update", string.Empty, entityFullName, (int) @event.Id, 
                    DateTime.UtcNow, @event.Persister.PropertyNames[dirtyFieldIndex], oldValue, newValue));
            }

            session.Flush();
        }
    }
}