﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Audit;
using MasterData.Authorisation;

namespace Domain.AuditLogs.DtoHandlers
{
    public class CreateAuditLogDtoHandler : CreationDtoHandler<AuditLog, CreateAuditLogDto, int>
    {
        public CreateAuditLogDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void EntitySaved(AuditLog entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override AuditLog HandleCreation(CreateAuditLogDto dto, HandlerResult<int> result)
        {
            var auditLog = new AuditLog(dto.UserAgent, dto.Referrer, dto.RequestId, dto.RequestType, dto.RequestIp, dto.RequestDate,
                dto.RequestMethod, dto.RequestUrl, dto.ChannelIds, dto.ActiveChannelId, dto.UserId, dto.UserName, dto.AuditEntryType,
                dto.AuditEntryDescription, dto.EntityName,
                dto.EntityId, dto.FieldName, dto.OldValue, dto.NewValue, dto.Timestamp);

            result.Processed(auditLog.Id);

            return auditLog;
        }
    }
}