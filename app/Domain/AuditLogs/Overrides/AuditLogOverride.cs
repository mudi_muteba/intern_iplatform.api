﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.AuditLogs.Overrides
{
    public class AuditLogOverride : IAutoMappingOverride<AuditLog>
    {
        public void Override(AutoMapping<AuditLog> mapping)
        {
            mapping.IgnoreProperty(x => x.IsDeleted);
            mapping.IgnoreProperty(x => x.SimpleAudit);
            mapping.IgnoreProperty(x => x.Channels);
            mapping.IgnoreProperty(x => x.ChannelTags);
            mapping.Map(x => x.UserAgent).Length(AuditLog.MaxFieldLength);
            mapping.Map(x => x.Referrer).Length(AuditLog.MaxFieldLength);
            mapping.Map(x => x.RequestIp).Length(20);
            mapping.Map(x => x.RequestMethod).Length(10);
            mapping.Map(x => x.RequestUrl).Length(AuditLog.MaxFieldLength);
            mapping.Map(x => x.AuditEntryDescription).Length(AuditLog.MaxFieldLength);
            mapping.Map(x => x.EntityName).Length(AuditLog.MaxFieldLength);
            mapping.Map(x => x.OldValue).Length(AuditLog.MaxFieldLength);
            mapping.Map(x => x.NewValue).Length(AuditLog.MaxFieldLength);
        }
    }
}