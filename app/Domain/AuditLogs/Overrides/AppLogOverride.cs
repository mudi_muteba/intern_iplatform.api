﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.AuditLogs.Overrides
{
    public class AppLogOverride : IAutoMappingOverride<AppLog>
    {
        public void Override(AutoMapping<AppLog> mapping)
        {
            mapping.IgnoreProperty(x => x.IsDeleted);
            mapping.IgnoreProperty(x => x.SimpleAudit);
            mapping.IgnoreProperty(x => x.Channels);
            mapping.IgnoreProperty(x => x.ChannelTags);
            mapping.Map(x => x.Level).Length(10);
            mapping.Map(x => x.Date).Length(30);
            mapping.Map(x => x.UserId).Length(10);
            mapping.Map(x => x.RequestId).Length(50);
            mapping.Map(x => x.ActiveChannelSystemId).Length(50);
            mapping.Map(x => x.Message).Length(4000);
            mapping.Map(x => x.Exception).Length(2000);
            mapping.Map(x => x.RequestMethod).Length(10);
        }
    }
}