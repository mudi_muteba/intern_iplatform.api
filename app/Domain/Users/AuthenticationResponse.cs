﻿namespace Domain.Users
{
    public class AuthenticationResponse
    {
        public AuthenticationResponse(bool isAuthenticated)
        {
            IsAuthenticated = isAuthenticated;
        }

        public AuthenticationResponse(bool isAuthenticated, string token, User user)
        {
            IsAuthenticated = isAuthenticated;
            Token = token;
            User = user;
        }

        public bool IsAuthenticated { get; private set; }
        public string Token { get; private set; }
        public User User { get; private set; }
    }
}