﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Users.Overrides
{
    public class UserChannelOverride : IAutoMappingOverride<UserChannel>
    {
        public void Override(AutoMapping<UserChannel> mapping)
        {
            mapping.References(x => x.User).Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.Channel).Fetch.Join().Cascade.None().LazyLoad(Laziness.NoProxy); ;
        }
    }
}