﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Users.Overrides
{
    public class UserIndividualOverride : IAutoMappingOverride<UserIndividual>
    {
        public void Override(AutoMapping<UserIndividual> mapping)
        {
            mapping.References(x => x.User).Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.Individual).Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy);
        }
    }
}