﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;
using MasterData;

namespace Domain.Users.Overrides
{
    public class UserAuthorisationGroupOverride : IAutoMappingOverride<UserAuthorisationGroup>
    {
        public void Override(AutoMapping<UserAuthorisationGroup> mapping)
        {
            mapping.References(x => x.User).LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.Channel).LazyLoad(Laziness.NoProxy);
        }
    }
}