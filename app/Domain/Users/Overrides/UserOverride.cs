﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Users.Overrides
{
    public class UserOverride : IAutoMappingOverride<User>
    {
        public void Override(AutoMapping<User> mapping)
        {
            mapping.Id().GeneratedBy.Native();
            mapping.Map(x => x.Password).Length(10000);
            mapping.Map(x => x.UserName).Length(512);

            mapping.Component(x => x.SimpleAudit, m =>
            {
                m.Map(a => a.Timestamps.CreatedOn, "CreatedOn").Nullable();
                m.Map(a => a.Timestamps.ModifiedOn, "ModifiedOn").Nullable();
            });

            mapping.Component(u => u.Status).ColumnPrefix("");
            mapping.Component(u => u.Login, m =>
            {
                m.Map(a => a.LastLoggedInDate);
                m.Map(a => a.LoggedInFromIP).Length(25);
                m.Map(a => a.LoggedInFromSystem).Length(512);
            });
            mapping.Component(u => u.Tokens, m =>
            {
                m.Map(x => x.RegistrationToken);
                m.Map(x => x.PasswordResetToken);
            }).ColumnPrefix("");

            mapping.HasMany(x => x.Channels).Fetch.Join().Inverse().Cascade.SaveUpdate();
            mapping.HasMany(x => x.UserIndividuals).Cascade.SaveUpdate();
            mapping.HasMany(x => x.AuthorisationGroups).Cascade.SaveUpdate();
        }
    }
}