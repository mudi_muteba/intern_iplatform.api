using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using iPlatform.Api.DTOs.Users;
using MasterData;

namespace Domain.Users
{
    internal class UserAuthorisationGroupMaintainer
    {
        private readonly User _user;

        public UserAuthorisationGroupMaintainer(User user)
        {
            _user = user;
        }

        public void Allocate(List<UserAuthorisationGroupDto> userAuthorisationGroups)
        {
            foreach (var change in userAuthorisationGroups)
                Allocate(change);

            foreach (var original in _user.AuthorisationGroups)
            {
                var matchingChannel =
                    userAuthorisationGroups.FirstOrDefault(c => c.ChannelIds.Contains(original.Channel.Id));
                if (matchingChannel == null)
                {
                    original.Delete();
                    continue;
                }

                if (!matchingChannel.Groups.Any(g => g.Id == original.AuthorisationGroup.Id))
                    original.Delete();
            }

            // remove all authorisation groups which does not belong to the user's allocated channels
            foreach (var userAuthorisationGroup in _user.AuthorisationGroups.Where(ag => ag.IsDeleted != true))
            {
                if (_user.Channels.Any(c => c.Channel.Id == userAuthorisationGroup.Channel.Id && c.IsDeleted != true))
                    continue;
                userAuthorisationGroup.Delete();
            }
        }

        private void Allocate(UserAuthorisationGroupDto newAuthorisationGroup)
        {
            // make sure the authorisation group's channel has been allocated 
            // to the user
            if (
                !_user.Channels.Any(
                    cm => newAuthorisationGroup.ChannelIds.Contains(cm.Channel.Id) && cm.IsDeleted != true))
                return;

            foreach (var authorisationGroup in newAuthorisationGroup.Groups)
                Allocate(newAuthorisationGroup.ChannelIds, authorisationGroup);
        }

        private void Allocate(List<int> channelIds, AuthorisationGroup newAuthorisationGroup)
        {
            foreach (var channelId in channelIds)
            {
                var match =
                    _user.AuthorisationGroups.FirstOrDefault(
                        eg => eg.Channel.Id == channelId && eg.AuthorisationGroup.Id == newAuthorisationGroup.Id);

                if (match == null)
                    _user.AuthorisationGroups.Add(new UserAuthorisationGroup(new Channel(channelId), _user,
                        newAuthorisationGroup));
            }
        }
    }
}