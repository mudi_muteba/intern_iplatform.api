﻿using System;
using iPlatform.Api.DTOs.Users.Authentication;
using Shared;

namespace Domain.Users
{
    public class UserLogin
    {
        public virtual DateTime? LastLoggedInDate { get; protected internal set; }
        public virtual string LoggedInFromIP { get; protected internal set; }
        public virtual string LoggedInFromSystem { get; private set; }

        public UserLogin(DateTime? lastLoggedInDate, string loggedInFromIp)
        {
            LastLoggedInDate = lastLoggedInDate;
            LoggedInFromIP = loggedInFromIp;
        }

        protected internal UserLogin()
        {
        }

        public void LoggedIn(AuthenticationRequestDto request)
        {
            LastLoggedInDate = SystemTime.Now();
            LoggedInFromIP = request.IPAddress;
            LoggedInFromSystem = request.System;
        }

        protected bool Equals(UserLogin other)
        {
            return LastLoggedInDate.Equals(other.LastLoggedInDate) && string.Equals(LoggedInFromIP, other.LoggedInFromIP) && string.Equals(LoggedInFromSystem, other.LoggedInFromSystem);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((UserLogin) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = LastLoggedInDate.GetHashCode();
                hashCode = (hashCode*397) ^ (LoggedInFromIP != null ? LoggedInFromIP.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (LoggedInFromSystem != null ? LoggedInFromSystem.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}