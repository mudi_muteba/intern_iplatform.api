﻿using System;
using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Users.Events
{
    public class PasswordResetEvent : BaseDomainEvent
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public PasswordResetEvent(int userId, string userName, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            UserName = userName;
        }
    }
}