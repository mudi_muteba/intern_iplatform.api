﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Users.Events
{
    public class UserUpdatedEvent : BaseDomainEvent
    {
        public int UserId { get; set; }

        public UserUpdatedEvent(int userId, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            UserId = userId;
        }
    }
}