﻿using System;
using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Emailing.Factory;

namespace Domain.Users.Events
{
    public class PasswordResetRequestedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public string UserName { get; set; }
        public virtual Guid? PasswordResetToken { get; protected internal set; }
        public AppSettingsEmailSetting AppSettingsEmailSetting { get; protected internal set; }

        public PasswordResetRequestedEvent(string userName, Guid? passwordResetToken, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            UserName = userName;
            PasswordResetToken = passwordResetToken;
        }

        public PasswordResetRequestedEvent(string userName, Guid? passwordResetToken, EventAudit audit, IEnumerable<ChannelReference> channelReferences, AppSettingsEmailSetting appSettingsEmailSetting)
            : base(audit, channelReferences)
        {
            UserName = userName;
            PasswordResetToken = passwordResetToken;
            AppSettingsEmailSetting = appSettingsEmailSetting;
        }
    }
}