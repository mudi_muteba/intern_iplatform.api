﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Users.Events
{
    public class UserCreatedEvent : BaseDomainEvent
    {
        public string UserName { get; set; }

        public UserCreatedEvent(string userName, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            UserName = userName;
        }
    }
}