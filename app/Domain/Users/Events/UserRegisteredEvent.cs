﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Individual;
using Newtonsoft.Json;

namespace Domain.Users.Events
{
    public class UserRegisteredEvent : BaseDomainEvent, ExpressDomainEvent
    {
        [JsonIgnore]
        public User User { get; private set; }
        public CreateIndividualDto IndividualDto { get; private set; }

        public UserRegisteredEvent(User user, CreateIndividualDto individualDto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            User = user;
            IndividualDto = individualDto;
        }
    }
}