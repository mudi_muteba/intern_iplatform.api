﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Users.Events
{
    public class UserDisabledEvent : BaseDomainEvent
    {
        public string UserName { get; set; }

        public UserDisabledEvent(string userName, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            UserName = userName;
        }
    }
}