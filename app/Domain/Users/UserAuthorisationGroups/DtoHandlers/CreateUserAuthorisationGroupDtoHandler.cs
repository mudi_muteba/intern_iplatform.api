﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Users.UserAuthorisationGroups;
using Domain.Admin;

namespace Domain.Users.UserAuthorisationGroups.DtoHandlers
{
    public class CreateUserAuthorisationGroupDtoHandler : CreationDtoHandler<UserAuthorisationGroup, CreateUserAuthorisationGroupDto, int>
    {
        private readonly IProvideContext _contextProvider;
        private readonly IRepository _repository;

        public CreateUserAuthorisationGroupDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _contextProvider = contextProvider;
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(UserAuthorisationGroup entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override UserAuthorisationGroup HandleCreation(CreateUserAuthorisationGroupDto dto, HandlerResult<int> result)
        {
            var user = _repository.GetById<User>(dto.UserId);
            var channel = _repository.GetById<Channel>(dto.ChannelId);

            return new UserAuthorisationGroup(channel,user, dto.AuthorisationGroup);
        }
    }
}