﻿namespace Domain.Users
{
    public class UserStatus
    {
        public virtual bool IsApproved { get; protected internal set; }
        public virtual bool IsActive { get; protected internal set; }
        public virtual bool IsLoggedIn { get; protected internal set; }

        public virtual void LoggedIn()
        {
            IsLoggedIn = true;
        }

        public virtual void Approved()
        {
            IsActive = true;
            IsApproved = true;
        }

        public virtual void Disable()
        {
            IsActive = false;
            IsApproved = false;
        }

        protected bool Equals(UserStatus other)
        {
            return IsApproved.Equals(other.IsApproved) && IsActive.Equals(other.IsActive) && IsLoggedIn.Equals(other.IsLoggedIn);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((UserStatus) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = IsApproved.GetHashCode();
                hashCode = (hashCode*397) ^ IsActive.GetHashCode();
                hashCode = (hashCode*397) ^ IsLoggedIn.GetHashCode();
                return hashCode;
            }
        }
    }
}