﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Users;
using ValidationMessages.Users;

namespace Domain.Users.Validators
{
    public class ExistingUserIdValidator : IValidateDto<CreateUserChannelDto>
    {
        private readonly GetUserById _queryById;

        public ExistingUserIdValidator(IProvideContext contextProvider, IRepository repository)
        {
            _queryById = new GetUserById(contextProvider, repository);
        }

        private IQueryable<User> GetExistingUserById(int userId)
        {
            return _queryById.WithUserId(userId).ExecuteQuery();
        }
       
        private void Validate(int userId, ExecutionResult result)
        {
            var user = GetExistingUserById(userId);

            if (!user.Any())
            {
                result.AddValidationMessage(UserValidationMessages.UserNotFound);
            }
        }

        public void Validate(CreateUserChannelDto dto, ExecutionResult result)
        {
            Validate(dto.UserId, result);
        }
    }
}
