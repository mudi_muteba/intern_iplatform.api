﻿
using System.Linq;
using Domain.Base.Execution;
using Domain.Users.Queries;
using ValidationMessages.Users;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Users;
using System;

namespace Domain.Users.Validators
{
    public class UniqueUserExternalReferenceValidation : IValidateDto<CreateUserDto>, IValidateDto<EditUserDto>
    {
        private readonly GetUserByExternalReference _query;
        private readonly GetAllUsersQuery _queryAll;
        public UniqueUserExternalReferenceValidation(GetUserByExternalReference query, GetAllUsersQuery queryAll)
        {
            _query = query;
            _queryAll = queryAll;
        }

        public void Validate(EditUserDto dto, ExecutionResult result)
        {
            var existingUsers = GetAllExistingUsers();

            if (existingUsers.Any())
            {
                var channel = existingUsers.Where(c => c.ExternalReference == dto.ExternalReference && c.Id != dto.Id).ToList();

                if (channel.Any())
                {
                    result.AddValidationMessage(UserValidationMessages.DuplicateExternalReference.AddParameters(new[] { dto.ExternalReference }));
                }
            }
        }

        public void Validate(CreateUserDto dto, ExecutionResult result)
        {
            var existingUser = GetUserByExternalReference(dto.ExternalReference);

            if (existingUser.Any())
            {
                result.AddValidationMessage(UserValidationMessages.DuplicateExternalReference.AddParameters(new[] { dto.ExternalReference }));
            }
        }

        private IQueryable<User> GetUserByExternalReference(string externalReference)
        {
            var match = _query
                .WithUserExternalReference(externalReference)
                .ExecuteQuery();

            return match;
        }
        private IQueryable<User> GetAllExistingUsers()
        {
            return _queryAll.ExecuteQuery();
        }

    }
}