﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Users;
using ValidationMessages;

namespace Domain.Users.Validators
{
    public class ExistingUserChannelValidator : IValidateDto<CreateUserChannelDto>
    {
        private readonly GetUserChannelByUserIdChannelId _queryByUserIdChannelId;

        public ExistingUserChannelValidator(IProvideContext contextProvider, IRepository repository)
        {
            _queryByUserIdChannelId = new GetUserChannelByUserIdChannelId(contextProvider, repository);
        }

        private IQueryable<UserChannel> GetExistingUserChannel(int userId, int channelId)
        {
            return _queryByUserIdChannelId.WithUserIdChannelId(userId, channelId).ExecuteQuery();
        }

        private void Validate(int userId, int channelId, ExecutionResult result)
        {
            var userChannel = GetExistingUserChannel(userId, channelId);

            if (userChannel.Any())
            {
                result.AddValidationMessage(UserChannelValidationMessages.CombinationExist);
            }
        }

        public void Validate(CreateUserChannelDto dto, ExecutionResult result)
        {
            Validate(dto.UserId, dto.ChannelId, result);
        }
    }
}
