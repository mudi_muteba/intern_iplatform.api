using Domain.Base.Execution;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Users;

namespace Domain.Users.Validators
{
    public class RegisterUserDtoValidator : IValidateDto<RegisterUserDto>
    {
        public void Validate(RegisterUserDto dto, ExecutionResult result)
        {
            new UserNameUniqueValidation().Validate(dto.UserName, result);
        }
    }
}