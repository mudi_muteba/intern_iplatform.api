using AutoMapper;
using Domain.Admin;
using Domain.Base.Execution;
using ValidationMessages.Settings;

namespace Domain.Users.Validators
{
    public class PasswordStrengthValidator
    {
        public void ValidatePasswordStrength(int channelId, string password, ExecutionResult result)
        {
            var setting = Mapper.Map<int, Channel>(channelId);
            if (setting == null) return;

            if (!setting.PasswordStrengthEnabled) return;

            if (!setting.IsStrongPassword(password))
                result.AddValidationMessage(SettingValidationMessages.InvalidPasswordStrength);
        }
    }
}