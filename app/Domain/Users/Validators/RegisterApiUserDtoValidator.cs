﻿using Domain.Base.Execution;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Users;

namespace Domain.Users.Validators
{
    public class RegisterApiUserDtoValidator : IValidateDto<RegisterApiUserDto>
    {
        public void Validate(RegisterApiUserDto dto, ExecutionResult result)
        {
            new UserNameUniqueValidation().Validate(dto.UserName, result);
            new PasswordStrengthValidator().ValidatePasswordStrength(dto.ChannelId, dto.Password, result);
        }
    }
}