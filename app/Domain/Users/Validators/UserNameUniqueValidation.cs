﻿using System;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Users.Queries;
using Microsoft.Practices.ServiceLocation;
using ValidationMessages.Users;

namespace Domain.Users.Validators
{
    public class UserNameUniqueValidation
    {
        private readonly FindUserByUserNameQuery _query;

        public UserNameUniqueValidation()
        {
            _query = new FindUserByUserNameQuery(ServiceLocator.Current.GetInstance<IProvideContext>(), ServiceLocator.Current.GetInstance<IRepository>());
        }

        public void Validate(string userName, ExecutionResult result)
        {
            var match = GetUserByUserName(userName);
            if (match != null)
                result.AddValidationMessage(UserValidationMessages.DuplicateUserName(userName));
        }

        private User GetUserByUserName(string userName)
        {
            var match = _query
                .WithUserName(userName)
                .ExecuteQuery()
                .FirstOrDefault();

            return match;
        }
    }
}