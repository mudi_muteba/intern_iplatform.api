﻿using System.Linq;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Users;
using ValidationMessages.Users;

namespace Domain.Users.Validators
{
    public class RequestPasswordResetDtoValidator : IValidateDto<RequestPasswordResetDto>
    {
        private readonly FindUserByUserNameQuery _query;

        public RequestPasswordResetDtoValidator(IProvideContext contextProvider, IRepository repository)
        {
            _query = new FindUserByUserNameQuery(contextProvider, repository);
        }

        public void Validate(RequestPasswordResetDto dto, ExecutionResult result)
        {
            var user = _query.WithUserName(dto.UserName).ExecuteQuery().FirstOrDefault();
            if (user == null)
                result.AddValidationMessage(UserValidationMessages.UserNotFound);
        }
    }
}