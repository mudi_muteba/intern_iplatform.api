﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Users;

namespace Domain.Users.Validators
{
    public class CreateUserDtoValidator : IValidateDto<CreateUserDto>
    {
        public void Validate(CreateUserDto dto, ExecutionResult result)
        {
            new UserNameUniqueValidation().Validate(dto.UserName, result);
            new PasswordStrengthValidator().ValidatePasswordStrength(dto.Channels.FirstOrDefault(), dto.Password, result);
        }
    }
}