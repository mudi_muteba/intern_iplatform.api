﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Users;
using ValidationMessages.Users;

namespace Domain.Users.Validators
{
    public class ResetPasswordDtoValidator : IValidateDto<ResetPasswordDto>
    {
        private readonly FindUserByPasswordResetTokenQuery _query;

        public ResetPasswordDtoValidator(IProvideContext contextProvider, IRepository repository)
        {
            _query = new FindUserByPasswordResetTokenQuery(contextProvider, repository);
        }

        public void Validate(ResetPasswordDto dto, ExecutionResult result)
        {
            var user = _query
                .WithPasswordResetToken(dto.PasswordResetToken)
                .WithToken(dto.Context.Token)
                .ExecuteQuery().FirstOrDefault();

            if (user == null)
            {
                result.AddValidationMessage(UserValidationMessages.UserNotFound);
                return;
            }

            if (!user.IsValidGuidToken(dto.PasswordResetToken))
                result.AddValidationMessage(UserValidationMessages.InvalidPasswordResetToken);

            new PasswordStrengthValidator().ValidatePasswordStrength(user.DefaultChannelId, dto.Password, result);
        }
    }
}