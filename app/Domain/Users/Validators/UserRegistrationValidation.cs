﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Users;

namespace Domain.Users.Validators
{
    public class UserRegistrationValidation : IValidateDto<ResetPasswordDto>
    {
        private readonly IRepository _repository;

        public UserRegistrationValidation(IRepository repository)
        {
            _repository = repository;
        }

        public void Validate(ResetPasswordDto dto, ExecutionResult result)
        {
        }
    }
}