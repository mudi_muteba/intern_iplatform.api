﻿using System;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Extentions;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Users.Events;
using iPlatform.Api.DTOs.Audit;
using iPlatform.Api.DTOs.Individual;
using Workflow.Messages;

namespace Domain.Users.EventHandlers
{
    public class UserRegisteredEventHandler : BaseEventHandler<UserRegisteredEvent>
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan _executionPlan;
        private readonly IWorkflowRouter _router;
        private readonly IProvideContext _contextProvider;

        public UserRegisteredEventHandler(IRepository repository, IExecutionPlan executionPlan, IWorkflowRouter router, IProvideContext contextProvider)
        {
            _repository = repository;
            _executionPlan = executionPlan;
            _router = router;
            _contextProvider = contextProvider;
        }

        public override void Handle(UserRegisteredEvent @event)
        {
            var result = _executionPlan.Execute<CreateIndividualDto, int>(@event.IndividualDto);
            var individual = _repository.GetById<Individual>(result.Response);

            if (@event.User != null) 
                @event.User.SetIndividual(individual);

            var userId = @event.User != null ? @event.User.Id : 0;
            _router.AddAuditLogEntry(new CreateAuditLogDto(_contextProvider.Get(), "UserRegistered", "User registered", typeof(User).FullName, userId, DateTime.UtcNow));
        }
    }
}