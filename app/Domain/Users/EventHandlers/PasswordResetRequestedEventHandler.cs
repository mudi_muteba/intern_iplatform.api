﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Extentions;
using Domain.Emailing;
using Domain.Users.Events;
using iPlatform.Api.DTOs.Audit;
using Shared.Extentions;
using Workflow.Messages;

namespace Domain.Users.EventHandlers
{
    public class PasswordResetRequestedEventHandler : BaseEventHandler<PasswordResetRequestedEvent>
    {
        private readonly IWorkflowRouter _router;
        private readonly IProvideContext _contextProvider;

        public PasswordResetRequestedEventHandler(IWorkflowRouter router, IProvideContext contextProvider)
        {
            _router = router;
            _contextProvider = contextProvider;
        }

        public override void Handle(PasswordResetRequestedEvent @event)
        {
            if (string.IsNullOrEmpty(@event.UserName))
            {
                this.Error(() => "UserName not found required for password reset request email");
                return;
            }
            if (!@event.PasswordResetToken.HasValue)
            {
                this.Error(() => "PasswordResetToken not found required for password reset request email");
                return;
            }

            var message = new Dictionary<string, string>
            {
                {"UserName", @event.UserName},
                {"ResetUrl", ConfigurationManager.AppSettings["iPlatform/web/resetPasswordUrl"] + @event.PasswordResetToken}
            };
            var communicationMessageData = new CommunicationMessageData(message);
            var emailCommunicationMessage = new EmailCommunicationMessage(@"Emailing\PasswordResetRequestTemplate", communicationMessageData, @event.UserName, "Password reset", @event.AppSettingsEmailSetting);

            _router.Publish(new WorkflowRoutingMessage().AddMessage(new EmailWorkflowExecutionMessage(emailCommunicationMessage)));

            var auditLogDto = new CreateAuditLogDto(_contextProvider.Get(), "PasswordResetRequested", "Password reset requested", typeof(User).FullName, @event.Audit.User.UserId, DateTime.UtcNow);
            _router.AddAuditLogEntry(auditLogDto);
        }
    }
}