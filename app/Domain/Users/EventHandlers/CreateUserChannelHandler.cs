﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;

namespace Domain.Users.EventHandlers
{
    public class CreateUserChannelHandler : CreationDtoHandler<UserChannel, CreateUserChannelDto, int>
    {
        private IRepository repository;
        public CreateUserChannelHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void EntitySaved(UserChannel entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override UserChannel HandleCreation(CreateUserChannelDto dto, HandlerResult<int> result)
        {
            var entity = UserChannel.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}