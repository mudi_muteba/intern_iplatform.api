﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Users.Workflow.Messages
{
    public class ApproveUserWorkflowExecutionMessage : WorkflowExecutionMessage
    {
        public ApproveUserWorkflowExecutionMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
        }

        public ApproveUserWorkflowExecutionMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType)
        {
        }
    }
}