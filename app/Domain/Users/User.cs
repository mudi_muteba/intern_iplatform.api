﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.Logging;
using Domain.Admin;
using Domain.AuditLogs.Events;
using Domain.Base;
using Domain.Base.Encryption;
using Domain.Base.Events;
using Domain.Campaigns;
using Domain.Individuals;
using Domain.Products;
using Domain.Users.Authorisation;
using Domain.Users.Discounts;
using Domain.Users.Events;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.ContactDetail;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;
using iPlatform.Api.DTOs.Users.Discount;
using iPlatform.Enums;
using Infrastructure.NHibernate.Attributes;
using MasterData;
using MasterData.Authorisation;
using Domain.Emailing.Factory;
using MoreLinq;

namespace Domain.Users
{
    public class User : EntityWithAudit
    {
        private static readonly ILog Log = LogManager.GetLogger<User>();
        private ISet<UserIndividual> _userIndividuals = new HashSet<UserIndividual>();
        private UserLogin login;
        private UserStatus status;
        private UserTokens tokens;

        public User(string userName, string password, List<Channel> channels)
        {
            Channels = Channels ?? new List<UserChannel>();
            foreach (var channel in channels)
            {
                Channels.Add(new UserChannel(this, channel));
            }
            UserName = userName;
            Password = PasswordHash.CreateHash(password);
            AuthorisationGroups = new List<UserAuthorisationGroup>();
        }

        public virtual Guid SystemId { get; protected internal set; }
        public virtual string UserName { get; protected internal set; }
        public virtual string Password { get; protected internal set; }
        public virtual IList<UserChannel> Channels { get; protected internal set; }
        public virtual IList<UserDiscountCoverDefinition> Discounts { get; protected internal set; }
        public virtual string ExternalReference { get; set; }
        public virtual bool IsBroker { get; set; }
        public virtual bool IsAccountExecutive { get; set; }
        public virtual bool IsBillable { get; set; }

        public virtual ISet<UserIndividual> UserIndividuals
        {
            get { return _userIndividuals; }
            protected internal set { _userIndividuals = value; }
        }

        [DoNotMap]
        public virtual IEnumerable<Individual> Individuals
        {
            get
            {
                return UserIndividuals != null
                    ? UserIndividuals.Where(x => x.IsDeleted != true).Select(x => x.Individual)
                    : Enumerable.Empty<Individual>();
            }
        }

        [DoNotMap]
        public virtual string ChannelIdsString
        {
            get
            {
                var channels = Channels != null
                    ? Channels.Where(x => x.IsDeleted != true).Select(x => x.Channel).ToList()
                    : Enumerable.Empty<Channel>().ToList();

                return string.Join(",", channels.Select(x => x.Id).ToList());
            }
        }

        [DoNotMap]
        public virtual IEnumerable<int> ChannelIds
        {
            get
            {
                var channels = Channels != null
                    ? Channels.Where(x => x.IsDeleted != true).Select(x => x.Channel).ToList()
                    : Enumerable.Empty<Channel>().ToList();

                return channels.Select(x => x.Id);
            }
        }

        [DoNotMap]
        public virtual Channel DefaultChannel
        {
            get
            {
                var userChannel = Channels.FirstOrDefault(x => x.IsDefault);
                if (userChannel != null)
                {
                    var channel = userChannel.Channel;
                    return channel;
                }
                return null;
            }
        }

        [DoNotMap]
        public virtual int DefaultChannelId
        {
            get
            {
                return DefaultChannel != null ? DefaultChannel.Id : 0;
            }
        }

        public virtual IList<UserAuthorisationGroup> AuthorisationGroups { get; protected internal set; }

        public virtual UserLogin Login
        {
            get { return login ?? (login = new UserLogin()); }
            protected internal set { login = value; }
        }

        public virtual UserStatus Status
        {
            get { return status ?? (status = new UserStatus()); }
            protected internal set { status = value; }
        }

        public virtual UserTokens Tokens
        {
            get { return tokens ?? (tokens = new UserTokens()); }
            protected internal set { tokens = value; }
        }

        public User(int id)
        {
            Id = id;
        }

        public User()
        {
            Login = new UserLogin();
            Status = new UserStatus();
            Tokens = new UserTokens();
            AuthorisationGroups = new List<UserAuthorisationGroup>();
            Discounts = new List<UserDiscountCoverDefinition>();
        }

        public User(string userName, string password, List<int> channels, int? defaultChannelId = null)
        {
            Channels = Channels ?? new List<UserChannel>();
            foreach (var channel in channels)
            {
                var isDefault = defaultChannelId.HasValue && defaultChannelId == channel;
                Channels.Add(new UserChannel(this, new Channel(channel, isDefault)));
            }
            UserName = userName;
            Password = PasswordHash.CreateHash(password);
            AuthorisationGroups = new List<UserAuthorisationGroup>();
            Discounts = new List<UserDiscountCoverDefinition>();
        }

        public virtual bool ValidatePassword(string password)
        {
            return PasswordHash.ValidatePassword(password, Password);
        }

        public virtual AuthenticationResponse Authenticate(AuthenticationRequestDto request)
        {
            if (!ValidatePassword(request.Password))
            {
                Log.ErrorFormat("Failed to Validate Password for username '{0}'", request.Email);
                return new AuthenticationResponse(false);
            }

            Tokens.Login(this, request.RequestType, request.ActiveChannelId);

            return new AuthenticationResponse(true, Tokens.Token, this);
        }

        public virtual string GenerateToken(ApiRequestType apiRequestType, int activeChannelId)
        {
            Tokens.Login(this, apiRequestType, activeChannelId == 0 ? DefaultChannelId : activeChannelId);
            return Tokens.Token;
        }

        public static User Create(CreateUserDto dto, List<Campaign> campaigns)
        {
            var user = new User(dto.UserName, dto.Password, dto.Channels, dto.DefaultChannelId);
            user.ExternalReference = dto.ExternalReference;
            user.IsAccountExecutive = dto.IsAccountExecutive;
            user.IsBroker = dto.IsBroker;
            user.IsBillable = dto.IsBillable;

            user.AllocateToAuthorisationGroups(dto.Groups);
            if (dto.CreateIndividualDto != null)
            {
                user.SetIndividual(Individual.Create(dto.CreateIndividualDto, user.Channels.FirstOrDefault() != null ? user.Channels.FirstOrDefault().Channel : null, campaigns));
            }

            if (dto.IsApproved)
            {
                user.Approved(dto);
            }

            user.RaiseEvent(new UserCreatedEvent(dto.UserName, dto.CreateAudit(), user.Channels.Select(c => c.Channel)));

            return user;
        }

        public virtual void Edit(EditUserDto dto, List<Campaign> campaigns)
        {
            AllocateChannels(dto.Channels, dto.DefaultChannelId);
            AllocateToAuthorisationGroups(dto.Groups);

            ExternalReference = dto.ExternalReference;
            IsAccountExecutive = dto.IsAccountExecutive;
            IsBroker = dto.IsBroker;
            IsBillable = dto.IsBillable;

            if (dto.CreateIndividualDto != null)
            {
                SetIndividual(Individual.Create(dto.CreateIndividualDto, Channels.FirstOrDefault() != null ? Channels.FirstOrDefault().Channel : null, campaigns));
            }
            if (dto.EditIndividualDto != null)
            {
                var individual = Mapper.Map<int, Individual>(dto.EditIndividualDto.Id);
                dto.EditIndividualDto.SetContext(dto.Context);
                individual.Update(dto.EditIndividualDto);
            }

            RaiseEvent(new UserUpdatedEvent(Id, dto.CreateAudit(), Channels.Select(x => new Channel(x.Id))));
        }

        public virtual User Register(RegisterUserDto dto)
        {
            var user = new User(dto.UserName, "", new List<int> {dto.ChannelId});
            var channels = user.Channels != null
                ? user.Channels.Where(x => x.Channel != null).Select(x => x.Channel)
                : Enumerable.Empty<ChannelReference>();
            var individualDto = new CreateIndividualDto
            {
                ChannelId = channels.Any() ? channels.FirstOrDefault().Id : 0,
                FirstName = dto.Name,
                Surname = dto.Surname,
                IdentityNo = dto.IdNumber,
                ContactDetail = new CreateContactDetailDto {Cell = dto.PhoneNumber, Email = dto.UserName}
            };

            user.RaiseEvent(new UserRegisteredEvent(user, individualDto, dto.CreateAudit(), channels.Select(x => new Channel(x.Id))));

            return user;
        }

        public virtual User Register(RegisterApiUserDto dto)
        {
            var user = new User(dto.UserName, dto.Password, new List<int> {dto.ChannelId});

            user.AllocateToAuthorisationGroups(new List<UserAuthorisationGroupDto>()
            {
                new UserAuthorisationGroupDto(new List<int> {dto.ChannelId}, new List<AuthorisationGroup> { MasterData.AuthorisationGroups.CallCentreAgent })
            });

            var channels = (user.Channels != null
                ? user.Channels.Where(x => x.Channel != null).Select(x => x.Channel)
                : Enumerable.Empty<ChannelReference>()).ToList();

            var @event = new UserRegisteredEvent(user, null, new EventAudit(new EventAuditUser(0, dto.UserName)), channels.Select(x => new Channel(x.Id)));

            user.RaiseEvent(@event);

            return user;
        }

        public virtual void AllocateToAuthorisationGroups(List<UserAuthorisationGroupDto> changedAuthorisationGroups)
        {
            var container = new UserAuthorisationGroupMaintainer(this);

            container.Allocate(changedAuthorisationGroups);
        }

        public virtual void AllocateDefaultChannel(int defaultChannelId)
        {
            if (defaultChannelId < 1)
                return;
            Channels.ForEach(a=>a.IsDefault = false);

            var channel = Channels.FirstOrDefault(a => a.Channel.Id == defaultChannelId);

            if (channel == null) 
            throw new ApplicationException(string.Format("Channel not found {0}",defaultChannelId));

            channel.IsDefault = true;
        }

        public virtual void Approved(CreateUserDto dto)
        {
            Status.Approved();
            RaiseEvent(new UserApprovedEvent(UserName, dto.CreateAudit(), Channels.Select(x => new Channel(x.Id))));
        }

        public virtual void Approved(ApproveUserDto dto)
        {
            Status.Approved();
            RaiseEvent(new UserApprovedEvent(UserName, dto.CreateAudit(), Channels.Select(x => new Channel(x.Id))));
        }

        public virtual void RequestPasswordReset(RequestPasswordResetDto dto)
        {
            Tokens.RequestPasswordReset();
            var channel = this.Channels.FirstOrDefault().Channel;
            if(channel.EmailCommunicationSettings != null && channel.EmailCommunicationSettings.Any())
            {
                var appsetting = Mapper.Map<AppSettingsEmailSetting>(channel.EmailCommunicationSettings.FirstOrDefault());
                RaiseEvent(new PasswordResetRequestedEvent(UserName, Tokens.PasswordResetToken, new EventAudit(new EventAuditUser(Id, UserName)), Channels.Select(x => new Channel(x.Id)), appsetting));
            }
        }

        public virtual void ResetPassword(ResetPasswordDto dto)
        {
            Password = PasswordHash.CreateHash(dto.Password);
            Tokens.ResetPasswordToken();
            Status.IsActive = true;
            RaiseEvent(new AuditLogEvent(dto.Context, "PasswordReset", "Password reset successful", GetType().FullName, Id, DateTime.UtcNow));
        }

        public virtual bool IsValidGuidToken(Guid token)
        {
            return Tokens.IsValidGuidToken(token);
        }

        public virtual void Disable(DisableUserDto dto)
        {
            Status.Disable();

            RaiseEvent(new UserDisabledEvent(UserName, dto.CreateAudit(), Channels.Select(x => new Channel(x.Id))));
        }

        public virtual SystemAuthorisation GetEffectiveAuthorisation()
        {
            var calculator = new EffectiveAuthorisationCalculator();
            var authorisation = calculator.Calculate(this);

            return authorisation;
        }

        private void AllocateChannels(List<int> changedChannels, int defaultChannelId)
        {
            var maintainer = new ChannelMaintainer(this);

            maintainer.MergeChanges(changedChannels, defaultChannelId);

        }

        public virtual void SetIndividual(Individual individual)
        {
            if (individual == null) return;
            var userIndividual = _userIndividuals.FirstOrDefault(x => Equals(x.Individual, this));
            if (userIndividual == null)
            {
                userIndividual = new UserIndividual(this, individual);
                _userIndividuals.Add(userIndividual);
            }
            else
                userIndividual.Individual = individual;
        }

        public virtual string GetFullname()
        {
            if(!this.Individuals.Any())
                return string.Empty;

            var individual = this.Individuals.FirstOrDefault();

            if(individual.Title == null)
                return string.Format("{0}, {1}", individual.Surname, individual.FirstName);

            return string.Format("{0}. {1}, {2}", individual.Title.Name, individual.Surname, individual.FirstName);
        }


        public virtual UserDiscountCoverDefinition AddDiscount(CreateDiscountDto dto, CoverDefinition coverDefinition)
        {
            var discount = new UserDiscountCoverDefinition
            {
                User = this,
                CoverDefinition = coverDefinition,
                Channel = new Channel(dto.ChannelId),
                Discount = dto.Discount,
                Load = dto.Load,
                ItemMaxDiscount = dto.ItemMaxDiscount,
                QuoteMaxDiscount = dto.QuoteMaxDiscount
            };
            Discounts.Add(discount);
            return discount;
        }

        protected bool Equals(User other)
        {
            return Equals(login, other.login) && Equals(status, other.status) && Equals(tokens, other.tokens) &&
                   Equals(_userIndividuals, other._userIndividuals) && string.Equals(UserName, other.UserName) &&
                   string.Equals(Password, other.Password) && Equals(Channels, other.Channels) &&
                   Equals(AuthorisationGroups, other.AuthorisationGroups);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((User) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (login != null ? login.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (status != null ? status.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (tokens != null ? tokens.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_userIndividuals != null ? _userIndividuals.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (UserName != null ? UserName.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Password != null ? Password.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Channels != null ? Channels.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (AuthorisationGroups != null ? AuthorisationGroups.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}