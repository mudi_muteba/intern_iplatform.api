﻿using Domain.Base;

namespace Domain.Users
{
    public class UserReference : EntityWithAudit
    {
        protected UserReference()
        {
        }

        public UserReference(int userId)
        {
            Id = userId;
        }

        public virtual string UserName { get; protected internal set; }
    }
}