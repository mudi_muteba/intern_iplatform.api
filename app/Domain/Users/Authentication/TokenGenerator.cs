﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Execution.Exceptions;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Enums;
using Infrastructure.Configuration;
using JWT;
using Newtonsoft.Json;
using Shared;

namespace Domain.Users.Authentication
{
    public class TokenGenerator
    {
        private const string _activeChannelId = "activeChannelId";
        private const string _requestType = "requestType";
        private const string _id = "id";
        private const string _timestamp = "timestamp";
        private const string _culture = "culture";
        private const string _activeChannelSystemId = "activeChannelSystemId";

        public string CreateToken(User user, ApiRequestType apiRequestType, int activeChannel)
        {
            var userChannel = user.Channels.FirstOrDefault(x => x.Channel.Id == activeChannel);
            var channel = userChannel != null ? userChannel.Channel : user.DefaultChannel;

            Mapper.CreateMap<Channel, CultureParameter>()
                .ForMember(t => t.DateFormat, o => o.MapFrom(s => s.DateFormat == null ? string.Empty : s.DateFormat))
                .ForMember(t => t.LanguageId, o => o.MapFrom(s => s.Language != null ? s.Language.Id : 2))
                .ForMember(t => t.CurrencyId, o => o.MapFrom(s => s.Currency != null ? s.Currency.Id : 1))
                .ForMember(t => t.CountryId, o => o.MapFrom(s => s.Country != null ? s.Country.Id : 197))
                ;

            var cultureParam = Mapper.Map<CultureParameter>(channel);

            var payload = new Dictionary<string, object>
            {
                {_id, user.Id},
                {_timestamp, SystemTime.Now()},
                {_activeChannelId, activeChannel},
                {_requestType, apiRequestType.ToString()},
                {_culture, JsonConvert.SerializeObject(cultureParam)},
                {_activeChannelSystemId, channel == null ? Guid.Empty.ToString(): channel.SystemId.ToString()}
            };

            return JsonWebToken.Encode(payload, ConfigurationReader.Security.JWTSecretKey, JwtHashAlgorithm.HS256);
        }

        public class TokenReader
        {
            private readonly IDictionary<string, object> token;

            public TokenReader(string token)
            {
                var secretKey = ConfigurationReader.Security.JWTSecretKey;
                var payload = JsonWebToken.DecodeToObject(token, secretKey) as IDictionary<string, object>;

                if (payload == null)
                    throw new TokenValidationException();

                if (!payload.Any())
                    throw new TokenValidationException();

                this.token = payload;
            }

            public int ActiveChannelId
            {
                get
                {
                    int apiActiveChannelId;
                    object activeChannelId = token.TryGetValue(_activeChannelId, out activeChannelId) ? activeChannelId : string.Empty;
                    return int.TryParse(activeChannelId + "", out apiActiveChannelId) ? apiActiveChannelId : 0;

                }
            }

            public Guid ActiveChannelSystemId
            {
                get
                {
                    Guid apiActiveChannelSystemId;
                    object activeChannelSystemId = token.TryGetValue(_activeChannelSystemId, out activeChannelSystemId) ? activeChannelSystemId : string.Empty;
                    return Guid.TryParse(activeChannelSystemId + "", out apiActiveChannelSystemId) ? apiActiveChannelSystemId : Guid.Empty;
                }
            }

            public ApiRequestType RequestType
            {
                get
                {
                    ApiRequestType apiRequestType;
                    object requestType = token.TryGetValue(_requestType, out requestType) ? requestType : string.Empty;
                    return Enum.TryParse(requestType + "", out apiRequestType) ? apiRequestType : ApiRequestType.Api;
                }
            }

            public int Id
            {
                get
                {
                    int apiId;
                    object id = token.TryGetValue(_id, out id) ? id : string.Empty;
                    return int.TryParse(id + "", out apiId) ? apiId : 0;
                }
            }

            public CultureParameter Culture
            {
                get
                {
                    object culture = token.TryGetValue(_culture, out culture) ? culture : string.Empty;
                    if(string.IsNullOrEmpty(culture.ToString()))
                    {
                        return new CultureParameter();
                    }
                    else
                    {
                        return JsonConvert.DeserializeObject<CultureParameter>(culture.ToString());
                    }
                }
            }
        }
    }
}