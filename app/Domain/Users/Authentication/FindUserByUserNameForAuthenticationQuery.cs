﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Users.Queries;
using MasterData.Authorisation;

namespace Domain.Users.Authentication
{
    public class FindUserByUserNameForAuthenticationQuery : BaseQuery<User>, IContextFreeQuery
    {
        private string _username;

        public FindUserByUserNameForAuthenticationQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new DefaultUserQueryFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public FindUserByUserNameForAuthenticationQuery WithUserName(string username)
        {
            _username = username;
            return this;
        }

        protected internal override IQueryable<User> Execute(IQueryable<User> query)
        {
            return query.Where(u => u.UserName == _username);
        }
    }
}