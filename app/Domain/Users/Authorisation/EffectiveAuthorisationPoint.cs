﻿using MasterData.Authorisation;

namespace Domain.Users.Authorisation
{
    public class EffectiveAuthorisationPoint
    {
        public RequiredAuthorisationPoint Point { get; set; }
        public EffectiveAuthorisationReason Reason { get; set; }
        public override string ToString()
        {
            return Point.ToString();
        }
    }
}