using System.Collections.Generic;
using Domain.Admin;

namespace Domain.Users.Authorisation
{
    public class ChannelEffectiveAuthorisationPoint
    {
        public ChannelEffectiveAuthorisationPoint()
        {
            Points = new List<EffectiveAuthorisationPoint>();
        }

        public ChannelReference Channel { get; set; }
        public List<EffectiveAuthorisationPoint> Points { get; set; }
    }
}