﻿using System.Collections.Generic;
using System.Linq;
using MasterData.Authorisation;
using MasterData.Authorisation.Groups;

namespace Domain.Users.Authorisation
{
    internal class GroupAuthorisationCalculator
    {
        public List<ChannelEffectiveAuthorisationPoint> Calculate(User user)
        {
            return Calculate(user.AuthorisationGroups.ToList());
        }

        public List<ChannelEffectiveAuthorisationPoint> Calculate(List<UserAuthorisationGroup> groups)
        {
            var authorisation = new List<ChannelEffectiveAuthorisationPoint>();
            var allAuthorisationGroups = new AllAuthorisationGroups();

            foreach (var @group in groups.Where(x => x.AuthorisationGroup != null))
            {
                var channel = authorisation.FirstOrDefault(a => a.Channel.Id == @group.Channel.Id);

                if (channel == null)
                {
                    channel = new ChannelEffectiveAuthorisationPoint()
                    {
                        Channel = @group.Channel,
                        Points = new List<EffectiveAuthorisationPoint>()
                    };
                    authorisation.Add(channel);
                }

                var match = allAuthorisationGroups.Find(@group.AuthorisationGroup);

                foreach (var point in match.Authorisation)
                {
                    if (!channel.Points.Any(p => p.Point.Key.Equals(point.Key)))
                    {
                        channel.Points.Add(new EffectiveAuthorisationPoint()
                        {
                            Point = point,
                            Reason = EffectiveAuthorisationReason.Group
                        });
                    }
                }
            }

            return authorisation;
        }
    }
}