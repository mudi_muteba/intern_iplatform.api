﻿using System.Collections.Generic;
using System.Linq;
using MasterData.Authorisation;

namespace Domain.Users.Authorisation
{
    public class EffectiveAuthorisationCalculator
    {
        public SystemAuthorisation Calculate(User user)
        {
            return Calculate(user.AuthorisationGroups.Where(ag => ag.IsDeleted != true).ToList());
        }

        private SystemAuthorisation Convert(List<ChannelEffectiveAuthorisationPoint> calculated)
        {
            var system = new SystemAuthorisation();

            foreach (var effectiveAuthorisationPoint in calculated)
            {
                var channelAuthorisation = new ChannelAuthorisation(effectiveAuthorisationPoint.Channel.Id);

                foreach (var point in effectiveAuthorisationPoint.Points)
                {
                    channelAuthorisation.Points.Add(point.Point);
                }

                system.Channels.Add(channelAuthorisation);
            }

            return system;
        }

        public SystemAuthorisation Calculate(List<UserAuthorisationGroup> groups)
        {
            var authorisation = new List<ChannelEffectiveAuthorisationPoint>();

            authorisation.AddRange(new GroupAuthorisationCalculator().Calculate(groups));

            return Convert(authorisation);
        }
    }
}