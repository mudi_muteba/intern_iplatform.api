using System;
using Domain.Users.Authentication;
using iPlatform.Enums;
using Infrastructure.NHibernate.Attributes;

namespace Domain.Users
{
    public class UserTokens
    {
        public UserTokens(string token, Guid passwordResetToken)
        {
            Token = token;
            PasswordResetToken = passwordResetToken;
        }

        protected internal UserTokens() { }

        [DoNotMap]
        public virtual string Token { get; protected internal set; }
        public virtual Guid? PasswordResetToken { get; protected internal set; }
        [DoNotMap]
        public virtual Guid RegistrationToken { get; protected internal set; }

        public virtual void Login(User user, ApiRequestType apiRequestType, int activeChannelId)
        {
            Token = new TokenGenerator().CreateToken(user, apiRequestType, activeChannelId);
            ResetPasswordToken();
        }

        public virtual void RequestPasswordReset()
        {
            PasswordResetToken = Guid.NewGuid();
        }

        public virtual void ResetPasswordToken()
        {
            PasswordResetToken = null;
        }

        public virtual void IssueRegistrationToken()
        {
            RegistrationToken = Guid.NewGuid();
        }

        public virtual void ResetRegistrationToken()
        {
            RegistrationToken = Guid.Empty;
        }

        public virtual bool IsValidGuidToken(Guid token)
        {
            return token != new Guid() && Equals(PasswordResetToken, token);
        }

        protected bool Equals(UserTokens other)
        {
            return string.Equals(Token, other.Token) && Equals(PasswordResetToken, other.PasswordResetToken);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((UserTokens) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Token != null ? Token.GetHashCode() : 0)*397) ^
                       (PasswordResetToken != null ? PasswordResetToken.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return string.Format("{0} - {1} - {2}", GetType(), Token, PasswordResetToken);
        }
    }
}