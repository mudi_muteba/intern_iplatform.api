﻿using System;
using Domain.Admin;
using Domain.Base;
using Domain.Individuals;
using Infrastructure.NHibernate.Attributes;
using Shared;

namespace Domain.Users
{
    public class UserIndividual : Entity
    {
        public virtual User User { get; protected internal set; }
        public virtual Individual Individual { get; protected internal set; }

        public virtual DateTime CreatedAt
        {
            get; protected internal set;
        }

        public virtual DateTime ModifiedAt
        {
            get; protected internal set;
        }

        [DoNotMap]
        public virtual bool IsDefault { get; protected internal set; }

        protected UserIndividual()
        {
            CreatedAt = SystemTime.Now();
            ModifiedAt = SystemTime.Now();
        }

        public UserIndividual(User user, Individual individual)
        {
            User = user;
            Individual = individual;
            CreatedAt = SystemTime.Now();
            ModifiedAt = SystemTime.Now();
        }

        public override string ToString()
        {
            return string.Format("Individual {0}; User Id {1} (IsDeleted: {2})", Individual.Id, User.Id, IsDeleted);
        }
    }
}