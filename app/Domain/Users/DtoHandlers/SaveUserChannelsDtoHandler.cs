﻿using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using MasterData.Authorisation;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using Infrastructure.Configuration;

namespace Domain.Users.DtoHandlers
{
    public class SaveUserChannelsDtoHandler : BaseDtoHandler<SaveUserChannelsDto, bool>
    {
        private IRepository m_Respository;
        private IExecutionPlan m_Executionplan;
        public SaveUserChannelsDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionplan) : base(contextProvider)
        {
            m_Respository = repository;
            m_Executionplan = executionplan;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(SaveUserChannelsDto dto, HandlerResult<bool> result)
        {
            List<int> UserIds = new List<int>();
            var datetime = DateTime.UtcNow;
            for (int i = 0; i < dto.Users.Count(); i++)
            {
                var success = false;
                var userdto = dto.Users[i];
                var user = m_Respository.GetAll<User>().FirstOrDefault(x => x.UserName == userdto.UserName);

                if (user != null)
                {
                    UserIds.Add(user.Id);
                    success = true;
                }
                else
                {
                    userdto.SetContext(dto.Context);
                    userdto.CreateIndividualDto.SetContext(dto.Context);
                    var userResult = m_Executionplan.Execute<CreateUserDto, int>(userdto).Response;


                    if (userResult > 0)
                    {
                        if (userdto.SaveMultipleSettingsiRateUser != null && userdto.SaveMultipleSettingsiRateUser.SettingsiRateUser.Count > 0)
                        {
                            foreach (var item in userdto.SaveMultipleSettingsiRateUser.SettingsiRateUser)
                            {
                                item.UserId = userResult;
                            }

                            m_Executionplan.Execute<SaveMultipleSettingsiRateUserDto, bool>(userdto.SaveMultipleSettingsiRateUser);
                        }

                        success = true;
                        UserIds.Add(userResult);
                    }
                }

                if (success)
                {
                    var userchannels = m_Respository.GetAll<UserChannel>().Where(x => x.User.Id == UserIds[i]);
                    if (!userchannels.Any(x => x.Channel.Id == dto.ChannelId))
                    {
                        var isDefault = !userchannels.Any(x => x.IsDefault);
                        var createUserChannelDto = new CreateUserChannelDto { ChannelId = dto.ChannelId, UserId = UserIds[i], IsDefault = isDefault, CreatedAt = datetime, ModifiedAt = datetime };
                        m_Executionplan.Execute<CreateUserChannelDto, int>(createUserChannelDto);
                    }
                }
            }

            //delete from userchannel those not in the list
            RemoveOldUserChannel(UserIds, dto.ChannelId);
            result.Processed(true);
        }

        private void RemoveOldUserChannel(List<int> userIds, int channelId)
        {
            var tobeDeletedUserChannels = m_Respository.GetAll<UserChannel>().Where(x => x.Channel.Id == channelId && !userIds.Contains(x.User.Id));

            var adminList = new AdminConfigurationReader().AdminList.Split(';');

            foreach (var userChannel in tobeDeletedUserChannels)
            {
                if (!adminList.Contains(userChannel.User.UserName))
                {
                    userChannel.Delete();
                    m_Respository.Save(userChannel);
                }
            }
        }

    }

}
