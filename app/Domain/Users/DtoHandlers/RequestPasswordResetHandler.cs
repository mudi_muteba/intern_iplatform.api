﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;

namespace Domain.Users.DtoHandlers
{
    public class RequestPasswordResetHandler : BaseDtoHandler<RequestPasswordResetDto, RequestPasswordResetResponseDto>
    {
        private readonly FindUserByUserNameQuery _query;
        private readonly IRepository _repository;
        public RequestPasswordResetHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _query = new FindUserByUserNameQuery(contextProvider, repository);
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(RequestPasswordResetDto dto, HandlerResult<RequestPasswordResetResponseDto> result)
        {
            var entity = _query
                .WithUserName(dto.UserName)
                .ExecuteQuery()
                .FirstOrDefault();

            if (entity == null) return;

            entity.RequestPasswordReset(dto);

            _repository.Save(entity);

            result.Processed(new RequestPasswordResetResponseDto
            {
                Id = entity.Id,
                ResetToken = entity.Tokens.PasswordResetToken.HasValue ? entity.Tokens.PasswordResetToken.Value : new Guid()
            });
        }
    }
}