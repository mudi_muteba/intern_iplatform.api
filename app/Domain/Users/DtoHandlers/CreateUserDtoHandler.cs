﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;

namespace Domain.Users.DtoHandlers
{
    public class CreateUserDtoHandler : CreationDtoHandler<User, CreateUserDto, int>
    {
        private readonly IProvideContext _contextProvider;
        private readonly IRepository _repository;

        public CreateUserDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _contextProvider = contextProvider;
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    //UserAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(User entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override User HandleCreation(CreateUserDto dto, HandlerResult<int> result)
        {
            var campaigns = dto.CreateIndividualDto.ResolveCampaigns(_contextProvider, _repository);

            var user = User.Create(dto, campaigns);
            return user;
        }
    }
}