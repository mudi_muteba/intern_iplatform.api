﻿using System.Linq;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Base;
using Domain.Campaigns;
using AutoMapper;

namespace Domain.Users.DtoHandlers
{
    public class GetCampaignUsersDtoHandler : BaseDtoHandler<GetCampaignUsersDto, ListResultDto<UserInfoDto>>
    {
        private readonly IRepository m_repository;
        public GetCampaignUsersDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            m_repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    UserAuthorisation.List
                };
            }
        }

        protected override void InternalHandle(GetCampaignUsersDto dto, HandlerResult<ListResultDto<UserInfoDto>> result)
        {
            //get List of channels from campaign list
            List<int> channels = GetChannelList(dto);

            //get channel users
            List<User> users = m_repository.GetAll<UserChannel>()
                .Where(x => channels.Contains(x.Channel.Id))
                .Select(s => s.User).ToList();

            List<UserInfoDto> usersDto = Mapper.Map<List<UserInfoDto>>(users);

            result.Processed(new ListResultDto<UserInfoDto> { Results = usersDto });
        }

        private List<int> GetChannelList(GetCampaignUsersDto dto)
        {
            List<int> channels = new List<int>();

            List<Campaign> campaign = null;

            if (dto.Campaigns.Any())
                campaign = m_repository.GetAll<Campaign>().Where(x => dto.Campaigns.Contains(x.Id)).ToList();
            else
            {
                campaign = m_repository.GetAll<Campaign>().Where(x => 
                x.Agents.Any(a => a.Party.Id == dto.PartyId) || 
                x.Managers.Any(m => m.User.Id == dto.UserId)).ToList();
            }

            campaign.ForEach(x => {
                if (!channels.Contains(x.Channel.Id))
                    channels.Add(x.Channel.Id);
            });

            return channels;
        }

    }
}