﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;

namespace Domain.Users.DtoHandlers
{
    public class RegisterUserHandler : CreationDtoHandler<User, RegisterUserDto, int>
    {
        public RegisterUserHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void EntitySaved(User entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override User HandleCreation(RegisterUserDto dto, HandlerResult<int> result)
        {
            return new User().Register(dto);
        }
    }
}