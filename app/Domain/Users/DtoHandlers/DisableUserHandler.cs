﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;

namespace Domain.Users.DtoHandlers
{
    public class DisableUserHandler : ExistingEntityDtoHandler<User, DisableUserDto, int>
    {
        public DisableUserHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    UserAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(User entity, DisableUserDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}