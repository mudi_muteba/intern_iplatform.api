﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;

namespace Domain.Users.DtoHandlers
{
    public class ApproveUserHandler : ExistingEntityDtoHandler<User, ApproveUserDto, int>
    {
        public ApproveUserHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    UserAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(User entity, ApproveUserDto dto, HandlerResult<int> result)
        {
            entity.Approved(dto);
            result.Processed(entity.Id);
        }
    }
}