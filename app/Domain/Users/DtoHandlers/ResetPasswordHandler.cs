﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;

namespace Domain.Users.DtoHandlers
{
    public class ResetPasswordHandler : BaseDtoHandler<ResetPasswordDto, int>
    {
        private readonly FindUserByPasswordResetTokenQuery _query;
         public ResetPasswordHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _query = new FindUserByPasswordResetTokenQuery(contextProvider, repository);
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(ResetPasswordDto dto, HandlerResult<int> result)
        {
            var entity = _query
                .WithPasswordResetToken(dto.PasswordResetToken)
                .WithToken(dto.Context.Token)
                .ExecuteQuery()
                .FirstOrDefault();

            if (entity == null) return;

            entity.ResetPassword(dto);

            result.Processed(entity.Id);
        }
    }
}