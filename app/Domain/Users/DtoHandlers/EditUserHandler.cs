using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;

namespace Domain.Users.DtoHandlers
{
    public class EditUserHandler : ExistingEntityDtoHandler<User, EditUserDto, int>
    {
        private readonly IProvideContext _contextProvider;
        private readonly IRepository _repository;

        public EditUserHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _contextProvider = contextProvider;
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    UserAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(User entity, EditUserDto dto, HandlerResult<int> result)
        {
            var campaigns = dto.EditIndividualDto.ResolveCampaigns(_contextProvider, _repository, dto.Channels);
            entity.Edit(dto, campaigns);
            _repository.Save(entity);
            result.Processed(entity.Id);
        }
    }
}