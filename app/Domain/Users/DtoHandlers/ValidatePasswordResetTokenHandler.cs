﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;
using ValidationMessages.Users;

namespace Domain.Users.DtoHandlers
{
    public class ValidatePasswordResetTokenHandler : BaseDtoHandler<ValidatePasswordResetTokenDto, ValidatePasswordResetTokenDto>
    {
        private readonly FindUserByPasswordResetTokenQuery _query;
        public ValidatePasswordResetTokenHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            _query = new FindUserByPasswordResetTokenQuery(contextProvider, repository);
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(ValidatePasswordResetTokenDto dto, HandlerResult<ValidatePasswordResetTokenDto> result)
        {
            var entity = _query
                .WithPasswordResetToken(dto.PasswordResetToken)
                .ExecuteQuery()
                .FirstOrDefault();

            if (entity == null)
            {
                result.Processed(new ValidatePasswordResetTokenDto { StatusCode = HttpStatusCode.BadRequest });
                result.AddValidationMessage(UserValidationMessages.UserNotFound);
                return;
            }

            if (!entity.IsValidGuidToken(dto.PasswordResetToken))
            {
                result.Processed(new ValidatePasswordResetTokenDto { StatusCode = HttpStatusCode.BadRequest });
                result.AddValidationMessage(UserValidationMessages.InvalidPasswordResetToken);
            }

            result.Processed(new ValidatePasswordResetTokenDto { StatusCode = HttpStatusCode.OK });
        }
    }
}