using System.Collections.Generic;
using System.Linq;
using Domain.Admin;

namespace Domain.Users
{
    internal class ChannelMaintainer
    {
        private readonly User user;

        public ChannelMaintainer(User user)
        {
            this.user = user;
        }

        public void MergeChanges(List<int> changes, int defaultChannelId)
        {
            foreach (var change in changes)
            {
                if (user.Channels.All(oc => oc.Channel.Id != change))
                {

                    user.Channels.Add(new UserChannel(user, new Channel(change)));
                }
            }

            foreach (var originalChannel in user.Channels)
            {
                var inChanges = changes.Any(c => c == originalChannel.Channel.Id);
                if (!inChanges)
                    originalChannel.Delete();
                else
                    originalChannel.IsDefault = originalChannel.Channel.Id == defaultChannelId;
            }
        }
    }
}