﻿using Domain.Admin;
using Domain.Base;
using MasterData;

namespace Domain.Users
{
    public class UserAuthorisationGroup : Entity
    {
        public virtual User User { get; protected internal set; }
        public virtual AuthorisationGroup AuthorisationGroup { get; protected internal set; }
        public virtual Channel Channel { get; protected internal set; }

        protected UserAuthorisationGroup()
        {
            IsDeleted = false;
        }

        public UserAuthorisationGroup(Channel channel, UserReference user, AuthorisationGroup group) : this()
        {
            Channel = channel;
            AuthorisationGroup = @group;
        }

        public UserAuthorisationGroup(Channel channel, User user, AuthorisationGroup group) : this()
        {
            Channel = channel;
            User = user;
            AuthorisationGroup = @group;
        }

        public override string ToString()
        {
            return string.Format("{0} / Group {1}", Channel, AuthorisationGroup.Name);
        }
    }
}