﻿using System;
using AutoMapper;
using Domain.Admin;
using Domain.Base;
using iPlatform.Api.DTOs.Users;
using Shared;

namespace Domain.Users
{
    public class UserChannel : Entity
    {
        public virtual User User { get; protected internal set; }
        public virtual Channel Channel { get; protected internal set; }

        public virtual DateTime CreatedAt
        {
            get; protected internal set;
        }

        public virtual DateTime ModifiedAt
        {
            get; protected internal set;
        }

        public virtual bool IsDefault { get; set; }

        protected UserChannel()
        {
            CreatedAt = SystemTime.Now();
            ModifiedAt = SystemTime.Now();
        }

        public UserChannel(User user, Channel channel)
        {
            User = user;
            Channel = channel;
            CreatedAt = SystemTime.Now();
            ModifiedAt = SystemTime.Now();
            IsDefault = channel.IsDefault;
        }

        public override string ToString()
        {
            return string.Format("Channel {0}; Id {1} (IsDeleted: {2})", Channel.Id, Id, IsDeleted);
        }

        public static UserChannel Create(CreateUserChannelDto dto)
        {
            var entity = Mapper.Map<UserChannel>(dto);

            return entity;
        }
    }
}