﻿using System;
using System.Linq;
using Shared.Extentions;

namespace Domain.Users.Extensions
{
    public static class UserExtensions
    {
        public static void ThrowOnChannelMismatch(this User user, string channelIds)
        {
            ThrowOnChannelMismatch(user, channelIds.Split(',').Select(int.Parse).ToArray());
        }

        public static void ThrowOnChannelMismatch(this User user, int channelId)
        {
            if (channelId == 0) return;
            ThrowOnChannelMismatch(user, new []{channelId});
        }

        public static void ThrowOnChannelMismatch(this User user, params int[] channelIds)
        {
            var hasChannels = user.ChannelIds.Any(x => channelIds.Contains(x));
            if (!hasChannels)
            {
                var exception = new Exception(string.Format("Report criteria channels are not allocated to User Id:{0} Channel Criteria: {1} User Channels:{2}", user.Id, channelIds, user.ChannelIdsString));
                typeof(UserExtensions).Error(() => exception);
                throw exception;
            }
        }
    }
}