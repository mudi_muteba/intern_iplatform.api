﻿using System.Linq;
using AutoMapper;
using Domain.Base;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;
using MasterData.Authorisation;
using NHibernate.Proxy;
using System.Collections.Generic;
using MasterData;
using Domain.Admin;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Users.Mappings
{
    public class UserMappings : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SystemAuthorisation, SystemAuthorisationDto>();
            Mapper.CreateMap<ChannelAuthorisation, ChannelAuthorisationDto>();
            Mapper.CreateMap<RequiredAuthorisationPoint, AuthorisationPointDto>();
            Mapper.CreateMap<PagedResults<User>, PagedResultDto<ListUserDto>>();
            Mapper.CreateMap<AuthenticationResponse, AuthenticateResponseDto>();

            Mapper.CreateMap<UserChannel, ChannelReferenceDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Channel.Id))
                .ForMember(t => t.Country, o=> o.MapFrom(s=>s.Channel.Country))
                ;

            Mapper.CreateMap<User, BasicUserDto>();
            Mapper.CreateMap<User, UserInfoDto>()
                .AfterMap((s,d) => {
                    d.UserId = s.Id;
                    UserIndividual userIndividual = s.UserIndividuals.FirstOrDefault();
                    if(userIndividual != null)
                    {
                        d.FirstName = userIndividual.Individual.FirstName;
                        d.LastName = userIndividual.Individual.Surname;
                        d.IndividualId = userIndividual.Individual.Id;
                    }
                });
            
            Mapper.CreateMap<User, ListUserDto>()
                .ForMember(t => t.LastLoggedInDate, o => o.MapFrom(s => s.Login.LastLoggedInDate))
                .ForMember(t => t.IsApproved, o => o.MapFrom(s => s.Status.IsApproved))
                .ForMember(t => t.IsLoggedIn, o => o.MapFrom(s => s.Status.IsLoggedIn))
                .ForMember(d => d.Discounts, o => o.MapFrom(s => s.Discounts.Where(d => d.Discount > 0))) //only return discounts that have a value
                .ForMember(t => t.Individual, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    if (!s.Individuals.Any()) return;

                    var inprox = s.Individuals.FirstOrDefault() as INHibernateProxy;
                    var individual = s.Individuals.FirstOrDefault();

                    if (inprox != null && !individual.IsDeleted)
                        individual = inprox.HibernateLazyInitializer.GetImplementation() as Individual;

                    if (individual != null && !individual.IsDeleted)
                        d.Individual = Mapper.Map<BasicIndividualDto>(individual);
                })
                .AfterMap((s, d) =>
                {
                    if (d.Individual != null)
                    {
                        if (s.Channels.Any())
                        {
                            d.Individual.UserChannel = Mapper.Map<List<UserChannelDto>>(s.Channels);
                        }
                    }

                })
                .AfterMap((s, d) =>
                {
                    foreach (var @group in s.AuthorisationGroups)
                    {
                        var destinationGroup = d.Groups.FirstOrDefault(t => t.ChannelIds.Contains(@group.Channel.Id));
                        if (destinationGroup == null)
                        {
                            destinationGroup = new UserAuthorisationGroupDto(new List<int> { @group.Channel.Id }, new List<AuthorisationGroup>());
                            d.Groups.Add(destinationGroup);
                        }

                        destinationGroup.Groups.Add(@group.AuthorisationGroup);
                    }
                });

            Mapper.CreateMap<List<User>, ListResultDto<ListUserDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;



            Mapper.CreateMap<User, UserDto>()
                .ConvertUsing<ITypeConverter<User, UserDto>>()
                ;

            Mapper.CreateMap<HandlerResult<ListResultDto<UserInfoDto>>, POSTResponseDto<ListResultDto<UserInfoDto>>>()
           .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
           ;
        }
    }


}