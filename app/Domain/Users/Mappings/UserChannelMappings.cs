﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Users;

namespace Domain.Users.Mappings
{
    public class UserChannelMappings : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateUserChannelDto, UserChannel>()
                .ForMember(t => t.User, o => o.MapFrom(s => Mapper.Map<int, User>(s.UserId) ))
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)));

            Mapper.CreateMap<List<UserChannel>, ListResultDto<ListUserChannelDto>>()
               .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<UserChannel, ListUserChannelDto>();

        }
    }
}