﻿using System.Collections.Generic;

using AutoMapper;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Users;

namespace Domain.Users.Mappings
{
    public class UserIndividualMappings : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UserIndividual, UserIndividualDto>();

            Mapper.CreateMap<List<UserIndividual>, List<UserIndividualDto>>();

            Mapper.CreateMap<List<UserIndividual>, ListResultDto<UserIndividualDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;
        }
    }
}