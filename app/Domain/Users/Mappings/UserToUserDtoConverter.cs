﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;
using MasterData;
using MasterData.Authorisation;
using Shared.Extentions;

namespace Domain.Users.Mappings
{
    public class UserToUserDtoConverter : TypeConverter<User, UserDto>
    {
        protected override UserDto ConvertCore(User source)
        {
            if (source == null)
                return null;

            var destination = new UserDto
            {
                Id = source.Id,
                UserName = source.UserName,
                IsLoggedIn = source.Status.IsLoggedIn,
                LastLoggedInDate =
                    source.Login.LastLoggedInDate == null
                        ? new DateTimeDto()
                        : new DateTimeDto(source.Login.LastLoggedInDate.Value),
                LoggedInFromIP = source.Login.LoggedInFromIP,
                LoggedInFromSystem = source.Login.LoggedInFromSystem,
                IsActive = source.Status.IsActive,
                IsApproved = source.Status.IsApproved,
                ExternalReference = source.ExternalReference,
                IsAccountExecutive = source.IsAccountExecutive,
                IsBroker = source.IsBroker,
                IsBillable = source.IsBillable
            };

            destination.SystemAuthorisation = Mapper.Map<SystemAuthorisation, SystemAuthorisationDto>(source.GetEffectiveAuthorisation());
            destination.Channels = Mapper.Map<List<ChannelReferenceDto>>(source.Channels.Where(c => c.IsDeleted != true));
            destination.Individuals = Mapper.Map<List<IndividualDto>>(source.Individuals);

            try
            {
                destination.DefaultChannelId = source.DefaultChannelId;
            }
            catch (Exception exception)
            {
                this.Error(() => "Unable to map as no default Channel was found. " + exception.InnerException);
                throw new AutoMapperMappingException();
            }

            BuildAuthorisationGroups(source, destination);

            return destination;
        }

        private void BuildAuthorisationGroups(User source, UserDto destination)
        {
            foreach (var @group in source.AuthorisationGroups)
            {
                var destinationGroup = destination.Groups.FirstOrDefault(t => t.ChannelIds.Contains(@group.Channel.Id));
                if (destinationGroup == null)
                {
                    destinationGroup = new UserAuthorisationGroupDto(new List<int> { @group.Channel.Id }, new List<AuthorisationGroup>());
                    destination.Groups.Add(destinationGroup);
                }

                destinationGroup.Groups.Add(@group.AuthorisationGroup);
            }
        }
    }
}