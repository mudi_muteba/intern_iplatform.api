﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Users.Discounts.Overrides
{
    public class UserDiscountCoverDefinitionOverride : IAutoMappingOverride<UserDiscountCoverDefinition>
    {
        public void Override(AutoMapping<UserDiscountCoverDefinition> mapping)
        {
            mapping.References(a => a.CoverDefinition).Cascade.SaveUpdate();
            mapping.References(a => a.User).Cascade.SaveUpdate();
        }
    }
}