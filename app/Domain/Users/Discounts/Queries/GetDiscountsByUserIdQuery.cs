﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Users.Discounts.Queries
{
    public class GetDiscountsByUserIdQuery : BaseQuery<UserDiscountCoverDefinition>
    {
        private int _UserId;
        private int _ChannelId;

        public GetDiscountsByUserIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<UserDiscountCoverDefinition>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    UserAuthorisation.List
                };
            }
        }

        public GetDiscountsByUserIdQuery WithUserIdAndChannelId(int userId, int channelId)
        {
            _UserId = userId;
            _ChannelId = channelId;
            return this;
        }

        protected internal override IQueryable<UserDiscountCoverDefinition> Execute(IQueryable<UserDiscountCoverDefinition> query)
        {
            return Repository.GetAll<UserDiscountCoverDefinition>()
                .Where(a => a.User.Id == _UserId &&
                    a.Channel.Id == _ChannelId);
        }
    }
}