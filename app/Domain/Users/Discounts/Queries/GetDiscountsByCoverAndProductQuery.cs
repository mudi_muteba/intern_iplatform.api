﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users.Discount;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Users.Discounts.Queries
{
    public class GetDiscountsByCoverAndProductQuery : BaseQuery<UserDiscountCoverDefinition>
    {
        private SearchDiscountDto _criteria;

        public GetDiscountsByCoverAndProductQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<UserDiscountCoverDefinition>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetDiscountsByCoverAndProductQuery WithCriteria(SearchDiscountDto criteria)
        {
            _criteria = criteria;
            return this;
        }

        protected internal override IQueryable<UserDiscountCoverDefinition> Execute(
            IQueryable<UserDiscountCoverDefinition> query)
        {
            var cover = new Covers().First(a => a.Id == _criteria.CoverId);
            return Repository.GetAll<UserDiscountCoverDefinition>()
                    .Where(a => a.User.Id == _criteria.UserId && a.CoverDefinition.Product.Id == _criteria.ProductId && a.CoverDefinition.Cover == cover);
        }
    }
}