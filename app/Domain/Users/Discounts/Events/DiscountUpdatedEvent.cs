﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Users.Discounts.Events
{
    public class DiscountUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public UserDiscountCoverDefinition Discount { get; private set; }
        public int UserId { get; private set; }

        public DiscountUpdatedEvent(UserDiscountCoverDefinition _Discount, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> { channelReferences })
        {
            Discount = _Discount;
        }
    }
}