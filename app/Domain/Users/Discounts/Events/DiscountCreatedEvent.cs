﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Users.Discounts.Events
{
    public class DiscountCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public UserDiscountCoverDefinition Discount { get; private set; }
        public int UserId { get; private set; }

        public DiscountCreatedEvent(UserDiscountCoverDefinition _Discount, EventAudit audit, ChannelReference channelReferences)
            : base(audit, new List<ChannelReference> { channelReferences })
        {
            Discount = _Discount;
        }
    }
}