﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Users.Discount;

namespace Domain.Users.Discounts.Mappings
{
    public class DiscountMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<List<ListDiscountDto>, ListResultDto<ListDiscountDto>>();

            Mapper.CreateMap<UserDiscountCoverDefinition, ListDiscountDto>();

            Mapper.CreateMap<UserDiscountCoverDefinition, DiscountDto>();
        }
    }
}