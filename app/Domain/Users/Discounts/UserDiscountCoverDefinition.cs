﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;
using Domain.Products;

namespace Domain.Users.Discounts
{
    public class UserDiscountCoverDefinition : ChannelEntity
    {
        public virtual User User { get; set; }

        public virtual CoverDefinition CoverDefinition { get; set; }

        public virtual decimal Discount { get; set; }

        public virtual decimal Load { get; set; }

        public virtual decimal QuoteMaxDiscount { get; set; }

        public virtual decimal ItemMaxDiscount { get; set; }
    }
}
