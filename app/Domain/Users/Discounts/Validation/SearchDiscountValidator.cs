﻿using System;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Users.Discount;
using ValidationMessages.Users.Discounts;

namespace Domain.Users.Discounts.Validation
{
    public class SearchDiscountValidator : IValidateDto<SearchDiscountDto>
    {
        private readonly IRepository _repository;

        public SearchDiscountValidator(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
        }

        public void Validate(SearchDiscountDto dto, ExecutionResult result)
        {
            result.AddValidationMessages(dto.Validate());
        }

    }
}