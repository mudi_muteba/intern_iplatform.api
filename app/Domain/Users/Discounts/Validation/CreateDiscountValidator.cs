﻿using System;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Users.Discount;
using ValidationMessages.Users.Discounts;

namespace Domain.Users.Discounts.Validation
{
    public class CreateDiscountValidator : IValidateDto<EditDiscountDto>, IValidateDto<CreateDiscountDto>
    {
        private readonly IRepository _repository;

        public CreateDiscountValidator(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
        }

        public void Validate(CreateDiscountDto dto, ExecutionResult result)
        {
            bool any = _repository.GetAll<UserDiscountCoverDefinition>()
                .Any(
                    a =>
                        a.CoverDefinition.Id == dto.CoverDefinitionId &&
                        a.User.Id == dto.UserId);

            if (any)
            {
                result.AddValidationMessage(DiscountValidationMessages.DiscountAllReadyExists.AddParameters(new[] { dto.UserId.ToString(),dto.CoverDefinitionId.ToString() }));
            }

        }

        public void Validate(EditDiscountDto dto, ExecutionResult result)
        {
           
        }
    }
}