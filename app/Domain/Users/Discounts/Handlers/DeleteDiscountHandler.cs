﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users.Discount;
using MasterData.Authorisation;

namespace Domain.Users.Discounts.Handlers
{
    public class DeleteDiscountHandler : ExistingEntityDtoHandler<UserDiscountCoverDefinition, DeleteDiscountDto, int>
    {
        public DeleteDiscountHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    UserAuthorisation.Delete
                };
            }
        }

        protected override void HandleExistingEntityChange(UserDiscountCoverDefinition entity, DeleteDiscountDto dto, HandlerResult<int> result)
        {
            entity.IsDeleted = true;
            result.Processed(entity.Id);
        }
    }
}