﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users.Discount;
using MasterData.Authorisation;

namespace Domain.Users.Discounts.Handlers
{
    public class UpdateDiscountHandler : ExistingEntityDtoHandler<UserDiscountCoverDefinition, EditDiscountDto, int>
    {
        public UpdateDiscountHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    UserAuthorisation.Edit
                };
            }
        }

        protected override UserDiscountCoverDefinition GetEntityById(EditDiscountDto dto)
        {
            return repository.GetById<UserDiscountCoverDefinition>(dto.Id);
        }

        protected override void HandleExistingEntityChange(UserDiscountCoverDefinition entity, EditDiscountDto dto, HandlerResult<int> result)
        {
            entity.Discount = dto.Discount;
            entity.Load = dto.Load;
            entity.ItemMaxDiscount = dto.ItemMaxDiscount;
            entity.QuoteMaxDiscount = dto.QuoteMaxDiscount;

            result.Processed(entity.Id);
        }
    }
}