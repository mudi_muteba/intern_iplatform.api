﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Users.Discount;
using MasterData.Authorisation;
using ValidationMessages;
using ValidationMessages.Users.Discounts;

namespace Domain.Users.Discounts.Handlers
{
    public class CreateDiscountDtoHandler : CreationDtoHandler<UserDiscountCoverDefinition, CreateDiscountDto, int>
    {
        public CreateDiscountDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    UserAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(UserDiscountCoverDefinition entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override UserDiscountCoverDefinition HandleCreation(CreateDiscountDto dto, HandlerResult<int> result)
        {
            var user = _repository.GetById<User>(dto.UserId);
            var coverDefinition = _repository.GetById<CoverDefinition>(dto.CoverDefinitionId);

            if (coverDefinition == null)
                throw new MissingEntityException(typeof(CoverDefinition), dto.CoverDefinitionId);

            UserDiscountCoverDefinition discount = user.AddDiscount(dto, coverDefinition);
            return discount;
        }
    }
}