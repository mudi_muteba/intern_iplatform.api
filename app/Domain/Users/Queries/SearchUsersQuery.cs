using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Users.Search;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class SearchUsersQuery : BaseQuery<User>, IOrderableQuery
    {
        private UserSearchDto m_Criteria = new UserSearchDto();

        public SearchUsersQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultUserQueryFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    UserAuthorisation.List
                };
            }
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("UserName"), () => "UserName"},
                    {new OrderByField("IsLoggedIn"), () => "Status.IsLoggedIn"},
                    {new OrderByField("IsApproved"), () => "Status.IsApproved"},
                    {new OrderByField("LastLoggedInDate"), () => "Login.LastLoggedInDate"},
                };
            }
        }

        protected internal override IQueryable<User> Execute(IQueryable<User> query)
        {
            query = AddFilters(query);
            query = AddOrdering(query);

            return query;
        }

        private IQueryable<User> AddOrdering(IQueryable<User> query)
        {
            query = new OrderByBuilder<User>(OrderByDefinition).Build(query, m_Criteria.OrderBy);

            return query;
        }

        private IQueryable<User> AddFilters(IQueryable<User> query)
        {
            if (!string.IsNullOrWhiteSpace(m_Criteria.ChannelName))
            {
                query = query.Where(x => x.Channels.Any(c => c.Channel.Name.StartsWith(m_Criteria.ChannelName) && c.IsDefault));
            }

            if (!string.IsNullOrWhiteSpace(m_Criteria.UserName))
            {
                query = query.Where(u => u.UserName.StartsWith(m_Criteria.UserName));
            }

            if (!string.IsNullOrEmpty(m_Criteria.Name))
            {
                if (m_Criteria.Name.Contains(","))
                {
                    var names = m_Criteria.Name.Split(',');
                    var firstname = names.Count() == 2 ? names[1] : "";
                    query =
                        query.Where(
                            x =>
                                x.UserIndividuals.Any(
                                    i =>
                                        i.Individual.FirstName.StartsWith(firstname) ||
                                        i.Individual.Surname.StartsWith(names[0])));
                }
                else
                {
                    query =
                        query.Where(
                            x =>
                                x.UserIndividuals.Any(
                                    i =>
                                        i.Individual.FirstName.StartsWith(m_Criteria.Name) ||
                                        i.Individual.Surname.StartsWith(m_Criteria.Name)));
                }
            }

            if (m_Criteria.IsApproved.HasValue)
            {
                query = query.Where(u => u.Status.IsApproved == m_Criteria.IsApproved);
            }

            if (m_Criteria.IsLoggedIn.HasValue)
            {
                query = query.Where(u => u.Status.IsLoggedIn == m_Criteria.IsLoggedIn);
            }

            if (m_Criteria.IsBillable.HasValue)
            {
                query = query.Where(u => u.IsBillable == m_Criteria.IsBillable);
            }

            if (m_Criteria.AuthorisationGroups != null && m_Criteria.AuthorisationGroups.Any())
            {
                foreach (var authorisationGroup in m_Criteria.AuthorisationGroups)
                {
                    query =
                        query.Where(
                            u =>
                                u.AuthorisationGroups.Any(
                                    x => x.AuthorisationGroup == new AuthorisationGroup {Id = authorisationGroup}));
                }
            }

            return query;
        }

        public SearchUsersQuery WithCriteria(UserSearchDto dto)
        {
            if (dto.OrderBy == null)
            {
                dto.OrderBy = new List<OrderByField>();
            }

            if (!dto.OrderBy.Any())
            {
                dto.OrderBy.Add(new OrderByField
                {
                    Direction = OrderByDirection.Ascending,
                    FieldName = "UserName"
                });
            }

            m_Criteria = dto;
            return this;
        }
    }
}