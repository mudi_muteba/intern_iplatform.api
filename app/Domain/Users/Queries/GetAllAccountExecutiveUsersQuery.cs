using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class GetAllAccountExecutiveUsersQuery : BaseQuery<User>
    {
        private int _ChannelId;

        public GetAllAccountExecutiveUsersQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NullUserQueryFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    //UserAuthorisation.List
                };
            }
        }

        public GetAllAccountExecutiveUsersQuery WithChannelId(int channelId)
        {
            _ChannelId = channelId;
            return this;
        }

        protected internal override IQueryable<User> Execute(IQueryable<User> query)
        {
            return query
                .Where(u => u.IsAccountExecutive &&
                    u.Status.IsApproved &&
                    u.Channels.FirstOrDefault(c => c.Channel.Id == _ChannelId) != null)
                .OrderBy(u => u.UserName);
        }
    }
}