﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class FindUserByPasswordResetTokenQuery : BaseQuery<User>, IContextFreeQuery
    {
        private Guid _passwordtoken;
        private string _token;
        public FindUserByPasswordResetTokenQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NullUserQueryFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        public FindUserByPasswordResetTokenQuery WithPasswordResetToken(Guid passwordResetToken)
        {
            _passwordtoken = passwordResetToken;
            return this;
        }

        public FindUserByPasswordResetTokenQuery WithToken(string token)
        {
            _token = token;
            return this;
        }

        protected internal override IQueryable<User> Execute(IQueryable<User> query)
        {
            var users = query.Where(u => u.Tokens.PasswordResetToken.Value == _passwordtoken);

            if (!users.Any())
                users = query.ToList().Where(x => x.Tokens.Token == _token).AsQueryable();

            return users;
        }
    }
}