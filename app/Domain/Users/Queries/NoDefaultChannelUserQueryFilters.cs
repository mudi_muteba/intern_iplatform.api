﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Users.Queries
{
    public class NoDefaultChannelUserQueryFilters : IApplyDefaultFilters<User>
    {
        public IQueryable<User> Apply(ExecutionContext executionContext, IQueryable<User> query)
        {
            return query
                .IsActive();
        }
    }
}