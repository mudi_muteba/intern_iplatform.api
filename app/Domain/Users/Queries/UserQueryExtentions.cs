﻿using System.Linq;

namespace Domain.Users.Queries
{
    public static class UserQueryExtentions
    {
        public static IQueryable<User> IsActive(this IQueryable<User> query)
        {
            return query.Where(s => s.Status.IsActive && s.Status.IsApproved);
        }
    }
}