﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class GetUserChannelByUserIdQuery : BaseQuery<UserChannel>, ISearchQuery<UserChannelSearchDto>
    {
        private UserChannelSearchDto _criteria;

        public GetUserChannelByUserIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<UserChannel>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>() { };
            }
        }

        public void WithCriteria(UserChannelSearchDto criteria)
        {
            this._criteria = criteria;
        }

        public GetUserChannelByUserIdQuery WithUserId(int userId)
        {
            this._criteria = new UserChannelSearchDto { UserId = userId };
            return this;
        }

        protected internal override IQueryable<UserChannel> Execute(IQueryable<UserChannel> query)
        {
            if (_criteria.UserId == 0)
                query = null;
            else
                query = query.Where(a => a.User.Id == _criteria.UserId);

            if(_criteria.ChannelId > 0)
                query = query.Where(a => a.Channel.Id == _criteria.ChannelId);

            return query;
        }
    }
}