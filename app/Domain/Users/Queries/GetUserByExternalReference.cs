﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class GetUserByExternalReference : BaseQuery<User>
    {
        private string _externalReference;

        public GetUserByExternalReference(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<User>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetUserByExternalReference WithUserExternalReference(string externalReference)
        {
            this._externalReference = externalReference;
            return this;
        }

        protected internal override IQueryable<User> Execute(IQueryable<User> query)
        {
            return query.Where(u => u.ExternalReference == _externalReference);
        }
    }
}