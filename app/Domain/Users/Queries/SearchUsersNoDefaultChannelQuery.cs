﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Users.Search;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class SearchUsersNoDefaultChannelQuery : BaseQuery<User>, IOrderableQuery
    {
        private UserSearchDto _criteria = new UserSearchDto();

        public SearchUsersNoDefaultChannelQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultChannelUserQueryFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    UserAuthorisation.List
                };
            }
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("UserName"), () => "UserName"},
                    {new OrderByField("IsLoggedIn"), () => "Status.IsLoggedIn"},
                    {new OrderByField("IsApproved"), () => "Status.IsApproved"},
                    {new OrderByField("LastLoggedInDate"), () => "Login.LastLoggedInDate"},
                };
            }
        }

        protected internal override IQueryable<User> Execute(IQueryable<User> query)
        {
            query = AddFilters(query);
            query = AddOrdering(query);

            return query;
        }

        private IQueryable<User> AddOrdering(IQueryable<User> query)
        {
            query = new OrderByBuilder<User>(OrderByDefinition).Build(query, _criteria.OrderBy);

            return query;
        }

        private IQueryable<User> AddFilters(IQueryable<User> query)
        {
            if (!string.IsNullOrWhiteSpace(_criteria.ChannelName))
            {
                query = query.Where(x => x.Channels.Any(c => c.Channel.Name.StartsWith(_criteria.ChannelName)));
            }

            if (!string.IsNullOrWhiteSpace(_criteria.UserName))
            {
                query = query.Where(u => u.UserName.StartsWith(_criteria.UserName));
            }

            if (!string.IsNullOrEmpty(_criteria.Name))
            {
                if (_criteria.Name.Contains(","))
                {
                    var names = _criteria.Name.Split(',');
                    var firstname = names.Count() == 2 ? names[1] : "";
                    query = query.Where(x => x.UserIndividuals.Any(i => i.Individual.FirstName.StartsWith(firstname) || i.Individual.Surname.StartsWith(names[0])));
                }
                else
                {
                    query = query.Where(x => x.UserIndividuals.Any(i => i.Individual.FirstName.StartsWith(_criteria.Name) || i.Individual.Surname.StartsWith(_criteria.Name)));
                }
            }

            if (_criteria.IsApproved.HasValue)
            {
                query = query.Where(u => u.Status.IsApproved == _criteria.IsApproved);
            }

            if (_criteria.IsLoggedIn.HasValue)
            {
                query = query.Where(u => u.Status.IsLoggedIn == _criteria.IsLoggedIn);
            }

            if (_criteria.AuthorisationGroups != null && _criteria.AuthorisationGroups.Any())
            {
                foreach (var authorisationGroup in _criteria.AuthorisationGroups)
                {
                    query = query.Where(u => u.AuthorisationGroups.Any(x => x.AuthorisationGroup == new AuthorisationGroup {Id = authorisationGroup}));
                }
            }

            return query;
        }

        public SearchUsersNoDefaultChannelQuery WithCriteria(UserSearchDto dto)
        {
            if (dto.OrderBy == null)
            {
                dto.OrderBy = new List<OrderByField>();
            }

            if (!dto.OrderBy.Any())
            {
                dto.OrderBy.Add(new OrderByField
                {
                    Direction = OrderByDirection.Ascending,
                    FieldName = "UserName"
                });
            }

            _criteria = dto;
            return this;
        }
    }
}