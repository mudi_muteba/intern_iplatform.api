using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Users.Queries
{
    public class NullUserQueryFilter : IApplyDefaultFilters<User>
    {
        public IQueryable<User> Apply(ExecutionContext executionContext, IQueryable<User> query)
        {
            if (executionContext == null)
            {
                return query;
            }

            return query
                .Where(u => u.Channels.Any(c => executionContext.Channels.Contains(c.Channel.Id)));
        }
    }
}