﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class FindUserByUserNameQuery : BaseQuery<User>, IContextFreeQuery
    {
        private string username;

        public FindUserByUserNameQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NullUserQueryFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    
                };
            }
        }

        public FindUserByUserNameQuery WithUserName(string username)
        {
            this.username = username;
            return this;
        }

        protected internal override IQueryable<User> Execute(IQueryable<User> query)
        {
            return query.Where(u => u.UserName == username);
        }
    }
}