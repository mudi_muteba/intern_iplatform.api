﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class GetUserChannelByUserIdChannelId : BaseQuery<UserChannel>
    {
        private int _userId;
        private int _channelId;

        public GetUserChannelByUserIdChannelId(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<UserChannel>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetUserChannelByUserIdChannelId WithUserIdChannelId(int userId, int channelId)
        {
            this._userId = userId;
            this._channelId = channelId;
            return this;
        }

        protected internal override IQueryable<UserChannel> Execute(IQueryable<UserChannel> query)
        {
            return query.Where(uc => uc.User.Id == _userId && uc.Channel.Id == _channelId);
        }
    }
}