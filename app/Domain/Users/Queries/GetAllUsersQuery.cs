using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class GetAllUsersQuery : BaseQuery<User>
    {
        private int _ChannelId;

        public GetAllUsersQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NullUserQueryFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    //UserAuthorisation.List
                };
            }
        }

        public GetAllUsersQuery WithChannelId(int channelId)
        {
            _ChannelId = channelId;
            return this;
        }

        protected internal override IQueryable<User> Execute(IQueryable<User> query)
        {
            if (_ChannelId > 0)
                return query
                    .Where(q => q.Channels.Any(c => c.Channel.Id == _ChannelId))
                    .OrderBy(q => q.UserName);
            else
                return query
                    .OrderBy(q => q.UserName);
        }
    }
}