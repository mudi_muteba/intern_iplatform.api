﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class GetUserById : BaseQuery<User>
    {
        private int _userId;

        public GetUserById(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<User>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetUserById WithUserId(int userId)
        {
            this._userId = userId;
            return this;
        }

        protected internal override IQueryable<User> Execute(IQueryable<User> query)
        {
            return query.Where(u => u.Id == _userId);
        }
    }
}