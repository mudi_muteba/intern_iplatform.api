﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Users.Queries
{
    public class GetUserIndividualByUserId : BaseQuery<UserIndividual>
    {
        private int _userId;

        public GetUserIndividualByUserId(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<UserIndividual>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetUserIndividualByUserId WithUserId(int userId)
        {
            this._userId = userId;
            return this;
        }

        protected internal override IQueryable<UserIndividual> Execute(IQueryable<UserIndividual> query)
        {
            return query.Where(x => x.User.Id == _userId);
        }
    }
}