﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Users.Queries
{
    public class DefaultUserQueryFilters : IApplyDefaultFilters<User>
    {
        public IQueryable<User> Apply(ExecutionContext executionContext, IQueryable<User> query)
        {
            if (executionContext == null)
            {
                return query.IsActive();
            }

            return query
                .IsActive()
                .Where(u => u.Channels.Any(c => executionContext.Channels.Contains(c.Channel.Id)));
        }
    }
}