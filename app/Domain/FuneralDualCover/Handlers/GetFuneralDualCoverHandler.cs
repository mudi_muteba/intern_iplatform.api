﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.FuneralDualCover;
using iPlatform.Integration.Services.Cims;

namespace Domain.FuneralDualCover.Handlers
{
    public class GetFuneralDualCoverHandler : BaseDtoHandler<GetFuneralDualCoverDto, FuneralDualCoverDto>
    {
        public GetFuneralDualCoverHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void InternalHandle(GetFuneralDualCoverDto dto, HandlerResult<FuneralDualCoverDto> result)
        {
            var response = new FuneralDualCoverDto();
            var client = new CimsFuneralMembers();
            response.Value = client.Get(dto.IdNumber, dto.SumInsured);
            result.Processed(response);
        }
    }
}