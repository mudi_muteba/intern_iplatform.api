﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.SecondLevel.Answers;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using MasterData.Authorisation;

namespace Domain.SecondLevel.Queries
{
    public class SecondLevelQuestionQuoteItemByQuoteQuery : BaseProcedureQuery<List<SecondLevelQuestionQuoteItemDto>>
    {
        private List<QuoteItemDto> _quoteItems;
        private List<SecondLevelQuestionDefinitionDto> _questions;
        private List<SecondLevelQuestionAnswerSaveDto> _savedAnswers;
        public SecondLevelQuestionQuoteItemByQuoteQuery(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider, repository, executionPlan)
        {
        }

        public SecondLevelQuestionQuoteItemByQuoteQuery WithQuoteItems(List<QuoteItemDto> items, List<SecondLevelQuestionDefinitionDto> questions, List<SecondLevelQuestionAnswerSaveDto> savedAnswers)
        {
            _quoteItems = items;
            _questions = questions;
            _savedAnswers = savedAnswers;
            return this;
        }

        protected override List<SecondLevelQuestionQuoteItemDto> Execute()
        {
            var result = new List<SecondLevelQuestionQuoteItemDto>();

            foreach (var item in _quoteItems)
            {
                var coverDefinitionId = item.CoverDefinition.Id;
                var quoteQuestions = _questions
                    .Where(w => w.CoverDefinition != null && w.CoverDefinition.Id == coverDefinitionId).ToList();

                if (!quoteQuestions.Any())
                    quoteQuestions = new List<SecondLevelQuestionDefinitionDto>();

                foreach (var quoteQuestion in quoteQuestions)
                {
                    var quoteQuestionAnswer =
                        _savedAnswers.ToList()
                            .FirstOrDefault(a => a.QuoteItemId == item.Id && a.SecondLevelQuestionId == quoteQuestion.Id);

                    if (quoteQuestionAnswer != null)
                    {
                        quoteQuestion.SecondLevelQuestion.Answer = quoteQuestionAnswer.Answer;
                    }
                }

                GetByIdResult<ProposalDefinitionDto> proposal = ExecutionPlan
                    .GetById<ProposalDefinition, ProposalDefinitionDto>(() => Repository.GetById<ProposalDefinition>(item.Asset.AssetNumber))
                    .OnSuccess(Mapper.Map<ProposalDefinition, ProposalDefinitionDto>)
                    .Execute();

                var dto = new SecondLevelQuestionQuoteItemDto()
                {
                    Description = (proposal == null || proposal.Response == null) ? "No Description Found" : proposal.Response.Description,
                    QuoteItemId = item.Id,
                    SumInsured = item.Asset.SumInsured.Value,
                    CreatedDate = (proposal == null || proposal.Response == null) ? DateTime.UtcNow : proposal.Response.Created.Value,
                    Cover = item.CoverDefinition.Cover,
                    Questions = quoteQuestions.Select(s => s.SecondLevelQuestion).ToList()
                };

                result.Add(dto);
            }

            return result;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    SecondLevelUnderwritingAuthorisation.CanAcceptQuote
                };
            }
        }
    }
}
