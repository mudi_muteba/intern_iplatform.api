﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.SecondLevel.Answers;

namespace Domain.SecondLevel.Queries
{
    public class NullSecondLevelQuestionSavedAnswerQueryFilter : IApplyDefaultFilters<SecondLevelQuestionSavedAnswer>
    {
        public IQueryable<SecondLevelQuestionSavedAnswer> Apply(ExecutionContext executionContext, IQueryable<SecondLevelQuestionSavedAnswer> query)
        {
            return query;
        }
    }
}
