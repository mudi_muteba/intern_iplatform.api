﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.SecondLevel.Questions;

namespace Domain.SecondLevel.Queries
{
    public class DefaultSecondLevelQuestionDefinitionQueryFilter : IApplyDefaultFilters<SecondLevelQuestionDefinition>
    {
        public IQueryable<SecondLevelQuestionDefinition> Apply(ExecutionContext executionContext, IQueryable<SecondLevelQuestionDefinition> query)
        {
            return query;
        }
    }
}
