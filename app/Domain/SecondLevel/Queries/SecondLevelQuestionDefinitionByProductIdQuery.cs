﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.SecondLevel.Questions;
using MasterData.Authorisation;

namespace Domain.SecondLevel.Queries
{
    public class SecondLevelQuestionDefinitionByProductIdQuery : BaseQuery<SecondLevelQuestionDefinition>
    {
        private int _id;

        public SecondLevelQuestionDefinitionByProductIdQuery WithId(int id)
        {
            _id = id;
            return this;
        }

        public SecondLevelQuestionDefinitionByProductIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultSecondLevelQuestionDefinitionQueryFilter())
        {
        }

        protected internal override IQueryable<SecondLevelQuestionDefinition> Execute(IQueryable<SecondLevelQuestionDefinition> query)
        {
            return query.Where(w => w.Product.Id == _id);
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    SecondLevelUnderwritingAuthorisation.CanAcceptQuote
                };
            }
        }
    }
}