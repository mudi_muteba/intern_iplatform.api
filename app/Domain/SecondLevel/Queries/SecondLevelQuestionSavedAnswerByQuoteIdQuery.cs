﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.SecondLevel.Answers;
using MasterData.Authorisation;

namespace Domain.SecondLevel.Queries
{
    public class SecondLevelQuestionSavedAnswerByQuoteIdQuery : BaseQuery<SecondLevelQuestionSavedAnswer>
    {
        private int _id;

        public SecondLevelQuestionSavedAnswerByQuoteIdQuery WithId(int id)
        {
            _id = id;
            return this;
        }

        public SecondLevelQuestionSavedAnswerByQuoteIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NullSecondLevelQuestionSavedAnswerQueryFilter())
        {
        }

        protected internal override IQueryable<SecondLevelQuestionSavedAnswer> Execute(IQueryable<SecondLevelQuestionSavedAnswer> query)
        {
            return query.Where(w => w.Quote.Id == _id);
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                   SecondLevelUnderwritingAuthorisation.CanAcceptQuote
                };
            }
        }
    }
}