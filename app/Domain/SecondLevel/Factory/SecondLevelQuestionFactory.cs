﻿using Castle.Windsor;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.SecondLevel.Factory
{
    public class SecondLevelQuestionFactory
    {
        private static readonly ILog log = LogManager.GetLogger<SecondLevelQuestionFactory>();
        public SecondLevelQuestionFactory(IWindsorContainer container,IRepository repository, IExecutionPlan executionPlan)
        {
            _container = container;
            _repository = repository;
            _executionPlan = executionPlan;
        }

        private readonly IRepository _repository;
        private static IWindsorContainer _container;
        private readonly IExecutionPlan _executionPlan;


        public ICreateSecondLevelQuestions Create(string productCode)
        {
            ICreateSecondLevelQuestions mapper;
            try
            {
                log.InfoFormat("Retrieving GetSecondLevelQuestions for product code {0}", productCode);
                mapper = _container.Resolve<ICreateSecondLevelQuestions>(productCode.ToUpper());
                _container.Release(mapper);
            }
            catch (Exception)
            {
                log.InfoFormat("Unable to retrieve SecondLevelQuestion, loading DefaultGetSecondLevelQuestions implementation");
                mapper = new AutoGetSecondLevelQuestions(_repository, _executionPlan);
            }
            return mapper;
        }
    }
}
