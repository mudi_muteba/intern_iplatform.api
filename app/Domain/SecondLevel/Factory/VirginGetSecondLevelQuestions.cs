﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.SecondLevel.Answers;
using Domain.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Party.Quotes;
using Domain.Party.ProposalDefinitions;
using MasterData;
using Domain.Party.Quotes;
using AutoMapper;


namespace Domain.SecondLevel.Factory
{
    //Default
    public class VirginGetSecondLevelQuestions : ICreateSecondLevelQuestions
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan _executionPlan;
        public VirginGetSecondLevelQuestions(IRepository repository, IExecutionPlan executionPlan)
        {
            _repository = repository;
            _executionPlan = executionPlan;
        }

        public SecondLevelQuestionsForQuoteDto Create(SecondLevelQuoteDto quote)
        {
            var secondLevelQuestionsForQuote = new SecondLevelQuestionsForQuoteDto();

            // Query all question definitions for product first
            IQueryable<SecondLevelQuestionDefinition> questionsForProduct = _repository.GetAll<SecondLevelQuestionDefinition>()
                .Where(x => x.Product.Id == quote.ProductId && x.IsDeleted == false);

            List<SecondLevelQuestionSavedAnswer> answersForQuote = _repository.GetAll<SecondLevelQuestionSavedAnswer>()
                .Where(x => x.Quote.Id == quote.QuoteId && x.IsDeleted == false).ToList();

            // Get all proposal definitions
            List<SecondLevelQuestionSavedAnswer> answersForQuoteList = answersForQuote.ToList();

            // For each item in quote, load detail and questions
            foreach (QuoteItemDto quoteItem in quote.Items)
            {
                ProposalDefinition proposalDefinition = _repository.GetAll<ProposalDefinition>()
                    .FirstOrDefault(x => x.Id == quoteItem.Asset.AssetNumber && x.IsDeleted == false);

                var proposalRegistrationNumberQuestion = proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(x => x.QuestionDefinition.Question.Id == MasterData.Questions.VehicleRegistrationNumber.Id);
                var proposalVinNumberQuestion = proposalDefinition.ProposalQuestionAnswers.FirstOrDefault(x => x.QuestionDefinition.Question.Id == MasterData.Questions.AIGVinNumber.Id);

                // get all second level questions for this product
                List<SecondLevelQuestionDto> quoteItemQuestions = GetQuestionsForItem(proposalDefinition, questionsForProduct, quoteItem);

                var underwritingRegistrationNumberQuestion = quoteItemQuestions.FirstOrDefault(x => x.Id == SecondLevelQuestions.VehicleRegistrationNumber.Id);
                var underwritingVinNumberQuestion = quoteItemQuestions.FirstOrDefault(x => x.Id == SecondLevelQuestions.VINNumber.Id);

                if (proposalRegistrationNumberQuestion != null && proposalRegistrationNumberQuestion.Answer != null)
                {
                    if (underwritingRegistrationNumberQuestion != null && underwritingRegistrationNumberQuestion.Answer == null || string.IsNullOrEmpty(underwritingRegistrationNumberQuestion.Answer))
                    {
                        underwritingRegistrationNumberQuestion.Answer = proposalRegistrationNumberQuestion.Answer;
                    }
                }

                if (proposalVinNumberQuestion != null && proposalVinNumberQuestion.Answer != null)
                {
                    if (underwritingVinNumberQuestion != null && underwritingVinNumberQuestion.Answer == null || string.IsNullOrEmpty(underwritingVinNumberQuestion.Answer))
                    {
                        underwritingVinNumberQuestion.Answer = proposalVinNumberQuestion.Answer;
                    }
                }


                foreach (SecondLevelQuestionDto quoteItemQuestion in quoteItemQuestions)
                {
                    SecondLevelQuestionSavedAnswer quoteItemAnswer = answersForQuoteList.FirstOrDefault(a => a.QuoteItem != null && a.QuoteItem.Id == quoteItem.Id && a.SecondLevelQuestion.Id == quoteItemQuestion.Id);

                    if (quoteItemAnswer != null)
                        quoteItemQuestion.Answer = quoteItemAnswer.Answer;
                }

                var dtoItem = new SecondLevelQuestionQuoteItemDto
                {
                    Description = (proposalDefinition == null) ? "No Description Found" : proposalDefinition.Description,
                    QuoteItemId = quoteItem.Id,
                    SumInsured = quoteItem.Asset.SumInsured.Value,
                    CreatedDate = (proposalDefinition == null) ? DateTime.UtcNow : proposalDefinition.Created,
                    Cover = quoteItem.CoverDefinition.Cover,
                    Questions = quoteItemQuestions

                };

                secondLevelQuestionsForQuote.QuoteItems.Add(dtoItem);
            }

            return secondLevelQuestionsForQuote;
        }

        private List<SecondLevelQuestionDto> GetQuestionsForItem(ProposalDefinition proposalDefinition
            , IQueryable<SecondLevelQuestionDefinition> questionsForProduct
            , QuoteItemDto quoteItem)
        {

            List<SecondLevelQuestionDto> quoteItemQuestions = questionsForProduct
                .Where(x => x.CoverDefinition.Id == quoteItem.CoverDefinition.Id)
                .Select(a => Mapper.Map<SecondLevelQuestionDto>(a))
                .ToList();

            return quoteItemQuestions;
        }
    }
}
