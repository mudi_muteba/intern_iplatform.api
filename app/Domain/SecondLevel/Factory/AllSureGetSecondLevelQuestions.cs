﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.SecondLevel.Answers;
using Domain.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Party.Quotes;
using Domain.Party.ProposalDefinitions;
using MasterData;
using AutoMapper;

namespace Domain.SecondLevel.Factory
{
    //Default
    public class AllsureGetSecondLevelQuestions : ICreateSecondLevelQuestions
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan _executionPlan;
        public AllsureGetSecondLevelQuestions(IRepository repository, IExecutionPlan executionPlan)
        {
            _repository = repository;
            _executionPlan = executionPlan;
        }

        public SecondLevelQuestionsForQuoteDto Create(SecondLevelQuoteDto quote)
        {
            var secondLevelQuestionsForQuote = new SecondLevelQuestionsForQuoteDto();

            // Query all question definitions for product first
            IQueryable<SecondLevelQuestionDefinition> questionsForProduct = _repository.GetAll<SecondLevelQuestionDefinition>()
                .Where(x => x.Product.Id == quote.ProductId && x.IsDeleted == false);

            List<SecondLevelQuestionSavedAnswer> answersForQuote = _repository.GetAll<SecondLevelQuestionSavedAnswer>()
                .Where(x => x.Quote.Id == quote.QuoteId && x.IsDeleted == false).ToList();

            // Get all proposal definitions
            List<SecondLevelQuestionSavedAnswer> answersForQuoteList = answersForQuote.ToList();

            // For each item in quote, load detail and questions
            foreach (QuoteItemDto quoteItem in quote.Items)
            {
                ProposalDefinition proposalDefinition = _repository.GetAll<ProposalDefinition>()
                    .FirstOrDefault(x => x.Id == quoteItem.Asset.AssetNumber && x.IsDeleted == false);

                List<SecondLevelQuestionDto> quoteItemQuestions = GetQuestionsForItem(proposalDefinition, questionsForProduct, quoteItem);

                foreach (SecondLevelQuestionDto quoteItemQuestion in quoteItemQuestions)
                {
                    SecondLevelQuestionSavedAnswer quoteItemAnswer = answersForQuoteList.FirstOrDefault(a => a.QuoteItem != null && a.QuoteItem.Id == quoteItem.Id && a.SecondLevelQuestion.Id == quoteItemQuestion.Id);

                    if (quoteItemAnswer != null)
                    {
                        quoteItemQuestion.Answer = quoteItemAnswer.Answer;
                    }
                }

                var dtoItem = new SecondLevelQuestionQuoteItemDto
                {
                    Description = (proposalDefinition == null) ? "No Description Found" : proposalDefinition.Description,
                    QuoteItemId = quoteItem.Id,
                    SumInsured = quoteItem.Asset.SumInsured.Value,
                    CreatedDate = (proposalDefinition == null) ? DateTime.UtcNow : proposalDefinition.Created,
                    Cover = quoteItem.CoverDefinition.Cover,
                    Questions = quoteItemQuestions
                };

                secondLevelQuestionsForQuote.QuoteItems.Add(dtoItem);
            }

            return secondLevelQuestionsForQuote;
        }

        private List<SecondLevelQuestionDto> GetQuestionsForItem(ProposalDefinition proposalDefinition
            , IQueryable<SecondLevelQuestionDefinition> questionsForProduct
            , QuoteItemDto quoteItem)
        {
            if (proposalDefinition.Cover.Id == Covers.AllRisk.Id)
            {
                return CustomAllRiskQuestions(proposalDefinition, questionsForProduct, quoteItem);
            }

            if (proposalDefinition.Cover.Id == Covers.Motor.Id)
            {
                return CustomMotorQuestions(proposalDefinition, questionsForProduct, quoteItem);
            }

            List<SecondLevelQuestionDto> quoteItemQuestions = questionsForProduct
                .Where(x => x.CoverDefinition.Id == quoteItem.CoverDefinition.Id)
                .Select(a => Mapper.Map<SecondLevelQuestionDto>(a))
                .ToList();

            return quoteItemQuestions;
        }


        private List<SecondLevelQuestionDto> CustomMotorQuestions(ProposalDefinition proposalDefinition
            , IQueryable<SecondLevelQuestionDefinition> questionsForProduct
            , QuoteItemDto quoteItem)
        {
            var questionsToIgnore = new List<int>();

            List<SecondLevelQuestionDefinition> questions = questionsForProduct
                .Where(x => x.CoverDefinition.Id == quoteItem.CoverDefinition.Id)
                .ToList();

            bool licensePlateNumberAnswered = proposalDefinition.IsQuestionAnswered(MasterData.Questions.VehicleRegistrationNumber);
            bool vinNumberAnswered = proposalDefinition.IsQuestionAnswered(MasterData.Questions.MFMotorVINNumber);
            bool engineNumberAnswered = proposalDefinition.IsQuestionAnswered(MasterData.Questions.MFMotorEngineNumber);
            bool natisNumberAnswered = proposalDefinition.IsQuestionAnswered(MasterData.Questions.MFMotorNATISOrRegisternumber);

            if (licensePlateNumberAnswered)
            {
                questionsToIgnore.Add(SecondLevelQuestions.VehicleRegistrationNumber.Id);
            }

            if (vinNumberAnswered)
            {
                questionsToIgnore.Add(SecondLevelQuestions.VINNumber.Id);
            }

            if (engineNumberAnswered)
            {
                questionsToIgnore.Add(SecondLevelQuestions.EngineNumber.Id);
            }

            if (natisNumberAnswered)
            {
                questionsToIgnore.Add(SecondLevelQuestions.NATISRegisternumber.Id);
            }

            return questions
                .Where(q => questionsToIgnore.All(qti => qti != q.SecondLevelQuestion.Id))
                .Select(a => Mapper.Map<SecondLevelQuestionDto>(a)).ToList();
        }

        private List<SecondLevelQuestionDto> CustomAllRiskQuestions(ProposalDefinition proposalDefinition
            , IQueryable<SecondLevelQuestionDefinition> questionsForProduct
            , QuoteItemDto quoteItem)
        {
            var questionsToIgnore = new List<int>();

            List<SecondLevelQuestionDefinition> questions = questionsForProduct
                .Where(x => x.CoverDefinition.Id == quoteItem.CoverDefinition.Id)
                .ToList();

            bool serialNumberRequired = proposalDefinition.IsQuestionAnsweredWith(MasterData.Questions.MFAllRiskRiskCategory, QuestionAnswers.MFAllRiskRiskCategoryCellularTelephone.Id.ToString()) ||
                                        proposalDefinition.IsQuestionAnsweredWith(MasterData.Questions.MFAllRiskRiskCategory, QuestionAnswers.MFAllRiskRiskCategoryFirearm.Id.ToString());

            bool serialNumberAnswered = proposalDefinition.IsQuestionAnswered(MasterData.Questions.MFAllRiskSerialIMEINumber);

            if (serialNumberRequired && serialNumberAnswered)
            {
                questionsToIgnore.Add(SecondLevelQuestions.MFAllRiskSerialIMEINumber.Id);
            }

            if (!serialNumberRequired)
            {
                questionsToIgnore.Add(SecondLevelQuestions.MFAllRiskSerialIMEINumber.Id);
            }

            return questions
                .Where(q => questionsToIgnore.All(qti => qti != q.SecondLevelQuestion.Id))
                .Select(a => Mapper.Map<SecondLevelQuestionDto>(a)).ToList();
        }
    }
}
