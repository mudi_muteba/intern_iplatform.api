﻿using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using Domain.Party.Quotes;
using Domain.SecondLevel.Answers;
using Domain.SecondLevel.Queries;
using Domain.SecondLevel.Questions;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Domain.SecondLevel.Factory
{
    //Default
    public class AutoGetSecondLevelQuestions : ICreateSecondLevelQuestions
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan _executionPlan;
        public AutoGetSecondLevelQuestions(IRepository repository, IExecutionPlan executionPlan)
        {
            _repository = repository;
            _executionPlan = executionPlan;
        }

        public SecondLevelQuestionsForQuoteDto Create(SecondLevelQuoteDto quote)
        {
            var secondLevelQuestionsForQuote = new SecondLevelQuestionsForQuoteDto();

            // Query all question definitions for product first
            IQueryable<SecondLevelQuestionDefinition> questionsForProduct = _repository.GetAll<SecondLevelQuestionDefinition>()
                .Where(x => x.Product.Id == quote.ProductId && x.IsDeleted == false);

            List<SecondLevelQuestionSavedAnswer> answersForQuote = _repository.GetAll<SecondLevelQuestionSavedAnswer>()
                .Where(x => x.Quote.Id == quote.QuoteId && x.IsDeleted == false).ToList();

            // Load top level questions not applicable to specific cover
            secondLevelQuestionsForQuote.Questions = new List<SecondLevelQuestionDto>();

            List<SecondLevelQuestionDefinition> genericQuestions = questionsForProduct
                .Where(x => x.CoverDefinition == null)
                .ToList();

            foreach (SecondLevelQuestionDefinition rootQuestion in genericQuestions)
            {
                SecondLevelQuestionDto genericQuestion = Mapper.Map<SecondLevelQuestionDto>(rootQuestion);

                SecondLevelQuestionSavedAnswer answer = answersForQuote.FirstOrDefault(a => a.QuoteItem == null && a.SecondLevelQuestion.Id == rootQuestion.SecondLevelQuestion.Id);

                if (answer != null)
                {
                    genericQuestion.Answer = answer.Answer;
                }
                else
                {
                    if (rootQuestion.SecondLevelQuestion.Name.Equals("Under debt review or administration"))
                    {
                        genericQuestion.Answer = new SecondLevelQuestionAnswers()
                            .FirstOrDefault(
                                x =>
                                    x.SecondLevelQuestion.Id == rootQuestion.SecondLevelQuestion.Id &&
                                    x.Answer == (quote.Individual.UnderAdministrationOrDebtReview ? "Yes" : "No"))
                            .Id.ToString();
                    }
                    else if (rootQuestion.SecondLevelQuestion.Name.Equals("Any defaults or judgements"))
                    {
                        genericQuestion.Answer = new SecondLevelQuestionAnswers()
                            .FirstOrDefault(
                                x =>
                                    x.SecondLevelQuestion.Id == rootQuestion.SecondLevelQuestion.Id &&
                                    x.Answer == (quote.Individual.AnyJudgements ? "Yes" : "No"))
                            .Id.ToString();
                    }
                    else if (rootQuestion.SecondLevelQuestion.Name.Equals("Insolvent, sequestrated or liquidated"))
                    {
                        genericQuestion.Answer = new SecondLevelQuestionAnswers()
                            .FirstOrDefault(
                                x =>
                                    x.SecondLevelQuestion.Id == rootQuestion.SecondLevelQuestion.Id &&
                                    x.Answer == (quote.Individual.BeenSequestratedOrLiquidated ? "Yes" : "No"))
                            .Id.ToString();
                    }
                }

                secondLevelQuestionsForQuote.Questions.Add(genericQuestion);
            }

            // Get all proposal definitions
            IQueryable<ProposalDefinition> proposalDefinitions = _repository.GetAll<ProposalDefinition>();

            List<SecondLevelQuestionSavedAnswer> answersForQuoteList = answersForQuote.ToList();

            // For each item in quote, load detail and questions
            foreach (QuoteItemDto quoteItem in quote.Items)
            {
                List<SecondLevelQuestionDto> quoteItemQuestions = questionsForProduct
                    .Where(x => x.CoverDefinition.Id == quoteItem.CoverDefinition.Id)
                    .Select(a => Mapper.Map<SecondLevelQuestionDto>(a))
                    .ToList();

                foreach (SecondLevelQuestionDto quoteItemQuestion in quoteItemQuestions)
                {
                    SecondLevelQuestionSavedAnswer quoteItemAnswer = answersForQuoteList.FirstOrDefault(a => a.QuoteItem != null && a.QuoteItem.Id == quoteItem.Id && a.SecondLevelQuestion.Id == quoteItemQuestion.Id);

                    if (quoteItemAnswer != null)
                    {
                        quoteItemQuestion.Answer = quoteItemAnswer.Answer;
                    }
                }

                ProposalDefinition proposalDefinitionEntity =
                    proposalDefinitions.FirstOrDefault(x => x.Id == quoteItem.Asset.AssetNumber);

                var dtoItem = new SecondLevelQuestionQuoteItemDto
                {
                    Description = (proposalDefinitionEntity == null) ? "No Description Found" : proposalDefinitionEntity.Description,
                    QuoteItemId = quoteItem.Id,
                    SumInsured = quoteItem.Asset.SumInsured.Value,
                    CreatedDate = (proposalDefinitionEntity == null) ? DateTime.UtcNow : proposalDefinitionEntity.Created,
                    Cover = quoteItem.CoverDefinition.Cover,
                    Questions = quoteItemQuestions
                };

                secondLevelQuestionsForQuote.QuoteItems.Add(dtoItem);
            }

            return secondLevelQuestionsForQuote;
        }

        
    }
}
