﻿using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;

namespace Domain.SecondLevel.Factory
{
    public interface ICreateSecondLevelQuestions
    {
        SecondLevelQuestionsForQuoteDto Create(SecondLevelQuoteDto quote);
    }
}
