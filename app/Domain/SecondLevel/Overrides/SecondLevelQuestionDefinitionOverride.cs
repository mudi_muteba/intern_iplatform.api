﻿using Domain.SecondLevel.Questions;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.SecondLevel.Overrides
{
    public class SecondLevelQuestionDefinitionOverride : IAutoMappingOverride<SecondLevelQuestionDefinition>
    {
        public void Override(AutoMapping<SecondLevelQuestionDefinition> mapping)
        {
            mapping.Map(x => x.DisplayName).Length(1000);
        }
    }
}