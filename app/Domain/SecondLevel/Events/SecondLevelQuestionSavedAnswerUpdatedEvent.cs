﻿using Domain.Admin;
using Domain.Base.Events;
using Domain.SecondLevel.Answers;

namespace Domain.SecondLevel.Events
{
    public class SecondLevelQuestionSavedAnswerUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public SecondLevelQuestionSavedAnswerUpdatedEvent(SecondLevelQuestionSavedAnswer secondLevelQuestionSavedAnswer, EventAudit audit, ChannelReference channelReference)
            : base(audit, channelReference)
        {
            SecondLevelQuestionSavedAnswer = secondLevelQuestionSavedAnswer;
        }

        public SecondLevelQuestionSavedAnswer SecondLevelQuestionSavedAnswer { get; private set; }
        public int UserId { get; private set; }
    }
}
