﻿using System;
using System.Collections.Generic;
using Domain.Base;
using Domain.Products;
using MasterData;

namespace Domain.SecondLevel.Questions
{
    [Serializable]
    public class SecondLevelQuestionDefinition : Entity
    {
        public SecondLevelQuestionDefinition()
        {
            Children = new List<SecondLevelQuestionDefinition>();
        }

        public virtual string DisplayName { get; set; }
        public virtual CoverDefinition CoverDefinition { get; set; }
        public virtual Product Product { get; set; }
        public virtual SecondLevelQuestion SecondLevelQuestion { get; set; }
        public virtual SecondLevelQuestionDefinition Parent { get; set; }
        public virtual IList<SecondLevelQuestionDefinition> Children { get; protected set; }
        public virtual QuestionDefinitionType QuestionDefinitionType { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual int MasterId { get; set; }
        public virtual bool Required { get; set; }
        public virtual bool RatingFactor { get; set; }
        public virtual bool ReadOnly { get; set; }
        public virtual string ToolTip { get; set; }
        public virtual string DefaultValue { get; set; }
        public virtual string RegexPattern { get; set; }
        public virtual void AddChildQuestion(SecondLevelQuestionDefinition questionDefinition)
        {
            questionDefinition.Parent = this;
            questionDefinition.CoverDefinition = CoverDefinition;

            Children.Add(questionDefinition);
        }

        public virtual SecondLevelQuestionDefinition CreateSubQuestion(SecondLevelQuestionDefinition subQuestion)
        {
            subQuestion.Parent = this;
            subQuestion.CoverDefinition = CoverDefinition;
            subQuestion.QuestionDefinitionType = QuestionDefinitionTypes.SubQuestion;
            Children.Add(subQuestion);

            return subQuestion;
        }

        public virtual SecondLevelQuestionDefinition CreateSubQuestion()
        {
            return CreateSubQuestion(new SecondLevelQuestionDefinition());
        }
    }
}