﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Activities;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Statistics
{
    public class LeadConversionStatisticsQuery<T> : BaseQuery<T> where T : LeadActivity
    {
        private int _agentId;
        private DateTime _date;
        private IEnumerable<int> _leadIds;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint> { CampaignAuthorisation.List }; }
        }

        public LeadConversionStatisticsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<T>())
        {
        }

        public LeadConversionStatisticsQuery<T> WithAgentId(int agentId)
        {
            _agentId = agentId;
            return this;
        }

        public LeadConversionStatisticsQuery<T> WithDate(DateTime date)
        {
            _date = date;
            return this;
        }

        public LeadConversionStatisticsQuery<T> WithLeadIds(IEnumerable<int> leadIds)
        {
            _leadIds = leadIds;
            return this;
        }

        protected internal override IQueryable<T> Execute(IQueryable<T> query)
        {
            if (_leadIds != null && _leadIds.Any())
                query = query.Where(a => _leadIds.Contains(a.Lead.Id));

            var result = query.Where(a => a.User.Id == _agentId && a.DateCreated > _date);

            return result;
        }
    }
}