﻿using Domain.Admin;
using Domain.Base;
using Domain.Base.Events;
using Domain.Party.Quotes;
using Domain.SecondLevel.Events;
using MasterData;

namespace Domain.SecondLevel.Answers
{
    public class SecondLevelQuestionSavedAnswer : EntityWithAudit
    {
        public SecondLevelQuestionSavedAnswer()
        {

        }

        public virtual void Update()
        {
            RaiseEvent(new SecondLevelQuestionSavedAnswerUpdatedEvent(this, new EventAudit(), new Channel()));
        }

        public virtual Quote Quote { get; set; }

        public virtual QuoteItem QuoteItem { get; set; }

        public virtual string Answer { get; set; }

        public virtual SecondLevelQuestion SecondLevelQuestion { get; set; }

        public virtual int VisibleIndex { get; set; }
    }
}