﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.Quotes;
using Domain.SecondLevel.Answers;
using iPlatform.Api.DTOs.SecondLevel.Answers;
using MasterData;
using MasterData.Authorisation;

namespace Domain.SecondLevel.Handlers
{
    public class SaveSecondLevelAnswersHandler : BaseDtoHandler<SecondLevelQuestionsAnswersSaveDto,bool>
    {
        private readonly IRepository _repository;
        public SaveSecondLevelAnswersHandler(IProvideContext contextProvider, IRepository repository) :base(contextProvider)
        {
            _repository = repository;
        }

        protected override void InternalHandle(SecondLevelQuestionsAnswersSaveDto dto, HandlerResult<bool> result)
        {
            result.Processed(false);

            var quote = _repository.GetById<Quote>(dto.QuoteId);

            if (quote == null)
                return;

            var answersForQuote = _repository.GetAll<SecondLevelQuestionSavedAnswer>().Where(x => x.Quote.Id == quote.Id).ToList();

            foreach (var answer in dto.Items)
            {
                var quoteItemEntity = quote.Items.FirstOrDefault(x => x.Id == answer.QuoteItemId);
                var saveAnswer = answer.QuoteItemId <= 1 ? answersForQuote.FirstOrDefault(x => x.QuoteItem == null && x.SecondLevelQuestion.Id == answer.SecondLevelQuestionId) : answersForQuote.FirstOrDefault(x => x.QuoteItem.Id == answer.QuoteItemId && x.SecondLevelQuestion.Id == answer.SecondLevelQuestionId);

                if (saveAnswer == null)
                {
                    saveAnswer = new SecondLevelQuestionSavedAnswer()
                    {
                        Answer = answer.Answer,
                        Quote = quote,
                        QuoteItem = quoteItemEntity,
                        SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == answer.SecondLevelQuestionId)
                    };
                }
                else
                {
                    saveAnswer.Answer = answer.Answer;
                }

                _repository.Save(saveAnswer);
                saveAnswer.Update();
            }

            result.Processed(true);
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                   // SecondLevelUnderwritingAuthorisation.Full
                };
            }
        }

        
    }
}
