﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.SecondLevel.Answers;
using Domain.SecondLevel.Extensions;
using Domain.SecondLevel.Factory;
using Domain.SecondLevel.Queries;
using Domain.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Answers;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using MasterData;
using MasterData.Authorisation;

namespace Domain.SecondLevel.Handlers
{
    public class GetSecondLevelQuestionsHandler : IHandleDto<SecondLevelQuoteDto, SecondLevelQuestionsForQuoteDto>
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IWindsorContainer _container;
        private readonly IRepository _repository;
        public GetSecondLevelQuestionsHandler(IExecutionPlan executionPlan, IWindsorContainer container, IRepository repository)
        {
            _executionPlan = executionPlan;
            _container = container;
            _repository = repository;
        }

        public void Handle(SecondLevelQuoteDto quote, HandlerResult<SecondLevelQuestionsForQuoteDto> result)
        {
            ICreateSecondLevelQuestions factory = new SecondLevelQuestionFactory(_container, _repository, _executionPlan).Create(quote.ProductCode);
            SecondLevelQuestionsForQuoteDto questions = factory.Create(quote);
            result.Processed(questions);
        }

        public List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    SecondLevelUnderwritingAuthorisation.CanAcceptQuote
                };
            }
        }
    }
}