﻿using AutoMapper;
using Domain.Individuals;
using Domain.Party.Quotes;
using Domain.SecondLevel.Answers;
using Domain.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Answers;
using iPlatform.Api.DTOs.SecondLevel.Individual;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using MasterData;
using System.Linq;

namespace Domain.SecondLevel.Mappings
{
    public class SecondLevelMappings : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SecondLevelQuestionSavedAnswer, SecondLevelQuestionAnswerSaveDto>()
                .ForMember(m => m.QuoteItemId, o => o.MapFrom(s => s.QuoteItem.Id))
                .ForMember(m => m.Answer, o => o.MapFrom(s => s.Answer))
                .ForMember(m => m.SecondLevelQuestionId, o => o.MapFrom(s => s.SecondLevelQuestion.Id));

            Mapper.CreateMap<SecondLevelQuestionGroup, SecondLevelQuestionGroupDto>();

            Mapper.CreateMap<QuestionType, SecondLevelQuestionTypeDto>()
                .ForMember(m => m.Name, o => o.MapFrom(s => s.Name))
                .ForMember(m => m.Id, o => o.MapFrom(s => s.Id));

            Mapper.CreateMap<SecondLevelQuestion, SecondLevelQuestionDto>()
                .ForMember(m => m.Type, o => o.MapFrom(s => s.QuestionType))
                .ForMember(m => m.Group, o => o.MapFrom(s => s.SecondLevelQuestionGroup))
                ;

            Mapper.CreateMap<SecondLevelQuestionDefinition, SecondLevelQuestionDefinitionDto>()
                .ForMember(m => m.SecondLevelQuestion, o => o.MapFrom(s => s.SecondLevelQuestion))
                .ForMember(m => m.CoverDefinition, o => o.MapFrom(s => s.CoverDefinition))
                .ForMember(m => m.DisplayName, o => o.MapFrom(s => s.DisplayName))
                .ForMember(m => m.ToolTip, o => o.MapFrom(s => s.ToolTip))
                ;

            Mapper.CreateMap<Individual, SecondLevelIndividualDto>()
                .ForMember(t => t.IndvidualId, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.UnderAdministrationOrDebtReview, o => o.MapFrom(s => s.UnderAdministrationOrDebtReview))
                .ForMember(t => t.AnyJudgements, o => o.MapFrom(s => s.AnyJudgements))
                .ForMember(t => t.AnyJudgementsReason, o => o.MapFrom(s => s.AnyJudgementsReason))
                .ForMember(t => t.BeenSequestratedOrLiquidated, o => o.MapFrom(s => s.BeenSequestratedOrLiquidated))
                .ForMember(t => t.BeenSequestratedOrLiquidatedReason, o => o.MapFrom(s => s.BeenSequestratedOrLiquidatedReason));

            Mapper.CreateMap<Quote, SecondLevelQuoteDto>()
                .ForMember(t => t.QuoteId, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.QuoteHeader.Proposal.LeadActivity.Lead.Party.Id))
                .ForMember(t => t.Items, o => o.MapFrom(s => s.Items))
                .ForMember(t => t.Individual, o => o.Ignore())
                .ForMember(t => t.ProductCode, o => o.MapFrom(s => s.Product.ProductCode))
                .ForMember(t => t.ProductId, o => o.MapFrom(s => s.Product.Id));


            Mapper.CreateMap<SecondLevelQuestionDefinition,SecondLevelQuestionDto>()
                .ForMember(t => t.Answer, o => o.Ignore())
                .ForMember(t => t.Id, o => o.MapFrom(s => s.SecondLevelQuestion.Id))
                .ForMember(t => t.Name, o => o.MapFrom(s => s.DisplayName))
                .ForMember(t => t.ToolTip, o => o.MapFrom(s => s.ToolTip))
                .ForMember(t => t.Group, o => o.MapFrom(s =>
                    new SecondLevelQuestionGroupDto(s.SecondLevelQuestion.SecondLevelQuestionGroup.Id, s.SecondLevelQuestion.SecondLevelQuestionGroup.Name)))
                .ForMember(t => t.Type, o => o.MapFrom(s => 
                    new SecondLevelQuestionTypeDto(s.SecondLevelQuestion.QuestionType.Id, s.SecondLevelQuestion.QuestionType.Name)))
                .AfterMap((s, t) =>
                {
                    t.PossibleAnswers = new SecondLevelQuestionAnswers()
                        .Where(q => q.SecondLevelQuestion.Id == s.SecondLevelQuestion.Id)
                        .Select(a => new  SecondLevelQuestionAnswerDto(a.Id, a.Name, a.Answer)).ToList();
                });

        }
    }
}