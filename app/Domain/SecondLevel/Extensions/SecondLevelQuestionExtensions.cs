﻿using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using MasterData;

namespace Domain.SecondLevel.Extensions
{
    public static class SecondLevelQuestionExtensions
    {
        public static string AnswerFor(this SecondLevelQuestionDto question, SecondLevelQuoteDto dto, int questionId)
        {
            if(dto == null)
                return string.Empty;

            if (dto.Individual == null)
                return string.Empty;

            var answer = Questions.FirstOrDefault(w => w.Key.Equals(question.Name, StringComparison.CurrentCultureIgnoreCase));
            return answer.Value == null ? string.Empty : answer.Value(questionId, dto);
        }

        private static readonly IDictionary<string, Func<int, SecondLevelQuoteDto, string>> Questions = new Dictionary
            <string, Func<int,SecondLevelQuoteDto, string>>()
        {
            { "Under debt review or administration",(id, dto) => GetId(id, dto.Individual.UnderAdministrationOrDebtReview ? "Yes" : "No") },
            { "Any defaults or judgements",(id, dto) => GetId(id, dto.Individual.AnyJudgements ? "Yes" : "No") },
            { "Insolvent, sequestrated or liquidated",(id, dto) => GetId(id, dto.Individual.BeenSequestratedOrLiquidated ? "Yes" : "No") }
        };

        private static string GetId(int id, string answer)
        {
            var secondLevelQuestionAnswer = new SecondLevelQuestionAnswers().FirstOrDefault(f => f.SecondLevelQuestion.Id == id && f.Answer == answer);
            return secondLevelQuestionAnswer != null ? secondLevelQuestionAnswer.Id.ToString() : "";
        }
    }
}