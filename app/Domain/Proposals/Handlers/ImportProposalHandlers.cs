﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Leads.Handlers;
using iPlatform.Api.DTOs.Leads;
using MasterData.Authorisation;
using Domain.Leads.Extensions;
using iPlatform.Api.DTOs.Admin;

namespace Domain.Proposals.Handlers
{
    public class ImportProposalHandlers: BaseDtoHandler<SubmitLeadDto, int>
    {
        private readonly ILog _log = LogManager.GetLogger<ImportLeadHandler>();
        private readonly IRepository _repository;
        private readonly SystemChannels _systemChannels;
        private readonly IExecutionPlan _executionPlan;

        public ImportProposalHandlers(IProvideContext contextProvider, IExecutionPlan executionPlan, IRepository repository, SystemChannels systemChannels)
            : base(contextProvider)
        {
            _repository = repository;
            _systemChannels = systemChannels;
            _executionPlan = executionPlan;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(SubmitLeadDto dto, HandlerResult<int> result)
        {
            var response = dto.ImportProposals(_repository, _executionPlan,_systemChannels);
            result.Processed(response);
        }
    }   
}
