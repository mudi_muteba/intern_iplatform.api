﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Leads;
using Domain.Party.Quotes;
using Domain.Party.Quotes.Builders;
using Domain.Party.Quotes.Events;
using Domain.Proposals.Mappings;
using Domain.Ratings.Engines;
using Domain.SalesForce.Events;
using Domain.Users;
using iPlatform.Api.DTOs.Proposals;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData.Authorisation;
using NHibernate.Proxy;
using Domain.Activities;
using Domain.AuditLeadlog.Events;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.AuditLeadLog;
using Infrastructure.Configuration;

namespace Domain.Proposals.Handlers
{
    public class GetQuotesByProposalDtoHandler : BaseDtoHandler<GetQuotesByProposalDto, ProposalQuoteDto>
    {
        private readonly IProvideContext m_ContextProvider;
        private readonly IRatingEngine m_Engine;
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly SystemChannels m_SystemChannels;
        private readonly IRepository m_Repository;
        private ExecutionResult m_Result;

        public GetQuotesByProposalDtoHandler(IProvideContext contextProvider, IRepository repository, IRatingEngine engine,
            IExecutionPlan executionPlan, SystemChannels systemChannels)
            : base(contextProvider)
        {
            m_Repository = repository;
            m_ContextProvider = contextProvider;
            m_Engine = engine;
            m_ExecutionPlan = executionPlan;
            m_SystemChannels = systemChannels;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void InternalHandle(GetQuotesByProposalDto dto, HandlerResult<ProposalQuoteDto> result)
        {
            m_Result = result;
            if (!dto.ReQuote)
            {
                var QuoteHeaders = GetQuotesByProposalId(dto.Id);
                if (QuoteHeaders.Any())
                {
                    var response = QuoteHeaders.OrderByDescending(x => x.Id).First().MaptoProposalQuoteDto(m_Repository, dto.Context.UserId, dto.ChannelId);
                    result.Processed(response);
                }
            }
            else // if requote is yes
            {
                var response = GetNewRatings(dto, m_SystemChannels);
                result.Processed(response);
            }
        }

        private List<QuoteHeader> GetQuotesByProposalId(int proposalId)
        {
            return m_Repository.GetAll<QuoteHeader>().Where(x => x.Proposal.Id == proposalId && x.IsRerated == false).ToList();
        }

        private ProposalQuoteDto GetNewRatings(GetQuotesByProposalDto dto, SystemChannels systemChannels)
        {
            var createQuoteDto = Mapper.Map<CreateQuoteDto>(dto);
            createQuoteDto.SetContext(dto.Context);

            if (Guid.Empty == createQuoteDto.RequestId)
            {
                createQuoteDto.RequestId = Guid.NewGuid();
            }

            var quoteHeader = QuoteHeaderBuilder.GetQuoteHeader(createQuoteDto, m_ExecutionPlan, m_Repository, m_Result, systemChannels, m_ContextProvider.Get().ActiveChannelId);

            if (quoteHeader == null && m_Result.AllErrorDTOMessages.Any())
            {
                return null;
            }

            //The QuoteHeaderState not saving is fixed on the quoteHeaderMapping ... exaclty where it belongs ... dont change this please 
            RaiseEvents(dto, quoteHeader, m_ContextProvider.Get().ActiveChannelSystemId);

            m_Repository.Save(quoteHeader);
            SetLeadStatusAsQuoted(quoteHeader);


            return quoteHeader.MaptoProposalQuoteDto(m_Repository, dto.Context.UserId, dto.ChannelId);
        }

        private void RaiseEvents(GetQuotesByProposalDto dto, QuoteHeader quoteHeader, Guid SystemId)
        {
            quoteHeader.RaiseEvent(new QuoteCreatedEvent(quoteHeader.Proposal.LeadActivity.Lead.Party.Id, quoteHeader,
                dto.Context.UserId));


            quoteHeader.RaiseEvent(new EditAuditLeadLogEvent(new EditAuditLeadLogDto() { LeadId = quoteHeader.Proposal.LeadActivity.Lead.Id, Timestamp = DateTime.UtcNow, IsQuoted = 1,
                SystemId = SystemId,
                EnvironmentType = new AdminConfigurationReader().EnvironmentTypeKey
            }));

            var source = string.Empty;
            if (m_ContextProvider.Get().RequestHeaders.ContainsKey("source"))
            {
                source = m_ContextProvider.Get().RequestHeaders["source"].FirstOrDefault();
            }
                

            if (source.Equals("aig-widget"))
            {
                //Business rule from aig if source == widget then call center must ReQuote
                quoteHeader.Proposal.ShouldReQuote = true;

                var proposals =
                    m_Repository.GetAll<ProposalHeader>().Where(x => x.Id == dto.Id)
                    .ToList();

                //If quote from digital and funeral the addional members needs to be reevaluated 
                var firstOrDefault = proposals.FirstOrDefault();
                if (firstOrDefault != null && firstOrDefault.ProposalDefinitions.Any(item => item.Cover.Id == 142))
                    quoteHeader.Proposal.ValidateProposals = true;

                var quote = quoteHeader.Quotes.OrderByDescending(x => x.Fees).FirstOrDefault();

                if (quote == null)
                {
                    Log.DebugFormat("no quotes returned -- no need to raise sf event for proposal header  {0}", dto.Id);
                    return;
                }

                var individual =
                    m_Repository.GetAll<Individual>().FirstOrDefault(a => a.Id == quoteHeader.Proposal.Party.Id);

                //determine Quote status
                var quoteStatus = QuoteStatus(quote);

                quoteHeader.RaiseEvent(new SalesForceEntryEvent(quoteStatus, individual, dto.CmpidSource, quote));
            }

            Log.DebugFormat("Source: {0} , ShouldRequote: {1}", source, quoteHeader.Proposal.ShouldReQuote);
        }

        private static string QuoteStatus(Quote quote)
        {
            var quoteStatus = "Quote Level";
            var refer =
                quote.Items.Select(a => a.QuoteItemState)
                    .SelectMany(a => a.Entries)
                    .ToList()
                    .Any(a => a.Message.ToLower().Contains("refer"));
            if (refer)
                quoteStatus = "UW Referral";
            var decline =
                quote.Items.Select(a => a.QuoteItemState)
                    .SelectMany(a => a.Entries)
                    .ToList()
                    .Any(a => a.Message.ToLower().Contains("decline"));
            if (decline)
                quoteStatus = "Declined";
            return quoteStatus;
        }

        private void SetLeadStatusAsQuoted(QuoteHeader quoteHeader)
        {
            var lead = m_Repository.GetAll<Lead>().FirstOrDefault(x => x.Party.Id == quoteHeader.Proposal.Party.Id);

            if (lead == null)
            {
                return;
            }
                
            var proposalactivity = quoteHeader.Proposal.LeadActivity;

            var activityProxy = proposalactivity as INHibernateProxy;

            if (activityProxy != null)
            {
                proposalactivity = (ProposalLeadActivity)activityProxy.HibernateLazyInitializer.GetImplementation();
            }

            lead.Quoted(new User(m_ContextProvider.Get().UserId), quoteHeader, proposalactivity.Campaign);

            m_Repository.Save(lead);
        }
    }
}