﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Proposals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace Domain.Proposals.Validations
{
    public class ProposalHeaderValidation : IValidateDto<GetQuotesByProposalDto>
    {
        private readonly IRepository repository;
        public ProposalHeaderValidation(IRepository repository)
        {
            this.repository = repository;
        }

        public void Validate(GetQuotesByProposalDto dto, ExecutionResult result)
        {
            var proposalheader = repository.GetById<ProposalHeader>(dto.Id);

            if(proposalheader == null)
                result.AddValidationMessage(ProposalValidationMessages.ProposalIdInvalid.AddParameters(new[] { dto.Id.ToString() }));
        }
    }
}
