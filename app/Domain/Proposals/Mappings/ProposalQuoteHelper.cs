﻿using System;
using AutoMapper;
using Domain.Base.Repository;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Proposals;
using System.Collections.Generic;
using System.Linq;
using Domain.Products;
using iPlatform.Api.DTOs.Base;
using Domain.Party.ProposalDefinitions;
using Domain.Users.Discounts;
using iPlatform.Api.DTOs.Products;
using Domain.PolicyBindings;

namespace Domain.Proposals.Mappings
{
    public static class ProposalQuoteHelper
    {
        private static IRepository m_Repository;
        private static int m_UserId;
        private static int m_ChannelId;
        public static ProposalQuoteDto MaptoProposalQuoteDto(this QuoteHeader quote, IRepository repository, int userId, int channelId)
        {
            m_Repository = repository;
            m_UserId = userId;
            m_ChannelId = channelId;

            var proposalDefitinitions =
                repository.GetAll<ProposalDefinition>().Where(x => x.ProposalHeader.Id == quote.Proposal.Id).ToList();

            var benefits = repository.GetAll<ProductBenefit>().ToList();
            var productAdditionalExcess = repository.GetAll<ProductAdditionalExcess>().ToList();
            var userdiscounts = m_Repository.GetAll<UserDiscountCoverDefinition>().Where(x => x.IsDeleted != true && x.User.Id == m_UserId).ToList();
            var model = new ProposalQuoteDto();


            model.ProposalHeader = Mapper.Map<ProposalHeaderDto>(quote.Proposal);
            model.State = Mapper.Map<QuoteHeaderStateDto>(quote.State);
            model.Lead = Mapper.Map<ProposalQuoteLeadDto>(quote.Proposal.LeadActivity != null ? quote.Proposal.LeadActivity.Lead : null);
            model.Policies = quote.Quotes.MaptoListQuotePolicyDto(repository, benefits, proposalDefitinitions, productAdditionalExcess, userdiscounts);


            var proposalDefinition = proposalDefitinitions.FirstOrDefault();
            var dateDiffernce = DateTime.UtcNow - model.ProposalHeader.RatingDate.Value;
            if (proposalDefinition != null)
            {
                model.ProposalHeader.QuoteExpirationDays = proposalDefinition.Product.QuoteExpiration;
            }

            if (dateDiffernce != null && model.ProposalHeader.QuoteExpirationDays != 0)// 0 QuoteExpirationDays means the quote does not expire
            {
                model.ProposalHeader.QuoteExpired = dateDiffernce.Value.TotalDays > model.ProposalHeader.QuoteExpirationDays;
            }

            return model;
        }


        private static bool IsPartialQuote(List<QuoteItem> quoteItems, List<ProposalDefinition> proposalDefinitions)
        {
            foreach (var item in quoteItems)
            {
                //check if all proposal items have been quoted
                if (quoteItems.Count(x => x.IsVap == false) != proposalDefinitions.Count())
                {
                    return true;
                }
                    
                //if all items have errors, no partial quote
                if (quoteItems.All(x => x.QuoteItemState.Errors))
                {
                    return false;
                }
                    
                //if one of the items have errors, then partial quote
                if (quoteItems.Any(x => x.QuoteItemState.Errors))
                {
                    return true;
                }
            }
            return false;
        }


        private static List<ProposalQuotePolicyDto> MaptoListQuotePolicyDto(this IList<Quote> quotes, IRepository repository,
            List<ProductBenefit> benefits, List<ProposalDefinition> proposalDefinitions, List<ProductAdditionalExcess> productAdditionalExcess, List<UserDiscountCoverDefinition> userdiscounts)
        {
            var result = new List<ProposalQuotePolicyDto>();

            var policybindings = m_Repository.GetAll<PolicyBinding>().Where(x => x.Channel.Id == m_ChannelId).ToList();

            foreach (Quote quote in quotes)
            {
                var item = quote.MaptoQuotePolicyDto(benefits, proposalDefinitions, productAdditionalExcess, userdiscounts, policybindings);

                if (item != null)
                {
                    result.Add(item);
                }
                    
            }
            return result;
        }

        private static ProposalQuotePolicyDto MaptoQuotePolicyDto(this Quote quote,
            List<ProductBenefit> benefits, List<ProposalDefinition> proposalDefinitions, List<ProductAdditionalExcess> productAdditionalExcess, 
            List<UserDiscountCoverDefinition> userdiscounts, List<PolicyBinding> policyBindings)
        {
            if (quote == null)
            {
                return null;
            }
            var item = new ProposalQuotePolicyDto();

            item.Fees = new MoneyDto(quote.Fees);
            item.QuoteId = quote.Id;
            item.State = Mapper.Map<QuoteStateDto>(quote.State ?? new QuoteState(quote));
            item.Product = Mapper.Map<ProposalQuoteProductDto>(quote.Product);
            item.InsurerReference = quote.InsurerReference;
            item.IsAccepted = quote.IsAccepted;
            item.Items = quote.Items.MaptoListQuotePolicyItemDto(benefits, proposalDefinitions, productAdditionalExcess, userdiscounts, policyBindings);
            item.IsPartialQuote = IsPartialQuote(quote.Items.ToList(), proposalDefinitions);
            item.UploadLog = quote.QuoteUploadLogs.Any() ? Mapper.Map<QuoteUploadLogDto>(quote.QuoteUploadLogs.First()) : null;
            
            return item;
        }

        private static List<ProposalQuotePolicyItemDto> MaptoListQuotePolicyItemDto(this IList<QuoteItem> quoteItems,
            List<ProductBenefit> benefits, List<ProposalDefinition> proposalDefinitions, List<ProductAdditionalExcess> productAdditionalExcess, 
            List<UserDiscountCoverDefinition> userdiscounts, List<PolicyBinding> policyBindings)
        {
            var result = new List<ProposalQuotePolicyItemDto>();

            foreach (QuoteItem quoteItem in quoteItems)
            {
                var item = quoteItem.MaptoQuotePolicyItemDto(benefits, proposalDefinitions, productAdditionalExcess, userdiscounts, policyBindings);

                if (item != null)
                {
                    result.Add(item);
                } 
            }
            return result;
        }

        private static ProposalQuotePolicyItemDto MaptoQuotePolicyItemDto(this QuoteItem quoteItem,
            List<ProductBenefit> benefits, List<ProposalDefinition> proposalDefinitions, List<ProductAdditionalExcess> productAdditionalExcess, 
            List<UserDiscountCoverDefinition> userdiscounts, List<PolicyBinding> policyBindings)
        {
            var item = new ProposalQuotePolicyItemDto();
            if (quoteItem == null)
            {
                return null;
            }
                

            //query to db removed due to performance issues.
            var proposal = proposalDefinitions
                    .FirstOrDefault(a => a.Asset != null && (a.Asset.AssetNo == quoteItem.Asset.AssetNumber || a.Id == quoteItem.Asset.AssetNumber));

            if (proposal == null && !quoteItem.IsVap)
            {
                return null;
            }
                

            //query to db removed due to performance issues.
            List<ProductBenefit> benefitList = benefits
                .Where(b => b.CoverDefinition.Id == quoteItem.CoverDefinition.Id)
                .ToList();

            //changes for IPL-624 Vaps
            item.PopulateItemAssetDescription(quoteItem, proposal);

            item.PopulateItemClauses(quoteItem);

            var additionalExcess = productAdditionalExcess.Where(x => x.CoverDefinition.Id == quoteItem.CoverDefinition.Id).ToList();
            item.AdditionalExcess = additionalExcess.Any() ? Mapper.Map<List<ProductAdditionalExcessDto>>(additionalExcess) : new List<ProductAdditionalExcessDto>();
            item.AssetNumber = quoteItem.Asset.AssetNumber;
            item.Benefits = Mapper.Map<List<ProductBenefitDto>>(benefitList);
            item.Cover = quoteItem.CoverDefinition.Cover;
            item.CoverDefinition = Mapper.Map<CoverDefinitionDto>(quoteItem.CoverDefinition);
            item.State = Mapper.Map<QuoteItemStateDto>(quoteItem.QuoteItemState);
            item.Excess = Mapper.Map<QuoteItemExcessDto>(quoteItem.Excess);
            item.FailureReason = string.Empty;
            item.OriginalPremium = quoteItem.QuoteItemState.Errors ? new MoneyDto(0) : new MoneyDto(quoteItem.Fees.OriginalPremium);
            item.Premium = quoteItem.QuoteItemState.Errors ? new MoneyDto(0) : new MoneyDto(quoteItem.Fees.Premium);
            item.DiscountedPercentage = quoteItem.QuoteItemState.Errors ? new MoneyDto(0) : new MoneyDto(quoteItem.Fees.DiscountedPercentage);
            item.DiscountedPremium = quoteItem.QuoteItemState.Errors ? new MoneyDto(0) : new MoneyDto(quoteItem.Fees.DiscountedPremium);
            item.Sasria = quoteItem.QuoteItemState.Errors ? new MoneyDto(0) : new MoneyDto(quoteItem.Fees.Sasria);
            item.CalculatedSasria = quoteItem.QuoteItemState.Errors ? new MoneyDto(0) : new MoneyDto(quoteItem.Fees.CalculatedSasria);
            item.SasriaShortfall = quoteItem.QuoteItemState.Errors ? new MoneyDto(0) : new MoneyDto(quoteItem.Fees.SasriaShortfall);
            item.SumInsured = new MoneyDto(quoteItem.Asset.SumInsured);
            item.VoluntaryExcess = new MoneyDto(quoteItem.Excess.VoluntaryExcess);
            item.BreakDown = Mapper.Map<List<QuoteItemBreakDownDto>>(quoteItem.QuoteItemBreakDowns);
            item.SumInsuredDescription = quoteItem.SumInsuredDescription;

            //policybinging validation IPL-1099
            item.HasPolicyBindingConfiguration = policyBindings.Any(x => x.CoverDefinition.Product.Id == quoteItem.CoverDefinition.Product.Id);

            var coverId = quoteItem.CoverDefinition.Cover.Id;
            var productId = quoteItem.CoverDefinition.Product.Id;

            var discount = userdiscounts.Where(x => x.CoverDefinition.Product.Id == productId
                               && x.CoverDefinition.Cover.Id == coverId);


            if (discount.Any())
            {
                item.MaxAllowedDiscount = discount.FirstOrDefault().Discount;
                item.MaxAllowedLoad = discount.FirstOrDefault().Load;
                item.ItemMaxDiscount = discount.FirstOrDefault().ItemMaxDiscount;
                item.QuoteMaxDiscount = discount.FirstOrDefault().QuoteMaxDiscount;
            }

            return item;
        }

        public static void PopulateItemAssetDescription(this ProposalQuotePolicyItemDto item, QuoteItem quoteItem, ProposalDefinition proposal)
        {
            if (quoteItem.IsVap)
            {
                item.AssetDescription = quoteItem.Description;
                item.Description = quoteItem.Description;
            }
            else
            {
                var asset = proposal.Asset;

                if (asset != null && asset.Description == null && asset.Party == null)
                {
                    item.AssetDescription = asset.Description;
                    item.Description = asset.Description;
                }
                else
                {
                    item.AssetDescription = proposal.Description;
                    item.Description = proposal.Description;
                }
            }
        }

        public static void PopulateItemClauses(this ProposalQuotePolicyItemDto item, QuoteItem quoteItem)
        {
            if (!string.IsNullOrEmpty(quoteItem.Clauses))
            {
                item.Clauses = quoteItem.GetClauses();
            }
        }
    }
}
