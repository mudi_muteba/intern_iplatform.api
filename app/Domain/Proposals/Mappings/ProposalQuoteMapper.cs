﻿using AutoMapper;
using Domain.Leads;
using Domain.Party.Quotes;
using Domain.Products;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Proposals;
using iPlatform.Api.DTOs.Ratings.Quoting;
using System.Collections.Generic;

namespace Domain.Proposals.Mappings
{
    public class ProposalQuoteMapper : Profile
    {

        protected override void Configure()
        {
            Mapper.CreateMap<Lead, ProposalQuoteLeadDto>()
                .ForMember(t => t.LeadName, o => o.MapFrom(s => s.Party.DisplayName))
                .ForMember(t => t.LeadId, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.PartyId, o => o.MapFrom(s => s.Party.Id))
                ;

            Mapper.CreateMap<Product, ProposalQuoteProductDto>()
                .ForMember(t => t.InsurerCode, o => o.MapFrom(s => s.ProductProvider.Code))
                .ForMember(t => t.InsurerName, o => o.MapFrom(s => s.ProductProvider.DisplayName))
                .ForMember(t => t.CaptureLeadTransferComment, o => o.MapFrom(s => s.ProductProvider.CaptureLeadTransferComment))
                .ForMember(t => t.ProductCode, o => o.MapFrom(s => s.ProductCode))
                .ForMember(t => t.ProductName, o => o.MapFrom(s => s.Name))
                .ForMember(t => t.Description, o => o.MapFrom(s => s.ProductOwner.Description))
                .ForMember(t => t.IsSelectedExcess, o => o.MapFrom(s => s.IsSelectedExcess))
                .ForMember(t => t.IsVoluntaryExcess, o => o.MapFrom(s => s.IsVoluntaryExcess))
                .ForMember(t => t.BenefitAcceptanceCheck, o => o.MapFrom(s => s.BenefitAcceptanceCheck))
                .ForMember(t => t.HideSasriaLine, o => o.MapFrom(s => s.HideSasriaLine))
                .ForMember(t => t.HideCompareButton, o => o.MapFrom(s => s.HideCompareButton))
                ;

            Mapper.CreateMap<QuoteUploadLog, QuoteUploadLogDto>()
                .ForMember(t => t.Premium, o => o.MapFrom(s => new MoneyDto(s.Premium)))
                .ForMember(t => t.Fees, o =>  o.MapFrom(s => new MoneyDto(s.Fees)))
                .ForMember(t => t.Sasria, o =>  o.MapFrom(s => new MoneyDto(s.Sasria)))
                .ForMember(t => t.IsAvailable, o => o.Ignore())
                ;

            Mapper.CreateMap<GetQuotesByProposalDto, CreateQuoteDto>()
                .ForMember(t => t.IndividualId, o => o.Ignore())
                .ForMember(t => t.ChannelId, o => o.MapFrom(s => s.ChannelId))
                .ForMember(t => t.ProposalHeaderId, o => o.MapFrom(s => s.Id))
                ;

        }
    }
}
