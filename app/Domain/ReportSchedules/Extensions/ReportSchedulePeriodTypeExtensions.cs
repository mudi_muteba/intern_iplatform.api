﻿using System;
using iPlatform.Enums.ReportSchedules;

namespace Domain.ReportSchedules.Extensions
{
    public static class ReportSchedulePeriodTypeExtensions
    {
        public static DateTime? GetStartDate(this ReportSchedulePeriodType periodType, DateTime endDate)
        {
            if (endDate == new DateTime()) endDate = DateTime.Today.AddDays(1).AddMinutes(-1);
            switch (periodType)
            {
                case ReportSchedulePeriodType.CurrentDay:
                    return DateTime.Today;
                case ReportSchedulePeriodType.CurrentWeek:
                    return endDate.AddDays(DayOfWeek.Monday - endDate.DayOfWeek);
                case ReportSchedulePeriodType.CurrentMonth:
                    return new DateTime(endDate.Year, endDate.Month, 1);
                case ReportSchedulePeriodType.CurrentYear:
                    return new DateTime(endDate.Year, 1, 1);
                case ReportSchedulePeriodType.PreviousMonth:
                    return endDate.AddMonths(-1);
                case ReportSchedulePeriodType.Previous3Months:
                    return endDate.AddMonths(-3);
            }
            return null;
        }
    }
}