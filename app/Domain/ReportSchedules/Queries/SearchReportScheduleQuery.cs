﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.ReportSchedules;
using MasterData.Authorisation;

namespace Domain.ReportSchedules.Queries
{
    public class SearchReportScheduleQuery : BaseQuery<ReportSchedule>, IOrderableQuery
    {
        private SearchReportScheduleDto _criteria = new SearchReportScheduleDto();

        public SearchReportScheduleQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<ReportSchedule>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>> { };
            }
        }

        public SearchReportScheduleQuery WithCriteria(SearchReportScheduleDto dto)
        {
            if (dto.OrderBy == null)
                dto.OrderBy = new List<OrderByField>();

            if (!dto.OrderBy.Any())
                dto.OrderBy.Add(new OrderByField
                {
                    Direction = OrderByDirection.Ascending,
                    FieldName = "Name"
                });

            _criteria = dto;
            return this;
        }

        protected internal override IQueryable<ReportSchedule> Execute(IQueryable<ReportSchedule> query)
        {
            if (!string.IsNullOrWhiteSpace(_criteria.Name))
                query = query.Where(u => u.Name.StartsWith(_criteria.Name));

            return query;
        }
    }
}