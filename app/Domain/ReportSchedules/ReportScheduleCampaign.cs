﻿using Domain.Base;
using Domain.Campaigns;

namespace Domain.ReportSchedules
{
    public class ReportScheduleCampaign : Entity
    {
        public virtual ReportSchedule ReportSchedule { get; set; }
        public virtual Campaign Campaign { get; set; }

        public ReportScheduleCampaign() { }

        public ReportScheduleCampaign(ReportSchedule reportSchedule, Campaign campaign)
        {
            ReportSchedule = reportSchedule;
            Campaign = campaign;
        }
    }
}
