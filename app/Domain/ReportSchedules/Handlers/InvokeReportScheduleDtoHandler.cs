using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using AutoMapper;
using Castle.Core.Internal;
using Domain.Admin;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Emailing.Factory;
using Domain.ReportSchedules.Extensions;
using iPlatform.Api.DTOs.ReportSchedules;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Infrastructure.NHibernate.ContractResolvers;
using MasterData.Authorisation;
using Newtonsoft.Json;
using Shared.Extentions;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Parameter = Telerik.Reporting.Parameter;

namespace Domain.ReportSchedules.Handlers
{
    public class InvokeReportScheduleDtoHandler : BaseDtoHandler<InvokeReportScheduleDto, int>
    {
        private readonly IRepository _repository;
        private readonly GetAllChannelsQuery _allChannelsQuery;
        private readonly UriReportSource _uriReportSource = new UriReportSource();
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly string _baseUrl = ConfigurationManager.AppSettings[@"iPlatform/serviceLocator/iPlatform/baseUrl"];
        private readonly string _reportSource = ConfigurationManager.AppSettings[@"Reports/Source"];

        public InvokeReportScheduleDtoHandler(IProvideContext contextProvider, IRepository repository, GetAllChannelsQuery allChannelsQuery) : base(contextProvider)
        {
            _repository = repository;
            _allChannelsQuery = allChannelsQuery;
            _serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new NHibernateContractResolver(),
                Formatting = Formatting.Indented
            };
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void InternalHandle(InvokeReportScheduleDto dto, HandlerResult<int> result)
        {
            var rs = _repository.GetAll<ReportSchedule>().FirstOrDefault(x => x.Name == dto.Name);
            var channel = _allChannelsQuery.ExecuteQuery().FirstOrDefault(x => x.IsDefault);
            var insurerCode = channel.Code;
            var channelId = channel.Id;

            this.Info(() => string.Format("source: {0}, baseUrl: {1}, insurerCode: {2}, channelId: {3}", _reportSource, _baseUrl, insurerCode, channelId));

            SendReportPack(rs, channel, dto.RootPath);

            result.Processed(rs.Id);
        }

        private void SendReportPack(ReportSchedule reportSchedule, Channel channel, string rootPath)
        {
            var emailSettings = Mapper.Map<AppSettingsEmailSetting>(channel.EmailCommunicationSettings.FirstOrDefault());
            var mailMessage = new MailMessage
            {
                Subject = "Scheduled Reports",
                IsBodyHtml = true,
                Body = reportSchedule.Message,
                BodyEncoding = Encoding.UTF8,
                From = new MailAddress(emailSettings.DefaultFrom)
            };

            reportSchedule.Recipients.Select(x => x.Recipient.UserName).ForEach(x => mailMessage.To.Add(x));

            if (reportSchedule.ShouldZip)
                mailMessage.Attachments.Add(new Attachment(Zip(GetReports(reportSchedule, channel, rootPath)), "Reports.zip"));
            else
                GetReports(reportSchedule, channel, rootPath).ForEach(x => mailMessage.Attachments.Add(new Attachment(new MemoryStream(x.Value), x.Key)));

            new SmtpClientAdapter(emailSettings).Send(mailMessage);
        }

        public MemoryStream Zip(IDictionary<string, byte[]> reports)
        {
            var outputMemStream = new MemoryStream();
            var zipStream = new ZipOutputStream(outputMemStream);

            zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

            reports.ForEach(x =>
            {
                zipStream.PutNextEntry(new ZipEntry(x.Key) { DateTime = DateTime.Now });
                StreamUtils.Copy(new MemoryStream(x.Value), zipStream, new byte[4096]);
            });

            zipStream.CloseEntry();
            zipStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
            zipStream.Close();                  // Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;
            return outputMemStream;
        }

        private IDictionary<string, byte[]> GetReports(ReportSchedule reportSchedule, Channel channel, string rootPath)
        {
            var reports = new Dictionary<string, byte[]>();
            foreach (var reportScheduleReport in reportSchedule.Reports)
            {
                this.Info(() => "Rendering report: " + reportScheduleReport.Report.Name);
                //UAP
                var sourceFile = (channel.Code.ToLower() == "uap" && reportScheduleReport.Report.Name.ToLower() == "comparative quote") ? "ComparativeQuote_UAP.trdx" : reportScheduleReport.Report.SourceFile;

                _uriReportSource.Uri = Path.Combine(rootPath, _reportSource, sourceFile);

                AddParam(reportScheduleReport, "baseurl", _baseUrl);
                AddParam(reportScheduleReport, "channelid", channel.Id);
                AddParam(reportScheduleReport, "insurercode", channel.Code);
                AddParam(reportScheduleReport, "organizationcode", channel.Code);
                AddParam(reportScheduleReport, "reportname", reportScheduleReport.Report.Name);
                AddParam(reportScheduleReport, "datefrom", reportSchedule.PeriodType.GetStartDate(DateTime.Today.AddDays(1).AddMinutes(-1)));
                AddParam(reportScheduleReport, "dateto", DateTime.Today.AddDays(1).AddMinutes(-1));
                reportScheduleReport.Report.Parameters.ForEach(p => AddParam(reportScheduleReport, p.Field, reportSchedule[p.Field] != null ? reportSchedule[p.Field].Value : null));

                this.Info(() => "UriReportSource: " + JsonConvert.SerializeObject(_reportSource, _serializerSettings));
                var file = new ReportProcessor().RenderReport(reportSchedule.Format.Key, _uriReportSource, new Hashtable());
                this.Info(() => "Rendered Report File: " + JsonConvert.SerializeObject(file, _serializerSettings));

                HandleErrors(file);

                reports[ reportScheduleReport.Report.Name + reportSchedule.Format.Extension] = file.DocumentBytes;
                this.Info(() => "Finished rendering report: " + reportScheduleReport.Report.Name);
            }
            return reports;
        }

        private void HandleErrors(RenderingResult file)
        {
            if (!file.Errors.Any()) return;
            this.Error(() => JsonConvert.SerializeObject(file.Errors, _serializerSettings));
            throw new Exception(string.Join(@"\r\n\r\n", file.Errors.Select(x => x.ToString())));
        }

        private void AddParam(ReportScheduleReport report, string paramName, object value)
        {
            if (value == null) return;
            var reportParam = report.Report[paramName];
            if (reportParam != null)
                _uriReportSource.Parameters.Add(new Parameter(reportParam.Field, value));
        }
    }
}