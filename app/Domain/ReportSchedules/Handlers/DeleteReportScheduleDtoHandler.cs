using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Hangfire;
using Hangfire.SqlServer;
using iPlatform.Api.DTOs.ReportSchedules;
using MasterData.Authorisation;

namespace Domain.ReportSchedules.Handlers
{
    public class DeleteReportScheduleDtoHandler : ExistingEntityDtoHandler<ReportSchedule, DeleteReportScheduleDto, int>
    {
        public DeleteReportScheduleDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void HandleExistingEntityChange(ReportSchedule entity, DeleteReportScheduleDto dto, HandlerResult<int> result)
        {
            DeleteHangfireJob(entity.HangfireJobId);
            entity.Delete();
            result.Processed(entity.Id);
        }

        private void DeleteHangfireJob(string jobId)
        {
            var conn = "iBrokerConnectionString";
#if DEBUG
            conn = string.Format("{0}-{1}", Environment.MachineName, "iBrokerConnectionString");
#endif
            JobStorage.Current = new SqlServerStorage(conn);
            RecurringJob.RemoveIfExists(jobId);
        }
    }
}