﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Reports.Base;
using Domain.ReportSchedules.HangfireJobs;
using Domain.Users;
using Hangfire;
using Hangfire.SqlServer;
using iPlatform.Api.DTOs.ReportSchedules;
using MasterData;
using MasterData.Authorisation;

namespace Domain.ReportSchedules.Handlers
{
    public class SaveReportScheduleDtoHandler : BaseDtoHandler<SaveReportScheduleDto, int>
    {
        private readonly IRepository _repository;
        private readonly IProvideContext _contextProvider;

        public SaveReportScheduleDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _contextProvider = contextProvider;
            _repository = repository;
        }

        protected override void InternalHandle(SaveReportScheduleDto dto, HandlerResult<int> result)
        {
            var rs = _repository.GetById<ReportSchedule>(dto.Id) ?? new ReportSchedule();
            var campaignIds = dto.Campaigns.Select(x => x.Campaign.Id).ToList();
            var campaigns = _repository.GetAll<Campaign>().Where(x => campaignIds.Contains(x.Id));
            var userIds = dto.Recipients.Select(x => x.Recipient.Id).ToList();
            var users = _repository.GetAll<User>().Where(x => userIds.Contains(x.Id));
            var user = _repository.GetById<User>(_contextProvider.Get().UserId);
            var reportIds = dto.Reports.Select(x => x.Report.Id);
            var reports  = _repository.GetAll<Report>().Where(x => reportIds.Contains(x.Id)).ToArray();

            if (rs.Id == 0)
                rs.Name = dto.Name;
            rs.Format = new ExportTypes().FirstOrDefault(x => x.Id == dto.FormatId);
            rs.Frequency = new FrequencyTypes().FirstOrDefault(x => x.Id == dto.FrequencyId);
            rs.PeriodType = dto.PeriodType;
            rs.WeekDay = dto.WeekDay;
            rs.MonthDay = dto.MonthDay;
            rs.Time = dto.Time;
            rs.Message = dto.Message;
            rs.ShouldZip = dto.ShouldZip;
            rs.Owner = user;
            rs.CreatedOn = DateTime.UtcNow;
            rs.AddCampaign(campaigns.ToArray());
            rs.AddRecipient(users.ToArray());
            rs.DeleteReports();
            dto.ReportScheduleParams.ForEach(x => rs.AddReportParam(x.ReportParam.Field, x.Value, reports));

            _repository.Save(rs);

            Schedule(dto, rs);
        }

        private void Schedule(ReportScheduleDto dto, ReportSchedule rs)
        {
            var cron = "";
            if (rs.Frequency.Name == "Daily")
                cron = string.Format("{0} {1} * * *", dto.Time.Minute, dto.Time.Hour);
            if (rs.Frequency.Name == "Weekly")
                cron = string.Format("{0} {1} * * {2}", dto.Time.Minute, dto.Time.Hour, dto.WeekDay);
            if (rs.Frequency.Name == "Monthly")
                cron = string.Format("{0} {1} {2} * *", dto.Time.Minute, dto.Time.Hour, dto.MonthDay);

            if (!string.IsNullOrEmpty(cron))
            {
                var conn = "iBrokerConnectionString";
#if DEBUG
                conn = string.Format("{0}-{1}", Environment.MachineName, "iBrokerConnectionString");
#endif
                JobStorage.Current = new SqlServerStorage(conn);
                RecurringJob.AddOrUpdate<PublishReportScheduleHangfireJob>(rs.HangfireJobId, x => x.Publish(rs.Name), cron);
            }
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }
    }
}
