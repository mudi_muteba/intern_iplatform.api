﻿using Domain.Base;
using Domain.Users;

namespace Domain.ReportSchedules
{
    public class ReportScheduleRecipient : Entity
    {
        public virtual ReportSchedule ReportSchedule { get; set; }
        public virtual User Recipient { get; set; }

        public ReportScheduleRecipient() { }

        public ReportScheduleRecipient(ReportSchedule reportSchedule, User recipient)
        {
            ReportSchedule = reportSchedule;
            Recipient = recipient;
        }
    }
}
