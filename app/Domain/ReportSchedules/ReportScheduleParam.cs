﻿using Domain.Base;
using Domain.Reports.Base;

namespace Domain.ReportSchedules
{
    public class ReportScheduleParam : Entity
    {
        public virtual ReportParam ReportParam { get; set; }
        public virtual string Value { get; set; }

        public ReportScheduleParam() { }

        public ReportScheduleParam(ReportParam reportParam, string value)
        {
            ReportParam = reportParam;
            Value = value;
        }
    }
}