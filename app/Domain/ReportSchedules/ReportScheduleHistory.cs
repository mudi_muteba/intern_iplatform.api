﻿using Domain.Base;
using Domain.Reports.Base;

namespace Domain.ReportSchedules
{
    public class ReportScheduleHistory : Entity
    {
        public ReportScheduleHistory() { }

        public virtual ReportSchedule ReportSchedule { get; set; }

        public virtual Report Report { get; set; }

        public virtual bool Success { get; set; }

        public virtual string Errors { get; set; }
    }
}
