﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Castle.Core.Internal;
using Domain.Base;
using Domain.Campaigns;
using Domain.Reports.Base;
using Domain.Users;
using iPlatform.Enums.ReportSchedules;
using Infrastructure.NHibernate.Attributes;
using MasterData;

namespace Domain.ReportSchedules
{
    public class ReportSchedule : Entity
    {
        [Required]
        public virtual string Name { get; set; }
        public virtual ExportType Format { get; set; }
        public virtual FrequencyType Frequency { get; set; }
        public virtual ReportSchedulePeriodType PeriodType { get; set; }
        public virtual string WeekDay { get; set; }
        public virtual int MonthDay { get; set; }
        public virtual DateTime? Time { get; set; }
        public virtual string Message { get; set; }
        public virtual bool ShouldZip { get; set; }
        public virtual string ZipPassword { get; set; }
        public virtual DateTime? CreatedOn { get; set; }
        public virtual User Owner { get; set; }
        [DoNotMap]
        public virtual string HangfireJobId
        {
            get { return string.Format("{0}#{1}", GetType().Name, Id); }
        }

        private IList<ReportScheduleReport> _reports;
        public virtual IList<ReportScheduleReport> Reports
        {
            get { return _reports ?? (_reports = new List<ReportScheduleReport>()); }
            set { _reports = value; }
        }

        [DoNotMap]
        public virtual ReportScheduleReport this[Report report]
        {
            get { return Reports.FirstOrDefault(x => x.Report.Id == report.Id && x.Report.IsVisibleInScheduler); }
        }

        private IList<ReportScheduleCampaign> _campaigns;
        public virtual IList<ReportScheduleCampaign> Campaigns
        {
            get { return _campaigns ?? (_campaigns = new List<ReportScheduleCampaign>()); }
            set { _campaigns = value; }
        }
        private IList<ReportScheduleRecipient> _recipients;
        public virtual IList<ReportScheduleRecipient> Recipients
        {
            get { return _recipients ?? (_recipients = new List<ReportScheduleRecipient>()); }
            set { _recipients = value; }
        }
        private IList<ReportScheduleParam> _reportScheduleParams;
        public virtual IList<ReportScheduleParam> ReportScheduleParams
        {
            get { return _reportScheduleParams ?? (_reportScheduleParams = new List<ReportScheduleParam>()); }
            set { _reportScheduleParams = value; }
        }

        [DoNotMap]
        public virtual ReportScheduleParam this[string paramName]
        {
            get { return ReportScheduleParams.FirstOrDefault(x => (x.ReportParam.Field + "").Trim().ToLower() == (paramName + "").Trim().ToLower()); }
        }

        public virtual string AdhocRecipients { get; set; }

        public virtual void AddCampaign(params Campaign[] campaigns)
        {
            Campaigns.ForEach(x => x.IsDeleted = true);
            foreach (var campaign in campaigns)
            {
                var item = Campaigns.FirstOrDefault(m2m => m2m.Campaign.Id == campaign.Id);
                if (item == null)
                    Campaigns.Add(new ReportScheduleCampaign(this, campaign));
                else
                    item.IsDeleted = false;
            }
        }

        public virtual void AddRecipient(params User[] users)
        {
            Recipients.ForEach(x => x.IsDeleted = true);
            foreach (var user in users)
            {
                var item = Recipients.FirstOrDefault(m2m => m2m.Recipient.Id == user.Id);
                if (item == null)
                    Recipients.Add(new ReportScheduleRecipient(this, user));
                else
                    item.IsDeleted = false;
            }
        }

        public virtual void DeleteReports()
        {
            Reports.ForEach(x => x.IsDeleted = true);
        }

        public virtual void AddReportParam(string paramName, string value, params Report[] reports)
        {
            foreach (var report in reports)
            {
                var reportScheduleReport = this[report] ?? new ReportScheduleReport(this, report);
                if (Reports.FirstOrDefault(m2m => m2m.Report.Id == report.Id) == null)
                    Reports.Add(reportScheduleReport);
                else
                    reportScheduleReport.IsDeleted = false;

                var reportScheduleParam = this[paramName] ?? new ReportScheduleParam();
                var reportParam = report[paramName];
                if (reportParam != null)
                {
                    reportScheduleParam.ReportParam = reportParam;
                    reportScheduleParam.Value = value;
                    if (ReportScheduleParams.FirstOrDefault(m2m => m2m.ReportParam.Id == reportParam.Id) == null)
                        ReportScheduleParams.Add(reportScheduleParam);
                }
            }
        }
    }
}
