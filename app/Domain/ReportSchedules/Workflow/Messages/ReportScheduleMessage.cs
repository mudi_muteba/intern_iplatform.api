﻿using Workflow.Messages;

namespace Domain.ReportSchedules.Workflow.Messages
{
    public class ReportScheduleMessage : WorkflowExecutionMessage
    {
         public string Name { get; set; }
    }
}