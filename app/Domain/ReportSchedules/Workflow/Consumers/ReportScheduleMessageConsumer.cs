﻿using Domain.ReportSchedules.Workflow.Messages;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.ReportSchedules;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.ReportSchedules.Workflow.Consumers
{
    public class ReportScheduleMessageConsumer : AbstractMessageConsumer<ReportScheduleMessage>
    {
        private readonly IConnector _connector;

        public ReportScheduleMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector) : base(router, executor)
        {
            _connector = connector;
        }

        public override void Consume(ReportScheduleMessage message)
        {
            _connector.ReportManagement.ReportSchedules.Invoke(new InvokeReportScheduleDto(message.Name));
        }
    }
}