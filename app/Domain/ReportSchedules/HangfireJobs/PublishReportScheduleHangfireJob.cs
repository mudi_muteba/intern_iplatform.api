﻿using Domain.ReportSchedules.Workflow.Messages;
using Workflow.Messages;
using Workflow.Publisher;

namespace Domain.ReportSchedules.HangfireJobs
{
    public class PublishReportScheduleHangfireJob
    {
        public void Publish(string reportName)
        {
            var router = new WorkflowRouter(new WorkflowBus(BusBuilder.CreateRouterBus()));
            router.Publish(new WorkflowRoutingMessage().AddMessage(new ReportScheduleMessage { Name = reportName }));
        } 
    }
}