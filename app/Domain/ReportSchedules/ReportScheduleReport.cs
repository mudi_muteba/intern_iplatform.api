﻿using Domain.Base;
using Domain.Reports.Base;

namespace Domain.ReportSchedules
{
    public class ReportScheduleReport : Entity
    {
        public virtual ReportSchedule ReportSchedule { get; set; }
        public virtual Report Report { get; set; }
        
        public ReportScheduleReport() { }

        public ReportScheduleReport(ReportSchedule reportSchedule, Report report)
        {
            ReportSchedule = reportSchedule;
            Report = report;
        }
    }
}
