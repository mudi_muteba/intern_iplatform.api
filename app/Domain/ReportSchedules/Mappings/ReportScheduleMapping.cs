﻿using AutoMapper;
using Domain.Base.Repository;
using Domain.Reports.Base;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.ReportSchedules;

namespace Domain.ReportSchedules.Mappings
{
    public class ReportScheduleMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ReportScheduleRecipient, ReportScheduleRecipientDto>()
                .ForMember(d => d.ReportSchedule, opt => opt.Ignore());

            Mapper.CreateMap<ReportScheduleParam, ReportScheduleParamDto>();

            Mapper.CreateMap<ReportScheduleReport, ReportScheduleReportDto>()
                .ForMember(d => d.ReportSchedule, opt => opt.Ignore());

            Mapper.CreateMap<ReportScheduleCampaign, ReportScheduleCampaignDto>()
                .ForMember(d => d.ReportSchedule, opt => opt.Ignore());

            Mapper.CreateMap<ReportSchedule, ReportScheduleDto>()
                .ForMember(d => d.AdhocRecipients, opt => opt.Ignore())
                .ForMember(d => d.Context, opt => opt.Ignore());
            Mapper.CreateMap<PagedResults<ReportSchedule>, PagedResultDto<ReportScheduleDto>>();
        }
    }
}