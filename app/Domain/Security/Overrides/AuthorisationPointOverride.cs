﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Security.Overrides
{
    public class AuthorisationPointOverride : IAutoMappingOverride<AuthorisationPoint>
    {
        public void Override(AutoMapping<AuthorisationPoint> mapping)
        {
            mapping.Id().GeneratedBy.Assigned();
            mapping.Map(x => x.Name).Length(512);
        }
    }
}