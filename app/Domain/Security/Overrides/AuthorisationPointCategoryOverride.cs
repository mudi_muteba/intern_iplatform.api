﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Security.Overrides
{
    public class AuthorisationPointCategoryOverride : IAutoMappingOverride<AuthorisationPointCategory>
    {
        public void Override(AutoMapping<AuthorisationPointCategory> mapping)
        {
            mapping.Id().GeneratedBy.Assigned();
            mapping.Map(x => x.Name).Length(512);
        }
    }
}