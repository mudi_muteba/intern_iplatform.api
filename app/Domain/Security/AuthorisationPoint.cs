﻿using Domain.Base;

namespace Domain.Security
{
    public class AuthorisationPoint : Entity
    {
        public virtual string Name { get; set; }
        public virtual AuthorisationPointCategory AuthorisationPointCategory { get; set; }
    }
}