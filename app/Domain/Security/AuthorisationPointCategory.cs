﻿using Domain.Base;

namespace Domain.Security
{
    public class AuthorisationPointCategory : Entity
    {
        public virtual string Name { get; set; }
    }
}