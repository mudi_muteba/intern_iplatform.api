﻿using Domain.Base;
using MasterData;

namespace Domain.Security
{
    public class AuthorisationGroupPoint : Entity
    {
        public virtual AuthorisationPoint AuthorisationPoint { get; set; }
        public virtual AuthorisationGroup AuthorisationGroup { get; set; }
    }
}