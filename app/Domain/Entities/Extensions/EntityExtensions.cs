﻿using System;
using Domain.Base;
using Shared.Extentions;

namespace Domain.Entities.Extensions
{
    public static class EntityExtensions
    {
        public static T ThrowExceptionOnNull<T>(this Entity entity, int id) where T : Entity
        {
            if (entity == null)
            {
                var exception = new Exception(string.Format("{0} cannot be found by Id {1}", typeof(T), id));
                typeof(EntityExtensions).Error(() => exception);
                throw exception;
            }
            return entity as T;
        }
    }
}