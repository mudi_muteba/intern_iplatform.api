﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Lookups.Addresses
{
    public interface IMapToStateProvince
    {
        StateProvince GetStateProvinceByPostCode(string postCode);
        StateProvince GetStateProvinceBySuburb(string postCode);
        IEnumerable<StateProvince> GetStateProvinces(string countryCode);
    }
}
