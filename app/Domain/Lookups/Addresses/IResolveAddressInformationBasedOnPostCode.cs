﻿using Domain.Party.Addresses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Lookups.Addresses
{
    public interface IResolveAddressInformationBasedOnPostCode
    {
        Address Resolve(Address address);
    }
}
