﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Repository;
using Domain.Lookups.PostalCodes;

namespace Domain.Lookups.Addresses
{
    // see https://en.wikipedia.org/wiki/List_of_postal_codes_in_South_Africa
    public class ZAStateProvinceMapper : IMapToStateProvince
    {

        public ZAStateProvinceMapper(IRepository repository)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;
        public StateProvince GetStateProvinceByPostCode(string postCode)
        {
            Func<int, bool> isGauteng = pc => (pc > 0 && pc <= 299) || (pc >= 1400 && pc <= 2199);
            Func<int, bool> isMpumalanga = pc => (pc >= 2200 && pc <= 2499) || (pc >= 1000 && pc <= 1399);
            Func<int, bool> isFreeState = pc => (pc >= 9300 && pc <= 9999);
            Func<int, bool> isNorthernCape = pc => (pc >= 8100 && pc <= 8999);
            Func<int, bool> isWesternCape = pc => (pc >= 6500 && pc <= 8099);
            Func<int, bool> isEasternCape = pc => (pc >= 4731 && pc <= 6499);
            Func<int, bool> isKwaZuluNatal = pc => (pc >= 2900 && pc <= 4730);
            Func<int, bool> isNorthWest = pc => (pc >= 300 && pc <= 499) || (pc >= 2500 && pc <= 2899);
            Func<int, bool> isLimpopo = pc => (pc >= 500 && pc <= 999);

            Func<int, StateProvince> nullAction = pc => StateProvinces.NotSpecified;
            var mappings = new List<Func<int, StateProvince>>
            {
                postcode => isGauteng(postcode) ? StateProvinces.Gauteng : StateProvinces.NotSpecified,
                postcode => isMpumalanga(postcode) ? StateProvinces.Mpumalanga : StateProvinces.NotSpecified,
                postcode => isFreeState(postcode) ? StateProvinces.FreeState : StateProvinces.NotSpecified,
                postcode => isNorthernCape(postcode) ? StateProvinces.NorthernCape : StateProvinces.NotSpecified,
                postcode => isWesternCape(postcode) ? StateProvinces.WesternCape : StateProvinces.NotSpecified,
                postcode => isEasternCape(postcode) ? StateProvinces.EasternCape : StateProvinces.NotSpecified,
                postcode => isKwaZuluNatal(postcode) ? StateProvinces.KwaZuluNatal : StateProvinces.NotSpecified,
                postcode => isNorthWest(postcode) ? StateProvinces.NorthWest : StateProvinces.NotSpecified,
                postcode => isLimpopo(postcode) ? StateProvinces.Limpopo : StateProvinces.NotSpecified,
            };

            var postcodeAsInt = 0;

            if (!int.TryParse(postCode, out postcodeAsInt))
            {
                return StateProvinces.NotSpecified;
            }

            var match = mappings.FirstOrDefault(m => m(postcodeAsInt) != StateProvinces.NotSpecified) ?? nullAction;

            return match(postcodeAsInt);
        }

        public StateProvince GetStateProvinceBySuburb(string suburb)
        {
            var postalCode = _repository.GetAll<PostalCode>().FirstOrDefault(pc => pc.City == suburb || pc.Town == suburb);
            if (postalCode == null)
                return null;

            if (string.IsNullOrWhiteSpace(postalCode.BoxCode) && string.IsNullOrWhiteSpace(postalCode.StreetCode))
                return null;

            var province = GetStateProvinceByPostCode(postalCode.BoxCode);

            return province == StateProvinces.NotSpecified
                ? GetStateProvinceByPostCode(postalCode.StreetCode)
                : province;
        }

        public IEnumerable<StateProvince> GetStateProvinces(string countryCode)
        {
            var country = new Countries().FirstOrDefault(c => c.Code.Equals(countryCode, StringComparison.InvariantCultureIgnoreCase)) ?? Countries.SouthAfrica;

            return new StateProvinces()
                .Where(sp => sp.Country.Id == country.Id);
        }
    }
}
