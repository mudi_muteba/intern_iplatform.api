﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Lookups.Addresses.Overrides
{
    public class SuburbProvinceMappingOverride : IAutoMappingOverride<SuburbProvinceMapping>
    {
        public void Override(AutoMapping<SuburbProvinceMapping> mapping)
        {
            mapping.IgnoreProperty(x => x.IsDeleted);
        }
    }
}