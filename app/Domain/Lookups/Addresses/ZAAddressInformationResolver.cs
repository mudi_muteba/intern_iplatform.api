﻿using Common.Logging;
using Domain.Base.Repository;
using Domain.Party.Addresses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Lookups.Addresses
{
    // see https://en.wikipedia.org/wiki/List_of_postal_codes_in_South_Africa
    public class ZAAddressInformationResolver : IResolveAddressInformationBasedOnPostCode
    {
        private static readonly ILog log = LogManager.GetLogger<ZAAddressInformationResolver>();

        public ZAAddressInformationResolver(IRepository repository)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public Address Resolve(Address address)
        {
            return ResolveProvince(address);
        }

        private Address ResolveProvince(Address address)
        {
            if (string.IsNullOrEmpty(address.Code))
                return address;

            var mapper = new ZAStateProvinceMapper(_repository);

            address.StateProvince = mapper.GetStateProvinceByPostCode(address.Code);

            return address;
        }
    }
}
