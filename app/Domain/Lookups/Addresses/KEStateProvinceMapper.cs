﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Repository;
using Domain.Lookups.PostalCodes;

namespace Domain.Lookups.Addresses
{
    public class KEStateProvinceMapper : IMapToStateProvince
    {

        public KEStateProvinceMapper(IRepository repository)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public StateProvince GetStateProvinceByPostCode(string postCode)
        {
            throw new NotImplementedException();
        }

        public StateProvince GetStateProvinceBySuburb(string suburb)
        {
            var match = _repository.GetAll<SuburbProvinceMapping>()
                .FirstOrDefault(m => m.Country == Countries.Kenya && m.Suburb == suburb);

            return match == null
                ? null
                : match.Province;
        }

        public IEnumerable<StateProvince> GetStateProvinces(string countryCode)
        {
            var country = new Countries().FirstOrDefault(c => c.Code.Equals(countryCode, StringComparison.InvariantCultureIgnoreCase)) ?? Countries.Kenya;

            return new StateProvinces()
                .Where(sp => sp.Country.Id == country.Id);
        }
    }
}
