﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Lookups.Address.GetAddress;
using iPlatform.Api.DTOs.Party.Address;
using MasterData.Authorisation;
using RestSharp;
using RestSharp.Extensions;

namespace Domain.Lookups.Addresses.Handlers.GetAddress
{
    public class GetAddressByPostCodeDtoHandler : BaseDtoHandler<GetAddressByPostCodeDto, List<ListAddressDto>>
    {
        private readonly RestClient _Client;
        private readonly string _ApiKey = "";

        public GetAddressByPostCodeDtoHandler(IProvideContext contextProvider) : base(contextProvider)
        {
            string restClientUrl = ConfigurationManager.AppSettings["GetAddress.io/RestClientURL"];
            if (!String.IsNullOrEmpty(restClientUrl))
            {
                _Client = new RestClient(restClientUrl);
                _ApiKey = ConfigurationManager.AppSettings["GetAddress.io/ApiKey"];
            }
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void InternalHandle(GetAddressByPostCodeDto dto, HandlerResult<List<ListAddressDto>> result)
        {
            if (_Client != null && !String.IsNullOrEmpty(_ApiKey))
            {
                string queryString = String.Format("/v2/uk/{0}?api-key={1}", dto.PostCode, _ApiKey);
                IRestRequest request = CreateRequest(queryString);
                IRestResponse<GetAddressDto> response = _Client.Execute<GetAddressDto>(request);

                int timeout = request.Timeout;

                if (response != null &&
                    response.Data != null)
                {
                    List<string> addresesList = response.Data.Addresses;
                    string description = string.Empty;
                    List<ListAddressDto> listAddressDto = addresesList.Select(a =>
                    {
                        description = a;
                        return a.Split(',');
                    })
                    .Select(s => new ListAddressDto()
                    {
                        Description = description,
                        Line1 = s.ElementAtOrDefault(0).HasValue() ? s[0].Trim() : string.Empty,
                        Line2 = s.ElementAtOrDefault(1).HasValue() ? s[1].Trim() : string.Empty,
                        Line3 = s.ElementAtOrDefault(2).HasValue() ? s[2].Trim() : string.Empty,
                        Line4 = s.ElementAtOrDefault(3).HasValue() ? s[3].Trim() : string.Empty,
                        Locality = s.ElementAtOrDefault(4).HasValue() ? s[4].Trim() : string.Empty,
                        Town = s.ElementAtOrDefault(5).HasValue() ? s[5].Trim() : string.Empty,
                        County = s.ElementAtOrDefault(6).HasValue() ? s[6].Trim() : string.Empty,
                        Longitude = response.Data.Longitude.ToString(CultureInfo.InvariantCulture),
                        Latitude = response.Data.Latitude.ToString(CultureInfo.InvariantCulture),
                        Code = dto.PostCode
                    })
                    .ToList();

                    result.Processed(listAddressDto);
                }
            }
        }

        #region [ Helpers ]

        private IRestRequest CreateRequest(string queryString)
        {
            RestRequest request = new RestRequest(queryString)
            {
                Method = Method.GET,
                RequestFormat = DataFormat.Json
            };

            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            return request;
        }

        #endregion
    }
}
