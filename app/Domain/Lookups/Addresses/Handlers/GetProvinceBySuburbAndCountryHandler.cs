﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Lookups.Address;
using Domain.Base.Execution;
using Domain.Base.Repository;
using MasterData.Authorisation;
using MasterData;

namespace Domain.Lookups.Addresses.Handlers
{
    public class GetProvinceBySuburbAndCountryHandler : BaseDtoHandler<GetProvinceBySuburbDto, AddressLookupDto>
    {
        public GetProvinceBySuburbAndCountryHandler(IProvideContext contextProvider, IWindsorContainer container, IRepository repository)
            : base(contextProvider)
        {
            _container = container;
            _repository = repository;
        }
        private readonly IRepository _repository;
        private readonly IWindsorContainer _container;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void InternalHandle(GetProvinceBySuburbDto dto, HandlerResult<AddressLookupDto> result)
        {
            var mapper = new ToStateProvinceMapperFactory(_container, _repository).Create(dto.CountryCode);
            StateProvince stateProvince = mapper.GetStateProvinceBySuburb(dto.Suburb);

            if (stateProvince != StateProvinces.NotSpecified)
                result.Processed(new AddressLookupDto
                {
                    Id = stateProvince.Id,
                    Name = stateProvince.Name
                });
        }
    }
}
