﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Common.Logging;
using Domain.Base.Repository;

namespace Domain.Lookups.Addresses
{
    public class ToStateProvinceMapperFactory
    {
        private static readonly ILog log = LogManager.GetLogger<ToStateProvinceMapperFactory>();
        public ToStateProvinceMapperFactory(IWindsorContainer container,IRepository repository)
        {
            _container = container;
            _repository = repository;
        }

        private readonly IRepository _repository;
        private static IWindsorContainer _container;
        public IMapToStateProvince Create(string countryCode)
        {
            IMapToStateProvince mapper;
            try
            {
                log.InfoFormat("Retrieving state province mapper for country code {0}", countryCode);
                mapper = _container.Resolve<IMapToStateProvince>(countryCode.ToUpper());
                _container.Release(mapper);
            }
            catch (Exception)
            {
                log.InfoFormat("Unable to retrieve state province mapper, loading default mapper South African Mapper");
                mapper = new ZAStateProvinceMapper(_repository);
            }
            return mapper;
        }
    }
}
