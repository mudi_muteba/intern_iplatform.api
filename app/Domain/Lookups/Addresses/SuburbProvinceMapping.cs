﻿using Domain.Base;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Lookups.Addresses
{
    public class SuburbProvinceMapping : Entity
    {
        public virtual string Suburb { get; set; }
        public virtual Country Country { get; set; }
        public virtual StateProvince Province { get; set; }
    }
}
