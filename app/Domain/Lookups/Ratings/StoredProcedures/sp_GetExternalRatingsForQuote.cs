﻿using Domain.Base.Repository.StoredProcedures;

namespace Domain.Lookups.Ratings.StoredProcedures
{
    public class sp_GetExternalRatingsForQuote : StoredProcedure
    {
        public override string Name
        {
            get
            {
                return "sp_GetExternalRatingsForQuote";
            }
        }

        public sp_GetExternalRatingsForQuote() { }

        public sp_GetExternalRatingsForQuote(int quoteId) : base()
        {
            base.
                SetParameters(new sp_GetExternalRatingsForQuoteStoredProcedureParameters
                {
                    QuoteId = quoteId
                });
        }
    }

    public class sp_GetExternalRatingsForQuoteStoredProcedureParameters : IStoredProcedureCriteria
    {
        public int QuoteId { get; set; }
    }
}