﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.Ratings.Queries;
using Domain.Lookups.Ratings.StoredProcedures;
using Domain.Lookups.VehicleGuide.Queries;
using Domain.Party.Quotes;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Context;
using iGuide.DTOs.Makes;
using iGuide.DTOs.Responses;
using iPlatform.Api.DTOs.Lookups.Ratings;
using iPlatform.Api.DTOs.Reports.AIG.Lead;

namespace Domain.Lookups.Ratings
{
    public class RatingsXmlQuery : BaseVehicleGuideQuery<string>
    {
        private string _search;
        private int _year;
        private IRepository repository;
        private IExecutionPlan executionPlan;
        private RatingSearchDto criteria;

        public RatingsXmlQuery(IProvideContext contextProvider, GetGuideSettingsQuery settingsQuery, IRepository repository, IGuideConnector connector, IExecutionPlan executionPlan) : base(contextProvider, settingsQuery, repository, connector)
        {
            this.repository = repository;
            this.executionPlan = executionPlan;
        }

        protected override string Execute()
        {
            var guideContext = GetContext();

            if (criteria.QuoteId == 0)
                return "Invalid Criteria";

            return GetRatingXml(criteria.QuoteId, criteria.GetRatingRequest);
        }

        public RatingsXmlQuery WithCriteria(RatingSearchDto criteria)
        {
            this.criteria = criteria;
            return this;
        }

        private string GetRatingXml(int quoteId, bool getRequest)
        {
            var ratingXml = getRequest ? GetRequest(quoteId) : GetResponse(quoteId);
            if (!string.IsNullOrEmpty(ratingXml))
                return ratingXml;

            var ratingXmlQueryResult = executionPlan.QueryStoredProcedureObject<
                GetExternalRatingsForQuoteQuery,
                GetExternalRatingsForQuoteDto,
                sp_GetExternalRatingsForQuote,
                GetExternalRatingsForQuoteDto>
                (new sp_GetExternalRatingsForQuote(quoteId))
                .OnSuccess(dtos => dtos)
               .Execute();

            if (ratingXmlQueryResult.Completed && ratingXmlQueryResult.Response != null && ratingXmlQueryResult.Response.Any())
            {
                var result = ratingXmlQueryResult.Response.FirstOrDefault() ?? new GetExternalRatingsForQuoteDto();
                return getRequest ? result.Request : result.Result;
            }
            return "No rating data found for quote id " + quoteId;
        }

        private string GetRequest(int quoteId)
        {
            var quote = repository.GetById<Quote>(quoteId);
            return quote.ExternalRatingRequest;
        }

        private string GetResponse(int quoteId)
        {
            var quote = repository.GetById<Quote>(quoteId);
            return quote.ExternalRatingResult;
        }

    }
}