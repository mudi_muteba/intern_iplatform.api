﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Lookups.Ratings.StoredProcedures;
using Domain.Reports.DigitalSignature.Popi;
using Domain.Reports.DigitalSignature.Popi.Queries.Filters;
using Domain.Reports.DigitalSignature.Popi.StoredProcedures;
using iPlatform.Api.DTOs.Lookups.Ratings;
using MasterData.Authorisation;

namespace Domain.Lookups.Ratings.Queries
{
    public class GetExternalRatingsForQuoteQuery : BaseStoredProcedureQueryObject<GetExternalRatingsForQuoteDto, sp_GetExternalRatingsForQuote>
    {
        public GetExternalRatingsForQuoteQuery(IProvideContext contextProvider, IRepository repository)
        : base(contextProvider, repository, null)
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IList<GetExternalRatingsForQuoteDto> Execute(IList<GetExternalRatingsForQuoteDto> query)
        {
            return query;
        }

      
    }
}
