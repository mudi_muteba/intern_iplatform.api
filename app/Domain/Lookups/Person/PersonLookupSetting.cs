﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base;
using iPerson.Api.DTOs.CompanyInformation;
using iPerson.Api.DTOs.PersonInformation;
using MasterData;

namespace Domain.Lookups.Person
{
    public class PersonLookupSetting : Entity
    {
        private readonly Dictionary<Func<PersonLookupSetting, bool>, Func<string, string>> _mapping = new Dictionary<Func<PersonLookupSetting, bool>, Func<string, string>>()
        {
            {(s) => s.PCubedEnabled, c => c += "PCubed,"},
            {(s) => s.IPRSEnabled, c => c += "IPRS,"},
            {(s) => s.LightStonePropertyEnabled, c => c += "LightStone,"},
            {(s) => s.TransUnionEnabled, c => c += "TransUnion,"},
        };

        protected PersonLookupSetting()
        {

        }

        public PersonLookupSetting(Country country, string apiKey, string email, bool pCubedEnabled, string pCubedUsername, string pCubedPassword, bool iprsEnabled, string iprsUsername, string iprsPassword, bool lightStonePropertyEnabled, string lightStonePropertyUserId, bool transUnionEnabled, string transUnionEndPoint, string transUnionSubscriberCode, string transUnionSecurityCode, bool transUnionGetClaims = false, bool transUnionGetDrivers = false)
        {
            Country = country;
            ApiKey = apiKey;
            Email = email;
            PCubedEnabled = pCubedEnabled;
            PCubedUsername = pCubedUsername;
            PCubedPassword = pCubedPassword;
            IPRSEnabled = iprsEnabled;
            IPRSUsername = iprsUsername;
            IPRSPassword = iprsPassword;
            LightStonePropertyEnabled = lightStonePropertyEnabled;
            LightStonePropertyUserId = lightStonePropertyUserId;
            TransUnionEnabled = transUnionEnabled;
            TransUnionEndPoint = transUnionEndPoint;
            TransUnionSubscriberCode = transUnionSubscriberCode;
            TransUnionSecurityCode = transUnionSecurityCode;
            TransUnionGetClaims = transUnionGetClaims;
            TransUnionGetDrivers = transUnionGetDrivers;
        }


        public PersonLookupSetting(Country country, string apiKey, string email, string companyInformationUsername, string companyInformationPassword)
        {
            Country = country;
            ApiKey = apiKey;
            Email = email;
            CompanyInformationUsername = companyInformationUsername;
            CompanyInformationPassword = companyInformationPassword;
        }

        public virtual Channel Channel { get;  set; }
        public virtual Country Country { get; protected internal set; }
        public virtual string ApiKey { get; protected internal set; }
        public virtual string Email { get; protected internal set; }
        public virtual string InvoiceReference { get; protected internal set; }
        public virtual bool PCubedEnabled { get; protected internal set; }
        public virtual string PCubedUsername { get; protected internal set; }
        public virtual string PCubedPassword { get; protected internal set; }
        public virtual bool IPRSEnabled { get; protected internal set; }
        public virtual string IPRSUsername { get; protected internal set; }
        public virtual string IPRSPassword { get; protected internal set; }
        public virtual bool LightStonePropertyEnabled { get; protected internal set; }
        public virtual string LightStonePropertyUserId { get; protected internal set; }

        public virtual bool TransUnionEnabled { get; protected internal set; }
        public virtual string TransUnionEndPoint { get; protected internal set; }
        public virtual string TransUnionSubscriberCode { get; protected internal set; }
        public virtual string TransUnionSecurityCode { get; protected internal set; }
        public virtual bool TransUnionGetClaims { get; protected internal set; }
        public virtual bool TransUnionGetDrivers { get; protected internal set; }

        public virtual string CompanyInformationUsername { get; protected internal set; }
        public virtual string CompanyInformationPassword { get; protected internal set; }


        public virtual PersonInformationContext CreateContext()
        {
            var matches = _mapping.Where(x => x.Key(this));

            var sources = matches.Aggregate(string.Empty, (current, m) => m.Value(current));

            var customisation = new List<ICustomPersonInformationSourceContext>(CustomIPersonContextFactory.Create(this));

            var context = new PersonInformationContext(Country.Code
                , sources
                , ApiKey
                , Email
                , InvoiceReference
                , customisation);

            return context;
        }

        public virtual CompanyInformationContext CreateCompanyInformationContext()
        {
            var context = new CompanyInformationContext(
                Country.Code
                , String.Empty
                , ApiKey
                , Email
                , InvoiceReference
                );

            context.AddCompanyInformationInput(CompanyInformationUsername);

            return context;
        }
    }

    internal class CustomIPersonContextFactory
    {
        public static IEnumerable<ICustomPersonInformationSourceContext> Create(PersonLookupSetting setting)
        {
            var possible = new List<Func<PersonLookupSetting, ICustomPersonInformationSourceContext>>()
            {
                PCubed,
                IPRS,
                LightStone,
                TransUnion
            };

            return possible.Select(p => p(setting)).Where(settings => settings != null);
        }


        private static CustomPCubedContext PCubed(PersonLookupSetting setting)
        {
            if (!string.IsNullOrWhiteSpace(setting.PCubedUsername) && !string.IsNullOrWhiteSpace(setting.PCubedPassword))
            {
                return new CustomPCubedContext(setting.PCubedUsername, setting.PCubedPassword);
            }

            return null;
        }

        private static CustomIPRSContext IPRS(PersonLookupSetting setting)
        {
            if (!string.IsNullOrWhiteSpace(setting.IPRSUsername) && !string.IsNullOrWhiteSpace(setting.IPRSPassword))
            {
                return new CustomIPRSContext(setting.IPRSUsername, setting.IPRSPassword);
            }

            return null;
        }

        private static CustomLightStonePropertyContext LightStone(PersonLookupSetting setting)
        {
            if (!string.IsNullOrWhiteSpace(setting.LightStonePropertyUserId))
            {
                return new CustomLightStonePropertyContext(setting.LightStonePropertyUserId);
            }

            return null;
        }

        private static CustomTransUnionContext TransUnion(PersonLookupSetting setting)
        {
            if (!string.IsNullOrWhiteSpace(setting.TransUnionEndPoint) && !string.IsNullOrWhiteSpace(setting.TransUnionSubscriberCode) && !string.IsNullOrWhiteSpace(setting.TransUnionSecurityCode))
                return new CustomTransUnionContext(setting.TransUnionEndPoint, setting.TransUnionSubscriberCode, setting.TransUnionSecurityCode, String.Empty, String.Empty, setting.TransUnionGetClaims, setting.TransUnionGetDrivers);

            return null;
        }
    }
}