﻿using System.Linq;
using AutoMapper;
using Domain.Individuals;
using Domain.Leads;
using iPerson.Api.DTOs.PersonInformation;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData;

namespace Domain.Lookups.Person.Mappings
{
    public class PersonMapper : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<IndividualDto, PersonInformationResponseDto>()
                .ForMember(x => x.IDNumber, o => o.MapFrom(l => l.IdentityNo));

            Mapper.CreateMap<PersonInformationResponseDto, IndividualDto>()
                .ForMember(p => p.IdentityNo, o => o.MapFrom(i => i.IDNumber))
                .AfterMap((s, d) => {
                    d.ContactDetail = new ContactDetailDto
                    {
                        Email = s.ContactDetails.EmailAddress1 ?? s.ContactDetails.EmailAddress2 ?? s.ContactDetails.EmailAddress3,
                        Cell = s.ContactDetails.Phone1 ?? s.ContactDetails.Phone2 ?? s.ContactDetails.Phone3,
                    };
                });

        }
    }
}