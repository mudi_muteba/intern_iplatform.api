﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Common.Logging;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;
using iPerson.Api.DTOs.Connector;
using iPerson.Api.DTOs.PersonInformation;
using iPerson.Api.DTOs.Responses;
using iPerson.Shared;
using iPlatform.Api.DTOs.Lookups.Person;
using MasterData;
using MasterData.Authorisation;
using NHibernate.Hql.Ast;

namespace Domain.Lookups.Person.Queries
{
    public class PersonLookupQuery : BaseExternalSystemQuery<PersonInformationResponsesDto>
    {
        private readonly IPersonConnector connector;
        private readonly GetPersonLookupSettingsQuery settingsQuery;
        private PersonLookupRequest request;
        private static readonly ILog log = LogManager.GetLogger<PersonLookupQuery>();

        public PersonLookupQuery(IProvideContext contextProvider
            , GetPersonLookupSettingsQuery settingsQuery
            , IPersonConnector connector) : base(contextProvider)
        {
            this.settingsQuery = settingsQuery;
            this.connector = connector;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    iPersonAuthorisation.PersonLookup
                };
            }
        }

        public PersonLookupQuery WithRequest(PersonLookupRequest request)
        {
            this.request = request;
            return this;
        }

        protected override PersonInformationResponsesDto Execute()
        {

            var channelId = ContextProvider.Get().ActiveChannelId;
            var settings = settingsQuery.ForChannel(channelId).ExecuteQuery();
            var personSetting = settings.FirstOrDefault();
            if (personSetting == null)
            {
                var source = request.Source.Trim().ToLower();

                if (string.IsNullOrEmpty(source.Replace("-", "")))
                {
                    //Fallback to default settings for iGuide.
                    //The billing will still be done against the person invoking iGuide

                    log.WarnFormat(
                        "No settings found for channel id {0} to determine how to invoke iPerson for person lookup. Using default settings.",
                        channelId);
                    personSetting = SetDefaultPersonSettings(ConfigurationReader.Root.DefaultSource);
                }
                else
                {
                    personSetting = SetDefaultPersonSettings(request.Source);
                }
            }


            var context = personSetting.CreateContext();

            var response = connector.PersonInformation.Get(context
                , request.IdNumber
                , request.FirstName
                , request.Surname
                , request.PhoneNumber
                , request.Email,
                propertyId: request.PropertyId);

            return HandleResponseFromPerson(response);
        }

        protected PersonInformationResponsesDto HandleResponseFromPerson(GETResponseDto<PersonInformationResponsesDto> response)
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                if (!response.Response.Results.Any())
                {
                    // this should really happen in iPerson
                    response.Response = null;
                }

                return response.Response;
            }

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorisationException("Downstream system (iPerson) returned Unauthorised");
            }

            var errors = response.Errors.Aggregate(string.Empty, (current, error) => current + string.Format("{0}{1}", error.DefaultMessage, Environment.NewLine));

            throw new Exception(string.Format("Downstream system (iPerson) returned errors. The errors are {0}{1}", Environment.NewLine, errors));
        }

        private PersonLookupSetting SetDefaultPersonSettings(string defaultSource)
        {
            try
            {
                //IPRS Check (Kenya - UAP)
                if (string.Equals(defaultSource, "iprs", StringComparison.InvariantCultureIgnoreCase))
                {
                    return new PersonLookupSetting(Countries.Kenya
                        , ConfigurationReader.Root.ApiKey
                        , ConfigurationReader.Root.Email
                        , false
                        , string.Empty
                        , string.Empty
                        , true
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , false
                        , string.Empty
                        , string.Empty
                        , string.Empty
                        );
                }

                //Lightstone Property Check
                if (string.Equals(defaultSource, "property", StringComparison.InvariantCultureIgnoreCase))
                {

                    return new PersonLookupSetting(Countries.SouthAfrica
                        , ConfigurationReader.Root.ApiKey
                        , ConfigurationReader.Root.Email
                        , false
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , string.Empty
                        , true
                        , Infrastructure.Configuration.ConfigurationReader.PersonSetting.LightStonePropertyUserId
                        , false
                        , string.Empty
                        , string.Empty
                        , string.Empty
                        );
                }

                //Transunion Consumer Check
                if (string.Equals(defaultSource, "consumer", StringComparison.InvariantCultureIgnoreCase))
                {
                    return new PersonLookupSetting(Countries.SouthAfrica
                        , ConfigurationReader.Root.ApiKey
                        , ConfigurationReader.Root.Email
                        , false
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , true
                        , "consumer"
                        , ConfigurationReader.TransUnion.SubscriberCode
                        , ConfigurationReader.TransUnion.SecurityCode
                        );
                }

                //Transunion Insurance Check
                if (string.Equals(defaultSource, "insurance", StringComparison.InvariantCultureIgnoreCase))
                {
                    return new PersonLookupSetting(Countries.SouthAfrica
                        , ConfigurationReader.Root.ApiKey
                        , ConfigurationReader.Root.Email
                        , false
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , true
                        , "insurance"
                        , ConfigurationReader.TransUnion.SubscriberCode
                        , ConfigurationReader.TransUnion.SecurityCode
                        );
                }

                if (string.Equals(defaultSource, "claims", StringComparison.InvariantCultureIgnoreCase))
                {
                    return new PersonLookupSetting(Countries.SouthAfrica
                        , ConfigurationReader.Root.ApiKey
                        , ConfigurationReader.Root.Email
                        , false
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , true
                        , "insurance"
                        , ConfigurationReader.TransUnion.SubscriberCode
                        , ConfigurationReader.TransUnion.SecurityCode
                        , true
                        , false
                        );
                }
                if (string.Equals(defaultSource, "drivers", StringComparison.InvariantCultureIgnoreCase))
                {
                    return new PersonLookupSetting(Countries.SouthAfrica
                        , ConfigurationReader.Root.ApiKey
                        , ConfigurationReader.Root.Email
                        , false
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , string.Empty
                        , false
                        , string.Empty
                        , true
                        , "insurance"
                        , ConfigurationReader.TransUnion.SubscriberCode
                        , ConfigurationReader.TransUnion.SecurityCode
                        , false
                        , true
                        );
                }
            }
            catch (Exception ex)
            {
                log.Error(String.Format("Error getting DefaultSource to determine how to invoke iPerson for person lookup. Reverting to default. Message: {0}", ex.Message), ex);
            }

            //PCubed (Default)
            return new PersonLookupSetting(Countries.SouthAfrica
                  , ConfigurationReader.Root.ApiKey
                  , ConfigurationReader.Root.Email
                  , true
                  , string.Empty
                  , string.Empty
                  , false
                  , string.Empty
                  , string.Empty
                  , false
                  , string.Empty
                  , false
                  , string.Empty
                  , string.Empty
                  , string.Empty
                  );
        }
    }
}