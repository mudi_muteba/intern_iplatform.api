﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using AutoMapper;
using Common.Logging;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Individuals.Queries;
using iPerson.Api.DTOs.Connector;
using iPerson.Api.DTOs.PersonInformation;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Lookups.Person;
using MasterData.Authorisation;


namespace Domain.Lookups.Person.Queries
{
    public class PersonLeadLookupQuery : BaseExternalSystemQuery<IndividualDto>
    {
        private readonly IPersonConnector connector;
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;
        private readonly GetPersonLookupSettingsQuery settingsQuery;
        private PersonLookupRequest request;
        private static readonly ILog log = LogManager.GetLogger<PersonLeadLookupQuery>();


        public PersonLeadLookupQuery(IProvideContext contextProvider
            , GetPersonLookupSettingsQuery settingsQuery
            , IPersonConnector connector, IExecutionPlan executionPlan, IRepository repository) : base(contextProvider)
        {
            this.settingsQuery = settingsQuery;
            this.connector = connector;
            _executionPlan = executionPlan;
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    iPersonAuthorisation.PersonLookup
                };
            }
        }

        public PersonLeadLookupQuery WithRequest(PersonLookupRequest request)
        {
            this.request = request;
            return this;
        }

        protected override IndividualDto Execute()
        {
            if (request == null || string.IsNullOrEmpty(request.IdNumber))
            {
                var message = "Invalid Id number supplied";
                log.WarnFormat(message);
            }

            var criteria = new IndividualSearchDto()
            {
                IdNumber = request.IdNumber
            };

            var individulaQueryResult = _executionPlan
             .Search<SearchIndividualsQuery, Individual, IndividualSearchDto, IndividualDto>(criteria)
             .OnSuccess(results => Mapper.Map<Individual, IndividualDto>(results.FirstOrDefault()))
             .Execute();

            if (individulaQueryResult.Response != null)
            {
                return individulaQueryResult.Response;
            }

            return HandleResponseFromPerson(new GETResponseDto<PersonInformationResponsesDto>());
        }

        protected IndividualDto HandleResponseFromPerson(GETResponseDto<PersonInformationResponsesDto> response)
        {
            if (response.Response == null)
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.Errors.Add(new ResponseErrorMessage("Id number not found",""));
            }

            var errors = response.Errors.Aggregate(string.Empty, (current, error) => current + string.Format("{0}{1}", error.DefaultMessage, Environment.NewLine));

            throw new Exception(string.Format("Downstream system (iPerson) returned errors. The errors are {0}{1}", Environment.NewLine, errors));
        }

    }
}