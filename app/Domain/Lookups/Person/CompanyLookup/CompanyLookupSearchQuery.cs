﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Common.Logging;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;
using Domain.Lookups.Person.Queries;
using iPerson.Api.DTOs.CompanyInformation;
using iPerson.Api.DTOs.CompanyInformation.ResponseDtos;
using iPerson.Api.DTOs.Connector;
using iPerson.Api.DTOs.Responses;
using iPerson.Shared;
using iPlatform.Api.DTOs.Lookups.Person;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Lookups.Person.CompanyLookup
{
    public class CompanyLookupSearchQuery<T> : BaseExternalSystemQuery<CompanyInformationSearchResultCollection<T>>
    {

        private readonly GetPersonLookupSettingsQuery settingsQuery;
        private PersonLookupRequest request;
        private static readonly ILog log = LogManager.GetLogger<PersonLookupQuery>();

        public CompanyLookupSearchQuery(IProvideContext contextProvider, GetPersonLookupSettingsQuery settingsQuery) : base(contextProvider)
        {
            this.settingsQuery = settingsQuery;
        }

        private Func<string, CompanyInformationContext, GETResponseDto<CompanyInformationSearchResultCollection<T>>> CompanyLookupQuery { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    iPersonAuthorisation.PersonLookup
                };
            }
        }

        public CompanyLookupSearchQuery<T> WithRequest(PersonLookupRequest request, Func<string, CompanyInformationContext, GETResponseDto<CompanyInformationSearchResultCollection<T>>> func)
        {
            this.request = request;
            CompanyLookupQuery = func;
            return this;
        }

        protected override CompanyInformationSearchResultCollection<T> Execute()
        {
            var search = request.Search;
            var channelId = ContextProvider.Get().ActiveChannelId;
            var settings = settingsQuery.ForChannel(channelId).ExecuteQuery().FirstOrDefault();

            if (settings == null)
            {
                log.WarnFormat("No settings found for channel id {0} to determine how to invoke iPerson for company lookup", channelId);
                throw new UnauthorisationException("No settings found for the company information lookup. Unauthorised");
            }

            if (string.IsNullOrEmpty(search))
                throw new Exception("Invalid search parameter");

            var context = settings.CreateCompanyInformationContext();
            var collection = CompanyLookupQuery(search, context);
            return HandleResponseFromPerson(collection);
        }

        protected CompanyInformationSearchResultCollection<T> HandleResponseFromPerson(GETResponseDto<CompanyInformationSearchResultCollection<T>> response)
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                if (!response.Response.Results.Any())
                {
                    // this should really happen in iPerson
                    response.Response = null;
                }
                return response.Response;
            }

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorisationException("Downstream system (iPerson) returned Unauthorised");
            }

            var errors = response.Errors.Aggregate(string.Empty, (current, error) => current + string.Format("{0}{1}", error.DefaultMessage, Environment.NewLine));

            throw new Exception(string.Format("Downstream system (iPerson) returned errors. The errors are {0}{1}", Environment.NewLine, errors));
        }
    }
}