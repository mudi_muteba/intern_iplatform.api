using System;
using System.Collections.Generic;
using AutoMapper;
using Common.Logging;
using Domain.Admin;
using Domain.Base;
using Domain.Base.Encryption;
using Domain.Base.Execution;
using iGuide.DTOs.Context;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using Infrastructure.Configuration;
using MasterData;

namespace Domain.Lookups.VehicleGuide
{
    public class VehicleGuideSetting : Entity
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(VehicleGuideSetting));

        public virtual Channel Channel { get; protected internal set; }
        public virtual Country Country { get; protected internal set; }
        public virtual string ApiKey { get; protected internal set; }
        public virtual string Email { get; protected internal set; }
        public virtual bool MMBookEnabled { get; protected internal set; }
        public virtual bool LightstoneEnabled { get; protected internal set; }
        public virtual string LSA_UserName { get; protected internal set; }
        public virtual string LSA_Password { get; protected internal set; }
        public virtual Guid LSA_UserId { get; protected internal set; }
        public virtual Guid LSA_ContractId { get; protected internal set; }
        public virtual Guid LSA_CustomerId { get; protected internal set; }
        public virtual Guid LSA_PackageId { get; protected internal set; }
        public virtual string KeAutoKey { get; protected internal set; }
        public virtual string KeAutoSecret { get; protected internal set; }
        public virtual string Evalue8Username { get; protected internal set; }
        public virtual string Evalue8Password { get; protected internal set; }
        public virtual bool Evalue8Enabled { get; protected internal set; }
        public virtual bool TransUnionAutoEnabled { get; protected internal set; }
        public virtual bool RegCheckUkEnabled { get; protected internal set; }
        public virtual string VehicleValuesPassword { get; protected internal set; }
        public virtual string VehicleValuesHashKey { get; protected internal set; }
        public virtual string VehicleValuesApiKey { get; protected internal set; }
        public virtual string VehicleValuesUserId { get; protected internal set; }
        public virtual string VehicleValuesReportCode { get; protected internal set; }


        protected VehicleGuideSetting()
        {
        }

        public VehicleGuideSetting(Channel channel
            , Country country
            , string apiKey
            , string email
            , bool mmBookEnabled
            , bool lightstoneEnabled
            , string lsaUserName
            , string lsaPassword
            , Guid? lsaUserId
            , Guid? lsaContractId
            , Guid? lsaCustomerId
            , Guid? lsaPackageId
            , string keAutoKey
            , string keAutoSecret
            , string evalue8Username
            , string evalue8Password
            , bool evalue8Enabled
            , bool transUnionAutoEnabled
            , bool regCheckUkEnabled
            , string vehicleValuesPassword
            , string vehicleValuesHashKey
            , string vehicleValuesApiKey
            , string vehicleValuesUserId
            , string vehicleValuesReportCode
            )
        {
            Channel = channel;
            Country = country;
            ApiKey = apiKey;
            Email = email;
            MMBookEnabled = mmBookEnabled;
            LightstoneEnabled = lightstoneEnabled;
            LSA_UserName = lsaUserName;
            LSA_Password = lsaPassword;
            LSA_UserId = lsaUserId.HasValue ? lsaUserId.Value : Guid.Empty;
            LSA_ContractId = lsaContractId.HasValue ? lsaContractId.Value : Guid.Empty;
            LSA_CustomerId = lsaCustomerId.HasValue ? lsaCustomerId.Value : Guid.Empty;
            LSA_PackageId = lsaPackageId.HasValue ? lsaPackageId.Value : Guid.Empty;
            KeAutoKey = keAutoKey;
            KeAutoSecret = keAutoSecret;

            Evalue8Username = evalue8Username;
            Evalue8Password = evalue8Password;
            Evalue8Enabled = evalue8Enabled;

            TransUnionAutoEnabled = transUnionAutoEnabled;

            RegCheckUkEnabled = regCheckUkEnabled;

            VehicleValuesPassword = vehicleValuesPassword;
            VehicleValuesHashKey = vehicleValuesHashKey;
            VehicleValuesApiKey = vehicleValuesApiKey;
            VehicleValuesUserId = vehicleValuesUserId;
            VehicleValuesReportCode = vehicleValuesReportCode;
        }

        public virtual GuideExecutionContext CreateGuideContext(ExecutionContext currentContext)
        {
            var context = new GuideExecutionContext
            {
                API = new APIContext(ApiKey, Email, Channel.SystemId.ToString()),
                Invoicing = new InvoicingContext(currentContext.Username),
                Country = CheckCountryDictionary(Country.Code) //Country.Code == Countries.SouthAfrica.Code ? GuideCountry.ZA : GuideCountry.Unknown,
            };

            Log.DebugFormat("Using channel settings {0}", Channel.Code);

            AddMMBook(context);

            AddLightstoneAuto(context);

            AddKenyaAuto(context);

            AddEvalue8MMBook(context);

            AddTransUnionAuto(context);

            AddRegCheckUk(context);

            return context;
        }

        private void AddKenyaAuto(GuideExecutionContext context)
        {
            if (context.Country == GuideCountry.KE)
            {
                context.Sources.Add(GuideInformationSource.KenyaVehicleLookup);
                context.KenyaVehicleLookupParameters = new KenyaVehicleLookupParameters(KeAutoKey, KeAutoSecret);
            }
        }


        private void AddLightstoneAuto(GuideExecutionContext context)
        {
            if (LightstoneEnabled && context.Country != GuideCountry.KE)
            {
                context.Sources.Add(GuideInformationSource.LightstoneAuto);

                context.LightstoneAutoParameters = new LightstoneAutoParameters(
                    LSA_UserId
                    , LSA_ContractId
                    , LSA_CustomerId
                    , LSA_PackageId
                    , LSA_UserName
                    , LSA_Password
                    );
            }
        }


        private void AddMMBook(GuideExecutionContext context)
        {
            if (MMBookEnabled)
            {
                context.Sources.Add(GuideInformationSource.MMBook);
                context.VehicleValuesParameters = new VehicleValuesParameters(VehicleValuesPassword, VehicleValuesHashKey, VehicleValuesApiKey, VehicleValuesUserId, VehicleValuesReportCode);

            }
        }

        private void AddEvalue8MMBook(GuideExecutionContext context)
        {
            if (Evalue8Enabled)
            {
                context.Sources.Add(GuideInformationSource.Evalue8);
                context.Evalue8LookupParameters = new Evalue8LookupParameters(Evalue8Username, Evalue8Password);
            }
        }

        private void AddTransUnionAuto(GuideExecutionContext context)
        {
            if (TransUnionAutoEnabled)
            {
                context.Sources.Add(GuideInformationSource.TransUnionAutoPrefill);
                context.TransUnionAutoParameters = new TransUnionAutoParameters(LSA_UserName, LSA_Password);
            }
        }


        private void AddRegCheckUk(GuideExecutionContext context)
        {
            if (RegCheckUkEnabled)
            {
                context.Sources.Add(GuideInformationSource.RegCheck);
                context.TransUnionAutoParameters = new TransUnionAutoParameters(LSA_UserName, LSA_Password);
            }
        }

        public static VehicleGuideSetting CreateFallback(ExecutionContext currentContext, Channel channel)
        {

            Guid lsaCustomerId, lsaUserId, lsaContractId, lsaPackageId;

            return new VehicleGuideSetting(
                channel
                , channel.Country
                , ConfigurationReader.GuideConnector.APIKey
                , ConfigurationReader.GuideConnector.Email
                , true
                , true
                , ConfigurationReader.GuideConnector.LsAutoUsername
                , ConfigurationReader.GuideConnector.LsAutoPassword
                , Guid.TryParse(ConfigurationReader.GuideConnector.LsAutoUserId, out lsaUserId) ? lsaUserId : Guid.Empty
                , Guid.TryParse(ConfigurationReader.GuideConnector.LsAutoContractId, out lsaContractId) ? lsaContractId : Guid.Empty
                , Guid.TryParse(ConfigurationReader.GuideConnector.LsAutoCustomerId, out lsaCustomerId) ? lsaCustomerId : Guid.Empty
                , Guid.TryParse(ConfigurationReader.GuideConnector.LsAutoPackageId, out lsaPackageId) ? lsaPackageId : Guid.Empty
                , ConfigurationReader.GuideConnector.KeAutoKey
                , ConfigurationReader.GuideConnector.KeAutoSecret
                , String.Empty
                , String.Empty
                , false
                , false
                , true
                , String.Empty
                , String.Empty
                , String.Empty
                , String.Empty
                , String.Empty
                );
        }

        private GuideCountry CheckCountryDictionary(string value)
        {
            GuideCountry outCountry;

            var result = Enum.TryParse(value, true, out outCountry);
            if (result)
                return outCountry;

            return GuideCountry.Unknown;
        }

        public static VehicleGuideSetting Create(CreateVehicleGuideSettingDto dto)
        {
            var entity = Mapper.Map<VehicleGuideSetting>(dto);
            return entity;
        }

        public virtual void Update(EditVehicleGuideSettingDto dto)
        {
            Mapper.Map(dto, this);
        }

        public virtual void Disable(DisableVehicleGuideSettingDto dto)
        {
            Delete();
        }
    }
}