﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using MasterData.Authorisation;

namespace Domain.Lookups.VehicleGuide.Handlers
{
    public class DisableVehicleGuideSettingHandler : ExistingEntityDtoHandler<VehicleGuideSetting, DisableVehicleGuideSettingDto, int>
    {
        private IRepository _Repository;
        public DisableVehicleGuideSettingHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            _Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void HandleExistingEntityChange(VehicleGuideSetting entity, DisableVehicleGuideSettingDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}
