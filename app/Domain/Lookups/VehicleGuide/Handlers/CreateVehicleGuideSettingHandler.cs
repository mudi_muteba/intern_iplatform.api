﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using MasterData.Authorisation;

namespace Domain.Lookups.VehicleGuide.Handlers
{
    public class CreateVehicleGuideSettingHandler :CreationDtoHandler<VehicleGuideSetting, CreateVehicleGuideSettingDto, int>
    {
        private IRepository _Repository;
        public CreateVehicleGuideSettingHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            _Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void EntitySaved(VehicleGuideSetting entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override VehicleGuideSetting HandleCreation(CreateVehicleGuideSettingDto dto, HandlerResult<int> result)
        {
            Channel channel = _Repository.GetAll<Channel>()
                .FirstOrDefault(c => c.Id == dto.ChannelId);
            if (channel != null)
                dto.CountryId = channel.Country.Id;

            var entity = VehicleGuideSetting.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}
