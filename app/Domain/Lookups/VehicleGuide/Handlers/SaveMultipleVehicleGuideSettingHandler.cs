﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin;
using Domain.Admin.SettingsiRates;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Lookups.VehicleGuide.VehicleGuideSetting;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using MasterData.Authorisation;
using NHibernate;

namespace Domain.Lookups.VehicleGuide.Handlers
{
    public class SaveMultipleVehicleGuideSettingHandler : BaseDtoHandler<SaveMultipleVehicleGuideSettingDto, bool>
    {
        private IRepository _repository;
        private ISession _session;
        public SaveMultipleVehicleGuideSettingHandler(IProvideContext contextProvider, IRepository repository, ISession session)
            : base(contextProvider)
        {
            _repository = repository;
            _session = session;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void InternalHandle(SaveMultipleVehicleGuideSettingDto dto, HandlerResult<bool> result)
        {
            var settings = _repository.GetAll<VehicleGuideSetting>()
                .Where(x => x.Channel.Id == dto.ChannelId && !x.IsDeleted).ToList();

            var channels = _repository.GetAll<Channel>().Where(x => !x.IsDeleted).ToList();

            RemoveOldSettings(dto, settings);
            UpdateExistingSettings(dto, settings, channels);
            AddNewSettings(dto, settings, channels);
            result.Processed(true);
        }

        private void RemoveOldSettings(SaveMultipleVehicleGuideSettingDto dto, List<VehicleGuideSetting> settings)
        {
            var toBeDeleted = settings.Where(t => dto.VehicleGuide.All(y => y.ChannelId != t.Channel.Id)).ToList();

            foreach (var setting in toBeDeleted)
            {
                setting.Delete();
                _repository.Save(setting);
            }
        }
        private void UpdateExistingSettings(SaveMultipleVehicleGuideSettingDto dto, List<VehicleGuideSetting> settings, List<Channel> channels)
        {
            var toBeUpdated = settings.Where(t => dto.VehicleGuide.Any(y => t.Channel.Id == y.ChannelId)).ToList();

            foreach (var setting in toBeUpdated)
            {
                var dtosetting = dto.VehicleGuide.First(x => x.ChannelId.Equals(setting.Channel.Id));

                setting.ApiKey = dtosetting.ApiKey;
                setting.Email = dtosetting.Email;
                setting.MMBookEnabled = dtosetting.MMBookEnabled;
                setting.LightstoneEnabled = dtosetting.LightstoneEnabled;
                setting.LSA_UserName = dtosetting.LSA_UserName;
                setting.LSA_Password = dtosetting.LSA_Password;
                setting.LSA_UserId = dtosetting.LSA_UserId;
                setting.LSA_ContractId = dtosetting.LSA_ContractId;
                setting.LSA_CustomerId = dtosetting.LSA_CustomerId;
                setting.LSA_PackageId = dtosetting.LSA_PackageId;
                setting.Evalue8Username = dtosetting.Evalue8Username;
                setting.Evalue8Password = dtosetting.Evalue8Password;
                setting.Evalue8Enabled = dtosetting.Evalue8Enabled;
                setting.TransUnionAutoEnabled = dtosetting.TransUnionAutoEnabled;
                setting.Channel = channels.FirstOrDefault(x => x.Id == dtosetting.ChannelId);

                _repository.Save(setting);
            }
        }

        private void AddNewSettings(SaveMultipleVehicleGuideSettingDto dto, List<VehicleGuideSetting> settings, List<Channel> channels)
        {
            var newSettings = dto.VehicleGuide.Where(x => settings.All(y => y.Channel.Id != x.ChannelId)).ToList();
            var channel = channels.FirstOrDefault(x => !x.IsDeleted && x.Id == dto.ChannelId);

            foreach (var setting in newSettings)
            {
                var createVehicleGuide = Mapper.Map<CreateVehicleGuideSettingDto>(setting);
                createVehicleGuide.ChannelId = channel.Id;
                var entity = VehicleGuideSetting.Create(createVehicleGuide);
                _repository.Save(entity);
            }
        }
    }
}