using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Details;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class VehicleAboutQuery : BaseVehicleGuideQuery<VehicleDetailsDto>
    {
        private string _mmCode;
        private int _year;

        public VehicleAboutQuery(IProvideContext contextProvider
            , GetGuideSettingsQuery settingsQuery
            , IRepository repository
            , IGuideConnector connector) : base(contextProvider, settingsQuery, repository, connector) { }

        protected override VehicleDetailsDto Execute()
        {
            var guideContext = GetContext();
            var response = Connector.Vehicle.GetAbout(_mmCode, _year, guideContext);

            return HandleResponseFromGuide(response);
        }

        public VehicleAboutQuery ForMMCode(string mmCode)
        {
            _mmCode = mmCode;
            return this;
        }

        public VehicleAboutQuery ForYear(int year)
        {
            _year = year;
            return this;
        }
    }
}