﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Lookups.VehicleGuide.VehicleGuideSetting;
using MasterData.Authorisation;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class SearchVehicleGuideSettingsQuery : BaseQuery<VehicleGuideSetting>, ISearchQuery<SearchVehicleGuideSettingDto>
    {
        private SearchVehicleGuideSettingDto _criteria;

        public SearchVehicleGuideSettingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<VehicleGuideSetting>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchVehicleGuideSettingDto criteria)
        {
            this._criteria = criteria;
        }

        protected internal override IQueryable<VehicleGuideSetting> Execute(IQueryable<VehicleGuideSetting> query)
        {

            if (!string.IsNullOrEmpty(_criteria.ChannelName))
                query = query.Where(x => x.Channel.Name.StartsWith(_criteria.ChannelName));
            return query;
        }
    }
}