﻿using System;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Context;
using iGuide.DTOs.Makes;
using iGuide.DTOs.Responses;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class VehicleMakesQuery : BaseVehicleGuideQuery<MakeSearchResultsDto>
    {
        private string _search;
        private int _year;

        public VehicleMakesQuery(IProvideContext contextProvider
            , GetGuideSettingsQuery settingsQuery
            , IRepository repository
            , IGuideConnector connector) : base(contextProvider, settingsQuery, repository, connector) { }

        protected override MakeSearchResultsDto Execute()
        {
            var guideContext = GetContext();
            var response = PickMethod(guideContext)();

            return HandleResponseFromGuide(response);
        }

        private Func<GETResponseDto<MakeSearchResultsDto>> PickMethod(GuideExecutionContext guideContext)
        {
            Func<GETResponseDto<MakeSearchResultsDto>> method = () => Connector.Makes.Get(guideContext);

            if (_year != 0 && !string.IsNullOrWhiteSpace(_search))
                return () => Connector.Makes.Search(_search, _year, guideContext);

            if (_year != 0)
                return () => Connector.Makes.GetByYear(_year, guideContext);

            if (!string.IsNullOrWhiteSpace(_search))
                return () => Connector.Makes.Search(_search, guideContext);

            return method;
        }

        public VehicleMakesQuery ForYear(int year)
        {
            _year = year;
            return this;
        }

        public VehicleMakesQuery SearchBy(string search)
        {
            _search = search;
            return this;
        }
    }
}