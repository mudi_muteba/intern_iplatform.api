using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Domain.Admin;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Context;
using iGuide.DTOs.Responses;
using MasterData.Authorisation;
using Shared.Extentions;
using ValidationMessages;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public abstract class BaseVehicleGuideQuery<TResult> : BaseExternalSystemQuery<TResult>
    {
        protected readonly IGuideConnector Connector;
        private readonly IRepository _repository;
        protected readonly GetGuideSettingsQuery SettingsQuery;

        protected BaseVehicleGuideQuery(IProvideContext contextProvider
            , GetGuideSettingsQuery settingsQuery
            , IRepository repository
            , IGuideConnector connector) : base(contextProvider)
        {
            SettingsQuery = settingsQuery;
            _repository = repository;
            Connector = connector;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    iGuideAuthorisation.Access
                };
            }
        }

        protected GuideExecutionContext GetContext()
        {
            var executionContext = ContextProvider.Get();
            var firstAvailableChannel = executionContext.ActiveChannelId;
            var channel = _repository.GetById<Channel>(firstAvailableChannel);

            if (channel == null)
                throw new UnauthorisationException(string.Format("No channel associated with the execution context. Provided channel was {0}", firstAvailableChannel));

            return GetContext(executionContext, channel);
        }

        private GuideExecutionContext GetContext(ExecutionContext executionContext, Channel channel)
        {
            var configuration = SettingsQuery
                .WithChannel(channel)
                .ExecuteQuery()
                .FirstOrDefault(x => !x.IsDeleted);

            if (configuration == null)
            {
                // fallback to default settings for iGuide.
                // the billing will still be done against the person invoking iGuide

                this.Warn(() => string.Format("No vehicle guide configuration found for execution context in channel {0}. Will use default", channel.Id));

                configuration = VehicleGuideSetting.CreateFallback(executionContext, channel);
            }

            return configuration.CreateGuideContext(executionContext);
        }

        protected TResult HandleResponseFromGuide(GETResponseDto<TResult> response)
        {
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NotFound)
                return response.Response;

            if (response.StatusCode == HttpStatusCode.Unauthorized)
                throw new UnauthorisationException("Downstream system (iGuide) returned Unauthorised");

            if (response.StatusCode == HttpStatusCode.BadRequest)
                throw new ValidationException(
                    response.Errors.Select(e => new ValidationErrorMessage(e.DefaultMessage, e.MessageKey)).ToArray());

            var errors = response.Errors.Aggregate(string.Empty, (current, error) => current + string.Format("{0}{1}", error.DefaultMessage, Environment.NewLine));

            throw new Exception(string.Format("Downstream system (iGuide) returned errors. The errors are {0}{1}", Environment.NewLine, errors));
        }
    }
}