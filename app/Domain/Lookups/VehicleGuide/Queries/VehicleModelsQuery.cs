using System;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Context;
using iGuide.DTOs.Models;
using iGuide.DTOs.Responses;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class VehicleModelsQuery : BaseVehicleGuideQuery<ModelSearchResultsDto>
    {
        private string _make;
        private string _search;
        private int _year;

        public VehicleModelsQuery(IProvideContext contextProvider
            , GetGuideSettingsQuery settingsQuery
            , IRepository repository
            , IGuideConnector connector) : base(contextProvider, settingsQuery, repository, connector) { }

        protected override ModelSearchResultsDto Execute()
        {
            var guideContext = GetContext();
            var response = PickMethod(guideContext)();

            return HandleResponseFromGuide(response);
        }

        private Func<GETResponseDto<ModelSearchResultsDto>> PickMethod(GuideExecutionContext guideContext)
        {
            Func<GETResponseDto<ModelSearchResultsDto>> method = () => Connector.Models.GetByMake(_make, guideContext);

            if (_year != 0 && !string.IsNullOrWhiteSpace(_search) && !string.IsNullOrWhiteSpace(_make))
                return () => Connector.Models.SearchModelsByMakeAndYear(_make, _year, _search, guideContext);

            if (_year != 0 && !string.IsNullOrWhiteSpace(_make))
                return () => Connector.Models.GetByMakeAndYear(_make, _year, guideContext);

            if (!string.IsNullOrWhiteSpace(_search) && !string.IsNullOrWhiteSpace(_make))
                return () => Connector.Models.SearchModelsByMake(_make, _search, guideContext);

            return method;
        }

        public VehicleModelsQuery ForMake(string make)
        {
            _make = make;
            return this;
        }

        public VehicleModelsQuery ForYear(int year)
        {
            _year = year;
            return this;
        }

        public VehicleModelsQuery SearchBy(string search)
        {
            _search = search;
            return this;
        }
    }
}