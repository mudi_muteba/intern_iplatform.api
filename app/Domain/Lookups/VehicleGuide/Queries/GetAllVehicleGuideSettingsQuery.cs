﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class GetAllVehicleGuideSettingsQuery : BaseQuery<VehicleGuideSetting>
    {
        public GetAllVehicleGuideSettingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultVehicleGuideSettingFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.List
                };
            }
        }
        protected internal override IQueryable<VehicleGuideSetting> Execute(IQueryable<VehicleGuideSetting> query)
        {
            query = query.Where(q => !q.IsDeleted)
                .OrderBy(q => q.Channel.Name);

            return query;
        }
    }
}
