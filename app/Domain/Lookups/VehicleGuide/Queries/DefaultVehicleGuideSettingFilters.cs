﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class DefaultVehicleGuideSettingFilters : IApplyDefaultFilters<VehicleGuideSetting>
    {
        public IQueryable<VehicleGuideSetting> Apply(ExecutionContext executionContext, IQueryable<VehicleGuideSetting> query)
        {
            return query.Where(q => !q.IsDeleted);
        }
    }
}
