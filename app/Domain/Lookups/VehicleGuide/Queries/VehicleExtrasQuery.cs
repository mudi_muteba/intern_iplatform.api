using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Values;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class VehicleExtrasQuery : BaseVehicleGuideQuery<VehiclesOptionalExtrasDto>
    {
        private string _mmCode;
        private int _year;

        public VehicleExtrasQuery(IProvideContext contextProvider
            , GetGuideSettingsQuery settingsQuery
            , IRepository repository
            , IGuideConnector connector) : base(contextProvider, settingsQuery, repository, connector) { }

        protected override VehiclesOptionalExtrasDto Execute()
        {
            var guideContext = GetContext();
            var response = Connector.Vehicle.GetExtras(_mmCode, _year, guideContext);

            return HandleResponseFromGuide(response);
        }

        public VehicleExtrasQuery ForMMCode(string mmCode)
        {
            _mmCode = mmCode;
            return this;
        }

        public VehicleExtrasQuery ForYear(int year)
        {
            _year = year;
            return this;
        }
    }
}