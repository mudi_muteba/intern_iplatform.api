﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Context;
using iGuide.DTOs.Responses;
using iGuide.DTOs.Specifications;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class VehicleSpecificationsQuery : BaseVehicleGuideQuery<VehicleSpecsResultsDto>
    {
        private VehicleSpecificationGetBy _getBy = VehicleSpecificationGetBy.Unknown;
        private string _licensePlate;
        private string _registrationNumber;
        private string _idNumber;

        public VehicleSpecificationsQuery(IProvideContext contextProvider
            , GetGuideSettingsQuery settingsQuery
            , IRepository repository
            , IGuideConnector connector) : base(contextProvider, settingsQuery, repository, connector) { }

        protected override VehicleSpecsResultsDto Execute()
        {
            var guideContext = GetContext();
            var method = FindMethod();
            var response = method(guideContext);
            return HandleResponseFromGuide(response);
        }

        private Func<GuideExecutionContext, GETResponseDto<VehicleSpecsResultsDto>> FindMethod()
        {
            IDictionary<VehicleSpecificationGetBy, Func<GuideExecutionContext, GETResponseDto<VehicleSpecsResultsDto>>> methods = new Dictionary
                <VehicleSpecificationGetBy, Func<GuideExecutionContext, GETResponseDto<VehicleSpecsResultsDto>>>()
            {
                {VehicleSpecificationGetBy.LicensePlateNumber, (context) => Connector.VehicleSpecifications.SearchLicensePlate(_licensePlate, context)},
                {VehicleSpecificationGetBy.RegistrationNumber, (context) => Connector.VehicleSpecifications.SearchRegistrationNumber(_registrationNumber, context)},
                {VehicleSpecificationGetBy.LicensePlateNumberAndIdNumber, (context) => Connector.VehicleSpecifications.SearchLicensePlate(_licensePlate, _idNumber, context)}
            };

            var method = methods.Where(w => w.Key == _getBy).ToList();
            return !method.Any() ? null : method.FirstOrDefault().Value;
        }

        public VehicleSpecificationsQuery ForLicensePlate(string licensePlate)
        {
            _licensePlate = licensePlate;
            _getBy = VehicleSpecificationGetBy.LicensePlateNumber;
            return this;
        }

        public VehicleSpecificationsQuery ForLicensePlateAndIdNumber(string licensePlate, string idNumber)
        {
            _licensePlate = licensePlate;
            _idNumber = idNumber;
            _getBy = VehicleSpecificationGetBy.LicensePlateNumberAndIdNumber;
            return this;
        }

        public VehicleSpecificationsQuery ForRegistrationNumber(string registrationNumber)
        {
            _registrationNumber = registrationNumber;
            _getBy = VehicleSpecificationGetBy.RegistrationNumber;
            return this;
        }

        private enum VehicleSpecificationGetBy
        {
            Unknown,
            LicensePlateNumber,
            RegistrationNumber,
            LicensePlateNumberAndIdNumber
        }
    }
}