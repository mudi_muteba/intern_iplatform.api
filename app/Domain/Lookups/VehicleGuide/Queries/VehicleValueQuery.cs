using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Values;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class VehicleValueQuery : BaseVehicleGuideQuery<VehicleValuesDto>
    {
        private string _mmCode;
        private int _year;

        public VehicleValueQuery(IProvideContext contextProvider
            , GetGuideSettingsQuery settingsQuery
            , IRepository repository
            , IGuideConnector connector) : base(contextProvider, settingsQuery, repository, connector) { }

        protected override VehicleValuesDto Execute()
        {
            var guideContext = GetContext();
            var response = Connector.Vehicle.GetValues(_mmCode, _year, guideContext);

            return HandleResponseFromGuide(response);
        }

        public VehicleValueQuery ForMMCode(string mmCode)
        {
            _mmCode = mmCode;
            return this;
        }

        public VehicleValueQuery ForYear(int year)
        {
            _year = year;
            return this;
        }
    }
}