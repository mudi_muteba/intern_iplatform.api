using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Years;

namespace Domain.Lookups.VehicleGuide.Queries
{
    public class VehicleYearsQuery : BaseVehicleGuideQuery<VehicleYearsDto>
    {
        private string _mmCode;

        public VehicleYearsQuery(IProvideContext contextProvider
            , GetGuideSettingsQuery settingsQuery
            , IRepository repository
            , IGuideConnector connector) : base(contextProvider, settingsQuery, repository, connector) { }

        protected override VehicleYearsDto Execute()
        {
            var guideContext = GetContext();
            var response = Connector.Years.Get(_mmCode, guideContext);

            return HandleResponseFromGuide(response);
        }

        public VehicleYearsQuery ForMMCode(string mmCode)
        {
            _mmCode = mmCode;
            return this;
        }
    }
}