﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Lookups.VehicleGuide.VehicleGuideSetting;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using MasterData;

namespace Domain.Lookups.VehicleGuide.Mappings
{
    public class VehicleGuideSettingMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<VehicleGuideSetting, VehicleGuideSettingDto>();

            Mapper.CreateMap<EditVehicleGuideSettingDto, VehicleGuideSetting>()
                .ForMember(d => d.Channel, o => o.MapFrom(s => new Channel() { Id = s.ChannelId }))
                .ForMember(d => d.Country, o => o.MapFrom(s => new Country() { Id = s.CountryId }));

            Mapper.CreateMap<PagedResults<VehicleGuideSetting>, PagedResultDto<ListVehicleGuideSettingDto>>();

            Mapper.CreateMap<VehicleGuideSetting, ListVehicleGuideSettingDto>();

            Mapper.CreateMap<CreateVehicleGuideSettingDto, VehicleGuideSetting>()
                .ForMember(d => d.Channel, o => o.MapFrom(s => new Channel() { Id = s.ChannelId }))
                .ForMember(d => d.Country, o => o.MapFrom(s => new Country() { Id = s.CountryId }));

            Mapper.CreateMap<List<VehicleGuideSetting>, ListResultDto<ListVehicleGuideSettingDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<PagedResults<VehicleGuideSetting>, PagedResultDto<VehicleGuideSettingDto>>();


            Mapper.CreateMap<SaveVehicleGuideSettingDto, CreateVehicleGuideSettingDto>();
        }
    }
}
