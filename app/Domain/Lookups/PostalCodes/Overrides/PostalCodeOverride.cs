﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Lookups.PostalCodes.Overrides
{
    public class PostalCodeOverride : IAutoMappingOverride<PostalCode>
    {
        public void Override(AutoMapping<PostalCode> mapping)
        {
            mapping.ReadOnly();
        }
    }
}