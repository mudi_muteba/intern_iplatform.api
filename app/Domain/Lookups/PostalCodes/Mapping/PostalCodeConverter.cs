﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Lookups.PostalCodes;

namespace Domain.Lookups.PostalCodes.Mapping
{
    public class PostalCodeConverter
    {
        private readonly Func<PostalCode, int, PostalCodeDto> createFullPostCode = (pc, vi) =>
        {
            // ReSharper disable once ConvertToLambdaExpression
            return new PostalCodeDto
            {
                Town = pc.Town,
                City = pc.City,
                Code = string.IsNullOrEmpty(pc.StreetCode) ? pc.BoxCode : pc.StreetCode,
                VisibleIndex = vi,
                Country = pc.Country
            };
        };

        private readonly Func<PostalCode, int, MasterTypeDto> createSimplified = (pc, vi) =>
        {
            // ReSharper disable once ConvertToLambdaExpression
            return new MasterTypeDto
            {
                Name = string.Format("{0} ({1})", pc.Town, pc.City),
                Code = string.IsNullOrEmpty(pc.StreetCode) ? pc.BoxCode : pc.StreetCode,
                VisibleIndex = vi
            };
        };


        public PagedResultDto<PostalCodeDto> Convert(string searchTerm, PagedResults<PostalCode> source)
        {
            if (source == null)
                return null;

            var target = new PagedResultDto<PostalCodeDto>()
            {
                PageNumber = source.PageNumber,
                TotalCount = source.TotalCount,
                TotalPages = source.TotalPages
            };

            var fixedResults = source.Results
                .Select(pc => ApplyRules(pc, searchTerm, createFullPostCode))
                .Where(pc => pc.VisibleIndex != 0)
                .OrderBy(pc => pc.VisibleIndex)
                .ThenBy(pc => pc.City)
                .ToList();

            target.Results = fixedResults;

            return target;
        }

        public PagedResultDto<MasterTypeDto> ConvertToSimplified(string searchTerm, PagedResults<PostalCode> source)
        {
            if (source == null)
                return null;

            var target = new PagedResultDto<MasterTypeDto>()
            {
                PageNumber = source.PageNumber,
                TotalCount = source.TotalCount,
                TotalPages = source.TotalPages
            };

            var fixedResults = source.Results
                .Select(pc => ApplyRules(pc, searchTerm, createSimplified))
                .Where(pc => pc.VisibleIndex != 0)
                .OrderBy(pc => pc.VisibleIndex)
                .ThenBy(pc => pc.Name)
                .ToList();

            target.Results = fixedResults;

            return target;
        }

        private TReturnType ApplyRules<TReturnType>(PostalCode postalCode, string searchTerm,
            Func<PostalCode, int, TReturnType> create)
        {
            Func<string, bool> equalsSearchTerm =
                s1 => string.Equals(s1, searchTerm, StringComparison.InvariantCultureIgnoreCase);

            Func<string, bool> startsWithSearchTerm =
                s1 => s1.StartsWith(searchTerm, StringComparison.InvariantCultureIgnoreCase);

            Func<bool, Func<TReturnType>, Func<TReturnType>> evaluate = (eval, creation) => eval ? creation : null;

            Func<PostalCode, Func<TReturnType>> townCityExactMatch = pc =>
                evaluate(equalsSearchTerm(pc.Town) && equalsSearchTerm(pc.City), () => create(pc, 1));

            Func<PostalCode, Func<TReturnType>> townOrCityExactMatch = pc =>
                evaluate(equalsSearchTerm(pc.Town) || equalsSearchTerm(pc.City), () => create(pc, 100));

            Func<PostalCode, Func<TReturnType>> townAndCityStartsWith = pc =>
                evaluate(startsWithSearchTerm(pc.Town) && startsWithSearchTerm(pc.City), () => create(pc, 200));

            Func<PostalCode, Func<TReturnType>> townOrCityStartsWith = pc =>
                evaluate(startsWithSearchTerm(pc.Town) || startsWithSearchTerm(pc.City), () => create(pc, 300));

            Func<PostalCode, Func<TReturnType>> catchAll = pc =>
                evaluate(true, () => create(pc, 999));

            var rules = new List<Func<PostalCode, Func<TReturnType>>>
            {
                townCityExactMatch,
                townOrCityExactMatch,
                townAndCityStartsWith,
                townOrCityStartsWith,
            };

            Func<PostalCode, Func<TReturnType>> match = rules.FirstOrDefault(r => r(postalCode) != null) ?? catchAll;

            return match(postalCode)();
        }

    }


}