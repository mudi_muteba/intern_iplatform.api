﻿using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Lookups.PostalCodes;

namespace Domain.Lookups.PostalCodes.Mapping
{
    public class PostalCodeMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PagedResults<PostalCode>, PagedResultDto<PostalCodeDto>>();
            Mapper.CreateMap<PagedResults<PostalCode>, PagedResultDto<MasterTypeDto>>();

            Mapper.CreateMap<PostalCode, PostalCodeDto>()
                .ForMember(t => t.City, o => o.MapFrom(s => s.City))
                .ForMember(t => t.Town, o => o.MapFrom(s => s.Town))
                .ForMember(t => t.Code, o => o.MapFrom(s => string.IsNullOrEmpty(s.StreetCode) ? s.BoxCode : s.StreetCode))
                .ForMember(t => t.Country, o => o.MapFrom(s => s.Country))
                .ForMember(t => t.VisibleIndex, o => o.MapFrom(s => 0))
                ;

            Mapper.CreateMap<PostalCode, MasterTypeDto>()
                .ForMember(t => t.Code, o => o.MapFrom(s => string.IsNullOrEmpty(s.StreetCode) ? s.BoxCode : s.StreetCode))
                .ForMember(t => t.Name, o => o.MapFrom(s => string.Format("{0} ({1})", s.Town, s.City)))
                .ForMember(t => t.VisibleIndex, o => o.MapFrom(s => 0))
                ;
        }
    }
}