﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Lookups.PostalCodes;
using Domain.Base.Execution;
using Domain.Base.Repository;
using MasterData.Authorisation;
using MasterData;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using iPlatform.Api.DTOs.Base;

namespace Domain.Lookups.PostalCodes.Handlers
{
    public class SearchPostCodeV2Handler : BaseDtoHandler<SearchPostCodeDto, List<PostalCodeDto>>
    {
        public SearchPostCodeV2Handler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            this._repository = repository;
        }
        
        private readonly IRepository _repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(SearchPostCodeDto dto, HandlerResult<List<PostalCodeDto>> result)
        {
            List<PostalCodeDto> postalcodes = _repository.GetAll<PostalCode>()
                .Where(CreateQuery(dto.search, dto.Country))
                .ToList()
                .Select(pc => ApplyRules(pc, dto.search, create))
                .Where(pc => pc.VisibleIndex != 0)
                .OrderBy(pc => pc.VisibleIndex)
                .ThenBy(pc => pc.City)
                .ToList();

            result.Processed(postalcodes);
        }

        private static Expression<Func<PostalCode, bool>> CreateQuery(string search, Country country)
        {
            if (new Regex("^[0-9]+$").IsMatch(search))
            {
                return o => (o.BoxCode.StartsWith(search) || o.StreetCode.StartsWith(search));
            }

            return o => (
                o.Country == country &&
                (o.City.Contains(search) || o.Town.Contains(search))
                );
        }
        private TReturnType ApplyRules<TReturnType>(PostalCode postalCode, string searchTerm,
            Func<PostalCode, int, TReturnType> create)
        {
            Func<string, bool> equalsSearchTerm =
                s1 => string.Equals(s1, searchTerm, StringComparison.InvariantCultureIgnoreCase);
            Func<string, bool> startsWithSearchTerm =
                s1 => s1.StartsWith(searchTerm, StringComparison.InvariantCultureIgnoreCase);
            Func<bool, Func<TReturnType>, Func<TReturnType>> evaluate = (eval, creation) => eval ? creation : null;

            Func<PostalCode, Func<TReturnType>> townCityExactMatch = pc =>
                evaluate(equalsSearchTerm(pc.Town) && equalsSearchTerm(pc.City), () => create(pc, 1));

            Func<PostalCode, Func<TReturnType>> townOrCityExactMatch = pc =>
                evaluate(equalsSearchTerm(pc.Town) || equalsSearchTerm(pc.City), () => create(pc, 100));

            Func<PostalCode, Func<TReturnType>> townAndCityStartsWith = pc =>
                evaluate(startsWithSearchTerm(pc.Town) && startsWithSearchTerm(pc.City), () => create(pc, 200));

            Func<PostalCode, Func<TReturnType>> townOrCityStartsWith = pc =>
                evaluate(startsWithSearchTerm(pc.Town) || startsWithSearchTerm(pc.City), () => create(pc, 300));

            Func<PostalCode, Func<TReturnType>> catchAll = pc =>
                evaluate(true, () => create(pc, 999));

            var rules = new List<Func<PostalCode, Func<TReturnType>>>
            {
                townCityExactMatch,
                townOrCityExactMatch,
                townAndCityStartsWith,
                townOrCityStartsWith,
            };

            Func<PostalCode, Func<TReturnType>> match = rules.FirstOrDefault(r => r(postalCode) != null) ?? catchAll;

            var item = match(postalCode)();

            return item;
        }

        private readonly Func<PostalCode, int, PostalCodeDto> create = (pc, vi) =>
        {
            return new PostalCodeDto
            {
                Town =  pc.Town,
                City = pc.City,
                Code = string.IsNullOrEmpty(pc.StreetCode) ? pc.BoxCode : pc.StreetCode,
                Country = pc.Country,
                VisibleIndex = vi
            };
        };
    }
}
