﻿using Domain.Base;
using MasterData;

namespace Domain.Lookups.PostalCodes
{
    public class PostalCode : Entity
    {
        public virtual string Town { get; protected internal set; }
        public virtual string City { get; protected internal set; }
        public virtual string StreetCode { get; protected internal set; }
        public virtual string BoxCode { get; protected internal set; }
        public virtual Country Country { get; protected internal set; }
    }
}