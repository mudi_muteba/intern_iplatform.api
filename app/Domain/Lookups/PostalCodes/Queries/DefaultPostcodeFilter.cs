﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Lookups.PostalCodes.Queries
{
    public class DefaultPostcodeFilter : IApplyDefaultFilters<PostalCode>
    {
        public IQueryable<PostalCode> Apply(ExecutionContext executionContext, IQueryable<PostalCode> query)
        {
            return query.Where(pc => pc.IsDeleted != true);
        }
    }
}