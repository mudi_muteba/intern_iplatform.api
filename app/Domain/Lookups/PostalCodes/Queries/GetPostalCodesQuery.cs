﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Lookups.PostalCodes.Queries
{
    public class GetPostalCodesQuery : BaseQuery<PostalCode>
    {
        private string search;

        public GetPostalCodesQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultPostcodeFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetPostalCodesQuery WithSearchTerm(string search)
        {
            this.search = search;
            return this;
        }

        protected internal override IQueryable<PostalCode> Execute(IQueryable<PostalCode> query)
        {
            if (string.IsNullOrWhiteSpace(search))
            {
                return query
                    .OrderBy(q => q.Town)
                    .ThenBy(q => q.City);
            }

            return query
                .Where(CreateQuery(search));
        }

        private static Expression<Func<PostalCode, bool>> CreateQuery(string search)
        {
            if (new Regex("^[0-9]+$").IsMatch(search))
            {
                return o => (o.BoxCode.StartsWith(search) || o.StreetCode.StartsWith(search));
            }
            return o => (o.City.Contains(search) || o.Town.Contains(search));
        }

    }
}