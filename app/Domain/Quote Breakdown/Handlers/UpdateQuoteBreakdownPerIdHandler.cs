﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.QuoteBreakdown;
using MasterData.Authorisation;

namespace Domain.Quote_Breakdown.Handlers
{
    public class UpdateQuoteBreakdownPerIdHandler : CreationDtoHandler<Party.Quotes.QuoteItemBreakDown, EditQuoteItemBreakDownDto, int>
    {
        private readonly IRepository _repository;

        public UpdateQuoteBreakdownPerIdHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    AddressAuthorisation.Edit
                };
            }
        }

        protected override void InternalHandle(EditQuoteItemBreakDownDto dto, HandlerResult<int> result)
        {


            foreach (var item in dto.Items)
            {
                var entity = _repository.GetAll<QuoteItemBreakDown>().FirstOrDefault(x => x.Id == item.Id);
                if (entity != null)
                {
                    entity.Update(item);
                    _repository.Save(entity);
                }
            }
        }

        protected override void EntitySaved(QuoteItemBreakDown entity, HandlerResult<int> result)
        {
            throw new System.NotImplementedException();
        }

        protected override QuoteItemBreakDown HandleCreation(EditQuoteItemBreakDownDto dto, HandlerResult<int> result)
        {
            throw new System.NotImplementedException();
        }
    } 
    }
