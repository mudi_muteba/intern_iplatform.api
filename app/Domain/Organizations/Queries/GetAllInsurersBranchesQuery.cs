﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Organizations.Queries
{
    public class GetAllInsurersBranchesQuery : BaseQuery<InsurersBranches>
    {
        public GetAllInsurersBranchesQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultInsurersBranchesFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<InsurersBranches> Execute(IQueryable<InsurersBranches> query)
        {
            return query;
        }
    }
}