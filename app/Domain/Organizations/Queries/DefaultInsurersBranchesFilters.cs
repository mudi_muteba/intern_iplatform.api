﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Organizations;

namespace Domain.Organizations.Queries
{
    public class DefaultInsurersBranchesFilters : IApplyDefaultFilters<InsurersBranches>
    {
        public IQueryable<InsurersBranches> Apply(ExecutionContext executionContext, IQueryable<InsurersBranches> query)
        {
            return query
                .Where(c => !c.IsDeleted);
        }
    }
}