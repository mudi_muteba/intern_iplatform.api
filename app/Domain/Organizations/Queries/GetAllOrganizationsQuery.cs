﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Organizations.Queries
{
    public class GetAllOrganizationsQuery : BaseQuery<Organization>, IContextFreeQuery
    {
        public GetAllOrganizationsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Organization>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<Organization> Execute(IQueryable<Organization> query)
        {
            return query;
        }
    }
}