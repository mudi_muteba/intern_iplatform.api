﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Insurers;
using MasterData.Authorisation;

namespace Domain.Organizations.Queries
{
    public class SearchOrganizationsQuery : BaseQuery<Organization>, ISearchQuery<InsurerSearchDto>
    {
        public SearchOrganizationsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Organization>())
        {
        }
        private InsurerSearchDto criteria;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(InsurerSearchDto criteria)
        {
            this.criteria = criteria;
        }

        public SearchOrganizationsQuery SetCriteria(InsurerSearchDto criteria)
        {
            this.criteria = criteria;
            return this;
        }

        protected internal override IQueryable<Organization> Execute(IQueryable<Organization> query)
        {
            if (criteria.Id != null && criteria.Id > 0)
                query = query.Where(a => a.Id == criteria.Id);

            if (!string.IsNullOrWhiteSpace(criteria.Name))
                query = query.Where(a => a.TradingName.StartsWith(criteria.Name));

            if (!string.IsNullOrWhiteSpace(criteria.Code))
                query = query.Where(a => a.Code.StartsWith(criteria.Code));

            return query;
        }
    }
}