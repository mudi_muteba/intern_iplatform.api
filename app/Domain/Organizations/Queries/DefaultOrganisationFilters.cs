﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Organizations.Queries
{
    public class DefaultOrganisationFilters : IApplyDefaultFilters<Organization>
    {
        public IQueryable<Organization> Apply(ExecutionContext executionContext, IQueryable<Organization> query)
        {
            return query.Where(c => c.IsDeleted == false);
        }
    }
}