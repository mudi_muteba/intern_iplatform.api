﻿using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Insurers;
using iPlatform.Api.DTOs.Organisations;

namespace Domain.Organizations.Mapping
{
    public class InsurersBranchesMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<InsurersBranches, InsurersBranchesDto>();
            Mapper.CreateMap<InsurersBranchesDto, InsurersBranches>();

        }
    }
}