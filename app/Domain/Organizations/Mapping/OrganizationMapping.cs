﻿using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Insurers;
using iPlatform.Api.DTOs.Organisations;
using MasterData;
using Domain.Admin;
using System.Collections.Generic;

namespace Domain.Organizations.Mapping
{
    public class OrganizationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Organization, ListOrganizationDto>()
                .ForMember(t => t.Name, o => o.MapFrom(s => s.TradingName))
                ;

            Mapper.CreateMap<Organization, OrganisationDto>()
                .ForMember(t => t.Name, o => o.MapFrom(s => s.TradingName))
                ;
            Mapper.CreateMap<Organization, ListInsurerDto>();

            Mapper.CreateMap<PagedResults<Organization>, PagedResultDto<ListInsurerDto>>();

            Mapper.CreateMap<CreateOrganizationDto, Organization>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId))) // new Channel { Id = s.ChannelId})
                .ForMember(t => t.ContactDetail, o => o.MapFrom(s => s.ContactDetail))
               ;

            Mapper.CreateMap<EditOrganizationDto, Organization>()
               ;

            Mapper.CreateMap<PagedResults<Organization>, PagedResultDto<ListOrganizationDto>>();

            Mapper.CreateMap<List<Organization>, ListResultDto<ListOrganizationDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;
        }
    }
}