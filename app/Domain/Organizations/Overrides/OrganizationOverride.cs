﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Organizations.Overrides
{
    public class OrganizationOverride : IAutoMappingOverride<Organization>
    {
        public void Override(AutoMapping<Organization> mapping)
        {
            mapping.Map(x => x.Code).Length(20);
            mapping.Map(x => x.RegisteredName).Length(500);
            mapping.Map(x => x.TradingName).Length(500);
            mapping.Map(x => x.Description).Length(2000);
            mapping.Map(x => x.RegNo).Length(20);
            mapping.Map(x => x.FspNo).Length(20);
            mapping.Map(x => x.VatNo).Length(20);
            mapping.Map(x => x.QuoteEMailBody).CustomType("StringClob").CustomSqlType("ntext");
            mapping.IgnoreProperty(s => s.ExternalReference);
            mapping.HasMany(s => s.InsurersBranches).Cascade.SaveUpdate();
        }
    }
}