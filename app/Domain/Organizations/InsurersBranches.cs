﻿using Domain.Base;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Organizations
{
    public class InsurersBranches : Entity
    {
        public InsurersBranches() { }
        public virtual string Name { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual InsurersBranchType InsurersBranchType { get; set; }
    }
}
