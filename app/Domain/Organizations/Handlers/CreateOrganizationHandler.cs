﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Organisations;
using MasterData.Authorisation;

namespace Domain.Organizations.Handlers
{
    public class CreateOrganizationHandler : CreationDtoHandler<Organization, CreateOrganizationDto, int>
    {
        private IRepository repository;
        public CreateOrganizationHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    
                };
            }
        }

        protected override void EntitySaved(Organization entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Organization HandleCreation(CreateOrganizationDto dto, HandlerResult<int> result)
        {
            var entity = Organization.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}