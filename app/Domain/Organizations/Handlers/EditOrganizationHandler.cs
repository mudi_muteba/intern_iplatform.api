﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Organisations;
using MasterData.Authorisation;

namespace Domain.Organizations.Handlers
{
    public class EditOrganizationHandler : ExistingEntityDtoHandler<Organization, EditOrganizationDto, int>
    {
        public EditOrganizationHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(Organization entity, EditOrganizationDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }

    }
}