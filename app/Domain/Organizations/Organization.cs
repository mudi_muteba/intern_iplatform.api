﻿using System;
using MasterData;
using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Organisations;

namespace Domain.Organizations
{
    public class Organization : Party.Party
    {
        private string _tradingName;

        public Organization()
        {
            base.PartyType = PartyTypes.Organization;
            InsurersBranches = new List<InsurersBranches>();
        }

        public virtual string Code { get; set; }

        public virtual string TradingName
        {
            get { return _tradingName; }
            set
            {
                _tradingName = value;
                base.DisplayName = _tradingName;
            }
        }

        public virtual DateTime? TradingSince { get; set; }
        public virtual string RegisteredName { get; set; }
        public virtual string Description { get; set; }
        public virtual string RegNo { get; set; }
        public virtual string FspNo { get; set; }
        public virtual string VatNo { get; set; }
        public virtual bool TemporaryFsp { get; set; }
        public virtual string ProfessionalIndemnityInsurer { get; set; }
        public virtual string PiCoverPolicyNo { get; set; }
        public virtual string TypeOfAgency { get; set; }
        public virtual string FgPolicyNo { get; set; }
        public virtual string FidelityGuaranteeInsurer { get; set; }
        public virtual OrganizationType OrganizationType { get; set; }
        public virtual IList<InsurersBranches> InsurersBranches { get; protected set; }
        public virtual bool ShortTermInsurers { get; set; }
        public virtual bool LongTermInsurers { get; set; }
        public virtual string QuoteEMailBody { get; set; }
        public virtual bool CaptureLeadTransferComment { get; set; }
        public static Organization Create(CreateOrganizationDto dto)
        {
            var entity = Mapper.Map<Organization>(dto);

            return entity;
        }

        public virtual void Update(EditOrganizationDto dto)
        {
            Mapper.Map(dto, this);
        }
    }
}