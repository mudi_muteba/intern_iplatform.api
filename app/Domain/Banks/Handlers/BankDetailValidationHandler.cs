﻿using System;
using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Banks;

using MasterData.Authorisation;
using log4net;
//using CardinalLegacyCOM;

namespace Domain.Users.Handlers
{
    public enum BackAccountType
    {
        Current = 1,
        Savings = 2,
        Transmission = 3,
        Bond = 4,
        Subscription = 6
    }

    public class BankDetailValidationHandler : BaseDtoHandler<BankDetailValidationDto, bool>
    {
        private static readonly ILog _Log = LogManager.GetLogger(typeof(BankDetailValidationHandler));

        public BankDetailValidationHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(BankDetailValidationDto dto, HandlerResult<bool> result)
        {
            //bool valid = false;
            //try
            //{
            //    BackAccountType accountType;
            //    if (Enum.TryParse(dto.AccountType, out accountType))
            //    {
            //        BankAccountValidator validator = new BankAccountValidator();
            //        validator.ValidateBankDetails(dto.LookupPath, dto.BranchCode, dto.AccountNumber, accountType.ToString().ToUpper());
            //        valid = validator.Result.get_Valid();
            //    }
            //    else
            //    {
            //        _Log.Error(String.Format("Failed to parse AccountType with value: {0} to BankAccountType Enum.", dto.AccountType));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _Log.Error(String.Format("An error occurred whilst trying to validate bank account details. Message: {0}", ex.Message), ex);
            //}
            result.Processed(true);
        }
    }
}