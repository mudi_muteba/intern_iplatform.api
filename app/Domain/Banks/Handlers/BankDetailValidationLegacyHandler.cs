﻿using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Banks;
using MasterData.Authorisation;
using log4net;


namespace Domain.Users.Handlers
{
    public class BankDetailValidationLegacyHandler : BaseDtoHandler<BankDetailValidationLegacyDto, bool>
    {
        private static readonly ILog m_Log = LogManager.GetLogger(typeof(BankDetailValidationHandler));

        public BankDetailValidationLegacyHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {

        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(BankDetailValidationLegacyDto dto, HandlerResult<bool> result)
        {
            bool valid = false;
            try
            {
                BackAccountType accountType;
                if (Enum.TryParse(dto.AccountType, out accountType))
                {
                    var validator = new CardinalLegacyCOM.BankAccountValidator();
                    validator.ValidateBankDetails(dto.LookupPath, dto.BranchCode, dto.AccountNumber, accountType.ToString().ToUpper());
                    valid = validator.Result.get_Valid();
                }
                else
                {
                    m_Log.Error(String.Format("Failed to parse AccountType with value: {0} to BankAccountType Enum.", dto.AccountType));
                }
            }
            catch (Exception ex)
            {
                m_Log.Error(String.Format("An error occurred whilst trying to validate bank account details. Message: {0}", ex.Message), ex);
            }

            result.Processed(valid);
        }
    }
}