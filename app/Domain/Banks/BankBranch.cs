﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Banks
{
    public class BankBranch : Entity
    {
        public virtual string Name { get; set; }

        public virtual string BranchCode { get; set; }

        public virtual Bank Bank { get; set; }
    }
}
