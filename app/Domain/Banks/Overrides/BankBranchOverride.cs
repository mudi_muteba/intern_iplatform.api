﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Banks.Overrides
{
    public class BankBranchOverride : IAutoMappingOverride<BankBranch>
    {
        public void Override(AutoMapping<BankBranch> mapping)
        {
            mapping.References(a => a.Bank, "BankId");
        }
    }
}