﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Banks;
using MasterData.Authorisation;

namespace Domain.Banks.Queries
{
    public class SearchBanksQuery : BaseQuery<Bank>, ISearchQuery<BankSearchDto>
    {
        private BankSearchDto criteria;

        public SearchBanksQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Bank>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(BankSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<Bank> Execute(IQueryable<Bank> query)
        {

            if (criteria.Id != null && criteria.Id > 0)
                query = query.Where(a => a.Id == criteria.Id);

            if (!string.IsNullOrWhiteSpace(criteria.Name))
            {
                query = query.Where(a => a.Name.StartsWith(criteria.Name));
            }

            return query;
        }
    }
}