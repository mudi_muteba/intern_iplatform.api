﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Banks;
using MasterData.Authorisation;

namespace Domain.Banks.Queries
{
    public class SearchBankBranchesQuery : BaseQuery<BankBranch>, ISearchQuery<BankBranchesSearchDto>
    {
        private BankBranchesSearchDto criteria;

        public SearchBankBranchesQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<BankBranch>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(BankBranchesSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<BankBranch> Execute(IQueryable<BankBranch> query)
        {

            if (criteria.Id != null && criteria.Id > 0)
                query = query.Where(a => a.Id == criteria.Id);

            if (criteria.BankId != null && criteria.BankId > 0)
                query = query.Where(a => a.Bank.Id == criteria.BankId);

            if (!string.IsNullOrWhiteSpace(criteria.Name))
                query = query.Where(a => a.Name.StartsWith(criteria.Name));

            if (!string.IsNullOrWhiteSpace(criteria.BranchCode))
                query = query.Where(a => a.BranchCode.StartsWith(criteria.BranchCode));

            return query;
        }
    }
}