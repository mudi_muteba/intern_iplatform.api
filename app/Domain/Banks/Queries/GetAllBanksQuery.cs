﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Banks.Queries
{
    public class GetAllBanksQuery : BaseQuery<Bank>
    {
        public GetAllBanksQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Bank>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }


        protected internal override IQueryable<Bank> Execute(IQueryable<Bank> query)
        {
            return query;
        }
    }
}