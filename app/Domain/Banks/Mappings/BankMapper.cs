﻿
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using System;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base;

namespace Domain.Banks.Mappings
{
    public class BankMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Bank, ListBankDto>();

            Mapper.CreateMap<PagedResults<Bank>, PagedResultDto<ListBankDto>>();

        }
    }
}