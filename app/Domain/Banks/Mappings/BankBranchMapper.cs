﻿
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using System;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base;

namespace Domain.Banks.Mappings
{
    public class BankBranchMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<BankBranch, ListBankBranchesDto>();

            Mapper.CreateMap<PagedResults<BankBranch>, PagedResultDto<ListBankBranchesDto>>();

        }
    }
}