﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Notifications.Queries
{
    public class GetNotificationsQuery : BaseQuery<NotificationUser>
    {
        private int RecipientUserId;
        private int SenderUserId;
        private bool IsMessageRead;
        private bool DateTimeToNotifyIsValid;

        public GetNotificationsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<NotificationUser>())
        {
            IsMessageRead = false;
            DateTimeToNotifyIsValid = true;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetNotificationsQuery ByRecipientUserId(int userId)
        {
            this.RecipientUserId = userId;
            return this;
        }

        public GetNotificationsQuery BySenderUserId(int userId)
        {
            SenderUserId = userId;
            return this;
        }

        public GetNotificationsQuery OnlyGetWhereDateTimeToNotifyIsValid(bool dateTimeToNotify)
        {
            DateTimeToNotifyIsValid = dateTimeToNotify;
            return this;
        }

        public GetNotificationsQuery GetUnreadReadMessagesOnly(bool unreadMessagesOnly)
        {
            IsMessageRead = !unreadMessagesOnly;
            return this;
        }

        protected internal override IQueryable<NotificationUser> Execute(IQueryable<NotificationUser> query)
        {
            query = query.Where(n => n.IsMessageRead == IsMessageRead);

            if (DateTimeToNotifyIsValid)
                query = query.Where(n => n.Notification.DateTimeToNotify <= DateTime.UtcNow);

            if (RecipientUserId > 0 )
                return query.Where(c => c.RecipientUser.Id == RecipientUserId);

            if (SenderUserId > 0)
                return query.Where(n => n.Notification.SenderUser.Id == SenderUserId);

            return query;
        }
    }
}