using System;
using System.Linq;
using AutoMapper;
using Domain.Base;
using Domain.Users;
using iPlatform.Api.DTOs.Notifications;

namespace Domain.Notifications
{
    public class Notification : Entity
    {
        public Notification()
        {
            DateCreated = DateTime.UtcNow;
            DateTimeToNotify = DateCreated;
        }

        public virtual User SenderUser { get; protected set; }
        public virtual DateTime DateCreated { get; protected set; }
        public virtual DateTime DateTimeToNotify { get; protected set; }
        public virtual string SenderAlias { get; protected set; }
        public virtual string Message { get; protected set; }

        public static Notification Create(CreateNotificationDto dto)
        {
            var notification = Mapper.Map<Notification>(dto);
            if (string.IsNullOrEmpty(notification.SenderAlias))
            {
                var individual = notification.SenderUser.Individuals.FirstOrDefault();
                if (individual != null)
                    notification.SenderAlias = individual.DisplayName;
            }
            return notification;
        }
    }
}
