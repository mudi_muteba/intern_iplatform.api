﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Notifications.Overrides
{
    public class NotificationOverride: IAutoMappingOverride<Notification>
    {
        public void Override(AutoMapping<Notification> mapping)
        {
            mapping.Map(x => x.Message).CustomType("StringClob").CustomSqlType("ntext");
            
        }
    }
    
}