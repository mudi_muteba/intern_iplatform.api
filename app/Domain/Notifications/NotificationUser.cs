using System;
using Domain.Base;
using Domain.Users;

namespace Domain.Notifications
{
    public class NotificationUser : Entity
    {
        public virtual Notification Notification { get; protected set; }
        public virtual User RecipientUser { get; protected set; }
        public virtual bool IsMessageRead { get; protected set; }
        public virtual DateTime? DateTimeMessageRead { get; protected set; }

        public static NotificationUser Create(Notification notification, User user)
        {
            var notificationUser = new NotificationUser()
            {
                Notification = notification,
                RecipientUser = user,
                IsMessageRead = false
            };
            return notificationUser;
        }

        public virtual bool MarkAsRead()
        {
            IsMessageRead = true;
            DateTimeMessageRead = DateTime.UtcNow;
            return true;
        }
    }
}
