using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Imports.Leads;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Notifications;
using MasterData.Authorisation;

namespace Domain.Notifications.Handlers
{
    public class EditNotificationDtoHandler : ExistingEntityDtoCustomHandler<NotificationUser, EditNotificationUserDto, bool>
    {
        private readonly IRepository m_Repository;

        public EditNotificationDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            m_Repository = repository;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override NotificationUser GetEntity(EditNotificationUserDto dto)
        {
            return m_Repository.GetById<NotificationUser>(dto.NotificationUserId);
        }

        protected override void HandleExistingEntityChange(NotificationUser entity, EditNotificationUserDto dto, HandlerResult<bool> result)
        {
            result.Processed(entity.MarkAsRead());
        }
    }
}