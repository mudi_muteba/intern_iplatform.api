﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using iPlatform.Api.DTOs.Notifications;
using MasterData.Authorisation;

namespace Domain.Notifications.Handlers
{
    public class CreateNotificationDtoHandler : CreationDtoHandler<Notification, CreateNotificationDto, int>
    {
      
        private readonly IRepository m_Repository;
        private ExecutionResult m_Result;

        public CreateNotificationDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }


        protected override void EntitySaved(Notification entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Notification HandleCreation(CreateNotificationDto dto, HandlerResult<int> result)
        {
            var entity = Notification.Create(dto);
            var usersToSendTo = new List<User>();

            if (dto.ChannelId.HasValue)
            {
                var users = m_Repository.GetAll<UserChannel>().Where(x => x.Channel.Id == Convert.ToInt32(dto.ChannelId)).Select(y => y.User);
                usersToSendTo.AddRange(users);
            }
            else if (dto.RecipientUserId.HasValue)
            {
                var user = m_Repository.GetById<User>(dto.RecipientUserId.Value);
                usersToSendTo.Add(user);
            }
            else
            {
                usersToSendTo.AddRange(GetAllUsers());
            }

            SaveUsers(usersToSendTo.AsQueryable(), entity);

            result.Processed(entity.Id);
            return entity;
        }

        private void SaveUsers(IQueryable<User> users, Notification entity)
        {
            foreach (var user in users)
            {
                CreateNotification(user, entity);
            }
        }

        private void CreateNotification(User user, Notification notification)
        {

            if (user != null) // && user.Id != dto.SenderUserId)
            {
                var notificationUser = NotificationUser.Create(notification, user);
                m_Repository.Save(notificationUser);
            }
        }

        private List<User> GetAllUsers()
        {
            var users = m_Repository.GetAll<User>().ToList().Where(u => ExecutionContext.Channels.Contains(u.DefaultChannelId));
            return users.ToList();
            
        }

    }
}