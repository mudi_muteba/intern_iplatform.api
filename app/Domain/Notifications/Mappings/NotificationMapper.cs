﻿using AutoMapper;
using Domain.Users;
using iPlatform.Api.DTOs.Notifications;

namespace Domain.Notifications.Mappings
{
    public class NotificationMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateNotificationDto, Notification>()
                .ForMember(d => d.SenderUser, opt => opt.MapFrom(x => Mapper.Map<int, User>(x.SenderUserId)));

            Mapper.CreateMap<NotificationUser, NotificationDto>()
                .ForMember(d => d.RecipientUserId, opt => opt.MapFrom(x => x.RecipientUser.Id))
                .ForMember(d => d.SenderUserId, opt => opt.MapFrom(x => x.Notification.SenderUser.Id))
                .ForMember(d => d.DateCreated, opt => opt.MapFrom(x => x.Notification.DateCreated))
                .ForMember(d => d.IsMessageRead, opt => opt.MapFrom(x => x.IsMessageRead))
                .ForMember(d => d.Message, opt => opt.MapFrom(x => x.Notification.Message))
                .ForMember(d => d.SenderAlias, opt => opt.MapFrom(x => x.Notification.SenderAlias))
                .ForMember(d => d.Id, opt => opt.MapFrom(x => x.Notification.Id))
                .ForMember(d => d.NotificationUserId, opt => opt.MapFrom(x => x.Id))
                ;
        }
    }
}
