﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.Components.ComponentDefinitionGroupTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentDefinitionGroupTypes
{
    public class ComponentDefinitionGroupType : Entity
    {
        public virtual string Name { get; set; }
        public virtual int VisibleIndex { get; set; }

        public ComponentDefinitionGroupType()
        {

        }

        public static ComponentDefinitionGroupType Create(CreateComponentDefinitionGroupTypeDto dto)
        {
            var motorLossHistory = Mapper.Map<ComponentDefinitionGroupType>(dto);

            return motorLossHistory;
        }

        public virtual void Update(EditComponentDefinitionGroupTypeDto editComponentDefinitionGroupTypeDto)
        {
            Mapper.Map(editComponentDefinitionGroupTypeDto, this);
        }

        public virtual void Disable()
        {
            this.Delete();
        }
    }
}
