﻿using AutoMapper;
using iPlatform.Api.DTOs.Components.ComponentDefinitionGroupTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentDefinitionGroupTypes.Mappings
{
    public class ComponentDefinitionGroupTypeMappings : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComponentDefinitionGroupType, ComponentDefinitionGroupTypeDto>()
                .ReverseMap()
                ;

        }
    }
}
