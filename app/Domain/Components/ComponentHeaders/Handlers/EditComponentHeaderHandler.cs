﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Components.ComponentHeaders;

namespace Domain.Components.ComponentHeaders.Handlers
{
    public class EditComponentHeaderHandler : ExistingEntityDtoHandler<ComponentHeader, EditComponentHeaderDto, int>
    {
        public EditComponentHeaderHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(ComponentHeader entity, EditComponentHeaderDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
