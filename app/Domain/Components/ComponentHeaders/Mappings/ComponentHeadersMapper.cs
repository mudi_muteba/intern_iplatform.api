﻿using AutoMapper;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentHeaders.Mappings
{
    public class ComponentHeadersMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComponentHeader, ComponentHeaderDto>()
                .ReverseMap()
                ;


            Mapper.CreateMap<CreateComponentHeaderDto, ComponentHeader>()
                .ReverseMap()
                ;

            Mapper.CreateMap<EditComponentHeaderDto, ComponentHeader>()
                .ReverseMap()
                ;
        }
    }
}
