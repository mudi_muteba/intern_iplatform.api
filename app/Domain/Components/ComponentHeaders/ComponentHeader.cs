﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentHeaders
{
    public class ComponentHeader : Entity
    {
        public virtual  int ActivityId { get; set; }
        public virtual  int ActivityTypeId { get; set; }
        public virtual  DateTime CreatedDate { get; set; }
        public virtual  DateTime DateModified { get; set; }
        public virtual  string Source { get; set; }
        public virtual int PartyId { get; set; }

        public ComponentHeader()
        {

        }

        public static ComponentHeader Create(CreateComponentHeaderDto dto)
        {
            var componentHeader = Mapper.Map<ComponentHeader>(dto);

            componentHeader.CreatedDate = DateTime.UtcNow;
            componentHeader.DateModified = DateTime.UtcNow;


            return componentHeader;
        }

        public virtual void Update(EditComponentHeaderDto componentDefinitionDto)
        {
            Mapper.Map(componentDefinitionDto, this);
        }

        public virtual void Disable()
        {
            this.Delete();
        }
    }
}
