﻿using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentQuestionDefinitions.Queries
{
    public class SearchComponentQuestionDefinitionsQuery : BaseQuery<ComponentQuestionDefinition>, ISearchQuery<SearchComponentDefinitionsDto>
    {
        private SearchComponentDefinitionsDto criteria;
        private IProvideContext _contextProvider;

        public SearchComponentQuestionDefinitionsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ComponentQuestionDefinition>())
        {
            _contextProvider = contextProvider;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(SearchComponentDefinitionsDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<ComponentQuestionDefinition> Execute(IQueryable<ComponentQuestionDefinition> query)
        {

            if (!string.IsNullOrWhiteSpace(criteria.ComponentTypeId) && !string.IsNullOrWhiteSpace(criteria.ComponentQuestionGroupId))
            {
                query = query.Where(x => x.ComponentType.Id == Convert.ToInt32(criteria.ComponentTypeId)
                && x.ComponentQuestionGroup.Id == Convert.ToInt32(criteria.ComponentQuestionGroupId)
                && x.ChannelId == _contextProvider.Get().ActiveChannelId)
                ;

                return query;
            }

            if (!string.IsNullOrWhiteSpace(criteria.ComponentTypeId))
            {
                query = query.Where(x => x.ComponentType.Id == Convert.ToInt32(criteria.ComponentTypeId)
                && x.ChannelId == _contextProvider.Get().ActiveChannelId)
                    .OrderBy(z => z.ComponentType.Id)
                    .ThenBy(c => c.ComponentQuestion.Id);

                return query;
            }

            return query;   
        }
    }
}
