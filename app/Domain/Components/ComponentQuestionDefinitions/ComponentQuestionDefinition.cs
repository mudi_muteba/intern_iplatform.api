﻿using AutoMapper;
using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using MasterData;

namespace Domain.Components.ComponentQuestionDefinitions
{
    public class ComponentQuestionDefinition : Entity
    {
        public virtual  string DisplayName { get; set; }
        public virtual  int ChannelId { get; set; }
        public virtual  ComponentQuestion ComponentQuestion { get; set; }
        public virtual  ComponentQuestionGroup ComponentQuestionGroup { get; set; }
        public virtual ComponentType ComponentType { get; set; }
        public virtual  int VisibleIndex { get; set; }
        public virtual  bool IsRequieredForQuote { get; set; }
        public virtual  bool IsRatingFactor { get; set; }
        public virtual  bool IsReadOnly { get; set; }
        public virtual  string ToolTip { get; set; }
        public virtual  string DefaultValue { get; set; }
        public virtual  string RegexPattern { get; set; }

        public ComponentQuestionDefinition()
        {

        }

        public static ComponentQuestionDefinition Create(CreateComponentQuestionDefinitionDto dto)
        {
            var componentQuestionDefinition = Mapper.Map<ComponentQuestionDefinition>(dto);

            return componentQuestionDefinition;
        }

        public virtual void Update(EditComponentQuestionDefinitionDto componentDefinitionDto)
        {
            Mapper.Map(componentDefinitionDto, this);
        }

        public virtual void Disable()
        {
            this.Delete();
        }
    }
}
