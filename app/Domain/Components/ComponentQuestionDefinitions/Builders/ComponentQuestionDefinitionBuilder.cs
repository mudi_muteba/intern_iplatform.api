﻿using AutoMapper;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentQuestionDefinitions.Builders
{
    public class ComponentQuestionDefinitionBuilder
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(ComponentQuestionDefinitionBuilder));
        private IProvideContext _contextProvider;
        private readonly IRepository _repository;


        public ComponentQuestionDefinitionBuilder(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        public ListResultDto<SearchComponentQuestionDefinitionResultDto> Build(IQueryable<ComponentQuestionDefinition> results)
        {

            var result = new ListResultDto<SearchComponentQuestionDefinitionResultDto>();
            var mdComponentAnswers = new ComponentAnswers();

            foreach (var item in results)
            {
                var definitionResult = new SearchComponentQuestionDefinitionResultDto()
                {
                    ComponentQuestionDefinition = Mapper.Map<ComponentQuestionDefinition,
                    SearchComponentDefinitionQuestionsResultDto> (item)

                };

                foreach (var answer in mdComponentAnswers.Where(x => x.Question.Id == item.ComponentQuestion.Id).ToList())
                {
                    definitionResult.ComponentQuestionDefinition.ComponentAnswerOptions.Add(new SearchComponentQuestionAnswerResultDto()
                    {
                        Id = answer.Id,
                        Name = answer.Name,
                        Answer = answer.Answer,
                        QuestionId = answer.Question.Id,
                        VisibleIndex = answer.VisibleIndex
                    });
                }
                
                
                result.Results.Add(definitionResult);
            }

            return result;
        }
    }
}
