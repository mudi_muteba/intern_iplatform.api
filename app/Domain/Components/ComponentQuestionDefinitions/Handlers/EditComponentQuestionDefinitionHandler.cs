﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;

namespace Domain.Components.ComponentQuestionDefinitions.Handlers
{
    public class EditComponentQuestionDefinitionHandler : ExistingEntityDtoHandler<ComponentQuestionDefinition, EditComponentQuestionDefinitionDto, int>
    {
        public EditComponentQuestionDefinitionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(ComponentQuestionDefinition entity, EditComponentQuestionDefinitionDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
