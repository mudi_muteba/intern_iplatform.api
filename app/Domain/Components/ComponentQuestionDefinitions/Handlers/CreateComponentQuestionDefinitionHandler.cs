﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentQuestionDefinitions.Handlers
{
    public class CreateComponentQuestionDefinitionHandler : CreationDtoHandler<ComponentQuestionDefinition, CreateComponentQuestionDefinitionDto, int>
    {
        public CreateComponentQuestionDefinitionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(ComponentQuestionDefinition entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ComponentQuestionDefinition HandleCreation(CreateComponentQuestionDefinitionDto dto, HandlerResult<int> result)
        {
            var componentQuestionDefinition = ComponentQuestionDefinition.Create(dto);

            result.Processed(componentQuestionDefinition.Id);

            return componentQuestionDefinition;
        }
    }
}
