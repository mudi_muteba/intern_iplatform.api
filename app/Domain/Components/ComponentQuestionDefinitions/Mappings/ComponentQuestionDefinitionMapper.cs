﻿using AutoMapper;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentQuestionDefinitions.Mappings
{
    public class ComponentQuestionDefinitionMapper: Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComponentQuestionDefinition, ComponentQuestionDefinitionDto>()
                .ReverseMap()
                ;

            Mapper.CreateMap<CreateComponentQuestionDefinitionDto, ComponentQuestionDefinition>()
                .ReverseMap()
                ;

            Mapper.CreateMap<EditComponentQuestionDefinitionDto, ComponentQuestionDefinition>()
                .ReverseMap()
                ;

            Mapper.CreateMap<ComponentQuestionDefinition, SearchComponentDefinitionQuestionsResultDto>()
                .ForMember(s => s.QuestionDefinitionId, d => d.MapFrom(o => o.Id))
                .ForMember(s => s.DisplayName, d => d.MapFrom(o => o.DisplayName))
                .ForMember(s => s.QuestionGoupId, d => d.MapFrom(o => o.ComponentQuestionGroup.Id))
                .ForMember(s => s.QuestionGroupName, d => d.MapFrom(o => o.ComponentQuestionGroup.Name))
                .ForMember(s => s.VisibleIndex, d => d.MapFrom(o => o.VisibleIndex))
                .ForMember(s => s.IsRequieredForQuote, d => d.MapFrom(o => o.IsRequieredForQuote))
                .ForMember(s => s.IsRatingFactor, d => d.MapFrom(o => o.IsRatingFactor))
                .ForMember(s => s.IsReadOnly, d => d.MapFrom(o => o.IsReadOnly))
                .ForMember(s => s.ToolTip, d => d.MapFrom(o => o.ToolTip))
                .ForMember(s => s.DefaultValue, d => d.MapFrom(o => o.DefaultValue))
                .ForMember(s => s.RegexPattern, d => d.MapFrom(o => o.RegexPattern))
                .ForMember(s => s.ComponentTypeId, d => d.MapFrom(o => o.ComponentType.Id))
                .ForMember(s => s.ComponentTypeName, d => d.MapFrom(o => o.ComponentType.Name))
                .ForMember(s => s.QuestionTypeId, d => d.MapFrom(o => o.ComponentQuestion.QuestionType.Id))
                .ForMember(s => s.QuestionTypeName, d => d.MapFrom(o => o.ComponentQuestion.QuestionType.Name))
                ;
        }
    }
}
