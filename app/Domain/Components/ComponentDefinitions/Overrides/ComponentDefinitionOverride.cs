﻿
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Components.ComponentDefinitions.Overrides
{
    public class ComponentDefinitionOverride : IAutoMappingOverride<ComponentDefinition>
    {
        public void Override(AutoMapping<ComponentDefinition> mapping)
        {
            mapping.References(c => c.ComponentType);
        }
    }
}