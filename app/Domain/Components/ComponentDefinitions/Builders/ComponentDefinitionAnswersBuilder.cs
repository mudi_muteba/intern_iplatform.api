﻿using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Repository;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using AutoMapper;
using Domain.Components.ComponentHeaders;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using Domain.Components.ComponentQuestionAnswers;
using MasterData;

namespace Domain.Components.ComponentDefinitions.Builders
{
    public class ComponentDefinitionAnswersBuilder
    {

        protected static readonly ILog log = LogManager.GetLogger(typeof(ComponentDefinitionAnswersBuilder));
        private IProvideContext _contextProvider;
        private readonly IRepository _repository;

        public ComponentDefinitionAnswersBuilder(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        public void Build()
        {

        }

        public ListResultDto<SearchComponentQuestionDefinitionAnswerResultDto> Build(IQueryable<ComponentDefinition> definitions)
        {
            var result = new ListResultDto<SearchComponentQuestionDefinitionAnswerResultDto>();
            var questionTypes = new QuestionTypes();
            foreach (var definition in definitions)
            {
                var questionAnswers = _repository.GetAll<ComponentQuestionAnswer>()
                    .Where(x => x.ComponentDefinitionId == definition.Id)
                    .ToList();

                foreach (var answer in questionAnswers)
                {
                    var dto = new SearchComponentQuestionDefinitionAnswerResultDto()
                    {
                        Header = Mapper.Map<ComponentHeader, ComponentHeaderDto>(definition.ComponentHeader),
                        Definition = Mapper.Map<ComponentDefinition, ComponentDefinitionDto>(definition),
                        Answer = Mapper.Map<ComponentQuestionAnswer, ComponentQuestionAnswerDto>(answer),
                        QuetionType = questionTypes.FirstOrDefault(x => x.Id == answer.ComponentQuestionTypeId)

                    };

                    result.Results.Add(dto);
                }                                
            }

            return result;
        }
    }
}
