﻿using Common.Logging;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Components.ComponentDefinitions.Questions;
using Domain.Base.Execution;

namespace Domain.Components.ComponentDefinitions.Builders
{
    public class ComponentDefinitionBuilderBuilder
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(ComponentDefinitionBuilderBuilder));
        private IProvideContext _contextProvider;
        private readonly IRepository _repository;

        public ComponentDefinitionBuilderBuilder(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        public ComponentDefinitionQuestionsDto BuildComponentDefinitionQuestions(ComponentDefinition definition)
        {
            var result = new ComponentDefinitionQuestionsDto();

            var activeChannel = _contextProvider.Get().ActiveChannelId;

            //have to stop here due to time constrains 

            return result;           
        }
    }
}
