﻿using Domain.Base;
using Domain.Components.ComponentHeaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using AutoMapper;
using Domain.Party;
using MasterData;

namespace Domain.Components.ComponentDefinitions
{
    public class ComponentDefinition : Entity
    {
        public virtual ComponentHeader ComponentHeader { get; set; }

        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime DateModified { get; set; }
        public virtual ComponentType ComponentType { get; set; }
        public virtual int PartyId { get; set; }

        public ComponentDefinition()
        {

        }

        public static ComponentDefinition Create(ComponentHeader componentHeader, CreateComponentDefinitionDto dto)
        {
            var componentDefinition = Mapper.Map<ComponentDefinition>(dto);

            componentDefinition.ComponentHeader = componentHeader;
            componentDefinition.CreatedDate = DateTime.UtcNow;
            componentDefinition.DateModified = DateTime.UtcNow;
            componentDefinition.ComponentType = new ComponentTypes().FirstOrDefault(x => x.Id == dto.ComponentTypeId);

            return componentDefinition;
        }

        public virtual void Update(EditComponentDefinitionDto componentDefinitionDto)
        {
            Mapper.Map(componentDefinitionDto, this);
        }

        public virtual void Disable()
        {
            this.Delete();
        }
    }
}
