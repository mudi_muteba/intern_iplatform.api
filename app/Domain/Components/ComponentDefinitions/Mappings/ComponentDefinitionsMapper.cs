﻿using AutoMapper;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentDefinitions.Mappings
{
    public class ComponentDefinitionsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComponentDefinition, ComponentDefinitionDto>()
                .ReverseMap()
                ;

            Mapper.CreateMap<CreateComponentDefinitionDto, ComponentDefinition>()
                .ReverseMap()
                ;

            Mapper.CreateMap<EditComponentDefinitionDto, ComponentDefinition>()
                .ReverseMap()
                ;

        }
    }
}
