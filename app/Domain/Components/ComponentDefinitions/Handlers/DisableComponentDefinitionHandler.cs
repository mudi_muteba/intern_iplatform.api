﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentDefinitions.Handlers
{
    public class DisableComponentDefinitionHandler : ExistingEntityDtoHandler<ComponentDefinition, DisableComponentDefinitionDto, int>
    {
        public DisableComponentDefinitionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(ComponentDefinition entity, DisableComponentDefinitionDto dto,
            HandlerResult<int> result)
        {
            entity.Disable();
            result.Processed(entity.Id);
        }
    }
}
