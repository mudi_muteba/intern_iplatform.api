﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;

namespace Domain.Components.ComponentDefinitions.Handlers
{
    public class EditComponentDefinitionHandler : ExistingEntityDtoHandler<ComponentDefinition, EditComponentDefinitionDto, int>
    {
        public EditComponentDefinitionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(ComponentDefinition entity, EditComponentDefinitionDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
