﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Components.ComponentHeaders;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentDefinitions.Handlers
{
    public class CreateComponentDefinitionHandler : CreationDtoHandler<ComponentDefinition, CreateComponentDefinitionDto, int>
    {
        private IRepository _repository;

        public CreateComponentDefinitionHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(ComponentDefinition entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ComponentDefinition HandleCreation(CreateComponentDefinitionDto dto, HandlerResult<int> result)
        {
            var header = _repository.GetAll<ComponentHeader>().FirstOrDefault(x => x.Id == dto.ComponentHeaderId);
            var componentDefinition = ComponentDefinition.Create(header,dto);

            result.Processed(componentDefinition.Id);

            return componentDefinition;
        }
    }
}
