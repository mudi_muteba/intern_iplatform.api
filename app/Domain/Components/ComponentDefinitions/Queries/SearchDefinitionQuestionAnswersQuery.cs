﻿using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Components.ComponentQuestionDefinitions;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentDefinitions.Queries
{
    public class SearchDefinitionQuestionAnswersQuery : BaseQuery<ComponentDefinition>, ISearchQuery<SearchComponentDefinitionAnswersDto>
    {
        private SearchComponentDefinitionAnswersDto criteria;
        private IProvideContext _contextProvider;

        public SearchDefinitionQuestionAnswersQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ComponentDefinition>())
        {
            _contextProvider = contextProvider;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(SearchComponentDefinitionAnswersDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<ComponentDefinition> Execute(IQueryable<ComponentDefinition> query)
        {
            query = query.Where(x => x.PartyId == criteria.PartyId
            && x.ComponentType.Id == Convert.ToInt16(criteria.ComponentTypeId)
            );

            return query;
        }
    }
}
