﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentQuestionAnswers.Handlers
{
    public class SaveComponentQuestionAnswersHandler : BaseDtoHandler<SaveComponentQuestionAnswersDto,bool>
    {
        private IProvideContext _contextProvider;
        private IRepository _repository;

        public SaveComponentQuestionAnswersHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(SaveComponentQuestionAnswersDto dto, HandlerResult<bool> result)
        {
            foreach (var answer in dto.Answers)
            {
                var entity = ComponentQuestionAnswer.Create(answer);
                _repository.Save(entity);
            }
            var context = _contextProvider.Get();
            
            result.Processed(true);
        }
    }
}
