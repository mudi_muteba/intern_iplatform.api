﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;

namespace Domain.Components.ComponentQuestionAnswers.Handlers
{
    public class EditComponentQuestionAnswerHandler : ExistingEntityDtoHandler<ComponentQuestionAnswer, EditComponentQuestionAnswerDto, int>
    {
        public EditComponentQuestionAnswerHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(ComponentQuestionAnswer entity, EditComponentQuestionAnswerDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
