﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentQuestionAnswers.Handlers
{
    public class CreateComponentQuestionAnswerHandler : CreationDtoHandler<ComponentQuestionAnswer, CreateComponentQuestionAnswerDto, int>
    {
        public CreateComponentQuestionAnswerHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(ComponentQuestionAnswer entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ComponentQuestionAnswer HandleCreation(CreateComponentQuestionAnswerDto dto, HandlerResult<int> result)
        {
            var componentQuestionAnswer = ComponentQuestionAnswer.Create(dto);

            result.Processed(componentQuestionAnswer.Id);

            return componentQuestionAnswer;
        }
    }
}
