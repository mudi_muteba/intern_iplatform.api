﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using MasterData.Authorisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentQuestionAnswers.Handlers
{
    public class DisableComponentQuestionAnswerHandler : ExistingEntityDtoHandler<ComponentQuestionAnswer, DisableComponentQuestionAnswerDto, int>
    {
        public DisableComponentQuestionAnswerHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(ComponentQuestionAnswer entity, DisableComponentQuestionAnswerDto dto,
            HandlerResult<int> result)
        {
            entity.Disable();
            result.Processed(entity.Id);
        }
    }
}