﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentQuestionAnswers
{
    public class ComponentQuestionAnswer : Entity
    {
        public  virtual int ComponentDefinitionId { get; set; }
        public  virtual int ComponentQuestionDefinitionId { get; set; }
        public  virtual string Answer { get; set; }
        public virtual int ComponentQuestionTypeId { get; set; }

        public ComponentQuestionAnswer()
        {

        }

        public static ComponentQuestionAnswer Create(CreateComponentQuestionAnswerDto dto)
        {
            var componentQuestionAnswer = Mapper.Map<ComponentQuestionAnswer>(dto);

            return componentQuestionAnswer;
        }



        public virtual void Update(EditComponentQuestionAnswerDto componentDefinitionDto)
        {
            Mapper.Map(componentDefinitionDto, this);
        }

        public virtual void Disable()
        {
            this.Delete();
        }
    }
}
