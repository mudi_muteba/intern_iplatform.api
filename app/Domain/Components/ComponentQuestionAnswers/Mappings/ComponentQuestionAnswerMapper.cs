﻿using AutoMapper;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Components.ComponentQuestionAnswers.Mappings
{
    public class ComponentQuestionAnswerMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ComponentQuestionAnswer, ComponentQuestionAnswerDto>()
                .ReverseMap()
                ;

            Mapper.CreateMap<CreateComponentQuestionAnswerDto, ComponentQuestionAnswer>()
                .ReverseMap()
                ;

            Mapper.CreateMap<EditComponentQuestionAnswerDto, ComponentQuestionAnswer>()
                .ReverseMap()
                ;

        }
    }
}
