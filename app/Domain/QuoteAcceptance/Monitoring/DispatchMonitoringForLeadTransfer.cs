﻿using System;
using Common.Logging;
using Domain.Ratings.Engines.iRate;
using Infrastructure.Configuration;
using iPlatform.Api.DTOs.Monitoring;
using RestSharp;
using Shared;

namespace Domain.QuoteAcceptance.Monitoring
{ 
    public interface IDispatchLeadTransferMessage
    {
        void Dispatch(MonitoringDto dto);
    }


    public class DispatchLeadTransferMessage : IDispatchLeadTransferMessage
    {
        private readonly string _monitoringUrl;
        private readonly string _token;
        private static readonly ILog Log = LogManager.GetLogger<DispatchLeadTransferMessage>();

        public DispatchLeadTransferMessage()
        {
            _monitoringUrl = new AdminConfigurationReader().BaseUrl;
            _token = new MonitoringAuthentication().Authenticate().Token;
        }

        public DispatchLeadTransferMessage(string monitoringUrl)
        {
            _monitoringUrl = string.IsNullOrEmpty(monitoringUrl) ? _monitoringUrl : monitoringUrl;
        }

        public void Dispatch(MonitoringDto dto)
       {
            var client = new RestClient(_monitoringUrl);
            var request = new RestRequest("monitorings/Track", Method.POST)
            {
                RequestFormat = DataFormat.Json,
            };
            request.AddParameter("Authorization", _token, ParameterType.HttpHeader);
            request.AddBody(dto);
            try
            {
                Log.InfoFormat("Sending quote acceptance monitoring to monitoring api on url {0}", _monitoringUrl);
                var response = client.Execute<MonitoringDto>(request);
            }
            catch (Exception ex)
            {
                var error = string.Format("Could not send monitoring information with Rating Request Id '{0}'. Exception details: {1}",
                    dto.ParentId,
                    new ExceptionPrettyPrinter().Print(ex));
                Log.ErrorFormat("Lead Transfer Monitoring Error: {0}", error);
            }
        }
    }
}
