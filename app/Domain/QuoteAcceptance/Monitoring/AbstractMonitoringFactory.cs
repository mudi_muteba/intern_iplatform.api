﻿using iPlatform.Api.DTOs.Monitoring;

namespace Domain.QuoteAcceptance.Monitoring
{
    public abstract class AbstractMonitoringFactory<T> : IBuildMonitoringDto<T>
    {
        public abstract MonitoringDto Exception(T message);
        public abstract MonitoringDto Faliure(T message);
        public abstract MonitoringDto Finished(T message);
        public abstract MonitoringDto Started(T message);
        public abstract MonitoringDto Successful(T message);

        public MonitoringDto Exception(object message)
        {
            return Exception((T) message);
        }

        public MonitoringDto Faliure(object message)
        {
            return Faliure((T)message);
        }
        public MonitoringDto Finished(object message)
        {
            return Finished((T)message);
        }
        public MonitoringDto Started(object message)
        {
            return Started((T)message);
        }
        public MonitoringDto Successful(object message)
        {
            return Successful((T)message);
        }
    }

    public interface IBuildMonitoringDto
    {
        MonitoringDto Exception(object message);
        MonitoringDto Faliure(object message);
        MonitoringDto Finished(object message);
        MonitoringDto Started(object message);
        MonitoringDto Successful(object message);
    }

    public interface IBuildMonitoringDto<in T> : IBuildMonitoringDto
    {
        MonitoringDto Exception(T message);
        MonitoringDto Faliure(T message);
        MonitoringDto Finished(T message);
        MonitoringDto Started(T message);
        MonitoringDto Successful(T message);
    }
}
