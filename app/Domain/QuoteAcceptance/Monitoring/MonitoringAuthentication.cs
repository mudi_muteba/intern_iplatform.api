﻿using System;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using Common.Logging;
using Domain.Ratings.Engines.iRate;
using iPlatform.Api.DTOs.Monitoring;
using Infrastructure.Configuration;
using MasterData.Authorisation.Groups;
using RestSharp;
using Shared.Extentions;
using Workflow.Infrastructure;

namespace Domain.QuoteAcceptance.Monitoring
{
    public class MonitoringAuthentication
    {
        private readonly string _postUrl;
        private readonly AuthenticationRequestDto _request;
        private static readonly ILog Log = LogManager.GetLogger<MonitoringAuthentication>();


        public MonitoringAuthentication()
        {
            _postUrl = new AdminConfigurationReader().BaseUrl;

            _request = new AuthenticationRequestDto
                            {
                                Email = new AdminConfigurationReader().UserName ?? "root@iplatform.co.za",
                                ApiKey = new AdminConfigurationReader().apiKey ?? "q1w2e3r4t5 ?_"
            };
        }


        public AuthenticateResponseDto Authenticate()
        {

            var client = new RestClient(_postUrl);
            var request = new RestRequest("authenticate", Method.POST)
            {
                RequestFormat = DataFormat.Json,
                //JsonSerializer = new JsonSerializerRestSharp()
            };

            request.AddBody(_request);
            try
            {
                Log.InfoFormat("Authenticating iAdmin with email address {0} and url {1}", _request.Email, _postUrl);
                var response = client.Execute<AuthenticateResponseDto>(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var token = response.Headers.FirstOrDefault(h => h.Name.EqualsIgnoreCase("Authorization"));

                    if (token == null)
                        return AuthenticateResponseDto.NotAuthenticated();

                    return string.IsNullOrEmpty(token.Value.ToString())
                        ? AuthenticateResponseDto.NotAuthenticated()
                        : AuthenticateResponseDto.Authenticated(token.Value.ToString());
                }
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Could not authenticate using email: '{0}' and password: '{1}' having following errors: {2}",
                        _request.Email,
                        _request.ApiKey,
                        new ExceptionPrettyPrinter().Print(ex));
                Log.ErrorFormat("Authentication Failed: {0}", error);
            }

            return AuthenticateResponseDto.NotAuthenticated();
        }
    }

    public class AuthenticationRequestDto
    {
        public string Email { get; set; }
        public string ApiKey { get; set; }
    }

    public class AuthenticateResponseDto
    {
        public bool IsAuthenticated { get; set; }
        public string Token { get; set; }

        public AuthenticateResponseDto()
        {
        }

        private AuthenticateResponseDto(string token, bool authenticated)
        {
            Token = token;
            IsAuthenticated = authenticated;
        }

        public static AuthenticateResponseDto Authenticated(string token)
        {
            return new AuthenticateResponseDto(token, true);
        }

        public static AuthenticateResponseDto NotAuthenticated()
        {
            return new AuthenticateResponseDto(string.Empty, false);
        }
    }
}
