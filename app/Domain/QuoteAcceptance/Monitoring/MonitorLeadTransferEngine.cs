﻿using iPlatform.Api.DTOs.Monitoring;

namespace Domain.QuoteAcceptance.Monitoring
{
    public interface IMonitorLeadTransfer
    {
        void Dispatch(MonitoringDto message);
    }

    public class MonitorLeadTransfer : IMonitorLeadTransfer
    {
        private readonly IDispatchLeadTransferMessage _forLeadTransfer;

        public MonitorLeadTransfer(IDispatchLeadTransferMessage forLeadTransfer)
        {
            _forLeadTransfer = forLeadTransfer;
        }

        public void Dispatch(MonitoringDto message)
        {
            _forLeadTransfer.Dispatch(message);
        }
    }
}