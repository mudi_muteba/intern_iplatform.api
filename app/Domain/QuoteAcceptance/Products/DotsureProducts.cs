﻿namespace Domain.QuoteAcceptance.Products
{
    public class DotsureProducts
    {
        static DotsureProducts()
        {
        }

        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "LITEAA",
                    "DOTSURE"
                };
            }
        }
    }
}