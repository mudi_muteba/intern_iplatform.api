﻿namespace Domain.QuoteAcceptance.Products
{
    public static class SAUProducts
    {
        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "CENTND",
                    "CENTRD",
                    "SNTMND",
                    "SNTMRD",
                };
            }
        }
    }
}