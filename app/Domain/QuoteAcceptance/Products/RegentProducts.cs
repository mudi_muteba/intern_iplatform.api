﻿namespace Domain.QuoteAcceptance.Products
{
    public static class RegentProducts
    {
        static RegentProducts()
        {
        }

        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "REGPRD"
                };
            }
        }
    }
}

