﻿namespace Domain.QuoteAcceptance.Products
{
    public static class KingPriceV2Products
    {
        static KingPriceV2Products()
        {
        }

        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "KPIPERS2"
                };
            }
        }
    }
}