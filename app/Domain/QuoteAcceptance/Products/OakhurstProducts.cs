﻿namespace Domain.QuoteAcceptance.Products
{
    public static class OakhurstProducts
    {
        static OakhurstProducts()
        {
        }

        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "LITE",
                    "OAKHURST",
                    "CONTRACTORSURE",
                    "OAKHURST ASPIRE",
                    "OAKHURST ELITE",
                    "OAKHURST ELEGANCE",
                    "OAKHURST PP",
                    "PERFORMER",
                    "PREMIER",
                    "OAKHRS"
                };
            }
        }
    }
}