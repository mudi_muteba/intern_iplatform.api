﻿namespace Domain.QuoteAcceptance.Products
{
    public static class TelesureProducts
    {
        static TelesureProducts()
        {
        }

        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "AUGPRD",
                    "BUD",
                    "DIA",
                    "FIR",
                    "UNI",
                    "VIR",
                    "VIRS",
                    "CRSAA",
                    "CRSAUG",
                    "CRSBUD",
                    "CRSDIA",
                    "CRSFIR",
                    "CRSUNI",
                    "CRSVIR"
                };
            }

        }
    }
}
