﻿namespace Domain.QuoteAcceptance.Products
{
    public static class IdsProducts
    {
        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "MUL",
                    "VIRGIN",
                    "IWYZEMOTORC",
                    "IWYZEMOTORTPF",
                    "IWYZEMOTORTO",
                    "DOMESTIC_ARMOUR",
                    "ZURPERS",
                    "AAC",
                    "SCRATC",
                    "AIGDTB"
                };
            }
        }
    }
}