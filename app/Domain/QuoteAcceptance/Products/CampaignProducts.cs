﻿namespace Domain.QuoteAcceptance.Products
{
    public class CampaignProducts
    {
        static CampaignProducts()
        {
        }

        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "MULPLX",
                    "FLS",
                    "RENPRD"
                };
            }
        }
    }
}