﻿namespace Domain.QuoteAcceptance.Products
{
    public static class HollardEasyProducts
    {
        static HollardEasyProducts()
        {
        }

        public static string[] Codes 
        {
            get
            {
                return new[]
                {
                     "HOLEASYO",
                     "HOLEASYP",
                };
            }
        }
    }
}
