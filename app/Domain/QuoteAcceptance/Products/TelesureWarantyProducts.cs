﻿namespace Domain.QuoteAcceptance.Products
{
    public static class TelesureWarantyProducts
    {
        static TelesureWarantyProducts()
        {

        }

        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "MOTORWARRANTY",
                    "PREOWNEDWARRANTY"
                };
            }

        }
    }
}
