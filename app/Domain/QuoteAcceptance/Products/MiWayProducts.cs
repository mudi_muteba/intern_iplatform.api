﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.QuoteAcceptance.Products
{
    public static class MiWayProducts
    {
        public static string[] Codes
        {
            get
            {
                return new[]
                {
                    "MW",
                };
            }
        }
    }
}
