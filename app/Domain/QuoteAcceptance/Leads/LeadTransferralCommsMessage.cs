﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using ToolBox.Communication.Interfaces;
using Workflow.Messages.Mail;
using Domain.Emailing.Factory;

namespace Domain.QuoteAcceptance.Leads
{
    [DataContract]
    public class LeadTransferralCommsMessage : CommunicationMessage<ICommunicationMessageData>
    {
        [DataMember]
        public override string Address { get; set; }
        [DataMember]
        public override string CCAddresses { get; set; }
        [DataMember]
        public override string BCCAddresses { get; set; }
        [DataMember]
        public override string Subject { get; set; }
        [DataMember]
        public override string Body { get; set; }
        [DataMember]
        public override string TemplateName { get; set; }
        [DataMember]
        public string ErrorAddress { get; set; }

        [DataMember, JsonConverter(typeof(JsonConcreteTypeConverter<CommunicationMessageData>))]
        public override ICommunicationMessageData MessageData { get; set; }

        [DataMember]
        public AppSettingsEmailSetting AppSettingsEmailSetting { get; set; }

        public LeadTransferralCommsMessage() { }
    }
}
