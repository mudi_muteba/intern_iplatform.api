﻿using System.Runtime.Serialization;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Infrastructure;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Enums.Workflows;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Leads
{
    [WorkflowExecutionMessageType(WorkflowMessageType.LeadTransferal)]
    public class QuoteUploadMessage : WorkflowExecutionMessage
    {
        [DataMember]
        public PublishQuoteUploadMessageDto Quote { get; set; }

        [DataMember]
        public IRetryStrategy RetryStrategy { get; set; }


        public QuoteUploadMessage(PublishQuoteUploadMessageDto quote, ITaskMetadata metadata, IRetryStrategy retryStrategy) : base(metadata)
        {
            Quote = quote;
            RetryStrategy = retryStrategy;
            ExecutionPlan = new ExecutionPlan();
        }

        public QuoteUploadMessage(PublishQuoteUploadMessageDto quote, IRetryStrategy retryStrategy)
        {
            Quote = quote;
            RetryStrategy = retryStrategy;
            ExecutionPlan = new ExecutionPlan();
        }

        public QuoteUploadMessage()
        {
            RetryStrategy = new RetryStrategy(2,2);
            ExecutionPlan = new ExecutionPlan();
        }
    }
}