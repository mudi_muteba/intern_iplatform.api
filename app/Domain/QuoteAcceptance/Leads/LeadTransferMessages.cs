﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Shared;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Monitoring;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData;
using Shared;
using Workflow.Messages;
using Workflow.Messages.Mail;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Leads
{

    public class LeadTransferStarted : WorkflowExecutionMessage
    {
        public LeadTransferStarted(ITaskMetadata metadata)
            : base(metadata)
        {
            
        }

        public static LeadTransferStarted WithChannelId(string message, Guid channelId, string parameters)
        {
            return new LeadTransferStarted(new LeadTransferStartedMetadata(message, parameters, channelId));
        }
    }


    public class LeadTransferFinished : WorkflowExecutionMessage
    {
        public LeadTransferFinished(ITaskMetadata metadata) : base(metadata)
        {
        }

        public static LeadTransferFinished WithChannelId(string message, Guid channelId)
        {
            return new LeadTransferFinished(new LeadTransferFinishedMetadata(message, channelId));
        }
    }


    public class LeadTransferFailed : WorkflowExecutionMessage
    {
        public LeadTransferFailed(ITaskMetadata metadata) : base(metadata)
        {
        }

        public static LeadTransferFailed WithChannelId(string message, Guid channelId)
        {
            return new LeadTransferFailed(new LeadTransferFailedMetadata(message, channelId));
        }
    }


    public class LeadTransferEmailMessage : WorkflowExecutionMessage
    {
        public LeadTransferEmailMessage()
        {
            
        }

        public LeadTransferEmailMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
        }

        public LeadTransferEmailMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default)
            : base(metadata, messageType)
        {
        }

        public static LeadTransferEmailMessage ErrorWithChannelId(CommunicationMetadata emailTask, Guid channelId)
        {
            return new LeadTransferEmailMessage(emailTask, WorkflowExecutionMessageType.Failure);
        }
        public static LeadTransferEmailMessage InformationWithChannelId(CommunicationMetadata emailTask, Guid channelId)
        {
            return new LeadTransferEmailMessage(emailTask, WorkflowExecutionMessageType.Sub);
        }

        public static LeadTransferEmailMessage SuccessWithChannelId(EmailTaskMetaData emailTask, Guid channelId)
        {
            return new LeadTransferEmailMessage(emailTask, WorkflowExecutionMessageType.Success);
        }
    }


    public class LeadTransferSuccessful : WorkflowExecutionMessage
    {
        
        public LeadTransferSuccessful(ITaskMetadata metadata) : base(metadata)
        {
        }

        public static LeadTransferSuccessful WithChannelId(string message, Guid channelId)
        {
            return new LeadTransferSuccessful(new LeadTransferSuccessfulMetadata(message, channelId));
        }
    }


    public class LeadTransferException : WorkflowExecutionMessage
    {
        public LeadTransferException(ITaskMetadata metadata) : base(metadata)
        {
        }

        public static LeadTransferException WithChannelId(Exception exception, string message, string insurer, Guid channelId)
        {
            return new LeadTransferException(new LeadTransferExceptionMetadata(message, new ExceptionPrettyPrinter().Print(exception), insurer, channelId));
        }
    }


    public class LeadTransferInformation : WorkflowExecutionMessage
    {
        public LeadTransferInformation(ITaskMetadata metadata)
            : base(metadata)
        {
            Date = DateTime.UtcNow;
        }

        public static LeadTransferInformation WithChannelId(string message, Priority priority, Guid channelId)
        {
            return new LeadTransferInformation(new LeadTransferInformationMetadata(message, priority, channelId));
        }

        [DataMember] public readonly DateTime Date;
    }


    public class LeadQuoteUploadMessage : WorkflowExecutionMessage
    {
        public LeadQuoteUploadMessage(ITaskMetadata metadata)
            : base(metadata)
        {

        }

        public static LeadQuoteUploadMessage CreateQuoteUploadLog(PublishQuoteUploadMessageDto dto, string errorMessage, string referenceNumber, string quoteUploadObject = "")
        {
            var createQuoteUploadLogDto = new CreateQuoteUploadLogDto
            {
                QuoteId = dto.Id,
                Fees = dto.Policy.Fees,
                Premium = dto.Items.Sum(x => x.Premium),
                Sasria = dto.Items.Sum(x => x.Sasria),
                ProductId = dto.Policy.ProductId,
                RequestId = dto.Request.Id.ToString(),
                Message = errorMessage,
                InsurerReference = referenceNumber,
                QuoteUploadObject = quoteUploadObject
            };

            return new LeadQuoteUploadMessage(new LeadQuoteUploadMetadata(createQuoteUploadLogDto));
        }
    }


    public class  CustomerSatisfactionSurveyMessage : WorkflowExecutionMessage
    {
        public CustomerSatisfactionSurveyMessage(ITaskMetadata metadata)
            : base(metadata)
        {

        }

        public static CustomerSatisfactionSurveyMessage CreateCustomerSatisfactionSurveyMessage(PublishQuoteUploadMessageDto dto, string referenceNumber)
        {
            var request = new CustomerSatisfactionSurveyRequestDto
            {
                ExportedDateTime = DateTime.UtcNow,
                SystemUser = dto.CampaignInfo.AgentName,
                GeneralNotes = "Quote has been accepted by client",
                TriggerEvent = EchoTCFEventTypes.NewBusiness.Id,
                EntityType = EntityTypes.Brokerage.Id,
                EntityCode = dto.ChannelInfo.Code,
                EntityName = dto.ChannelInfo.Name,
                PolicyHolder = dto.InsuredInfo.Firstname + " " + dto.InsuredInfo.Surname,
                ClientType = ClientTypes.Policy.Id,
                IDCoRegNo = dto.InsuredInfo.IDNumber,
                PolicyClaimNumber = string.Format("{0}~{1}", dto.Environment, referenceNumber),
                IsPolicy = 1,
                IsClaim = 0,
                PolicyHolderCell = dto.InsuredInfo.ContactNumber,
                PolicyHolderEmail = dto.InsuredInfo.EmailAddress,
                InsurerName = dto.Policy.InsurerName,
                InsuranceType = InsuranceTypes.PersonalLines.Id,
                EffectiveDate = DateTime.UtcNow, // todo: need to verify
                EchoTCF = dto.EchoTCF,
            };
            return new CustomerSatisfactionSurveyMessage(new CustomerSatisfactionSurveyMetadata(request));
        }
    }
    public class DeleteQuoteAcceptMessage : WorkflowExecutionMessage
    {
        public DeleteQuoteAcceptMessage(ITaskMetadata metadata)
            : base(metadata)
        {

        }

        public static DeleteQuoteAcceptMessage Create(int quoteId)
        {
            return new DeleteQuoteAcceptMessage(new LeadQuoteAcceptDeleteMetaData(quoteId));
        }
    }
}