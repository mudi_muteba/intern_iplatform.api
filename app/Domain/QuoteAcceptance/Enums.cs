﻿using System.Runtime.Serialization;

namespace Domain.QuoteAcceptance
{
    public enum LeadCategory
    {
        [EnumMember(Value = "Lead Transfer")]
        LeadTransfer
    }

    public enum LeadSubCategory
    {
        [EnumMember(Value = "Finished Lead Transfer")]
        LeadTransferFinished,
        [EnumMember(Value = "Lead Transfer Failed")]
        LeadTransferFailed,
        [EnumMember(Value = "Lead Transfer Started")]
        LeadTransferStarted,
        [EnumMember(Value = "Exception Occurred in Lead Transfer")]
        LeadTransferException,
        [EnumMember(Value = "Lead Transfer was Successfull")]
        LeadTransferSuccessful
    }

    public enum InsurerName
    {
        [EnumMember(Value = "Campaign")] Campaign = 1,
        [EnumMember(Value = "Dotsure")] Dotsure = 2,
        [EnumMember(Value = "Hollard")] Hollard = 3,
        [EnumMember(Value = "King Price")] KingPrice = 4,
        [EnumMember(Value = "Oakhurst")] Oakhurst = 5,
        [EnumMember(Value = "Regent")] Regent = 6,
        [EnumMember(Value = "Telesure")] Telesure = 7,
        [EnumMember(Value = "IDS")] Ids = 7,
        [EnumMember(Value = "SAU")] SAU = 8,
        [EnumMember(Value = "AA")] AA = 9,
        [EnumMember(Value = "MiWay")] MiWay = 10,
        [EnumMember(Value = "Hollard Easy")] HollardEasy = 11,
    }
}