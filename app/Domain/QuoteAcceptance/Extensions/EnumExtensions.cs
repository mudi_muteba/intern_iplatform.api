﻿using System.Linq;
using System.Runtime.Serialization;

namespace Domain.QuoteAcceptance.Extensions
{
    public static class EnumExtensions
    {
        public static string Name(this InsurerName value)
        {
            var attribute = value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof (EnumMemberAttribute), false).SingleOrDefault() as EnumMemberAttribute;
            return attribute == null ? value.ToString() : attribute.Value;
        }

        public static string Name(this LeadSubCategory value)
        {
            var attribute =
                value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof (EnumMemberAttribute), false).SingleOrDefault() as
                    EnumMemberAttribute;
            return attribute == null ? value.ToString() : attribute.Value;
        }

        public static string Name(this LeadCategory value)
        {
            var attribute =
                value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof (EnumMemberAttribute), false).SingleOrDefault() as
                    EnumMemberAttribute;
            return attribute == null ? value.ToString() : attribute.Value;
        }
    }
}