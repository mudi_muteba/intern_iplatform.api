﻿using Domain.PolicyBindings.Metadata;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.PolicyBindings;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Extensions
{
    public static class TaskMetadataExtensions
    {
        private static T Get<T>(this ITaskMetadata metadata, string property)
        {
            var obj = metadata.GetType().GetProperty(property).GetValue(metadata, null);
            return (T) obj;
        }

        public static QuoteAcceptedMetadata GetQuoteAcceptedMetadata(this ITaskMetadata metadata)
        {
            return (QuoteAcceptedMetadata)metadata;
        }

        public static AbstractMetadata GetAbstractMetadata(this ITaskMetadata metadata)
        {
            return (AbstractMetadata)metadata;
        }


        public static PolicyBindingMetadata GetPolicyBindingMetadata(this ITaskMetadata metadata)
        {
            return (PolicyBindingMetadata)metadata;
        }

        public static PublishQuoteUploadMessageDto GetQuoteAcceptanceDto(this QuoteAcceptedMetadata metadata)
        {
            return metadata.Get<PublishQuoteUploadMessageDto>("Quote");
        }

        public static PolicyBindingRequestDto GetPolicyBindingRequestDto(this PolicyBindingMetadata metadata)
        {
            return metadata.Get<PolicyBindingRequestDto>("PolicyBinding");
        }

        public static T GetTaskMetadata<T>(this AbstractMetadata metadata)
        {
            return metadata.Get<T>("TaskMetadata");
        }

        public static ApiMetadata GetApiMetadata(this AbstractMetadata metadata)
        {
            return metadata.Get<ApiMetadata>("ApiMetadata");
        }

        public static RetryStrategy GetRetryStrategy(this AbstractMetadata metadata)
        {
            return metadata.Get<RetryStrategy>("RetryStrategy");
        }
      
        public static LeadTransferralCommsMessage GetCommunicationMetadata(this LeadTransferEmailMessage message)
        {
            return ((CommunicationMetadata)message.Metadata).CommunicationMessage;
        }
    }
}