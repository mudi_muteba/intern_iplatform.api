﻿using Domain.Emailing.Factory;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using System.Linq;
using System.Runtime.Serialization;

namespace Domain.QuoteAcceptance.Extensions
{
    public static class EmailSettingExtensions
    {
        public static AppSettingsEmailSetting Get(this EmailCommunicationSettingDto dto)
        {
            if (dto == null)
                return new AppSettingsEmailSetting();

            return new AppSettingsEmailSetting
            {
                CustomFrom = "",
                DefaultBCC = dto.DefaultBCC,
                DefaultContactNumber = dto.DefaultContactNumber,
                DefaultFrom = dto.DefaultFrom,
                Host = dto.Host,
                Password = dto.Password,
                Port = dto.Port,
                SubAccountID = dto.SubAccountID,
                UseDefaultCredentials = dto.UseDefaultCredentials,
                UserName = dto.Username,
                UseSSL = dto.UseSSL
            };
        }
    }
}