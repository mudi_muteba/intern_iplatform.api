﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData.Authorisation;

namespace Domain.QuoteAcceptance.Handlers
{
    public class CreateQuoteUploadLogDtoHandler : CreationDtoHandler<QuoteUploadLog, CreateQuoteUploadLogDto, int>
    {
        public CreateQuoteUploadLogDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void EntitySaved(QuoteUploadLog entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override QuoteUploadLog HandleCreation(CreateQuoteUploadLogDto dto, HandlerResult<int> result)
        {
            var quoteUploadLog = QuoteUploadLog.Create(dto);

            result.Processed(quoteUploadLog.Id);
            return quoteUploadLog;
        }
    }
}