﻿using System;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Ratings.Quoting;

using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using Domain.PolicyBindings.Metadata;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.QuoteAcceptance.Workflow.Router
{
    public interface IGetConfiguredInsurerMetadata
    {
        QuoteAcceptedMetadata Generate(PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads);

        PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads);
    }
}
