﻿using System;
using System.Collections.Generic;
using Domain.QuoteAcceptance.Infrastructure.Factories;
using Domain.QuoteAcceptance.Products;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Workflow.Router
{
    public sealed class RouteAcceptedQuoteWorkflowMessageTasks
    {
        public static readonly IDictionary<IEnumerable<string>, Func<ITaskMetadata, IWorkflowExecutionMessage>> Tasks = new Dictionary
            <IEnumerable<string>, Func<ITaskMetadata, IWorkflowExecutionMessage>>()
        {
            {CampaignProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {DotsureProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {HollardProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {KingPriceProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {KingPriceV2Products.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {OakhurstProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {RegentProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {TelesureProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {IdsProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {SAUProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {MiWayProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {AAProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {TelesureWarantyProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
            {HollardEasyProducts.Codes, (metadata) => new CreateLeadTransferralMessageFactory().Create(metadata)},
        };
    }
}