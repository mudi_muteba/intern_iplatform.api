﻿using System;
using System.Collections.Generic;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Products;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;
using Domain.QuoteAcceptance.Metadata;
using Domain.PolicyBindings.Metadata;
using Domain.PolicyBindings.Messages;

namespace Domain.QuoteAcceptance.Workflow.Engine
{
    public sealed class LeadTransferrals
    {
        public static readonly IDictionary<IEnumerable<string>, Func<ITaskMetadata, IWorkflowExecutionMessage>> Destinations = new Dictionary
            <IEnumerable<string>, Func<ITaskMetadata, IWorkflowExecutionMessage>>()
        {
            {
                HollardProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.Hollard.Name(), ((LeadTransferHollardTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                CampaignProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.Campaign.Name(), ((LeadTransferCampaignTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                DotsureProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.Dotsure.Name(), ((LeadTransferDotsureTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                KingPriceV2Products.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.KingPrice.Name(), ((LeadTransferKingPriceV2TaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                KingPriceProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.KingPrice.Name(), ((LeadTransferKingPriceTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                OakhurstProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.Oakhurst.Name(), ((LeadTransferOakhurstTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                RegentProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.Regent.Name(), ((LeadTransferRegentTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                TelesureProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.Telesure.Name(), ((LeadTransferTelesureTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                IdsProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.Ids.Name(), ((LeadTransferIdsTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                SAUProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.SAU.Name(), ((LeadTransferSAUTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                AAProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.AA.Name(), ((LeadTransferAATaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                MiWayProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.MiWay.Name(), ((LeadTransferToMiWayMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                TelesureWarantyProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.MiWay.Name(), ((LeadTransferIdsTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            },
            {
                HollardEasyProducts.Codes, (metadata) => LeadTransferralMessage(metadata, InsurerName.HollardEasy.Name(), ((LeadTransferHollardEasyTaskMetadata)metadata.GetAbstractMetadata().TaskMetadata).ToString())
            }
        };

        private static IWorkflowExecutionMessage LeadTransferralMessage(ITaskMetadata metadata, string insurerName, string parameters)
        {
            var quoteAcceptedMetadata = metadata as QuoteAcceptedMetadata;
            if (quoteAcceptedMetadata != null)
            {
                var leadTransferralMessage = new QuoteUploadMessage(quoteAcceptedMetadata.GetQuoteAcceptanceDto(), metadata.GetAbstractMetadata(), metadata.GetAbstractMetadata().GetRetryStrategy());
                leadTransferralMessage.AddMessage(LeadTransferStarted.WithChannelId(string.Format("Started transferring lead for {0}", insurerName), metadata.GetAbstractMetadata().ChannelId, parameters));
                leadTransferralMessage.AddFailureMessage(LeadTransferFailed.WithChannelId(string.Format("Lead Transfer for {0} Failed", insurerName), metadata.GetAbstractMetadata().ChannelId));
                leadTransferralMessage.AddSuccessMessage(LeadTransferSuccessful.WithChannelId(string.Format("Lead Transfer for {0} was Successful", insurerName), metadata.GetAbstractMetadata().ChannelId));
                leadTransferralMessage.AddSubMessage(LeadTransferFinished.WithChannelId(string.Format("Lead Transfer for {0} has Finished", insurerName), metadata.GetAbstractMetadata().ChannelId));
                return leadTransferralMessage;
            }

            var policyBindingMetadata = metadata as PolicyBindingMetadata;
            if (policyBindingMetadata != null)
            {
                var leadTransferralMessage = new SubmitPolicyBindingMessage(policyBindingMetadata.GetPolicyBindingRequestDto(), metadata.GetAbstractMetadata(), metadata.GetAbstractMetadata().GetRetryStrategy());
                leadTransferralMessage.AddMessage(LeadTransferStarted.WithChannelId(string.Format("Started transferring policy binding for {0}", insurerName), metadata.GetAbstractMetadata().ChannelId, parameters));
                leadTransferralMessage.AddFailureMessage(LeadTransferFailed.WithChannelId(string.Format("Policy binding Transfer for {0} Failed", insurerName), metadata.GetAbstractMetadata().ChannelId));
                leadTransferralMessage.AddSuccessMessage(LeadTransferSuccessful.WithChannelId(string.Format("Policy binding Transfer for {0} was Successful", insurerName), metadata.GetAbstractMetadata().ChannelId));
                leadTransferralMessage.AddSubMessage(LeadTransferFinished.WithChannelId(string.Format("Policy binding Transfer for {0} has Finished", insurerName), metadata.GetAbstractMetadata().ChannelId));
                return leadTransferralMessage;
            }

            return null;
        }
    }
}