﻿using System;
using System.Linq;
using AutoMapper;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Workflow.Router;
using Domain.Ratings.Events;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using Domain.Admin.SettingsQuoteUploads;
using Domain.PolicyBindings.Messages;

namespace Domain.QuoteAcceptance.Mapping
{
    public class WorkflowMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ExternalQuoteAcceptedWithUnderwritingEnabledEvent, QuoteUploadMessage>()
                .ConvertUsing<ITypeConverter<ExternalQuoteAcceptedWithUnderwritingEnabledEvent, QuoteUploadMessage>>();

            Mapper.CreateMap<SubmitPolicyBindingEvent, SubmitPolicyBindingMessage>()
                .ConvertUsing<ITypeConverter<SubmitPolicyBindingEvent, SubmitPolicyBindingMessage>>();

        }
    }

    public class LeadTransferralMessageConverter : TypeConverter<ExternalQuoteAcceptedWithUnderwritingEnabledEvent, QuoteUploadMessage>
    {
        private readonly ICreateTaskMetdata<RouteAcceptedQuoteTask> _taskMetadataFactory;

        public LeadTransferralMessageConverter(ICreateTaskMetdata<RouteAcceptedQuoteTask> taskMetadataFactory)
        {
            _taskMetadataFactory = taskMetadataFactory;
        }

        protected override QuoteUploadMessage ConvertCore(ExternalQuoteAcceptedWithUnderwritingEnabledEvent @event)
        {
            if(!@event.SettingsQuoteUploads.Any())
                throw new Exception(string.Format("There are no SettingQuoteUpload configured for product code {0} and Channel {1}", @event.ProductCode, @event.Dto.ChannelInfo.SystemId));

            var metadata = _taskMetadataFactory.Create(new RouteAcceptedQuoteTask(@event.Dto, @event.ProductCode, @event.InsurerCode, @event.Dto.Request.RatingContext.ChannelId, @event.SettingsQuoteUploads));
            var keyValuePair = RouteAcceptedQuoteWorkflowMessageTasks.Tasks
                .FirstOrDefault(w => w.Key.Any(productCode => productCode.Equals(@event.ProductCode, StringComparison.CurrentCultureIgnoreCase)));

            if (keyValuePair.Value == null)
                throw new Exception(string.Format("There are no configured tasks for product code {0}", @event.ProductCode));

            return keyValuePair.Value(metadata) as QuoteUploadMessage;
        }
    }

    public class PolicyBindingTransferralMessageConverter : TypeConverter<SubmitPolicyBindingEvent, SubmitPolicyBindingMessage>
    {
        private readonly ICreateTaskMetdata<RoutePolicyBindingTask> m_TaskMetadataFactory;

        public PolicyBindingTransferralMessageConverter(ICreateTaskMetdata<RoutePolicyBindingTask> taskMetadataFactory)
        {
            m_TaskMetadataFactory = taskMetadataFactory;
        }

        protected override SubmitPolicyBindingMessage ConvertCore(SubmitPolicyBindingEvent @event)
        {
            if (!@event.SettingsQuoteUploads.Any())
            {
                throw new Exception(string.Format("There are no SettingQuoteUpload configured for product code {0} and Channel {1}", @event.ProductCode, @event.Dto.ChannelInfo.SystemId));
            }
                
            var metadata = m_TaskMetadataFactory.Create(new RoutePolicyBindingTask(@event.Dto, @event.ProductCode, @event.InsurerCode, @event.Dto.ChannelInfo.SystemId, @event.SettingsQuoteUploads));
            var keyValuePair = RouteAcceptedQuoteWorkflowMessageTasks.Tasks
                .FirstOrDefault(w => w.Key.Any(productCode => productCode.Equals(@event.ProductCode, StringComparison.CurrentCultureIgnoreCase)));

            if (keyValuePair.Value == null)
            {
                throw new Exception(string.Format("There are no configured tasks for product code {0}", @event.ProductCode));
            }
                
            return keyValuePair.Value(metadata) as SubmitPolicyBindingMessage;
        }
    }
}