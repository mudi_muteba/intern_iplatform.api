﻿using System.Collections.Generic;
using Domain.QuoteAcceptance.Products;
using Domain.QuoteAcceptance.Workflow.Router;

namespace Domain.QuoteAcceptance.Metadata
{
    public sealed class InsurerMetadata
    {
        public static readonly IDictionary<IEnumerable<string>, IGetConfiguredInsurerMetadata> Configurations = new Dictionary<IEnumerable<string>, IGetConfiguredInsurerMetadata>()
        {
            { HollardProducts.Codes, MetadataConfigurationReader.Hollard },
            { CampaignProducts.Codes, MetadataConfigurationReader.Campaign },
            { DotsureProducts.Codes, MetadataConfigurationReader.Dotsure },
            { KingPriceProducts.Codes, MetadataConfigurationReader.KingPrice },
            { KingPriceV2Products.Codes, MetadataConfigurationReader.KingPriceV2 },
            { OakhurstProducts.Codes, MetadataConfigurationReader.Oakhurst },
            { RegentProducts.Codes, MetadataConfigurationReader.Regent },
            { TelesureProducts.Codes, MetadataConfigurationReader.Telesure },
            { IdsProducts.Codes, MetadataConfigurationReader.Ids },
            { SAUProducts.Codes, MetadataConfigurationReader.SAU },
            { MiWayProducts.Codes, MetadataConfigurationReader.MiWay },
            { AAProducts.Codes, MetadataConfigurationReader.AA },
            { TelesureWarantyProducts.Codes, MetadataConfigurationReader.Ids },
            { HollardEasyProducts.Codes, MetadataConfigurationReader.HollardEasy },
        };
    }
}
