﻿using System;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Workflow.Router;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages.Mail;
using Domain.QuoteAcceptance.Extensions;
using Domain.Emailing.Factory;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using iPlatform.Api.DTOs.PolicyBindings;
using Domain.PolicyBindings.Metadata;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public class KingPriceV2Configuration : AbstractConfigurationReader, IGetConfiguredInsurerMetadata
    {
        public QuoteAcceptedMetadata Generate(PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new QuoteAcceptedMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), quote, productCode, RetryStrategy(settingsQuoteUploads), Mail(quote.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        public PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new PolicyBindingMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), policyBinding, productCode, RetryStrategy(settingsQuoteUploads), Mail(policyBinding.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        private LeadTransferKingPriceV2TaskMetadata TaskMetadata(Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var agentCode = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/AgentCode");
            var wsTrustClientIdentityServerUrl = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/SecurityEndpoint");
            var wsTrustClientServicesUrl = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/SecurityDestination");

            return new LeadTransferKingPriceV2TaskMetadata(wsTrustClientIdentityServerUrl, wsTrustClientServicesUrl, agentCode, channelId);
        }

        private ApiMetadata Api(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var url = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/ChannelEndpoint");
            var token = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/Token");
            var user = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/UserName");
            var password = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/Password");
            var BrokerCode = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/BrokerCode");
            var CompanyCode = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/CompantCode");
            return ApiMetadata.UrlWithCompleteInfo(token, url, user, password, BrokerCode, CompanyCode);
        }

        private RetryStrategy RetryStrategy(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var retryDelay = int.Parse(settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/RetryDelayIncrement", 5.ToString()));
            var maxRetries = int.Parse(settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/MaxRetries", 2.ToString()));
            return new RetryStrategy(retryDelay, maxRetries);
        }

        private CommunicationMetadata Mail(AppSettingsEmailSetting appSettingsEmailSettings, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var address = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            var errorAddress = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za");
            var subject = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/subject", "UPLOADING LEAD TO KING PRICE");
            var templateName = settingsQuoteUploads.GetSetting("Kingpriceobv2/Upload/template", "LeadUploadTemplate");
            return new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = address,
                ErrorAddress = errorAddress,
                Subject = subject,
                TemplateName = templateName,
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = appSettingsEmailSettings
            });
        }
    }
}
