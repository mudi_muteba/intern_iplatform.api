﻿using System;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Workflow.Router;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Domain.Emailing.Factory;
using Domain.QuoteAcceptance.Extensions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using Domain.PolicyBindings.Metadata;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public class DotsureConfiguration : AbstractConfigurationReader, IGetConfiguredInsurerMetadata
    {
        public QuoteAcceptedMetadata Generate(PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> SettingsQuoteUploads)
        {
            return new QuoteAcceptedMetadata(TaskMetadata(channelId, SettingsQuoteUploads), Api(SettingsQuoteUploads), quote, productCode, RetryStrategy(SettingsQuoteUploads), Mail(quote.ChannelInfo.EmailCommunicationSetting.Get(), SettingsQuoteUploads), channelId);
        }

        public PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new PolicyBindingMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), policyBinding, productCode, RetryStrategy(settingsQuoteUploads), Mail(policyBinding.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        private LeadTransferDotsureTaskMetadata TaskMetadata(Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var agentCode = settingsQuoteUploads.GetSetting("Dotsure/Upload/AgentCode");
            var agentEmail = settingsQuoteUploads.GetSetting("Dotsure/Upload/AcceptanceEmailAddress");
            var agentName = settingsQuoteUploads.GetSetting("Dotsure/Upload/AgentName");
            var agentTelephone = settingsQuoteUploads.GetSetting("Dotsure/Upload/AgentTelephone");
            var brokerCode = settingsQuoteUploads.GetSetting("Dotsure/Upload/BrokerCode");
            var subBrokerCode = settingsQuoteUploads.GetSetting("Dotsure/Upload/SubBrokerCode");
            var subPartnerCode = settingsQuoteUploads.GetSetting("Dotsure/Upload/SubPartnerCode");
            var subPartnerName = settingsQuoteUploads.GetSetting("Dotsure/Upload/SubPartnerName");
            var schemeName = settingsQuoteUploads.GetSetting("Dotsure/Upload/SchemeName");
            var acceptanceEmail = settingsQuoteUploads.GetSetting(@"Dotsure/Upload/AcceptanceEmailAddress");

            return new LeadTransferDotsureTaskMetadata(agentCode, agentEmail, agentName, agentTelephone, brokerCode,
                subBrokerCode, subPartnerCode, subPartnerName, schemeName, acceptanceEmail, channelId);
        }

        private ApiMetadata Api(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var url = settingsQuoteUploads.GetSetting("Dotsure/Upload/ServiceURL");
            var user = settingsQuoteUploads.GetSetting("Dotsure/Upload/User");
            var password = settingsQuoteUploads.GetSetting("Dotsure/Upload/Password");

            return ApiMetadata.UrlWithCredentials(url, user, password);
        }

        private RetryStrategy RetryStrategy(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var retryDelay = int.Parse(settingsQuoteUploads.GetSetting("Dotsure/Upload/RetryDelayIncrement", 5.ToString()));
            var maxRetries = int.Parse(settingsQuoteUploads.GetSetting("Dotsure/Upload/MaxRetries", 2.ToString()));
            return new RetryStrategy(retryDelay, maxRetries);
        }

        private CommunicationMetadata Mail(AppSettingsEmailSetting appSettingsEmailSettings, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var address = settingsQuoteUploads.GetSetting("Dotsure/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            var errorAddress = settingsQuoteUploads.GetSetting("Dotsure/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za");
            var subject = settingsQuoteUploads.GetSetting("Dotsure/Upload/subject", "UPLOADING LEAD TO DOSTSURE");
            var templateName = settingsQuoteUploads.GetSetting("Dotsure/Upload/template", "LeadUploadTemplate");
            return new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = address,
                ErrorAddress = errorAddress,
                Subject = subject,
                TemplateName = templateName,
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = appSettingsEmailSettings
            });
        }

        
    }
}
