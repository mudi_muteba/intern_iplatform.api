﻿using System;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Workflow.Router;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Domain.Emailing.Factory;
using Domain.QuoteAcceptance.Extensions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using System.Linq;
using Domain.PolicyBindings.Metadata;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public class TelesureConfiguration : AbstractConfigurationReader, IGetConfiguredInsurerMetadata
    {

        public IDictionary<string, string> Telesureconfiguration
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    {"AUGPRD", "Telesureag"},
                    {"FIR", "Telesureffw"},
                    {"UNI", "Telesureunity"},
                    {"VIRS", "Telesurevirseker"},
                    {"BUD", "Telesurebudget"},
                    {"DIA", "Telesurediald"}
                };
            }
        }

        public QuoteAcceptedMetadata Generate(PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var config = Telesureconfiguration.FirstOrDefault(x => x.Key == productCode).Value;

            return new QuoteAcceptedMetadata(TaskMetadata(channelId, settingsQuoteUploads, config), Api(settingsQuoteUploads, config), quote, productCode, RetryStrategy(settingsQuoteUploads, config), Mail(quote.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads, config), channelId);
        }


        public PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var config = Telesureconfiguration.FirstOrDefault(x => x.Key == productCode).Value;

            return new PolicyBindingMetadata(TaskMetadata(channelId, settingsQuoteUploads, config), Api(settingsQuoteUploads, config), policyBinding, productCode, RetryStrategy(settingsQuoteUploads, config), Mail(policyBinding.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads, config), channelId);
        }

        private LeadTransferTelesureTaskMetadata TaskMetadata(Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads, string config)
        {
            var taskMetaData = new LeadTransferTelesureTaskMetadata { IRateUrl = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/iRateUrl", config)), ChannelId = channelId };
            taskMetaData.TeleSureInfos.Add(CreateRatingSetting(settingsQuoteUploads, config));
            return taskMetaData;
        }

        public LeadTransferTelesureTaskMetadataInfo CreateRatingSetting(List<SettingsQuoteUploadDto> settingsQuoteUploads, string config)
        {
            var agentCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/AgentCode", config));
            var brokerCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/BrokerCode", config));
            var insurerCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/InsurerCode", config));
            var productCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/ProductCode", config));
            var companyCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/CompanyCode", config));
            var uwCompanyCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/UwCompanyCode", config));
            var uwProductCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/UwProductCode", config));
            var schemeCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/SchemeCode", config));
            var subscriberCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/SubscriberCode", config));
            var authCode = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/AuthCode", config));
            var token = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/Token", config));

            return new LeadTransferTelesureTaskMetadataInfo(insurerCode, productCode, brokerCode, agentCode, schemeCode, subscriberCode,
               companyCode, uwCompanyCode, uwProductCode, authCode, token);
        }

        private ApiMetadata Api(List<SettingsQuoteUploadDto> settingsQuoteUploads, string config)
        {
            var url = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/ServiceURL", config));
            var user = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/User", config));
            var password = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/Password", config));

            return ApiMetadata.UrlWithCredentials(url, user, password);
        }
        private RetryStrategy RetryStrategy(List<SettingsQuoteUploadDto> settingsQuoteUploads, string config)
        {
            var retryDelay = int.Parse(settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/RetryDelayIncrement", config), 5.ToString()));
            var maxRetries = int.Parse(settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/MaxRetries", config), 2.ToString()));
            return new RetryStrategy(retryDelay, maxRetries);
        }
        private CommunicationMetadata Mail(AppSettingsEmailSetting appSettingsEmailSettings, List<SettingsQuoteUploadDto> settingsQuoteUploads, string config)
        {
            var address = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/AcceptanceEmailAddress", config), "monitoring@iplatform.co.za");
            var errorAddress = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/ErrorEmailAddress", config), "monitoring@iplatform.co.za");
            var subject = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/subject", config), "UPLOADING LEAD TO TELESURE");
            var templateName = settingsQuoteUploads.GetSetting(string.Format("{0}/Upload/template", config), "LeadUploadTemplate");
            return new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = address,
                ErrorAddress = errorAddress,
                Subject = subject,
                TemplateName = templateName,
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = appSettingsEmailSettings
            });
        }
    }
}
