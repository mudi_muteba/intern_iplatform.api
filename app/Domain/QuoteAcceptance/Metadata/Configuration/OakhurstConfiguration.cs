﻿using System;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Workflow.Router;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Domain.QuoteAcceptance.Extensions;
using Domain.Emailing.Factory;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using iPlatform.Api.DTOs.PolicyBindings;
using Domain.PolicyBindings.Metadata;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public class OakhurstConfiguration : AbstractConfigurationReader, IGetConfiguredInsurerMetadata
    {
        public QuoteAcceptedMetadata Generate(PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new QuoteAcceptedMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), quote, productCode, RetryStrategy(settingsQuoteUploads), Mail(quote.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        public PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new PolicyBindingMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), policyBinding, productCode, RetryStrategy(settingsQuoteUploads), Mail(policyBinding.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        private LeadTransferOakhurstTaskMetadata TaskMetadata(Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var agentCode = settingsQuoteUploads.GetSetting("Oakhurst/Upload/AgentCode");
            var agentEmail = settingsQuoteUploads.GetSetting("Oakhurst/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            var agentName = settingsQuoteUploads.GetSetting("Oakhurst/Upload/AgentName");
            var agentTelephone = settingsQuoteUploads.GetSetting("Oakhurst/Upload/AgentTelephone");
            var brokerCode = settingsQuoteUploads.GetSetting("Oakhurst/Upload/BrokerCode");
            var subBrokerCode = settingsQuoteUploads.GetSetting("Oakhurst/Upload/SubBrokerCode");
            var subPartnerCode = settingsQuoteUploads.GetSetting("Oakhurst/Upload/SubPartnerCode");
            var subPartnerName = settingsQuoteUploads.GetSetting("Oakhurst/Upload/SubPartnerName");
            var schemeName = settingsQuoteUploads.GetSetting("Oakhurst/Upload/SchemeName");
            var campaignId = settingsQuoteUploads.GetSetting("Oakhurst/Upload/CampaignId");
            return new LeadTransferOakhurstTaskMetadata(agentCode, agentEmail, agentName, agentTelephone, brokerCode,
                subBrokerCode, subPartnerCode, subPartnerName, schemeName, channelId, campaignId);
        }

        private ApiMetadata Api(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var url = settingsQuoteUploads.GetSetting("Oakhurst/Upload/ServiceURL");
            var user = settingsQuoteUploads.GetSetting("Oakhurst/Upload/User");
            var password = settingsQuoteUploads.GetSetting("Oakhurst/Upload/Password");

            return ApiMetadata.UrlWithCredentials(url, user, password);
        }

        private RetryStrategy RetryStrategy(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var retryDelay = int.Parse(settingsQuoteUploads.GetSetting("Oakhurst/Upload/RetryDelayIncrement", 5.ToString()));
            var maxRetries = int.Parse(settingsQuoteUploads.GetSetting("Oakhurst/Upload/MaxRetries", 2.ToString()));
            return new RetryStrategy(retryDelay, maxRetries);
        }

        private CommunicationMetadata Mail(AppSettingsEmailSetting appSettingsEmailSettings, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var address = settingsQuoteUploads.GetSetting("Oakhurst/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            var errorAddress = settingsQuoteUploads.GetSetting("Oakhurst/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za");
            var subject = settingsQuoteUploads.GetSetting("Oakhurst/Upload/subject", "UPLOADING LEAD TO OAKHURST");
            var templateName = settingsQuoteUploads.GetSetting("Oakhurst/Upload/template", "LeadUploadTemplate");
            return new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = address,
                ErrorAddress = errorAddress,
                Subject = subject,
                TemplateName = templateName,
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = appSettingsEmailSettings
            });
        }
    }
}
