﻿using System;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Workflow.Router;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Domain.Emailing.Factory;
using Domain.QuoteAcceptance.Extensions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using Domain.PolicyBindings.Metadata;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public class HollardConfiguration : AbstractConfigurationReader, IGetConfiguredInsurerMetadata
    {
      

        public QuoteAcceptedMetadata Generate(PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> SettingsQuoteUploads)
        {
            return new QuoteAcceptedMetadata(TaskMetadata(channelId, SettingsQuoteUploads), Api(SettingsQuoteUploads), quote, productCode, RetryStrategy(SettingsQuoteUploads), Mail(quote.ChannelInfo.EmailCommunicationSetting.Get(), SettingsQuoteUploads), channelId);
        }

        public PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new PolicyBindingMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), policyBinding, productCode, RetryStrategy(settingsQuoteUploads), Mail(policyBinding.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        private LeadTransferHollardTaskMetadata TaskMetadata(Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var schemeCode = settingsQuoteUploads.GetSetting("Hollard/Upload/SchemeCode");
            var campaign = settingsQuoteUploads.GetSetting("Hollard/Upload/Campaign");
            var campaignType = settingsQuoteUploads.GetSetting("Hollard/Upload/CampaignType");
            var productCode = settingsQuoteUploads.GetSetting("Hollard/Upload/ProductCode");
            var productCodeMotor = settingsQuoteUploads.GetSetting("Hollard/Upload/ProductCode/Motor");
            var productCodeAllRisks = settingsQuoteUploads.GetSetting("Hollard/Upload/ProductCode/AllRisks");
            var productCodeHo = settingsQuoteUploads.GetSetting(@"Hollard/Upload/ProductCode/HO");
            var productCodeHh = settingsQuoteUploads.GetSetting("Hollard/Upload/ProductCode/HH");
            var acceptanceEmailAddress = settingsQuoteUploads.GetSetting("Hollard/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            return new LeadTransferHollardTaskMetadata(schemeCode, campaign, campaignType, productCode, productCodeMotor,
                productCodeAllRisks, productCodeHo, productCodeHh, acceptanceEmailAddress, channelId);
        }

        private ApiMetadata Api(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var url = settingsQuoteUploads.GetSetting("Hollard/Upload/ServiceURL");
            var authentication = settingsQuoteUploads.GetSetting("Hollard/Upload/AuthToken");

            return ApiMetadata.UrlWithToken(authentication, url);
        }

        private RetryStrategy RetryStrategy(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var retryDelay = int.Parse(settingsQuoteUploads.GetSetting("Hollard/Upload/RetryDelayIncrement", 5.ToString()));
            var maxRetries = int.Parse(settingsQuoteUploads.GetSetting("Hollard/Upload/MaxRetries", 2.ToString()));
            return new RetryStrategy(retryDelay, maxRetries);
        }

        private CommunicationMetadata Mail(AppSettingsEmailSetting appSettingsEmailSettings, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var address = settingsQuoteUploads.GetSetting("Campaign/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            var errorAddress = settingsQuoteUploads.GetSetting("Campaign/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za");
            var subject = settingsQuoteUploads.GetSetting("Campaign/Upload/subject", "UPLOADING LEAD TO HOLLARD");
            var templateName = settingsQuoteUploads.GetSetting("Campaign/Upload/template", "LeadUploadTemplate");

            return new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = address,
                ErrorAddress = errorAddress,
                Subject = subject,
                TemplateName = templateName,
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = appSettingsEmailSettings
            });
        }
    }
}
