﻿using Domain.Emailing.Factory;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Workflow.Router;
using System;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using iPlatform.Api.DTOs.PolicyBindings;
using Domain.PolicyBindings.Metadata;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public class MiWayConfiguration : AbstractConfigurationReader, IGetConfiguredInsurerMetadata
    {
        public QuoteAcceptedMetadata Generate(iPlatform.Api.DTOs.Ratings.Quoting.PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new QuoteAcceptedMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), quote, productCode, RetryStrategy(settingsQuoteUploads), Mail(quote.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        public PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new PolicyBindingMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), policyBinding, productCode, RetryStrategy(settingsQuoteUploads), Mail(policyBinding.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        private LeadTransferToMiWayMetadata TaskMetadata(Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var agentFullName = settingsQuoteUploads.GetSetting("MiWay/Upload/AgentFullName");
            var agentId = settingsQuoteUploads.GetSetting("MiWay/Upload/AgentId");
            var userId = settingsQuoteUploads.GetSetting("MiWay/Upload/UserId");
            var productId = settingsQuoteUploads.GetSetting("MiWay/Upload/ProductId");
            var siteSequence = settingsQuoteUploads.GetSetting("MiWay/Upload/SiteSequence");
            var sourceSystemId = settingsQuoteUploads.GetSetting("MiWay/Upload/SourceSystemId");
            var apiKey = settingsQuoteUploads.GetSetting("MiWay/Upload/ApiKey");

            return new LeadTransferToMiWayMetadata(channelId, agentFullName, agentId, userId, productId, siteSequence, sourceSystemId, apiKey);
        }
        private ApiMetadata Api(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var url = settingsQuoteUploads.GetSetting("MiWay/Upload/ServiceURL");
            return ApiMetadata.UrlOnly(url);
        }
        private RetryStrategy RetryStrategy(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var retryDelay = int.Parse(settingsQuoteUploads.GetSetting("MiWay/Upload/RetryDelayIncrement", 5.ToString()));
            var maxRetries = int.Parse(settingsQuoteUploads.GetSetting("MiWay/Upload/MaxRetries", 2.ToString()));
            return new RetryStrategy(retryDelay, maxRetries);
        }

        private CommunicationMetadata Mail(AppSettingsEmailSetting appSettingsEmailSettings, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var address = settingsQuoteUploads.GetSetting("MiWay/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            var errorAddress = settingsQuoteUploads.GetSetting("MiWay/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za");
            var subject = settingsQuoteUploads.GetSetting("MiWay/Upload/subject", "UPLOADING LEAD TO MIWAY");
            var templateName = settingsQuoteUploads.GetSetting("MiWay/Upload/template", "LeadUploadTemplate");

            return new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = address,
                ErrorAddress = errorAddress,
                Subject = subject,
                TemplateName = templateName,
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = appSettingsEmailSettings
            });
        }
    }
}
