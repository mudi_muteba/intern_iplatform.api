﻿using System;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Workflow.Router;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Domain.Emailing.Factory;
using Domain.QuoteAcceptance.Extensions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using Domain.PolicyBindings.Metadata;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public class HollardEasyConfiguration : AbstractConfigurationReader, IGetConfiguredInsurerMetadata
    {
        public QuoteAcceptedMetadata Generate(PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> SettingsQuoteUploads)
        {
            return new QuoteAcceptedMetadata(TaskMetadata(channelId, SettingsQuoteUploads), Api(SettingsQuoteUploads), quote, productCode, RetryStrategy(SettingsQuoteUploads), Mail(quote.ChannelInfo.EmailCommunicationSetting.Get(), SettingsQuoteUploads), channelId);
        }

        public PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new PolicyBindingMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), policyBinding, productCode, RetryStrategy(settingsQuoteUploads), Mail(policyBinding.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        private LeadTransferHollardEasyTaskMetadata TaskMetadata(Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
           
            var productCode = settingsQuoteUploads.GetSetting("HollardEasy/Upload/ProductCode");
            var acceptanceEmailAddress = settingsQuoteUploads.GetSetting("HollardEasy/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            return new LeadTransferHollardEasyTaskMetadata(productCode, acceptanceEmailAddress, channelId);
        }

        private ApiMetadata Api(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var url = settingsQuoteUploads.GetSetting("HollardEasy/Upload/ServiceURL");
            var authentication = settingsQuoteUploads.GetSetting("HollardEasy/Upload/AuthToken");

            return ApiMetadata.UrlWithToken(authentication, url);
        }

        private RetryStrategy RetryStrategy(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var retryDelay = int.Parse(settingsQuoteUploads.GetSetting("HollardEasy/Upload/RetryDelayIncrement", 5.ToString()));
            var maxRetries = int.Parse(settingsQuoteUploads.GetSetting("HollardEasy/Upload/MaxRetries", 2.ToString()));
            return new RetryStrategy(retryDelay, maxRetries);
        }

        private CommunicationMetadata Mail(AppSettingsEmailSetting appSettingsEmailSettings, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var address = settingsQuoteUploads.GetSetting("Campaign/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            var errorAddress = settingsQuoteUploads.GetSetting("Campaign/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za");
            var subject = settingsQuoteUploads.GetSetting("Campaign/Upload/subject", "UPLOADING LEAD TO HOLLARD EASY");
            var templateName = settingsQuoteUploads.GetSetting("Campaign/Upload/template", "LeadUploadTemplate");

            return new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = address,
                ErrorAddress = errorAddress,
                Subject = subject,
                TemplateName = templateName,
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = appSettingsEmailSettings
            });
        }
    }
}
