﻿using System;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Workflow.Router;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages.Mail;
using Domain.Emailing.Factory;
using Domain.QuoteAcceptance.Extensions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using Domain.PolicyBindings.Metadata;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public class KingPriceConfiguration : AbstractConfigurationReader, IGetConfiguredInsurerMetadata
    {
        public QuoteAcceptedMetadata Generate(PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> SettingsQuoteUploads)
        {
            return new QuoteAcceptedMetadata(TaskMetadata(channelId, SettingsQuoteUploads), Api(SettingsQuoteUploads), quote, productCode, RetryStrategy(SettingsQuoteUploads), Mail(quote.ChannelInfo.EmailCommunicationSetting.Get(), SettingsQuoteUploads), channelId);
        }

        public PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new PolicyBindingMetadata(TaskMetadata(channelId, settingsQuoteUploads), Api(settingsQuoteUploads), policyBinding, productCode, RetryStrategy(settingsQuoteUploads), Mail(policyBinding.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        private LeadTransferKingPriceTaskMetadata TaskMetadata(Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var agentCode = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/AgentCode");
            var wsTrustClientIdentityServerUrl = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/SecurityEndpoint");
            var wsTrustClientServicesUrl = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/SecurityDestination");

            return new LeadTransferKingPriceTaskMetadata(wsTrustClientIdentityServerUrl, wsTrustClientServicesUrl, agentCode, channelId);
        }

        private ApiMetadata Api(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var url = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/ChannelEndpoint");
            var token = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/Token");
            var user = settingsQuoteUploads.GetSetting(@"Kingpriceob/Upload/UserName");
            var password = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/Password");
            return ApiMetadata.UrlWithTokenAndCredentials(token, url, user, password);
        }

        private RetryStrategy RetryStrategy(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var retryDelay = int.Parse(settingsQuoteUploads.GetSetting("Kingpriceob/Upload/RetryDelayIncrement", 5.ToString()));
            var maxRetries = int.Parse(settingsQuoteUploads.GetSetting("Kingpriceob/Upload/MaxRetries", 2.ToString()));
            return new RetryStrategy(retryDelay, maxRetries);
        }

        private CommunicationMetadata Mail(AppSettingsEmailSetting appSettingsEmailSettings, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var address = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            var errorAddress = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za");
            var subject = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/subject", "UPLOADING LEAD TO KING PRICE");
            var templateName = settingsQuoteUploads.GetSetting("Kingpriceob/Upload/template", "LeadUploadTemplate");
            return new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = address,
                ErrorAddress = errorAddress,
                Subject = subject,
                TemplateName = templateName,
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = appSettingsEmailSettings,
            });
        }
    }
}
