﻿using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public static class ConfigurationHelper
    {
        public static string GetSetting(this List<SettingsQuoteUploadDto> settings, string key)
        {
            var setting = settings.FirstOrDefault(x => x.SettingName == key);
            return setting != null ? setting.SettingValue : string.Empty;
        }

        public static string GetSetting(this List<SettingsQuoteUploadDto> settings, string key, string value)
        {
            var setting = settings.FirstOrDefault(x => x.SettingName == key);
            return setting != null ? setting.SettingValue: value;
        }





    }
}
