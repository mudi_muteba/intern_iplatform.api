﻿using System;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Workflow.Router;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Domain.Emailing.Factory;
using Domain.QuoteAcceptance.Extensions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using iPlatform.Api.DTOs.PolicyBindings;
using Domain.PolicyBindings.Metadata;

namespace Domain.QuoteAcceptance.Metadata.Configuration
{
    public class SAUConfiguration : AbstractConfigurationReader, IGetConfiguredInsurerMetadata
    {
        public QuoteAcceptedMetadata Generate(PublishQuoteUploadMessageDto quote, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new QuoteAcceptedMetadata(TaskMetadata(channelId), Api(settingsQuoteUploads), quote, productCode, RetryStrategy(settingsQuoteUploads), Mail(quote.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        public PolicyBindingMetadata Generate(PolicyBindingRequestDto policyBinding, string productCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            return new PolicyBindingMetadata(TaskMetadata(channelId), Api(settingsQuoteUploads), policyBinding, productCode, RetryStrategy(settingsQuoteUploads), Mail(policyBinding.ChannelInfo.EmailCommunicationSetting.Get(), settingsQuoteUploads), channelId);
        }

        private LeadTransferSAUTaskMetadata TaskMetadata(Guid channelId)
        {

            return new LeadTransferSAUTaskMetadata(channelId);
        }

        private ApiMetadata Api(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var url = settingsQuoteUploads.GetSetting("SAU/Upload/ServiceURL");
            var brokerCode = settingsQuoteUploads.GetSetting("SAU/Upload/BrokerCode");
            var userName = settingsQuoteUploads.GetSetting("SAU/Upload/UserName");
            var password = settingsQuoteUploads.GetSetting("SAU/Upload/Password");
            var fspCode = settingsQuoteUploads.GetSetting("SAU/Upload/fspCode");
            var appName = settingsQuoteUploads.GetSetting("SAU/Upload/AppName");
            var companyCode = settingsQuoteUploads.GetSetting("SAU/Upload/CompanyCode");
            return new ApiMetadata(url, userName, password, brokerCode, companyCode, fspCode, appName);
        }

        private RetryStrategy RetryStrategy(List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var retryDelay = int.Parse(settingsQuoteUploads.GetSetting("SAU/RetryDelayIncrement", 5.ToString()));
            var maxRetries = int.Parse(settingsQuoteUploads.GetSetting("SAU/MaxRetries", 2.ToString()));
            return new RetryStrategy(retryDelay, maxRetries);
        }

        private CommunicationMetadata Mail(AppSettingsEmailSetting appSettingsEmailSettings, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            var address = settingsQuoteUploads.GetSetting("SAU/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za");
            var erroraddress = settingsQuoteUploads.GetSetting("SAU/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za");
            var subject = settingsQuoteUploads.GetSetting("SAU/Upload/subject", "UPLOADING LEAD TO SAU");
            var templateName = settingsQuoteUploads.GetSetting("SAU/Upload/template", "LeadUploadTemplate");
            return new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = address,
                ErrorAddress = erroraddress,
                Subject = subject,
                TemplateName = templateName,
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = appSettingsEmailSettings
            });
        }
    }
}
