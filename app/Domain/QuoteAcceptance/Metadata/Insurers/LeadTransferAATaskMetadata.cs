﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferAATaskMetadata : ITaskMetadata
    {
        public LeadTransferAATaskMetadata(Guid channelId)
        {
            ChannelId = channelId;
        }

        [DataMember]
        public readonly Guid ChannelId;
        public override string ToString()
        {
            return string.Join(", ",
                typeof(LeadTransferAATaskMetadata).GetFields().Select(s => string.Format("{0}: {1}", s.Name, s.GetValue(this))));
        }
    }
}
