﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferCampaignTaskMetadata : ITaskMetadata
    {
        public LeadTransferCampaignTaskMetadata(string productCode, string campaignReference, string systemReference, string leadSourceReference,
            string branchCode,
            string branchName, string groupCode, string groupName, string userName, string userEmailAddress, string accptanceEmailAddress, Guid channelId)
        {
            ProductCode = productCode;
            CampaignSystemReference = campaignReference;
            SystemReference = systemReference;
            LeadSourceReference = leadSourceReference;
            BranchCode = branchCode;
            BranchName = branchName;
            GroupCode = groupCode;
            GroupName = groupName;
            UserName = userName;
            UserEmailAddress = userEmailAddress;
            AcceptanceEmailAddress = accptanceEmailAddress;
            ChannelId = channelId;
        }

        [DataMember] public readonly string ProductCode;
        [DataMember] public readonly string CampaignSystemReference;
        [DataMember] public readonly string SystemReference;
        [DataMember] public readonly string LeadSourceReference;
        [DataMember] public readonly string BranchCode;
        [DataMember] public readonly string BranchName;
        [DataMember] public readonly string GroupCode;
        [DataMember] public readonly string GroupName;
        [DataMember] public readonly string UserName;
        [DataMember] public readonly string UserEmailAddress;
        [DataMember] public readonly string AcceptanceEmailAddress;
        [DataMember] public readonly Guid ChannelId;

        public override string ToString()
        {
            return string.Join(", ", typeof(LeadTransferCampaignTaskMetadata).GetFields().Select(s => string.Format("{0}: {1}", s.Name, s.GetValue(this))));
        }
    }
}