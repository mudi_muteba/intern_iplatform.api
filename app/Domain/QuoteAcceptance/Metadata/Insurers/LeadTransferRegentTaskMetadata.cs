﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferRegentTaskMetadata : ITaskMetadata
    {
        public LeadTransferRegentTaskMetadata(string acceptanceEmailAddress, Guid channelId)
        {
            AcceptanceEmailAddress = acceptanceEmailAddress;
            ChannelId = channelId;
        }

        [DataMember] public readonly string AcceptanceEmailAddress;

        [DataMember] public readonly Guid ChannelId;

        public override string ToString()
        {
            return string.Join(", ",
                typeof (LeadTransferRegentTaskMetadata).GetFields().Select(s => string.Format("{0}: {1}", s.Name, s.GetValue(this))));
        }
    }
}