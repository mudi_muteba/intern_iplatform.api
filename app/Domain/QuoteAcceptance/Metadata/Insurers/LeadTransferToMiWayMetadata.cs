﻿using System;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferToMiWayMetadata : ITaskMetadata
    {
        public LeadTransferToMiWayMetadata(Guid channelId, string agentFullName, string agentId, string userId, string productId, string siteSequence, string sourceSystemId, string apiKey)
        {
            ChannelId = channelId;
            AgentFullName = agentFullName;
            UserId = userId;
            ProductId = productId;
            ApiKey = apiKey;
            SourceSystemId = sourceSystemId;

            SiteSequence = 0;
            Int32.TryParse(siteSequence, out SiteSequence);

            AgentId = 0;
            Int32.TryParse(agentId, out AgentId);
        }

        [DataMember]
        public readonly Guid ChannelId;
        [DataMember]
        public readonly string AgentFullName;
        [DataMember]
        public readonly int AgentId;
        [DataMember]
        public readonly string UserId;
        [DataMember]
        public readonly string ProductId;
        [DataMember]
        public readonly int SiteSequence;
        [DataMember]
        public readonly string SourceSystemId;
        [DataMember]
        public readonly string ApiKey;
    }
}
