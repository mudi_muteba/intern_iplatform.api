﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferTelesureTaskMetadata : ITaskMetadata
    {

        public LeadTransferTelesureTaskMetadata()
        {
            TeleSureInfos = new List<LeadTransferTelesureTaskMetadataInfo>();
        }

        public LeadTransferTelesureTaskMetadata(string irateUrl, Guid channelId, LeadTransferTelesureTaskMetadataInfo info)
        {
            TeleSureInfos = new List<LeadTransferTelesureTaskMetadataInfo>();
            TeleSureInfos.Add(info);

            IRateUrl = irateUrl;
            ChannelId = channelId;
        }

        [DataMember]
        public string IRateUrl { get; set; }
        [DataMember]
        public Guid ChannelId { get; set; }
        [DataMember]
        public List<LeadTransferTelesureTaskMetadataInfo> TeleSureInfos { get; set; }

    }
}
