﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferHollardEasyTaskMetadata : ITaskMetadata
    {
        public LeadTransferHollardEasyTaskMetadata()
        {

        }

        public LeadTransferHollardEasyTaskMetadata(string productCode, string acceptancEmailAddress, Guid channelId)
        {
            ProductCode = productCode;
            AcceptancEmailAddress = acceptancEmailAddress;
            ChannelId = channelId;
        }

        [DataMember] public readonly string ProductCode;
        [DataMember] public readonly string AcceptancEmailAddress;
        [DataMember] public readonly Guid ChannelId;

        public override string ToString()
        {
            return string.Join(", ", typeof(LeadTransferHollardEasyTaskMetadata).GetFields().Select(s => string.Format("{0}: {1}", s.Name, s.GetValue(this))));
        }
    }
}