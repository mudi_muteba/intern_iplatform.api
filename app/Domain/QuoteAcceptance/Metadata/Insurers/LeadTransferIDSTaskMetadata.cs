﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferIdsTaskMetadata : ITaskMetadata
    {
        public LeadTransferIdsTaskMetadata(Guid channelId,string externalReference)
        {
            ChannelId = channelId;
            ExternalReference = externalReference;
        }

        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly string ExternalReference;

        public override string ToString()
        {
            return string.Join(", ",
                typeof(LeadTransferIdsTaskMetadata).GetFields().Select(s => string.Format("{0}: {1}", s.Name, s.GetValue(this))));
        }
    }
}
