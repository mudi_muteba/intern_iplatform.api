﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferTelesureTaskMetadataInfo
    {

        public LeadTransferTelesureTaskMetadataInfo() { }
        public LeadTransferTelesureTaskMetadataInfo(string insurerCode, string productCode, string brokerCode, string agentCode, string schemeCode,
            string subscriberCode, string companyCode, string uwCompanyCode, string uwProductCode, string authCode, string token)
        {
            InsurerCode = insurerCode;
            ProductCode = productCode;
            BrokerCode = brokerCode;
            AgentCode = agentCode;
            SchemeCode = schemeCode;
            SubscriberCode = subscriberCode;
            CompanyCode = companyCode;
            UwCompanyCode = uwCompanyCode;
            UwProductCode = uwProductCode;
            AuthCode = authCode;
            Token = token;
        }
        [DataMember]
        public string InsurerCode { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
        [DataMember]
        public string BrokerCode { get; set; }
        [DataMember]
        public string AgentCode { get; set; }
        [DataMember]
        public string SchemeCode { get; set; }
        [DataMember]
        public string SubscriberCode { get; set; }
        [DataMember]
        public string CompanyCode { get; set; }
        [DataMember]
        public string UwCompanyCode { get; set; }
        [DataMember]
        public string UwProductCode { get; set; }
        [DataMember]
        public string AuthCode { get; set; }
        [DataMember]
        public string Token { get; set; }


        public override string ToString()
        {
            return string.Join(", ", typeof(LeadTransferTelesureTaskMetadataInfo).GetFields().Select(s => string.Format("{0}: {1}", s.Name, s.GetValue(this))));
        }
    }
}
