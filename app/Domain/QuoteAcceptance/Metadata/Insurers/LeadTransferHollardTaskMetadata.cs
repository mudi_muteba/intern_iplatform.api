﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferHollardTaskMetadata : ITaskMetadata
    {
        public LeadTransferHollardTaskMetadata()
        {

        }

        public LeadTransferHollardTaskMetadata(string schemeCode, string campaign, string campaignType,
            string productCode, string productCodeMotor, string productCodeAllRisks, string productCodeHo, string productCodeHh,
            string acceptancEmailAddress, Guid channelId)
        {

            SchemeCode = schemeCode;
            Campaign = campaign;
            CampaignType = campaignType;
            ProductCode = productCode;
            ProductCodeMotor = productCodeMotor;
            ProductCodeAllRisks = productCodeAllRisks;
            ProductCodeHo = productCodeHo;
            ProductCodeHh = productCodeHh;
            AcceptancEmailAddress = acceptancEmailAddress;
            ChannelId = channelId;
        }


        [DataMember] public readonly string SchemeCode;
        [DataMember] public readonly string Campaign;
        [DataMember] public readonly string CampaignType;
        [DataMember] public readonly string ProductCode;
        [DataMember] public readonly string ProductCodeMotor;
        [DataMember] public readonly string ProductCodeAllRisks;
        [DataMember] public readonly string ProductCodeHo;
        [DataMember] public readonly string ProductCodeHh;
        [DataMember] public readonly string AcceptancEmailAddress;
        [DataMember] public readonly Guid ChannelId;

        public override string ToString()
        {
            return string.Join(", ", typeof(LeadTransferHollardTaskMetadata).GetFields().Select(s => string.Format("{0}: {1}", s.Name, s.GetValue(this))));
        }
    }
}