﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Domain.Admin.Queries;
using iPlatform.Api.DTOs.Campaigns;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferOakhurstTaskMetadata : ITaskMetadata
    {
        public LeadTransferOakhurstTaskMetadata(string agentCode, string agentEmail, string agentName, string agentTelephone, string brokerCode,
            string subBrokerCode, string subPartnerCode, string subPartnerName, string schemeName, Guid channelId, string campaignId)
        {
            AgentCode = agentCode;
            AgentEmail = agentEmail;
            AgentName = agentName;
            AgentTelephone = agentTelephone;
            BrokerCode = brokerCode;
            SubBrokerCode = subBrokerCode;
            SubPartnerCode = subPartnerCode;
            SubPartnerName = subPartnerName;
            SchemeName = schemeName;
            ChannelId = channelId;
            CampaignId = campaignId;
        }

    
        [DataMember] public readonly string AgentCode;
        [DataMember] public readonly string AgentEmail;
        [DataMember] public readonly string AgentName;
        [DataMember] public readonly string AgentTelephone;
        [DataMember] public readonly string BrokerCode;
        [DataMember] public readonly string SubBrokerCode;
        [DataMember] public readonly string SubPartnerCode;
        [DataMember] public readonly string SubPartnerName;
        [DataMember] public readonly string SchemeName;
        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly string CampaignId;

        public override string ToString()
        {
            return string.Join(", ", typeof(LeadTransferOakhurstTaskMetadata).GetFields().Select(s => string.Format("{0}: {1}", s.Name, s.GetValue(this))));
        }
    }
}
