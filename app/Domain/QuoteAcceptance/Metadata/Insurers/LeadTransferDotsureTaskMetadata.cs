﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferDotsureTaskMetadata : ITaskMetadata
    {
        public LeadTransferDotsureTaskMetadata(string agentCode, string agentEmail, string agentName,
            string agentTelephone, string brokerCode, string subBrokerCode, string subPartnerCode, string subPartnerName,
            string schemeName, string acceptanceEmailAddress, Guid channelId)
        {
            AgentCode = agentCode;
            AgentEmail = agentEmail;
            AgentName = agentName;
            AgentTelephone = agentTelephone;
            BrokerCode = brokerCode;
            SubBrokerCode = subBrokerCode;
            SubPartnerCode = subPartnerCode;
            SubPartnerName = subPartnerName;
            SchemeName = schemeName;
            AcceptanceEmailAddress = acceptanceEmailAddress;
            ChannelId = channelId;
        }

        [DataMember] public readonly string AgentCode;
        [DataMember] public readonly string AgentEmail;
        [DataMember] public readonly string AgentName;
        [DataMember] public readonly string AgentTelephone;
        [DataMember] public readonly string BrokerCode;
        [DataMember] public readonly string SubBrokerCode;
        [DataMember] public readonly string SubPartnerCode;
        [DataMember] public readonly string SubPartnerName;
        [DataMember] public readonly string SchemeName;
        [DataMember] public readonly string AcceptanceEmailAddress;
        [DataMember] public readonly Guid ChannelId;

        public override string ToString()
        {
            return string.Join(", ", typeof(LeadTransferDotsureTaskMetadata).GetFields().Select(s => string.Format("{0}: {1}", s.Name, s.GetValue(this))));
        }
    }
}