﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Insurers
{
    [DataContract]
    public class LeadTransferKingPriceTaskMetadata : ITaskMetadata
    {
        public LeadTransferKingPriceTaskMetadata(string wsTrustClientIdentityServerUrl, string wsTrustClientServicesUrl, string agentCode, Guid channelId
            )
        {
            WsTrustClientServicesUrl = wsTrustClientServicesUrl;
            WsTrustClientIdentityServerUrl = wsTrustClientIdentityServerUrl;
            AgentCode = agentCode;
            ChannelId = channelId;
        }
      
        [DataMember] public readonly string WsTrustClientIdentityServerUrl;
        [DataMember] public readonly string WsTrustClientServicesUrl;
        [DataMember] public readonly string AgentCode;
        [DataMember] public readonly Guid ChannelId;
        public override string ToString()
        {
            return string.Join(", ", typeof(LeadTransferKingPriceTaskMetadata).GetFields().Select(s => string.Format("{0}: {1}", s.Name,  s.GetValue(this))));
        }
    }
}