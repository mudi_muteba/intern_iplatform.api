﻿using System;
using System.Runtime.Serialization;
using Domain.QuoteAcceptance.Infrastructure;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages.Mail;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata
{
    public abstract class AbstractMetadata : ITaskMetadata
    {
        public AbstractMetadata() { }
        public AbstractMetadata(ITaskMetadata taskMetadata, ApiMetadata apiMetadata,  string productCode, 
            RetryStrategy retryStrategy, CommunicationMetadata mail, Guid channelId)
        {
            TaskMetadata = taskMetadata;
            ApiMetadata = apiMetadata;
            ProductCode = productCode;
            RetryStrategy = retryStrategy;
            Mail = mail;
            ChannelId = channelId;
        }

        [DataMember]
        public ITaskMetadata TaskMetadata { get; set; }

        [DataMember]
        public ApiMetadata ApiMetadata { get; set; }

        [DataMember]
        public RetryStrategy RetryStrategy { get;  set; }
        [DataMember]
        public CommunicationMetadata Mail { get; set; }
        [DataMember]
        public Guid ChannelId { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
    }
}