﻿using Domain.QuoteAcceptance.Metadata.Configuration;

namespace Domain.QuoteAcceptance.Metadata
{
    public static class MetadataConfigurationReader
    {
        public static readonly HollardConfiguration Hollard;
        public static readonly DotsureConfiguration Dotsure;
        public static readonly CampaignConfiguration Campaign;
        public static readonly KingPriceConfiguration KingPrice;
        public static readonly KingPriceV2Configuration KingPriceV2;
        public static readonly OakhurstConfiguration Oakhurst;
        public static readonly RegentConfiguration Regent;
        public static readonly TelesureConfiguration Telesure;
        public static readonly IdsConfiguration Ids;
        public static readonly SAUConfiguration SAU;
        public static readonly MiWayConfiguration MiWay;
        public static readonly AAConfiguration AA;
        public static readonly HollardEasyConfiguration HollardEasy;

        static MetadataConfigurationReader()
        {
            Hollard = new HollardConfiguration();
            Dotsure = new DotsureConfiguration();
            Campaign = new CampaignConfiguration();
            KingPrice = new KingPriceConfiguration();
            KingPriceV2 = new KingPriceV2Configuration();
            Oakhurst = new OakhurstConfiguration();
            Regent = new RegentConfiguration();
            Telesure = new TelesureConfiguration();
            Ids = new IdsConfiguration();
            SAU = new SAUConfiguration();
            MiWay = new MiWayConfiguration();
            AA = new AAConfiguration();
            HollardEasy = new HollardEasyConfiguration();

        }
    }
}
