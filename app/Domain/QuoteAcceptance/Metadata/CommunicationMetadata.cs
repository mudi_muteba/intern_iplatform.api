﻿using System.Runtime.Serialization;
using Domain.QuoteAcceptance.Leads;
using Workflow.Messages.Plan.Tasks;
using Domain.Emailing.Factory;

namespace Domain.QuoteAcceptance.Metadata
{
    public class CommunicationMetadata : ITaskMetadata
    {
        public CommunicationMetadata()
        {
        }

        public CommunicationMetadata(LeadTransferralCommsMessage message)
        {
            CommunicationMessage = message;
        }

        [DataMember]
        public LeadTransferralCommsMessage CommunicationMessage { get; set; }

    }
}