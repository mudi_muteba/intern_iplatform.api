﻿using System;
using System.Runtime.Serialization;
using Domain.QuoteAcceptance.Infrastructure;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages.Mail;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata
{
    public class QuoteAcceptedMetadata : AbstractMetadata
    {
        public QuoteAcceptedMetadata() : base()
        {
            
        }
        public QuoteAcceptedMetadata(ITaskMetadata taskMetadata, ApiMetadata apiMetadata, PublishQuoteUploadMessageDto quote, string productCode,
            RetryStrategy retryStrategy, CommunicationMetadata mail, Guid channelId)
            : base(taskMetadata, apiMetadata, productCode, retryStrategy, mail, channelId)
        {
            Quote = quote;
        }


        [DataMember] 
        public PublishQuoteUploadMessageDto Quote { get; set; }
    }
}