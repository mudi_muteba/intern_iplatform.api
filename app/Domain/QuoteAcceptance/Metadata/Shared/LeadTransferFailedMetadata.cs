﻿using System;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Monitoring;
using Shared;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class LeadTransferFailedMetadata : ITaskMetadata
    {
        public LeadTransferFailedMetadata()
        {
        }

        public LeadTransferFailedMetadata(string message, Guid? channelId = null)
        {
            Message = message;
            Status = Status.Failure;
            Priority = Priority.Critical;
            ChannelId = channelId ?? Guid.Empty;
            Date = SystemTime.Now();
        }

        [DataMember] public readonly string Message;
        [DataMember] public readonly Status Status;
        [DataMember] public readonly Priority Priority;
        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly DateTime Date;
    }
}
