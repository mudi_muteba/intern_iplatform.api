﻿using iPlatform.Api.DTOs.Ratings.Quoting;
using System;
using System.Runtime.Serialization;
using ToolBox.Communication.Interfaces;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    [DataContract]
    public class LeadTransferCommunicationData : ICommunicationMessageData
    {
        public LeadTransferCommunicationData()
        {

        }
        public LeadTransferCommunicationData(string message, string data,  string date, string ratingRequestId)
        {
            Message = message;
            Date = date;
            RatingRequestId = ratingRequestId;
            Data = data;
        }
        public LeadTransferCommunicationData(PublishQuoteUploadMessageDto quote, string insurer, string product, string insurerReference = "")
        {
            Quote = quote;
            Insurer = insurer;
            Product = product;
            InsurerReference = insurerReference;
        }

        [DataMember]
        public dynamic Message { get; private set; }
        [DataMember]
        public dynamic Data { get; private set; }

        [DataMember]
        public dynamic Date { get; private set; }

        [DataMember]
        public dynamic RatingRequestId { get; private set; }

        [DataMember]
        public PublishQuoteUploadMessageDto Quote { get; private set; }
        [DataMember]
        public string Insurer { get; private set; }
        [DataMember]
        public string Product { get; private set; }
        [DataMember]
        public string InsurerReference { get; set; }




    }
}
