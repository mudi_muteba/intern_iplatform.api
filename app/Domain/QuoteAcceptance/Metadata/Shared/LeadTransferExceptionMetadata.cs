﻿using System;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Monitoring;
using Shared;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class LeadTransferExceptionMetadata : ITaskMetadata
    {
        public LeadTransferExceptionMetadata(string exceptionMessage, string prettyException, string insurer, Guid? channelId = null)
        {
            ExceptionMessage = exceptionMessage;
            PrettyException = prettyException;
            Insurer = insurer;
            Status = Status.Failure;
            Priority = Priority.Critical;
            ChannelId = channelId ?? Guid.Empty;
            Date = SystemTime.Now();
        }

        [DataMember] public readonly string ExceptionMessage;
        [DataMember] public readonly string PrettyException;
        [DataMember] public readonly string Insurer;
        [DataMember] public readonly Status Status;
        [DataMember] public readonly Priority Priority;
        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly DateTime Date;
    }
}