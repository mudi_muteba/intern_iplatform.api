﻿using System;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Monitoring;
using Shared;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class LeadTransferInformationMetadata : ITaskMetadata
    {
        public LeadTransferInformationMetadata()
        {
        }

        public LeadTransferInformationMetadata(string message, Priority priority, Guid? channelId = null)
        {
            Message = message;
            Priority = priority;
            ChannelId = channelId ?? Guid.Empty;
            Date = SystemTime.Now();
        }

        [DataMember] public readonly string Message;
        [DataMember] public readonly Priority Priority;
        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly DateTime Date;
    }
}
