﻿using System;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Monitoring;
using Shared;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class LeadTransferFinishedMetadata : ITaskMetadata
    {
        public LeadTransferFinishedMetadata()
        {
        }

        public LeadTransferFinishedMetadata(string message, Guid? channelId = null)
        {
            Message = message;
            Priority = Priority.Normal;
            ChannelId = channelId ?? Guid.Empty;
            FinishedDateTime = SystemTime.Now();
        }

        [DataMember] public readonly string Message;
        [DataMember] public readonly Priority Priority;
        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly DateTime FinishedDateTime;
    }
}