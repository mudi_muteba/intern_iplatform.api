﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Monitoring;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Shared;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class CustomerSatisfactionSurveyMetadata : ITaskMetadata
    {
        public CustomerSatisfactionSurveyMetadata()
        {
        }

        public CustomerSatisfactionSurveyMetadata(CustomerSatisfactionSurveyRequestDto dto)
        {
            CustomerSatisfactionSurveyRequest = dto;
        }

        public CustomerSatisfactionSurveyRequestDto CustomerSatisfactionSurveyRequest { get; set; }
    }
}