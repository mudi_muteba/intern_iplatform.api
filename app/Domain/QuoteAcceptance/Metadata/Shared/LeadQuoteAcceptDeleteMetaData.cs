﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Monitoring;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Shared;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class LeadQuoteAcceptDeleteMetaData : ITaskMetadata
    {
        public LeadQuoteAcceptDeleteMetaData()
        {
        }

        public LeadQuoteAcceptDeleteMetaData(int quoteId)
        {
            QuoteId = quoteId;
        }

        public int QuoteId { get; set; }
    }
}