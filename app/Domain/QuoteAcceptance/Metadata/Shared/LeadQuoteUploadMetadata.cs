﻿using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class LeadQuoteUploadMetadata : ITaskMetadata
    {
        public LeadQuoteUploadMetadata() { }

        public LeadQuoteUploadMetadata(CreateQuoteUploadLogDto dto)
        {
            CreateQuoteUploadLogDto = dto;
        }

        public CreateQuoteUploadLogDto CreateQuoteUploadLogDto { get; set; }
    }
}