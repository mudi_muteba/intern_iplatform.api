﻿using System;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Monitoring;
using Shared;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    [DataContract]
    public class LeadTransferStartedMetadata : ITaskMetadata
    {
        public LeadTransferStartedMetadata()
        {
        }

        public LeadTransferStartedMetadata(string message, string parameters, Guid? channelId = null)
        {
            Message = message;
            Parameters = parameters;
            Priority = Priority.Normal;
            ChannelId = channelId ?? Guid.Empty;
            StarDateTime = SystemTime.Now();
        }

        [DataMember] public readonly string Parameters;
        [DataMember] public readonly string Message;
        [DataMember] public readonly Priority Priority;
        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly DateTime StarDateTime;
    }
}
