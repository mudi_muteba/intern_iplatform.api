﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Monitoring;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Shared;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class LeadTransferSuccessfulMetadata : ITaskMetadata
    {
        public LeadTransferSuccessfulMetadata()
        {
        }

        public LeadTransferSuccessfulMetadata(string message, Guid? channelId = null)
        {

            Message = message;
            Status = Status.Success;
            Priority = Priority.Normal;
            ChannelId = channelId ?? Guid.Empty;
            Date = SystemTime.Now();
        }


        [DataMember] public readonly string Message;
        [DataMember] public readonly Status Status;
        [DataMember] public readonly Priority Priority;
        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly DateTime Date;
    }
}