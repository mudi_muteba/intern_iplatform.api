﻿using ToolBox.Communication.Interfaces;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class CommsMessageNullData : ICommunicationMessageData
    {
        public CommsMessageNullData()
        {
            
        }
    }
}
