﻿using System.Configuration;

namespace Domain.QuoteAcceptance.Metadata
{
    public abstract class AbstractConfigurationReader
    {
        protected string AppSetting(string key, string defaultValue)
        {
            var value = ConfigurationManager.AppSettings[key];

            return string.IsNullOrEmpty(value) ? defaultValue : value;
        }

        protected string ConnectionString(string key, string defaultValue)
        {
            var value = ConfigurationManager.ConnectionStrings[key];

            if (value == null)
                return defaultValue;

            return string.IsNullOrEmpty(value.ConnectionString) ? defaultValue : value.ConnectionString;
        }


    }
}
