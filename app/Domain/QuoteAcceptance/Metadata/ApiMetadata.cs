﻿using System.Runtime.Serialization;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata
{
    [DataContract]
    public class ApiMetadata : ITaskMetadata
    {
        public ApiMetadata()
        {

        }

        public static ApiMetadata UrlWithCompleteInfo(string authenticationToken, string url, string user, string password, string brokerCode, string companyCode)
        {
            return new ApiMetadata(authenticationToken, url, user, password, brokerCode, companyCode);
        }

        public static ApiMetadata UrlWithTokenAndCredentials(string authenticationToken, string url, string user, string password)
        {
            return new ApiMetadata(authenticationToken, url, user, password);
        }

        public static ApiMetadata UrlWithToken(string authenticationToken, string url)
        {
            return new ApiMetadata(authenticationToken, url, null, null);
        }

        public static ApiMetadata UrlOnly(string url)
        {
            return new ApiMetadata(null, url, null, null);
        }

        public static ApiMetadata UrlWithCredentials(string url, string user, string password)
        {
            return new ApiMetadata(null, url, user, password);
        }

        public static ApiMetadata UrlWithTokenAndAgent(string url, string token)
        {
            return new ApiMetadata(token, url, null, null);
        }

        public static ApiMetadata UrlWithCredentialsAndBrokerCode(string url, string user, string password, string brokerCode)
        {
            return new ApiMetadata(null, url, user, password, brokerCode, null);
        }

        public ApiMetadata(string authenticationToken, string url, string user, string password)
        {
            AuthenticationToken = authenticationToken ?? string.Empty;
            Url = url ?? string.Empty;
            User = user ?? string.Empty;
            Password = password ?? string.Empty;
        }

        public ApiMetadata(string authenticationToken, string url, string user, string password, string brokerCode, string companyCode)
        {
            AuthenticationToken = authenticationToken ?? string.Empty;
            Url = url ?? string.Empty;
            User = user ?? string.Empty;
            Password = password ?? string.Empty;
            BrokerCode = brokerCode ?? string.Empty;
            CompanyCode = companyCode ?? string.Empty;
        }


        public ApiMetadata(string url, string user, string password, string brokerCode, string companyCode, string fspCode, string appName)
        {
            Url = url ?? string.Empty;
            User = user ?? string.Empty;
            Password = password ?? string.Empty;
            BrokerCode = brokerCode ?? string.Empty;
            CompanyCode = companyCode ?? string.Empty;
            FspCode = fspCode ?? string.Empty;
            AppName = appName ?? string.Empty;
        }

        [DataMember] public readonly string AuthenticationToken;
        [DataMember] public readonly string Url;
        [DataMember] public readonly string Password;
        [DataMember] public readonly string User;
        [DataMember] public readonly string AgentCode;
        [DataMember] public readonly string BrokerCode;
        [DataMember] public readonly string CompanyCode;
        [DataMember] public readonly string FspCode;
        [DataMember] public readonly string AppName;
    }
}