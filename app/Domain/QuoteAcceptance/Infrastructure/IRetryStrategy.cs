﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Castle.Core.Internal;
using Shared.Retry;
using Workflow.Messages;

namespace Domain.QuoteAcceptance.Infrastructure
{
    public interface IRetryStrategy
    {
        IRetryStrategy While(Func<bool> condition);
        IRetryStrategy OnStart(Action action);
        IRetryStrategy OnSuccess(Action action);
        IRetryStrategy OnFailure(Action action);
        IRetryStrategy OnFinish(Action<object> action);
        TResult Execute<TResult>(Func<TResult> action);
        int MaxRetries { get; }
        int DelayIncrement { get; }
        IEnumerable<KeyValuePair<int, TimeSpan>> RetryCountDelayScale { get; }
    }

    public class RetryStrategy : IRetryStrategy
    {
        private readonly IEnumerable<int> _range;
        private TimeSpan _nextIncrement;
        private Func<bool> _condition;
        private Action _onSuccess;
        private Action _onFailure;
        private Action<object> _onFinish;
        private Action _onStart;
        private readonly IEnumerable<KeyValuePair<int, TimeSpan>> _retryCountDelayScale;

        public RetryStrategy(int delayIncrement, int maxRetries)
        {
            MaxRetries = maxRetries;
            DelayIncrement = delayIncrement;
            _range = Enumerable.Range(0, (maxRetries + 1));
            _nextIncrement = DelayIncrement.Seconds();
            _retryCountDelayScale = BuildDelay();
        }

        public IEnumerable<KeyValuePair<int, TimeSpan>> RetryCountDelayScale
        {
            get { return _retryCountDelayScale; }
        }

        private IEnumerable<KeyValuePair<int, TimeSpan>> BuildDelay()
        {
            var scales = new List<KeyValuePair<int, TimeSpan>>();
            _range.ForEach(f =>
            {
                var scale = new KeyValuePair<int, TimeSpan>(f, _nextIncrement);
                _nextIncrement = (_nextIncrement.Seconds + DelayIncrement).Seconds();
                scales.Add(scale);
            });
            return scales;
        }

        public IRetryStrategy While(Func<bool> condition)
        {
            _condition = condition;
            return this;
        }

        public TResult Execute<TResult>(Func<TResult> action)
        {
            Start();
            var result = default(TResult);
            var attempt = 0;
            while (TryAgain(attempt))
            {
                result = action();
                if (TryAgain(attempt))
                    Thread.Sleep(GetDelay(attempt));

                attempt++;

                if (_condition())
                    Fail();
            }

            if (_condition() || MaxRetries < attempt)
                Fail();

            if (!_condition() && MaxRetries >= attempt)
                Successful();

            Finish(result);

            return result;
        }

        private bool TryAgain(int attempts)
        {
            return _condition() && MaxRetries > attempts;
        }

        public IRetryStrategy OnSuccess(Action action)
        {
            _onSuccess = action;
            return this;
        }

        public IRetryStrategy OnFailure(Action action)
        {
            _onFailure = action;
            return this;
        }

        public IRetryStrategy OnStart(Action action)
        {
            _onStart = action;
            return this;
        }

        public IRetryStrategy OnFinish(Action<object> action)
        {
            _onFinish = action;
            return this;
        }

        private TimeSpan GetDelay(int key)
        {
            var timespanKey = RetryCountDelayScale.Where(w => w.Key == key).ToList();
            return !timespanKey.Any() ? new TimeSpan(0) : timespanKey.FirstOrDefault().Value;
        }

        private void Finish(object result)
        {
            if(_onFinish == null)
                return;
            _onFinish(result);
        }

        private void Start()
        {
            if(_onStart == null)
                return;
            _onStart();
        }

        private void Fail()
        {
            if (_onFailure == null)
                return;
            _onFailure();
        }

        private void Successful()
        {
            if(_onSuccess == null)
                return;
            _onSuccess();
        }

        public int MaxRetries { get; private set; }
        public int DelayIncrement { get; private set; }
    }
}