﻿using System;
using System.Linq;
using Domain.Base.Workflow;
using Workflow.Messages.Plan.Tasks;
using Workflow.QuoteAcceptance.Domain.Workflow.Router;

namespace Workflow.QuoteAcceptance.Domain.Infrastructure.Factories
{
    public class CreateRouteAcceptedQuoteWorkflowTaskFactory : AbstractWorkflowTaskFactory<RouteAcceptedQuoteTask>
    {
        private readonly ICreateTaskMetdata<RouteAcceptedQuoteTask> _taskMetadataFactory;

        public CreateRouteAcceptedQuoteWorkflowTaskFactory(ICreateTaskMetdata<RouteAcceptedQuoteTask> taskMetadataFactory)
        {
            _taskMetadataFactory = taskMetadataFactory;
        }

        public override IWorkflowMessageTask Create(RouteAcceptedQuoteTask command)
        {
            var metadata = _taskMetadataFactory.Create(command);
            var task = RouteAcceptedQuoteWorkflowMessageTasks.Tasks
                .FirstOrDefault(w => w.Key.Any(productCode => productCode.Equals(command.ProductCode, StringComparison.CurrentCultureIgnoreCase)));

            if (task.Value == null)
                throw new Exception(string.Format("There are no configured tasks for product code {0}",command.ProductCode));

            return task.Value(metadata);
        }
    }
}