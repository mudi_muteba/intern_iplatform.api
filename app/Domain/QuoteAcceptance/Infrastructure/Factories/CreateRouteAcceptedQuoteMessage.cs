﻿using System;
using System.Linq;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Workflow.Router;
using Workflow.Messages;

namespace Domain.QuoteAcceptance.Infrastructure.Factories
{
    public class CreateRouteAcceptedQuoteMessageFactory : AbstractWorkflowRoutingMessageFactory<RouteAcceptedQuoteTask>
    {
        private readonly ICreateTaskMetdata<RouteAcceptedQuoteTask> _taskMetadataFactory;

        public CreateRouteAcceptedQuoteMessageFactory(ICreateTaskMetdata<RouteAcceptedQuoteTask> taskMetadataFactory)
        {
            _taskMetadataFactory = taskMetadataFactory;
        }

        public override IWorkflowRoutingMessage Create(RouteAcceptedQuoteTask command)
        {
            var metadata = _taskMetadataFactory.Create(command);
            var keyValuePair = RouteAcceptedQuoteWorkflowMessageTasks.Tasks
                .FirstOrDefault(w => w.Key.Any(productCode => productCode.Equals(command.ProductCode, StringComparison.CurrentCultureIgnoreCase)));

            if (keyValuePair.Value == null)
                throw new Exception(string.Format("There are no configured tasks for product code {0}", command.ProductCode));

            var workflowRoutingMessage = new WorkflowRoutingMessage();
            workflowRoutingMessage.AddMessage(keyValuePair.Value(metadata));

            return workflowRoutingMessage; 
        }
    }
}