﻿using System;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Monitoring;
using iPlatform.Api.DTOs.Monitoring;
using Workflow.Messages;

namespace Domain.QuoteAcceptance.Infrastructure.Factories
{
    public class ConsumerWorkflowMonitoringFactory : AbstractMonitoringFactory<IWorkflowExecutionMessage>
    {
        public override MonitoringDto Exception(IWorkflowExecutionMessage message)
        {
            return new MonitoringDto(message.CorrelationId, Environment.MachineName, message.GetMetadata<LeadTransferExceptionMetadata>().Status,
                message.GetMetadata<LeadTransferExceptionMetadata>().Priority, message.GetMetadata<LeadTransferExceptionMetadata>().ChannelId,
                message.GetMetadata<LeadTransferExceptionMetadata>().PrettyException ?? "Exception",
                LeadCategory.LeadTransfer.Name(), LeadSubCategory.LeadTransferException.Name(), "Empty",
                message.GetMetadata<LeadTransferExceptionMetadata>().ExceptionMessage);
        }

        public override MonitoringDto Faliure(IWorkflowExecutionMessage message)
        {
            return new MonitoringDto(message.CorrelationId, Environment.MachineName, message.GetMetadata<LeadTransferFailedMetadata>().Status,
                message.GetMetadata<LeadTransferFailedMetadata>().Priority, message.GetMetadata<LeadTransferFailedMetadata>().ChannelId,
                message.GetMetadata<LeadTransferFailedMetadata>().Message ?? "Faliure",
                LeadCategory.LeadTransfer.Name(), LeadSubCategory.LeadTransferFailed.Name(), "Empty",
                message.GetMetadata<LeadTransferFailedMetadata>().Message);
        }

        public override MonitoringDto Finished(IWorkflowExecutionMessage message)
        {
            return new MonitoringDto(message.CorrelationId, Environment.MachineName, Status.Success,
                message.GetMetadata<LeadTransferFinishedMetadata>().Priority, message.GetMetadata<LeadTransferFinishedMetadata>().ChannelId,
                "Finished",
                LeadCategory.LeadTransfer.Name(), LeadSubCategory.LeadTransferFinished.Name(), "Empty",
                message.GetMetadata<LeadTransferFinishedMetadata>().Message);
        }

        public override MonitoringDto Started(IWorkflowExecutionMessage message)
        {
            return new MonitoringDto(message.CorrelationId, Environment.MachineName, Status.Success,
                Priority.High, message.GetMetadata<LeadTransferStartedMetadata>().ChannelId, "Started", LeadCategory.LeadTransfer.Name(), LeadSubCategory.LeadTransferStarted.Name(),
                message.GetMetadata<LeadTransferStartedMetadata>().Parameters ?? "Empty", message.GetMetadata<LeadTransferStartedMetadata>().Message);
        }

        public override MonitoringDto Successful(IWorkflowExecutionMessage message)
        {
            return new MonitoringDto(message.CorrelationId, Environment.MachineName, message.GetMetadata<LeadTransferSuccessfulMetadata>().Status,
                message.GetMetadata<LeadTransferSuccessfulMetadata>().Priority, message.GetMetadata<LeadTransferSuccessfulMetadata>().ChannelId,
                message.GetMetadata<LeadTransferSuccessfulMetadata>().Message ?? "Successful",
                LeadCategory.LeadTransfer.Name(), LeadSubCategory.LeadTransferFailed.Name(), "Empty",
                message.GetMetadata<LeadTransferSuccessfulMetadata>().Message);
        }
    }
}