﻿using System;
using System.Linq;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Workflow.Engine;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Infrastructure.Factories
{
    public class CreateLeadTransferralMessageFactory : AbstractWorkflowExecutionMessageFactory<ITaskMetadata>
    {
        public override IWorkflowExecutionMessage Create(ITaskMetadata command)
        {
            var product = command.GetAbstractMetadata();
            if (product == null || string.IsNullOrEmpty(product.ProductCode))
                throw new Exception("There is no product code to generate the workflow lead transferral execution message");
            return
                LeadTransferrals.Destinations.FirstOrDefault(
                    w => w.Key.Any(productCode => productCode.Equals(product.ProductCode, StringComparison.CurrentCultureIgnoreCase))).Value(command);
        }
    }
}
