﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Infrastructure.Factories;
using Domain.QuoteAcceptance.Monitoring;
using Workflow.Messages;

namespace Domain.QuoteAcceptance.Installers
{
    public class WorkflowQuoteAcceptanceInstallers : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ICreateTaskMetadata>().ImplementedBy<WorkflowTaskMetadataMessageHandlerResolver>());
            container.Register(Component.For<ICreateWorkflowExecutionMessage>().ImplementedBy<WorkflowExecutionMessageHandlerResolver>());
            container.Register(Component.For<ICreateWorkflowRoutingMessage>().ImplementedBy<WorkflowRoutingMessageHandlerResolver>());
            container.Register(Component.For<IBuildMonitoringDto<IWorkflowExecutionMessage>>().ImplementedBy<ConsumerWorkflowMonitoringFactory>());
            container.Register(Component.For<IMonitorLeadTransfer>().ImplementedBy<MonitorLeadTransfer>());

            container.Register(Component.For<IDispatchLeadTransferMessage>().ImplementedBy<DispatchLeadTransferMessage>());

            container.Register(Classes.FromAssemblyContaining<CreateLeadTransferTaskMetadataFactory>()
                .BasedOn(typeof (ICreateTaskMetdata<>))
                .WithServiceAllInterfaces()
                .LifestyleTransient());

            container.Register(Classes.FromAssemblyContaining<CreateRouteAcceptedQuoteMessageFactory>()
               .BasedOn(typeof(ICreateWorkflowRoutingMessage<>))
               .WithServiceAllInterfaces()
               .LifestyleTransient());

            container.Register(Classes.FromAssemblyContaining<ConsumerWorkflowMonitoringFactory>()
             .BasedOn(typeof(IBuildMonitoringDto<>))
             .WithServiceAllInterfaces()
             .LifestyleTransient());
        }
    }
}
