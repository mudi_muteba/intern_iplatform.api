﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.HistoryLoss.Motor.Overrides
{
    public class MotorLossHistoryOverride : IAutoMappingOverride<MotorLossHistory>
    {
        public void Override(AutoMapping<MotorLossHistory> mapping)
        {
            mapping.References(x => x.Party, "PartyId");
        }
    }
}
