﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.Motor.Handlers
{
    public class EditMotorLossHistoryHandler : ExistingEntityDtoHandler<MotorLossHistory, EditMotorLossHistoryDto, int>
    {
        public EditMotorLossHistoryHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(MotorLossHistory entity, EditMotorLossHistoryDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}