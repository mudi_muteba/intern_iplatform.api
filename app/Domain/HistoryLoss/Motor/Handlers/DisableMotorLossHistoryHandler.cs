﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using MasterData.Authorisation;
using Domain.FuneralMembers;
using Domain.HistoryLoss.Motor;

namespace Domain.HistoryLoss.Motor.Handlers
{
    public class DisableMotorLossHistoryHandler : ExistingEntityDtoHandler<MotorLossHistory, DisableMotorLossHistoryDto, int>
    {
        public DisableMotorLossHistoryHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(MotorLossHistory entity, DisableMotorLossHistoryDto dto,
            HandlerResult<int> result)
        {
            entity.Disable();
            result.Processed(entity.Id);
        }
    }
}