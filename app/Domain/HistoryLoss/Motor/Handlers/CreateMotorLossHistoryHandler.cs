﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.Motor.Handlers
{
    public class CreateMotorLossHistoryHandler : CreationDtoHandler<MotorLossHistory, CreateMotorLossHistoryDto, int>
    {
        public CreateMotorLossHistoryHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(MotorLossHistory entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override MotorLossHistory HandleCreation(CreateMotorLossHistoryDto dto, HandlerResult<int> result)
        {
            var motorLossHistory = MotorLossHistory.Create(dto);

            result.Processed(motorLossHistory.Id);

            return motorLossHistory;
        }
    }
}