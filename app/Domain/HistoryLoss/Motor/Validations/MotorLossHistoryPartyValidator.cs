﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using ValidationMessages;
using Domain.Individuals;



namespace Domain.HistoryLoss.Motor.Validation
{
    public class MotorLossHistoryPartyValidator : IValidateDto<CreateMotorLossHistoryDto>, IValidateDto<EditMotorLossHistoryDto>
    {

        public MotorLossHistoryPartyValidator(IProvideContext contextProvider, IRepository repository)
        {
        }

        public void Validate(CreateMotorLossHistoryDto dto, ExecutionResult result)
        {
            ValidateParty(dto.PartyId, result);
        }

        public void Validate(EditMotorLossHistoryDto dto, ExecutionResult result)
        {
            ValidateParty(dto.PartyId, result);
        }

        public void ValidateParty(int PartyId, ExecutionResult result)
        {
            var party = Mapper.Map<int, Party.Party>(PartyId);

            if(party == null)
                result.AddValidationMessage(MotorLossHistoryValidationMessages.PartyIdInvalid.AddParameters(new[] { PartyId.ToString() }));
        }

    }
}