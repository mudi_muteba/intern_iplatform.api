﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.HistoryLoss.Motor
{
    public class MotorLossHistory : Entity, IEntityIncludeParty
    {
        public virtual MotorTypeOfLoss MotorTypeOfLoss { get; set; }
        public virtual string DateOfLoss { get; set; }
        public virtual MotorClaimAmount MotorClaimAmount { get; set; }
        public virtual CurrentlyInsured CurrentlyInsured { get; set; }
        public virtual UninterruptedPolicy UninterruptedPolicy { get; set; }
        public virtual bool InsurerCancel { get; set; }
        public virtual MotorCurrentTypeOfCover MotorCurrentTypeOfCover { get; set; }
        public virtual bool PreviousComprehensiveInsurance { get; set; }
        public virtual MotorUninterruptedPolicyNoClaim MotorUninterruptedPolicyNoClaim { get; set; }
        public virtual Party.Party Party { get; set; }
        public virtual string Why { get; set; }
        
        public static MotorLossHistory Create(CreateMotorLossHistoryDto dto)
        {
            var motorLossHistory = Mapper.Map<MotorLossHistory>(dto);

            return motorLossHistory;
        }

        public virtual void Update(EditMotorLossHistoryDto editMotorLossHistoryDto)
        {
            Mapper.Map(editMotorLossHistoryDto, this);
        }

        public virtual void Disable()
        {
            this.Delete();
        }
    }
}
