﻿using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using MasterData;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.HistoryLoss.Motor.Mappings
{
    public class MotorLossHistoryMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateMotorLossHistoryDto, MotorLossHistory>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party.Party() { Id = s.PartyId }))
                .ForMember(t => t.MotorTypeOfLoss, o => o.MapFrom(s => new MotorTypeOfLoss() { Id = s.MotorTypeOfLossId }))
                .ForMember(t => t.MotorClaimAmount, o => o.MapFrom(s => new MotorClaimAmount() { Id = s.MotorClaimAmountId }))
                .ForMember(t => t.CurrentlyInsured, o => o.MapFrom(s => new CurrentlyInsured() { Id = s.CurrentlyInsuredId }))
                .ForMember(t => t.UninterruptedPolicy, o => o.MapFrom(s => new UninterruptedPolicy() { Id = s.UninterruptedPolicyId }))
                .ForMember(t => t.MotorCurrentTypeOfCover, o => o.MapFrom(s => new MotorCurrentTypeOfCover() { Id = s.MotorCurrentTypeOfCoverId }))
                .ForMember(t => t.MotorUninterruptedPolicyNoClaim, o => o.MapFrom(s => new MotorUninterruptedPolicyNoClaim() { Id = s.MotorUninterruptedPolicyNoClaimId }))
                ;

            Mapper.CreateMap<EditMotorLossHistoryDto, MotorLossHistory>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party.Party() { Id = s.PartyId }))
                .ForMember(t => t.MotorTypeOfLoss, o => o.MapFrom(s => new MotorTypeOfLoss() { Id = s.MotorTypeOfLossId }))
                .ForMember(t => t.MotorClaimAmount, o => o.MapFrom(s => new MotorClaimAmount() { Id = s.MotorClaimAmountId }))
                .ForMember(t => t.CurrentlyInsured, o => o.MapFrom(s => new CurrentlyInsured() { Id = s.CurrentlyInsuredId }))
                .ForMember(t => t.UninterruptedPolicy, o => o.MapFrom(s => new UninterruptedPolicy() { Id = s.UninterruptedPolicyId }))
                .ForMember(t => t.MotorCurrentTypeOfCover, o => o.MapFrom(s => new MotorCurrentTypeOfCover() { Id = s.MotorCurrentTypeOfCoverId }))
                .ForMember(t => t.MotorUninterruptedPolicyNoClaim, o => o.MapFrom(s => new MotorUninterruptedPolicyNoClaim() { Id = s.MotorUninterruptedPolicyNoClaimId }))
                ;

            Mapper.CreateMap<MotorLossHistory, ListMotorLossHistoryDto>();
            Mapper.CreateMap<PagedResults<MotorLossHistory>, PagedResultDto<ListMotorLossHistoryDto>>();
        }
    }
}