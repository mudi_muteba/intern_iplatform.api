﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.Motor.Queries
{
    public class GetAllMotorLossHistoryQuery : BaseQuery<MotorLossHistory>
    {
        public GetAllMotorLossHistoryQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<MotorLossHistory>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<MotorLossHistory> Execute(IQueryable<MotorLossHistory> query)
        {
            return query;
        }
    }
}