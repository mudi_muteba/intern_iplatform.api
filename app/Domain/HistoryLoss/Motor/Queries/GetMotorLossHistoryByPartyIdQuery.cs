﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.Motor.Queries
{
    public class GetMotorLossHistoryByPartyIdQuery : BaseQuery<MotorLossHistory>
    {
        public GetMotorLossHistoryByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<MotorLossHistory>())
        {
        }
        private int partyId;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }
        public GetMotorLossHistoryByPartyIdQuery WithPartyId(int id)
        {
            this.partyId = id;
            return this;
        }
        protected internal override IQueryable<MotorLossHistory> Execute(IQueryable<MotorLossHistory> query)
        {
            return query.Where(x => x.Party.Id == partyId);
        }
    }
}