﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.HistoryLoss.Motor;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.LossHistory.Queries
{
    public class SearchMotorLossHistoryQuery : BaseQuery<MotorLossHistory>, ISearchQuery<SearchMotorLossHistoryDto>
    {
        private SearchMotorLossHistoryDto criteria;

        public SearchMotorLossHistoryQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<MotorLossHistory>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        public void WithCriteria(SearchMotorLossHistoryDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<MotorLossHistory> Execute(IQueryable<MotorLossHistory> query)
        {
            query = query.Where(x => x.Party.Id == criteria.PartyId);

            return query;
        }
    }
}
