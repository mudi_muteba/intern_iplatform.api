﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;
using MasterData;
using AutoMapper;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;

namespace Domain.HistoryLoss.LossHistory
{
    public class LossHistory : Entity, IEntityIncludeParty
    {
        public virtual Party.Party Party { get; set; }
        public virtual CurrentlyInsured CurrentlyInsured { get; set; }
        public virtual UninterruptedPolicy UninterruptedPolicy { get; set; }
        public virtual bool InsurerCancel { get; set; }
        public virtual bool PreviousComprehensiveInsurance { get; set; }
        public virtual MotorUninterruptedPolicyNoClaim MotorUninterruptedPolicyNoClaim { get; set; }
        public virtual bool InterruptedPolicyClaim { get; set; }
        public virtual int ContactId { get; set; }
        public virtual bool MissedPremiumLast6Months { get; set; }

        public static LossHistory Create(CreateLossHistoryDto dto)
        {
            var lossHistory = Mapper.Map<LossHistory>(dto);

            return lossHistory;
        }

        public virtual void Update(EditLossHistoryDto editLossHistoryDto)
        {
            Mapper.Map(editLossHistoryDto, this);
        }

        public virtual void Disable()
        {
            this.Delete();
        }
    }
}
