﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using ValidationMessages;

namespace Domain.HistoryLoss.LossHistory.Validations
{
    public class LossHistoryPartyValidator : IValidateDto<CreateLossHistoryDto>
    {
        public LossHistoryPartyValidator(IProvideContext contextProvider, IRepository repository)
        {
        }

        public void Validate(CreateLossHistoryDto dto, ExecutionResult result)
        {
            ValidateParty(dto.PartyId, result);
        }

        public void ValidateParty(int PartyId, ExecutionResult result)
        {
        }
    }
}
