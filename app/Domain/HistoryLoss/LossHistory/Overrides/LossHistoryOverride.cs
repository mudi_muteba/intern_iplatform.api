﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.HistoryLoss.LossHistory.Overrides
{
    public class LossHistoryOverride : IAutoMappingOverride<LossHistory>
    {
        public void Override(AutoMapping<LossHistory> mapping)
        {
            mapping.References(x => x.Party, "PartyId");
        }
    }
}
