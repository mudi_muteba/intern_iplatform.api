﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using MasterData;
using Domain.Base.Repository;
namespace Domain.HistoryLoss.LossHistory.Mappings
{
    public class LossHistoryMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateLossHistoryDto, LossHistory>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Party, o => o.MapFrom(s => (s.PartyId <= 0) ? new Party.Party() { Id = 0 } : new Party.Party() { Id = s.PartyId }))
                .ForMember(t => t.CurrentlyInsured, o => o.MapFrom(s => new CurrentlyInsured() { Id = s.CurrentlyInsuredId }))
                .ForMember(t => t.UninterruptedPolicy, o => o.MapFrom(s => new UninterruptedPolicy() { Id = s.UninterruptedPolicyId }))
                .ForMember(t => t.InsurerCancel, o => o.MapFrom(s => s.InsurerCancel))
                .ForMember(t => t.MotorUninterruptedPolicyNoClaim, o => o.MapFrom(s => new MotorUninterruptedPolicyNoClaim() { Id = s.UninterruptedPolicyNoClaimId }))
                .ForMember(t => t.InterruptedPolicyClaim, o => o.MapFrom(s => s.InterruptedPolicyClaim))
                .ForMember(t => t.ContactId, o => o.MapFrom(s => s.ContactId))
                .ForMember(t => t.MissedPremiumLast6Months, o => o.MapFrom(s => s.MissedPremiumLast6Months));

            Mapper.CreateMap<EditLossHistoryDto, LossHistory>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party.Party() { Id = s.PartyId }))
                .ForMember(t => t.CurrentlyInsured, o => o.MapFrom(s => new CurrentlyInsured() { Id = s.CurrentlyInsuredId }))
                .ForMember(t => t.UninterruptedPolicy, o => o.MapFrom(s => new UninterruptedPolicy() { Id = s.UninterruptedPolicyId }))
                .ForMember(t => t.InsurerCancel, o => o.MapFrom(s => s.InsurerCancel))
                .ForMember(t => t.MotorUninterruptedPolicyNoClaim, o => o.MapFrom(s => new MotorUninterruptedPolicyNoClaim() { Id = s.UninterruptedPolicyNoClaimId }))
                .ForMember(t => t.InterruptedPolicyClaim, o => o.MapFrom(s => s.InterruptedPolicyClaim))
                .ForMember(t => t.ContactId, o => o.MapFrom(s => s.ContactId))
                .ForMember(t => t.MissedPremiumLast6Months, o => o.MapFrom(s => s.MissedPremiumLast6Months));

            Mapper.CreateMap<LossHistory, LossHistoryDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.CurrentlyInsuredId, o => o.MapFrom(s => s.CurrentlyInsured.Id))
                .ForMember(t => t.UninterruptedPolicyId, o => o.MapFrom(s => s.UninterruptedPolicy.Id))
                .ForMember(t => t.InsurerCancel, o => o.MapFrom(s => s.InsurerCancel))
                .ForMember(t => t.UninterruptedPolicyNoClaimId, o => o.MapFrom(s => s.MotorUninterruptedPolicyNoClaim.Id))
                .ForMember(t => t.InterruptedPolicyClaim, o => o.MapFrom(s => s.InterruptedPolicyClaim))
                .ForMember(t => t.ContactId, o => o.MapFrom(s => s.ContactId))
                .ForMember(t => t.MissedPremiumLast6Months, o => o.MapFrom(s => s.MissedPremiumLast6Months));

            Mapper.CreateMap<List<LossHistory>, ListResultDto<LossHistoryDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
            ;

            Mapper.CreateMap<PagedResults<LossHistory>, PagedResultDto<LossHistoryDto>>();
        }
    }
}
