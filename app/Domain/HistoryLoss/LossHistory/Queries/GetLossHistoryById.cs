﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.JsonDataStores;
using Domain.JsonDataStores.Queries;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.LossHistory.Queries
{
    public class GetLossHistoryById : BaseQuery<LossHistory>
    {
        public GetLossHistoryById(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<LossHistory>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public int id;
        public void WithGuid(int id)
        {
            this.id = id;
        }

        protected internal override IQueryable<LossHistory> Execute(IQueryable<LossHistory> query)
        {
            return query
                .Where(x => x.Id == id);
        }
    }
}
