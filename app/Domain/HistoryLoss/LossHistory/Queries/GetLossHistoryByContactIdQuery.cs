﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.LossHistory.Queries
{
    public class GetLossHistoryByContactIdQuery : BaseQuery<LossHistory>
    {
        public GetLossHistoryByContactIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<LossHistory>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public int contactId;
        public void WithPartyId(int contactId)
        {
            this.contactId = contactId;
        }

        protected internal override IQueryable<LossHistory> Execute(IQueryable<LossHistory> query)
        {
            var result = query
                .Where(c => c.ContactId == contactId);
            return result;
        }
    }
}
