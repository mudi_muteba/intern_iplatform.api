﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.LossHistory.Queries
{
    public class SearchLossHistoryQuery : BaseQuery<LossHistory>, ISearchQuery<LossHistorySearchDto>
    {
        private LossHistorySearchDto criteria;

        public SearchLossHistoryQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<LossHistory>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(LossHistorySearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<LossHistory> Execute(IQueryable<LossHistory> query)
        {
            query = query.Where(x => x.ContactId == criteria.ContactId);

            return query;
        }
    }
}
