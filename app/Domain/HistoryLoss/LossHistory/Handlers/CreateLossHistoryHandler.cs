﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.LossHistory.Handlers
{
    public class CreateLossHistoryHandler : CreationDtoHandler<LossHistory, CreateLossHistoryDto, int>
    {
        public CreateLossHistoryHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(LossHistory entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override LossHistory HandleCreation(CreateLossHistoryDto dto, HandlerResult<int> result)
        {
            var lossHistory = LossHistory.Create(dto);
            return lossHistory;
        }
    }
}
