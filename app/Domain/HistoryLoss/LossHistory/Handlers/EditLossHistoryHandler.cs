﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.LossHistory.Handlers
{
    public class EditLossHistoryHandler : ExistingEntityDtoHandler<LossHistory, EditLossHistoryDto, int>
    {
        public EditLossHistoryHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(LossHistory entity, EditLossHistoryDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
