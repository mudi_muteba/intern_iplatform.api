﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.HistoryLoss.Motor;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.LossHistory.Handlers
{
    public class DisableLossHistoryHandler : ExistingEntityDtoHandler<LossHistory, DisableLossHistoryDto, int>
    {
        public DisableLossHistoryHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void HandleExistingEntityChange(LossHistory entity, DisableLossHistoryDto dto, HandlerResult<int> result)
        {
            entity.Disable();
            result.Processed(entity.Id);
        }
    }
}
