﻿using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using MasterData;

namespace Domain.HistoryLoss.Home.Mappings
{
    public class HomeLossHistoryMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateHomeLossHistoryDto, HomeLossHistory>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party.Party() { Id = s.PartyId }))
                .ForMember(t => t.HomeTypeOfLoss, o => o.MapFrom(s => new HomeTypeOfLoss() { Id = s.HomeTypeOfLossId }))
                .ForMember(t => t.HomeClaimAmount, o => o.MapFrom(s => new HomeClaimAmount() { Id = s.HomeClaimAmountId }))
                .ForMember(t => t.CurrentlyInsured, o => o.MapFrom(s => new CurrentlyInsured() { Id = s.CurrentlyInsuredId }))
                .ForMember(t => t.UninterruptedPolicy, o => o.MapFrom(s => new UninterruptedPolicy() { Id = s.UninterruptedPolicyId }))
                .ForMember(t => t.HomeClaimLocation, o => o.MapFrom(s => new HomeClaimLocation() { Id = s.HomeClaimLocationId }))
                .ForMember(t => t.OtherLossType, o => o.MapFrom(s => s.OtherLossType))
                ;

            Mapper.CreateMap<EditHomeLossHistoryDto, HomeLossHistory>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party.Party() { Id = s.PartyId }))
                .ForMember(t => t.HomeTypeOfLoss, o => o.MapFrom(s => new HomeTypeOfLoss() { Id = s.HomeTypeOfLossId }))
                .ForMember(t => t.HomeClaimAmount, o => o.MapFrom(s => new HomeClaimAmount() { Id = s.HomeClaimAmountId }))
                .ForMember(t => t.CurrentlyInsured, o => o.MapFrom(s => new CurrentlyInsured() { Id = s.CurrentlyInsuredId }))
                .ForMember(t => t.UninterruptedPolicy, o => o.MapFrom(s => new UninterruptedPolicy() { Id = s.UninterruptedPolicyId }))
                .ForMember(t => t.HomeClaimLocation, o => o.MapFrom(s => new HomeClaimLocation() { Id = s.HomeClaimLocationId }))
                .ForMember(t => t.OtherLossType, o => o.MapFrom(s => s.OtherLossType))
                ;

            Mapper.CreateMap<HomeLossHistory, ListHomeLossHistoryDto>();
            Mapper.CreateMap<List<HomeLossHistory>, List<ListHomeLossHistoryDto>>();
            Mapper.CreateMap<PagedResults<HomeLossHistory>, PagedResultDto<ListHomeLossHistoryDto>>();

            Mapper.CreateMap < List<HomeLossHistory>, List<ListHomeLossHistoryDto>>();
            Mapper.CreateMap<List<HomeLossHistory>, ListResultDto<ListHomeLossHistoryDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

        }
    }
}