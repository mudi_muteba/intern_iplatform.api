﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.HistoryLoss.Home.Overrides
{
    public class HomeLossHistoryOverride : IAutoMappingOverride<HomeLossHistory>
    {
        public void Override(AutoMapping<HomeLossHistory> mapping)
        {
            mapping.References(x => x.Party, "PartyId");
        }
    }
}
