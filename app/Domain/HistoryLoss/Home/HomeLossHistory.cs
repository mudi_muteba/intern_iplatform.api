﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.HistoryLoss.Home
{
    public class HomeLossHistory : Entity, IEntityIncludeParty
    {
        public virtual HomeTypeOfLoss HomeTypeOfLoss { get; set; }
        public virtual string DateOfLoss { get; set; }
        public virtual HomeClaimAmount HomeClaimAmount { get; set; }
        public virtual HomeClaimLocation HomeClaimLocation { get; set; }
        public virtual CurrentlyInsured CurrentlyInsured { get; set; }
        public virtual UninterruptedPolicy UninterruptedPolicy { get; set; }
        public virtual bool InsurerCancel { get; set; }
        public virtual string Why { get; set; }
        public virtual Party.Party Party { get; set; }
        public virtual string OtherLossType { get; set; }

        public static HomeLossHistory Create(CreateHomeLossHistoryDto dto)
        {
            var HomeLossHistory = Mapper.Map<HomeLossHistory>(dto);

            return HomeLossHistory;
        }

        public virtual void Update(EditHomeLossHistoryDto editHomeLossHistoryDto)
        {
            Mapper.Map(editHomeLossHistoryDto, this);
        }

        public virtual void Disable()
        {
            this.Delete();
        }
    }
}
