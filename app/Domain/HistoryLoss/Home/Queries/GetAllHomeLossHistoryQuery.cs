﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.Home.Queries
{
    public class GetAllHomeLossHistoryQuery : BaseQuery<HomeLossHistory>
    {
        public GetAllHomeLossHistoryQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<HomeLossHistory>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<HomeLossHistory> Execute(IQueryable<HomeLossHistory> query)
        {
            return query;
        }
    }
}