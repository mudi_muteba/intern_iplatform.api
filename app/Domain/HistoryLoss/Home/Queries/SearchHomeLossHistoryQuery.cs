﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.Home.Queries
{
    public class SearchHomeLossHistoryQuery : BaseQuery<HomeLossHistory>, ISearchQuery<SearchHomeLossHistoryDto>
    {
        private SearchHomeLossHistoryDto criteria;

        public SearchHomeLossHistoryQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<HomeLossHistory>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        public void WithCriteria(SearchHomeLossHistoryDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<HomeLossHistory> Execute(IQueryable<HomeLossHistory> query)
        {
            query = query.Where(x => x.Party.Id == criteria.PartyId);

            return query;
        }
    }
}
