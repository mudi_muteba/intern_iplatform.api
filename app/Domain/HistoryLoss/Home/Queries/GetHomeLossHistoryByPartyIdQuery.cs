﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.Home.Queries
{
    public class GetHomeLossHistoryByPartyIdQuery : BaseQuery<HomeLossHistory>
    {
        public GetHomeLossHistoryByPartyIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<HomeLossHistory>())
        {
        }

        private int partyId;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetHomeLossHistoryByPartyIdQuery WithPartyId(int id)
        {
            this.partyId = id;
            return this;
        }


        protected internal override IQueryable<HomeLossHistory> Execute(IQueryable<HomeLossHistory> query)
        {
            return query.Where(x => x.Party.Id == partyId);
        }
    }
}