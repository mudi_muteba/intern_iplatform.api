﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using MasterData.Authorisation;

namespace Domain.HistoryLoss.Home.Handlers
{
    public class CreateHomeLossHistoryHandler : CreationDtoHandler<HomeLossHistory, CreateHomeLossHistoryDto, int>
    {
        public CreateHomeLossHistoryHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(HomeLossHistory entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override HomeLossHistory HandleCreation(CreateHomeLossHistoryDto dto, HandlerResult<int> result)
        {
            var homeLossHistory = HomeLossHistory.Create(dto);

            result.Processed(homeLossHistory.Id);

            return homeLossHistory;
        }
    }
}