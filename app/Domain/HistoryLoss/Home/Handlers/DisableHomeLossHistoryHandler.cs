﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using MasterData.Authorisation;
using Domain.FuneralMembers;
using Domain.HistoryLoss.Home;

namespace Domain.HistoryLoss.Home.Handlers
{
    public class DisableHomeLossHistoryHandler : ExistingEntityDtoHandler<HomeLossHistory, DisableHomeLossHistoryDto, int>
    {
        public DisableHomeLossHistoryHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(HomeLossHistory entity, DisableHomeLossHistoryDto dto,
            HandlerResult<int> result)
        {
            entity.Disable();
            result.Processed(entity.Id);
        }
    }
}