﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using ValidationMessages;
using Domain.Individuals;



namespace Domain.HistoryLoss.Home.Validation
{
    public class HomeLossHistoryPartyValidator : IValidateDto<CreateHomeLossHistoryDto>, IValidateDto<EditHomeLossHistoryDto>
    {

        public HomeLossHistoryPartyValidator(IProvideContext contextProvider, IRepository repository)
        {
        }

        public void Validate(CreateHomeLossHistoryDto dto, ExecutionResult result)
        {
            ValidateParty(dto.PartyId, result);
        }

        public void Validate(EditHomeLossHistoryDto dto, ExecutionResult result)
        {
            ValidateParty(dto.PartyId, result);
        }

        public void ValidateParty(int PartyId, ExecutionResult result)
        {
            var party = Mapper.Map<int, Party.Party>(PartyId);

            if(party == null)
                result.AddValidationMessage(HomeLossHistoryValidationMessages.PartyIdInvalid.AddParameters(new[] { PartyId.ToString() }));
        }

    }
}