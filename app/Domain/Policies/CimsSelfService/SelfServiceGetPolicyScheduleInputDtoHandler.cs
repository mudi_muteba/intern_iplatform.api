﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Policy.CimsSelfService;
using iPlatform.Integration.Services.CimsSelfService;
using iPlatform.Integration.Services.CimsSelfServiceWebService;
using MasterData.Authorisation;
using Remotion.Logging;

namespace Domain.Policies.CimsSelfService
{
    public class SelfServiceGetPolicyScheduleInputDtoHandler : BaseDtoHandler<SelfServiceGetPolicyScheduleInputDto, SelfServiceGetPolicyScheduleResponseDto>
    {
        public SelfServiceGetPolicyScheduleInputDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            this.repository = repository;
        }

        private readonly ILog log;
        private readonly IRepository repository;
        private PolicyServiceSetting _policyServiceSetting;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        public SelfServiceGetPolicyScheduleInputDtoHandler WithPolicyServiceSetting(PolicyServiceSetting policyServiceSetting)
        {
            _policyServiceSetting = policyServiceSetting;
            return this;
        }

        protected override void InternalHandle(SelfServiceGetPolicyScheduleInputDto dto, HandlerResult<SelfServiceGetPolicyScheduleResponseDto> result)
        {
            if (_policyServiceSetting == null)
                _policyServiceSetting = repository.GetAll<PolicyServiceSetting>().FirstOrDefault(x => x.Name == "Default");

            var response = new SelfServiceGetPolicyScheduleResponseDto();
            try
            {
                var cimsSelfService = new CimsSelfServices(_policyServiceSetting.PolicyServiceUrl);
                response = cimsSelfService.GetPolicySchedule(Mapper.Map<PolicyServiceSetting, SecuredWebServiceHeader>(_policyServiceSetting), dto);
            }
            catch (Exception e)
            {
                var error = string.Format("Error getting CIMS self service policy schedule pdf: {0} ", e.Message);
                log.Error(error);
                response.LastErrorDescription = error;
                response.Success = false;
            }
            result.Processed(response);
        }
    }
}
