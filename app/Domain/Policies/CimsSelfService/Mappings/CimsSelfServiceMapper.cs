﻿using AutoMapper;
using iPlatform.Integration.Services.CimsSelfServiceWebService;


namespace Domain.Policies.CimsSelfService.Mappings
{
    public class CimsSelfServiceMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PolicyServiceSetting, SecuredWebServiceHeader>()
                .ForMember(d => d.Username, opt => opt.MapFrom(x => x.PolicyServiceUsername))
                .ForMember(d => d.Password, opt => opt.MapFrom(x => x.PolicyServicePassword))
                .ForMember(d => d.CompanyID, opt => opt.MapFrom(x => x.PolicyServiceCompanyId))
                ;
        }
    }
}