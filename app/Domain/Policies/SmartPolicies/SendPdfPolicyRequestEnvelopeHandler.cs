﻿using System.Collections.Generic;
using System.Xml.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Policies.SmartPolicies.Helpers;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Api.DTOs.Policy.Smart.SoapDtos;
using MasterData.Authorisation;
using Remotion.Logging;

namespace Domain.Policies.SmartPolicies
{
    public class SendPdfPolicyRequestEnvelopeHandler : BaseDtoHandler<SendPdfPolicyRequestEnvelope, XElement>
    {
        public SendPdfPolicyRequestEnvelopeHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            this.repository = repository;
            this.executionPlan = executionPlan;

        }
        private readonly ILog log;
        private IExecutionPlan executionPlan;
        private IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(SendPdfPolicyRequestEnvelope dto, HandlerResult<XElement> result)
        {
            var data = dto.Body.SendPdfPolicy.Data;
            var smartPolicyDto = executionPlan.Execute<SendSmartPdfPoliciesDto, BasicSmartPolicyResponseHeaderDto>(Mapper.Map<SendPdfRequestData, SendSmartPdfPoliciesDto>(data));
           
            var mapp = new SendPdfPoliciesResponseEnvelope();
            mapp.Body.SendPdfPolicy.Result = Mapper.Map<BasicSmartPolicyResponseHeaderDto, SendPdfPoliciesResponseResult>(smartPolicyDto.Response);
            var soapResponse = XmlSoapHelper.BuildSoapResponse(mapp);

            result.Processed(soapResponse);
        }
       
    }
}
