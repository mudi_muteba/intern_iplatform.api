﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Integration.Services.SmartBinding;
using iPlatform.Integration.Services.SmartServiceReference;
using MasterData.Authorisation;
using Common.Logging;

namespace Domain.Policies.SmartPolicies
{
    public class RegisterSmartPolicyDtoHandler : BaseDtoHandler<RegisterSmartPolicyDto, SmartGetPdfResponseDto>
    {
        public RegisterSmartPolicyDtoHandler(IProvideContext contextProvider, IRepository repository, IWindsorContainer container)
            : base(contextProvider)
        {
            this.repository = repository;
            this.container = container;

        }
        private IWindsorContainer container;
        private IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void InternalHandle(RegisterSmartPolicyDto dto, HandlerResult<SmartGetPdfResponseDto> result)
        {
            var response = new Response();
            try
            {
                var policyServiceIntegration = new SmartPolicyService();
                response = policyServiceIntegration.RegisterPolicy(Mapper.Map<RegisterSmartPolicyDto, RegisterPolicyRequest>(dto));
            }
            catch (Exception e)
            {
                var error = string.Format("Error registering SMART policy: {0} ", e.Message);
                Log.Error(error);
                response.ErrorMessages = error;
                response.SuccessOrFail = false;
            }
            result.Processed(Mapper.Map<Response, SmartGetPdfResponseDto>(response));
        }
    }
}
