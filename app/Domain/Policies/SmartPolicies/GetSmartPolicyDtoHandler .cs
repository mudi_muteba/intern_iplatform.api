﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Policy.CimsSelfService;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Integration.Services.CimsSelfService;
using iPlatform.Integration.Services.SmartBinding;
using iPlatform.Integration.Services.SmartServiceReference;
using MasterData.Authorisation;

namespace Domain.Policies.SmartPolicies
{
    public class GetSmartPolicyDtoHandler : BaseDtoHandler<GetSmartPolicyDto, SmartGetPolicyResponseDto>
    {
        public GetSmartPolicyDtoHandler(IProvideContext contextProvider, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            this.m_ExecutionPlan = executionPlan;
        }

        private readonly IExecutionPlan m_ExecutionPlan;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void InternalHandle(GetSmartPolicyDto dto, HandlerResult<SmartGetPolicyResponseDto> result)
        {
            GetPolicyResponse response = new GetPolicyResponse();

            try
            {
                bool hasCimsPolicy;
                var cimsPolicyInfo = GetCimsPolicyInfo(dto);

                if (bool.TryParse(cimsPolicyInfo.SuccessOrFail, out hasCimsPolicy) && hasCimsPolicy)
                {
                    result.Processed(cimsPolicyInfo);
                    return;
                }

                SmartPolicyService policyServiceIntegration = new SmartPolicyService();
                response = policyServiceIntegration.GetPolicy(Mapper.Map<GetSmartPolicyDto, GetPolicyRequest>(dto));
            }
            catch (Exception e)
            {
                var error = string.Format("Error getting SMART policy info: {0} ", e.Message);
                Log.Error(error);
                response.ErrorMessages = error;
                response.SuccessOrFail = false;

            }
            result.Processed(Mapper.Map<GetPolicyResponse, SmartGetPolicyResponseDto>(response));

        }

        private SmartGetPolicyResponseDto GetCimsPolicyInfo(GetSmartPolicyDto dto)
        {
            var result = new SmartGetPolicyResponseDto();
            var cimsSelfServiceGetPolicyResult = m_ExecutionPlan.Execute<SelfServiceGetPolicyInfoInputDto, SelfServiceGetPolicyInfoResponseDto>(Mapper.Map<GetSmartPolicyDto, SelfServiceGetPolicyInfoInputDto>(dto));

            if (cimsSelfServiceGetPolicyResult.Completed && cimsSelfServiceGetPolicyResult.Response != null)
            {
                result = (Mapper.Map<SelfServiceGetPolicyInfoResponseDto, SmartGetPolicyResponseDto>(cimsSelfServiceGetPolicyResult.Response));
            }
            else
            {
                result.SuccessOrFail = false.ToString();
            }

            return result;

        }
    }
}
