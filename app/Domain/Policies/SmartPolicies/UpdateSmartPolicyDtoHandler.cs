﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Integration.Services.SmartBinding;
using iPlatform.Integration.Services.SmartServiceReference;
using MasterData.Authorisation;
using Remotion.Logging;

namespace Domain.Policies.SmartPolicies
{
    public class UpdateSmartPolicyDtoHandler : BaseDtoHandler<UpdateSmartPolicyDto, SmartUpdatePolicyResponseDto>
    {
        public UpdateSmartPolicyDtoHandler(IProvideContext contextProvider, IRepository repository, IWindsorContainer container)
            : base(contextProvider)
        {
            this.repository = repository;
            this.container = container;

        }
        private readonly ILog log;
        private IWindsorContainer container;
        private IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void InternalHandle(UpdateSmartPolicyDto dto, HandlerResult<SmartUpdatePolicyResponseDto> result)
        {
            var response = new Response();
            try
            {
                var policyServiceIntegration = new SmartPolicyService();
                response = policyServiceIntegration.UpdatePolicy(Mapper.Map<UpdateSmartPolicyDto, UpdatePolicyRequest>(dto));
            }
            catch (Exception e)
            {
                var error = string.Format("Error updating SMART policy: {0} ", e.Message);
                log.Error(error);
                response.ErrorMessages = error;
                response.SuccessOrFail = false;
            }
            result.Processed(Mapper.Map<Response, SmartUpdatePolicyResponseDto>(response));
        }
    }
}
