﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Policy.CimsSelfService;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Integration.Services.SmartBinding;
using iPlatform.Integration.Services.SmartServiceReference;
using MasterData.Authorisation;
using Remotion.Logging;

namespace Domain.Policies.SmartPolicies
{
    public class GetSmartPdfUrlDtoHandler : BaseDtoHandler<GetSmartPdfUrlDto, SmartGetPdfResponseDto>
    {
        public GetSmartPdfUrlDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            this.repository = repository;
            this.executionPlan = executionPlan;

        }

        private const string BaseUrl = @"iPlatform/serviceLocator/iPlatform/baseUrl";
        private const string ReportStorage = @"Reports/Storage";
        private const string FileExtention = @"pdf";

        private readonly ILog log;
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void InternalHandle(GetSmartPdfUrlDto dto, HandlerResult<SmartGetPdfResponseDto> result)
        {
            bool hasCimsPolicy;
            var cimsPolicySchedule = GetCimsPolicySchedule(dto);

            if (bool.TryParse(cimsPolicySchedule.SuccessOrFail, out hasCimsPolicy) && hasCimsPolicy)
            {
                result.Processed(cimsPolicySchedule);
                return;
            }
            var response = new Response();
            try
            {
                var policyServiceIntegration = new SmartPolicyService();
                response = policyServiceIntegration.GetPdfUrl(Mapper.Map<GetSmartPdfUrlDto, GetPDFUrlRequest>(dto));
            }
            catch (Exception e)
            {
                var error = string.Format("Error getting SMART pdf url: {0} ", e.Message);
                log.Error(error);
                response.ErrorMessages = error;
                response.SuccessOrFail = false;

            }
            result.Processed(Mapper.Map<Response, SmartGetPdfResponseDto>(response));
        }

        private SmartGetPdfResponseDto GetCimsPolicySchedule(GetSmartPdfUrlDto dto)
        {
            var result = new SmartGetPdfResponseDto();
            var cimsSelfServiceGetPolicyResult = executionPlan.Execute<SelfServiceGetPolicyScheduleInputDto, SelfServiceGetPolicyScheduleResponseDto>(Mapper.Map<GetSmartPdfUrlDto, SelfServiceGetPolicyScheduleInputDto>(dto));

            if (cimsSelfServiceGetPolicyResult.Completed && cimsSelfServiceGetPolicyResult.Response != null)
            {
                result = (Mapper.Map<SelfServiceGetPolicyScheduleResponseDto, SmartGetPdfResponseDto>(cimsSelfServiceGetPolicyResult.Response));

                if (cimsSelfServiceGetPolicyResult.Response.Success)
                {
                    var schedule = cimsSelfServiceGetPolicyResult.Response.Data.FirstOrDefault();
                    if (schedule == null)
                        throw new Exception(string.Format("Error Getting Cims Self Service file for PolicyNumber {0} - No File Data", dto.PolicyNumber));

                    var baseUrl = ConfigurationManager.AppSettings[BaseUrl];
                    var reportStorage = ConfigurationManager.AppSettings[ReportStorage];

                    //Store the file
                    var name = string.Format("{0}_{1}.{2}", dto.PolicyNumber, DateTime.Now.ToString("yyyyMMddHHmmss"), FileExtention);
                    var path = Path.Combine(dto.PathMap, reportStorage, name);
                    File.WriteAllBytes(path, schedule.FileData);

                    //Create a Uri to the file
                    result.PdfUrl = Path.Combine(baseUrl, reportStorage, name).Replace(@"\", "/");
                    result.PolicyNumber = dto.PolicyNumber;
                }

            }
            else
            {
                result.SuccessOrFail = false.ToString();
            }

            return result;

        }
    }
}
