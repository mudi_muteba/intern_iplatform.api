﻿using System.Collections.Generic;
using System.Xml.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Policies.SmartPolicies.Helpers;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Api.DTOs.Policy.Smart.SoapDtos;
using MasterData.Authorisation;
using Remotion.Logging;

namespace Domain.Policies.SmartPolicies
{
    public class UpdatePolicyRequestEnvelopeHandler : BaseDtoHandler<UpdatePolicyRequestEnvelope, XElement>
    {
        public UpdatePolicyRequestEnvelopeHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            this.repository = repository;
            this.executionPlan = executionPlan;

        }
        private readonly ILog log;
        private IExecutionPlan executionPlan;
        private IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(UpdatePolicyRequestEnvelope dto, HandlerResult<XElement> result)
        {
            var data = dto.Body.UpdatePolicy.Data;
            var smartPolicyDto = executionPlan.Execute<UpdateSmartPolicyDto, SmartUpdatePolicyResponseDto>(Mapper.Map<UpdatePolicyData, UpdateSmartPolicyDto>(data));

            var mapp = new UpdatePolicyResponseEnvelope();
            mapp.Body.UpdatePolicyResponse.Result = Mapper.Map<SmartUpdatePolicyResponseDto, UpdatePolicyResponseResult>(smartPolicyDto.Response);
            var soapResponse = XmlSoapHelper.BuildSoapResponse(mapp);

            result.Processed(soapResponse);
        }
       
    }
}
