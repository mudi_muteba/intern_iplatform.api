﻿using System.Collections.Generic;
using System.Xml.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Policies.SmartPolicies.Helpers;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Api.DTOs.Policy.Smart.SoapDtos;
using MasterData.Authorisation;
using Remotion.Logging;

namespace Domain.Policies.SmartPolicies
{
    public class GetPolicyRequestEnvelopeHandler : BaseDtoHandler<GetPolicyRequestEnvelope, XElement>
    {
        public GetPolicyRequestEnvelopeHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            this.repository = repository;
            this.executionPlan = executionPlan;

        }
        private readonly ILog log;
        private IExecutionPlan executionPlan;
        private IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(GetPolicyRequestEnvelope dto, HandlerResult<XElement> result)
        {
            var data = dto.Body.GetPolicy.Data;
            var smartPolicyDto = executionPlan.Execute<GetSmartPolicyDto, SmartGetPolicyResponseDto>(Mapper.Map<GetPolicyRequestData, GetSmartPolicyDto>(data));
            var mapp = new GetPolicyResponseEnvelope();

            mapp.Body.GetPolicyResponse.Result = Mapper.Map<SmartGetPolicyResponseDto, GetPolicyResponseResult>(smartPolicyDto.Response);
            var soapResponse = XmlSoapHelper.BuildSoapResponse(mapp);
            result.Processed(soapResponse);
        }
    }
}
