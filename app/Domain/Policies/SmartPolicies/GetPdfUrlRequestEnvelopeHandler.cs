﻿using System.Collections.Generic;
using System.Xml.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Policies.SmartPolicies.Helpers;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Api.DTOs.Policy.Smart.SoapDtos;
using MasterData.Authorisation;
using Remotion.Logging;

namespace Domain.Policies.SmartPolicies
{
    public class GetPdfUrlRequestEnvelopeHandler : BaseDtoHandler<GetPdfUrlRequestEnvelope, XElement>
    {
        public GetPdfUrlRequestEnvelopeHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            this.repository = repository;
            this.executionPlan = executionPlan;

        }
        private readonly ILog log;
        private IExecutionPlan executionPlan;
        private IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(GetPdfUrlRequestEnvelope dto, HandlerResult<XElement> result)
        {
            var data = dto.Body.GetPDFUrl.Data;
            var getSmartPdfUrlDto = Mapper.Map<GetPolicyRequestData, GetSmartPdfUrlDto>(data);
            getSmartPdfUrlDto.PathMap = dto.PathMap;
            var smartPolicyDto = executionPlan.Execute<GetSmartPdfUrlDto, SmartGetPdfResponseDto>(getSmartPdfUrlDto);

            var mapp = new GetPdfUrlResponseEnvelope();
            mapp.Body.GetPDFUrlResponse.Result = Mapper.Map<SmartGetPdfResponseDto, GetPdfUrlResponseResult>(smartPolicyDto.Response);
            var soapResponse = XmlSoapHelper.BuildSoapResponse(mapp);

            result.Processed(soapResponse);
        }
       
    }
}
