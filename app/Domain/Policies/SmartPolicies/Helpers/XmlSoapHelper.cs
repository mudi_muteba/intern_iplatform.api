﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Domain.Policies.SmartPolicies.Helpers
{
    public static class XmlSoapHelper
    {

        public static XElement BuildSoapResponse(object mapp)
        {
            XNamespace SOAPENV = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace ns1 = "https://api.smartinsurance.com/api/";
            XElement root = new XElement(SOAPENV + "Envelope",
                new XAttribute(XNamespace.Xmlns + "SOAP-ENV", SOAPENV.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "ns1", ns1.NamespaceName));
            CreateXmlResponse(mapp, root);

            return root;
        }

        private static void CreateXmlResponse(object obj, XElement parent)
        {
            if (obj == null)
                return;

            var children = obj.GetType().GetProperties();

            foreach (var childProperty in children)
            {
                var attr = childProperty.CustomAttributes.FirstOrDefault(at => at.AttributeType.Name == "XmlAtt");
                string attrValue, attrName = "";
                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                if (attr != null && attr.ConstructorArguments.Count > 0)
                {
                    foreach (var arg in attr.NamedArguments)
                    {
                        dictionary.Add(arg.MemberName, arg.TypedValue.Value.ToString());
                    }
                }

                var name = childProperty.Name;
                var value = childProperty.GetValue(obj);
                bool hasNamespace = dictionary.TryGetValue("Namespace", out attrValue);
                bool hasName = dictionary.TryGetValue("ElementName", out attrName);
                XNamespace nmsp = attrValue;

                if (childProperty.PropertyType == typeof(string) || childProperty.PropertyType == typeof(int) || childProperty.PropertyType == typeof(bool))
                {
                    XElement el = new XElement((nmsp ?? "") + (hasName ? attrName : name), value);
                    parent.Add(el);
                }
                else
                {
                    XElement el = new XElement((nmsp ?? "") + (hasName ? attrName : name));
                    CreateXmlResponse(value, el);
                    parent.Add(el);
                }
            }
        }
    }
}