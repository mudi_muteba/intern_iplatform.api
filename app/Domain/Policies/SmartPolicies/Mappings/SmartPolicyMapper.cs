﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using iGuide.DTOs.Responses;
using iPlatform.Api.DTOs.Policy.CimsSelfService;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Api.DTOs.Policy.Smart.SoapDtos;
using iPlatform.Integration.Services.SmartServiceReference;
using GetPolicyResponse = iPlatform.Integration.Services.SmartServiceReference.GetPolicyResponse;

namespace Domain.Policies.SmartPolicies.Mappings
{
    public class SmartPolicyMapper : Profile
    {
       

        protected override void Configure()
        {
           
            //Get Policy
            Mapper.CreateMap<GetSmartPolicyDto, GetPolicyRequest>();
            Mapper.CreateMap<GetPolicyResponse, SmartGetPolicyResponseDto>();
            Mapper.CreateMap<policyObject, SmartDataDto>();
            Mapper.CreateMap<SmartGetPolicyResponseDto, GetPolicyResponseResult>();
            Mapper.CreateMap<GetSmartPolicyDto, SelfServiceGetPolicyInfoInputDto>()
                .ForMember(d => d.PolicyNumber, opt => opt.MapFrom(dto => dto.PolicyNumber));
            Mapper.CreateMap<SelfServiceGetPolicyData, SmartDataDto>()
                .ForMember(d => d.PolicyStartDate, opt => opt.MapFrom(x => x.OriginalInceptionDate))
                .ForMember(d => d.CarManufacturer, opt => opt.MapFrom(x => x.PolicyItems.FirstOrDefault().ItemDescription1))
                .ForMember(d => d.CarModel, opt => opt.MapFrom(x => x.PolicyItems.FirstOrDefault().ItemDescription2))
                .ForMember(d => d.Price, opt => opt.MapFrom(x => x.PolicyItems.FirstOrDefault().SumInsured))
                ;
            Mapper.CreateMap<SelfServiceGetPolicyInfoResponseDto, SmartGetPolicyResponseDto>()
                 .ForMember(d => d.Policy, opt => opt.MapFrom(x => Mapper.Map<SelfServiceGetPolicyData, SmartDataDto>(x.Data.FirstOrDefault())))
                 .ForMember(d => d.SuccessOrFail, opt => opt.MapFrom(x => x.Success))
                 .ForMember(d => d.ErrorMessages, opt => opt.MapFrom(x => x.LastErrorDescription));




            //Get PDf Url
            Mapper.CreateMap<GetSmartPdfUrlDto, SmartGetPdfResponseDto>();
            Mapper.CreateMap<GetPolicyRequestData, GetSmartPdfUrlDto>();
            Mapper.CreateMap<GetSmartPdfUrlDto, GetPDFUrlRequest>();
            Mapper.CreateMap<Response, SmartGetPdfResponseDto>();
            Mapper.CreateMap<GetSmartPdfUrlDto, SelfServiceGetPolicyScheduleInputDto>();
            Mapper.CreateMap<SelfServiceGetPolicyScheduleResponseDto, SmartGetPdfResponseDto>()
                .ForMember(d => d.SuccessOrFail, opt => opt.MapFrom(x => x.Success))
                .ForMember(d => d.ErrorMessages, opt => opt.MapFrom(x => x.LastErrorDescription));


            //Register Policy
            Mapper.CreateMap<RegisterSmartPolicyDto, RegisterPolicyRequest>()
                .ForMember(d => d.DurationInMonths, opt => opt.MapFrom(dto => MapDurationMonths(dto.DurationInMonths)))
                .ForMember(d => d.PaymentDurationInMonths, opt => opt.MapFrom(x => MapPaymentDurationMonths(x.PaymentDurationInMonths)))
                ;
            Mapper.CreateMap<RegisterPolicyData, RegisterSmartPolicyDto>();


            //Update Policy
            Mapper.CreateMap<UpdateSmartPolicyDto, UpdatePolicyRequest>()
                .ForMember(d => d.DurationInMonths, opt => opt.MapFrom(dto => MapDurationMonths(dto.DurationInMonths)))
                .ForMember(d => d.PaymentDurationInMonths, opt => opt.MapFrom(x => MapPaymentDurationMonths(x.PaymentDurationInMonths)))
                ;
            Mapper.CreateMap<Response, SmartUpdatePolicyResponseDto>();
            Mapper.CreateMap<UpdatePolicyData, UpdateSmartPolicyDto>();
            Mapper.CreateMap<SmartUpdatePolicyResponseDto, UpdatePolicyResponseResult>();

            //Send Pdf policy
            Mapper.CreateMap<SendPdfRequestData, SendSmartPdfPoliciesDto>()
                .ForMember(d => d.PolicyNumber, opt => opt.MapFrom(dto => dto.Policies.PolicyNumber));
            Mapper.CreateMap<BasicSmartPolicyResponseHeaderDto, SendPdfPoliciesResponseResult>();
            Mapper.CreateMap<SendSmartPdfPoliciesDto, SendPdfPolicyRequest>()
                .ForMember(d => d.Policies, opt => opt.MapFrom(dto => dto.PolicyNumber));
            Mapper.CreateMap<Response, BasicSmartPolicyResponseHeaderDto>();



            //To XML SOAP Response
            Mapper.CreateMap<SmartDataDto, GetPolicyResponsePolicy>();
            Mapper.CreateMap<SmartGetPdfResponseDto, GetPdfUrlResponseResult>();

            //From XML SOAP Request
            Mapper.CreateMap<GetPolicyRequestData, GetSmartPolicyDto>();

        }


        private dictDurationInMonths_ MapDurationMonths(string input)
        {
            dictDurationInMonths_ output;
            var durationInMonthsDict = new Dictionary<string, dictDurationInMonths_>()
            {
                {"12", dictDurationInMonths_._12},
                {"24", dictDurationInMonths_._24},
                {"36", dictDurationInMonths_._36}
            };

            durationInMonthsDict.TryGetValue(input, out output);
            return output;
        }


        private dictPaymentDurationInMonths_ MapPaymentDurationMonths(string input)
        {
            dictPaymentDurationInMonths_ output;
            var durationInMonthsDict = new Dictionary<string, dictPaymentDurationInMonths_>()
            {
                {"1", dictPaymentDurationInMonths_._1},
                {"12", dictPaymentDurationInMonths_._12},
                {"24", dictPaymentDurationInMonths_._24},
                {"36", dictPaymentDurationInMonths_._36}
            };

            durationInMonthsDict.TryGetValue(input, out output);
            return output;
        }

    }
}