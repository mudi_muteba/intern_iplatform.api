﻿using System.Collections.Generic;
using System.Xml.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Policies.SmartPolicies.Helpers;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Api.DTOs.Policy.Smart.SoapDtos;
using MasterData.Authorisation;
using Remotion.Logging;

namespace Domain.Policies.SmartPolicies
{
    public class RegisterPolicyRequestEnvelopeHandler : BaseDtoHandler<RegisterPolicyRequestEnvelope, XElement>
    {
        public RegisterPolicyRequestEnvelopeHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            this.repository = repository;
            this.executionPlan = executionPlan;

        }
        private readonly ILog log;
        private IExecutionPlan executionPlan;
        private IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(RegisterPolicyRequestEnvelope dto, HandlerResult<XElement> result)
        {
            var data = dto.Body.RegisterPolicy.Data;
            var smartPolicyDto = executionPlan.Execute<RegisterSmartPolicyDto, SmartGetPdfResponseDto>(Mapper.Map<RegisterPolicyData, RegisterSmartPolicyDto>(data));

            var mapp = new RegisterPolicyResponseEnvelope();
            mapp.Body.RegisterPolicyResponse.Result = Mapper.Map<SmartGetPdfResponseDto, GetPdfUrlResponseResult>(smartPolicyDto.Response);
            var soapResponse = XmlSoapHelper.BuildSoapResponse(mapp);

            result.Processed(soapResponse);
        }
       
    }
}
