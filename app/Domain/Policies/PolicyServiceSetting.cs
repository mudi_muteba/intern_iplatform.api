﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Policies
{
    public class PolicyServiceSetting : Entity
    {
        public PolicyServiceSetting()
        {

        }
        
        public virtual string Name { get; set; }
        public virtual string PolicyServiceUrl { get; set; }
        public virtual string PolicyServiceUsername { get; set; }
        public virtual string PolicyServicePassword { get; set; }
        public virtual string PolicyServiceCompanyId { get; set; }
        public virtual bool PolicyServiceNeedAuthentication { get; set; }
    }
}
