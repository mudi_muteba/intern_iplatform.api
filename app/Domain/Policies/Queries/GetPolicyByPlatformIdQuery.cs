using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Policies.Queries
{
    public class GetPolicyByPlatformIdQuery : BaseQuery<PolicyHeader>
    {
        public GetPolicyByPlatformIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PolicyHeader>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        private Guid _platformId;

        public GetPolicyByPlatformIdQuery WithPlatformId(Guid platformId)
        {
            _platformId = platformId;
            return this;
        }

        protected internal override IQueryable<PolicyHeader> Execute(IQueryable<PolicyHeader> query)
        {
            return query.Where(c => c.PlatformId == _platformId);
        }
    }
}