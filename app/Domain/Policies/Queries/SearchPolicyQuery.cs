﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Policy;

namespace Domain.Policies.Queries
{
    public class SearchPolicyQuery : BaseQuery<PolicyHeader>, ISearchQuery<PolicySearchDto>
    {

        public SearchPolicyQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PolicyHeader>())
        {
        }

        private PolicySearchDto criteria;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(PolicySearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<PolicyHeader> Execute(IQueryable<PolicyHeader> query)
        {
            if (criteria.Id != null)
                query = query.Where(x => x.Id == criteria.Id);

            if (criteria.PartyId != null)
                query = query.Where(x => x.Party.Id == criteria.PartyId);

            if (criteria.QuoteId > 0)
                query = query.Where(x => x.QuoteId == criteria.QuoteId);

            return query;
        }
    }
}