﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Policies.Queries
{
    public class GetPolicyByIdQuery :BaseQuery<PolicyHeader>
    {
        public GetPolicyByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PolicyHeader>())
        {
        }

        private int id;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        public GetPolicyByIdQuery WithId(int id)
        {
            this.id = id;
            return this;
        }

        protected internal override IQueryable<PolicyHeader> Execute(IQueryable<PolicyHeader> query)
        {
            return query.Where(c => c.Id == id);
        }
    }
}