﻿using Domain.Party.Assets;
using Domain.Party.Quotes;
using MasterData;
using System;
using System.Collections.Generic;

namespace Domain.Policies
{
    public class PolicyItemFactory
    {
        private static Dictionary<Cover, Func<QuoteItem, Asset, PolicyItem>> _dtCoverMapping;
        private PolicyHeader _policyHeader;

        public PolicyItemFactory(PolicyHeader policyHeader)
        {
            _dtCoverMapping = new Dictionary<Cover, Func<QuoteItem, Asset, PolicyItem>>
            {
                { Covers.Motor, NewPolicyPersonalVehicle },
                { Covers.Contents, NewPolicyContent },
                { Covers.AllRisk, NewPolicyAllRisk },
                { Covers.FuneralCostsAddDeathBenefits, NewPolicyFuneral },
                { Covers.Building, NewPolicyBuilding },
                { Covers.HouseOwners, NewPolicyBuilding },
                { Covers.IdentityTheft, NewPolicySpecial },
                { Covers.DisasterCash, NewPolicySpecial },
                { Covers.PersonalLegalLiability, NewPolicySpecial },
                { Covers.CaravanOrTrailer, NewPolicyPersonalVehicle },
                { Covers.MotorWarranty, NewPolicyPersonalVehicle },
                { Covers.PersonalAccident, NewPersonalAccident},
                { Covers.TouchUp, NewPolicyPersonalVehicle},
                { Covers.LegalPlan, NewPolicySpecial},
            };
            _policyHeader = policyHeader;
        }

        private PolicyItem NewPersonalAccident(QuoteItem quoteItem, Asset asset)
        {
            return new PolicyPersonalAccident(_policyHeader, quoteItem, asset);
        }

        private PolicyItem NewPolicyPersonalVehicle(QuoteItem quoteItem, Asset asset)
        {
            return new PolicyPersonalVehicle(_policyHeader, quoteItem, asset);
        }

        private PolicyItem NewPolicyContent(QuoteItem quoteItem, Asset asset)
        {
            return new PolicyContent(_policyHeader, quoteItem, asset);
        }

        private PolicyItem NewPolicySpecial(QuoteItem quoteItem, Asset asset)
        {
            return new PolicySpecial(_policyHeader, quoteItem, asset);
        }

        private PolicyItem NewPolicyBuilding(QuoteItem quoteItem, Asset asset)
        {
            return new PolicyBuilding(_policyHeader, quoteItem, asset);
        }

        private PolicyItem NewPolicyAllRisk(QuoteItem quoteItem, Asset asset)
        {
            return new PolicyAllRisk(_policyHeader, quoteItem, asset);
        }

        private PolicyItem NewPolicyFuneral(QuoteItem quoteItem, Asset asset)
        {
            return new PolicyFuneral(_policyHeader, quoteItem, asset);
        }

        public PolicyItem CreatePolicyItem(QuoteItem quoteItem, Asset asset)
        {
            if (!_dtCoverMapping.ContainsKey(quoteItem.CoverDefinition.Cover))
            {
                throw new Exception("You requested a policy item for cover type " + quoteItem.CoverDefinition.Cover.Name + " which is not yet supported.");
            }

            Func<QuoteItem, Asset, PolicyItem> fn = _dtCoverMapping[quoteItem.CoverDefinition.Cover];
            return fn(quoteItem, asset);
        }
    }
}
