﻿using AutoMapper;
using Domain.Base.Execution;
using Domain.Organizations;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Policy.FNOL;
using iPlatform.PolicyEngine.Data.Models;
using iPlatform.PolicyEngine.Data.Requests;
using MasterData;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;

namespace Domain.Policies.Mappings
{
    public class PolicyEngineMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<OrganizationPolicyServiceSetting, PolicyServiceSettingInfo>();
            Mapper.CreateMap<Organization, OrganizationData>();
            Mapper.CreateMap<OrganizationType, OrganizationTypeData>();
            Mapper.CreateMap<PolicyServiceSetting, PolicyServiceSettingData>();

            Mapper.CreateMap<PolicySearchDto, PolicyEngineRequest>()
                .ForMember(t => t.Setting, o => o.Ignore())
                .ForMember(t => t.Container, o => o.Ignore())
                ;


            Mapper.CreateMap<HandlerResult<PolicyHeaderDto>, GETResponseDto<PolicyHeaderDto>>()
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response));


            //Schedules
            Mapper.CreateMap<Schedule, PolicyScheduleDto>();
            Mapper.CreateMap<List<Schedule>, ListResultDto<PolicyScheduleDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));
                ;

                Mapper.CreateMap<FnolPolicyDto, FnolSimplePolicyHeader>()
                .ForMember(t => t.PolicyMasterGuid, o => o.MapFrom(a=>a.PolicyMasterGuid.ToString()));

                Mapper.CreateMap<List<FnolPolicyDto>, List<FnolSimplePolicyHeader>>();
                    

            Mapper.CreateMap<FnolPolicyDataResponseDto,PolicyDataForFNOLDto>();

            Mapper.CreateMap<FnolPolicyDataDto, PolicyDataFnolDto>();
            Mapper.CreateMap<InsurerProduct, InsurerProductDto>();
            Mapper.CreateMap<ClientInfo, ClientInfoDto>();
            Mapper.CreateMap<ClientAddressInfo, ClientAddressInfoDto>();
            Mapper.CreateMap<ClientBankInfo, ClientBankInfoDto>();
            Mapper.CreateMap<PolicyInfo, PolicyInfoDto>()
                .ForMember(t => t.Premium, o => o.MapFrom(s => new MoneyDto(s.Premium)))
                .ForMember(t => t.Fees, o => o.MapFrom(s => new MoneyDto(s.Fees)))
                .ForMember(t => t.Sasria, o => o.MapFrom(s => new MoneyDto(s.Sasria)))
                .ForMember(t => t.SasriaShortfall, o => o.MapFrom(s => new MoneyDto(s.SasriaShortfall)))
                ;
            Mapper.CreateMap<SectionInfo, SectionInfoDto>();
            Mapper.CreateMap<RiskItemInfo, RiskItemInfoDto>()
                .ForMember(t => t.Sum_Insured, o => o.MapFrom(s => new MoneyDto(s.Sum_Insured)))
                .ForMember(t => t.Premium, o => o.MapFrom(s => new MoneyDto(s.Premium)))
                .ForMember(t => t.Sasria, o => o.MapFrom(s => new MoneyDto(s.Sasria)))
                ;

            Mapper.CreateMap<RiskItemExtensionInfo, RiskItemExtensionInfoDto>()
                .ForMember(t => t.Sum_Insured, o => o.MapFrom(s => new MoneyDto(s.Sum_Insured)))
                .ForMember(t => t.Premium, o => o.MapFrom(s => new MoneyDto(s.Premium)))
                .ForMember(t => t.Sasria, o => o.MapFrom(s => new MoneyDto(s.Sasria)))
                ;
            Mapper.CreateMap<RiskItemFuneralInfo, RiskItemFuneralInfoDto>();

            Mapper.CreateMap<FnolPolicyDataDto, PolicyHeaderDto>()
                .ForMember(t => t.InceptionDate, o => o.Ignore())
                .ForMember(t => t.InsurerId, o => o.Ignore())
                .ForMember(t => t.InsurerName, o => o.Ignore())
                .ForMember(t => t.PaymentPlan, o => o.Ignore())
                .ForMember(t => t.PolicyItems, o => o.Ignore())
                .ForMember(t => t.PolicyMasterGuid, o => o.Ignore())
                .ForMember(t => t.PolicyNo, o => o.Ignore())
                .ForMember(t => t.PolicyStatus, o => o.Ignore())
                .ForMember(t => t.ProductId, o => o.Ignore())
                .ForMember(t => t.ProductName, o => o.Ignore())
                .ForMember(t => t.RenewalDate, o => o.Ignore())
                .ForMember(t => t.TotalPayment, o => o.Ignore())
                .ForMember(t => t.Id, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    var policy = s.InsurerProduct.FirstOrDefault();
                    var policyInfo = policy.policyInfo.FirstOrDefault();
                    d.InsurerName = policy.InsurerName;
                    d.PolicyNo = policyInfo.PolicyNo;
                    d.PolicyStatus = policyInfo.PolicyStatus;
                    d.RenewalDate = new DateTimeDto(policyInfo.AnnualRenewalDate);
                    d.PaymentPlan = policyInfo.PaymentMethod;
                    d.TotalPayment = new MoneyDto(policyInfo.Fees + policyInfo.Premium + policyInfo.Sasria);
                    d.InceptionDate = new DateTimeDto(policyInfo.OriginalInceptionDate);
                    d.PolicyMasterGuid = policyInfo.PolicyMasterGUID.ToString();
                    d.PolicyItems = new List<PolicyItemDto>();
                    policyInfo.sectionInfo.ForEach(x => x.riskItemInfo.ForEach(r =>
                    {
                        var item = new PolicyItemDto();
                        item.ItemDescription = r.ItemDescription;
                        item.Cover = x.CoverCode;
                        item.SumInsured =  new MoneyDto(r.Sum_Insured);
                        item.ItemMasterGuid = r.ItemMasterGUID.ToString();
                        item.TotalPayment =  new MoneyDto(r.Premium+r.Sasria);
                        d.PolicyItems.Add(item);
                    }));

                })
                ;
        }

    } 
}