﻿

using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Policies;
using System;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Base;

namespace Domain.Policies.Mappings
{
    public class PolicyItemMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PolicyItem, PolicyItemDto>()
                .ForMember(t => t.SumInsured, o => o.MapFrom(s => new MoneyDto(s.SumInsured)))
                .ForMember(t => t.ExcessBasic, o => o.MapFrom(s => new MoneyDto(s.ExcessBasic)))
                .ForMember(t => t.TotalPayment, o => o.MapFrom(s => new MoneyDto(s.Premium + s.Sasria)))
                .ForMember(t => t.VoluntaryExcess, o => o.MapFrom(s => new MoneyDto(s.VoluntaryExcess)))
                ;

            Mapper.CreateMap<CreatePolicyItemDto, PolicyItem>()
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                
                ;
        }
    }
}