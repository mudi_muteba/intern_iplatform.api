﻿using AutoMapper;
using Domain.Activities;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Policies;
using Domain.Products;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Policy;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Policies.Mappings
{
    public class PolicyHeaderClaimableItemMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PolicyHeaderClaimableItem, PolicyHeaderClaimableItemDto>();
        }
    }
}