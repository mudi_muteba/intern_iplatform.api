﻿using AutoMapper;
using Domain.Activities;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Policies;
using Domain.Policies.Message;
using Domain.Products;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Policy;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Policies.Mappings
{
    public class PolicyMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PolicyHeader, BasicPolicyHeaderDto>()
                .ForMember(t => t.TotalPayment, o => o.MapFrom(s => GetTotalPayment(s)));

            Mapper.CreateMap<PolicyHeader, PolicyHeaderDto>()
                .ForMember(t => t.PaymentPlan, o => o.MapFrom(s => s.PaymentPlan.Name))
                .ForMember(t => t.TotalPayment, o => o.MapFrom(s => GetTotalPayment(s)))
                .ForMember(t => t.InceptionDate, o=> o.MapFrom(s => s.DateEffective ?? DateTime.UtcNow))
                .ForMember(t => t.RenewalDate, o=> o.MapFrom(s => s.DateRenewal ?? DateTime.UtcNow))
                .ForMember(t => t.InsurerName, o => o.MapFrom(s => s.Product.ProductOwner.RegisteredName))
                ;
            Mapper.CreateMap<PagedResults<PolicyHeader>, PagedResultDto<PolicyHeaderDto>>();

            Mapper.CreateMap<PolicyHeaderDto, EditPolicyHeaderDto>();

            Mapper.CreateMap<EditPolicyHeaderDto, PolicyHeader>();

            Mapper.CreateMap<PagedResults<PolicyHeader>, PagedResultDto<BasicPolicyHeaderDto>>();

            Mapper.CreateMap<CreatePolicyHeaderDto, PolicyHeader>()
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.LeadActivity, o=> o.MapFrom(s => s.LeadActivityID > 0 ? new LeadActivity(){Id = s.LeadActivityID} : null))
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party.Party() { Id = s.PartyId }))
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }))
                .ForMember(t => t.PolicyItems, o => o.Ignore())
                ;

            Mapper.CreateMap<List<PolicyHeader>, ListResultDto<PolicyHeaderDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));


            Mapper.CreateMap<PolicyMetaData, CreatePolicyWhenQuoteAcceptedDto>();

        }

        private MoneyDto GetTotalPayment(PolicyHeader policyheader)
        {
            decimal totalForItems = policyheader.PolicyItems.Sum(a => (a.Premium + a.Sasria));

            return new MoneyDto(totalForItems + policyheader.Fees);
        }
    }
}