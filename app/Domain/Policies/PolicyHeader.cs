﻿using AutoMapper;
using Domain.Activities;
using Domain.Base;
using Domain.Party.Quotes;
using Domain.Products;
using iPlatform.Api.DTOs.Policy;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Policies
{
    public class PolicyHeader : Entity
    {
        public PolicyHeader()
        {
            PolicyItems = new List<PolicyItem>();
        }

        public PolicyHeader(Product product, Party.Party party)
        {
            this.Product = product;
            this.Party = party;
            this.PolicyNo = "Pending";
            this.PaymentPlan = PaymentPlans.Monthly;
            this.PolicyStatus = PolicyStatuses.Potential;
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
            PolicyItems = new List<PolicyItem>();
        }

        public PolicyHeader(Product product, Party.Party party, Quote quote, PaymentPlan paymentPlan, PolicyStatus policyStatus, LeadActivity activity, Guid reference = new Guid())
        {
            PaymentPlan paymentDetail = null;
            if (quote.QuoteHeader.Proposal.LeadActivity.Lead.Party.PaymentDetails != null)
            {
                var paymentDetials = quote.QuoteHeader.Proposal.LeadActivity.Lead.Party.PaymentDetails.LastOrDefault();
                if (paymentDetials != null)
                {
                    paymentDetail = paymentDetials.PaymentPlan;
                    DateUpdated = paymentDetials.TermEndDate;
                    DateCreated = paymentDetials.InceptionDate;
                }
            }
            else
            {
                paymentDetail = PaymentPlans.Monthly;
                DateCreated = DateTime.UtcNow;
                DateUpdated = DateTime.UtcNow;
            }
            
            this.Product = product;
            this.Party = party;
            this.PolicyNo = "Pending";
            this.Fees = quote.Fees;
            this.PlatformId = quote.PlatformId;
            this.QuoteId = quote.Id;
            this.PaymentPlan = paymentDetail;
            this.PolicyStatus = policyStatus ?? PolicyStatuses.Potential;
            this.LeadActivity = activity;
            Reference = reference;
            PolicyItems = new List<PolicyItem>();
        }

        public virtual LeadActivity LeadActivity { get; set; }
        public virtual string QuoteNo { get; set; }
        public virtual string PolicyNo { get; set; }
        public virtual Party.Party Party { get; protected set; }
        public virtual Product Product { get; protected set; }
        public virtual DateTime? DateEffective { get; set; }
        public virtual DateTime? DateEnd { get; set; }
        public virtual DateTime? DateRenewal { get; set; }
        public virtual DateTime? DateCreated { get; set; }
        public virtual DateTime? DateUpdated { get; set; }
        public virtual int Frequency { get; set; }
        public virtual PaymentPlan PaymentPlan { get; set; }
        public virtual bool Renewable { get; set; }
        public virtual PolicyStatus PolicyStatus { get; set; }
        public virtual bool NewBusiness { get; set; }
        public virtual decimal Fees { get; set; }
        public virtual bool VAP { get; set; }
        public virtual IList<PolicyItem> PolicyItems { get; set; }
        public virtual int QuoteId { get; set; }
        public virtual Guid PlatformId { get; set; }
        public virtual Guid ExternalPlatformGuid { get; set; }
        public virtual Guid Reference { get; set; }

        public virtual PolicyHeader Create(CreatePolicyHeaderDto dto)
        {
            var policyHeader = Mapper.Map<PolicyHeader>(dto);

            AddPolicyItems(dto.PolicyItems, policyHeader);

            return policyHeader;
        }

        public virtual void AddPolicyItems(List<CreatePolicyItemDto> policyItems, PolicyHeader entity)
        {
            foreach(var dto in policyItems)
            {
                var policyItem = Mapper.Map<PolicyItem>(dto);
                policyItem.PolicyHeader = entity;
                entity.AddPolicyItem(policyItem);
            }
        }


        public virtual void AddPolicyItem(PolicyItem policyItem)
        {
            PolicyItems.Add(policyItem);
        }

        public virtual void SetActivity(LeadActivity activity)
        {
            this.LeadActivity = activity;
            DateUpdated = DateTime.UtcNow;
        }
    }
}
