﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using ValidationMessages.Campaigns;
using System.Collections.Generic;
using ValidationMessages.Individual;
using Domain.Party.Queries;
using iPlatform.Api.DTOs.Policy;
using Domain.Policies.Queries;
using ValidationMessages.Policy;


namespace Domain.Policies.Validation
{
    public class PolicyHeaderByIdAndPartyIdValidator : IValidateDto<GetPolicyDataForFNOLDto>
    {

        public PolicyHeaderByIdAndPartyIdValidator(IProvideContext contextProvider, IRepository repository)
        {
            this.repository = repository;
        }
        private IRepository repository;


        public void Validate(GetPolicyDataForFNOLDto dto, ExecutionResult result)
        {
            ValidatePolicyHeader(dto.Id, dto.PartyId, result);
        }

        private void ValidatePolicyHeader(int policyHeaderId, int PartyId,  ExecutionResult result)
        {
            var queryResult = GetPolicyHeaderByIdQuery(policyHeaderId, PartyId);

            if (queryResult.Count() <= 0)
            {
                result.AddValidationMessage(PolicyValidationMessages.InvalidId.AddParameters(new[] { policyHeaderId.ToString() }));
            }
        }

        private IQueryable<PolicyHeader> GetPolicyHeaderByIdQuery(int id, int partyId)
        {
            return repository.GetAll<PolicyHeader>().Where(x => x.Party.Id == partyId && x.Id == id && x.IsDeleted == false);
        }
    }
}