﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using ValidationMessages.Campaigns;
using System.Collections.Generic;
using ValidationMessages.Individual;
using Domain.Party.Queries;
using iPlatform.Api.DTOs.Policy;
using Domain.Policies.Queries;
using ValidationMessages.Policy;


namespace Domain.Policies.Validation
{
    public class PolicyHeaderValidator : IValidateDto<GetPolicyAvailableItemsForFNOLDto> 
    {
        private readonly GetPolicyByIdQuery query;

        public PolicyHeaderValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetPolicyByIdQuery(contextProvider, repository);
        }

        public void Validate(GetPolicyAvailableItemsForFNOLDto dto, ExecutionResult result)
        {
            ValidatePolicyHeader(dto.Id, result);
        }


        private void ValidatePolicyHeader(int policyHeaderId, ExecutionResult result)
        {
            var queryResult = GetPolicyHeaderByIdQuery(policyHeaderId);

            if (queryResult.Count() <= 0)
            {
                result.AddValidationMessage(PolicyValidationMessages.InvalidId.AddParameters(new[] { policyHeaderId.ToString() }));
            }
        }

        private IQueryable<PolicyHeader> GetPolicyHeaderByIdQuery(int id)
        {
            return query.WithId(id).ExecuteQuery();
        }
    }
}