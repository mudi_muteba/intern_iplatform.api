﻿using System.Linq;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Validation;
using Domain.Policies.Queries;
using iPlatform.Api.DTOs.Upload;
using ValidationMessages.Policy;

namespace Domain.Policies.ValidateDtoHandlers
{
    public class PolicyUploadDtoValidator : IValidateDto<PolicyUploadDto>
    {
        private static readonly ILog Log = LogManager.GetLogger<PolicyUploadDtoValidator>();
        private readonly GetPolicyByPlatformIdQuery _query;

        public PolicyUploadDtoValidator(GetPolicyByPlatformIdQuery query)
        {
            _query = query;
        }

        public void Validate(PolicyUploadDto dto, ExecutionResult result)
        {
            var policyHeaders = _query.WithPlatformId(dto.Id).ExecuteQuery();
            if (policyHeaders.Any()) return;

            Log.DebugFormat("Debug: Request {0} with policy number {1} not found in Policy", dto.Id.ToString(), dto.ExternalPolicyNo);
            result.AddValidationMessage(PolicyUploadConfirmationValidationMessages.PlatformIdNotFound.AddParameters(new []{dto.Id + ""}));
        }
    }
}