﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Policies
{
    public class PolicyHeaderClaimableItem : Entity
    {
        public virtual PolicyHeader PolicyHeader { get; set; }
        public virtual Guid RequestId { get; set; }
        public virtual DateTime DateOfLoss { get; set; }
        public virtual PolicyItem PolicyItem { get; set; }
        public virtual Guid ExternalItemId { get; set; }
        public virtual Guid ExternalItemMasterId { get; set; }
        public virtual Guid ExternalSubmitGuid { get; set; }
        public virtual string Description { get; set; }
    }
}
