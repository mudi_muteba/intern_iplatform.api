﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using MasterData;

namespace Domain.Policies.Overrides
{
    public class PolicyItemOverride : IAutoMappingOverride<PolicyItem>
    {
        public void Override(AutoMapping<PolicyItem> mapping)
        {
            mapping.Table(mapping.GetType().GetGenericArguments()[0].Name);
        }
    }
}