﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Policies.Overrides
{
    public class PolicyHeaderOverride : IAutoMappingOverride<PolicyHeader>
    {
        public void Override(AutoMapping<PolicyHeader> mapping)
        {
            mapping.HasMany(x => x.PolicyItems).Cascade.SaveUpdate();
        }
    }
}