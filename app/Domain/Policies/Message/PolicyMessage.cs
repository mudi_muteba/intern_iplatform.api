﻿using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Policies.Message
{
    public class PolicyMessage : WorkflowExecutionMessage
    {
        protected PolicyMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }
        public PolicyMessage(ITaskMetadata metadata) : base(metadata) { }

        public static PolicyMessage UpdatePolicyMessage(PublishQuoteUploadMessageDto dto, string referenceNumber)
        {
            return new PolicyMessage(new PolicyMetaData(dto.Id, dto.Reference, dto.QuoteAcceptedLeadActivityId, referenceNumber));
        }
    }
}
