﻿using iPlatform.Api.DTOs.Policy;
using System;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Policies.Message
{
    public class PolicyMetaData : ITaskMetadata
    {
        public PolicyMetaData()
        {
        }

        public PolicyMetaData(int quoteId, Guid reference, int quoteAcceptedLeadActivityId, string policyNo)
        {
            QuoteId = quoteId;
            Reference = reference;
            PolicyNo = policyNo;
            QuoteAcceptedLeadActivityId = quoteAcceptedLeadActivityId;
        }

        public int QuoteId { get; set; }
        public Guid Reference { get; set; }
        public string PolicyNo { get; set; }
        public int QuoteAcceptedLeadActivityId { get; set; }
    }
}
