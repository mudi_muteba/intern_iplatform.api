﻿using Domain.Base.Events;
using iPlatform.Api.DTOs.Policy;

namespace Domain.Policies.Event
{
    public class PolicyUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public EditPolicyHeaderDto EditPolicyHeaderDto { get; private set; }

        public PolicyUpdatedEvent(PolicyHeader entity)
        {
            this.EditPolicyHeaderDto = EditPolicyHeaderDto;
        }
    }
}
