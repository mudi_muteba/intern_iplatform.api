﻿using Domain.Base;
using Domain.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Policies
{
    public class OrganizationPolicyServiceSetting : Entity
    {
        public OrganizationPolicyServiceSetting()
        {

        }
        
        public virtual int ChannelId { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual PolicyServiceSetting PolicyServiceSetting { get; set; }
    }
}
