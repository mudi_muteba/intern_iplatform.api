﻿using AutoMapper;
using Domain.Policies.Message;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Enums;
using Infrastructure.Configuration;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Policies.Consumers
{
    public class PolicyMessageConsumer : AbstractMessageConsumer<PolicyMessage>
    {
        private readonly IConnector _connector;

        public PolicyMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector) : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
        }

        public override void Consume(PolicyMessage message)
        {
            var metaData = (PolicyMetaData)message.Metadata;
            var dto = Mapper.Map<CreatePolicyWhenQuoteAcceptedDto>(metaData);

            _connector.PolicyHeaderManagement.PolicyHeaders.CreatePolicyWhenQuoteAccepted(dto);
        }
    }
}
