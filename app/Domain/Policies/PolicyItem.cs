﻿using Common.Logging;
using Domain.Base;
using Domain.Party.Addresses;
using Domain.Party.Assets;
using Domain.Party.Assets.AssetAddresses;
using Domain.Party.Assets.AssetRiskItems;
using Domain.Party.Assets.AssetVehicles;
using Domain.Party.Quotes;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Policies
{
    public class PolicyItem : Entity
    {
        public PolicyItem()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }
        public virtual PolicyHeader PolicyHeader { get; set; }
        public virtual int AssetNumber { get; set; }
        public virtual string ItemDescription { get; set; }
        public virtual DateTime? DateEffective { get; set; }
        public virtual DateTime? DateEnd { get; set; }
        public virtual DateTime? DateCreated { get; set; }
        public virtual DateTime? DateUpdated { get; set; }
        public virtual decimal SumInsured { get; set; }
        public virtual decimal Premium { get; set; }
        public virtual decimal Sasria { get; set; }
        public virtual decimal ExcessBasic { get; set; }
        public virtual decimal VoluntaryExcess { get; set; }
        public virtual Cover Cover { get; set; }
        public virtual string ExternalGuid { get; set; }
        public virtual string ExternalMasterGuid { get; set; }

        public virtual PolicyItem GetBase()
        {
            var baseItem = new PolicyItem();
            UpdateForType(this.GetType().BaseType, this, baseItem);
            return baseItem;
        }

        private static void UpdateForType(Type type, PolicyItem source, PolicyItem destination)
        {
            var myObjectFields = type.GetFields(
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

            foreach (var fi in myObjectFields)
            {
                fi.SetValue(destination, fi.GetValue(source));
            }
        }
    }

    public class PolicyAccident : PolicyItem
    {
        public virtual string TypeCode { get; set; }
        public virtual string Description { get; set; }

    }

    public class PolicyFuneral : PolicyItem
    {
        protected PolicyFuneral()
        {

        }

        public PolicyFuneral(PolicyHeader policyHeader, QuoteItem quoteItem, Asset asset)
        {
            AssetAddress item = asset as AssetAddress;

            if (item == null)
            {
                throw new Exception("The asset " + asset.GetType() + " specified is not an AssetAddress.");
            }

            this.PolicyHeader = policyHeader;
            this.AssetNumber = quoteItem.Asset.AssetNumber;
            this.ItemDescription = asset.Description;
            this.Description = asset.Description;
            this.Address = item.Address;
            this.Cover = quoteItem.CoverDefinition.Cover;
            this.Premium = quoteItem.Fees.Premium;
            this.SumInsured = quoteItem.Asset.SumInsured;
            this.Sasria = quoteItem.Fees.Sasria;
            this.ExcessBasic = quoteItem.Excess.Basic;
            this.VoluntaryExcess = quoteItem.Excess.VoluntaryExcess;
        }

        public virtual Address Address { get; set; }
        public virtual string Description { get; set; }
    }

    public class PolicyAllRisk : PolicyItem
    {
        protected PolicyAllRisk()
        {

        }

        public PolicyAllRisk(PolicyHeader policyHeader, QuoteItem quoteItem, Asset asset)
        {
            AssetRiskItem item = asset as AssetRiskItem;

            if (item == null)
            {
                throw new Exception("The asset " + asset.GetType() + " specified is not an AssetRiskItem.");
            }

            this.PolicyHeader = policyHeader;
            this.AssetNumber = quoteItem.Asset.AssetNumber;
            this.ItemDescription = asset.Description;
            this.SerialNo = item.SerialIMEINumber;
            this.TypeCode = item.AllRiskCategory.ToString();
            this.Cover = quoteItem.CoverDefinition.Cover;
            this.Premium = quoteItem.Fees.Premium;
            this.SumInsured = quoteItem.Asset.SumInsured;
            this.Sasria = quoteItem.Fees.Sasria;
            this.ExcessBasic = quoteItem.Excess.Basic;
            this.VoluntaryExcess = quoteItem.Excess.VoluntaryExcess;
        }

        public virtual Address Address { get; set; }
        public virtual string TypeCode { get; set; }
        public virtual string Description { get; set; }
        public virtual string SerialNo { get; set; }
    }

    public class PolicySpecial : PolicyItem
    {
        protected PolicySpecial()
        {

        }

        public PolicySpecial(PolicyHeader policyHeader, QuoteItem quoteItem, Asset asset)
        {
            AssetAddress item = asset as AssetAddress;

            if (item == null)
            {
                throw new Exception("The asset " + asset.GetType() + " specified is not an AssetAddress.");
            }

            this.PolicyHeader = policyHeader;
            this.AssetNumber = quoteItem.Asset.AssetNumber;
            this.ItemDescription = asset.Description;
            this.Description = asset.Description;
            this.Address = item.Address;
            this.Cover = quoteItem.CoverDefinition.Cover;
            this.Premium = quoteItem.Fees.Premium;
            this.SumInsured = quoteItem.Asset.SumInsured;
            this.Sasria = quoteItem.Fees.Sasria;
            this.ExcessBasic = quoteItem.Excess.Basic;
            this.VoluntaryExcess = quoteItem.Excess.VoluntaryExcess;
        }

        public virtual Address Address { get; set; }
        public virtual string Description { get; set; }
    }

    public class PolicyBuilding : PolicyItem
    {
        protected PolicyBuilding()
        {

        }

        public PolicyBuilding(PolicyHeader policyHeader, QuoteItem quoteItem, Asset asset)
        {
            AssetAddress item = asset as AssetAddress;

            if (item == null)
            {
                throw new Exception("The asset " + asset.GetType() + " specified is not an AssetAddress.");
            }

            this.PolicyHeader = policyHeader;
            this.AssetNumber = quoteItem.Asset.AssetNumber;
            this.ItemDescription = asset.Description;
            this.Description = asset.Description;
            this.Address = item.Address;
            this.Cover = quoteItem.CoverDefinition.Cover;
            this.Premium = quoteItem.Fees.Premium;
            this.SumInsured = quoteItem.Asset.SumInsured;
            this.Sasria = quoteItem.Fees.Sasria;
            this.ExcessBasic = quoteItem.Excess.Basic;
            this.VoluntaryExcess = quoteItem.Excess.VoluntaryExcess;
        }

        public virtual Address Address { get; set; }
        public virtual string TypeCode { get; set; }
        public virtual string Description { get; set; }
        public virtual string SerialNo { get; set; }
    }

    public class PolicyContent : PolicyItem
    {
        protected PolicyContent()
        {

        }

        public PolicyContent(PolicyHeader policyHeader, QuoteItem quoteItem, Asset asset)
        {
            AssetAddress item = asset as AssetAddress;

            if (item == null)
            {
                throw new Exception("The asset " + asset.GetType() + " specified is not an AssetAddress.");
            }

            this.PolicyHeader = policyHeader;
            this.AssetNumber = quoteItem.Asset.AssetNumber;
            this.ItemDescription = asset.Description;
            this.Description = asset.Description;
            this.Address = item.Address;
            this.Cover = quoteItem.CoverDefinition.Cover;
            this.Premium = quoteItem.Fees.Premium;
            this.SumInsured = quoteItem.Asset.SumInsured;
            this.Sasria = quoteItem.Fees.Sasria;
            this.ExcessBasic = quoteItem.Excess.Basic;
            this.VoluntaryExcess = quoteItem.Excess.VoluntaryExcess;
        }

        public virtual Address Address { get; set; }
        public virtual string TypeCode { get; set; }
        public virtual string Description { get; set; }
        public virtual string SerialNo { get; set; }
    }

    public class PolicyCoverage : PolicyItem
    {
        public virtual decimal CoverageLimit { get; set; }
        public virtual int ParentCoverage { get; set; }
    }

    public class PolicyFinance : PolicyItem
    {
        public virtual decimal Amount { get; set; }
        public virtual decimal TAX { get; set; }
        public virtual decimal AmountProRata { get; set; }
        public virtual decimal TAXProRata { get; set; }
    }

    public class PolicyLiability : PolicyItem
    {
        public virtual string TypeCode { get; set; }
        public virtual string Description { get; set; }
    }

    public class PolicyPersonalVehicle : PolicyItem
    {
        private static readonly ILog log = LogManager.GetLogger<PolicyPersonalVehicle>();

        protected PolicyPersonalVehicle()
        {

        }

        public PolicyPersonalVehicle(PolicyHeader policyHeader, QuoteItem quoteItem, Asset asset)
        {
            if (asset == null)
            {
                log.ErrorFormat("The asset can not be null: Policy Header Id {0}", policyHeader.Id);

            }

            AssetVehicle vehicle = asset as AssetVehicle;

            if (vehicle == null)
            {
                log.ErrorFormat("Unable to cast asset {0}", asset.Id);
                throw new Exception("The asset " + asset.GetType() + " specified is not an AssetVehicle.");
            }



            this.PolicyHeader = policyHeader;
            this.AssetNumber = quoteItem.Asset.AssetNumber;
            this.Make = vehicle.VehicleMake;
            this.Model = vehicle.VehicleModel;
            this.Year = Int32.Parse(vehicle.YearOfManufacture.ToString());
            this.RegistrationNo = vehicle.VehicleRegistrationNumber;
            this.ItemDescription = asset.Description;
            this.Cover = quoteItem.CoverDefinition.Cover;
            this.Premium = quoteItem.Fees.Premium;
            this.SumInsured = quoteItem.Asset.SumInsured;
            this.Sasria = quoteItem.Fees.Sasria;
            this.ExcessBasic = quoteItem.Excess.Basic;
            this.VoluntaryExcess = quoteItem.Excess.VoluntaryExcess;
        }

        public virtual Address Address { get; set; }
        public virtual string VehicleSymbolCode { get; set; }
        public virtual string Make { get; set; }
        public virtual string Model { get; set; }
        public virtual int Year { get; set; }
        public virtual int CFG { get; set; }
        public virtual string ChassisNo { get; set; }
        public virtual string EngineNo { get; set; }
        public virtual string RegistrationNo { get; set; }
        public virtual string VinNo { get; set; }
        public virtual bool VehicleAlteredIndicator { get; set; }
        public virtual bool VehicleOptionsIncludedIndicator { get; set; }
        public virtual bool VehicleModificationsIncludedIndicator { get; set; }
        public virtual bool AntiTheftDeviceIndicator { get; set; }
        public virtual bool AntiLockBrakesIndicator { get; set; }
        public virtual bool ExistingUnrepairedDamageIndicator { get; set; }
        public virtual bool FourWheelDriveIndicator { get; set; }
        public virtual bool TractionControlIndicator { get; set; }
        public virtual bool SeenVehicleIndicator { get; set; }
        public virtual bool DualControlsIndicator { get; set; }
        public virtual bool CarpoolIndicator { get; set; }
        public virtual bool EmployeeOperatedIndicator { get; set; }
        public virtual bool IsRegisteredIndicator { get; set; }
        public virtual bool NonOwnedVehicleIndicator { get; set; }
        public virtual bool AgreedValueRequiredIndicator { get; set; }



    }

    public class PolicyWatercraft : PolicyItem
    {
        public virtual string TypeCode { get; set; }
        public virtual string Description { get; set; }
    }

    public class PolicyPersonalAccident : PolicyItem
    {
        public virtual Address Address { get; set; }
        public virtual string TypeCode { get; set; }
        public virtual string Description { get; set; }
        private static readonly ILog log = LogManager.GetLogger<PolicyPersonalAccident>();
        public PolicyPersonalAccident()
        {

        }

        public PolicyPersonalAccident(PolicyHeader policyHeader, QuoteItem quoteItem, Asset asset)
        {

            AssetAddress item = asset as AssetAddress;

            if (item == null)
            {
                throw new Exception("The asset " + asset.GetType() + " specified is not an AssetAddress: Policy Header Id " + policyHeader.Id);
            }

            this.Address = item.Address;
            this.PolicyHeader = policyHeader;
            this.AssetNumber = quoteItem.Asset.AssetNumber;
            this.Description = asset.Description;
            this.ItemDescription = asset.Description;
            this.Cover = quoteItem.CoverDefinition.Cover;
            this.Premium = quoteItem.Fees.Premium;
            this.SumInsured = quoteItem.Asset.SumInsured;
            this.Sasria = quoteItem.Fees.Sasria;
            this.ExcessBasic = quoteItem.Excess.Basic;
            this.VoluntaryExcess = quoteItem.Excess.VoluntaryExcess;
        }
    }
}
