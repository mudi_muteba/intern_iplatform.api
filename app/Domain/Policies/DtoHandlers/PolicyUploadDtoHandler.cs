﻿using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Policies.Queries;
using iPlatform.Api.DTOs.Upload;
using MasterData.Authorisation;
using Shared.Extentions;

namespace Domain.Policies.DtoHandlers
{
    public class PolicyUploadDtoHandler : BaseDtoHandler<PolicyUploadDto, Guid>
    {
        private readonly GetPolicyByPlatformIdQuery _query;

        public PolicyUploadDtoHandler(IProvideContext contextProvider, GetPolicyByPlatformIdQuery query) : base(contextProvider)
        {
            _query = query;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    
                };
            }
        }

        protected override void InternalHandle(PolicyUploadDto dto, HandlerResult<Guid> result)
        {
            this.Debug(() => string.Format("Received a policy upload confirmation PlatformId:{0} Policy No:{1}", dto.Id, dto.ExternalPolicyNo));

            var policyHeaders = _query.WithPlatformId(dto.Id).ExecuteQuery();
            foreach (var policyHeader in policyHeaders)
                policyHeader.PolicyNo = dto.ExternalPolicyNo;

            result.Processed(dto.Id);
        }
    }
}