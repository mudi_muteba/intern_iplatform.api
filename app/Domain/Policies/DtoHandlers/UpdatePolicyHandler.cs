﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Policy;
using MasterData.Authorisation;

namespace Domain.Policies.DtoHandlers
{
    public class UpdatePolicyHandler : BaseDtoHandler<EditPolicyHeaderDto, int>
    {
        public UpdatePolicyHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.Edit
                };
            }
        }


        protected override void InternalHandle(EditPolicyHeaderDto dto, HandlerResult<int> result)
        {
            var entity = _repository.GetAll<PolicyHeader>().FirstOrDefault(x => x.Reference == dto.Reference);
            if (entity == null) return;
            entity.PolicyNo = dto.PolicyNo;

            _repository.Save(entity);
        }
    }
}
