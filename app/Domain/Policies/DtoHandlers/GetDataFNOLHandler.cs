﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Policy.FNOL;
using iPlatform.Integration.Services.Cims;
using MasterData.Authorisation;
using Remotion.Logging;

namespace Domain.Policies.DtoHandlers
{
    public class GetDataFNOLHandler : BaseDtoHandler<GetPolicyDataForFNOLDto, PolicyDataFnolDto>
    {
        public GetDataFNOLHandler(IProvideContext contextProvider, IRepository repository, IWindsorContainer container)
            : base(contextProvider)
        {
            this.repository = repository;
            this.container = container;
            this.log = LogManager.GetLogger(this.GetType());
        }
        private readonly ILog log;
        private IWindsorContainer container;
        private IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void InternalHandle(GetPolicyDataForFNOLDto dto, HandlerResult<PolicyDataFnolDto> result)
        {
            var individual = repository.GetById<Individual>(dto.PartyId);
            DateTime dtDateOfLoss = DateTime.Parse(dto.DateOfLoss);
            var policyHeader = repository.GetAll<PolicyHeader>().FirstOrDefault(x => x.Party.Id == dto.PartyId && x.Id == dto.Id && x.IsDeleted == false);

            CimsPolicies policyIntegration = new CimsPolicies();

            var requestGuid = Guid.NewGuid();

            FnolPolicyDataResponseDto policyData = policyIntegration.GetDetailForPolicyGuid(policyHeader.PlatformId, dtDateOfLoss, requestGuid);

            result.Processed(Mapper.Map<PolicyDataFnolDto>(policyData));

        }
    }
}
