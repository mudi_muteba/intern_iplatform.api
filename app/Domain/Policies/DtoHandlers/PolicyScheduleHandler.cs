﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Policy;
using iPlatform.PolicyEngine;
using iPlatform.PolicyEngine.Data.Requests;
using MasterData.Authorisation;

namespace Domain.Policies.DtoHandlers
{
    public class PolicyScheduleHandler : BaseDtoHandler<PolicySearchDto, ListResultDto<PolicyScheduleDto>>
    {
        public PolicyScheduleHandler(IProvideContext contextProvider, IRepository repository, IWindsorContainer container)
            : base(contextProvider)
        {
            _repository = repository;
            _container = container;
        }

        private readonly IWindsorContainer _container;
        private readonly IRepository _repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(PolicySearchDto dto, HandlerResult<ListResultDto<PolicyScheduleDto>> result)
        {
            var returnSchedule = new ListResultDto<PolicyScheduleDto>();
            
            var organizationPolicyServiceSetting = _repository.GetAll<OrganizationPolicyServiceSetting>().FirstOrDefault(a => a.Organization.Code == dto.insurerCode);
            if (organizationPolicyServiceSetting != null)
            {
                try
                {
                    var request = Mapper.Map<PolicyEngineRequest>(dto);
                    request.Setting = Mapper.Map<PolicyServiceSettingInfo>(organizationPolicyServiceSetting);
                    request.Container = _container;

                    var scheduleData = PolicyEngineFactory.GetPolicySchedule(request);

                    if (scheduleData.Errors.Any())
                        scheduleData.Errors.ForEach(e => AddMessage(e, result));
                    else
                    {
                        returnSchedule.Results = Mapper.Map<List<PolicyScheduleDto>>(scheduleData.Schedules);

                        result.Processed(returnSchedule);
                    }
                }
                catch (Exception e)
                {
                    AddErrorMessage(e, result);
                    result.Processed(returnSchedule);
                }
            }
            else
            {
                var message = string.Format("Executing handler {0}, ERROR : No OrganizationPolicyServiceSetting obtained for insurer {1}",
                                GetType().ToString(), dto.insurerCode);
                AddMessage(message, result);
                result.Processed(returnSchedule);
            }
        }

        #region Add Error Message
        private void AddErrorMessage(Exception e, HandlerResult<ListResultDto<PolicyScheduleDto>> result)
        {
            var message = string.Format("Executing handler {0} (Message: {1},InnerException: {2}, Stacktrace: {3})",
                                this.GetType().ToString(), e.Message, e.InnerException.ToString(), e.StackTrace.ToString());

            AddMessage(message, result); 
        }
        private void AddMessage(string error, HandlerResult<ListResultDto<PolicyScheduleDto>> result)
        {
            Log.ErrorFormat(error);
            result.AddFailure(error);
        }
        #endregion
    }
}