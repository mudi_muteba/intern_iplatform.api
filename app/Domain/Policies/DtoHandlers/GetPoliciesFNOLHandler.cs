﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin.DisplaySetting;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.CimsIntegrationLogging;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Policy.FNOL;
using iPlatform.Integration.Services.Cims;
using MasterData;
using MasterData.Authorisation;
using Raven.Client.Linq;

namespace Domain.Policies.DtoHandlers
{
    public class GetPoliciesFNOLHandler : BaseDtoHandler<GetPoliciesFNOLDto, ListResultDto<PolicyHeaderDto>>
    {
        public GetPoliciesFNOLHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            this._repository = repository;
            this._executionPlan = executionPlan;
        }

        private IExecutionPlan _executionPlan;
        private readonly IRepository _repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(GetPoliciesFNOLDto dto, HandlerResult<ListResultDto<PolicyHeaderDto>> result)
        {
            var individual = _repository.GetById<Individual>(dto.PartyId);

            if (individual != null)
            {
                List<PolicyHeader> policyHeaders = GetPolicyHeadersForClaim(individual);

                //Convert policy headers to dtos
                var dtos = Mapper.Map<ListResultDto<PolicyHeaderDto>>(policyHeaders.ToList());
                result.Processed(dtos);
            }
        }

        private List<PolicyHeader> GetPolicyHeadersForClaim(Individual party)
        {
            List<PolicyHeader> policyHeaders = new List<PolicyHeader>();

            var entityPolicies = _repository.GetAll<PolicyHeader>().Where(a => a.Party.Id == party.Id).ToList();

            int[] productIds = entityPolicies.Select(ep => ep.Product.Id).ToArray();
            IQueryable<DisplaySetting> displaySettings = _repository.GetAll<DisplaySetting>()
                .Where(ds => productIds.Contains(ds.Product.Id) &&
                    ds.Channel.Id == party.Channel.Id);

            //Check if any of the products require a third party call.
            if (!displaySettings.Any(ds => ds.ThirdPartyDisplay))
                return entityPolicies;

            //Fetch policies from CimsConnect
            CimsPolicies policyIntegration = new CimsPolicies();
            Guid requestGuid = Guid.NewGuid();
            FnolPolicyResponseDto cimsResponse = policyIntegration.GetSimplePoliciesForIDNumber(party.IdentityNo, requestGuid);

            //Cims Logging
            CreateCimsIntegrationLoggingDto integrationLog = new CreateCimsIntegrationLoggingDto
            {
                RequestId = requestGuid,
                RequestData = party.IdentityNo,
                ResponseData = cimsResponse.RawResponse
            };
            var response = _executionPlan.Execute<CreateCimsIntegrationLoggingDto, int>(integrationLog);


            foreach (var policyFnol in cimsResponse.Data)
            {
                Log.InfoFormat("processing cims response policy {0} {1}", policyFnol.PolicyNo,policyFnol.PolicyMasterGuid);
                var foundPolicy =
                    entityPolicies.FirstOrDefault(p => p.PolicyNo.ToString().ToUpper() == policyFnol.PolicyNo.ToUpper());

                if (foundPolicy != null)
                {
                    foundPolicy.PolicyNo = policyFnol.PolicyNo;
                    foundPolicy.DateEffective = policyFnol.OnCoverFrom;
                    foundPolicy.DateEnd = policyFnol.OnCoverTo;
                    foundPolicy.DateRenewal = policyFnol.AnnualRenewalDate;
                    foundPolicy.ExternalPlatformGuid = policyFnol.PolicyMasterGuid;

                    var status =
                        new PolicyStatuses().FirstOrDefault(x => x.Name.ToLower() == policyFnol.PolicyStatus.ToLower());

                    if (status != null)
                        foundPolicy.PolicyStatus = status;

                    var paymentPlan =
                        new PaymentPlans().FirstOrDefault(x => x.Name.ToLower() == policyFnol.PolicyType.ToLower());

                    if (paymentPlan != null)
                        foundPolicy.PaymentPlan = paymentPlan;

                    _repository.Save<PolicyHeader>(foundPolicy);
                }

                policyHeaders.Add(foundPolicy);
            }

            if (!cimsResponse.Data.Any())
                return entityPolicies;

            //get policies not found in cims
            if (cimsResponse.Data.Any())
            {
                var policiesNotCims =
                    entityPolicies.Where(x => cimsResponse.Data.All(y => y.PolicyNo.ToUpper() != x.PolicyNo.ToUpper()));

                policyHeaders.AddRange(policiesNotCims);
            }

            return policyHeaders;
        }
    }
}
