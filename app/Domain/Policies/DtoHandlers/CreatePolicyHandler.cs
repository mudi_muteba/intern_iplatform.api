﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Policy;
using MasterData.Authorisation;

namespace Domain.Policies.DtoHandlers
{
    public class CreatePolicyHandler : CreationDtoHandler<PolicyHeader, CreatePolicyHeaderDto, int>
    {
        public CreatePolicyHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(PolicyHeader entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override PolicyHeader HandleCreation(CreatePolicyHeaderDto dto, HandlerResult<int> result)
        {
            var policyHeader = new PolicyHeader().Create(dto);

             result.Processed(policyHeader.Id);
             return policyHeader;
        }

    }
}