﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Claims.ClaimsHeaders;
using Domain.Individuals;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.CimsIntegrationLogging;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Policy.FNOL;
using iPlatform.Integration.Services.Cims;
using MasterData;
using MasterData.Authorisation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Remotion.Logging;

namespace Domain.Policies.DtoHandlers
{
    public class GetAvailableDataFNOLHandler : BaseDtoHandler<GetPolicyAvailableItemsForFNOLDto, ListResultDto<PolicyHeaderClaimableItemDto>>
    {
        public GetAvailableDataFNOLHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            this.repository = repository;
            this._executionPlan = executionPlan;
            this.log = LogManager.GetLogger(this.GetType());
        }
        private readonly ILog log;
        private IExecutionPlan _executionPlan;
        private IRepository repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }
        protected override void InternalHandle(GetPolicyAvailableItemsForFNOLDto dto, HandlerResult<ListResultDto<PolicyHeaderClaimableItemDto>> result)
        {
            var individual = repository.GetById<Individual>(dto.PartyId);

            DateTime? dtDateOfLoss = null;
            if(dto.ClaimsHeaderId > 0)
            {
                var claimsHeader = repository.GetById<ClaimsHeader>(dto.ClaimsHeaderId);

                if (claimsHeader.DateOfLoss != null) dtDateOfLoss = claimsHeader.DateOfLoss.Value;
            }

            if (dtDateOfLoss == null)
            {
                DateTime parsedDate = new DateTime();
                bool dateParsed = DateTime.TryParse(dto.DateOfLoss, out parsedDate);
                dtDateOfLoss = parsedDate;
            }

            //Call setup method calling cims which will update PolicyHeaderClaimableItem table
            //Fetch policy items where found in PolicyHeaderClaimableItem for policy header id and specirfieddate of loss
            try
            {
                var response = GetItemsAvailableForClaim(dto.Id, dto.ClaimsHeaderId, dtDateOfLoss.Value);
                result.Processed(new ListResultDto<PolicyHeaderClaimableItemDto>() { Results = response });
            }
            catch(Exception ex)
            {
                log.Error(String.Format("Cims call failure. Unable to retrieve available items for claims(PolicHeaderId: {0}, ClaimsHeaderId: {1}, DateOfLoss: {2}). Message: {3}",
                    dto.Id.ToString(), dto.ClaimsHeaderId.ToString(), dtDateOfLoss.Value.ToShortDateString(), ex.Message), ex);
            }
        }


        #region Get Policy Claimable Item from Cims
        private List<PolicyHeaderClaimableItemDto> GetItemsAvailableForClaim(int policyHeaderId, int claimHeaderId, DateTime dateOfLoss)
        {
            List<PolicyHeaderClaimableItem> result = new List<PolicyHeaderClaimableItem>();

            var policyHeaderEntity = repository.GetById<PolicyHeader>(policyHeaderId);

            if (claimHeaderId == 0)
                return SyncItemAvailableForClaim(policyHeaderEntity, dateOfLoss);
            else 
                return GetPolicyHeaderClaimableItems(policyHeaderEntity, dateOfLoss);
        }
        private List<PolicyHeaderClaimableItemDto> SyncItemAvailableForClaim(PolicyHeader policyHeaderEntity, DateTime dateOfLoss)
        {
            //Delete claimable items for date of loss
            DeleteExistingPolicyHeaderClaimableItem(policyHeaderEntity, dateOfLoss);

            //Fetch and sync items using CimsConnect
            Guid requestGuid = Guid.NewGuid();
            CimsPolicies policyIntegration = new CimsPolicies();
            FnolPolicyDataResponseDto cimsResponse = policyIntegration.GetDetailForPolicyGuid(policyHeaderEntity.ExternalPlatformGuid, dateOfLoss, requestGuid);

            //Log to Cims Integration
            LogCimsIntegration(requestGuid, policyHeaderEntity, dateOfLoss, cimsResponse);

            //Log new PolicyHeaderClaimableItem
            if (cimsResponse.Success)
                return LogPolicyHeaderClaimableItem(requestGuid, policyHeaderEntity, dateOfLoss, cimsResponse);

            return new List<PolicyHeaderClaimableItemDto>();
        }
        private List<PolicyHeaderClaimableItemDto> GetPolicyHeaderClaimableItems (PolicyHeader policyHeaderEntity, DateTime dateOfLoss)
        {
            var result = repository.GetAll<PolicyHeaderClaimableItem>().Where(x => x.PolicyHeader.Id == policyHeaderEntity.Id &&
                x.DateOfLoss == dateOfLoss && x.IsDeleted == false).ToList();

            return Mapper.Map<List<PolicyHeaderClaimableItemDto>>(result);
        }
        private void DeleteExistingPolicyHeaderClaimableItem(PolicyHeader policyHeaderEntity, DateTime dateOfLoss)
        {
            var result = repository.GetAll<PolicyHeaderClaimableItem>().Where(x => x.PolicyHeader.Id == policyHeaderEntity.Id &&
                x.DateOfLoss == dateOfLoss && x.IsDeleted == false);

            foreach(var entity in result.ToList())
            {
                entity.Delete();
                repository.Save<PolicyHeaderClaimableItem>(entity);
            }
        }
        private void LogCimsIntegration(Guid requestGuid, PolicyHeader policyHeaderEntity, DateTime dateOfLoss, FnolPolicyDataResponseDto cimsResponse)
        {
            JObject requestData = new JObject();
            requestData.Add("PlatformId", policyHeaderEntity.ExternalPlatformGuid);
            requestData.Add("DateOfLoss", dateOfLoss);

            //Cims Logging
            CreateCimsIntegrationLoggingDto integrationLog = new CreateCimsIntegrationLoggingDto
            {
                RequestId = requestGuid,
                RequestData = JsonConvert.SerializeObject(requestData),
                ResponseData = cimsResponse.RawResponse
            };
            var response = _executionPlan.Execute<CreateCimsIntegrationLoggingDto, int>(integrationLog);
        }
        private List<PolicyHeaderClaimableItemDto> LogPolicyHeaderClaimableItem(Guid requestGuid, PolicyHeader policyHeaderEntity, DateTime dateOfLoss, FnolPolicyDataResponseDto cimsResponse)
        {
            //Get Policy Items
            var policyItemsEntity = repository.GetAll<PolicyItem>().Where(x => x.PolicyHeader.Id == policyHeaderEntity.Id).ToList();

            List<PolicyHeaderClaimableItemDto> result = new List<PolicyHeaderClaimableItemDto>();
            foreach (var externalPolicy in cimsResponse.Data)
            {
                foreach (var externalInsurerProduct in externalPolicy.InsurerProduct)
                {
                    foreach (var externalPolicyInfo in externalInsurerProduct.policyInfo)
                    {
                        foreach (var externalPolicySectionInfo in externalPolicyInfo.sectionInfo)
                        {
                            foreach (var externalRiskItemInfo in externalPolicySectionInfo.riskItemInfo)
                            {
                                Cover sectionCover = new Covers().Any(x => x.Code.ToUpper() == externalPolicySectionInfo.CoverCode.ToUpper())
                                    ? new Covers().First(x => x.Code.ToUpper() == externalPolicySectionInfo.CoverCode.ToUpper())
                                    : Covers.Motor;

                                // Get Matching Policy Item from Cims    
                                var foundPolicyItem =
                                    policyItemsEntity.FirstOrDefault(
                                        a => a.AssetNumber.ToString() == externalRiskItemInfo.VendorItemCode && a.Cover.Id == sectionCover.Id);
                                //Save PolicyHeaderClaimableItem
                                SyncRiskItemInfo(result, policyHeaderEntity, externalInsurerProduct, externalPolicyInfo, externalPolicySectionInfo, externalRiskItemInfo, foundPolicyItem, dateOfLoss, requestGuid);
                            }
                        }
                    }
                }
            }
            return result;
        }
        private void SyncRiskItemInfo(List<PolicyHeaderClaimableItemDto> result, PolicyHeader policyHeaderEntity, InsurerProduct insurerProduct, PolicyInfo policyInfo, SectionInfo sectionInfo, RiskItemInfo riskItemInfo, PolicyItem policyItem, DateTime dateOfLoss, Guid requestGuid)
        {
            if (policyItem == null)
            {
                return;
            }

            //Risk Item
            var riskItem = new PolicyHeaderClaimableItem()
            {
                PolicyHeader = policyHeaderEntity,
                DateOfLoss = dateOfLoss,
                PolicyItem = policyItem,
                RequestId = requestGuid,
                Description = "",
                ExternalItemId = riskItemInfo.ItemGUID,
                ExternalItemMasterId = riskItemInfo.ItemMasterGUID,
                ExternalSubmitGuid = riskItemInfo.ItemGUID
            };

            repository.Save<PolicyHeaderClaimableItem>(riskItem);
            result.Add(Mapper.Map<PolicyHeaderClaimableItemDto>(riskItem));

            //Risk Item Extension
            foreach (var extension in riskItemInfo.riskItemExtensionInfo)
            {
                var riskItemExtension = new PolicyHeaderClaimableItem()
                {
                    PolicyHeader = policyHeaderEntity,
                    DateOfLoss = dateOfLoss,
                    PolicyItem = policyItem,
                    RequestId = requestGuid,
                    Description = extension.Extension,
                    ExternalItemId = extension.ItemGUID,
                    ExternalItemMasterId = extension.ItemMasterGUID,
                    ExternalSubmitGuid = extension.ItemGUID
                };

                repository.Save<PolicyHeaderClaimableItem>(riskItemExtension);
                result.Add(Mapper.Map<PolicyHeaderClaimableItemDto>(riskItemExtension));
            }

            foreach (var funeralItem in riskItemInfo.riskItemFuneralInfo)
            {
                var funeralItemEntity = new PolicyHeaderClaimableItem()
                {
                    PolicyHeader = policyHeaderEntity,
                    DateOfLoss = dateOfLoss,
                    PolicyItem = policyItem,
                    RequestId = requestGuid,
                    Description = funeralItem.PlanName + "(" + funeralItem.IndividualsOnCover + ")",
                    ExternalItemId = funeralItem.ItemGUID,
                    ExternalItemMasterId = funeralItem.ItemMasterGUID,
                    ExternalSubmitGuid = funeralItem.GridItemUniqueGUID
                };

                repository.Save<PolicyHeaderClaimableItem>(funeralItemEntity);
                result.Add(Mapper.Map<PolicyHeaderClaimableItemDto>(funeralItemEntity));
            }
        }
        #endregion

    }
}
