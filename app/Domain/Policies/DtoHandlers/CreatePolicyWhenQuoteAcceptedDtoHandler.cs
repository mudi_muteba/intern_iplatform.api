﻿using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using iPlatform.Api.DTOs.Policy;
using MasterData.Authorisation;
using Domain.Party.Quotes;
using Domain.Activities;
using MasterData;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using System.Linq;

namespace Domain.Policies.DtoHandlers
{
    public class CreatePolicyWhenQuoteAcceptedDtoHandler : CreationDtoHandler<PolicyHeader, CreatePolicyWhenQuoteAcceptedDto, int>
    {
        public CreatePolicyWhenQuoteAcceptedDtoHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(PolicyHeader entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override PolicyHeader HandleCreation(CreatePolicyWhenQuoteAcceptedDto dto, HandlerResult<int> result)
        {
            var quote = _repository.GetById<Quote>(dto.QuoteId);
            var leadActivity = GetLeadActivity(dto);
            return CreatePolicyFromQuote(quote, leadActivity, dto.Reference, dto.PolicyNo);
        }

        private PolicyHeader CreatePolicyFromQuote(Quote quote, LeadActivity activity, Guid reference, string policyNo)
        {
            
            var policyHeader = new PolicyHeader(quote.Product, quote.QuoteHeader.Proposal.LeadActivity.Lead.Party, quote,
               PaymentPlans.Monthly, PolicyStatuses.Potential, activity, reference);

            policyHeader.PolicyNo = policyNo;

            var policyItemFactory = new PolicyItemFactory(policyHeader);
            var proposalDefinitions = _repository.GetAll<ProposalDefinition>().Where(x => x.ProposalHeader.Id == quote.QuoteHeader.Proposal.Id);

            foreach (var item in quote.Items)
            {
                var asset = proposalDefinitions.Where(x => x.Asset.AssetNo == item.Asset.AssetNumber || x.Id == item.Asset.AssetNumber)
                        .Select(a => a.Asset)
                        .FirstOrDefault();

                if (asset == null)
                {
                    Log.ErrorFormat(string.Format("POLICY NOT CREATED: Asset not found from quote {0} on proposalHeader {1}, AssetNo {2}",
                            item.Id, quote.QuoteHeader.Proposal.Id, item.Asset.AssetNumber));
                    break;
                }
                policyHeader.AddPolicyItem(policyItemFactory.CreatePolicyItem(item, asset));
            }
            return policyHeader;
        }

        private LeadActivity GetLeadActivity(CreatePolicyWhenQuoteAcceptedDto dto)
        {
            LeadActivity leadActivity = null;

            if (dto.QuoteAcceptedLeadActivityId == 0)
                leadActivity = _repository.GetAll<QuoteAcceptedLeadActivity>().Where(x => x.Quote.Id == dto.QuoteId).FirstOrDefault();
            else
                leadActivity = _repository.GetById<QuoteAcceptedLeadActivity>(dto.QuoteAcceptedLeadActivityId);

            return leadActivity;
        }
    }
}
