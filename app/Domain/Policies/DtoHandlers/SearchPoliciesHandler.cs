﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Policy;
using iPlatform.PolicyEngine;
using iPlatform.PolicyEngine.Data;
using iPlatform.PolicyEngine.Data.Helper;
using iPlatform.PolicyEngine.Data.Models;
using iPlatform.PolicyEngine.Data.Requests;
using MasterData.Authorisation;

namespace Domain.Policies.DtoHandlers
{
    public class SearchPoliciesHandler :  BaseDtoHandler<PolicySearchDto, PolicyHeaderDto>
    {
        
        public SearchPoliciesHandler(IProvideContext contextProvider, IRepository repository, IWindsorContainer container)
            : base(contextProvider)
        {
            this.repository = repository;
            this.container = container;
        }
        private IWindsorContainer container;
        private IRepository repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(PolicySearchDto dto, HandlerResult<PolicyHeaderDto> result)
        {
            var organizationPolicyServiceSetting = repository.GetAll<OrganizationPolicyServiceSetting>().FirstOrDefault(a => a.Organization.Code == dto.insurerCode);

            if (organizationPolicyServiceSetting != null)
            {
                PolicyData policyData = null;
                PolicyHeaderDto returnPolicy = null;

                try
                {
                    var request = Mapper.Map<PolicyEngineRequest>(dto);
                    request.Setting = Mapper.Map<PolicyServiceSettingInfo>(organizationPolicyServiceSetting);
                    request.Container = container;

                    policyData = PolicyEngineFactory.GetPolicyData(request);

                    if (policyData.Errors.Count() > 0)
                        policyData.Errors.ForEach(e => AddMessage(e, result));
                    else
                    {
                        Insurer insurer = policyData.Insurers
                        .Where(p => p.Products
                        .Any(s => s.Policies
                        .Any(t => t.PolicyMasterGuid == dto.PolicyId))).FirstOrDefault();

                        returnPolicy = new PolicyHeaderDto()
                            .MapFromEntity(insurer, dto);
                        result.Processed(returnPolicy);
                    }
                }
                catch (Exception e)
                {
                    AddErrorMessage(e, result);
                }
            }
            else
            {
                var message = string.Format("Executing handler {0}, ERROR : No OrganizationPolicyServiceSetting obtained for insurer {1}",
                                this.GetType().ToString(), dto.insurerCode);
                AddMessage(message, result);
                result.Processed(new PolicyHeaderDto());
            }
        }

        #region Add Error Message
        private void AddErrorMessage(Exception e, HandlerResult<PolicyHeaderDto> result)
        {
            var message = string.Format("Executing handler {0} (Message: {1},InnerException: {2}, Stacktrace: {3})",
                                this.GetType().ToString(), e.Message, e.InnerException.ToString(), e.StackTrace.ToString());

            AddMessage(message, result); 
        }
        private void AddMessage(string error, HandlerResult<PolicyHeaderDto> result)
        {
            Log.ErrorFormat(error);
            result.AddFailure(error);
        }
        #endregion
    }
}