﻿using Domain.Users.Authentication;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace Domain.Base.Culture
{
    public static class CultureExtension
    {
        public static CultureParameter GetCultureParam(this DtoContext context)
        {
            var tokenReader = new TokenGenerator.TokenReader(context.Token);
            return tokenReader.Culture;
        }
    }
}
