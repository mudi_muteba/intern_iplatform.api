using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Handlers
{
    public abstract class ExistingEntityDtoHandler<TEntity, TDto, TId> : BaseDtoHandler<TDto, TId>
        where TEntity: Entity
        where TDto: IAffectExistingEntity
    {
        protected readonly IRepository repository;

        protected ExistingEntityDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            this.repository = repository;
        }

        protected override void InternalHandle(TDto dto, HandlerResult<TId> result)
        {
            var entity = GetEntityById(dto);
            if (entity == null)
                throw new MissingEntityException(typeof (TEntity), dto.Id);

            HandleExistingEntityChange(entity, dto, result);

            repository.Save(entity);
        }

        protected virtual TEntity GetEntityById(TDto dto)
        {
            return repository.GetById<TEntity>(dto.Id);
        }

        protected abstract void HandleExistingEntityChange(TEntity entity, TDto dto, HandlerResult<TId> result);
    }
}