using Domain.Base.Execution;
using Domain.Base.Repository;

namespace Domain.Base.Handlers
{
    public abstract class CreationDtoHandler<TEntity, TDto, TId> : BaseDtoHandler<TDto, TId>
        where TEntity: Entity
    {
        private readonly IRepository repository;

        protected CreationDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            this.repository = repository;
        }

        protected override void InternalHandle(TDto dto, HandlerResult<TId> result)
        {
            var entity = HandleCreation(dto, result);

            repository.Save(entity);

            EntitySaved(entity, result);
        }

        protected abstract void EntitySaved(TEntity entity, HandlerResult<TId> result);

        protected abstract TEntity HandleCreation(TDto dto, HandlerResult<TId> result);
    }
}