using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Handlers
{
    public abstract class ExistingEntityDtoCustomHandler<TEntity, TDto, TId> : BaseDtoHandler<TDto, TId>
        where TEntity: Entity
        where TDto: IAffectExistingEntity
    {
        protected readonly IRepository repository;

        protected ExistingEntityDtoCustomHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            this.repository = repository;
        }

        protected override void InternalHandle(TDto dto, HandlerResult<TId> result)
        {
            var entity = GetEntity(dto);
            if (entity == null)
                throw new MissingEntityException(typeof (TEntity), dto.Id);

            HandleExistingEntityChange(entity, dto, result);

            repository.Save(entity);
        }

        protected abstract TEntity GetEntity(TDto dto);

        protected abstract void HandleExistingEntityChange(TEntity entity, TDto dto, HandlerResult<TId> result);
    }
}