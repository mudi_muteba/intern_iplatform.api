using System.Collections.Generic;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using iPlatform.Api.DTOs.Base;
using MasterData.Authorisation;

namespace Domain.Base.Handlers
{
    public abstract class BaseDtoHandler<TDto, TId> : IHandleDto<TDto, TId>
    {
        protected List<int> Channels = new List<int>();
        protected ExecutionContext ExecutionContext;
        protected static readonly ILog Log = LogManager.GetLogger<BaseDtoHandler<TDto, TId>>();

        protected BaseDtoHandler(IProvideContext contextProvider)
        {
            ExecutionContext = contextProvider.Get();
        }

        public void Handle(TDto dto, HandlerResult<TId> result)
        {
            GetApplicableChannelIds(dto);

            Allowed();

            InternalHandle(dto, result);
        }

        public abstract List<RequiredAuthorisationPoint> RequiredRights { get; }

        protected void Allowed()
        {
            var allowed = ExecutionContext.Authorised(ExecutionContext.ActiveChannelId, RequiredRights);
            if (allowed)
                return;

            throw new UnauthorisationException(RequiredRights);
        }

        private void GetApplicableChannelIds(TDto dto)
        {
            if (dto is IChannelAwareDto)
                Channels.Add((dto as IChannelAwareDto).ChannelId);

            if (dto is IMultiChannelDto)
                foreach (var channel in (dto as IMultiChannelDto).Channels)
                    Channels.Add(channel);
        }

        protected abstract void InternalHandle(TDto dto, HandlerResult<TId> result);
    }
}