﻿using System.Collections.Generic;
using Domain.Base.Execution;
using MasterData.Authorisation;

namespace Domain.Base.Handlers
{
    public interface IHandleDto<in TDto, TId>
    {
        void Handle(TDto dto, HandlerResult<TId> result);
        List<RequiredAuthorisationPoint> RequiredRights { get; }
    }
}