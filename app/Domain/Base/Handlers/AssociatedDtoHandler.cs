using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;

namespace Domain.Base.Handlers
{
    public abstract class AssociatedDtoHandler<TParent, TDto, TId, TAssociation> : BaseDtoHandler<TDto, TId>
        where TParent : Entity
        where TAssociation: Entity
    {
        private readonly IRepository repository;

        protected AssociatedDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            this.repository = repository;
        }

        protected override void InternalHandle(TDto dto, HandlerResult<TId> result)
        {
            var parentEntity = repository.GetById<TParent>(GetParentId(dto));

            if (parentEntity == null)
            {
                throw new MissingEntityException(typeof (TParent), GetParentId(dto));
            }

            var associatedEntity = Associate(parentEntity, dto, result);
            if (associatedEntity == null || result.AllErrorDTOMessages.Any()) return;
            repository.Save(associatedEntity);
            Processed(associatedEntity, result);
        }

        protected abstract void Processed(TAssociation associatedEntity, HandlerResult<TId> result);

        protected abstract TAssociation Associate(TParent parentEntity, TDto dto, HandlerResult<TId> result);

        protected abstract int GetParentId(TDto dto);
    }
}