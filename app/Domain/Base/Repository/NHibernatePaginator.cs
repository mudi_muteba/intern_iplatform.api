﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using NHibernate.Linq;

namespace Domain.Base.Repository
{
    public class NHibernatePaginator : IPaginateResults
    {
        public PagedResults<T> Paginate<T>(IQueryable<T> query, int pageNumber, int pageSize)
        {
            if (!query.Any())
                return new PagedResults<T>(new List<T>(), pageSize, 1, 0);

            var futureResults = query
                .Skip((pageNumber - 1)*pageSize)
                .Take(pageSize)
                .ToFuture();

            var futureCount = query.ToFutureCount(x => x.Count());

            var results = new PagedResults<T>(futureResults, pageSize, pageNumber, futureCount.Value);

            return results;
        }

        public PagedResults<T> Paginate<T>(IQueryable<T> query, Pagination pagination)
        {
            return Paginate(query, pagination.PageNumber, pagination.PageSize);
        }
    }
}