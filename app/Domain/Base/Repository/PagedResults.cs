﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Base.Repository
{
    public class PagedResults<T>
    {
        public List<T> Results = new List<T>();
        public int PageNumber;
        public int TotalCount;
        public int TotalPages;

        public PagedResults()
        {
            PageNumber = 0;
            TotalCount = 0;
            TotalPages = 0;
        }

        public PagedResults(IEnumerable<T> data, int pageSize, int pageNumber, int numberOfRecords = 1000)
        {
            PageNumber = pageNumber < 0 ? 1 : pageNumber;
            pageSize = pageSize < 0 ? 20 : pageSize;

            TotalCount = numberOfRecords;
            TotalPages = TotalCount/pageSize;
            if (TotalCount%pageSize > 0)
                TotalPages++;

            Results.AddRange(data.ToList());
        }
    }
}