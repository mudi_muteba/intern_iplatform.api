using System;
using System.Linq;
using System.Linq.Expressions;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base;
using NHibernate;
using NHibernate.Linq;
using System.Collections.Generic;
using Domain.Base.Queries;

namespace Domain.Base.Repository
{
    public static class NHibernateExtentions
    {
        public static TEntity Get<TEntity>(this ISession session, int id, ExecutionContext context)
            where TEntity : Entity
        {
            if (!typeof (ChannelEntity).IsAssignableFrom(typeof (TEntity)))
                return session.Get<TEntity>(id);

            return session
                .Query<TEntity>()
                .FirstOrDefault(e => e.Id == id && context.Channels.Contains((e as ChannelEntity).Channel.Id));
        }

        public static TEntity GetByParty<TEntity>(this ISession session, int id, int partyId, ExecutionContext context)
            where TEntity : EntityWithParty
        {
            if (!typeof (ChannelEntity).IsAssignableFrom(typeof (TEntity)))
                return session.Get<TEntity>(id);

            return session
                .Query<TEntity>()
                .FirstOrDefault(e => e.Id == id && e.Party.Id == partyId &&
                        context.Channels.Contains((e as ChannelEntity).Channel.Id));
        }


        public static TEntity GetByProposal<TEntity>(this ISession session, int proposalId, ExecutionContext context)
            where TEntity : EntityWithProposal
        {
            if (!typeof (ChannelEntity).IsAssignableFrom(typeof (TEntity)))
                return session.Query<TEntity>()
                    .FirstOrDefault(e => e.Proposal.Id == proposalId && e.IsDeleted == false);

            return session
                .Query<TEntity>()
                .FirstOrDefault(e => e.Proposal.Id == proposalId && e.IsDeleted == false); 
        }

        public static IEnumerable<TEntity> GetByParty<TEntity>(this ISession session, int partyId, ExecutionContext context)
           where TEntity : Entity, IEntityIncludeParty
        {
            if (!typeof (Entity).IsAssignableFrom(typeof (TEntity)))
                return null;

                return session
                    .Query<TEntity>().Where(e => e.Party.Id == partyId).ToList();
        }
        public static TEntity GetByUserId<TEntity>(this ISession session, int id, int userId, ExecutionContext context)
    where TEntity : EntityWithUser
        {
            if (!typeof(ChannelEntity).IsAssignableFrom(typeof(TEntity)))
                return session.Get<TEntity>(id);

            return session
                .Query<TEntity>()
                .FirstOrDefault(e => e.Id == id && e.User.Id == userId &&
                        context.Channels.Contains((e as ChannelEntity).Channel.Id));
        }

        public static TEntity GetByQuoteId<TEntity>(this ISession session, int id, int quoteId, ExecutionContext context) where TEntity : EntityWithQuote
        {
            if (!typeof(ChannelEntity).IsAssignableFrom(typeof(TEntity)))
                return session.Get<TEntity>(id);

            return session
              .Query<TEntity>()
              .FirstOrDefault(e => e.Id == id && e.Quote.Id == quoteId &&
                      context.Channels.Contains((e as ChannelEntity).Channel.Id));
        }

        public static IQueryable<TEntity> ChannelFilter<TEntity>(this IQueryable<TEntity> query,
            ExecutionContext context) where TEntity : class
        {
            if (!typeof (ChannelEntity).IsAssignableFrom(typeof (TEntity)) || context.IsChannelFreeQuery)
                return query;

            return query.Where(q => context.Channels.Contains((q as ChannelEntity).Channel.Id));
        }

        public static IFutureValue<TResult> ToFutureCount<TSource, TResult>(
            this IQueryable<TSource> source,
            Expression<Func<IQueryable<TSource>, TResult>> selector)
            where TResult : struct
        {
            var provider = (INhQueryProvider) source.Provider;
            var method = ((MethodCallExpression) selector.Body).Method;
            var expression = Expression.Call(null, method, source.Expression);
            return (IFutureValue<TResult>) provider.ExecuteFuture(expression);
        }

        public static PagedResults<TResult> Paginate<TResult>(this IQueryable<TResult> query,
            IPaginateResults paginator,
            Pagination pagination)
        {
            return paginator.Paginate(query, pagination.PageNumber, pagination.PageSize);
        }
    }
}