﻿using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Repository.StoredProcedures;

using System.Collections.Generic;

namespace Domain.Base.Repository
{
    public interface IRepository
    {
        void Save<TEntity>(TEntity entity) where TEntity : Entity;
        TEntity GetById<TEntity>(int id) where TEntity : Entity;
        TEntity GetByPartyId<TEntity>(int id, int individualId) where TEntity : EntityWithParty;
        TEntity GetByProposalId<TEntity>(int id) where TEntity : EntityWithProposal;
        IEnumerable<TEntity> GetByPartyId<TEntity>(int partyId) where TEntity : Entity, IEntityIncludeParty;
        TEntity GetByUserId<TEntity>(int id, int individualId) where TEntity : EntityWithUser;
        TEntity GetByQuoteId<TEntity>(int id, int quoteId) where TEntity : EntityWithQuote;
        TEntity GetByIdWithChannel<TEntity>(int id) where TEntity : ChannelEntity, new();
        IQueryable<TEntity> GetAll<TEntity>(ExecutionContext context = null) where TEntity : class;
        void SetFetchMode<TEntity>(string entity) where TEntity : Entity;
        TEntity UnProxy<TEntity>(object entity);

        IList<object[]> QueryStoredProcedure<TProcedure>(TProcedure procedure)
            where TProcedure : StoredProcedure;

        void PublishEvents<TEntity>(TEntity entity) where TEntity : Entity;
    }
}