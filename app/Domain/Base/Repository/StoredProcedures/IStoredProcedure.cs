﻿namespace Domain.Base.Repository.StoredProcedures
{
    public interface IStoredProcedure
    {
        string Name { get; set; }

        object Parameters { get; set; }

        void SetParameters<T>(T value);

        void GetParameters<T>(T value);
    }
}