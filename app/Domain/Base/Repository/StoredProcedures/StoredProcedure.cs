﻿namespace Domain.Base.Repository.StoredProcedures
{
    public abstract class StoredProcedure : IStoredProcedure
    {
        public StoredProcedure()
        {
                
        }

        public virtual string Name { get; set; }

        public virtual object Parameters { get; set; }

        public virtual void SetParameters<T>(T value)
        {
            Parameters = value;
        }

        public virtual void GetParameters<T>(T value)
        {
            Parameters = value;
        }
    }
}