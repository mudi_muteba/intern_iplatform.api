﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository.StoredProcedures;
using NHibernate;
using NHibernate.Linq;

namespace Domain.Base.Repository
{
    public class NHibernateRepository : IRepository
    {
        private readonly IProvideContext _contextProvider;
        private readonly IPublishEvents _publisher;
        private readonly ISession _session;
        private ExecutionContext _context;

        public NHibernateRepository(ISession session, IProvideContext contextProvider, IPublishEvents publisher)
        {
            _session = session;
            _contextProvider = contextProvider;
            _publisher = publisher;
        }

        private ExecutionContext Context
        {
            get { return _context ?? (_context = _contextProvider.Get()); }
        }

        public void Save<TEntity>(TEntity entity) where TEntity : Entity
        {
            _session.Save(entity);
            PublishEvents(entity);
        }

        public TEntity GetById<TEntity>(int id) where TEntity : Entity
        {
            return _session.Get<TEntity>(id, Context);
        }

        public IList<object[]> QueryStoredProcedure<TProcedure>(TProcedure procedure) where TProcedure : StoredProcedure
        {
            return procedure.Parameters == null 
                ? _session.GetNamedQuery(procedure.Name).List<object[]>() 
                : _session.GetNamedQuery(procedure.Name).SetProperties(procedure.Parameters).List<object[]>();
        }

        public TEntity GetByPartyId<TEntity>(int id, int partyId) where TEntity : EntityWithParty
        {
            return _session.GetByParty<TEntity>(id, partyId, Context);
        }

        public TEntity GetByProposalId<TEntity>(int proposalId) where TEntity : EntityWithProposal
        {
            return _session.GetByProposal<TEntity>(proposalId, Context);
        }

        public IEnumerable<TEntity> GetByPartyId<TEntity>(int partyId) where TEntity : Entity, IEntityIncludeParty
        {
            return _session.GetByParty<TEntity>(partyId, Context);
        }

        public TEntity GetByUserId<TEntity>(int id, int partyId) where TEntity : EntityWithUser
        {
            return _session.GetByUserId<TEntity>(id, partyId, Context);
        }

        public TEntity GetByQuoteId<TEntity>(int id, int quoteId) where TEntity : EntityWithQuote
        {
            return _session.GetByQuoteId<TEntity>(id, quoteId, Context);
        }

        public TEntity GetByIdWithChannel<TEntity>(int id) where TEntity : ChannelEntity, new()
        {
            return _session.Get<TEntity>(id);
        }

        public IQueryable<TEntity> GetAll<TEntity>(ExecutionContext overridingContext = null) where TEntity : class
        {
            overridingContext = overridingContext ?? Context;

            return _session.Query<TEntity>().ChannelFilter(overridingContext);
        }

        public void PublishEvents<TEntity>(TEntity entity) where TEntity : Entity
        {
            var auditableEntity = entity as EntityWithAudit;
            if (auditableEntity == null)
                return;

            var eventsToPublish = auditableEntity.NewEvents().ToList();
            _publisher.Publish(eventsToPublish);
            auditableEntity.EventsPublished();
        }

        public void SetFetchMode<TEntity>(string entity) where TEntity : Entity
        {
            _session.CreateCriteria(typeof (TEntity)).SetFetchMode(entity, FetchMode.Eager);
        }

        public TEntity UnProxy<TEntity>(object entity)
        {
            return (TEntity)_session.GetSessionImplementation().PersistenceContext.Unproxy(entity);
        }
    }
}