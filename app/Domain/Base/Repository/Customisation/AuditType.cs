﻿using System;
using System.Data;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace Domain.Base.Repository.Customisation
{
    [Serializable]
    public class AuditType : IUserType
    {
        public new bool Equals(object x, object y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return AuditJsonUtil.ConvertToJson(x) == AuditJsonUtil.ConvertToJson(y);
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

        public object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            var json = NHibernateUtil.StringClob.NullSafeGet(rs, names[0]) ?? "{}";
            return AuditJsonUtil.FromJson(json.ToString());
        }

        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            if (value == null)
            {
                NHibernateUtil.StringClob.NullSafeSet(cmd, new Audit(), index);
                return;
            }

            var auditToSave = AuditJsonUtil.ConvertToJson(value);

            NHibernateUtil.StringClob.NullSafeSet(cmd, auditToSave, index);
        }

        public object DeepCopy(object value)
        {
            if (value == null)
                return null;

            if (!(value is Audit))
                throw new Exception("Invalid type");

            var oldAudit = value as Audit;
            var newAudit = new Audit(oldAudit);

            return newAudit;
        }

        public object Replace(object original, object target, object owner)
        {
            return DeepCopy(original);
        }

        public object Assemble(object cached, object owner)
        {
            return DeepCopy(cached);
        }

        public object Disassemble(object value)
        {
            return DeepCopy(value);
        }

        public SqlType[] SqlTypes
        {
            get
            {
                return new[]
                {
                    SqlTypeFactory.GetStringClob(int.MaxValue)
                };
            }
        }

        public Type ReturnedType
        {
            get { return typeof (Audit); }
        }

        public bool IsMutable
        {
            get { return true; }
        }
    }
}