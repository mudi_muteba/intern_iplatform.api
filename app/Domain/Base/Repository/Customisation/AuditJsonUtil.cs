﻿using Newtonsoft.Json;
using Domain.Admin;

namespace Domain.Base.Repository.Customisation
{
    public static class AuditJsonUtil
    {
        public static string ConvertToJson(object item)
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore,
            };
            

            return JsonConvert.SerializeObject(item, settings);
        }
        public static Audit FromJson(string json)
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                MaxDepth = 500,
                ContractResolver = new AuditContractResolver()
            };


            var audit = JsonConvert.DeserializeObject<Audit>(json, settings);
            return audit;
        }
    }
}