﻿using System.Linq;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Repository
{
    public interface IPaginateResults
    {
        PagedResults<T> Paginate<T>(IQueryable<T> query, int pageNumber, int pageSize);
        PagedResults<T> Paginate<T>(IQueryable<T> query, Pagination pagination);
    }
}