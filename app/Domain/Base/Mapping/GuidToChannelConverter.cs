﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using System;
using System.Linq;

namespace Domain.Base.Mapping
{
    public class GuidToChannelConverter : TypeConverter<Guid, Channel>
    {
        private readonly IRepository _repository;

        public GuidToChannelConverter(IRepository repository)
        {
            _repository = repository;
        }

        protected override Channel ConvertCore(Guid source)
        {
            return _repository.GetAll<Channel>().FirstOrDefault(x => x.SystemId == source);
        }
    }
}