﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Mapping
{
    public class EntityMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<EntityWithAudit, IAuditableDto>()
                .ForMember(t => t.Events, o => o.MapFrom(e => e.Audit.Events))
                ;

            Mapper.CreateMap<Audit, List<AuditEventDto>>()
                .ConvertUsing<ITypeConverter<Audit, List<AuditEventDto>>>();

            Mapper.CreateMap<IDomainEvent, AuditEventDto>()
                .ForMember(t => t.Date, o => o.MapFrom(s => s.Audit.Date))
                .ForMember(t => t.UserName, o => o.MapFrom(s => s.Audit.User.UserName))
                .ForMember(t => t.Version, o => o.MapFrom(s => s.Audit.Versioning.Version == null ? 1 : s.Audit.Versioning.Version.Value))
                .ForMember(t => t.EventName, o => o.MapFrom(s => s.GetType().Name))
                ;


            Mapper.CreateMap<EntityAudit, AuditDto>()
                .ForMember(t => t.CreatedBy, o => o.MapFrom(s => s.Users.CreatedBy))
                .ForMember(t => t.ModifiedBy, o => o.MapFrom(s => s.Users.ModifiedBy))
                .ForMember(t => t.CreatedOn, o => o.MapFrom(s => s.Timestamps.CreatedOn))
                .ForMember(t => t.ModifiedOn, o => o.MapFrom(s => s.Timestamps.ModifiedOn))
                ;
        }
    }
}