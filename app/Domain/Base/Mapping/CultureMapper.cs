﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Mapping
{
    public class CultureMapper: Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DateTime, DateTimeDto>()
                .ForMember(t => t.Value, o => o.MapFrom(s => s))
                ;

        }

    }
}
