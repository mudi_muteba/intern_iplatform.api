﻿using AutoMapper;
using Domain.Base.Repository;

namespace Domain.Base.Mapping
{
    public class IdToEntityConverter<T> : TypeConverter<int, T> where T : Entity
    {
        private readonly IRepository _repository;

        public IdToEntityConverter(IRepository repository)
        {
            _repository = repository;
        }

        protected override T ConvertCore(int source)
        {
            return _repository.GetById<T>(source);
        }
    }
}