﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Mapping
{
    public class AuditableDtoMapper : ITypeConverter<Audit, List<AuditEventDto>>
    {
        public List<AuditEventDto> Convert(ResolutionContext context)
        {
            var audit = context.SourceValue as Audit;
            var list = new List<AuditEventDto>();

            foreach (var @event in audit.Events)
            {
                var eventDto = Mapper.Map<AuditEventDto>(@event);

                list.Add(eventDto);
            }

            return list;
        }
    }
}