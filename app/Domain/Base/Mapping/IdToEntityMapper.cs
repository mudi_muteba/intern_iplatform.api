﻿using System.Linq;
using AutoMapper;

namespace Domain.Base.Mapping
{
    public class IdToEntityMapper : Profile
    {
        protected override void Configure()
        {
            var entityTypes = typeof(Entity).Assembly.GetTypes().Where(type => type.IsSubclassOf(typeof(Entity)));
            foreach (var entityType in entityTypes)
            {
                var type1 = typeof(ITypeConverter<,>);
                var implementationType = type1.MakeGenericType(typeof(int), entityType);
                Mapper.CreateMap(typeof(int), entityType).ConvertUsing(implementationType);
            }
        }
    }
}