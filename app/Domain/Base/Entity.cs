using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.ChannelTags;
using Infrastructure.NHibernate.Attributes;

namespace Domain.Base
{
    public abstract class Entity
    {
        public virtual int Id { get; protected internal set; }
        public virtual EntityAudit SimpleAudit { get; protected internal set; }
        public virtual bool IsDeleted { get; protected internal set; }
        private ISet<ChannelTag> _channelTags = new HashSet<ChannelTag>();
        public virtual ISet<ChannelTag> ChannelTags
        {
            get { return _channelTags; }
            set { _channelTags = value; }
        }

        [DoNotMap]
        public virtual IEnumerable<Channel> Channels
        {
            get
            {
                return ChannelTags != null
                    ? ChannelTags.Where(x => x.IsDeleted != true).Select(x => x.Channel).ToList()
                    : Enumerable.Empty<Channel>().ToList();
            }
        }

        protected Entity()
        {
            SimpleAudit = new EntityAudit();
        }

        /// <summary>
        /// Un-proxy
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual T As<T>() where T : class
        {
            return this as T;
        }

        public virtual void Delete()
        {
            IsDeleted = true;
            SimpleAudit.Timestamps.Updated();
        } 

        /// <summary>
        /// Use this method to associate this entity with one or more channels
        /// </summary>
        public virtual void TagWithChannel(params Channel[] channels)
        {
            foreach (var channelTag in ChannelTags)
                channelTag.Delete();

            foreach (var channel in channels)
            {
                var channelTag = ChannelTags.FirstOrDefault(x => x.Channel.Id == channel.Id);
                if (channelTag == null)
                    ChannelTags.Add(new ChannelTag(channel));
                else
                    channelTag.IsDeleted = false;
            }
        }
    }
}