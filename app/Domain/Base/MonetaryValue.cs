﻿namespace Domain.Base
{
    public class MonetaryValue
    {
        protected MonetaryValue()
        {
        }

        public MonetaryValue(decimal value, string currencyCode)
        {
            Value = value;
            CurrencyCode = currencyCode;
        }

        public decimal Value { get; protected internal set; }
        public string CurrencyCode { get; protected internal set; }

        public static MonetaryValue Zero(string currencyCode)
        {
            return new MonetaryValue(0.00m, currencyCode);
        }
    }
}