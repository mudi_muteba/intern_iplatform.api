using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Queries
{
    internal class OrderByBuilder<TEntity> where TEntity : Entity
    {
        private readonly IEnumerable<KeyValuePair<OrderByField, Func<string>>> orderByDefinition;

        public OrderByBuilder(Dictionary<OrderByField, Func<string>> orderByDefinition)
        {
            this.orderByDefinition = orderByDefinition.Where(o => o.Key != null);
        }

        public IQueryable<TEntity> Build(IQueryable<TEntity> query, List<OrderByField> orderBy)
        {
            if (orderBy == null)
                return query;

            foreach (var orderByField in orderBy)
            {
                var match =
                    orderByDefinition.FirstOrDefault(
                        o => o.Key.FieldName.Equals(orderByField.FieldName, StringComparison.InvariantCultureIgnoreCase));

                if (match.Key == null)
                    continue;

                var orderByString = string.Format("{0} {1}",
                    match.Value(),
                    orderByField.Direction == OrderByDirection.Ascending ? "ASC" : "DESC");


                query = query.OrderBy(orderByString);
            }

            return query;
        }
    }
}