using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using MasterData.Authorisation;

namespace Domain.Base.Queries
{
    public abstract class BaseExternalSystemQuery<TExternalResponse> : IExternalSystemQuery
    {
        protected readonly IProvideContext ContextProvider;

        protected BaseExternalSystemQuery(IProvideContext contextProvider)
        {
            this.ContextProvider = contextProvider;
        }

        public abstract List<RequiredAuthorisationPoint> RequiredRights { get; }

        public TExternalResponse ExecuteQuery()
        {
            var executionContext = (this is IContextFreeQuery) ? null : Allowed();

            return Execute();
        }

        protected abstract TExternalResponse Execute();

        protected ExecutionContext Allowed()
        {
            var context = ContextProvider.Get();

            var allowed = context.Authorised(context.Channels, RequiredRights);
            if (!allowed.Satisfied)
                throw new UnauthorisationException(RequiredRights);

            return new ExecutionContext(context.Token, context.UserId, context.Username, allowed.Channels, context.ActiveChannelId, context.Authorisation);
        }

    }
}