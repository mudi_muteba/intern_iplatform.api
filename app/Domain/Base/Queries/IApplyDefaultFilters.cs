﻿using System.Linq;
using Domain.Base.Execution;

namespace Domain.Base.Queries
{
    public interface IApplyDefaultFilters<TState>
    {
        IQueryable<TState> Apply(ExecutionContext executionContext, IQueryable<TState> query);
    }
}