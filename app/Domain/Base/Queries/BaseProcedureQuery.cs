﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Base.Queries
{
    public abstract class BaseProcedureQuery<TViewResponse> : IQueryProcedure
    {
        protected readonly IProvideContext ContextProvider;
        protected readonly IRepository Repository;
        protected readonly IExecutionPlan ExecutionPlan;

        protected BaseProcedureQuery(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
        {
            ContextProvider = contextProvider;
            Repository = repository;
            ExecutionPlan = executionPlan;
        }

        public abstract List<RequiredAuthorisationPoint> RequiredRights { get; }

        public TViewResponse ExecuteQuery()
        {
            var executionContext = (this is IContextFreeQuery) ? null : Allowed();

            return Execute();
        }

        protected abstract TViewResponse Execute();

        protected ExecutionContext Allowed()
        {
            var context = ContextProvider.Get();

            var allowed = context.Authorised(context.Channels, RequiredRights);
            if (!allowed.Satisfied)
                throw new UnauthorisationException(RequiredRights);

            return new ExecutionContext(context.Token, context.UserId, context.Username, allowed.Channels, context.ActiveChannelId, context.Authorisation);
        }
    }
}