﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;
using Domain.Base.Repository.StoredProcedures;
using MasterData.Authorisation;

namespace Domain.Base.Queries
{
    public abstract class BaseStoredProcedureQueryObject<TObject, TProcedure> : IStoredProcedureQuery
        where TObject : class 
        where TProcedure : StoredProcedure
    {
        private readonly IProvideContext _contextProvider;
        private readonly IRepository _repository;

        internal BaseStoredProcedureQueryObject() { }

        protected BaseStoredProcedureQueryObject(IProvideContext contextProvider, IRepository repository, IApplyDefaultFilters<TObject> defaults)
        {
            _repository = repository;
            _contextProvider = contextProvider;
        }

        public abstract List<RequiredAuthorisationPoint> RequiredRights { get; }

        public IList<TEntity> ExecuteQuery<TEntity, TProcedure>(TProcedure procedure)
            where TProcedure : StoredProcedure
        {
            return _repository.QueryStoredProcedure(procedure).Select(x => (TEntity)Activator.CreateInstance(typeof(TEntity), new object[] { x })).ToList();
        }

        protected ExecutionContext Allowed()
        {
            var context = _contextProvider.Get();
            var allowed = context.Authorised(context.Channels, RequiredRights);
            if (!allowed.Satisfied)
                throw new UnauthorisationException(RequiredRights);

            return new ExecutionContext(context.Token, context.UserId, context.Username, allowed.Channels, context.ActiveChannelId, context.Authorisation);
        }

        protected internal abstract IList<TObject> Execute(IList<TObject> storedProcedure);
    }
}