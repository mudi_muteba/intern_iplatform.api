using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Queries
{
    public interface IOrderableQuery
    {
        Dictionary<OrderByField, Func<string>> OrderByDefinition { get; }
    }
}