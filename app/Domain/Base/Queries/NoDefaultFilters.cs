﻿using System.Linq;
using Domain.Base.Execution;

namespace Domain.Base.Queries
{
    public class NoDefaultFilters<TEntity> : IApplyDefaultFilters<TEntity> where TEntity : Entity
    {
        public IQueryable<TEntity> Apply(ExecutionContext executionContext, IQueryable<TEntity> query)
        {
            return query.Where(x => x.IsDeleted == false);
        }
    }
}