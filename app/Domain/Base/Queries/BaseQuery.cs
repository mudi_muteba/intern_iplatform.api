using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Base.Queries
{
    public abstract class BaseQuery<TEntity> : IQuery where TEntity : Entity
    {
        protected readonly IProvideContext ContextProvider;
        private readonly IApplyDefaultFilters<TEntity> _defaults;
        protected readonly IRepository Repository;

        // Testing purposes
        internal BaseQuery() { }

        protected BaseQuery(IProvideContext contextProvider, IRepository repository,
            IApplyDefaultFilters<TEntity> defaults)
        {
            Repository = repository;
            _defaults = defaults;
            ContextProvider = contextProvider;
        }

        public abstract List<RequiredAuthorisationPoint> RequiredRights { get; }

        public IQueryable<TEntity> ExecuteQuery()
        {
            var executionContext = (this is IContextFreeQuery) ? null : Allowed();

            // this is done to bypass channel filtering on entity implementing ChannelEntity
            // how it works: implement IChannelFreeQuery in query 
            if (this is IChannelFreeQuery)
            {
                SetChannelFreeContext(executionContext);
            }
                
            var filtered = Repository.GetAll<TEntity>(executionContext);

            return _defaults.Apply(executionContext, Execute(filtered));
        }

        protected ExecutionContext Allowed()
        {
            var context = ContextProvider.Get();

            var allowed = context.Authorised(context.Channels, RequiredRights);
            if (!allowed.Satisfied)
                throw new UnauthorisationException(RequiredRights);

            return new ExecutionContext(context.Token, context.UserId, context.Username, allowed.Channels, context.ActiveChannelId, context.Authorisation);
        }

        protected internal abstract IQueryable<TEntity> Execute(IQueryable<TEntity> query);

        private void SetChannelFreeContext(ExecutionContext executionContext)
        {
            if (executionContext == null)
            {
                ExecutionContext context = ContextProvider.Get();
                context.SetChannelFree(true);
                ContextProvider.SetContext(context);
            }
            else
            {
                executionContext.SetChannelFree(true);
            }  
        }
    }
}