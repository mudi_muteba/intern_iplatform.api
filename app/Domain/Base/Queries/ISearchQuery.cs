namespace Domain.Base.Queries
{
    public interface ISearchQuery<in TCriteria>
    {
        void WithCriteria(TCriteria criteria);
    }
}