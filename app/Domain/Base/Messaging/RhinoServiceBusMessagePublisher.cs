﻿using System;
using Common.Logging;
using Messages;
using Rhino.ServiceBus;

namespace Domain.Base.Messaging
{
    public class RhinoServiceBusMessagePublisher : ISendMessages
    {
        private readonly IOnewayBus serviceBus;
        private static readonly ILog log = LogManager.GetLogger<RhinoServiceBusMessagePublisher>();

        public RhinoServiceBusMessagePublisher(IOnewayBus serviceBus)
        {
            this.serviceBus = serviceBus;
        }

        public void Send(IMessage message)
        {
            if (message == null)
                return;

            if (message.GetType() == typeof (NullPublishMessage))
                return;

            PrintDebug(message);
            serviceBus.Send(message);
        }

        private void PrintDebug(IMessage message)
        {
            var debugMessage = string.Format("Send message {0}{1}", message.GetType(), Environment.NewLine);
            debugMessage += string.Format("Message: {0}", message);

            log.InfoFormat(debugMessage);
        }
    }
}