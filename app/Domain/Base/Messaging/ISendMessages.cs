﻿using Messages;

namespace Domain.Base.Messaging
{
    public interface ISendMessages
    {
        void Send(IMessage message);
    }
}