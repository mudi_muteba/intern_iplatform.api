using Domain.Party.Correspondence;
using Domain.Party.ProposalHeaders;
using Domain.Party.Quotes;
using Domain.Users;

namespace Domain.Base
{
    public abstract class EntityWithParty : EntityWithAudit
    {
        public virtual Party.Party Party { get; protected internal set; }
    }

    public abstract class EntityWithProposal : EntityWithAudit
    {
        public virtual ProposalHeader Proposal { get; protected internal set; }
    }

    public abstract class EntityWithUser : EntityWithAudit
    {
        public virtual User User { get; protected internal set; }

    }

    public abstract class EntityWithPartyCorrespondence : EntityWithAudit
    {
        public virtual PartyCorrespondence PartyCorrespondence { get; protected internal set; }

    }

    public abstract class EntityWithQuote : EntityWithAudit
    {
        public virtual Quote Quote { get; protected internal set; }
    }


    public interface IEntityIncludeParty
    {
        Party.Party Party { get; set; }
    }
}