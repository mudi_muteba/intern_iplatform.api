﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace Domain.Base.Extentions
{
    public static class QuestionsExtentions
    {
        public static decimal GetSumInsured(this IEnumerable<RatingRequestItemAnswerDto> answers)
        {
            var sumInsured = 0m;

            if (answers == null)
                return sumInsured;

            var answer = answers.FirstOrDefault(x => x.Question.Id == Questions.SumInsured.Id);

            if (answer == null)
                return sumInsured;

            return decimal.TryParse(answer.QuestionAnswer as string, out sumInsured) ? sumInsured : sumInsured;
        }
    }
}