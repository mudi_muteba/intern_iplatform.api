﻿using System;

namespace Domain.Base.Extentions
{
    public static class StringExtensions
    {
        public static bool IsNumeric(this string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        public static string TrimFirst(this string s, int lengthToTrim)
        {
            return string.IsNullOrEmpty(s) ? "" : s.Substring(0, s.Length < lengthToTrim ? s.Length : lengthToTrim);
        }

        public static bool IsNumeric(this char s)
        {
            float output;
            return float.TryParse(s.ToString(), out output);
        }

        public static Guid ToGuid(this string strguid)
        {
            Guid guidVal = new Guid();
            if (string.IsNullOrEmpty(strguid))
                return guidVal;

            if (Guid.TryParse(strguid, out guidVal))
                return guidVal;

            return guidVal;
        }

        public static string ReplaceCharacters(this string value)
        {
            return value.Replace("&", "and");
        }
    }
}