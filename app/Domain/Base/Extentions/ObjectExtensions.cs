﻿using System.Collections;
using System.Text;

namespace Domain.Base.Extentions
{
    public static class ObjectExtensions
    {
        public static string PrintProperties(this object o)
        {
            var s = new StringBuilder();
            s.Append(string.Format("{0}: ", o.GetType()));
            var propertyInfos = o.GetType().GetProperties();
            foreach (var item in propertyInfos)
            {
                var prop = item.GetValue(o);
                if (prop == null)
                    s.Append(item.Name + ": NULL, ");
                else
                    s.Append(item.Name + ": " + prop + ", ");

                if (prop is IEnumerable && !(prop is string))
                    foreach (var listitem in prop as IEnumerable)
                        s.Append("Item: " + listitem + ", ");
            }

            return s.ToString();
        }
    }
}