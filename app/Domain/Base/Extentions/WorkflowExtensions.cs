﻿using System.Collections.Generic;
using Domain.AuditLogs.Message;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Audit;
using Workflow.Messages;

namespace Domain.Base.Extentions
{
    public static class WorkflowExtensions
    {
        public static void AddAuditLogEntry(this IWorkflowRouter router, CreateAuditLogDto auditLogLogEvent)
        {
            router.Publish(new WorkflowRoutingMessage().AddMessage(new AuditLogWorkflowExecutionMessage(auditLogLogEvent)));
        }

        public static void Publish(this IWorkflowRouter router, ExecutionContext context, params IWorkflowExecutionMessage[] messages)
        {
            Publish(router, context.UserId, context.Username, context.ChannelIds, context.ActiveChannelId, messages);
        }

        public static void Publish(this IWorkflowRouter router, int userId, string userName, string channelIds, int activeChannelId, params IWorkflowExecutionMessage[] messages)
        {
            SetAuditInfo(messages, userId, userName, channelIds, activeChannelId);
            router.Publish(messages);
        }

        internal static void SetAuditInfo(IEnumerable<IWorkflowExecutionMessage> messages, int userId, string userName, string channelIds, int activeChannelId)
        {
            foreach (var message in messages)
            {
                message.UserId = userId;
                message.UserName = userName;
                message.ChannelIds = channelIds;
                message.ActiveChannelId = activeChannelId;
                SetAuditInfo(message.ExecutionPlan.ExecutionMessages, userId, userName, channelIds, activeChannelId);
            }
        }
    }
}