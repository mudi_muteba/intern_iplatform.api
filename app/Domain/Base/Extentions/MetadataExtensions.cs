﻿using System;
using System.Linq;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Base.Extentions
{
    public static class MetadataExtensions
    {
        public static T Get<T>(this ITaskMetadata metadata)
        {
            return (T) metadata;
        }

        public static T GetMetadata<T>(this IWorkflowExecutionMessage message)
        {
            return (T) message.Metadata;
        }

        public static T GetExecutionTask<T>(this WorkflowExecutionMessage message)
        {
            if (message.ExecutionPlan == null || !message.ExecutionPlan.ExecutionMessages.Any())
                throw new Exception("There are no tasks to be executed in the workflow execution message");

            if (message.ExecutionPlan.ExecutionMessages.OfType<T>().FirstOrDefault() == null)
                throw new Exception(string.Format("Type {0} does not existing in workflow execution message for {1}", typeof (T).FullName,
                    message.GetType().FullName));

            return message.ExecutionPlan.ExecutionMessages.OfType<T>().FirstOrDefault();
        }
    }
}