﻿using System;
using Common.Logging;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;

namespace Domain.Base.Execution
{
    public class QueryProcedure<TQuery, TQueryViewResponse, TResult> where TQuery : BaseProcedureQuery<TQueryViewResponse>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(QueryProcedure<TQuery, TQueryViewResponse, TResult>));
        private readonly TQuery query;
        private Action<TQuery> configure = __ => { };
        private Func<TQueryViewResponse, TResult> success = __ => default(TResult);

        public QueryProcedure(TQuery query)
        {
            this.query = query;
        }

        public QueryProcedure<TQuery, TQueryViewResponse, TResult> OnSuccess(Func<TQueryViewResponse, TResult> onSuccess)
        {
            this.success = onSuccess;
            return this;
        }

        public QueryProcedure<TQuery, TQueryViewResponse, TResult> Configure(Action<TQuery> configure)
        {
            this.configure = configure ?? (__ => { });
            return this;
        }

        public QueryViewResult<TResult> Execute()
        {
            var result = new QueryViewResult<TResult>(default(TResult));

            try
            {
                configure(query);

                log.InfoFormat("Executing query {0}", typeof (TQuery));

                var queryResult = query.ExecuteQuery();
                var postExecutionResult = success(queryResult);

                result.Processed(postExecutionResult);

                log.InfoFormat("Completed execution of query {0}. Completed? {1}", typeof (TQuery), result.Completed);
            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TQuery>(authException);
            }
            catch (Exception exception)
            {
                result.HandleException<TQuery>(exception);
            }

            return result;
        }
    }
}