namespace Domain.Base.Execution
{
    public class AuthorisationFailureMessage
    {
        public string DefaultMessage { get; private set; }
        public string MessageKey { get; private set; }

        public AuthorisationFailureMessage(string defaultMessage, string messageKey)
        {
            DefaultMessage = defaultMessage;
            MessageKey = messageKey;
        }
    }
}