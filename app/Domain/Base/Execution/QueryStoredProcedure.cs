using System;
using System.Collections.Generic;

using Common.Logging;

using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Base.Execution
{
    public class QueryStoredProcedure<TQuery, TEntity, TProcedure, TTransform>
        where TQuery : BaseStoredProcedureQuery<TEntity, TProcedure>
        where TEntity : Entity
        where TProcedure : StoredProcedure
        where TTransform : class
    {
        private readonly TQuery query;
        private readonly TProcedure procedure;

        private Action<TQuery> configure = __ => { };
        private Func<IList<TEntity>, IList<TTransform>> success = __ => default(IList<TTransform>);
        private static readonly ILog log = LogManager.GetLogger(typeof(QueryStoredProcedure<TQuery, TEntity, TProcedure, TTransform>));

        public QueryStoredProcedure(TQuery query, TProcedure procedure)
        {
            this.query = query;
            this.procedure = procedure;
        }

        public QueryStoredProcedure<TQuery, TEntity, TProcedure, TTransform> OnSuccess(Func<IList<TEntity>, IList<TTransform>> onSuccess)
        {
            this.success = onSuccess;
            return this;
        }

        public QueryStoredProcedure<TQuery, TEntity, TProcedure, TTransform> Configure(Action<TQuery> configure)
        {
            this.configure = configure ?? (__ => { });
            return this;
        }

        public QueryResult<IList<TTransform>> Execute()
        {
            var result = new QueryResult<IList<TTransform>>(default(IList<TTransform>));

            try
            {
                configure(query);

                log.InfoFormat("Executing query {0}", typeof(TQuery));

                var queryResult = query.ExecuteQuery<TEntity, TProcedure>(procedure);
                var postExecutionResult = success(queryResult);

                result.Processed(postExecutionResult);

                log.InfoFormat("Completed execution of query {0}. Completed? {1}", typeof(TQuery), result.Completed);
            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TQuery>(authException);
            }
            catch (Exception exception)
            {
                result.HandleException<TQuery>(exception);
            }

            return result;
        }
    }
}