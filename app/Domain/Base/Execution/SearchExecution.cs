using System;
using System.Linq;
using Common.Logging;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Execution
{
    public class SearchExecution<TQuery, TEntity, TCriteria, TResult>
        where TEntity : Entity
        where TQuery : BaseQuery<TEntity>
        where TCriteria: BaseCriteria
    {
        private readonly TQuery query;
        private readonly TCriteria criteria;
        private Action<TQuery> configure = __ => { };
        private Func<IQueryable<TEntity>, TResult> success = __ => default(TResult);
        private static readonly ILog log = LogManager.GetLogger(typeof(SearchExecution<TQuery, TEntity, TCriteria, TResult>));

        public SearchExecution(TQuery query, TCriteria criteria)
        {
            this.query = query;
            this.criteria = criteria;
        }

        public SearchExecution<TQuery, TEntity, TCriteria, TResult> OnSuccess(Func<IQueryable<TEntity>, TResult> onSuccess)
        {
            this.success = onSuccess;
            return this;
        }

        public SearchExecution<TQuery, TEntity, TCriteria, TResult> Configure(Action<TQuery> configure)
        {
            this.configure = configure ?? (__ => { });
            return this;
        }

        public QueryResult<TResult> Execute()
        {
            var result = new QueryResult<TResult>(default(TResult));

            ValidateCriteria(result);

            if (!result.Completed)
                return result;

            try
            {
                HandleSearchQuery();

                configure(query);

                log.InfoFormat("Executing query {0}", typeof(TQuery));

                var queryResult = query.ExecuteQuery();
                var postExecutionResult = success(queryResult);

                result.Processed(postExecutionResult);

                log.InfoFormat("Completed execution of query {0}. Completed? {1}", typeof(TQuery), result.Completed);

            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TQuery>(authException);
            }
            catch (Exception exception)
            {
                result.HandleException<TQuery>(exception);
            }

            return result;
        }

        private void HandleSearchQuery()
        {
            var searchQuery = query as ISearchQuery<TCriteria>;
            if (searchQuery == null)
                return;

            searchQuery.WithCriteria(criteria);
        }

        private void ValidateCriteria(QueryResult<TResult> result)
        {
            var validationCriteria = criteria as IValidationAvailableDto;

            if (validationCriteria == null)
                return;

            result.AddValidationMessages(validationCriteria.Validate());
        }
    }
}