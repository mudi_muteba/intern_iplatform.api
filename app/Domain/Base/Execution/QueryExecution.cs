using System;
using System.Linq;
using Common.Logging;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;

namespace Domain.Base.Execution
{
    public class QueryExecution<TQuery, TEntity, TResult>
        where TEntity : Entity
        where TQuery : BaseQuery<TEntity>
    {
        private readonly TQuery query;
        private Action<TQuery> configure = __ => { };
        private Func<IQueryable<TEntity>, TResult> success = __ => default(TResult);
        private static readonly ILog log = LogManager.GetLogger(typeof(QueryExecution<TQuery, TEntity, TResult>));

        public QueryExecution(TQuery query)
        {
            this.query = query;
        }

        public QueryExecution<TQuery, TEntity, TResult> OnSuccess(Func<IQueryable<TEntity>, TResult> onSuccess)
        {
            this.success = onSuccess;
            return this;
        }

        public QueryExecution<TQuery, TEntity, TResult> Configure(Action<TQuery> configure)
        {
            this.configure = configure ?? (__ => { });
            return this;
        }

        public QueryResult<TResult> Execute()
        {
            var result = new QueryResult<TResult>(default(TResult));

            try
            {
                configure(query);

                log.InfoFormat("Executing query {0}", typeof(TQuery));
                var queryResult = query.ExecuteQuery();
                var postExecutionResult = success(queryResult);
                result.Processed(postExecutionResult);

                log.InfoFormat("Completed execution of query {0}. Completed? {1}", typeof(TQuery), result.Completed);
            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TQuery>(authException);
            }
            catch (Exception exception)
            {
                result.HandleException<TQuery>(exception);
            }

            return result;
        }
    }
}