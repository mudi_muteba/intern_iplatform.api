using System;
using System.Linq;
using Common.Logging;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;

namespace Domain.Base.Execution
{
    public class QueryExecutionWithCulture<TQuery, TEntity, TResult, TDto>
        where TEntity : Entity
        where TQuery : BaseQuery<TEntity>
    {
        private readonly TQuery query;
        private Action<TQuery> configure = __ => { };
        private Func<IQueryable<TEntity>, TResult> success = __ => default(TResult);
        private static readonly ILog log = LogManager.GetLogger(typeof(QueryExecutionWithCulture<TQuery, TEntity, TResult, TDto>));
        private readonly IProvideContext contextProvider;

        public QueryExecutionWithCulture(TQuery query, IProvideContext contextProvider)
        {
            this.query = query;
            this.contextProvider = contextProvider;
        }

        public QueryExecutionWithCulture<TQuery, TEntity, TResult, TDto> OnSuccess(Func<IQueryable<TEntity>, TResult> onSuccess)
        {
            this.success = onSuccess;
            return this;
        }

        public QueryExecutionWithCulture<TQuery, TEntity, TResult, TDto> Configure(Action<TQuery> configure)
        {
            this.configure = configure ?? (__ => { });
            return this;
        }

        public QueryResult<TResult> Execute()
        {
            var result = new QueryResult<TResult>(default(TResult));

            try
            {
                configure(query);

                log.InfoFormat("Executing query {0}", typeof(TQuery));

                var queryResult = query.ExecuteQuery();
                var postExecutionResult = success(queryResult);

                var cultureInfo = contextProvider.Get().CultureInfo;

                //Culture
                if (cultureInfo != null)
                    CultureProcessing<TDto>.ProcessCulture(postExecutionResult, cultureInfo);

                result.Processed(postExecutionResult);

                log.InfoFormat("Completed execution of query {0}. Completed? {1}", typeof(TQuery), result.Completed);

            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TQuery>(authException);
            }
            catch (Exception exception)
            {
                result.HandleException<TQuery>(exception);
            }

            return result;
        }
    }
}