using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Base.Execution
{
    public class ExternalQueryResult<TResponse> : ExecutionResult
    {
        public ExternalQueryResult()
        {
        }

        public ExternalQueryResult(TResponse defaultValue)
        {
            Response = defaultValue;
        }

        public TResponse Response { get; private set; }

        public void Processed(TResponse response)
        {
            this.Response = response;
        }

        public LISTPagedResponseDto<TTransformed> CreateLISTPagedResponse<TTransformed>(
            PagedResultDto<TTransformed> transformedResponse)
        {
            var response = new LISTPagedResponseDto<TTransformed>(transformedResponse);

            AssignFailuresToResponse(response);

            return response;
        }
        public LISTResponseDto<TTransformed> CreateLISTResponse<TTransformed>(
            ListResultDto<TTransformed> transformedResponse)
        {
            var response = new LISTResponseDto<TTransformed>(transformedResponse);

            AssignFailuresToResponse(response);

            return response;
        }

        public GETResponseDto<TResponse> CreateGetResponse(TResponse queryResult)
        {
            var response = new GETResponseDto<TResponse>(queryResult);

            AssignFailuresToResponse(response);

            return response;
        }
    }
}