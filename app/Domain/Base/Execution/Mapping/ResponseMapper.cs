﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.CustomApp;
using iPlatform.Api.DTOs.DocumentManagement;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Api.DTOs.Proposals;
using iPlatform.Api.DTOs.Ratings.Response;
using iPlatform.Api.DTOs.SalesForce;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SectionsPerChannel;
using iPlatform.Api.DTOs.Tia.Parties;
using iPlatform.Api.DTOs.Users;
using ValidationMessages;
using iPlatform.Api.DTOs.CimsDataSync;
using iPlatform.Api.DTOs.Logs;
using iPlatform.Api.DTOs.PolicyBindings;
using iPlatform.Api.DTOs.Quotes;

namespace Domain.Base.Execution.Mapping
{
    public class ResponseMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ValidationErrorMessage, ResponseErrorMessage>();

            Mapper
                .CreateMap<HandlerResult<SalesForceExternalResponseDto>, POSTResponseDto<SalesForceExternalResponseDto>>
                ().ForMember(t => t.Errors, o => o.ResolveUsing(result =>
                {
                    var list = new List<ResponseErrorMessage>();

                    list.AddRange(result.AllErrorDTOMessages);
                    list.AddRange(result.Authorisation.DTOMessages);
                    list.AddRange(result.Execution.DTOMessages);
                    list.AddRange(result.Validation.DTOMessages);

                    return list;
                }))
                ;


            Mapper.CreateMap<HandlerResult<int>, DELETEResponseDto<int>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.ResolveUsing(result =>
                {
                    var list = new List<ResponseErrorMessage>();

                    list.AddRange(result.Authorisation.DTOMessages);
                    list.AddRange(result.Execution.DTOMessages);
                    list.AddRange(result.Validation.DTOMessages);

                    return list;
                }))
                ;

            Mapper.CreateMap<HandlerResult<int>, POSTResponseDto<int>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.ResolveUsing(result =>
                {
                    var list = new List<ResponseErrorMessage>();

                    list.AddRange(result.Authorisation.DTOMessages);
                    list.AddRange(result.Execution.DTOMessages);
                    list.AddRange(result.Validation.DTOMessages);
                    list.AddRange(result.AllErrorDTOMessages);

                    return list;
                }))
                ;

            Mapper.CreateMap<HandlerResult<string>, POSTResponseDto<string>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.ResolveUsing(result =>
                {
                    var list = new List<ResponseErrorMessage>();

                    list.AddRange(result.Authorisation.DTOMessages);
                    list.AddRange(result.Execution.DTOMessages);
                    list.AddRange(result.Validation.DTOMessages);

                    return list;
                }));

            Mapper.CreateMap<HandlerResult<bool>, POSTResponseDto<bool>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.ResolveUsing(result =>
                {
                    var list = new List<ResponseErrorMessage>();

                    list.AddRange(result.Authorisation.DTOMessages);
                    list.AddRange(result.Execution.DTOMessages);
                    list.AddRange(result.Validation.DTOMessages);

                    return list;
                }))
                ;

            Mapper.CreateMap<HandlerResult<Guid>, POSTResponseDto<Guid>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.ResolveUsing(result =>
                {
                    var list = new List<ResponseErrorMessage>();

                    list.AddRange(result.Authorisation.DTOMessages);
                    list.AddRange(result.Execution.DTOMessages);
                    list.AddRange(result.Validation.DTOMessages);

                    return list;
                }))
                ;

            Mapper.CreateMap<HandlerResult<ProposalQuoteDto>, POSTResponseDto<ProposalQuoteDto>>()
                .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
                .ForMember(t => t.Response, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<ResultChannelSectionDto>, POSTResponseDto<ResultChannelSectionDto>>()
                .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
                .ForMember(t => t.Response, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<int>, PUTResponseDto<int>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper.CreateMap<HandlerResult<bool>, PUTResponseDto<bool>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper.CreateMap<HandlerResult<ProposalDefinitionDto>, PUTResponseDto<ProposalDefinitionDto>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore());

            Mapper
                .CreateMap
                <HandlerResult<RequestPasswordResetResponseDto>, PUTResponseDto<RequestPasswordResetResponseDto>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper
                .CreateMap
                <HandlerResult<DeleteProposalDefinitionResponseDto>,
                    DELETEResponseDto<DeleteProposalDefinitionResponseDto>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper.CreateMap<HandlerResult<RatingResultDto>, GETResponseDto<RatingResultDto>>()
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper
                .CreateMap
                <HandlerResult<SecondLevelQuestionsForQuoteDto>, GETResponseDto<SecondLevelQuestionsForQuoteDto>>()
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper.CreateMap<HandlerResult<QuoteHeaderDto>, POSTResponseDto<QuoteHeaderDto>>()
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper.CreateMap<QueryResult<LeadActivityCountDto>, POSTResponseDto<LeadActivityCountDto>>()
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper.CreateMap<QueryResult<LeadActivityCountDto>, POSTResponseDto<LeadActivityCountDto>>()
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;
            Mapper.CreateMap<QueryResult<CimsDataSyncProductPricingStructureStoredProcedureDto>, POSTResponseDto<CimsDataSyncProductPricingStructureStoredProcedureDto>>()
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper.CreateMap<QueryResult<IList<CimsDataSyncChannelUserStoredProcedureDto>>, POSTResponseDto<IList<CimsDataSyncChannelUserStoredProcedureDto>>>()
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            Mapper.CreateMap<HandlerResult<NextLeadResponseDto>, PUTResponseDto<NextLeadResponseDto>>()
                .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
                .ForMember(t => t.Response, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<NextLeadResponseDto>, POSTResponseDto<NextLeadResponseDto>>()
                .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
                .ForMember(t => t.Response, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<NextLeadResponseDto>, POSTResponseDto<NextLeadResponseDto>>()
                .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
                .ForMember(t => t.Response, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<int>, POSTResponseDto<NextLeadResponseDto>>()
                .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
                .ForMember(t => t.Response, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<TiaPartiesSearchResultDto>, POSTResponseDto<TiaPartiesSearchResultDto>>()
                .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
                .ForMember(t => t.Response, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<TiaPartySearchResultDto>, POSTResponseDto<TiaPartySearchResultDto>>()
                .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
                .ForMember(t => t.Response, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<DocumentDto>, POSTResponseDto<DocumentDto>>()
              .ForMember(t => t.StatusCode, o => o.Ignore())
              .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
              .ForMember(t => t.Errors, o => o.Ignore())
              ;

            Mapper.CreateMap<HandlerResult<List<DocumentDto>>, POSTResponseDto<List<DocumentDto>>>()
             .ForMember(t => t.StatusCode, o => o.Ignore())
             .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
             .ForMember(t => t.Errors, o => o.Ignore())
             ;

            Mapper.CreateMap<HandlerResult<DocumentResultDto>, GETResponseDto<DocumentResultDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<HandlerResult<LogsResultDto>, POSTResponseDto<LogsResultDto>>()
           .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
           ;

            Mapper.CreateMap<HandlerResult<SubmitPolicyBindingResponseDto>, POSTResponseDto<SubmitPolicyBindingResponseDto>>()
           .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.MapFrom(a => a.AllErrorDTOMessages))
           ;

            Mapper.CreateMap<HandlerResult<QuoteDiscountResultDto>, PUTResponseDto<QuoteDiscountResultDto>>()
                .ForMember(t => t.Link, o => o.Ignore())
                .ForMember(t => t.StatusCode, o => o.Ignore())
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                .ForMember(t => t.Errors, o => o.Ignore())
                ;

            #region [ CustomApp Mapping ]

            Mapper.CreateMap<HandlerResult<List<RegisterDto>>, CustomAppResponseDto<List<RegisterDto>>>()
                .ForMember(t => t.LastErrorDescription, o => o.MapFrom(a => a.AllErrorDtoMessagesToString))
                .ForMember(t => t.Data, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<List<VerifyMemberDto>>, CustomAppResponseDto<List<VerifyMemberDto>>>()
                .ForMember(t => t.LastErrorDescription, o => o.MapFrom(a => a.AllErrorDtoMessagesToString))
                .ForMember(t => t.Data, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<List<GetPolicyInfoDto>>, CustomAppResponseDto<List<GetPolicyInfoDto>>>()
                .ForMember(t => t.LastErrorDescription, o => o.MapFrom(a => a.AllErrorDtoMessagesToString))
                .ForMember(t => t.Data, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<List<GetMemberDetailsDto>>, CustomAppResponseDto<List<GetMemberDetailsDto>>>()
                .ForMember(t => t.LastErrorDescription, o => o.MapFrom(a => a.AllErrorDtoMessagesToString))
                .ForMember(t => t.Data, o => o.MapFrom(a => a.Response));

            Mapper.CreateMap<HandlerResult<List<UpdateMemberDetailsDto>>, CustomAppResponseDto<List<UpdateMemberDetailsDto>>>()
                .ForMember(t => t.LastErrorDescription, o => o.MapFrom(a => a.AllErrorDtoMessagesToString))
                .ForMember(t => t.Data, o => o.MapFrom(a => a.Response));

            #endregion

            #region Smart Policies

            Mapper.CreateMap<HandlerResult<SmartGetPolicyResponseDto>, POSTResponseDto<SmartGetPolicyResponseDto>>()
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response));

            #endregion
        }
    }
}