namespace Domain.Base.Execution
{
    public class DefaultContextProvider : IProvideContext
    {
        private ExecutionContext context;

        public void SetContext(ExecutionContext context)
        {
            this.context = context;
        }

        public ExecutionContext Get()
        {
            return context;
        }
    }
}