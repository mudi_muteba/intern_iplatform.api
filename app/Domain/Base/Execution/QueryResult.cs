using System;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Base.Execution
{
    public class QueryResult<TResponse> : ExecutionResult
    {
        public QueryResult()
        {
        }

        public QueryResult(TResponse defaultValue)
        {
            Response = defaultValue;
        }

        public TResponse Response { get; private set; }

        public void Processed(TResponse response)
        {
            this.Response = response;
        }

        public LISTPagedResponseDto<TTransformed> CreateLISTPagedResponse<TTransformed>(
            PagedResultDto<TTransformed> transformedResponse)
        {
            var response = new LISTPagedResponseDto<TTransformed>(transformedResponse);

            AssignFailuresToResponse(response);

            return response;
        }
        public LISTResponseDto<TTransformed> CreateLISTResponse<TTransformed>(
            ListResultDto<TTransformed> transformedResponse)
        {
            var response = new LISTResponseDto<TTransformed>(transformedResponse);

            AssignFailuresToResponse(response);

            return response;
        }

        public POSTResponseDto<TResponse> CreateGetPostResponse()
        {
            var response = Mapper.Map<POSTResponseDto<TResponse>>(this);
            AssignFailuresToResponse(response);
            return response;
        }

        public GETResponseDto<TResponse> CreateGETResponse()
        {
            var response = new GETResponseDto<TResponse>(Response);

            if (!Authorisation.Passed)
            {
                response.AuthorisationFailed(Authorisation.DTOMessages);
            }

            if (!Validation.Passed)
            {
                response.ValidationFailed(Authorisation.DTOMessages);
            }

            if (!Execution.Passed)
            {
                response.ExecutionFailed(Authorisation.DTOMessages);
            }

            return response;
        }

    }
}