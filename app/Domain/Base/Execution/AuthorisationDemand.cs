﻿using System.Collections.Generic;
using System.Linq;
using MasterData.Authorisation;

namespace Domain.Base.Execution
{
    public class AuthorisationDemand
    {
        internal AuthorisationDemand()
        {
            Channels = new List<int>();
        }

        public AuthorisationDemand(List<RequiredAuthorisationPoint> allowed, List<int> channels)
        {
            Allowed = allowed;
            Channels = channels;
        }

        public List<RequiredAuthorisationPoint> Allowed { get; private set; }
        public List<int> Channels { get; private set; }

        public bool Satisfied
        {
            get { return Channels.Any(); }
        }

        internal void Allow(int channel)
        {
            Channels.Add(channel);
        }
    }
}