using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Base.Execution
{
    public class GetByIdResult<TResponse> : ExecutionResult
    {
        public TResponse Response { get; private set; }

        public GetByIdResult() { }

        public GetByIdResult(TResponse response)
        {
            Response = response;
        }

        public void Processed(TResponse response)
        {
            Response = response;
        }

        public GETResponseDto<TResponse> CreateGETResponse()
        {
            var response = new GETResponseDto<TResponse>(Response);

            if (!Authorisation.Passed)
                response.AuthorisationFailed(Authorisation.DTOMessages);

            if (!Validation.Passed)
                response.ValidationFailed(Authorisation.DTOMessages);

            if (!Execution.Passed)
                response.ExecutionFailed(Authorisation.DTOMessages);

            return response;
        }

    }
}