﻿using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Execution
{
    public static class CultureProcessing<TDto>
    {
        public static void ProcessCulture<T>(T result, CultureParameter cultureinfo)
        {
            //Culture Aware
            var cultureAwareResult = result as ICultureAware;
            if (cultureAwareResult != null)
                cultureAwareResult.Accept(cultureinfo);

            //PagedResultDto
            var pagedResult = result as IPagedResultDto;
            if (pagedResult != null)
            {
                var values = result as PagedResultDto<TDto>;
                if (values != null)
                {
                    for (int i = 0; i < values.Results.Count(); i++)
                    {
                        var cultureAware = (values.Results[i] as ICultureAware);
                        if (cultureAware != null)
                            cultureAware.Accept(cultureinfo);
                    }
                }
            }


            //PagedResultDto
            var listResult = result as IListResultDto;
            if (listResult != null)
            {
                var values = result as ListResultDto<TDto>;

                for (int i = 0; i < values.Results.Count(); i++)
                {
                    var cultureAware = (values.Results[i] as ICultureAware);
                    if (cultureAware != null)
                        cultureAware.Accept(cultureinfo);
                }
            }


            //Generic
            var genericResult = result as IEnumerable;
            if (genericResult != null)
            {
                var values = result as List<TDto>;

                for (int i = 0; i < values.Count(); i++)
                {
                    var cultureAware = (values[i] as ICultureAware);
                    if (cultureAware != null)
                        cultureAware.Accept(cultureinfo);
                }
            }

        }
    }
}
