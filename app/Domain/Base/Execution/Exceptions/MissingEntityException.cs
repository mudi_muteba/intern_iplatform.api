﻿using System;
using System.Runtime.Serialization;

namespace Domain.Base.Execution.Exceptions
{
    public class MissingEntityException : Exception
    {
        public MissingEntityException(Type entityType, int missingEntityId)
        {
            MissingEntityId = missingEntityId;
            MissingEntityType = entityType;
        }

        public MissingEntityException(string message) : base(message)
        {
        }

        public MissingEntityException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MissingEntityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public Type MissingEntityType { get; private set; }
        public int MissingEntityId { get; private set; }
    }
}