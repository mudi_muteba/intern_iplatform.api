﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MasterData.Authorisation;

namespace Domain.Base.Execution.Exceptions
{
    public class UnauthorisationException : Exception
    {
        public UnauthorisationException(List<RequiredAuthorisationPoint> need)
        {
            Need = need;
        }

        public UnauthorisationException(string message) : base(message)
        {
            Need = new List<RequiredAuthorisationPoint>();
        }

        public UnauthorisationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnauthorisationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public List<RequiredAuthorisationPoint> Need { get; private set; }
    }
}