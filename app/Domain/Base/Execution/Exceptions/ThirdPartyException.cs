﻿using System;
using System.Runtime.Serialization;

namespace Domain.Base.Execution.Exceptions
{
    public class ThirdPartyException : Exception
    {
        public ThirdPartyException()
        {
        }

        public ThirdPartyException(string message)
            : base(message)
        {
        }

        public ThirdPartyException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ThirdPartyException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

    }
}