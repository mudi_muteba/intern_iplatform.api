using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ValidationMessages;

namespace Domain.Base.Execution.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException()
        {
            Messages = new List<ValidationErrorMessage>();
        }

        public ValidationException(params ValidationErrorMessage[] messages)
        {
            Messages = new List<ValidationErrorMessage>(messages);
        }

        public ValidationException(string message) : base(message)
        {
            Messages = new List<ValidationErrorMessage>();
        }

        public ValidationException(string message, Exception innerException) : base(message, innerException)
        {
            Messages = new List<ValidationErrorMessage>();
        }

        protected ValidationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Messages = new List<ValidationErrorMessage>();
        }

        public List<ValidationErrorMessage> Messages { get; private set; }
    }
}