﻿using System;
using System.Collections.Generic;
using MasterData.Authorisation;
using Shared.Extentions;
using iPlatform.Api.DTOs.Base.Culture;
using Shared;

namespace Domain.Base.Execution
{
    public class ExecutionContext : IExecutionContext
    {
        public string UserAgent { get; private set; }
        public string Referrer { get; private set; }
        public Guid RequestId { get; private set; }
        public string RequestType { get; private set; }
        public string RequestIp { get; private set; }
        public DateTime RequestDate { get; private set; }
        public string RequestMethod { get; private set; }
        public string RequestUrl { get; private set; }
        public Dictionary<string, IEnumerable<string>> RequestHeaders { get; set; }
        public List<int> Channels { get; private set; }
        public int ActiveChannelId { get; private set; }
        public bool IsChannelFreeQuery { get; private set; }
        public string ChannelIds
        {
            get { return string.Join(",", Channels); }
        }
        public string Token { get; private set; }
        public int UserId { get; private set; }
        public string Username { get; private set; }
        public string UserFullname { get; private set; }
        public SystemAuthorisation Authorisation { get; set; }
        public CultureParameter CultureInfo { get; set; }
        public Guid ActiveChannelSystemId { get; set; }
        internal ExecutionContext()
        {
            Channels = new List<int>();
            IsChannelFreeQuery = false;
        }

        public ExecutionContext(string token, int userId, string username, List<int> channels, int activeChannelId, SystemAuthorisation authorisation)
        {
            Channels = channels ?? new List<int>();
            ActiveChannelId = activeChannelId;
            Token = token;
            UserId = userId;
            Username = username;
            Authorisation = authorisation;
        }

        public ExecutionContext(string token, int userId, string username, string userFullname, CultureParameter cultureInfo, List<int> channels, int activeChannelId, SystemAuthorisation authorisation, Dictionary<string, IEnumerable<string>> headers, string userAgent, string referrer, Guid requestId, string requestType, string requestIp, DateTime requestDate, string requestMethod, string requestUrl, Guid systemId)
        {
            Channels = channels ?? new List<int>();
            ActiveChannelId = activeChannelId;
            Token = token;
            UserId = userId;
            Username = username;
            UserFullname = userFullname;
            Authorisation = authorisation;
            CultureInfo = cultureInfo;
            RequestHeaders = headers;
            UserAgent = userAgent;
            Referrer = referrer;
            RequestId = requestId;
            RequestType = requestType;
            RequestIp = requestIp;
            RequestDate = requestDate;
            RequestMethod = requestMethod;
            RequestUrl = requestUrl;
            ActiveChannelSystemId = systemId;
        }

        public bool Authorised(int channelId, List<RequiredAuthorisationPoint> needAuthorisationPoints)
        {
            return Authorisation.Authorised(channelId, needAuthorisationPoints);
        }

        public SystemAuthorisation GetAuthorisation()
        {
            return Authorisation;
        }

        public List<RequiredAuthorisationPoint> GetAuthorisation(int channelId)
        {
            return Authorisation.GetRights(channelId);
        }

        public AuthorisationDemand Authorised(List<int> channels, List<RequiredAuthorisationPoint> needAuthorisationPoints)
        {
            var demand = new AuthorisationDemand();
            
            var demandSatisfied = Authorised(ActiveChannelId, needAuthorisationPoints);
            if (demandSatisfied)
                demand.Allow(ActiveChannelId);

            return demand;
        }

        public override string ToString()
        {
            return string.Format("Context: {0}/{1} \n Channels: {2} \n Authorisation:\n {3}",
                UserId,
                Username,
                Channels.FlattenListToString(c => c.ToString()),
                Authorisation
                );
        }

        public void SetChannelFree(bool isChannelFreeQuery)
        {
            this.IsChannelFreeQuery = isChannelFreeQuery;
        }
    }
}