using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Castle.Windsor;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Base;
using Shared.Extentions;
using ValidationMessages;

namespace Domain.Base.Execution
{
    internal class ValidatorExecution
    {
        private readonly IWindsorContainer _container;
        private readonly IProvideContext _contextProvider;

        public ValidatorExecution(IProvideContext contextProvider, IWindsorContainer container)
        {
            _contextProvider = contextProvider;
            _container = container;
        }

        public void Validate<TDto>(TDto dto, ExecutionResult result)
        {
            this.Info(() => string.Format("Validating DTO of type {0}", typeof (TDto)));

            HandleChannelDto(dto, result);
            HandleDefaultValidation(dto, result);
            HandleCustomValidators(dto, result);
            HandleAttributeBasedValidation(dto, result);
        }

        private void HandleCustomValidators<TDto>(TDto dto, ExecutionResult result)
        {
            var validators = _container.ResolveAll<IValidateDto<TDto>>();

            this.Info(() => string.Format("Found {0} validator(s) for {1}", validators.Count(), typeof (TDto)));

            foreach (var validator in validators)
            {
                this.Info(() => string.Format("Executing validator {0}", validator.GetType()));
                ExecuteCustomValidation(dto, result, validator);
            }
        }

        private void ExecuteCustomValidation<TDto>(TDto dto, ExecutionResult result, IValidateDto<TDto> validator)
        {
            try
            {
                validator.Validate(dto, result);
            }
            finally
            {
                _container.Release(validator);
            }
        }

        private void HandleDefaultValidation<TDto>(TDto dto, ExecutionResult result)
        {
            if (!(dto is IValidationAvailableDto))
                return;

            this.Info(() => string.Format("Executing default DTO validation for {0}", typeof (TDto)));

            var messages = (dto as IValidationAvailableDto).Validate();
            if (messages.Any())
                this.Debug(() => string.Format("Default validation for {0} failed. There are {1} messages", dto.GetType(), messages.Count));

            foreach (ValidationErrorMessage message in messages)
                result.AddValidationMessage(message.PropertyName, message.DefaultMessage, message.MessageKey);
        }

        private void HandleAttributeBasedValidation<TDto>(TDto dto, ExecutionResult result)
        {
            var properties = dto.GetType().GetProperties();
            foreach (var propertyInfo in properties)
            {
                var attributes = propertyInfo.GetCustomAttributes(true);
                var typeConverter = TypeDescriptor.GetConverter(typeof (ValidationAttribute));

                foreach (var attr in attributes)
                {
                    var attrib = attr as ValidationAttribute;
                    if (attrib != null)
                        if (!attrib.IsValid(propertyInfo.GetValue(dto, null)))
                            result.AddValidationMessage(ValidationMessageCollection.FindByKey(attrib.ErrorMessage));
                }
            }
        }

        private void HandleChannelDto<TDto>(TDto dto, ExecutionResult result)
        {
            if (!typeof (IChannelAwareDto).IsAssignableFrom(typeof (TDto)))
                return;

            this.Info(() => string.Format("Executing channel validation for {0}", typeof (TDto)));

            new ChannelValidation(_contextProvider.Get()).Validate(dto as IChannelAwareDto, result);
        }
    }
}