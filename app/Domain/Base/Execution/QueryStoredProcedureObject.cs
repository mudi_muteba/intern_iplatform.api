﻿using System;
using System.Collections.Generic;
using Common.Logging;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Base.Execution
{
    public class QueryStoredProcedureObject<TQuery, TObject, TProcedure, TTransform>
        where TQuery : BaseStoredProcedureQueryObject<TObject, TProcedure>
        where TObject : class 
        where TProcedure : StoredProcedure
        where TTransform : class
    {
        private readonly TQuery query;
        private readonly TProcedure procedure;

        private Action<TQuery> configure = __ => { };
        private Func<IList<TObject>, IList<TTransform>> success = __ => default(IList<TTransform>);
        private static readonly ILog log = LogManager.GetLogger(typeof(QueryStoredProcedureObject<TQuery, TObject, TProcedure, TTransform>));

        public QueryStoredProcedureObject(TQuery query, TProcedure procedure)
        {
            this.query = query;
            this.procedure = procedure;
        }

        public QueryStoredProcedureObject<TQuery, TObject, TProcedure, TTransform> OnSuccess(Func<IList<TObject>, IList<TTransform>> onSuccess)
        {
            this.success = onSuccess;
            return this;
        }

        public QueryStoredProcedureObject<TQuery, TObject, TProcedure, TTransform> Configure(Action<TQuery> configure)
        {
            this.configure = configure ?? (__ => { });
            return this;
        }

        public QueryResult<IList<TTransform>> Execute()
        {
            var result = new QueryResult<IList<TTransform>>(default(IList<TTransform>));

            try
            {
                configure(query);

                log.InfoFormat("Executing query {0}", typeof(TQuery));

                var queryResult = query.ExecuteQuery<TObject, TProcedure>(procedure);
                var postExecutionResult = success(queryResult);

                result.Processed(postExecutionResult);

                log.InfoFormat("Completed execution of query {0}. Completed? {1}", typeof(TQuery), result.Completed);
            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TQuery>(authException);
            }
            catch (Exception exception)
            {
                result.HandleException<TQuery>(exception);
            }

            return result;
        }
    }
}