using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using Domain.Base.Execution.Exceptions;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using MasterData.Authorisation;
using Shared;
using Shared.Extentions;
using ValidationMessages;

namespace Domain.Base.Execution
{
    public class ExecutionResult
    {
        private static readonly ILog log = LogManager.GetLogger<ExecutionResult>();

        public ExecutionResult()
        {
            Validation = new ValidationSummary();
            Execution = new HandlerSummary();
            Authorisation = new AuthorisationSummary();
            MissingEntity = new MissingEntitySummary();
        }

        public AuthorisationSummary Authorisation { get; private set; }
        public ValidationSummary Validation { get; private set; }
        public HandlerSummary Execution { get; private set; }
        public MissingEntitySummary MissingEntity { get; private set; }

        private List<ResponseErrorMessage> errors = new List<ResponseErrorMessage>();

        public void AddError(ResponseErrorMessage error)
        {
            errors.Add(error);
        }
        public void AddErrors(List<ResponseErrorMessage> _errors)
        {
            errors.AddRange(_errors);
        }

        public List<ResponseErrorMessage> AllErrorDTOMessages
        {
            get
            {
                return Authorisation.DTOMessages
                    .Union(Validation.DTOMessages)
                    .Union(Execution.DTOMessages)
                    .Union(errors)
                    .Union(MissingEntity.DTOMessages)
                    .ToList();
            }
        }

        public string AllErrorDtoMessagesToString
        {
            get
            {
                var errorsList = Authorisation.DTOMessages
                    .Union(Validation.DTOMessages)
                    .Union(Execution.DTOMessages)
                    .Union(errors)
                    .Union(MissingEntity.DTOMessages)
                    .ToList();
                var result = string.Empty;
                errorsList.ForEach(a=> result+=a.DefaultMessage);
                return result;
            }
        }

        public bool Completed
        {
            get { return Validation.Passed && Execution.Passed && Authorisation.Passed && MissingEntity.Passed; }
        }

        public void AddFailure(string failure)
        {
            Execution.AddFailure(failure);
        }

        public void AddValidationMessage(string propertyName, string defaultMessage, string messageKey)
        {
            Validation.AddValidationMessage(propertyName, defaultMessage, messageKey);
        }

        public void AddValidationMessage(ValidationErrorMessage validationErrorMessage)
        {
            Validation.AddValidationMessage(validationErrorMessage);
        }

        public void AddValidationMessages(IEnumerable<ValidationErrorMessage> validationErrorMessages)
        {
            foreach (var message in validationErrorMessages)
                AddValidationMessage(message);
        }

        public void AddAuthorisationFailure(List<RequiredAuthorisationPoint> requiredAuthorisationPoints)
        {
            foreach (var point in requiredAuthorisationPoints)
                Authorisation.AddReason(string.Format("Required authorisation: {0}", point), point.MessageKey);
        }

        public void HandleUnauthorisationException<T>(UnauthorisationException authException)
        {
            var errorMessage = string.Format("Failed to complete call handled by {0}. Insufficient authorisation. Need: {1}.",
                typeof(T),
                authException.Need.FlattenListToString(p => p.Name));

            log.ErrorFormat(errorMessage);

            AddAuthorisationFailure(authException.Need);

            if (!authException.Need.Any())
                Authorisation.AddReason(authException.Message, "iPLATFORM_AUTHORISATION_FAILED");
        }

        public void HandleValidationException(ValidationException validationException)
        {
            foreach (var validationErrorMessage in validationException.Messages)
                AddValidationMessage(validationErrorMessage);
        }

        public void HandleException<T>(Exception e)
        {
            var exceptionMessage = new ExceptionPrettyPrinter().Print(e);
            var errorMessage = string.Format("Failed to execute {0}. The exception was {1}.", typeof(T), exceptionMessage);

            log.ErrorFormat(errorMessage);

            AddFailure(errorMessage);
        }

        public void HandleMissingEntity(MissingEntityException missingEntityException)
        {
            var message = SystemErrorMessages.MissingEntityErrorMessage(missingEntityException.MissingEntityType, missingEntityException.MissingEntityId);

            log.ErrorFormat(message.DefaultMessage);

            MissingEntity.AddMessage(message);
        }

        protected void AssignFailuresToResponse(BaseResponseDto response)
        {
            if (!Authorisation.Passed)
                response.AuthorisationFailed(Authorisation.DTOMessages);

            if (!Validation.Passed)
                response.ValidationFailed(Validation.DTOMessages);

            if (!Execution.Passed)
                response.ExecutionFailed(Execution.DTOMessages);

            if (!MissingEntity.Passed)
                response.MissingEntity(MissingEntity.DTOMessages);
        }
    }
}