using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Base.Execution
{
    public class HandlerResult<TId> : ExecutionResult
    {
        public TId Response { get; private set; }

        public void Processed(TId response)
        {
            Response = response;
        }

        public PUTResponseDto<TId> CreatePUTResponse()
        {
            var response = Mapper.Map<PUTResponseDto<TId>>(this);
            AssignFailuresToResponse(response);
            return response;
        }

        public POSTResponseDto<TId> CreatePOSTResponse()
        {
            var response = Mapper.Map<POSTResponseDto<TId>>(this);
            AssignFailuresToResponse(response);
            return response;
        }

        public GETResponseDto<TId> CreateGETResponse()
        {
            var response = Mapper.Map<GETResponseDto<TId>>(this);
            AssignFailuresToResponse(response);
            return response;
        }

        public DELETEResponseDto<TId> DeletePOSTResponse()
        {
            var response = Mapper.Map<DELETEResponseDto<TId>>(this);
            AssignFailuresToResponse(response);
            return response;
        }

        public LISTResponseDto<TId> CreateLISTResponse<TId>(ListResultDto<TId> transformedResponse)
        {
            var response = new LISTResponseDto<TId>(transformedResponse);
            AssignFailuresToResponse(response);
            return response;
        }

        public CustomAppResponseDto<TId> CreateCustomAppResponse()
        {
            var response = Mapper.Map<CustomAppResponseDto<TId>>(this);
            return response;
        }
    }
}