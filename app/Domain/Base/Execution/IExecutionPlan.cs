using System;

using Domain.Base.Queries;

using iPlatform.Api.DTOs.Base;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Base.Execution
{
    public interface IExecutionPlan
    {
        HandlerResult<TId> Execute<T, TId>(T dto) where T : IExecutionDto;

        ExternalQueryExecution<TQuery, TExternalResponse, TResult> ExternalQuery<TQuery, TExternalResponse, TResult>()
            where TQuery : BaseExternalSystemQuery<TExternalResponse>;

        GetByIdPlan<TEntity, TTransform> GetById<TEntity, TTransform>(Func<TEntity> get) where TEntity : Entity;

        QueryExecution<TQuery, TEntity, TResult> Query<TQuery, TEntity, TResult>()
            where TQuery : BaseQuery<TEntity> where TEntity : Entity;

        QueryExecutionWithCulture<TQuery, TEntity, TResult, TDto> Query<TQuery, TEntity, TResult, TDto>()
            where TQuery : BaseQuery<TEntity>
            where TEntity : Entity;

        SearchExecution<TQuery, TEntity, TCriteria, TResult> Search<TQuery, TEntity, TCriteria, TResult>(TCriteria criteria)
            where TQuery : BaseQuery<TEntity>
            where TEntity : Entity
            where TCriteria : BaseCriteria;

        SearchExecutionWithCulture<TQuery, TEntity, TCriteria, TResult, TDto> Search<TQuery, TEntity, TCriteria, TResult, TDto>(TCriteria criteria)
            where TQuery : BaseQuery<TEntity>
            where TEntity : Entity
            where TCriteria : BaseCriteria;

        QueryProcedure<TQuery, TQueryViewResponse, TResult> QueryProcedure<TQuery, TQueryViewResponse, TResult>()
            where TQuery : BaseProcedureQuery<TQueryViewResponse>;

        QueryStoredProcedure<TQuery, TEntity, TProcedure, TTransform> QueryStoredProcedure<TQuery, TEntity, TProcedure, TTransform>(TProcedure procedure)
            where TQuery : BaseStoredProcedureQuery<TEntity, TProcedure>
            where TEntity : Entity
            where TProcedure : StoredProcedure
            where TTransform : class;

        QueryStoredProcedureObject<TQuery, TObject, TProcedure, TTransform> QueryStoredProcedureObject<TQuery, TObject, TProcedure, TTransform>(TProcedure procedure)
           where TQuery : BaseStoredProcedureQueryObject<TObject, TProcedure>
           where TObject : class 
           where TProcedure : StoredProcedure
           where TTransform : class;
    }
}