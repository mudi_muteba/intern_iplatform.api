using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using ValidationMessages;

namespace Domain.Base.Execution
{
    public class MissingEntitySummary
    {
        private readonly List<MissingEntityErrorMessage> messages = new List<MissingEntityErrorMessage>();

        public IEnumerable<ResponseErrorMessage> DTOMessages
        {
            get { return Messages.Select(m => new ResponseErrorMessage(m.DefaultMessage, m.MessageKey)); }
        }

        public IEnumerable<MissingEntityErrorMessage> Messages
        {
            get { return messages; }
        }

        public bool Passed
        {
            get { return !messages.Any(); }
        }

        public void AddMessage(string typeName, string defaultMessage, string messageKey)
        {
            messages.Add(new MissingEntityErrorMessage(typeName, defaultMessage, messageKey));
        }

        public void AddMessage(MissingEntityErrorMessage validationErrorMessage)
        {
            messages.Add(validationErrorMessage);
        }
    }
}