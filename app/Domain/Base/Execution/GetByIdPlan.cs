using System;
using Common.Logging;
using Domain.Base.Execution.Exceptions;
using iPlatform.Api.DTOs.Base.Culture;

namespace Domain.Base.Execution
{
    public class GetByIdPlan<TEntity, TTransform>
    {
        private readonly Func<TEntity> get;
        private Func<TEntity, TTransform> transform;
        private static readonly ILog log = LogManager.GetLogger(typeof(GetByIdPlan<TEntity, TTransform>));
        private readonly IProvideContext contextProvider;

        public GetByIdPlan(Func<TEntity> get, IProvideContext contextProvider)
        {
            this.get = get;
            this.contextProvider = contextProvider;
        }

        public GetByIdResult<TTransform> Execute()
        {
            var result = new GetByIdResult<TTransform>();

            try
            {
                log.InfoFormat("Getting {0} by id", typeof(TEntity));

                var entity = get();
                var transformed = transform(entity);

                //Culture
                var cultureinfo = contextProvider.Get().CultureInfo;
                if (cultureinfo != null && transformed is ICultureAware)
                    ((ICultureAware)transformed).Accept(cultureinfo);

                result.Processed(transformed);

                log.InfoFormat("Completed get by id for {0}. Completed? {1}. Anything found? {2}", 
                    typeof(TEntity), 
                    result.Completed,
                    result.Response != null);
            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TEntity>(authException);
            }
            catch (Exception exception)
            {
                result.HandleException<TEntity>(exception);
            }

            return result;
        }


        public GetByIdPlan<TEntity, TTransform> OnSuccess(Func<TEntity, TTransform> transform)
        {
            this.transform = transform ?? (r => default(TTransform));
            return this;
        }
    }
}