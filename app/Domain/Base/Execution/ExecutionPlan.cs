using System;
using Castle.Windsor;
using Domain.Base.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using Domain.Base.Repository.StoredProcedures;

namespace Domain.Base.Execution
{
    public class ExecutionPlan : IExecutionPlan
    {
        private readonly IWindsorContainer _container;
        private readonly IProvideContext _contextProvider;

        public ExecutionPlan(IWindsorContainer container, IProvideContext contextProvider)
        {
            _container = container;
            _contextProvider = contextProvider;
        }

        public HandlerResult<TId> Execute<TDto, TId>(TDto dto) where TDto : IExecutionDto
        {
            var plan = new HandlerPlan(_contextProvider, _container);
            var result = plan.Execute<TDto, TId>(dto);

            //Culture
            if(typeof(ICultureAware).IsAssignableFrom(typeof(TId)))
            {
                var cultureInfo = _contextProvider.Get().CultureInfo;
                if (cultureInfo != null && result.Response != null)
                    (result.Response as ICultureAware).Accept(_contextProvider.Get().CultureInfo);
            }

            return result; 
        }

        public HandlerResult<TId> Execute<TDto, TId, TInnerDto>(TDto dto) where TDto : IExecutionDto
        {
            var plan = new HandlerPlan(_contextProvider, _container);
            var result = plan.Execute<TDto, TId>(dto);

            //Culture
            if (typeof(ICultureAware).IsAssignableFrom(typeof(TId)))
            {
                var cultureInfo = _contextProvider.Get().CultureInfo;
                if (cultureInfo != null)
                    (result.Response as ICultureAware).Accept(_contextProvider.Get().CultureInfo);
            }

            return result;
        }

        public QueryExecution<TQuery, TEntity, TResult> Query<TQuery, TEntity, TResult>()
            where TQuery : BaseQuery<TEntity> where TEntity : Entity
        {
            var query = _container.Resolve<TQuery>();
            _container.Release(query);
            return new QueryExecution<TQuery, TEntity, TResult>(query);
        }

        public QueryStoredProcedure<TQuery, TEntity, TProcedure, TTransform> QueryStoredProcedure<TQuery, TEntity, TProcedure, TTransform>(TProcedure procedure)
            where TQuery : BaseStoredProcedureQuery<TEntity, TProcedure>
            where TEntity : Entity
            where TProcedure : StoredProcedure
            where TTransform : class
        {
            var query = _container.Resolve<TQuery>();
            _container.Release(query);
            return new QueryStoredProcedure<TQuery, TEntity, TProcedure, TTransform>(query, procedure);
        }

        public QueryStoredProcedureObject<TQuery, TObject, TProcedure, TTransform> QueryStoredProcedureObject<TQuery, TObject, TProcedure, TTransform>(TProcedure procedure)
            where TQuery : BaseStoredProcedureQueryObject<TObject, TProcedure>
            where TObject: class 
            where TProcedure : StoredProcedure
            where TTransform : class
        {
            var query = _container.Resolve<TQuery>();
            _container.Release(query);
            return new QueryStoredProcedureObject<TQuery, TObject, TProcedure, TTransform>(query, procedure);
        }

        public QueryExecutionWithCulture<TQuery, TEntity, TResult, TDto> Query<TQuery, TEntity, TResult, TDto>()
            where TQuery : BaseQuery<TEntity>
            where TEntity : Entity
        {
            var query = _container.Resolve<TQuery>();
            _container.Release(query);
            return new QueryExecutionWithCulture<TQuery, TEntity, TResult, TDto>(query, _contextProvider);
        }

        public ExternalQueryExecution<TQuery, TExternalResponse, TResult> ExternalQuery<TQuery, TExternalResponse, TResult>() where TQuery : BaseExternalSystemQuery<TExternalResponse>
        {
            var query = _container.Resolve<TQuery>();
            _container.Release(query);
            return new ExternalQueryExecution<TQuery, TExternalResponse, TResult>(query);
        }

        public QueryProcedure<TQuery, TQueryViewResponse, TResult> QueryProcedure<TQuery, TQueryViewResponse, TResult>() where TQuery : BaseProcedureQuery<TQueryViewResponse>
        {
            var query = _container.Resolve<TQuery>();
            _container.Release(query);
            return new QueryProcedure<TQuery, TQueryViewResponse, TResult>(query);
        }

        public SearchExecution<TQuery, TEntity, TCriteria, TResult> Search<TQuery, TEntity, TCriteria, TResult>(TCriteria criteria) 
            where TQuery : BaseQuery<TEntity> 
            where TEntity : Entity 
            where TCriteria : BaseCriteria
        {
            var query = _container.Resolve<TQuery>();
            _container.Release(query);
            return new SearchExecution<TQuery, TEntity, TCriteria, TResult>(query, criteria);
        }

        public SearchExecutionWithCulture<TQuery, TEntity, TCriteria, TResult, TDto> Search<TQuery, TEntity, TCriteria, TResult, TDto>(TCriteria criteria)
            where TQuery : BaseQuery<TEntity>
            where TEntity : Entity
            where TCriteria : BaseCriteria
        {
            var query = _container.Resolve<TQuery>();
            _container.Release(query);
            return new SearchExecutionWithCulture<TQuery, TEntity, TCriteria, TResult, TDto>(query, criteria, _contextProvider);
        }

        public GetByIdPlan<TEntity, TTransform> GetById<TEntity, TTransform>(Func<TEntity> get) where TEntity : Entity
        {
            return new GetByIdPlan<TEntity, TTransform>(get, _contextProvider);
        }
    }
}