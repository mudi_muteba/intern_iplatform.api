using System;
using Castle.Windsor;
using Domain.Base.Execution.Exceptions;
using iPlatform.Api.DTOs.Base;
using Shared.Extentions;

namespace Domain.Base.Execution
{
    internal class HandlerPlan
    {
        private readonly IWindsorContainer _container;
        private readonly IProvideContext _contextProvider;

        public HandlerPlan(IProvideContext contextProvider, IWindsorContainer container)
        {
            _contextProvider = contextProvider;
            _container = container;
        }

        public HandlerResult<TId> Execute<TDto, TId>(TDto dto) where TDto : IExecutionDto
        {
            var result = new HandlerResult<TId>();

            try
            {
                var context = _contextProvider.Get();
                if (context != null)
                    dto.SetContext(new DtoContext(context.UserId, context.Username, context.Token));

                PrintContextForDebug(context);

                PerformValidation(dto, result);

                if (!result.Completed)
                    return result;

                PerformExecution(dto, result);

                this.Info(() => string.Format("Completed execution plan for {0}. Completed? {1} ({2})", typeof (TDto), result.Completed, result.Response));
            }
            catch (ValidationException validationException)
            {
                result.HandleValidationException(validationException);
            }
            catch (MissingEntityException missingEntityException)
            {
                result.HandleMissingEntity(missingEntityException);
            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TDto>(authException);
                foreach (var authorisationPoint in authException.Need)
                    this.Error(() => string.Format("Missing authorisationPoint: {0}", authorisationPoint));
            }
            catch (Exception e)
            {
                result.HandleException<TDto>(e);
            }

            return result;
        }

        private void PrintContextForDebug(ExecutionContext executionContext)
        {
            this.Debug(() => executionContext + "");
        }

        private void PerformExecution<TDto, TId>(TDto dto, HandlerResult<TId> result)
        {
            new HandlerExecution(_container).Execute(dto, result);
        }

        private void PerformValidation<TDto, TId>(TDto dto, HandlerResult<TId> result)
        {
            new ValidatorExecution(_contextProvider, _container).Validate(dto, result);
        }
    }
}