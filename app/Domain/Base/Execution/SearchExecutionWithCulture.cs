using System;
using System.Linq;
using Common.Logging;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace Domain.Base.Execution
{
    public class SearchExecutionWithCulture<TQuery, TEntity, TCriteria, TResult, TDto>
        where TEntity : Entity
        where TQuery : BaseQuery<TEntity>
        where TCriteria: BaseCriteria
    {
        private readonly TQuery query;
        private readonly TCriteria criteria;
        private Action<TQuery> configure = __ => { };
        private Func<IQueryable<TEntity>, TResult> success = __ => default(TResult);
        private static readonly ILog log = LogManager.GetLogger(typeof(SearchExecutionWithCulture<TQuery, TEntity, TCriteria, TResult, TDto>));
        private readonly IProvideContext contextProvider;

        public SearchExecutionWithCulture(TQuery query, TCriteria criteria, IProvideContext contextProvider)
        {
            this.query = query;
            this.criteria = criteria;
            this.contextProvider = contextProvider;
        }

        public SearchExecutionWithCulture<TQuery, TEntity, TCriteria, TResult, TDto> OnSuccess(Func<IQueryable<TEntity>, TResult> onSuccess)
        {
            this.success = onSuccess;
            return this;
        }

        public SearchExecutionWithCulture<TQuery, TEntity, TCriteria, TResult, TDto> Configure(Action<TQuery> configure)
        {
            this.configure = configure ?? (__ => { });
            return this;
        }

        public QueryResult<TResult> Execute()
        {
            var result = new QueryResult<TResult>(default(TResult));

            ValidateCriteria(result);

            if (!result.Completed)
                return result;

            try
            {
                HandleSearchQuery();

                configure(query);

                log.InfoFormat("Executing query {0}", typeof(TQuery));

                var queryResult = query.ExecuteQuery();
                var postExecutionResult = success(queryResult);

                var cultureInfo = contextProvider.Get().CultureInfo;

                //Culture
                if (cultureInfo != null)
                    CultureProcessing<TDto>.ProcessCulture(postExecutionResult, cultureInfo);


                result.Processed(postExecutionResult);

                log.InfoFormat("Completed execution of query {0}. Completed? {1}", typeof(TQuery), result.Completed);

            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TQuery>(authException);
            }
            catch (Exception exception)
            {
                result.HandleException<TQuery>(exception);
            }

            return result;
        }

        private void HandleSearchQuery()
        {
            var searchQuery = query as ISearchQuery<TCriteria>;
            if (searchQuery == null)
                return;

            searchQuery.WithCriteria(criteria);
        }

        private void ValidateCriteria(QueryResult<TResult> result)
        {
            var validationCriteria = criteria as IValidationAvailableDto;

            if (validationCriteria == null)
                return;

            result.AddValidationMessages(validationCriteria.Validate());
        }
    }
}