using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Base.Execution
{
    public class AuthorisationSummary
    {
        private readonly List<AuthorisationFailureMessage> messages = new List<AuthorisationFailureMessage>();

        public IEnumerable<ResponseErrorMessage> DTOMessages
        {
            get { return Messages.Select(m => new ResponseErrorMessage(m.DefaultMessage, m.MessageKey)); }
        }

        public IEnumerable<AuthorisationFailureMessage> Messages
        {
            get { return messages; }
        }

        public bool Passed
        {
            get { return !messages.Any(); }
        }

        public void AddReason(string reason, string messageKey)
        {
            messages.Add(new AuthorisationFailureMessage(reason, messageKey));
        }
    }
}