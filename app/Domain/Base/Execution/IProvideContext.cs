﻿namespace Domain.Base.Execution
{
    public interface IProvideContext
    {
        void SetContext(ExecutionContext context);
        ExecutionContext Get();
    }
}