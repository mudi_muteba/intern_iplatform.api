using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Shared;

namespace Domain.Base.Execution
{
    public class HandlerSummary
    {
        private readonly List<string> messages = new List<string>();

        public IEnumerable<ResponseErrorMessage> DTOMessages
        {
            get
            {
                Func<string, string> createMessage = m => string.Format("An occur occured. The timestamp is {0}", SystemTime.Now());

#if DEBUG
                createMessage = m => m;
#endif

                return Messages.Select(m => new ResponseErrorMessage(createMessage(m), string.Empty));
            }
        }

        public IEnumerable<string> Messages
        {
            get { return messages; }
        }

        public bool Passed
        {
            get { return !messages.Any(); }
        }

        public void AddFailure(string failure)
        {
            messages.Add(failure);
        }
    }
}