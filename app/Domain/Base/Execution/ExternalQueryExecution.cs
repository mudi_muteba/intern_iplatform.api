using System;
using Common.Logging;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Queries;

namespace Domain.Base.Execution
{
    public class ExternalQueryExecution<TQuery, TExternalResponse, TResult>
        where TQuery : BaseExternalSystemQuery<TExternalResponse>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ExternalQueryExecution<TQuery, TExternalResponse, TResult>));
        private readonly TQuery query;
        private Action<TQuery> configure = __ => { };
        private Func<TExternalResponse, TResult> success = __ => default(TResult);

        public ExternalQueryExecution(TQuery query)
        {
            this.query = query;
        }

        public ExternalQueryExecution<TQuery, TExternalResponse, TResult> OnSuccess(Func<TExternalResponse, TResult> onSuccess)
        {
            this.success = onSuccess;
            return this;
        }

        public ExternalQueryExecution<TQuery, TExternalResponse, TResult> Configure(Action<TQuery> configure)
        {
            this.configure = configure ?? (__ => { });
            return this;
        }

        public ExternalQueryResult<TResult> Execute()
        {
            var result = new ExternalQueryResult<TResult>(default(TResult));

            try
            {
                configure(query);

                log.InfoFormat("Executing query {0}", typeof (TQuery));

                var queryResult = query.ExecuteQuery();
                var postExecutionResult = success(queryResult);

                result.Processed(postExecutionResult);

                log.InfoFormat("Completed execution of query {0}. Completed? {1}", typeof (TQuery), result.Completed);
            }
            catch (UnauthorisationException authException)
            {
                result.HandleUnauthorisationException<TQuery>(authException);
            }
            catch (ValidationException validationException)
            {
                result.HandleValidationException(validationException);
            }
            catch (Exception exception)
            {
                result.HandleException<TQuery>(exception);
            }

            return result;
        }
    }
}