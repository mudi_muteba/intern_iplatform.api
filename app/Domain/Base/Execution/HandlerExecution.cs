using System;
using System.Linq;
using Castle.Windsor;
using Common.Logging;
using Domain.Base.Handlers;
using Shared.Extentions;

namespace Domain.Base.Execution
{
    internal class HandlerExecution
    {
        private static readonly ILog Log = LogManager.GetLogger<HandlerExecution>();
        private readonly IWindsorContainer _container;

        public HandlerExecution(IWindsorContainer container)
        {
            _container = container;
        }

        public void Execute<TDto, TId>(TDto dto, HandlerResult<TId> result)
        {
            var handlers = _container.ResolveAll<IHandleDto<TDto, TId>>();

            Log.InfoFormat("Found {0} handler(s) for {1}", handlers.Count(), typeof (TDto));

            HandleMissingHandlers(handlers);

            // if you are here because you have multiple handlers for a DTO,
            // STOP. A DTO should HAVE ONE AND ONLY ONE HANDLER.

            var handlerToExecute = handlers.First();

            Log.InfoFormat("Executing handler {0} (Need: {1})", handlerToExecute.GetType(),
                handlerToExecute.RequiredRights.FlattenListToString(r => r.ToString()));

            ExecuteHandler(dto, result, handlerToExecute);
        }

        private void HandleMissingHandlers<TDto, TId>(IHandleDto<TDto, TId>[] handlers)
        {
            if (handlers.Any())
                return;

            throw new Exception(string.Format("No handlers found for {0}. That will be R10000000000000000", typeof(TDto)));
        }

        private void ExecuteHandler<TDto, TId>(TDto dto, HandlerResult<TId> result, IHandleDto<TDto, TId> handler)
        {
            try
            {
                handler.Handle(dto, result);
            }
            finally
            {
                _container.Release(handler);
            }
        }
    }
}