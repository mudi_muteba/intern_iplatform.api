using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using ValidationMessages;

namespace Domain.Base.Execution
{
    public class ValidationSummary
    {
        private readonly List<ValidationErrorMessage> messages = new List<ValidationErrorMessage>();

        public IEnumerable<ResponseErrorMessage> DTOMessages
        {
            get { return Messages.Select(m => new ResponseErrorMessage(m.DefaultMessage, m.MessageKey)); }
        }

        public IEnumerable<ValidationErrorMessage> Messages
        {
            get { return messages; }
        }

        public bool Passed
        {
            get { return !messages.Any(); }
        }

        public void AddValidationMessage(string propertyName, string defaultMessage, string messageKey)
        {
            messages.Add(new ValidationErrorMessage(propertyName, defaultMessage, messageKey));
        }

        public void AddValidationMessage(ValidationErrorMessage validationErrorMessage)
        {
            messages.Add(validationErrorMessage);
        }
    }
}