using Domain.Admin;
using Newtonsoft.Json;

namespace Domain.Base
{
    public abstract class ChannelEntity : EntityWithAudit
    {
        [JsonIgnore]
        public virtual ChannelReference Channel { get; protected internal set; }
    }
}