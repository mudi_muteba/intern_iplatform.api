using Domain.Users;

namespace Domain.Base
{
    public class EntityUsers
    {
        public virtual User CreatedBy { get; protected internal set; }
        public virtual User ModifiedBy { get; protected internal set; }
    }
}