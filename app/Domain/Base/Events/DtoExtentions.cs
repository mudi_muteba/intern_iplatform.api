﻿using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Events
{
    public static class DtoExtentions
    {
        public static EventAudit CreateAudit(this IExecutionDto dto)
        {
            return new EventAudit(new EventAuditUser(dto.Context));
        }

        public static EventAudit CreateAudit(this ExecutionContext context)
        {
            return new EventAudit(new EventAuditUser(context.UserId, context.Username));
        }
    }
}