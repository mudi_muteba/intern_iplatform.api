﻿using System.Collections.Generic;
using System.Linq;
using Common.Logging;

namespace Domain.Base.Events
{
    public class InMemoryEventPublisher : IPublishEvents
    {
        private readonly ICreateEventHandlers eventHandlersFactory;
        private readonly IPublishEvents nextPublisher;
        private static readonly ILog log = LogManager.GetLogger<InMemoryEventPublisher>();

        public InMemoryEventPublisher(ICreateEventHandlers eventHandlersFactory, IPublishEvents nextPublisher)
        {
            this.eventHandlersFactory = eventHandlersFactory;
            this.nextPublisher = nextPublisher ?? new NullEventPublisher();
        }

        public void Publish(IEnumerable<IDomainEvent> events)
        {
            var expressType = typeof (ExpressDomainEvent);
            var eventsToPublish = events.ToList();

            log.InfoFormat("Publishing express events");

            foreach (var @event in eventsToPublish.Where(expressType.IsInstanceOfType))
            {
                log.InfoFormat("Publishing event '{0}' as an express event", @event.GetType());

                var handlers = eventHandlersFactory.Create(@event);

                Publish(@event, handlers);
            }

            nextPublisher.Publish(eventsToPublish);
        }

        public void Publish(IDomainEvent @event)
        {
            Publish(new List<IDomainEvent>() { @event });
        }

        private void Publish(IDomainEvent @event, IEnumerable<IHandleEvent> handlers)
        {
            foreach (var handler in handlers)
            {
                try
                {
                    handler.Handle(@event);
                }
                finally
                {
                    eventHandlersFactory.Release(handler);
                }
            }
        }
    }
}