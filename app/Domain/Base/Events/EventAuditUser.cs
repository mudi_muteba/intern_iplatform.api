﻿using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Events
{
    public class EventAuditUser
    {
        protected EventAuditUser() { }

        public EventAuditUser(DtoContext context) : this(context.UserId, context.Username) { }

        public EventAuditUser(int userId, string userName)
        {
            UserId = userId;
            UserName = userName;
        }

        public int UserId { get; private set; }
        public string UserName { get; private set; }
    }
}