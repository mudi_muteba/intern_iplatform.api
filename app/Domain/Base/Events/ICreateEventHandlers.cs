﻿using System.Collections.Generic;

namespace Domain.Base.Events
{
    public interface ICreateEventHandlers
    {
        IEnumerable<IHandleEvent> Create<TEvent>(TEvent @event) where TEvent : IDomainEvent;
        void Release(IHandleEvent handler);
    }
}