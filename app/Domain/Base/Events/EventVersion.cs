﻿namespace Domain.Base.Events
{
    public class EventVersion
    {
        public int? Version { get; protected internal set; }

        public EventVersion()
        {
            Version = null;
        }

        public EventVersion(int version)
        {
            Version = version;
        }

        internal bool IsVersioned
        {
            get { return Version.HasValue; }
        }

        public static EventVersion NotVersioned()
        {
            return new EventVersion();
        }
    }
}