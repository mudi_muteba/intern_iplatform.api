﻿using System.Collections.Generic;
using Domain.Admin;

namespace Domain.Base.Events
{
    public interface IDomainEvent
    {
        EventAudit Audit { get; }
        bool IsVersioned { get; }
        List<ChannelReference> ChannelReferences { get; }
    }
}