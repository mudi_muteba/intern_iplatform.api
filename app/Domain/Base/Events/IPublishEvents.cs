﻿using System.Collections.Generic;

namespace Domain.Base.Events
{
    public interface IPublishEvents
    {
        void Publish(IEnumerable<IDomainEvent> events);
        void Publish(IDomainEvent @event);
    }
}