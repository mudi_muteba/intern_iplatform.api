namespace Domain.Base.Events
{
    public interface IProductRelatedDomainEvent : IDomainEvent
    {
        string ProductCode { get; }
        string InsurerCode { get; }
    }
}