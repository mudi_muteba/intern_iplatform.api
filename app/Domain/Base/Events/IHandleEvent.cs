﻿namespace Domain.Base.Events
{
    public interface IHandleEvent
    {
        void Handle(IDomainEvent @event);
    }

    public interface IHandleEvent<in TEvent> : IHandleEvent
    {
        void Handle(TEvent @event);
    }
}