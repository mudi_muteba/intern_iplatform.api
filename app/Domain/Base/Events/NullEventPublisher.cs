﻿using System.Collections.Generic;

namespace Domain.Base.Events
{
    public class NullEventPublisher : IPublishEvents
    {
        public void Publish(IEnumerable<IDomainEvent> events)
        {
            
        }

        public void Publish(IDomainEvent @event)
        {
            
        }
    }
}