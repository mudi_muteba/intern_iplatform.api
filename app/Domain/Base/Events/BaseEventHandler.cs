﻿using System;
using Shared.Extentions;

namespace Domain.Base.Events
{
    public abstract class BaseEventHandler<TEvent> : IHandleEvent<TEvent>
    {
        public abstract void Handle(TEvent @event);

        public void Handle(IDomainEvent @event)
        {
            PrintDebug(@event);

            Handle((TEvent) @event);
        }

        private void PrintDebug(IDomainEvent @event)
        {
            var message = string.Format("Event handler {0} handling {1}{2}", GetType(),
                @event.GetType(),
                Environment.NewLine);

            message = string.Format("{0}{1}{2}", message, @event, Environment.NewLine);

            this.Info(() => message);
        }
    }
}