﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin;

namespace Domain.Base.Events
{
    public abstract class BaseDomainEvent : IDomainEvent
    {
        public EventAudit Audit { get; set; }

        public bool IsVersioned
        {
            get { return Audit != null && Audit.IsVersioned; }
            set { }
        }

        public List<ChannelReference> ChannelReferences { get; private set; }
        public string ChannelIds
        {
            get
            {
                var channels = ChannelReferences != null
                    ? ChannelReferences.Where(x => x.IsDeleted != true).ToList()
                    : Enumerable.Empty<ChannelReference>().ToList();

                return string.Join(",", channels.Select(x => x.Id).ToList());
            }
        }

        protected BaseDomainEvent()
        {
            Audit = new EventAudit();
        }

        protected BaseDomainEvent(EventAudit audit, IEnumerable<ChannelReference> channelReferences)
        {
            Audit = audit;
            ChannelReferences = channelReferences.ToList();
        }

        protected BaseDomainEvent(EventAudit audit, ChannelReference channelReference)
        {
            Audit = audit;
            ChannelReferences = new List<ChannelReference> { channelReference };
        }
    }
}