﻿using System;

namespace Domain.Base.Events
{
    public class EventAudit
    {
        public EventAudit()
        {
            Versioning = EventVersion.NotVersioned();
        }

        public EventAudit(EventAuditUser user) : this()
        {
            User = user;
            Date = DateTime.UtcNow;
        }

        public EventAuditUser User { get; protected internal set; }
        public DateTime Date { get; protected internal set; }
        public EventVersion Versioning { get; protected internal set; }

        internal bool IsVersioned
        {
            get { return Versioning.IsVersioned; }
        }

        internal void Version(int versionNumber)
        {
            Versioning = new EventVersion(versionNumber);
        }
    }
}