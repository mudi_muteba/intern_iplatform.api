﻿using Domain.Base.Execution;

namespace Domain.Base.Validation
{
    public interface IValidateDto<in TDto>
    {
        void Validate(TDto dto, ExecutionResult result);
    }
}