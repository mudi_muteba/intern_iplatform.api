﻿
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base;

namespace Domain.Base.Validation
{
    public class ChannelValidation
    {
        private readonly ExecutionContext context;

        public ChannelValidation(ExecutionContext context)
        {
            this.context = context;
        }

        public void Validate(IChannelAwareDto dto, ExecutionResult result)
        {
            if (dto.ChannelId == 0)
            {
                result.AddValidationMessage("ChannelId", "No channel provided", "NO_CHANNEL_ID");
                return;
            }

            if (context.Channels.Contains(dto.ChannelId))
                return;

            result.AddValidationMessage("ChannelId", "Provided channel ID not allocated to caller", "CHANNEL_ID_NOT_ALLOWED");
        }
    }
}