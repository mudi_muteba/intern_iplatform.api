﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Events;
using Infrastructure.Configuration;
using System;

namespace Domain.Base
{
    public class Audit
    {
        public virtual List<IDomainEvent> Events { get; private set; }
        public virtual int CurrentVersion { get; private set; }

        public Audit()
        {
            Events = new List<IDomainEvent>();
        }

        public Audit(Audit audit)
        {
            CurrentVersion = audit.CurrentVersion;
            Events = new List<IDomainEvent>();
            Events.AddRange(audit.Events);
        }

        public void RaiseEvent(IDomainEvent domainEvent)
        {
            RemoveEvent(domainEvent);
            Events.Add(domainEvent);
        }

        internal void EventsPublished()
        {
            if (!Events.Any()) return;
            foreach (var @event in Events.Where(e => e.Audit != null && !e.Audit.IsVersioned).OrderBy(e => e.Audit.Date))
                @event.Audit.Version(++CurrentVersion);
        }

        private void RemoveEvent(IDomainEvent domainEvent)
        {
            var eventName = domainEvent.GetType().Name;
            var configCount = string.IsNullOrEmpty(ConfigurationReader.Appsetting.Read(eventName, false))
                ? 0
                : Convert.ToInt32(ConfigurationReader.Appsetting.Read(eventName, false));

            var availableEventCount = Events.Count(x => x.GetType().Name == eventName);
            var removeEventCount = configCount > 0 ? (availableEventCount - configCount + 1) : 0;
            while (removeEventCount > 0)
            {
                var item = Events.FirstOrDefault(x => x.GetType().Name == eventName);
                Events.Remove(item);
                removeEventCount--;
            }
        }
    }
}