using System;
using Shared;

namespace Domain.Base
{
    public class EntityTimestamps
    {
        public virtual DateTime CreatedOn { get; protected internal set; }
        public virtual DateTime ModifiedOn { get; protected internal set; }

        public EntityTimestamps()
        {
            CreatedOn = SystemTime.Now();
            Updated();
        }

        internal virtual void Updated()
        {
            ModifiedOn = SystemTime.Now();
        }
    }
}