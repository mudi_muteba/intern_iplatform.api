using System.Collections.Generic;
using System.Linq;
using Domain.Base.Events;
using Infrastructure.NHibernate.Attributes;

namespace Domain.Base
{
    public abstract class EntityWithAudit : Entity
    {
        protected Audit audit = new Audit();

        protected EntityWithAudit()
        {
            Audit = new Audit();
        }

        [DoNotMap]
        public virtual Audit Audit
        {
            get { return audit; }
            protected internal set { audit = value; }
        }

        public virtual void EventsPublished()
        {
            audit.EventsPublished();
        }

        public virtual IEnumerable<IDomainEvent> NewEvents()
        {
            return Audit.Events.Where(e => !e.IsVersioned).Union(NewContainedEvents());
        }

        protected virtual IEnumerable<IDomainEvent> NewContainedEvents()
        {
            return new List<IDomainEvent>();
        }

        public virtual void RaiseEvent(IDomainEvent domainEvent)
        {
            Audit.RaiseEvent(domainEvent);
        }
    }
}