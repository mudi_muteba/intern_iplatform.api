using System;

namespace Domain.Base
{
    public class EntityAudit
    {
        public EntityAudit()
        {
            Users = new EntityUsers();
            Timestamps = new EntityTimestamps();
        }

        public virtual EntityUsers Users { get; protected internal set; }
        public virtual EntityTimestamps Timestamps { get; protected internal set; }

        public virtual DateTime CreatedOn
        {
            get { return Timestamps.CreatedOn; }
            protected internal set { Timestamps.CreatedOn = value; }
        }

        public virtual DateTime ModifiedOn
        {
            get { return Timestamps.ModifiedOn; }
            protected internal set { Timestamps.ModifiedOn = value; }
        }
    }
}