using System;

namespace Domain.Base
{
    public abstract class EntityWithGuid : Entity
    {
        public virtual Guid SystemId { get; protected internal set; }
    }
}