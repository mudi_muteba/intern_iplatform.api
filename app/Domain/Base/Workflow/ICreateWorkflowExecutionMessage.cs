﻿using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public interface ICreateWorkflowExecutionMessage
    {
        IWorkflowExecutionMessage Create(object command);
    }

    public interface ICreateWorkflowExecutionMessage<in T> : ICreateWorkflowExecutionMessage
    {
        IWorkflowExecutionMessage Create(T command);
    }
}