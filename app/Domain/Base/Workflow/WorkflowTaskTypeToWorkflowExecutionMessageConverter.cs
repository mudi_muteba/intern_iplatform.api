﻿using System;
using System.Linq;
using AutoMapper;
using Common.Logging;
using iPlatform.Enums.Workflows;
using Workflow.Messages;

namespace Domain.Base.Workflow
{
    /// <summary>
    /// Finds & instantiates a single WorkflowExecutionMessage decorated with the specific WorkflowTaskType enum.
    /// </summary>
    public class WorkflowTaskTypeToWorkflowExecutionMessageConverter : TypeConverter<WorkflowMessageType, WorkflowExecutionMessage>
    {
        private readonly ILog _log;

        public WorkflowTaskTypeToWorkflowExecutionMessageConverter()
        {
            _log = LogManager.GetLogger(GetType());
        }

        protected override WorkflowExecutionMessage ConvertCore(WorkflowMessageType workflowTaskType)
        {
            Type workflowExecutionMessageType;
            try
            {
                workflowExecutionMessageType = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                                                    from type in assembly.GetTypes()
                                                    from a in type.GetCustomAttributes(typeof(WorkflowExecutionMessageTypeAttribute), false)
                                                    where ((WorkflowExecutionMessageTypeAttribute)a).WorkflowTaskType == workflowTaskType
                                                    select type).FirstOrDefault();
            }
            catch 
            {
                workflowExecutionMessageType = (from type in typeof(DomainMarker).Assembly.GetTypes()
                                                     from a in type.GetCustomAttributes(typeof(WorkflowExecutionMessageTypeAttribute), false)
                                                     where ((WorkflowExecutionMessageTypeAttribute)a).WorkflowTaskType == workflowTaskType
                                                     select type).FirstOrDefault();
            }
            
            if (workflowExecutionMessageType == null)
            {
                _log.WarnFormat("No WorkflowExecutionMessage could be found for WorkflowTaskType {0}", workflowTaskType.ToString());
                return null;
            }

            var workflowExecutionMessage = Activator.CreateInstance(workflowExecutionMessageType) as WorkflowExecutionMessage;
            if (workflowExecutionMessage == null)
                _log.WarnFormat("WorkflowExecutionMessage could not be instantiated for WorkflowTaskType {0}", workflowTaskType.ToString());

            return workflowExecutionMessage;
        }
    }
}