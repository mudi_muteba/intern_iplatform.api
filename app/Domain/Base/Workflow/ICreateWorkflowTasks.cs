﻿using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public interface ICreateWorkflowTasks
    {
        IWorkflowRoutingMessage Create(object command);
    }

    public interface ICreateWorkflowTasks<in T> : ICreateWorkflowTasks
    {
        IWorkflowRoutingMessage Create(T command);
    }
}