﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base.Events;
using Shared.Extentions;
using Shared.Retry;
using Workflow.Messages;
using Domain.Admin.ChannelEvents;
using log4net;

namespace Domain.Base.Workflow
{
    public class EventWorkflowMessagePublisher : IPublishEventWorkflowMessages, IPublishEvents
    {
        private static readonly ILog _Log = LogManager.GetLogger(typeof(EventWorkflowMessagePublisher));
        private readonly IWorkflowRouter _router;

        public EventWorkflowMessagePublisher(IWorkflowRouter router)
        {
            _router = router;
        }

        public void Publish(IDomainEvent @event)
        {
            Publish(new[] { @event });
        }

        public void Publish(IEnumerable<IDomainEvent> events)
        {
            Publish(events.ToArray());
        }

        public void Publish(params IDomainEvent[] events)
        {
            this.Info(() => "Publishing events with custom workflow configuration");

            foreach (var @event in events)
            {
                this.Info(() => string.Format("Publishing event '{0}' as with custom workflow configuration", @event.GetType()));
                var workflowRoutingMessage = new WorkflowRoutingMessage();
                var workflowtaskArray = GetWorkflowTasks(@event);

                try
                {
                    workflowtaskArray = workflowtaskArray.ToArray();
                }
                catch (Exception ex)
                {
                    workflowtaskArray = null;
                    _Log.Error(String.Format("An error occurred whilst trying to copy elements from IEnumerable<IWorkflowExecutionMessage> to array. Message: {0}", ex.Message), ex);
                }
                

                if (workflowtaskArray != null)
                {
                    workflowRoutingMessage.AddMessage(workflowtaskArray.ToArray());
                    Publish(workflowRoutingMessage);
                }
            }
        }

        internal void Publish(WorkflowRoutingMessage workflowRoutingMessage)
        {
            if (!workflowRoutingMessage.ExecutionPlan.ExecutionMessages.Any())
                return;

            this.Info(() => string.Format("Publishing to workflow with {0} tasks", workflowRoutingMessage.ExecutionPlan.ExecutionMessages.Count()));

            Exception exception = null;
            var published = false;

            RepeatAction
                .While(() => !published)
                .UpTo(3.Times())
                .RetryAfter(1.Seconds())
                .Do(() =>
                {
                    try
                    {
                        _router.Publish(workflowRoutingMessage);
                        published = true;
                    }
                    catch (Exception e)
                    {
                        exception = e;
                    }
                });

            if (!published)
            {
                this.Error(() => "Failed to publish message to the workflow");
                throw exception;
            }
        }

        internal IEnumerable<IWorkflowExecutionMessage> GetWorkflowTasks(IDomainEvent @event)
        {
            var list = new List<IWorkflowExecutionMessage>();
            if (@event.ChannelReferences == null) return list;

            foreach (var channelReference in @event.ChannelReferences)
            {
                var channelRef = channelReference as Channel;
                if (channelRef == null)
                {
                    this.Warn(() => string.Format("Channel used to resolve channel configuration is not valid. The type passed is '{0}' but was hoping for '{1}'",
                        channelReference.GetType().FullName, typeof(ChannelReference).FullName));
                    continue;
                }

                var channelEvent = GetChannelConfiguration(@event, channelRef);
                var eventType = @event.GetType();
                if (channelEvent == null || channelEvent.Tasks == null|| !channelEvent.Tasks.Any())
                {
                    this.Warn(() => string.Format("No channel configuration found for event '{0}' for channel '{1}'", eventType.FullName, channelRef.Id));
                    continue;
                }

                list.AddRange(channelEvent.TaskNames.GetWorkflowMessages(@event));
            }

            return list;
        }

        internal ChannelEvent GetChannelConfiguration(IDomainEvent @event, ChannelReference channelReference)
        {
            Func<ChannelEvent, bool> criteria = ce => ce.EventName == @event.GetType().Name;

            var productEvent = @event as IProductRelatedDomainEvent;
            if (productEvent != null)
                criteria = ce => ce.EventName == @event.GetType().Name && ce.ProductCode == productEvent.ProductCode;

            return channelReference.ChannelConfigurations.FirstOrDefault(x => criteria(x));
        }
    }
}