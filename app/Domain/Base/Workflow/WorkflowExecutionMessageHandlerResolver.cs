﻿using Castle.Windsor;
using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public class WorkflowExecutionMessageHandlerResolver : ICreateWorkflowExecutionMessage
    {
        private readonly IWindsorContainer _container;

        public WorkflowExecutionMessageHandlerResolver(IWindsorContainer container)
        {
            _container = container;
        }

        public IWorkflowExecutionMessage Create(object command)
        {
            var type = command.GetType();
            var executorType = typeof (ICreateWorkflowExecutionMessage<>).MakeGenericType(type);
            var executor = (ICreateWorkflowExecutionMessage) _container.Resolve(executorType);

            return ExecuteHandler(command, executor);
        }

        private IWorkflowExecutionMessage ExecuteHandler(object message, ICreateWorkflowExecutionMessage executor)
        {
            try
            {
                return executor.Create(message);
            }
            finally
            {
                _container.Release(executor);
            }
        }
    }
}