﻿using Workflow.Messages.Plan.Tasks;

namespace Domain.Base.Workflow
{
    public interface ICreateTaskMetadata
    {
        ITaskMetadata Create(object command);
    }

    public interface ICreateTaskMetdata<in T> : ICreateTaskMetadata
    {
        ITaskMetadata Create(T command);
    }
}
