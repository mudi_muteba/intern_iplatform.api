﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Common.Logging;
using Shared;

namespace Domain.Base.Workflow
{
    internal class TaskCollection
    {
        private static readonly ILog log = LogManager.GetLogger<TaskCollection>();

        public static IList<Type> Types { get; set; }

        static TaskCollection()
        {
            var baseType = typeof (IWorkflowFactoryKey);
            Types = new List<Type>();

            try
            {
                var filteredAssemblies = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(a => a.GetTypes())
                    .Where(t => baseType.IsAssignableFrom(t));

                foreach (var assemblyTypes in filteredAssemblies)
                {
                    Types.Add(assemblyTypes);
                }
            }
            catch (ReflectionTypeLoadException loadException)
            {
                var loaderExceptions = loadException.LoaderExceptions;

                foreach (var loaderException in loaderExceptions)
                {
                    log.ErrorFormat(new ExceptionPrettyPrinter().Print(loaderException));
                }
            }
            catch (Exception e)
            {
                log.ErrorFormat(new ExceptionPrettyPrinter().Print(e));
            }
        }

        public Type this[string name]
        {
            get { return Types.FirstOrDefault(x => x.Name == name); }
        }
    }
}