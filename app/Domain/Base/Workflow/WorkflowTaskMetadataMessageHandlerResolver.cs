﻿using Castle.Windsor;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Base.Workflow
{
    public class WorkflowTaskMetadataMessageHandlerResolver : ICreateTaskMetadata
    {
        private readonly IWindsorContainer _container;

        public WorkflowTaskMetadataMessageHandlerResolver(IWindsorContainer container)
        {
            _container = container;
        }
        public ITaskMetadata Create(object message)
        {
            var type = message.GetType();
            var executoryType = typeof (ICreateTaskMetadata).MakeGenericType(type);
            var executor = (ICreateTaskMetadata) _container.Resolve(executoryType);
            return ExecuteHandler(message, executor);
        }

        private ITaskMetadata ExecuteHandler(object message, ICreateTaskMetadata executor)
        {
            try
            {
                return executor.Create(message);
            }
            finally
            {
                _container.Release(executor);
            }
        }
    }
}
