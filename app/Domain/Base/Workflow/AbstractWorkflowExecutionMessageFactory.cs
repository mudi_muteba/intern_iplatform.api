﻿using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public abstract class AbstractWorkflowExecutionMessageFactory<T> : ICreateWorkflowExecutionMessage<T>
    {
        public abstract IWorkflowExecutionMessage Create(T command);

        public IWorkflowExecutionMessage Create(object command)
        {
            return Create((T) command);
        }
    }
}