﻿using System;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.Base.Workflow
{
    [DataContract]
    public class RoutePolicyBindingTask : IWorkflowFactoryKey
    {
        public RoutePolicyBindingTask(PolicyBindingRequestDto dto, string productCode, string insurerCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            Dto = dto;
            ProductCode = productCode;
            InsurerCode = insurerCode;
            ChannelId = channelId;
            SettingsQuoteUploads = settingsQuoteUploads;
        }
        
        [DataMember] public readonly PolicyBindingRequestDto Dto;
        [DataMember] public readonly string ProductCode;
        [DataMember] public readonly string InsurerCode;
        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly List<SettingsQuoteUploadDto> SettingsQuoteUploads;

    }
}