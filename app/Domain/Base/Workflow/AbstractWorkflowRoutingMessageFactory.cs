﻿using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public abstract class AbstractWorkflowRoutingMessageFactory<T> : ICreateWorkflowRoutingMessage<T>
    {
        public abstract IWorkflowRoutingMessage Create(T command);

        public IWorkflowRoutingMessage Create(object command)
        {
            return Create((T) command);
        }
    }
}