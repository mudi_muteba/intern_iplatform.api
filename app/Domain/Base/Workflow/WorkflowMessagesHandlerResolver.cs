﻿using Castle.Windsor;
using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public class WorkflowTasksHandlerResolver : ICreateWorkflowTasks
    {
        private readonly IWindsorContainer _container;

        public WorkflowTasksHandlerResolver(IWindsorContainer container)
        {
            _container = container;
        }

        public IWorkflowRoutingMessage Create(object message)
        {
            var type = message.GetType();
            var executorType = typeof(ICreateWorkflowTasks<>).MakeGenericType(type);
            var executor = (ICreateWorkflowTasks)_container.Resolve(executorType);

            return ExecuteHandler(message, executor);
        }

        private IWorkflowRoutingMessage ExecuteHandler(object message, ICreateWorkflowTasks executor)
        {
            try
            {
                return executor.Create(message);
            }
            finally
            {
                _container.Release(executor);
            }
        }
    }
}