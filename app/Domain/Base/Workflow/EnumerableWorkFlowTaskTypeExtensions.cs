﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Ratings.Events;
using iPlatform.Enums.Workflows;
using Shared.Extentions;
using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public static class EnumerableWorkFlowTaskTypeExtensions
    {
        public static IEnumerable<IWorkflowExecutionMessage> GetWorkflowMessages(this IEnumerable<WorkflowMessageType> workflowTaskTypes, object sourceObjectToMapOntoEachWorkflowMessage)
        {
            return workflowTaskTypes.Select(workflowTaskType => workflowTaskType.GetWorkflowMessage(sourceObjectToMapOntoEachWorkflowMessage));
        }

        public static IWorkflowExecutionMessage GetWorkflowMessage(this WorkflowMessageType workflowTaskType, object sourceObjectToMapOntoEachWorkflowMessage)
        {
            var obj = sourceObjectToMapOntoEachWorkflowMessage as ExternalQuoteAcceptedWithUnderwritingEnabledEvent;
            if (obj!= null)
            {
                typeof(EnumerableWorkFlowTaskTypeExtensions).Info(() => string.Format("Finding WorkflowMessage for Insurer: '{0}' and Product: '{1}'", obj.InsurerCode, obj.ProductCode));
            }
                

            var policyBindingObject = sourceObjectToMapOntoEachWorkflowMessage as SubmitPolicyBindingEvent;
            if (policyBindingObject != null)
            {
                typeof(EnumerableWorkFlowTaskTypeExtensions).Info(() => string.Format("Finding Policy Binding WorkflowMessage for Insurer: '{0}' and Product: '{1}'", policyBindingObject.InsurerCode, policyBindingObject.ProductCode));
            }
                
            // Create empty WorkflowExecutionMessage implementation for specific WorkflowTaskType
            var workflowExecutionMessage = Mapper.Map<WorkflowMessageType, WorkflowExecutionMessage>(workflowTaskType);
            if (workflowExecutionMessage == null)
            {
                typeof (EnumerableWorkFlowTaskTypeExtensions).Error(() => string.Format("No WorkflowExecutionMessage found, there is no WorkflowExecutionMessage class decorated with {0} {1}", typeof(WorkflowMessageType), workflowTaskType));
                return null;
            }

            var sourceType = sourceObjectToMapOntoEachWorkflowMessage.GetType();
            var destinationType = workflowExecutionMessage.GetType();
            var canMap = Mapper.FindTypeMapFor(sourceType, destinationType) != null;
            if (!canMap)
            {
                typeof(EnumerableWorkFlowTaskTypeExtensions).Info(() => string.Format("No AutoMapper mapping found for '{0}' and WorkflowExecutionMessage '{1}'", sourceType.FullName, destinationType.FullName));
                return null;
            }

            typeof(EnumerableWorkFlowTaskTypeExtensions).Info(() => string.Format("Handing '{0}' with WorkflowExecutionMessage '{1}'", sourceType.FullName, destinationType.FullName));

            // Populate empty WorkflowExecutionMessage implementation with source object data
            return Mapper.Map(sourceObjectToMapOntoEachWorkflowMessage, workflowExecutionMessage, sourceType, destinationType) as WorkflowExecutionMessage;
        }
    }
}