﻿namespace Domain.Base.Workflow
{
    public class SendEmailWorkflowTask : IWorkflowFactoryKey
    {
        public string UserName { get; set; }
    }
}