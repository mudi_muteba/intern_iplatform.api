using Domain.Base.Events;

namespace Domain.Base.Workflow
{
    public interface IPublishEventWorkflowMessages
    {
        void Publish(params IDomainEvent[] events);
    }
}