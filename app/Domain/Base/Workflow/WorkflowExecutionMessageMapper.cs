﻿using AutoMapper;
using iPlatform.Enums.Workflows;
using Workflow.Messages;

namespace Domain.Base.Workflow
{
    /// <summary>
    /// Finds & instantiates a single WorkflowExecutionMessage decorated with the specific WorkflowTaskType enum.
    /// Implemented in the WorkflowTaskTypeToWorkflowExecutionMessageConverter AutoMapper converter.
    /// </summary>
    public class WorkflowExecutionMessageMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<WorkflowMessageType, WorkflowExecutionMessage>()
                .ConvertUsing<ITypeConverter<WorkflowMessageType, WorkflowExecutionMessage>>();
        }
    }
}