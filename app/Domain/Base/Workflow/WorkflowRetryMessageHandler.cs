﻿using System;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.WorkflowRetries;
using NHibernate.Util;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public class WorkflowRetryMessageHandler : IWorkflowRetryMessageHandler
    {
        private static readonly ILog Log = LogManager.GetLogger<WorkflowRetryMessageHandler>();
        private readonly IConnector _connector;
        private readonly IWorkflowRouter _router;

        public WorkflowRetryMessageHandler(IWorkflowRouter router, IConnector connector)
        {
            _router = router;
            _connector = connector;
        }

        public void Retry(IWorkflowRetryMessage message)
        {
            if (message.RetryLimit < 1) message.RetryLimit = 3;
            if (!message.MessageDelay.HasValue && !message.FuturePublishDate.HasValue) message.MessageDelay = TimeSpan.FromSeconds(0);
            if (message.RetryCount == message.RetryLimit)
                return;

            message.RetryCount++;

            Log.InfoFormat("Retry attempt {0} of {1} ID: {2} for message: {3}", message.RetryCount, message.RetryLimit, message.CorrelationId, message);

            var retries = _connector.WorkflowRetryLogManagement.WorkflowRetryLogs.Search(new SearchWorkflowRetryLogDto(message.CorrelationId, message.RetryCount));
            if (retries.Response.Results.Any())
            {
                var error = string.Format("Retry already attempted {0} of {1} ID: {2} for message: {3}", message.RetryCount, message.RetryLimit, message.CorrelationId, message);
                Log.ErrorFormat(error);
                throw new Exception(error);
            }

            var created = _connector.WorkflowRetryLogManagement.WorkflowRetryLogs.Create(new CreateWorkflowRetryLogDto(message.CorrelationId, message.RetryCount, message.RetryLimit, DateTime.UtcNow, message.MessageDelay + "", message.FuturePublishDate, message));
            if (created.Response < 1)
            {
                var error = string.Format("Failed recording retry attempt {0} of {1} ID: {2} for message: {3}", message.RetryCount, message.RetryLimit, message.CorrelationId, message);
                Log.ErrorFormat(error);
                throw new Exception(error);
            }

            var routingMessage = new WorkflowRoutingMessage().AddMessage(message);
            if (message.MessageDelay.HasValue)
                _router.PublishDelayed(routingMessage, message.MessageDelay.Value);
            if (message.FuturePublishDate.HasValue)
                _router.PublishDelayed(routingMessage, message.FuturePublishDate.Value);
        }
    }
}