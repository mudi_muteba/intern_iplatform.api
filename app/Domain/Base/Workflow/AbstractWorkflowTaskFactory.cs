﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Base.Workflow
{
    public abstract class AbstractWorkflowTaskFactory<T> : ICreateWorkflowTasks<T>
    {
        public abstract IWorkflowRoutingMessage Create(T command);

        public IWorkflowRoutingMessage Create(object command)
        {
            return Create((T)command);
        }
    }
}