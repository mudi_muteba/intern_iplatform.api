﻿using Castle.Windsor;
using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public class WorkflowRoutingMessageHandlerResolver : ICreateWorkflowRoutingMessage
    {
        private readonly IWindsorContainer _container;

        public WorkflowRoutingMessageHandlerResolver(IWindsorContainer container)
        {
            _container = container;
        }

        public IWorkflowRoutingMessage Create(object command)
        {
            var type = command.GetType();
            var executorType = typeof (ICreateWorkflowRoutingMessage<>).MakeGenericType(type);
            var executor = (ICreateWorkflowRoutingMessage) _container.Resolve(executorType);

            return ExecuteHandler(command, executor);
        }

        private IWorkflowRoutingMessage ExecuteHandler(object message, ICreateWorkflowRoutingMessage executor)
        {
            try
            {
                return executor.Create(message);
            }
            finally
            {
                _container.Release(executor);
            }
        }
    }
}