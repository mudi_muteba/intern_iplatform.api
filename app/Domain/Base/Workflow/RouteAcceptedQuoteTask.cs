﻿using System;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System.Collections.Generic;

namespace Domain.Base.Workflow
{
    [DataContract]
    public class RouteAcceptedQuoteTask : IWorkflowFactoryKey
    {
        public RouteAcceptedQuoteTask(PublishQuoteUploadMessageDto dto, string productCode, string insurerCode, Guid channelId, List<SettingsQuoteUploadDto> settingsQuoteUploads)
        {
            Dto = dto;
            ProductCode = productCode;
            InsurerCode = insurerCode;
            ChannelId = channelId;
            SettingsQuoteUploads = settingsQuoteUploads;
        }
        
        [DataMember] public readonly PublishQuoteUploadMessageDto Dto;
        [DataMember] public readonly string ProductCode;
        [DataMember] public readonly string InsurerCode;
        [DataMember] public readonly Guid ChannelId;
        [DataMember] public readonly List<SettingsQuoteUploadDto> SettingsQuoteUploads;

    }
}