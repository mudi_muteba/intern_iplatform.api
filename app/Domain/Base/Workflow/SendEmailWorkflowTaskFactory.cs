﻿using Workflow.Messages;
using Workflow.Messages.Mail;

namespace Domain.Base.Workflow
{
    public class SendEmailWorkflowTaskFactory : AbstractWorkflowTaskFactory<SendEmailWorkflowTask>
    {
        public override IWorkflowRoutingMessage Create(SendEmailWorkflowTask command)
        {
            var emailCommunicationMessage = new EmailCommunicationMessage("ResetPasswordTemplate",
                new CommunicationMessageData(""), command.UserName, "Password reset");
            return new WorkflowRoutingMessage().AddMessage(new EmailMessage(new EmailTaskMetaData(emailCommunicationMessage)));
        }
    }
}