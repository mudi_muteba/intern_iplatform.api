﻿using Workflow.Messages;

namespace Domain.Base.Workflow
{
    public interface ICreateWorkflowRoutingMessage
    {
        IWorkflowRoutingMessage Create(object command);
    }

    public interface ICreateWorkflowRoutingMessage<in T> : ICreateWorkflowRoutingMessage
    {
        IWorkflowRoutingMessage Create(T command);
    }
}
