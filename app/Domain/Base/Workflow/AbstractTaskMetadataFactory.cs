﻿using Workflow.Messages.Plan.Tasks;

namespace Domain.Base.Workflow
{
    public abstract class AbstractTaskMetadataFactory<T> : ICreateTaskMetdata<T>
    {
        public abstract ITaskMetadata Create(T command);
        public ITaskMetadata Create(object command)
        {
            return Create((T)command);
        }
    }
}
