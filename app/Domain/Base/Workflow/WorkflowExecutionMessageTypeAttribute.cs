﻿using System;
using iPlatform.Enums.Workflows;

namespace Domain.Base.Workflow
{
    [AttributeUsage(AttributeTargets.Class)]
    public class WorkflowExecutionMessageTypeAttribute : Attribute
    {
        public WorkflowExecutionMessageTypeAttribute(WorkflowMessageType workflowTaskType)
        {
            WorkflowTaskType = workflowTaskType;
        }

        public WorkflowMessageType WorkflowTaskType { get; set; }
    }
}