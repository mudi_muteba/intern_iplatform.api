﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Base.Overrides
{
    public class EntityOverride : IAutoMappingOverride<Entity>
    {
        public void Override(AutoMapping<Entity> mapping)
        {
            mapping.Map(x => x.IsDeleted).Default("0");
        }
    }
}