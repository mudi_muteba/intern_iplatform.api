using System;
using Domain.Base;

namespace Domain.SalesForce
{
    public class SalesForceExternalLog : Entity
    {
        public SalesForceExternalLog()
        {
        }

        public SalesForceExternalLog(string externalReference, string jsonData, string response, bool isSuccess)
        {
            ExternalReference = externalReference;
            JsonData = jsonData;
            Response = response;
            IsSuccess = isSuccess;
            IsDeleted = false;
            CreatedOn = DateTime.UtcNow;
            ModifiedOn = DateTime.UtcNow;
        }

        public virtual string ExternalReference { get; set; }
        public virtual DateTime ModifiedOn { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual string JsonData { get; set; }
        public virtual string Response { get; set; }
        public virtual bool IsSuccess { get; set; }


    }
}