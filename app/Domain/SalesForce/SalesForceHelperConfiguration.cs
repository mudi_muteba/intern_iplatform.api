﻿namespace Domain.SalesForce
{
    public class SalesForceHelperConfiguration
    {
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SecurityKey { get; set; }
        public string AccessToken { get; set; }
        public string AuthServiceUrl { get; set; }
    }
}
