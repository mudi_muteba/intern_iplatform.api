﻿using AutoMapper;
using iPlatform.Api.DTOs.SalesForce;

namespace Domain.SalesForce.Mappings
{
    public class SalesForceMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SalesForceEntry, SalesForceEntryDto>();
        }
    }
}