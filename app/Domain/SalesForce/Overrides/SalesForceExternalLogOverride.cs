﻿using Domain.JsonDataStores;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using iPlatform.Api.DTOs.SalesForce;

namespace Domain.SalesForce.Overrides
{
    public class SalesForceExternalLogOverride : IAutoMappingOverride<SalesForceExternalLog>
    {
        public void Override(AutoMapping<SalesForceExternalLog> mapping)
        {
            mapping.Map(x => x.ExternalReference).Length(10000);
            mapping.Map(x => x.Response).Length(10000);
            mapping.Map(x => x.JsonData).Length(10000);
        }
    }
}