using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.SalesForce.Overrides
{
    public class SalesForceIntegrationLogOverride : IAutoMappingOverride<SalesForceIntegrationLog>
    {
        public void Override(AutoMapping<SalesForceIntegrationLog> mapping)
        {
            mapping.Map(x => x.Response).Length(10000);
            mapping.Map(x => x.JsonData).Length(10000);
        }
    }
}