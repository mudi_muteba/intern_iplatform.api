﻿using System;
using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Individuals;
using Domain.Party.Quotes;
using Newtonsoft.Json;

namespace Domain.SalesForce.Events
{
    public class SalesForceEntryEvent : BaseDomainEvent, ExpressDomainEvent
    {

        [JsonIgnore]
        public Individual Individual { get; private set; }
        public Quote Quote { get; private set; }
        public int UserId { get; private set; }
        public int ProposalHeaderId { get; private set; }
        public string QuoteStatus { get; private set; }
        public string Cmpid { get; private set; }
        public string ContactOnlyIntendedProduct { get; set; }

        public SalesForceEntryEvent(string quoteStatus, Individual individual, string cmpid, Quote quote, string contactOnlyIntendedProduct = "", int proposalHeaderId = 0)
        {
            ProposalHeaderId = proposalHeaderId;
            Cmpid = cmpid ?? String.Empty;
            Individual = individual;
            Quote = quote;
            QuoteStatus = quoteStatus;
            ContactOnlyIntendedProduct = contactOnlyIntendedProduct;
        }
    }
}