﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base;
using Domain.Users;

namespace Domain.SalesForce
{

    public class SalesForceIntegrationLog : Entity
    {
        public SalesForceIntegrationLog()
        {
        }

        public SalesForceIntegrationLog(int partyId, string jsonData, string response, bool isSuccess,string status)
        {
            PartyId = partyId;
            JsonData = jsonData;
            Response = response;
            IsSuccess = isSuccess;
            IsDeleted = false;
            Status = status;
            CreatedOn = DateTime.UtcNow;
            ModifiedOn = DateTime.UtcNow;
        }

        public virtual int PartyId { get; set; }
        public virtual DateTime ModifiedOn { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual string JsonData { get; set; }
        public virtual string Response { get; set; }
        public virtual string Status { get; set; }
        public virtual bool IsSuccess { get; set; }


    }
}
