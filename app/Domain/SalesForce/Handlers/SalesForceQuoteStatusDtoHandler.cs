using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Party.ProposalHeaders;
using Domain.Party.Quotes;
using Domain.SalesForce.Events;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Quotes;
using iPlatform.Api.DTOs.SalesForce;
using MasterData.Authorisation;

namespace Domain.SalesForce.Handlers
{
    /// <summary>
    /// This is used to update a lead in SF with the status "Declined", "UW Referral", "Intent to Buy"
    /// </summary>
    public class SalesForceQuoteStatusDtoHandler : ExistingEntityDtoHandler<Individual, SalesForceQuoteStatusDto, int>
    {
        private readonly IProvideContext _contextProvider;
        private readonly IRepository _repository;

        public SalesForceQuoteStatusDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _contextProvider = contextProvider;
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void InternalHandle(SalesForceQuoteStatusDto dto, HandlerResult<int> result)
        {
            var entity = _repository.GetById<Individual>(dto.PartyId);
            if (entity == null)
                throw new MissingEntityException(typeof(Individual), dto.Id);

            HandleExistingEntityChange(entity, dto, result);

            repository.Save(entity);
        }

        protected override void HandleExistingEntityChange(Individual entity, SalesForceQuoteStatusDto dto,
            HandlerResult<int> result)
        {
            var source = string.Empty;
            if (_contextProvider.Get().RequestHeaders.ContainsKey("source"))
                source = _contextProvider.Get().RequestHeaders["source"].FirstOrDefault();

            if (!source.Equals("aig-widget")) return;

            //Check allowed statuses
            var allowedStatuses = new List<string>() { "Declined", "UW Referral", "Intent to Buy" };
            if (!allowedStatuses.Contains(dto.QuoteStatus))
            {
                result.AddError(new ResponseErrorMessage(String.Format("Invalid Status: {0}", dto.QuoteStatus), "SALES_FORCE_ERROR"));
                return;
            }


            var quote = _repository.GetAll<Quote>().FirstOrDefault(a => a.Id == dto.QuoteId);
            SalesForceEntryEventHandler.GetQuoteInfo(quote, dto);

            var proposalHeader = _repository.GetAll<ProposalHeader>().FirstOrDefault(a => a.Id == dto.ProposalHeaderId);
            string cmpid = String.Empty;
            if (proposalHeader != null)
            {
                if (dto.QuoteStatus.Equals("Intent to Buy"))
                {
                    proposalHeader.LeadActivity.Lead.QuoteIntentToBuy(entity.Id);
                    proposalHeader.IsIntentToBuy = true;
                }

                repository.Save(proposalHeader);
                cmpid = proposalHeader.CmpidSource;
            }
            result.Processed(entity.Id);


            entity.RaiseEvent(new SalesForceEntryEvent(dto.QuoteStatus, entity, cmpid, quote));
        }
    }
}