﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.SalesForce.Queries;
using iPlatform.Api.DTOs.SalesForce;
using MasterData.Authorisation;

namespace Domain.SalesForce.Handlers
{
    public class EditSalesForceEntryDtoHandler : ExistingEntityDtoHandler<SalesForceEntry, EditSalesForceEntryDto, int>
    {
        private readonly IProvideContext _contextProvider;
        private readonly IRepository _repository;

        public EditSalesForceEntryDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _contextProvider = contextProvider;
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override SalesForceEntry GetEntityById(EditSalesForceEntryDto dto)
        {
            return new GetSalesForceEntryByCriteriaQuery(_contextProvider, _repository)
                .WithPartyId(dto.Id)
                .Execute().FirstOrDefault();
        }

        protected override void HandleExistingEntityChange(SalesForceEntry entity, EditSalesForceEntryDto dto,
            HandlerResult<int> result)
        {
            entity.Delayed = dto.Delayed;
            entity.Suspended = dto.Suspended;
            entity.ModifiedOn = DateTime.UtcNow;
            result.Processed(entity.Id);
        }
    }
}