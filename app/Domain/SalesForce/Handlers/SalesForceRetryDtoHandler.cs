using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.SalesForce;
using MasterData.Authorisation;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Driver;
using ServiceStack.Common.Web;

namespace Domain.SalesForce.Handlers
{
    public class SalesForceRetryDtoHandler : ExistingEntityDtoHandler<SalesForceIntegrationLog, SalesForceRetryDto, int>
    {
        public SalesForceRetryDtoHandler(IProvideContext contextProvider, IRepository repository, ISession session)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _session = session;
        }

        private IRepository _repository { get; set; }
        private ISession _session { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }



        protected override void HandleExistingEntityChange(SalesForceIntegrationLog entity, SalesForceRetryDto dto, HandlerResult<int> resultDto)
        {
            System.Threading.Tasks.Task<string> task;
            string authError = String.Empty;

            Log.InfoFormat("Retry to sales force for Id {0}", dto.Id);


            SalesForceIntegrationLog item = new SalesForceIntegrationLog();
            try
            {
                var helper = new SalesForceHelper();
                authError = helper.AuthError;

                object data = JsonConvert.DeserializeObject(entity.JsonData);

                task = helper.Create(dto.ObjectType, data);
                task.Wait();
                item = new SalesForceIntegrationLog(entity.PartyId, entity.JsonData, task.Result, true,entity.Status);
                Log.InfoFormat("Pushed to sales force for retry Id {0}", dto.Id);
            }
            catch (Exception e)
            {
                var message = string.Format("{0},{1},{2}", authError, e.Message, e.InnerException);
                Log.ErrorFormat(message);
                item = new SalesForceIntegrationLog(entity.PartyId, entity.JsonData, message, false, entity.Status);
            }
            finally
            {
                _session.Save(item);
                resultDto.Processed(item.Id);
            }
        }
    }
}