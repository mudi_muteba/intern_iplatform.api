using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.SalesForce;
using MasterData.Authorisation;
using Newtonsoft.Json;
using NHibernate;

namespace Domain.SalesForce.Handlers
{
    public class SalesForceLeadStatusDtoHandler : CreationDtoHandler<SalesForceIntegrationLog, SalesForceLeadStatusDto, int>
    {
        public SalesForceLeadStatusDtoHandler(IProvideContext contextProvider, IRepository repository, ISession session)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _session = session;
        }

        private IRepository _repository { get; set; }
        private ISession _session { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void EntitySaved(SalesForceIntegrationLog entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override SalesForceIntegrationLog HandleCreation(SalesForceLeadStatusDto dto, HandlerResult<int> result)
        {
            System.Threading.Tasks.Task<string> task;

            Log.InfoFormat("Pushing to sales force for party {0} and status {1}", dto.PartyId, dto.QuoteStatus);
            var individual = _repository.GetAll<Individual>().FirstOrDefault(a => a.Id == dto.PartyId);
            if (individual == null)
                throw new MissingEntityException(typeof(Individual), dto.Id);

            var submitDto = individual.GetSalesForceSubmitDto();

            // for some reason auto mapper is not doing this on IndividualMapper
            submitDto.Salutation = individual.Title != null ? individual.Title.Name : "";

            var status = ConfigurationManager.AppSettings[@"salesforce/Status"];
            var submittedFromWebsitePage = ConfigurationManager.AppSettings[@"salesforce/SubmittedFromWebsitePage"];
            var leadSource = ConfigurationManager.AppSettings[@"salesforce/LeadSource"];
            submitDto.UpdateStatus(dto, status, submittedFromWebsitePage, leadSource);

            string authError = String.Empty;
            var js = JsonConvert.SerializeObject(submitDto);
            object o = JsonConvert.DeserializeObject(js);
            SalesForceIntegrationLog item;
            try
            {
                var helper = new SalesForceHelper();
                authError = helper.AuthError;
                task = helper.Create("lead", o);
                task.Wait();
                item = new SalesForceIntegrationLog(dto.PartyId, JsonConvert.SerializeObject(submitDto), task.Result, true, dto.QuoteStatus);
                Log.InfoFormat("Pushed to sales force for party {0} and status {1}", dto.PartyId, dto.QuoteStatus);
            }
            catch (Exception e)
            {
                var message = string.Format("{0},{1},{2}", authError, e.Message, e.InnerException);
                Log.ErrorFormat(message);
                item = new SalesForceIntegrationLog(dto.PartyId, JsonConvert.SerializeObject(submitDto), message, false, dto.QuoteStatus);

                // create new ttransaction and commit because of the 'throw' at the end of this method (it causes a rollback)
                var tran = _session.BeginTransaction();
                _session.Save(item);
                tran.Commit();
                result.AddError(new ResponseErrorMessage(message, "SALES_FORCE_ERROR"));
                throw; //this triggers the workflow retry
            }

            return item;
        }
    }
}