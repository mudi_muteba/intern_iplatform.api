﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using Castle.Core.Internal;
using Common.Logging;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Individuals.WorkFlow.Messages;
using Domain.Party.Quotes;
using Domain.SalesForce.Events;
using Domain.SalesForce.Queries;
using iPlatform.Api.DTOs.SalesForce;
using Workflow.Messages;

namespace Domain.SalesForce.Handlers
{
    public class SalesForceEntryEventHandler : BaseEventHandler<SalesForceEntryEvent>
    {
        private static readonly ILog Log = LogManager.GetLogger<SalesForceEntryEventHandler>();
        private readonly IWorkflowRouter _router;

        public SalesForceEntryEventHandler(IWorkflowRouter router, IRepository repository,
            IProvideContext contextProvider)
        {
            _router = router;
            _repository = repository;
            _contextProvider = contextProvider;
        }

        private IRepository _repository { get; set; }
        private IProvideContext _contextProvider { get; set; }

        public override void Handle(SalesForceEntryEvent @event)
        {
            var source = string.Empty;

            if (_contextProvider.Get().RequestHeaders.ContainsKey("source"))
                source = _contextProvider.Get().RequestHeaders["source"].FirstOrDefault();

            if (!source.Equals("aig-widget"))
            {
                Log.DebugFormat("SalesForceEntryEventHandler SKIP SalesForce SOURCE is set as: aig-widget");
                return;
            }

            var salesForceStatus = CreateSalesForceStatusDto(@event);

            //Create or update sales force entry
            var salesForceEntry = new GetSalesForceEntryByCriteriaQuery(_contextProvider, _repository)
                .WithPartyId(@event.Individual.Id)
                .Execute()
                .FirstOrDefault() ?? new SalesForceEntry(@event.Individual.Id);

            int quoteId = (@event.Quote == null) ? 0 : @event.Quote.Id;

            salesForceEntry.Update(salesForceStatus.MessageId, @event.QuoteStatus, @event.Cmpid, @event.ContactOnlyIntendedProduct, quoteId, false);
            _repository.Save(salesForceEntry);

            PublishMessage(salesForceStatus);
        }

        private void PublishMessage(SalesForceLeadStatusDto salesForceLeadStatus)
        {
            var message = new WorkflowRoutingMessage();
            var metadata = new SalesForcePostMetaData().WithMetaData(salesForceLeadStatus);
            var delay = ConfigurationManager.AppSettings[@"salesforce/delay"];


            message.AddMessage(
                new SalesForcePostMessage(metadata));
            try
            {
                var allowedStatuses = new List<string>() { "Declined", "UW Referral", "Intent to Buy" };

                if (allowedStatuses.Contains(salesForceLeadStatus.QuoteStatus))
                    _router.Publish(message);
                else
                _router.PublishDelayed(message, TimeSpan.FromSeconds(double.Parse(delay)));
                Log.DebugFormat("SalesForceEntryEventHandler published message for party {0}", salesForceLeadStatus.PartyId);
            }
            catch (Exception e)
            {
                Log.ErrorFormat("SalesForceEntryEventHandler publish failed {0}", e.Message);
                throw;
            }

        }

        private SalesForceLeadStatusDto CreateSalesForceStatusDto(SalesForceEntryEvent @event)
        {
            var messageId = Guid.NewGuid(); 
            var salesForceStatus = new SalesForceLeadStatusDto
            {
                PartyId = @event.Individual.Id,
                CMPID = @event.Cmpid,
                QuoteStatus = @event.QuoteStatus,
                MessageId = messageId
            };

            if (@event.Quote != null)
                GetQuoteInfo(@event.Quote, salesForceStatus);
            else
            {
                salesForceStatus.QuotedProduct = @event.ContactOnlyIntendedProduct;
                salesForceStatus.ProposalHeaderId = @event.ProposalHeaderId;
            }
            return salesForceStatus;
        }

        public static void GetQuoteInfo(Quote quote, ISalesForceStatusDto salesForceStatus)
        {
            if (quote == null)
                return;

            string quotedProduct = string.Empty;

            salesForceStatus.QuoteAmount =
                quote.Items.Sum(a => a.Fees.TotalPremium).ToString(CultureInfo.InvariantCulture);
            salesForceStatus.QuoteNumber = quote.QuoteHeader.ExternalReference;
            salesForceStatus.QuoteId = quote.Id;
            salesForceStatus.ProposalHeaderId = quote.QuoteHeader.Proposal.Id;
            if (quote.Items != null && quote.Items.Any())
                foreach (var item in quote.Items)
                {
                    if (item.CoverDefinition.Cover.Name.ToLower().Contains("motor")) quotedProduct += "Auto:";
                    if (item.CoverDefinition.Cover.Name.ToLower().Contains("contents")) quotedProduct += "Home:";
                    if (item.CoverDefinition.Cover.Name.ToLower().Contains("funeral")) quotedProduct += "Funeral:";
                }
            salesForceStatus.QuotedProduct = quotedProduct;
        }
    }
}