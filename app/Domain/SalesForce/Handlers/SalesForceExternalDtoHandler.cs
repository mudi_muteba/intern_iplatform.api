using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.SalesForce;
using MasterData.Authorisation;
using Newtonsoft.Json;
using NHibernate;

namespace Domain.SalesForce.Handlers
{
    public class SalesForceExternalDtoHandler : CreationDtoHandler<SalesForceExternalLog, SalesForceExternalDto, SalesForceExternalResponseDto>
    {
        public SalesForceExternalDtoHandler(IProvideContext contextProvider, IRepository repository,ISession session)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _session = session;
        }

        private IRepository _repository { get; set; }
        private ISession _session { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void EntitySaved(SalesForceExternalLog entity, HandlerResult<SalesForceExternalResponseDto> result)
        {
        }

        protected override SalesForceExternalLog HandleCreation(SalesForceExternalDto dto, HandlerResult<SalesForceExternalResponseDto> result)
        {
            System.Threading.Tasks.Task<string> task;
            var resultDto = new SalesForceExternalResponseDto();

            Log.InfoFormat("Pushing to sales force for external reference {0}", dto.ExternalReference);
        
            var model = JsonConvert.DeserializeObject<object>(dto.Model);
            SalesForceExternalLog item;
            try
            {
                var helper = new SalesForceHelper();
                task = helper.Create(dto.ObjectType, model);
                task.Wait();
                item = new SalesForceExternalLog(dto.ObjectType, dto.Model, task.Result, true);
                Log.InfoFormat("Pushed to sales force for external reference {0}", dto.ExternalReference);
                resultDto.Response = task.Result;
                result.Processed(resultDto);
            }
            catch (Exception e)
            {
                var message = string.Format("{0},{1}", e.Message, e.InnerException);
                item = new SalesForceExternalLog(dto.ObjectType, dto.Model, message, false);
                result.AddError(new ResponseErrorMessage(message, "SALES_FORCE_ERROR"));
                resultDto.Response = message;
                result.Processed(resultDto);
                Log.ErrorFormat("Pushed to sales force FAIL for external reference {0}", dto.ExternalReference);
                Log.ErrorFormat(message);
            }
            return item;
        }
    }
}