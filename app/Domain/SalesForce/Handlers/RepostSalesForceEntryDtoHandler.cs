using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.SalesForce.Events;
using iPlatform.Api.DTOs.SalesForce;
using MasterData.Authorisation;
using Newtonsoft.Json;
using NHibernate;

namespace Domain.SalesForce.Handlers
{
    public class RepostSalesForceEntryDtoHandler : ExistingEntityDtoHandler<Individual, RepostSalesForceEntryDto, int>
    {
        public RepostSalesForceEntryDtoHandler(IProvideContext contextProvider, IRepository repository, ISession session)
            : base(contextProvider, repository)
        {
            _repository = repository;
            _session = session;
        }

        private IRepository _repository { get; set; }
        private ISession _session { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }



        protected override void HandleExistingEntityChange(Individual entity, RepostSalesForceEntryDto dto, HandlerResult<int> resultDto)
        {
  
            Log.InfoFormat("Repost salesforce entry to sales force for Id {0}", dto.Id);

            var salesForceEntry = _repository.GetAll<SalesForceEntry>().FirstOrDefault(a => a.PartyId == entity.Id);
            entity.RaiseEvent(new SalesForceEntryEvent(salesForceEntry.Status, entity, salesForceEntry.Cmpid, null, salesForceEntry.ContactOnlyIntendedProduct));
  
        }
    }
}