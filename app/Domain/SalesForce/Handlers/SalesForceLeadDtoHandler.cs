using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.SalesForce;
using MasterData.Authorisation;
using Newtonsoft.Json;

namespace Domain.SalesForce.Handlers
{
    public class SalesForceLeadDtoHandler : CreationDtoHandler<SalesForceIntegrationLog, SalesForceLeadDto, int>
    {
        public SalesForceLeadDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private IRepository _repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void EntitySaved(SalesForceIntegrationLog entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override SalesForceIntegrationLog HandleCreation(SalesForceLeadDto dto, HandlerResult<int> result)
        {
            var helper = new SalesForceHelper();
            var task = helper.Create("lead", dto.GetSubmitDto());
            task.Wait();

            return new SalesForceIntegrationLog(dto.PartyId, JsonConvert.SerializeObject(dto), task.Result,true,dto.QuoteStatus);
        }
    }
}