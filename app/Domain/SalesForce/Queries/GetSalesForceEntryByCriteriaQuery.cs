﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.SalesForce.Queries
{
    public class GetSalesForceEntryByCriteriaQuery : BaseQuery<SalesForceEntry>
    {
        private Guid _messageId;
        private int _partyId;

        public GetSalesForceEntryByCriteriaQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultFilters()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetSalesForceEntryByCriteriaQuery WithPartyId(int partyId)
        {
            _partyId = partyId;
            return this;
        }

        public GetSalesForceEntryByCriteriaQuery WithMessageId(Guid messageId)
        {
            _messageId = messageId;
            return this;
        }


        public IQueryable<SalesForceEntry> GetQuery(IQueryable<SalesForceEntry> query = null)
        {
            return Execute(query);
        }

        protected internal override IQueryable<SalesForceEntry> Execute(IQueryable<SalesForceEntry> query = null)
        {
            query = query ?? Repository.GetAll<SalesForceEntry>();
            
            if (_partyId > 0)
                query = query.Where(a => a.PartyId == _partyId);

            if (_messageId != Guid.Empty)
                query = query.Where(a => a.MessageId == _messageId);

            return query;
        }
    }
}