using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Products;
using Shared;

namespace Domain.SalesForce.Queries
{
    public class DefaultFilters : IApplyDefaultFilters<SalesForceEntry>
    {
        public IQueryable<SalesForceEntry> Apply(ExecutionContext executionContext, IQueryable<SalesForceEntry> query)
        {
            return query;
        }
    }
}