﻿using System;
using Domain.Base;

namespace Domain.SalesForce
{
    public class SalesForceEntry : Entity
    {
        public SalesForceEntry()
        {
            ModifiedOn = DateTime.UtcNow;
            CreatedOn = DateTime.UtcNow;
        }
        public SalesForceEntry(int partyId)
        {
            PartyId = partyId;
            ModifiedOn = DateTime.UtcNow;
            CreatedOn = DateTime.UtcNow;
        }

        public virtual int PartyId { get; set; }
        public virtual Guid MessageId { get; set; }
        public virtual bool Delayed { get; set; }
        public virtual bool Suspended { get; set; }
        public virtual string Status { get; set; }
        public virtual string Cmpid { get; set; }
        public virtual string ContactOnlyIntendedProduct { get; set; }
        public virtual int QuoteId { get; set; }
        public virtual DateTime ModifiedOn { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual SalesForceEntry Update(Guid messagesId, string status, string cmpid, string contactOnlyIntendedProduct,int quoteId, bool delayed)
        {
            // remove suspension for all new entries 
            Suspended = false;

            Delayed = delayed;
            MessageId = messagesId;
            Status = status;
            Cmpid = cmpid;
            ContactOnlyIntendedProduct = contactOnlyIntendedProduct;
            QuoteId = quoteId;
            ModifiedOn = DateTime.UtcNow;
            return this;
        }
    }
}