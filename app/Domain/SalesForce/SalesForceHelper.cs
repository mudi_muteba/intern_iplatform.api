﻿using System;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using Common.Logging;
using iPlatform.Api.DTOs.SalesForce;
using Salesforce.Common;
using Salesforce.Force;
namespace Domain.SalesForce
{
    public class SalesForceHelper
    {
        private readonly SalesForceHelperConfiguration _helperConfig;
        private AuthenticationClient _authClient;
        protected static readonly ILog Log = LogManager.GetLogger<SalesForceHelper>();
        public string AuthError= String.Empty;

        public SalesForceHelper()
        {
            _helperConfig = new SalesForceHelperConfiguration
            {
                ConsumerKey = ConfigurationManager.AppSettings[@"salesforce/consumerKey"],
                ConsumerSecret = ConfigurationManager.AppSettings[@"salesforce/consumerSecret"],
                Username = ConfigurationManager.AppSettings[@"salesforce/username"],
                Password = ConfigurationManager.AppSettings[@"salesforce/password"],
                SecurityKey = ConfigurationManager.AppSettings[@"salesforce/securityKey"],
                AuthServiceUrl = ConfigurationManager.AppSettings[@"salesforce/authServiceUrl"],
            };
            var task = Authenticate();
            task.Wait();
        }


        public async Task<string> Create(string objectType, object model)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            var client = new ForceClient(_authClient.InstanceUrl, _authClient.AccessToken, _authClient.ApiVersion);
            string resp = await client.CreateAsync(objectType, model).ConfigureAwait(false);
            return resp;
        }

        private async Task Authenticate()
        {
            _authClient = new AuthenticationClient();
            try
            {
                await _authClient.UsernamePasswordAsync(_helperConfig.ConsumerKey,
                    _helperConfig.ConsumerSecret,
                    _helperConfig.Username,
                    _helperConfig.Password + _helperConfig.SecurityKey,
                    _helperConfig.AuthServiceUrl).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                AuthError = String.Format("AUTH ERROR {0},{1}", ex.Message, ex.InnerException != null ? ex.InnerException.Message : "");
                Log.ErrorFormat("Sales force Authentication. {0}", AuthError);
            }
        }
    }
}