﻿using System;
using System.Linq;
using System.Net;
using Common.Logging;
using Infrastructure.Configuration;
using RestSharp;
using Shared.Extentions;
using Workflow.Infrastructure;

namespace Domain.AuditLeadlog
{
    public class AuditLeadLogAuthentication
    {
        private readonly string _postUrl;
        private readonly AuthenticationRequestDto _request;
        private static ILog Log = LogManager.GetLogger<AuditLeadLogAuthentication>();

        public AuditLeadLogAuthentication()
        {
            AdminConfigurationReader config = new AdminConfigurationReader();
            _postUrl = config.BaseUrl;
            
            _request = new AuthenticationRequestDto()
            {
                Email = config.AdminUsername ?? "admin@iplatform.co.za",
                ApiKey = config.AdminApiKey ?? "password"
            };
        }

        public AuthenticateResponseDto Authenticate()
        {

            var client = new RestClient(_postUrl);
            var request = new RestRequest("authenticate", Method.POST)
            {
                RequestFormat = DataFormat.Json,
            };

            request.AddBody(_request);
            try
            {
                Log.InfoFormat("Authenticating iAdmin with email address {0} and url {1}", _request.Email, _postUrl);
                var response = client.Execute<AuthenticateResponseDto>(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var token = response.Headers.FirstOrDefault(h => h.Name.EqualsIgnoreCase("Authorization"));

                    if (token == null)
                        return AuthenticateResponseDto.NotAuthenticated();

                    return string.IsNullOrEmpty(token.Value.ToString())
                        ? AuthenticateResponseDto.NotAuthenticated()
                        : AuthenticateResponseDto.Authenticated(token.Value.ToString());
                }
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Could not authenticate using email: '{0}' and password: '{1}' having following errors: {2}",
                        _request.Email,
                        _request.ApiKey,
                        new ExceptionPrettyPrinter().Print(ex));
                Log.ErrorFormat("Authentication Failed: {0}", error);
            }

            return AuthenticateResponseDto.NotAuthenticated();
        }
    }

    public class AuthenticationRequestDto
    {
        public string Email { get; set; }
        public string ApiKey { get; set; }
    }

    public class AuthenticateResponseDto
    {
        public bool IsAuthenticated { get; set; }
        public string Token { get; set; }

        public AuthenticateResponseDto()
        {
        }

        private AuthenticateResponseDto(string token, bool authenticated)
        {
            Token = token;
            IsAuthenticated = authenticated;
        }

        public static AuthenticateResponseDto Authenticated(string token)
        {
            return new AuthenticateResponseDto(token, true);
        }

        public static AuthenticateResponseDto NotAuthenticated()
        {
            return new AuthenticateResponseDto(string.Empty, false);
        }
    }
}
