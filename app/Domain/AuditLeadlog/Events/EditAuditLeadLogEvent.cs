﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Events;
using iPlatform.Api.DTOs.AuditLeadLog;

namespace Domain.AuditLeadlog.Events
{
    public class EditAuditLeadLogEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public EditAuditLeadLogDto EditAuditLeadLogDto { get; set; }

        public EditAuditLeadLogEvent(EditAuditLeadLogDto editAuditLeadLogDto)
        {
            EditAuditLeadLogDto = editAuditLeadLogDto;
        }
    }
}
