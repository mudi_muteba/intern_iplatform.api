﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Events;
using iPlatform.Api.DTOs.AuditLeadLog;

namespace Domain.AuditLeadlog.Events
{
    public class CreateAuditLeadLogEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public CreateAuditLeadLogDto CreateAuditLeadLogDto { get; set; }

        public CreateAuditLeadLogEvent(CreateAuditLeadLogDto createAuditLeadLogDto)
        {
            CreateAuditLeadLogDto = createAuditLeadLogDto;
        }
    }
}
