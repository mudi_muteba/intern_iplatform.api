﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.AuditLeadLog;
using Workflow.Messages;

namespace Domain.AuditLeadlog.Message
{
    public class CreateAuditLeadLogMessage : WorkflowExecutionMessage
    {
        public CreateAuditLeadLogDto CreateAuditLeadLogDto { get; set; }

        public CreateAuditLeadLogMessage(CreateAuditLeadLogDto createAuditLeadLogDto,
            WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
            CreateAuditLeadLogDto = createAuditLeadLogDto;
        }
    }
}
