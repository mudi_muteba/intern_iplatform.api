﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.AuditLeadLog;
using Workflow.Messages;

namespace Domain.AuditLeadlog.Message
{
    public class EditAuditLeadLogMessage : WorkflowExecutionMessage
    {
        public EditAuditLeadLogDto EditAuditLeadLogDto { get; set; }

        public EditAuditLeadLogMessage(EditAuditLeadLogDto editAuditLeadLogDtoDto,
            WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
            EditAuditLeadLogDto = editAuditLeadLogDtoDto;
        }
    }
}
