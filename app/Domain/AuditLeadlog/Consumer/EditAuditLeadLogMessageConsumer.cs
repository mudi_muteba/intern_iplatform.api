﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Domain.AuditLeadlog.Message;
using iPlatform.Api.DTOs.AuditLeadLog;
using Infrastructure.Configuration;
using RestSharp;
using Shared;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.AuditLeadlog.Consumer
{
    public class EditAuditLeadLogMessageConsumer : AbstractMessageConsumer<EditAuditLeadLogMessage>
    {
        private readonly string _BaseUrl;
        private readonly RestClient _Client;
        private readonly string _Token;
        private static readonly ILog Log = LogManager.GetLogger<EditAuditLeadLogMessageConsumer>();

        public EditAuditLeadLogMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor) : base(router, executor)
        {
            _BaseUrl = new AdminConfigurationReader().BaseUrl;

            _Client = new RestClient(_BaseUrl);
            _Token = new AuditLeadLogAuthentication().Authenticate().Token;
        }

        public override void Consume(EditAuditLeadLogMessage message)
        {
            this.Info(() => string.Format("Logging to Lead Audit: {0} by LeadId {1}", message.EditAuditLeadLogDto, message.EditAuditLeadLogDto.LeadId));

            string queryString = String.Format("auditLeadLog/updateByLeadId/{0}", message.EditAuditLeadLogDto.LeadId);
            IRestRequest request = CreateRequest<EditAuditLeadLogDto>(queryString, message.EditAuditLeadLogDto);
            try
            {
                Log.InfoFormat("Sending EditAuditLeadLog to iAdmin api on url {0}", _BaseUrl);
                IRestResponse response = _Client.Execute<EditAuditLeadLogDto>(request);

                if (response.StatusCode == HttpStatusCode.OK)
                    this.Info(() => string.Format("Successfully Logged to Lead Audit: {0} by LeadId {1}", message.EditAuditLeadLogDto, message.EditAuditLeadLogDto.LeadId));
                else
                    this.Error(() => string.Format("Logging to Lead Audit: {0} by LeadId {1} - {2}", message.EditAuditLeadLogDto, message.EditAuditLeadLogDto.LeadId, response.ErrorMessage));
            }
            catch (Exception ex)
            {
                var error = string.Format("Could not send EditAuditLeadLog with LeadId '{0}'. Exception details: {1}",
                    message.EditAuditLeadLogDto.LeadId,
                    new ExceptionPrettyPrinter().Print(ex));
                Log.ErrorFormat("EditAuditLeadLog Transfer Error: {0}", error);
            }
        }

        #region [ Helpers ]

        private IRestRequest CreateRequest<T>(string queryString, T dto)
        {
            RestRequest request = new RestRequest(queryString)
            {
                Method = Method.PUT,
                RequestFormat = DataFormat.Json
            };
            request.AddParameter("Authorization", _Token, ParameterType.HttpHeader);
            request.AddBody(dto);

            return request;
        }

        #endregion
    }
}
