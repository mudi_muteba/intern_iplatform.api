﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Domain.AuditLeadlog.Message;
using iPlatform.Api.DTOs.AuditLeadLog;
using iPlatform.Api.DTOs.AuditUserLog;
using Infrastructure.Configuration;
using RestSharp;
using Shared;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.AuditLeadlog.Consumer
{
    public class CreateAuditLeadLogMessageConsumer : AbstractMessageConsumer<CreateAuditLeadLogMessage>
    {
        private readonly string _BaseUrl;
        private readonly RestClient _Client;
        private readonly string _Token;
        private static readonly ILog Log = LogManager.GetLogger<CreateAuditLeadLogMessageConsumer>();

        public CreateAuditLeadLogMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor) : base(router, executor)
        {
            _BaseUrl = new AdminConfigurationReader().BaseUrl;

            _Client = new RestClient(_BaseUrl);
            _Token = new AuditLeadLogAuthentication().Authenticate().Token;
        }

        public override void Consume(CreateAuditLeadLogMessage message)
        {
            this.Info(() => string.Format("Logging to Lead Audit: {0} by LeadId {1}", message.CreateAuditLeadLogDto, message.CreateAuditLeadLogDto.LeadId));

            IRestRequest request = CreateRequest<CreateAuditLeadLogDto>("auditLeadLog", message.CreateAuditLeadLogDto);
            try
            {
                Log.InfoFormat("Sending AuditLeadLog to iAdmin api on url {0}", _BaseUrl);
                IRestResponse response = _Client.Execute<CreateAuditLeadLogDto>(request);

                if (response.StatusCode == HttpStatusCode.OK)
                    this.Info(() => string.Format("Successfully Logged to Lead Audit: {0} by LeadId {1}", message.CreateAuditLeadLogDto, message.CreateAuditLeadLogDto.LeadId));
                else
                    this.Error(() => string.Format("Logging to Lead Audit: {0} by LeadId {1} - {2}", message.CreateAuditLeadLogDto, message.CreateAuditLeadLogDto.LeadId, response.ErrorMessage));
            }
            catch (Exception ex)
            {
                var error = string.Format("Could not send AuditLeadLog with LeadId '{0}'. Exception details: {1}",
                    message.CreateAuditLeadLogDto.LeadId,
                    new ExceptionPrettyPrinter().Print(ex));
                Log.ErrorFormat("AuditLeadLog Transfer Error: {0}", error);
            }
        }

        #region [ Helpers ]

        private IRestRequest CreateRequest<T>(string queryString, T dto)
        {
            RestRequest request = new RestRequest(queryString)
            {
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };
            request.AddParameter("Authorization", _Token, ParameterType.HttpHeader);
            request.AddBody(dto);

            return request;
        }

        #endregion
    }
}
