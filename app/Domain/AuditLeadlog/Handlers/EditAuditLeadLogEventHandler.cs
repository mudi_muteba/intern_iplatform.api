﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Domain.AuditLeadlog.Events;
using Domain.AuditLeadlog.Message;
using Domain.Base.Events;
using iPlatform.Api.DTOs.AuditLeadLog;
using Workflow.Messages;

namespace Domain.AuditLeadlog.Handlers
{
    public class EditAuditLeadLogEventHandler : BaseEventHandler<EditAuditLeadLogEvent>
    {
        private static readonly ILog _Log = LogManager.GetLogger<EditAuditLeadLogEventHandler>();
        private readonly IWorkflowRouter _Router;

        public EditAuditLeadLogEventHandler(IWorkflowRouter router)
        {
            _Router = router;
        }

        public override void Handle(EditAuditLeadLogEvent @event)
        {
            _Router.Publish(
                new WorkflowRoutingMessage().AddMessage(
                    new EditAuditLeadLogMessage(@event.EditAuditLeadLogDto)));
        }
    }
}
