﻿using Domain.Base;
using Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Teams
{
    public class TeamUser : Entity
    {
        public virtual Team Team { get; set; }
        public virtual User User { get; set; }
    }
}
