﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Teams;
using MasterData.Authorisation;

namespace Domain.Teams.Handlers
{
    public class CreateTeamHandler : CreationDtoHandler<Team, CreateTeamDto, int>
    {
        public CreateTeamHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    TeamAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(Team entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Team HandleCreation(CreateTeamDto dto, HandlerResult<int> result)
        {
            var team = Team.Create(dto);

            result.Processed(team.Id);

            return team;
        }
    }
}