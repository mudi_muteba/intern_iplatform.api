﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Teams;
using MasterData.Authorisation;

namespace Domain.Teams.Handlers
{
    public class DisableTeamHandler : ExistingEntityDtoHandler<Team, DisableTeamDto, int>
    {
        public DisableTeamHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    TeamAuthorisation.Delete
                };
            }
        }

        protected override void HandleExistingEntityChange(Team entity, DisableTeamDto dto,
            HandlerResult<int> result)
        {
            entity.Disable();
            result.Processed(entity.Id);
        }
    }
}