﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Teams;
using MasterData.Authorisation;

namespace Domain.Teams.Handlers
{
    public class EditTeamHandler : ExistingEntityDtoHandler<Team, EditTeamDto, int>
    {
        public EditTeamHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    TeamAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(Team entity, EditTeamDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}