﻿using Domain.Base;
using Domain.Campaigns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Teams
{
    public class TeamCampaign : Entity
    {
        public virtual Team Team { get; set; }
        public virtual Campaign Campaign { get; set; }
    }
}
