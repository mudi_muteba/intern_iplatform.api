﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Teams.Overrides
{
    public class TeamOverride : IAutoMappingOverride<Team>
    {
        public void Override(AutoMapping<Team> mapping)
        {
            mapping.HasMany(c => c.Users).Inverse().Cascade.SaveUpdate();
            mapping.HasMany(c => c.Campaigns).Inverse().Cascade.SaveUpdate();
        }
    }
}
