﻿using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Teams;

namespace Domain.Teams.Mappings
{
    public class TeamMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateTeamDto, Team>()
                .ForMember(t => t.Campaigns, o => o.Ignore())
                .ForMember(t => t.Users, o => o.Ignore())
                ;

            Mapper.CreateMap<Team, ListTeamDto>();

            Mapper.CreateMap<PagedResults<Team>, PagedResultDto<ListTeamDto>>();
            Mapper.CreateMap<TeamCampaign, TeamCampaignDto>();
            Mapper.CreateMap<TeamUser, TeamUserDto>();


            Mapper.CreateMap<EditTeamDto, Team>()
                .ForMember(t => t.DateCreated, o => o.Ignore())
                .ForMember(t => t.Campaigns, o=> o.Ignore())
                .ForMember(t => t.Users, o => o.Ignore())
                .ForMember(t => t.DateUpdated, o => o.MapFrom(s => DateTime.UtcNow))
                ;

        }
    }
}