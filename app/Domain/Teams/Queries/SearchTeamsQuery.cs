﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Teams;
using MasterData.Authorisation;

namespace Domain.Teams.Queries
{
    public class SearchTeamsQuery : BaseQuery<Team>, ISearchQuery<SearchTeamDto>
    {
        private SearchTeamDto criteria;

        public SearchTeamsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Team>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    TeamAuthorisation.List
                };
            }
        }

        public void WithCriteria(SearchTeamDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<Team> Execute(IQueryable<Team> query)
        {

            if (criteria.TeamId.HasValue && criteria.TeamId > 0)
                query = query.Where(a => a.Id == criteria.TeamId);

            if (!string.IsNullOrWhiteSpace(criteria.Name))
            {
                query = query.Where(a => a.Name.ToLower().Contains(criteria.Name.ToLower()));
            }

            return query;
        }
    }
}