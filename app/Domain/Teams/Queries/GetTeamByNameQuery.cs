﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Teams.Queries
{
    public class GetTeamByNameQuery : BaseQuery<Team>
    {
        private string name;

        public GetTeamByNameQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<Team>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    TeamAuthorisation.List
                };
            }
        }

        public GetTeamByNameQuery ByName(string name)
        {
            this.name = name;
            return this;
        }

        protected internal override IQueryable<Team> Execute(IQueryable<Team> query)
        {
            return query
                .Where(c => c.Name == name);
        }
    }
}