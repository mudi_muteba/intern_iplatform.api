﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Teams;
using ValidationMessages.Campaigns;
using Domain.Products.Queries;
using Domain.Products;
using System.Collections.Generic;
using AutoMapper;
using Domain.Users;
using ValidationMessages.Teams;
using Domain.Campaigns;

namespace Domain.Teams.Validation
{
    public class TeamCampaignValidator : IValidateDto<CreateTeamDto>, IValidateDto<EditTeamDto>
    {

        public TeamCampaignValidator(IProvideContext contextProvider, IRepository repository)
        {
        }

        public void Validate(CreateTeamDto dto, ExecutionResult result)
        {
            ValidateCampaign(dto.Campaigns, result);
        }

        public void Validate(EditTeamDto dto, ExecutionResult result)
        {
            ValidateCampaign(dto.Campaigns, result);
        }

        private void ValidateCampaign(List<CampaignInfoDto> campaigns, ExecutionResult result)
        {
            foreach (var campaign in campaigns)
            {
                var _campaign = Mapper.Map<int, Campaign>(campaign.Id);

                if (_campaign == null || _campaign.IsDeleted == true)
                {
                    result.AddValidationMessage(TeamValidationMessages.InvalidCampaignId.AddParameters(new[] { campaign.Id.ToString() }));
                }
            }
        }
    }
}