﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Users;
using iPlatform.Api.DTOs.Teams;
using iPlatform.Api.DTOs.Users;
using ValidationMessages.Teams;

namespace Domain.Teams.Validation
{
    public class TeamUserValidator : IValidateDto<CreateTeamDto>, IValidateDto<EditTeamDto>
    {
        public TeamUserValidator(IProvideContext contextProvider, IRepository repository)
        {
        }

        public void Validate(CreateTeamDto dto, ExecutionResult result)
        {
            ValidateUser(dto.Users, result);
        }

        public void Validate(EditTeamDto dto, ExecutionResult result)
        {
            ValidateUser(dto.Users, result);
        }

        private void ValidateUser(List<UserInfoDto> users, ExecutionResult result)
        {
            foreach (var u in users)
            {
                var user = Mapper.Map<int, User>(u.Id);
                if (user == null || user.IsDeleted)
                    result.AddValidationMessage(TeamValidationMessages.InvalidUserId.AddParameters(new[] { u.Id.ToString() }));
            }
        }
    }
}