﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Teams;
using Domain.Teams.Queries;
using System.Linq;
using iPlatform.Api.DTOs.Teams;
using ValidationMessages.Teams;

namespace Domain.Campaigns.Validation
{
    public class UniqueTeamNameValidator : IValidateDto<CreateTeamDto>
    {
        private readonly GetTeamByNameQuery query;

        public UniqueTeamNameValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetTeamByNameQuery(contextProvider, repository);
        }

        public void Validate(CreateTeamDto dto, ExecutionResult result)
        {
            var existingCampaign = GetExistingTeamByName(dto.Name);

            if (existingCampaign.Any())
            {
                result.AddValidationMessage(TeamValidationMessages.DuplicateName.AddParameters(new[] { dto.Name }));
            }
        }


        private IQueryable<Team> GetExistingTeamByName(string name)
        {
            return query.ByName(name).ExecuteQuery();
        }
    }
}