﻿using AutoMapper;
using Domain.Base;
using Domain.Campaigns;
using Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Teams;
using iPlatform.Api.DTOs.Users;

namespace Domain.Teams
{
    public class Team : Entity
    {
        public Team()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }

        public virtual string Name { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual DateTime DateUpdated { get; set; }
        public virtual IList<TeamCampaign> Campaigns { get; set; }
        public virtual IList<TeamUser> Users { get; set; }

        public static Team Create(CreateTeamDto dto)
        {
            var team = Mapper.Map<Team>(dto);

            AddUsers(team, dto);
            AddCampaigns(team, dto);

            return team;
        }

        public virtual void Update(EditTeamDto editTeamDto)
        {
            Mapper.Map(editTeamDto, this);

            UpdateUsers(editTeamDto);

            UpdateCampaigns(editTeamDto);

        }

        public virtual void Disable()
        {
            this.Delete();
            this.Users.All(x => { x.Delete(); return true; });
            this.Campaigns.All(x => { x.Delete(); return true; });
        }

        #region Users
        public static void AddUsers(Team entity, CreateTeamDto dto)
        {
            foreach (var user in dto.Users)
            {
                entity.AddUser(new User() { Id = user.Id });
            }
        }

        public virtual void AddUser(User user)
        {
            if (user == null)
                return;

            if (Users == null)
                Users = new List<TeamUser>();

            var existing = Users.FirstOrDefault(x => x.User.Id == user.Id);
            if (existing == null)
                Users.Add(new TeamUser { Team = this, User = user });
        }

        public virtual void UpdateUsers(EditTeamDto dto)
        {
            //delete users that have been removed from list
            RemoveOldUsers(dto.Users);

            //Add new users from list
            var newUsers = dto.Users.Where(x => !Users.Any(y => y.User.Id == x.Id)).ToList();

            foreach (var user in newUsers)
            {
                AddUser(new User() { Id = user.Id });
            }
        }

        public virtual void RemoveOldUsers(List<UserInfoDto> users)
        {
            var toBeDeleted = Users.Where(t => !users.Any(y => y.Id == t.User.Id));

            foreach (var user in toBeDeleted)
            {
                user.Delete();
            }
        }
        #endregion

        #region Campaigns
        public static void AddCampaigns(Team entity, CreateTeamDto dto)
        {
            foreach (var campaign in dto.Campaigns)
            {
                entity.AddCampaign(new Campaign() { Id = campaign.Id });
            }
        }

        public virtual void AddCampaign(Campaign campaign)
        {
            if (campaign == null)
                return;

            if (Campaigns == null)
                Campaigns = new List<TeamCampaign>();

            var existing = Campaigns.FirstOrDefault(x => x.Campaign.Id == campaign.Id);
            if (existing == null)
                Campaigns.Add(new TeamCampaign { Team = this, Campaign = campaign });
        }

        public virtual void UpdateCampaigns(EditTeamDto dto)
        {
            //delete campaigns that have been removed from list
            RemoveOldCampaigns(dto.Campaigns);

            //Add new campaigns from list
            var newCampaigns = dto.Campaigns.Where(x => !Campaigns.Any(y => y.Campaign.Id == x.Id)).ToList();

            foreach (var campaign in newCampaigns)
            {
                AddCampaign(new Campaign() { Id = campaign.Id });
            }
        }

        public virtual void RemoveOldCampaigns(List<CampaignInfoDto> campaigns)
        {
            var toBeDeleted = Campaigns.Where(t => !campaigns.Any(y => y.Id == t.Campaign.Id));

            foreach (var campaign in toBeDeleted)
            {
                campaign.Delete();
            }
        }
        #endregion
    }
}
