﻿using Domain.Base;
using MasterData;

namespace Domain.Escalations
{
    public class EscalationPlanStepWorkflowMessage : EntityWithAudit
    {
        public virtual WorkflowMessageEnumType WorkflowMessageEnumType { get; protected internal set; }
        public virtual EscalationPlanStep EscalationPlanStep { get; protected internal set; }

        protected EscalationPlanStepWorkflowMessage() { }

        public EscalationPlanStepWorkflowMessage(EscalationPlanStep escalationPlanStep, string workflowMessageEnumType)
        {
            EscalationPlanStep = escalationPlanStep;
            WorkflowMessageEnumType = new WorkflowMessageEnumTypes()[workflowMessageEnumType];
        }
    }
}