﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Enums.Escalations;
using MasterData.Authorisation;

namespace Domain.Escalations.Queries
{
    public class FindEscalationPlanExecutionHistoryQuery : BaseQuery<EscalationPlanExecutionHistory>, IContextFreeQuery
    {
        private Guid _initialCorrelationId;
        private int _stepId;
        private int _escalationPlanStepWorkflowMessageId;
        private int _userId;
        private EscalationWorkflowMessageStatus _escalationWorkflowMessageStatus;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public FindEscalationPlanExecutionHistoryQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<EscalationPlanExecutionHistory>())
        {
        }

        public FindEscalationPlanExecutionHistoryQuery WithParameters(Guid initialCorrelationId, int stepId, int escalationPlanStepWorkflowMessageId, int userId, EscalationWorkflowMessageStatus escalationWorkflowMessageStatusEnumType)
        {
            _initialCorrelationId = initialCorrelationId;
            _stepId = stepId;
            _escalationPlanStepWorkflowMessageId = escalationPlanStepWorkflowMessageId;
            _userId = userId;
            _escalationWorkflowMessageStatus = escalationWorkflowMessageStatusEnumType;
            return this;
        }

        protected internal override IQueryable<EscalationPlanExecutionHistory> Execute(IQueryable<EscalationPlanExecutionHistory> query)
        {
            var results = query.Where(x => x.InitialCorrelationId == _initialCorrelationId && x.EscalationPlanStep.Id == _stepId && 
                                           x.EscalationPlanStepWorkflowMessage.Id == _escalationPlanStepWorkflowMessageId && 
                                           x.User.Id == _userId && x.EscalationWorkflowMessageStatus == _escalationWorkflowMessageStatus);
            
            return results;
        }
    }
}