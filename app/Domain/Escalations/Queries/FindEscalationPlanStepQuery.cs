using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Escalations.Queries
{
    public class FindEscalationPlanStepQuery : BaseQuery<EscalationPlanStep>, IContextFreeQuery
    {
        private int _escalationPlanId;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public FindEscalationPlanStepQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<EscalationPlanStep>())
        {
        }

        public FindEscalationPlanStepQuery WithParameters(int escalationPlanId)
        {
            _escalationPlanId = escalationPlanId;
            return this;
        }

        protected internal override IQueryable<EscalationPlanStep> Execute(IQueryable<EscalationPlanStep> query)
        {
            query = query.Where(x => x.EscalationPlan.Id == _escalationPlanId);

            return query;
        }
    }
}