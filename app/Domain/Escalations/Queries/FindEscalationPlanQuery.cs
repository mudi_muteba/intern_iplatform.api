﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Escalations.Queries
{
    public class FindEscalationPlanQuery : BaseQuery<EscalationPlan>, IContextFreeQuery
    {
        private IEnumerable<int> _channels;
        private IEnumerable<int> _campaigns;
        private string _eventType;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public FindEscalationPlanQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<EscalationPlan>())
        {
        }

        public FindEscalationPlanQuery WithParameters(IEnumerable<int> channels, string eventType = null, params int[] campaigns)
        {
            _channels = channels;
            _campaigns = campaigns;
            _eventType = eventType;

            return this;
        }

        protected internal override IQueryable<EscalationPlan> Execute(IQueryable<EscalationPlan> query)
        {
            if (_channels.Any())
                query = query.Where(x => x.ChannelTags.Any(ca => _channels.Contains(ca.Channel.Id)));
            if (_campaigns.Any())
                query = query.Where(x => x.CampaignTags.Any(ca => _campaigns.Contains(ca.Campaign.Id)));
            if (!string.IsNullOrEmpty(_eventType))
                query = query.Where(x => x.EventEnumType == new EventEnumTypes()[_eventType]);

            return query;
        }
    }
}