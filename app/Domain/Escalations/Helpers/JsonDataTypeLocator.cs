﻿using System;
using System.Collections.Generic;
using Domain.Campaigns.Events;
using iPlatform.Enums.Workflows;

namespace Domain.Escalations.Helpers
{
    public class JsonDataTypeLocator
    {
        public static Dictionary<EventType, Type> Dtos
        {
            get
            {
                return new Dictionary<EventType, Type>
                {
                    { EventType.LeadCallBack, typeof(LeadCallBackEvent) }
                };
            }
        }
    }
}