﻿using System;
using Domain.Base;
using Domain.Users;
using iPlatform.Enums.Escalations;
using MasterData;

namespace Domain.Escalations
{
    public class EscalationPlanExecutionHistory : EntityWithAudit
    {
        public virtual Guid InitialCorrelationId { get; protected internal set; }
        public virtual Guid CorrelationId { get; protected internal set; }
        public virtual Guid ParentCorrelationId { get; protected internal set; }
        public virtual EscalationPlan EscalationPlan { get; protected internal set; }
        public virtual string PlanName { get; protected internal set; }
        public virtual EventEnumType EventEnumType { get; protected internal set; }
        public virtual EscalationPlanStep EscalationPlanStep { get; protected internal set; }
        public virtual string StepName { get; protected internal set; }
        public virtual int Sequence { get; protected internal set; }
        public virtual DelayType DelayType { get; protected internal set; }
        public virtual IntervalType IntervalType { get; protected internal set; }
        public virtual int IntervalDelay { get; protected internal set; }
        public virtual DateTime? DateTimeDelay { get; protected internal set; }
        public virtual EscalationPlanStepWorkflowMessage EscalationPlanStepWorkflowMessage { get; protected internal set; }
        public virtual WorkflowMessageEnumType WorkflowMessageEnumType { get; protected internal set; }
        public virtual User User { get; protected internal set; }
        public virtual EscalationWorkflowMessageStatus EscalationWorkflowMessageStatus { get; protected internal set; }

        protected EscalationPlanExecutionHistory() { }

        public EscalationPlanExecutionHistory(Guid initialCorrelationId, Guid correlationId, Guid parentCorrelationId, 
            EscalationPlanStep escalationPlanStep, EscalationPlanStepWorkflowMessage escalationPlanStepWorkflowMessage, 
            User user, EscalationWorkflowMessageStatus escalationWorkflowMessageStatus)
        {
            InitialCorrelationId = initialCorrelationId;
            CorrelationId = correlationId;
            ParentCorrelationId = parentCorrelationId;
            EscalationPlan = escalationPlanStep.EscalationPlan;
            PlanName = escalationPlanStep.EscalationPlan.Name;
            EventEnumType = escalationPlanStep.EscalationPlan.EventEnumType;
            EscalationPlanStep = escalationPlanStep;
            StepName = escalationPlanStep.Name;
            Sequence = escalationPlanStep.Sequence;
            DelayType = escalationPlanStep.DelayType;
            IntervalType = escalationPlanStep.IntervalType;
            IntervalDelay = escalationPlanStep.IntervalDelay;
            DateTimeDelay = escalationPlanStep.DateTimeDelay;
            EscalationPlanStepWorkflowMessage = escalationPlanStepWorkflowMessage;
            if (escalationPlanStepWorkflowMessage != null)
                WorkflowMessageEnumType = escalationPlanStepWorkflowMessage.WorkflowMessageEnumType;
            User = user;
            EscalationWorkflowMessageStatus = escalationWorkflowMessageStatus;
        }

        public virtual void UpdateStatus(EscalationWorkflowMessageStatus status)
        {
            EscalationWorkflowMessageStatus = status;
        }
    }
}