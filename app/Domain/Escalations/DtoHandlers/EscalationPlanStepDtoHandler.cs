using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Escalations;
using MasterData.Authorisation;

namespace Domain.Escalations.DtoHandlers
{
    public class EscalationPlanStepDtoHandler : BaseDtoHandler<EscalationPlanStepDto, int>
    {
        private readonly IRepository _repository;

        public EscalationPlanStepDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights { get { return new List<RequiredAuthorisationPoint>(); } }

        protected override void InternalHandle(EscalationPlanStepDto dto, HandlerResult<int> result)
        {
            var escalationPlan = _repository.GetById<EscalationPlan>(dto.EscalationPlanId);
            var escalationPlanStep = _repository.GetById<EscalationPlanStep>(dto.Id) ?? new EscalationPlanStep(escalationPlan, dto.Name, dto.Sequence, dto.DelayType, dto.IntervalType, dto.IntervalDelay, dto.DateTimeDelay);
            escalationPlanStep.AddWorkflowMessage(dto.WorkflowMessageTypes.ToArray());

            if (!dto.IsDeleted)
            {
                escalationPlanStep.Name = dto.Name;
                escalationPlanStep.Sequence = dto.Sequence;
                escalationPlanStep.DelayType = dto.DelayType;
                escalationPlanStep.IntervalType = dto.IntervalType;
                escalationPlanStep.IntervalDelay = dto.IntervalDelay;
                if (dto.IntervalDelay > 0)
                    dto.DateTimeDelay = null;
                escalationPlanStep.DateTimeDelay = dto.DateTimeDelay;
                escalationPlanStep.IsDeleted = dto.IsDeleted;
            }
            else
                escalationPlanStep.Delete();

            _repository.Save(escalationPlanStep);
        }
    }
}