﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Extentions;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Base.Workflow;
using Domain.Escalations.Extensions;
using Domain.Escalations.Queries;
using Domain.Escalations.Workflow.Messages;
using Domain.Users;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Enums.Escalations;
using iPlatform.Enums.Workflows;
using MasterData.Authorisation;
using Shared.Extentions;
using Workflow.Messages;

namespace Domain.Escalations.DtoHandlers
{
    public class CreateEscalationDtoHandler : BaseDtoHandler<CreateEscalationDto, int>
    {
        private readonly IWorkflowRouter _router;
        private readonly IRepository _repository;
        private readonly FindEscalationPlanExecutionHistoryQuery _query;

        public CreateEscalationDtoHandler(IProvideContext contextProvider, IRepository repository, IWorkflowRouter router, FindEscalationPlanExecutionHistoryQuery query) : base(contextProvider)
        {
            _router = router;
            _query = query;
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void InternalHandle(CreateEscalationDto dto, HandlerResult<int> result)
        {
            var step = _repository.GetById<EscalationPlanStep>(dto.EscalationPlanStepId);
            if (step == null)
            {
                this.Error(() => string.Format("Could not get {0} {1}", typeof(EscalationPlanStep), dto.EscalationPlanStepId));
                return;
            }

            ExecuteCurrentStep(dto, step);

            var nextStep = step.GetNextStep();
            if (nextStep == null) return;

            ScheduleNextStep(nextStep, dto);
        }

        private void ScheduleNextStep(EscalationPlanStep step, CreateEscalationDto dto)
        {
            var createEscalationWorkflowMessage = new CreateEscalationWorkflowMessage(dto.InitialCorrelationId, step.Id, dto.UserId, dto.EventType,
                dto.ChannelId, step.DelayType, step.IntervalType, step.IntervalDelay, step.DateTimeDelay, dto.JsonData);
            var routingMessage = new WorkflowRoutingMessage().AddMessage(createEscalationWorkflowMessage);

            this.Info(() => string.Format("Publishing {0}", routingMessage.PrintProperties()));
            if (step.DateTimeDelay.HasValue)
                _router.PublishDelayed(routingMessage, step.DateTimeDelay.Value);

            _router.PublishDelayed(routingMessage, step.GetTimeSpanDelay());
            this.Info(() => "Published");
        }

        private void ExecuteCurrentStep(CreateEscalationDto dto, EscalationPlanStep step)
        {
            var routingMessage = new WorkflowRoutingMessage().AddMessage(GetWorkflowMessages(step, dto).ToArray());
            this.Info(() => string.Format("Publishing {0}", routingMessage.PrintProperties()));
            _router.Publish(routingMessage);
            this.Info(() => "Published");
        }

        private IEnumerable<IWorkflowExecutionMessage> GetWorkflowMessages(EscalationPlanStep step, CreateEscalationDto dto)
        {
            foreach (var stepWorkflowMessage in step.EscalationPlanStepWorkflowMessages)
            {
                WorkflowMessageType workflowMessageType;
                if (!Enum.TryParse(stepWorkflowMessage.WorkflowMessageEnumType.Name, out workflowMessageType))
                {
                    this.Error(() => string.Format("{0} {1} could not be parsed", typeof(WorkflowMessageType), dto.EventType));
                    yield break;
                }
                EventType eventType;
                if (!Enum.TryParse(dto.EventType, out eventType))
                {
                    this.Error(() => string.Format("{0} {1} could not be parsed", typeof(EventType), dto.EventType));
                    yield break;
                }
                var workflowMessageDtoData = eventType.ToDto(dto.JsonData);
                if (workflowMessageDtoData == null)
                {
                    this.Error(() => string.Format("Could not map & populate workflow message dto for {0}", eventType));
                    yield break;
                }
                var workflowMessage = workflowMessageType.GetWorkflowMessage(workflowMessageDtoData);
                if (workflowMessage == null)
                {
                    this.Error(() => string.Format("Could not get workflow message for {0}", workflowMessageType));
                    yield break;
                }

                var user = _repository.GetById<User>(dto.UserId);
                if (user == null)
                {
                    this.Error(() => string.Format("Could not get user {0}", dto.UserId));
                    yield break;
                }

                var existing = _query.WithParameters(dto.InitialCorrelationId, dto.EscalationPlanStepId, stepWorkflowMessage.Id, user.Id, EscalationWorkflowMessageStatus.InProgress).ExecuteQuery();
                if (existing.Any())
                {
                    this.Error(() => string.Format(
                    "Duplicate escalation found InitialCorrelationId {0} EscalationPlanStepId {1} EscalationPlanStepWorkflowMessageId {2} UserId {3} EscalationWorkflowMessageStatus {4}",
                    dto.InitialCorrelationId, dto.EscalationPlanStepId, stepWorkflowMessage.Id, user.Id, EscalationWorkflowMessageStatus.InProgress));
                    yield break;
                }

                var hist = new EscalationPlanExecutionHistory(dto.InitialCorrelationId, workflowMessage.CorrelationId, workflowMessage.ParentCorrelationId, step, 
                    stepWorkflowMessage, user, EscalationWorkflowMessageStatus.InProgress);
                _repository.Save(hist);

                workflowMessage.AddSuccessMessage(new EditEscalationWorkflowMessage(hist.Id, EscalationWorkflowMessageStatus.Complete));
                workflowMessage.AddFailureMessage(new EditEscalationWorkflowMessage(hist.Id, EscalationWorkflowMessageStatus.Failed));

                yield return workflowMessage;
            }
        }
    }
}