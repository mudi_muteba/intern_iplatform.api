﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Escalations;
using MasterData.Authorisation;

namespace Domain.Escalations.DtoHandlers
{
    public class EditEscalationDtoHandler : ExistingEntityDtoHandler<EscalationPlanExecutionHistory, EditEscalationDto, int>
    {
        public EditEscalationDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        protected override void HandleExistingEntityChange(EscalationPlanExecutionHistory entity, EditEscalationDto dto, HandlerResult<int> result)
        {
            //todo: add validation
            entity.UpdateStatus(dto.Status);
            result.Processed(entity.Id);
        }
    }
}