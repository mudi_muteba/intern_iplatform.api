﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using iPlatform.Api.DTOs.Escalations;
using MasterData;
using MasterData.Authorisation;

namespace Domain.Escalations.DtoHandlers
{
    public class EscalationPlanDtoHandler : BaseDtoHandler<EscalationPlanDto, int>
    {
        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights { get { return new List<RequiredAuthorisationPoint>(); } }

        public EscalationPlanDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        protected override void InternalHandle(EscalationPlanDto dto, HandlerResult<int> result)
        {
            var @event = new EventEnumTypes()[dto.EventType];
            var channels = Mapper.Map<IEnumerable<int>, IEnumerable<Channel>>(dto.Channels);
            var campaigns = Mapper.Map<IEnumerable<int>, IEnumerable<Campaign>>(dto.Campaigns);
            var escalationPlan = _repository.GetById<EscalationPlan>(dto.Id) ?? new EscalationPlan(dto.Name, dto.EventType);

            if (!dto.IsDeleted)
            {
                escalationPlan.Name = dto.Name;
                escalationPlan.IsDeleted = dto.IsDeleted;
                escalationPlan.EventEnumType = @event;
                escalationPlan.TagWithChannel(channels.ToArray());
                escalationPlan.TagWithCampaign(campaigns.ToArray());
            }
            else
                escalationPlan.Delete();

            _repository.Save(escalationPlan);
        }
    }
}