﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Enums.Escalations;
using Infrastructure.NHibernate.Attributes;
using MasterData;

namespace Domain.Escalations
{
    public class EscalationPlanStep : EntityWithAudit, IEscalationDelay
    {
        public virtual string Name { get; protected internal set; }
        public virtual int Sequence { get; protected internal set; }
        public virtual DelayType DelayType { get; protected internal set; }
        public virtual IntervalType IntervalType { get; protected internal set; }
        public virtual int IntervalDelay { get; protected internal set; }
        public virtual DateTime? DateTimeDelay { get; protected internal set; }
        public virtual EscalationPlan EscalationPlan { get; protected internal set; }
        private ISet<EscalationPlanStepWorkflowMessage> _escalationPlanStepWorkflowMessages = new HashSet<EscalationPlanStepWorkflowMessage>();
        public virtual ISet<EscalationPlanStepWorkflowMessage> EscalationPlanStepWorkflowMessages
        {
            get { return _escalationPlanStepWorkflowMessages; }
            protected internal set { _escalationPlanStepWorkflowMessages = value; }
        }

        [DoNotMap]
        public virtual EscalationPlanStep GetNextStep()
        {
            return EscalationPlan.EscalationPlanSteps.OrderBy(x => x.Sequence).FirstOrDefault(x => x.Sequence > Sequence);
        }

        protected EscalationPlanStep() { }

        public EscalationPlanStep(EscalationPlan escalationPlan, string name, int sequence, DelayType delayType, IntervalType intervalType, int intervalDelay, DateTime? dateTimeDelay)
        {
            EscalationPlan = escalationPlan;
            Name = name;
            Sequence = sequence;
            DelayType = delayType;
            IntervalType = intervalType;
            IntervalDelay = intervalDelay;
            DateTimeDelay = dateTimeDelay;
        }

        public virtual void AddWorkflowMessage(params string[] workflowMessageEnumTypes)
        {
            foreach (var workflowMessageType in workflowMessageEnumTypes)
            {
                var existing = _escalationPlanStepWorkflowMessages.FirstOrDefault(x => x.WorkflowMessageEnumType == new WorkflowMessageEnumTypes()[workflowMessageType] && x.EscalationPlanStep.Id == Id);
                if (existing != null) continue;

                existing = new EscalationPlanStepWorkflowMessage(this, workflowMessageType);
                _escalationPlanStepWorkflowMessages.Add(existing);
            }

            var messageTypes = workflowMessageEnumTypes.Select(workflowMessageType => new WorkflowMessageEnumTypes()[workflowMessageType]);
            var messagesToDelete = _escalationPlanStepWorkflowMessages.Where(x => !messageTypes.Contains(x.WorkflowMessageEnumType));
            foreach (var message in messagesToDelete)
                message.Delete();
        }

        public virtual void DeleteWorkflowMessage(string workflowMessageEnumType)
        {
            var existing = _escalationPlanStepWorkflowMessages.FirstOrDefault(x => x.WorkflowMessageEnumType == new WorkflowMessageEnumTypes()[workflowMessageEnumType] && x.EscalationPlanStep.Id == Id);
            if (existing == null) return;

            existing.Delete();
        } 
    }
}