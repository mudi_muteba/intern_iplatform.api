﻿using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using Domain.Escalations.Workflow.Messages;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Escalations;

namespace Domain.Escalations.Mappings
{
    public class EscalationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<EscalationPlanExecutionHistory, EscalationHistoryDto>();
            Mapper.CreateMap<PagedResults<EscalationPlanExecutionHistory>, PagedResultDto<EscalationHistoryDto>>();

            Mapper.CreateMap<EscalationPlan, EscalationPlanDto>()
                .ForMember(d => d.EventType, opt => opt.MapFrom(s => s.EventEnumType.EnumType))
                .ForMember(d => d.Channels, opt => opt.MapFrom(s => s.Channels.Select(x => x.Id)))
                .ForMember(d => d.Campaigns, opt => opt.MapFrom(s => s.Campaigns.Select(x => x.Id)));
            Mapper.CreateMap<PagedResults<EscalationPlan>, PagedResultDto<EscalationPlanDto>>();

            Mapper.CreateMap<EscalationPlanStep, EscalationPlanStepDto>()
                .ForMember(d => d.WorkflowMessageTypes, s => s.MapFrom(x => x.EscalationPlanStepWorkflowMessages.Select(m => m.WorkflowMessageEnumType.EnumType).ToList()));
            Mapper.CreateMap<PagedResults<EscalationPlanStep>, PagedResultDto<EscalationPlanStepDto>>();

            Mapper.CreateMap<CreateEscalationWorkflowMessage, CreateEscalationDto>();
            Mapper.CreateMap<CreateEscalationDto, CreateEscalationWorkflowMessage>();
        }
    }
}