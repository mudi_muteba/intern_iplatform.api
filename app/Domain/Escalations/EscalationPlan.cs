﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base;
using Domain.Campaigns;
using Domain.CampaignTags;
using iPlatform.Enums.Escalations;
using Infrastructure.NHibernate.Attributes;
using MasterData;

namespace Domain.Escalations
{
    public class EscalationPlan : EntityWithAudit
    {
        public virtual string Name { get; protected internal set; }
        public virtual EventEnumType EventEnumType { get; protected internal set; }

        private ISet<EscalationPlanStep> _escalationPlanSteps = new HashSet<EscalationPlanStep>();
        public virtual ISet<EscalationPlanStep> EscalationPlanSteps
        {
            get { return _escalationPlanSteps; }
            protected internal set { _escalationPlanSteps = value; }
        }

        private ISet<CampaignTag> _campaignTags = new HashSet<CampaignTag>();
        public virtual ISet<CampaignTag> CampaignTags
        {
            get { return _campaignTags; }
            set { _campaignTags = value; }
        }

        [DoNotMap]
        public virtual IEnumerable<Campaign> Campaigns
        {
            get
            {
                return CampaignTags != null
                    ? CampaignTags.Where(x => x.IsDeleted != true).Select(x => x.Campaign).ToList()
                    : Enumerable.Empty<Campaign>().ToList();
            }
        }

        [DoNotMap]
        public virtual bool HasStep(int sequence)
        {
            return this[sequence] != null;
        }

        [DoNotMap]
        public virtual EscalationPlanStep this[int sequence]
        {
            get
            {
                return _escalationPlanSteps.FirstOrDefault(x => x.Sequence == sequence);
            }
        }

        [DoNotMap]
        public virtual IEnumerable<int> Sequences
        {
            get
            {
                return _escalationPlanSteps.Select(x => x.Sequence).OrderBy(x => x).ToList();
            }
        }

        protected EscalationPlan() { }

        public EscalationPlan(string name, string eventType)
        {
            Name = name;
            EventEnumType = new EventEnumTypes()[eventType];
        }

        public virtual EscalationPlanStep AddEscalationPlanStep(string name, int sequence, DelayType delayType, IntervalType intervalType, int intervalDelay, DateTime? dateTimeDelay = null)
        {
            var existing = _escalationPlanSteps.FirstOrDefault(x => x.Name == name && x.Sequence == sequence && x.EscalationPlan.Id == Id);
            if (existing != null) return existing;

            existing = new EscalationPlanStep(this, name, sequence, delayType, intervalType, intervalDelay, dateTimeDelay);
            _escalationPlanSteps.Add(existing);

            return existing;
        }

        public virtual void DeleteStep(string name, int sequence)
        {
            var existing = _escalationPlanSteps.FirstOrDefault(x => x.Name == name && x.Sequence == sequence && x.EscalationPlan.Id == Id);
            if (existing == null) return;

            existing.Delete();
        }

        public virtual void TagWithCampaign(params Campaign[] campaigns)
        {
            foreach (var campaignTag in CampaignTags)
                campaignTag.Delete();

            foreach (var campaign in campaigns)
            {
                var campaignTag = CampaignTags.FirstOrDefault(x => x.Campaign.Id == campaign.Id);
                if (campaignTag == null)
                    CampaignTags.Add(new CampaignTag(campaign));
                else
                    campaignTag.IsDeleted = false;
            }
        }
    }
}