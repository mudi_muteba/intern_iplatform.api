﻿using System;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Enums.Escalations;
using Workflow.Messages;

namespace Domain.Escalations.Workflow.Messages
{
    public class CreateEscalationWorkflowMessage : WorkflowExecutionMessage, IEscalationDelay
    {
        public Guid InitialCorrelationId { get; set; }
        public int EscalationPlanStepId { get; set; }
        public string EventType { get; set; }
        public int ChannelId { get; set; }
        public DateTime LeadCallBackDate { get; set; }
        public DelayType DelayType { get; set; }
        public IntervalType IntervalType { get; set; }
        public int IntervalDelay { get; set; }
        public DateTime? DateTimeDelay { get; set; }
        public string JsonData { get; set; }

        public CreateEscalationWorkflowMessage() { }

        public CreateEscalationWorkflowMessage(Guid initialCorrelationId, int escalationPlanStepId, int userId, string eventType, int channelId, DelayType delayType, 
            IntervalType intervalType, int intervalDelay, DateTime? dateTimeDelay, string jsonData, 
            WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
            InitialCorrelationId = initialCorrelationId;
            EscalationPlanStepId = escalationPlanStepId;
            UserId = userId;
            EventType = eventType;
            ChannelId = channelId;
            DelayType = delayType;
            IntervalType = intervalType;
            IntervalDelay = intervalDelay;
            DateTimeDelay = dateTimeDelay;
            JsonData = jsonData;
        }
    }
}