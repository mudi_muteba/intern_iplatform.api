﻿using iPlatform.Enums.Escalations;
using Workflow.Messages;

namespace Domain.Escalations.Workflow.Messages
{
    public class EditEscalationWorkflowMessage : WorkflowExecutionMessage
    {
        public int EscalationPlanExecutionHistoryId { get; set; }
        public EscalationWorkflowMessageStatus Status { get; set; }

        public EditEscalationWorkflowMessage() { }

        public EditEscalationWorkflowMessage(int escalationPlanExecutionHistoryId, EscalationWorkflowMessageStatus status, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
            EscalationPlanExecutionHistoryId = escalationPlanExecutionHistoryId;
            Status = status;
        }
    }
}