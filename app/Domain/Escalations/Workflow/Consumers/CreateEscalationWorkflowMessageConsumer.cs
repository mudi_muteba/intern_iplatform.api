﻿using System.Configuration;
using AutoMapper;
using Domain.Base.Extentions;
using Domain.Escalations.Workflow.Messages;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Enums;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Escalations.Workflow.Consumers
{
    public class CreateEscalationWorkflowMessageConsumer : AbstractMessageConsumer<CreateEscalationWorkflowMessage>
    {
        private readonly IConnector _connector;

        public CreateEscalationWorkflowMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
        }

        public override void Consume(CreateEscalationWorkflowMessage message)
        {
            this.Info(() => string.Format("Consuming message: {0}", message.PrintProperties()));
            if (message == null)
            {
                this.Error(() => string.Format("CreateEscalationWorkflowMessage is null & can therefore not schedule escalation. CreateEscalationWorkflowMessage Id: {0}", message.CorrelationId));
                return;
            }

            this.Info(() => string.Format("Calling CreateEscalation API endpoint UserId:{0} EventType:{1} ChannelId:{2}", message.UserId, message.EventType, message.ChannelId));
            
            var createEscalationDto = Mapper.Map<CreateEscalationWorkflowMessage, CreateEscalationDto>(message);
            if (createEscalationDto == null)
            {
                this.Error(() => string.Format("Error mapping {0} to {1} {2}", typeof(CreateEscalationWorkflowMessage), typeof(CreateEscalationDto), message.PrintProperties()));
                return;
            }

            var userName = ConfigurationManager.AppSettings[@"iPlatform/connector/userName"];
            var userPassword = ConfigurationManager.AppSettings[@"iPlatform/connector/apiKey"];
            var token = _connector.Authentication.Authenticate(userName, userPassword, "local", "", ApiRequestType.Workflow);
            _connector.SetToken(token.Token);

            var response = _connector.EscalationManagement.Escalations.CreateEscalation(createEscalationDto);
            if (!response.IsSuccess)
            {
                foreach (var error in response.Errors)
                    this.Error(() => string.Format("Calling CreateEscalation API endpoint error: {0}", error.DefaultMessage));
            }

            this.Info(() => string.Format("Called CreateEscalation API endpoint UserId:{0} EventType:{1} ChannelId:{2}", message.UserId, message.EventType, message.ChannelId));
        }
    }
}