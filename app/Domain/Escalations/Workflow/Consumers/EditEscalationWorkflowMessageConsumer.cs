using System.Configuration;
using Domain.Escalations.Workflow.Messages;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Escalations;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Escalations.Workflow.Consumers
{
    public class EditEscalationWorkflowMessageConsumer : AbstractMessageConsumer<EditEscalationWorkflowMessage>
    {
        private readonly IConnector _connector;

        public EditEscalationWorkflowMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
        }

        public override void Consume(EditEscalationWorkflowMessage message)
        {
            _connector
                .EscalationManagement
                .Escalation(message.EscalationPlanExecutionHistoryId)
                .Edit(new EditEscalationDto(message.EscalationPlanExecutionHistoryId, message.Status));
        }
    }
}