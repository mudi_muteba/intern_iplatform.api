﻿using System;
using Domain.Base.Extentions;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Enums.Escalations;
using Shared.Extentions;

namespace Domain.Escalations.Extensions
{
    public static class EscalationExtensions
    {
        public static TimeSpan GetTimeSpanDelay(this IEscalationDelay escalationDelay)
        {
            switch (escalationDelay.IntervalType)
            {
                case IntervalType.Minutes:
                    return TimeSpan.FromMinutes(escalationDelay.IntervalDelay);
                case IntervalType.Hours:
                    return TimeSpan.FromHours(escalationDelay.IntervalDelay);
                case IntervalType.Days:
                    return TimeSpan.FromDays(escalationDelay.IntervalDelay);
            }

            typeof (EscalationExtensions).Error(() => string.Format("TimeSpan could not be set, using delay of 1 second  {0}", escalationDelay.PrintProperties()));

            return TimeSpan.FromSeconds(1);
        }
    }
}