﻿using Domain.Escalations.Helpers;
using iPlatform.Enums.Workflows;
using Newtonsoft.Json;
using Shared.Extentions;

namespace Domain.Escalations.Extensions
{
    public static class EventTypeExtensions
    {
        public static object ToDto(this EventType enumType, string jsonData)
        {
            var type = JsonDataTypeLocator.Dtos[enumType];
            if (type == null)
            {
                typeof(EventTypeExtensions).Error(() => string.Format("No matching type found for {0} ", enumType));
                return null;
            }

            var dto = JsonConvert.DeserializeObject(jsonData, type);
            if (dto == null)
            {
                typeof(EventTypeExtensions).Error(() => string.Format("Could not Deserialize Object {0} by type {1}", jsonData, enumType));
                return null;
            }

            return dto;
        }
    }
}