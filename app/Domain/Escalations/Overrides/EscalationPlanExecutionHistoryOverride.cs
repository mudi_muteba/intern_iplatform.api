﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Escalations.Overrides
{
    public class EscalationPlanExecutionHistoryOverride : IAutoMappingOverride<EscalationPlanExecutionHistory>
    {
        public void Override(AutoMapping<EscalationPlanExecutionHistory> mapping)
        {
            mapping.References(x => x.EscalationPlanStepWorkflowMessage);
        }
    }
}