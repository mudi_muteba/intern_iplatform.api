﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Escalations.Overrides
{
    public class EscalationPlanOverride : IAutoMappingOverride<EscalationPlan>
    {
        public void Override(AutoMapping<EscalationPlan> mapping)
        {
            mapping.HasMany(x => x.EscalationPlanSteps).Cascade.SaveUpdate();
            mapping.HasMany(x => x.ChannelTags).Cascade.SaveUpdate();
            mapping.HasMany(x => x.CampaignTags).Cascade.SaveUpdate();
        }
    }
}