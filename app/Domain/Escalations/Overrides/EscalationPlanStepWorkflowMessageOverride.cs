﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Escalations.Overrides
{
    public class EscalationPlanStepWorkflowMessageOverride : IAutoMappingOverride<EscalationPlanStepWorkflowMessage>
    {
        public void Override(AutoMapping<EscalationPlanStepWorkflowMessage> mapping)
        {
            
        }
    }
}