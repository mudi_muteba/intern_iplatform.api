﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Escalations.Overrides
{
    public class EscalationPlanStepOverride : IAutoMappingOverride<EscalationPlanStep>
    {
        public void Override(AutoMapping<EscalationPlanStep> mapping)
        {
            mapping.References(x => x.EscalationPlan).Cascade.None();
            mapping.HasMany(x => x.EscalationPlanStepWorkflowMessages).Cascade.SaveUpdate();
        }
    }
}