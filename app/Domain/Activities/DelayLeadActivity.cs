﻿using Domain.Users;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Activities
{
    public class DelayLeadActivity : LeadActivity
    {
        public DelayLeadActivity()
        {
            ActivityType = ActivityTypes.Delayed;
        }

        public DelayLeadActivity(User user, Party.Party party)
            : base(user, party)
        {
            User = user;
            ActivityType = ActivityTypes.Delayed;
        }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.Delayed; }
        }
    }
}
