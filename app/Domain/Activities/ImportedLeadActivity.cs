﻿using System;
using Domain.Campaigns;
using Domain.Party.ProposalHeaders;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class ImportedLeadActivity : LeadActivity
    {
        public ImportedLeadActivity()
        {
            ActivityType = ActivityTypes.Imported;
        }
        public ImportedLeadActivity(User user, Party.Party party, Campaign campaign = null, ImportType importtype = null, bool existing = false)
            : base(user, party)
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
            ActivityType = ActivityTypes.Imported;
            ImportType = importtype ?? ImportTypes.Excel;
            Existing = existing;
        }

        public virtual new LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.Imported; }
        }

        public virtual ImportType ImportType { get; set; }

        public virtual bool Existing { get; set; }

    }
}