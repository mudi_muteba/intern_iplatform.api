﻿using System;
using Domain.Campaigns;
using Domain.Party.ProposalHeaders;
using Domain.Users;
using Infrastructure.NHibernate.Attributes;
using MasterData;

namespace Domain.Activities
{
    public class ProposalLeadActivity : LeadActivity
    {
        public ProposalLeadActivity()
        {
            ActivityType = ActivityTypes.ProposalCreated;
        }
        [DoNotMap]
        public virtual ProposalHeader ProposalHeader { get; protected internal set; }
        public ProposalLeadActivity(User user, Party.Party party,ProposalHeader header, Campaign campaign)
            : base(user, party)
        {
            ProposalHeader = header;
            Campaign = campaign;
            CampaignSourceId = campaign != null ? campaign.Id : 0;
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
            ActivityType = ActivityTypes.ProposalCreated;
        }

        public virtual new LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.Proposal; }
        }
    }
}