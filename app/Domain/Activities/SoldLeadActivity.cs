﻿
using Domain.Campaigns;
using Domain.Party.CustomerSatisfactionSurveys;
using Domain.Party.Quotes;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class SoldLeadActivity : LeadActivity
    {
        public SoldLeadActivity()
        {
            ActivityType = ActivityTypes.Sold;
        }

        public SoldLeadActivity(User user, Party.Party party, Campaign campaign, Quote quote)
            : base(user, party)
        {
            ActivityType = ActivityTypes.Sold;
            Campaign = campaign;
            CampaignSourceId = campaign.Id;
            Quote = quote;
        }

        public virtual Quote Quote { get; set; }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.Sold; }
        }
    }
}
