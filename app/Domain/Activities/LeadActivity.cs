﻿using System;
using AutoMapper;
using Domain.Activities.Mappings;
using Domain.Base;
using Domain.Campaigns;
using Domain.Campaigns.Mappings;
using Domain.Leads;
using Domain.Users;
using iPlatform.Api.DTOs.Activities;
using Infrastructure.Configuration;
using MasterData;

namespace Domain.Activities
{
    public class LeadActivity : Entity, ILeadActivity
    {

        public LeadActivity()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }

        protected LeadActivity(User user, Party.Party party)
            : this()
        {
            User = user;
            Party = party;
            Lead = party.Lead; 
        }

        public virtual Lead Lead { get; set; }
        public virtual Campaign Campaign { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual DateTime DateUpdated { get; set; }

        public virtual int CampaignSourceId { get; set; }
        public virtual string CampaignReference { get; set; }
        public virtual User User { get; set; }
        public virtual Party.Party Party { get; set; }
        public virtual ActivityType ActivityType { get; set; }

        private LeadStatus tmpStatus;
        public virtual LeadStatus GetImpliedStatus
        {
            get { return null; }
            set {   tmpStatus = value; }
        }

        public virtual void AllocateToCampaign(Campaign campaign, string campaignReference = "")
        {
            CampaignReference = campaignReference;
            CampaignSourceId = campaign != null? campaign.Id: 0;
            Campaign = campaign;
            if (Campaign != null)
                return;

            //Find and assign default campaign
            Campaign = Mapper.Map<CampaignDefaultDto, Domain.Campaigns.Campaign>(new CampaignDefaultDto());
        }

        public virtual void Disable(DisableLeadActivityDto dto)
        {
            Delete();

            //raise event
        }
    }
}
