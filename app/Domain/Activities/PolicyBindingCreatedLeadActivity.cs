﻿using Domain.Party.Quotes;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class PolicyBindingCreatedLeadActivity : LeadActivity
    {
        public PolicyBindingCreatedLeadActivity()
        {
            ActivityType = ActivityTypes.PolicyBindingCreated;
        }

        public virtual Quote Quote { get; set; }

        public PolicyBindingCreatedLeadActivity(User user, Party.Party party, Quote quote)
            : base(user, party)
        {
            User = user;
            ActivityType = ActivityTypes.PolicyBindingCreated;
            Quote = quote;
        }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.PolicyBindingCreated; }
        }
    }
}
