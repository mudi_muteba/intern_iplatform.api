﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class SentForUnderwritingLeadActivityOverride : IAutoMappingOverride<SentForUnderwritingLeadActivity>
    {
        public void Override(AutoMapping<SentForUnderwritingLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(a => a.Quote);
        }
    }
}