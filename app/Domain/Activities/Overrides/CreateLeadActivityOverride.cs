﻿using Domain.Individuals;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class CreateLeadActivityOverride : IAutoMappingOverride<CreateLeadActivity>
    {
        public void Override(AutoMapping<CreateLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
        }
    }
}