﻿using Domain.Activities;
using Domain.Individuals;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class ImportedLeadActivityOverride : IAutoMappingOverride<ImportedLeadActivity>
    {
        public void Override(AutoMapping<ImportedLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);


        }
    }
}