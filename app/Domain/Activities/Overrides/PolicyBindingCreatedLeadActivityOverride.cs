﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class PolicyBindingCreatedLeadActivityOverride : IAutoMappingOverride<PolicyBindingCreatedLeadActivity>
    {
        public void Override(AutoMapping<PolicyBindingCreatedLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(x => x.Quote).NotFound.Ignore();
        }
    }
}