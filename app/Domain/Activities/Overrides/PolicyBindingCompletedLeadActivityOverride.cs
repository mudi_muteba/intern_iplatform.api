﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class PolicyBindingCompletedLeadActivityOverride : IAutoMappingOverride<PolicyBindingCompletedLeadActivity>
    {
        public void Override(AutoMapping<PolicyBindingCompletedLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(x => x.Quote).NotFound.Ignore();
        }
    }
}