﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class DeadLeadActivityOverride : IAutoMappingOverride<DeadLeadActivity>
    {
        public void Override(AutoMapping<DeadLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
          ;
        }
    }
}