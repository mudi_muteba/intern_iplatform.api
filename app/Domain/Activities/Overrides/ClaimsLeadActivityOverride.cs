﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class ClaimsLeadActivityOverride : IAutoMappingOverride<ClaimsLeadActivity>
    {
        public void Override(AutoMapping<ClaimsLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(x => x.ClaimsHeader).NotFound.Ignore();
        }
    }
}