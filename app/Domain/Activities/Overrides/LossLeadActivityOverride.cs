﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class LossLeadActivityOverride : IAutoMappingOverride<LossLeadActivity>
    {
        public void Override(AutoMapping<LossLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
        }
    }
}