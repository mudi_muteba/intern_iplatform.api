﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class PolicyLeadActivityOverride : IAutoMappingOverride<PolicyLeadActivity>
    {
        public void Override(AutoMapping<PolicyLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(x => x.PolicyHeader).NotFound.Ignore();
        }
    }
}