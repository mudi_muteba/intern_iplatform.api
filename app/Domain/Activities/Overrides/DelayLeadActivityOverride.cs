﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Activities.Overrides
{
    public class DelayLeadActivityOverride : IAutoMappingOverride<DelayLeadActivity>
    {
        public void Override(AutoMapping<DelayLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a => a.GetImpliedStatus);
        }
    }

}
