﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class RejectedAtUnderwritingLeadActivityOverride : IAutoMappingOverride<RejectedAtUnderwritingLeadActivity>
    {
        public void Override(AutoMapping<RejectedAtUnderwritingLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
        }
    }
}