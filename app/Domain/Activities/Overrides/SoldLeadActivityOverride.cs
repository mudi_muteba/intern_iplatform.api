﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class SoldLeadActivityOverride : IAutoMappingOverride<SoldLeadActivity>
    {
        public void Override(AutoMapping<SoldLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(x => x.User).Cascade.None();
        }
    }
}