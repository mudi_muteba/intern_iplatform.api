﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class QuoteAcceptedLeadActivityOverride : IAutoMappingOverride<QuoteAcceptedLeadActivity>
    {
        public void Override(AutoMapping<QuoteAcceptedLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);

            mapping.References(x => x.Quote).NotFound.Ignore();
        }
    }
}