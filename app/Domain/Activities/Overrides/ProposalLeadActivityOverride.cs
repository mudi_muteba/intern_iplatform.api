﻿using Domain.Individuals;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class ProposalLeadActivityOverride : IAutoMappingOverride<ProposalLeadActivity>
    {
        public void Override(AutoMapping<ProposalLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(x => x.ProposalHeader, "ProposalHeaderId")
                .Cascade.SaveUpdate()
                .NotFound
                .Ignore();
        }
    }
}