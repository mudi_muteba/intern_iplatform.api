﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class QuotedLeadActivityOverride : IAutoMappingOverride<QuotedLeadActivity>
    {
        public void Override(AutoMapping<QuotedLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(x => x.QuoteHeader).NotFound.Ignore();
        }
    }
}