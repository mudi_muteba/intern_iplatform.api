﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;
using MasterData;

namespace Domain.Activities.Overrides
{

    public class LeadActivityOverride : IAutoMappingOverride<LeadActivity>
    {
        public void Override(AutoMapping<LeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(x => x.Campaign, "CampaignId").LazyLoad(Laziness.NoProxy); ;
            mapping.References(x => x.User, "UserId").LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.Party, "PartyId").LazyLoad(Laziness.NoProxy); ;
            mapping.References(x => x.Lead, "LeadId").Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy); ;

        }
    }
}