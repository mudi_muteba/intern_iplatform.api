﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Activities.Overrides
{

    public class SurveyRespondedLeadActivityOverride : IAutoMappingOverride<SurveyRespondedLeadActivity>
    {
        public void Override(AutoMapping<SurveyRespondedLeadActivity> mapping)
        {
            mapping.IgnoreProperty(a=>a.GetImpliedStatus);
            mapping.References(x => x.CustomerSatisfactionSurvey).NotFound.Ignore();
        }
    }
}