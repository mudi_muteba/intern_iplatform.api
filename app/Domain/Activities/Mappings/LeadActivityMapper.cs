﻿using AutoMapper;
using Domain.Base.Repository;
using System.Collections.Generic;
using System.Linq;
using Domain.Campaigns;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using MasterData;
using Domain.Leads.Builder;
using iPlatform.Api.DTOs.Campaigns;

namespace Domain.Activities.Mappings
{

    public class LeadActivityMapper : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<ActivityType, ActivityTypeDto>();

            Mapper.CreateMap<LeadActivity, ListLeadActivityDto>()
                .ForMember(t => t.Lead, o => o.Ignore())
                ;

            Mapper.CreateMap<PagedResults<LeadActivity>, PagedResultDto<ListLeadActivityDto>>();

            Mapper.CreateMap<PagedResults<LeadActivity>, PagedResultDto<LeadActivityDto>>()
                .AfterMap((s, d) =>
                {
                    d.Results.ForEach(x =>
                    {
                        var firstOrDefault = s.Results.FirstOrDefault(a => a.Id == x.Id);
                        if (firstOrDefault != null)
                            x.CampaignLeadBuckets.AddRange(firstOrDefault.Lead.CampaignLeadBuckets.Select(Mapper.Map<ListCampaignLeadBucketDto>));
                    });
                })
                ;

            Mapper.CreateMap<List<LeadActivity>, ListResultDto<LeadActivityDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                .AfterMap((s, d) =>
                {
                    d.Results.ForEach(x =>
                    {
                        var firstOrDefault = s.FirstOrDefault(a => a.Id == x.Id);
                        if (firstOrDefault != null)
                            x.CampaignLeadBuckets.AddRange(firstOrDefault.Lead.CampaignLeadBuckets.Select(Mapper.Map<ListCampaignLeadBucketDto>));
                    });
                });

            Mapper.CreateMap<List<LeadActivity>, List<ListLeadActivityDto>>();

            Mapper.CreateMap<LeadActivity, LeadActivityDto>()
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.CampaignLeadBuckets , o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    d.CampaignLeadBuckets.AddRange(s.Lead.CampaignLeadBuckets.Select(Mapper.Map<ListCampaignLeadBucketDto>));
                })
                ;

            Mapper.CreateMap<CreateLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Description, o => o.MapFrom(s => s.Description ?? ""))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<ClaimsLeadActivity, LeadActivityDto>()
                .ForMember(t => t.ClaimsHeader, o => o.MapFrom(s => s.ClaimsHeader))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<DeadLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<DelayLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<LossLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<PolicyLeadActivity, LeadActivityDto>()
                .ForMember(t => t.PolicyHeader, o => o.MapFrom(s => s.PolicyHeader))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<ProposalLeadActivity, LeadActivityDto>()
                .ForMember(t => t.ProposalHeader, o => o.MapFrom(s => s.ProposalHeader))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<QuoteAcceptedLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Quote, o => o.MapFrom(s => s.Quote))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<QuotedLeadActivity, LeadActivityDto>()
                .ForMember(t => t.QuoteHeader, o => o.MapFrom(s => s.QuoteHeader))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<ImportedLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                ;

            Mapper.CreateMap<SurveyRespondedLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Quote, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.CustomerSatisfactionSurvey, o => o.MapFrom(s => s.CustomerSatisfactionSurvey))
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<SentForUnderwritingLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Quote, o => o.MapFrom(s => s.Quote))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<RejectedAtUnderwritingLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Quote, o => o.MapFrom(s => s.Quote))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<SoldLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Quote, o => o.MapFrom(s => s.Quote))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<PolicyBindingCreatedLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Quote, o => o.MapFrom(s => s.Quote))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;

            Mapper.CreateMap<PolicyBindingCompletedLeadActivity, LeadActivityDto>()
                .ForMember(t => t.Quote, o => o.MapFrom(s => s.Quote))
                .ForMember(t => t.Lead, o => o.MapFrom(s => s.Lead.Build()))
                .ForMember(t => t.ClaimsHeader, o => o.Ignore())
                .ForMember(t => t.PolicyHeader, o => o.Ignore())
                .ForMember(t => t.ProposalHeader, o => o.Ignore())
                .ForMember(t => t.QuoteHeader, o => o.Ignore())
                .ForMember(t => t.Party, o => o.Ignore())
                .ForMember(t => t.ImportType, o => o.Ignore())
                .ForMember(t => t.Existing, o => o.Ignore())
                ;
        }
    }
}