﻿
using Domain.Claims.ClaimsHeaders;
using Domain.Party.Quotes;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class ClaimsLeadActivity : LeadActivity
    {
        protected ClaimsLeadActivity()
        {
            ActivityType = ActivityTypes.ClaimsCreated;
        }

        public ClaimsLeadActivity(ClaimsHeader claimsHeader)
            : this()
        {
            ActivityType = ActivityTypes.ClaimsCreated;
            ClaimsHeader = claimsHeader;
        }

        public virtual ClaimsHeader ClaimsHeader { get; set; }
    }
}
