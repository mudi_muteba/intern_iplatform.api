﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using MasterData.Authorisation;
using Infrastructure.Configuration;

namespace Domain.Activities.Queries
{
    public class SearchLeadActivitiesQuery : BaseQuery<LeadActivity>, ISearchQuery<LeadActivitySearchDto>
    {
        private LeadActivitySearchDto _criteria;

        public SearchLeadActivitiesQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<LeadActivity>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    CampaignAuthorisation.List
                };
            }
        }

        public void WithCriteria(LeadActivitySearchDto criteria)
        {
            _criteria = criteria;
        }

        protected internal override IQueryable<LeadActivity> Execute(IQueryable<LeadActivity> query)
        {
            query = query.Where(x => !x.Lead.IsDeleted);
            query = query.Where(x => x.Lead.CampaignLeadBuckets.Any(y => ContextProvider.Get().Channels.Contains(y.Campaign.Channel.Id)));
            if (_criteria.ActivityType != null)
                query = query.Where(x => x.ActivityType == _criteria.ActivityType);

            if (_criteria.LeadId != null)
                query = query.Where(x => x.Lead.Id == _criteria.LeadId);

            if (_criteria.Id != null)
                query = query.Where(x => x.Id == _criteria.Id);

            if (_criteria.PartyId != null)
                query = query.Where(x => x.Lead.Party.Id == _criteria.PartyId);

            if (!string.IsNullOrEmpty(_criteria.Name))
            {
                if (_criteria.Name.Any(char.IsDigit))
                {
                    query = query.Where(x =>
                        (x.Lead.Party as Individual).IdentityNo.Contains(_criteria.Name) ||
                            x.Party.ContactDetail.Cell.StartsWith(_criteria.Name) ||
                            x.Party.ContactDetail.Home.StartsWith(_criteria.Name) ||
                            x.Party.ContactDetail.Work.StartsWith(_criteria.Name) ||
                            x.Party.ContactDetail.Direct.StartsWith(_criteria.Name) ||
                            x.Party.ContactDetail.Fax.StartsWith(_criteria.Name)
                            );
                }
                else if (_criteria.Name.Contains(","))
                {
                    var names = _criteria.Name.Split(',');
                    var firstname = names.Count() == 2 ? names[1] : "";
                    query = query.Where(x =>
                        (x.Lead.Party as Individual).FirstName.StartsWith(firstname) ||
                        (x.Lead.Party as Individual).Surname.StartsWith(names[0]));
                }
                else
                {
                    query = query.Where(x =>
                               (x.Lead.Party as Individual).FirstName.StartsWith(_criteria.Name) ||
                               (x.Lead.Party as Individual).Surname.StartsWith(_criteria.Name));
                }
            }

            if (!string.IsNullOrEmpty(_criteria.Phone))
            {
                query = query.Where(x => 
                    x.Party.ContactDetail.Cell.StartsWith(_criteria.Phone) ||
                    x.Party.ContactDetail.Home.StartsWith(_criteria.Phone) ||
                    x.Party.ContactDetail.Work.StartsWith(_criteria.Phone) ||
                    x.Party.ContactDetail.Direct.StartsWith(_criteria.Phone) ||
                    x.Party.ContactDetail.Fax.StartsWith(_criteria.Phone) 
                    );
            }

            if(ConfigurationReader.Platform.InsurerCode == "AIG")
            {
                if (_criteria.CampaignId > 0)
                    query = query.Where(x => x.Campaign.Id == _criteria.CampaignId);
            }
            else
            {
                if (_criteria.ChannelId > 0)
                    query = query.Where(x => x.Lead.CampaignLeadBuckets.Any(y => y.Campaign.Channel.Id == _criteria.ChannelId));

                if (_criteria.CampaignId > 0)
                    query = query.Where(x => x.Lead.CampaignLeadBuckets.Any(y => y.Campaign.Id == _criteria.CampaignId) && x.Campaign.Id == _criteria.CampaignId);
            }

            if (_criteria.AgentId != null)
                query = query.Where(x => x.Lead.CampaignLeadBuckets.Any(y => y.Agent.Id == _criteria.AgentId));

            if (_criteria.OrderBy.Any(x => x.FieldName.Equals("Name", StringComparison.InvariantCultureIgnoreCase)))
                query = _criteria.OrderBy.First(x => x.FieldName.Equals("Name", StringComparison.InvariantCultureIgnoreCase)).Direction == OrderByDirection.Ascending 
                    ? query.OrderBy(x => (x.Lead.Party as Individual).Surname) 
                    : query.OrderByDescending(x => (x.Lead.Party as Individual).Surname);

            if (_criteria.OrderBy.Any(x => x.FieldName.Equals("Added", StringComparison.InvariantCultureIgnoreCase)))
                query = _criteria.OrderBy.First(x => x.FieldName.Equals("Added", StringComparison.InvariantCultureIgnoreCase)).Direction == OrderByDirection.Ascending 
                    ? query.OrderBy(x => x.DateCreated) 
                    : query.OrderByDescending(x => x.DateCreated);

            if (_criteria.OrderBy.Any(x => x.FieldName.Equals("Edited", StringComparison.InvariantCultureIgnoreCase)))
                query = _criteria.OrderBy.First(x => x.FieldName.Equals("Edited", StringComparison.InvariantCultureIgnoreCase)).Direction == OrderByDirection.Ascending 
                    ? query.OrderBy(x => x.DateUpdated) 
                    : query.OrderByDescending(x => x.DateUpdated);

            if (!_criteria.OrderBy.Any())
                query = query.OrderByDescending(x => x.DateUpdated).ThenByDescending(x => x.DateCreated).ThenByDescending(x => x.Id);

            return query;
        }
    }
}