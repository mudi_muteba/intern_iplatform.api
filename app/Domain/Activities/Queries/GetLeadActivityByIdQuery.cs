﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Activities.Queries
{
    public class GetLeadActivityByIdQuery : BaseQuery<LeadActivity>
    {
        private int id;

        public GetLeadActivityByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<LeadActivity>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetLeadActivityByIdQuery ById(int id)
        {
            this.id = id;
            return this;
        }

        protected internal override IQueryable<LeadActivity> Execute(IQueryable<LeadActivity> query)
        {
            return query
                .Where(c => c.Id == id);
        }
    }
}