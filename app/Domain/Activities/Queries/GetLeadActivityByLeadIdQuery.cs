﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using MasterData;

namespace Domain.Activities.Queries
{
    public class GetLeadActivityByLeadIdQuery : BaseQuery<LeadActivity>
    {
        private int leadId;
        private ActivityType activityType;

        public GetLeadActivityByLeadIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<LeadActivity>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetLeadActivityByLeadIdQuery WithLeadId(int LeadId)
        {
            this.leadId = LeadId;
            return this;
        }

        public GetLeadActivityByLeadIdQuery WithActivityTypeId(ActivityType activityType)
        {
            this.activityType = activityType;
            return this;
        }

        protected internal override IQueryable<LeadActivity> Execute(IQueryable<LeadActivity> query)
        {
            if (leadId > 0)
                query = query.Where(c => c.Lead.Id == leadId);

            if (activityType != null)
                query = query.Where(c => c.ActivityType == activityType);

            return query;
        }
    }
}