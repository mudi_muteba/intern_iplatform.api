﻿using Domain.Users;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Activities
{
    public class DeadLeadActivity : LeadActivity
    {
        public DeadLeadActivity()
        {
            ActivityType = ActivityTypes.Dead;
        }

        public DeadLeadActivity(User user, Party.Party party)
            : base(user, party)
        {
            base.ActivityType = ActivityTypes.Dead;
        }

        public virtual new LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.Dead; }
        }
    }
}
