﻿using System;
using Domain.Campaigns;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class CreateLeadActivity : LeadActivity
    {
        public CreateLeadActivity()
        {
            ActivityType = ActivityTypes.Created;
        }

        public CreateLeadActivity(User user, Party.Party party, Campaign campaign)
            : base(user, party)
        {
            Campaign = campaign;
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
            ActivityType = ActivityTypes.Created;
        }

        public virtual string Description { get; set; }


        public virtual new LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.Created; }
        }
    }
}