﻿using Domain.Party.Quotes;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class QuoteIntentToBuyLeadActivity : LeadActivity
    {
        public QuoteIntentToBuyLeadActivity()
        {
            ActivityType = ActivityTypes.QuoteIntentToBuy;
        }

        public QuoteIntentToBuyLeadActivity(User user, Party.Party party)
            : base(user, party)
        {
            ActivityType = ActivityTypes.QuoteIntentToBuy;
        }
        public virtual Quote Quote { get; set; }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.QuoteIntentToBuy; }
        }

        public virtual void HasQuote(Quote quote)
        {
            Quote = quote;
        }
    }
}
