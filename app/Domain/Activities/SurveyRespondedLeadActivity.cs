﻿using Domain.Party.CustomerSatisfactionSurveys;
using Domain.Party.Quotes;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class SurveyRespondedLeadActivity : LeadActivity
    {
        public SurveyRespondedLeadActivity()
        {
            ActivityType = ActivityTypes.SurveyResponded;
        }

        public SurveyRespondedLeadActivity(User user, Party.Party party, CustomerSatisfactionSurvey survey)
            : base(user, party)
        {
            CustomerSatisfactionSurvey = survey;
            ActivityType = ActivityTypes.SurveyResponded;
        }
        public virtual CustomerSatisfactionSurvey CustomerSatisfactionSurvey { get; protected internal set; }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.SurveyResponded; }
        }
    }
}
