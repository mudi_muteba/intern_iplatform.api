﻿using Domain.Party.Quotes;
using Domain.Users;
using MasterData;
using System;

namespace Domain.Activities
{
    public class PolicyBindingCompletedLeadActivity : LeadActivity
    {
        public PolicyBindingCompletedLeadActivity()
        {
            ActivityType = ActivityTypes.PolicyBindingCreated;
        }

        public virtual Quote Quote { get; set; }
        public virtual Guid RequestId { get; set; }
        public virtual string Comments { get; set; }


        public PolicyBindingCompletedLeadActivity(User user, Party.Party party, Quote quote, Guid requestId, string comments)
            : base(user, party)
        {
            User = user;
            ActivityType = ActivityTypes.PolicyBindingCompleted;
            Quote = quote;
            RequestId = requestId;
            Comments = comments;
        }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.PolicyBindingCompleted; }
        }
    }
}
