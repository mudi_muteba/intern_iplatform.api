﻿using Domain.Party.Quotes;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class QuoteAcceptedLeadActivity : LeadActivity
    {
        public QuoteAcceptedLeadActivity()
        {
            ActivityType = ActivityTypes.QuoteAccepted;
        }

        public QuoteAcceptedLeadActivity(User user, Party.Party party)
            : base(user, party)
        {
            ActivityType = ActivityTypes.QuoteAccepted;
        }

        public virtual Quote Quote { get; set; }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.QuoteAccepted; }
        }

        public virtual void HasQuote(Quote quote)
        {
            Quote = quote;
        }
    }
}
