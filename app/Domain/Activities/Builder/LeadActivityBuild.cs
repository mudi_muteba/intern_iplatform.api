﻿using AutoMapper;
using Domain.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Individual;
using Domain.Individuals;
using Domain.Leads.Builder;
using NHibernate.Proxy;

namespace Domain.Activities.Builder
{
    public class LeadActivityBuild : IBuildLeadActivity
    {
        public LeadActivityDto Build(LeadActivity leadActivity)
        {
            if (leadActivity.GetType() != typeof(LeadActivity))
                return null;


            var model = Mapper.Map<LeadActivityDto>(leadActivity);
            return model;
        }
    }

    public static class leadactivitybuilds
    {
        public static LeadActivityDto Build(this LeadActivity entity)
        {
            if (entity == null)
                return null;

            var model = Mapper.Map<LeadActivityDto>(entity);

            //TODO: Check why mapping directly to individual getting error
            //Done due to Lazy Loading fixed by calling GetImplementation
            var individual = entity.Party as INHibernateProxy;

            if (individual != null)
                model.Party = Mapper.Map<BasicIndividualDto>(individual.HibernateLazyInitializer.GetImplementation());

            return model;
        }
    }
}
