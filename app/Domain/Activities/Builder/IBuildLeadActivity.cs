﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Activities;

namespace Domain.Activities.Builder
{
    public interface IBuildLeadActivity
    {
        LeadActivityDto Build(LeadActivity leadActivity);
    }
}
