﻿using Domain.Campaigns;
using Domain.Party.CustomerSatisfactionSurveys;
using Domain.Party.Quotes;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class RejectedAtUnderwritingLeadActivity : LeadActivity
    {
        public RejectedAtUnderwritingLeadActivity()
        {
            ActivityType = ActivityTypes.RejectedAtUnderwriting;
        }

        public RejectedAtUnderwritingLeadActivity(User user, Party.Party party,Quote quote, Campaign campaign = null)
            : base(user, party)
        {
            ActivityType = ActivityTypes.RejectedAtUnderwriting;
            Campaign = campaign;
            CampaignSourceId = campaign.Id;
            Quote = quote;
        }

        public virtual Quote Quote { get; set; }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.RejectedAtUnderwriting; }
        }
    }
}
