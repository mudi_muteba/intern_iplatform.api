﻿using Domain.Users;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Activities
{
    public class LossLeadActivity : LeadActivity
    {
        public LossLeadActivity()
        {
            ActivityType = ActivityTypes.Loss;
        }

        public LossLeadActivity(User user, Party.Party party)
            : base(user, party)
        {
            User = user;
            ActivityType = ActivityTypes.Loss;
        }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.Loss; }
        }
    }
}
