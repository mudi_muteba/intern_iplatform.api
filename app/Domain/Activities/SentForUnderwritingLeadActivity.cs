﻿using Domain.Campaigns;
using Domain.Party.CustomerSatisfactionSurveys;
using Domain.Party.Quotes;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class SentForUnderwritingLeadActivity : LeadActivity
    {
        public SentForUnderwritingLeadActivity()
        {
            ActivityType = ActivityTypes.SentForUnderwriting;
        }

        public SentForUnderwritingLeadActivity(User user, Party.Party party, Campaign campaign, Quote quote)
            : base(user, party)
        {
            ActivityType = ActivityTypes.SentForUnderwriting;
            Campaign = campaign;
            Quote = quote;
        }

        public virtual Quote Quote { get; set; }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.SentForUnderwriting; }
        }
    }
}
