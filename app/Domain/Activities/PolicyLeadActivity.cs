﻿using Domain.Policies;
using Domain.Users;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Activities
{
    public class PolicyLeadActivity : LeadActivity
    {
        public PolicyLeadActivity()
        {
            ActivityType = ActivityTypes.PolicyCreated;
        }

        public virtual PolicyHeader PolicyHeader { get; set; }

        public PolicyLeadActivity(User user, Party.Party party)
            : base(user, party)
        {
            User = user;
            ActivityType = ActivityTypes.PolicyCreated;
        }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.Policy; }
        }
    }
}
