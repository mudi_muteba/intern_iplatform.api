﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Activities;
using ValidationMessages.Campaigns;
using System.Collections.Generic;
using ValidationMessages.Individual;
using Domain.Party.Queries;
using ValidationMessages.Leads;
using Domain.Activities.Queries;


namespace Domain.Activities.Validation
{
    public class LeadActivityValidator : IValidateDto<DisableLeadActivityDto>
    {
        private readonly GetLeadActivityByIdQuery query;

        public LeadActivityValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetLeadActivityByIdQuery(contextProvider, repository);
        }

        public void Validate(DisableLeadActivityDto dto, ExecutionResult result)
        {
            ValidateLeadAcitivity(dto.Id, result);
        }


        private void ValidateLeadAcitivity(int Id, ExecutionResult result)
        {
            var queryResult = GetLeadActivityByIdQuery(Id);

            if (queryResult.Count() <= 0)
            {
                result.AddValidationMessage(LeadActivityValidationMessages.UnKnownId.AddParameters(new[] { Id.ToString() }));
            }
        }

        private IQueryable<LeadActivity> GetLeadActivityByIdQuery(int id)
        {
            return query.ById(id).ExecuteQuery();
        }
    }
}