﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Party;
using ValidationMessages.Campaigns;
using Domain.Products.Queries;
using Domain.Products;
using Domain.Party;
using Domain.Party.Queries;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Activities;
using Domain.Activities.Queries;
using Domain.Activities;
using MasterData;
using ValidationMessages.Leads;


namespace Domain.Leads.Validation
{
    public class LeadActivityValidator : 
        IValidateDto<DeadLeadActivityDto>, 
        IValidateDto<LossLeadActivityDto>, 
        IValidateDto<DelayLeadActivityDto>,
        IValidateDto<CreateQuoteAcceptedLeadActivityDto>
    {
        private readonly GetLeadActivityByLeadIdQuery query;

        public LeadActivityValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetLeadActivityByLeadIdQuery(contextProvider, repository);
        }

        public void Validate(DeadLeadActivityDto dto, ExecutionResult result)
        {
            ValidateLeadActivity(dto.Id, ActivityTypes.Dead, result);
        }

        public void Validate(LossLeadActivityDto dto, ExecutionResult result)
        {
            ValidateLeadActivity(dto.Id, ActivityTypes.Loss, result);
        }

        public void Validate(DelayLeadActivityDto dto, ExecutionResult result)
        {
            ValidateLeadActivity(dto.Id, ActivityTypes.Delayed, result);
        }

        public void Validate(CreateQuoteAcceptedLeadActivityDto dto, ExecutionResult result)
        {
            ValidateLeadActivity(dto.Id, ActivityTypes.QuoteAccepted, result);
        }


        private void ValidateLeadActivity(int leadId, ActivityType activityType, ExecutionResult result)
        {
            var queryResult = GetLeadActivity(leadId, activityType);

            if (queryResult.Any())
            {
                result.AddValidationMessage(LeadActivityValidationMessages.ActivityTypeExists.AddParameters(new[] { activityType.Name, leadId.ToString() }));
            }
        }

        private IQueryable<LeadActivity> GetLeadActivity(int leadid, ActivityType activityType)
        {
            return query.WithLeadId(leadid).WithActivityTypeId(activityType).ExecuteQuery();
        }
    }
}