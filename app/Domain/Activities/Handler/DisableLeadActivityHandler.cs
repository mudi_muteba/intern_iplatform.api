﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Activities;
using MasterData.Authorisation;

namespace Domain.Activities.Handlers
{
    public class DisableLeadActivityHandler : ExistingEntityDtoHandler<LeadActivity, DisableLeadActivityDto, int>
    {
        public DisableLeadActivityHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(LeadActivity entity, DisableLeadActivityDto dto, HandlerResult<int> result)
        {

            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}