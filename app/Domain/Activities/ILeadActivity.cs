﻿using MasterData;

namespace Domain.Activities
{
    public interface ILeadActivity
    {
        LeadStatus GetImpliedStatus { get; }
    }
}
