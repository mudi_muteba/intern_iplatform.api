﻿using Domain.Party.Quotes;
using Domain.Users;
using MasterData;

namespace Domain.Activities
{
    public class QuotedLeadActivity : LeadActivity
    {
        public QuotedLeadActivity()
        {
            ActivityType = ActivityTypes.QuoteRequested;
        }

        public QuotedLeadActivity(User user, Party.Party party, QuoteHeader quoteHeader)
            : base(user, party)
        {
            QuoteHeader = quoteHeader;
            ActivityType = ActivityTypes.QuoteRequested;
        }

        public virtual QuoteHeader QuoteHeader { get; protected internal set; }

        public new virtual LeadStatus GetImpliedStatus
        {
            get { return LeadStatuses.Quoted; }
        }
    }
}
