﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.AuditUserLog;
using Workflow.Messages;

namespace Domain.AuditUserLog.Message
{
    public class AuditUserLogMessage : WorkflowExecutionMessage
    {
        public AuditUserLogDto CreateAuditUserLogDto { get; set; }

        public AuditUserLogMessage(AuditUserLogDto createAuditUserLogDto,
            WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
            CreateAuditUserLogDto = createAuditUserLogDto;
        }
    }
}
