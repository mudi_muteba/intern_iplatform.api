﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Domain.AuditLogs.Message;
using Domain.AuditUserLog.Message;
using iPlatform.Api.DTOs.AuditUserLog;
using iPlatform.Api.DTOs.Base.Connector;
using Infrastructure.Configuration;
using RestSharp;
using Shared;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.AuditUserLog.Consumer
{
    public class AuditUserLogMessageConsumer : AbstractMessageConsumer<AuditUserLogMessage>
    {
        private readonly string _BaseUrl;
        private readonly RestClient _Client;
        private readonly string _Token;
        private static readonly ILog Log = LogManager.GetLogger<AuditUserLogMessageConsumer>();

        public AuditUserLogMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor) : base(router, executor)
        {
            _BaseUrl = new AdminConfigurationReader().BaseUrl;

            _Client = new RestClient(_BaseUrl);
            _Token = new AuditUserLogAuthentication().Authenticate().Token;
        }

        public override void Consume(AuditUserLogMessage message)
        {
            this.Info(() => string.Format("Logging to User Audit: {0} by Username {1}", message.CreateAuditUserLogDto, message.CreateAuditUserLogDto.Username));

            IRestRequest request = CreateRequest<AuditUserLogDto>("auditUserLog", message.CreateAuditUserLogDto);
            try
            {
                Log.InfoFormat("Sending AuditUserLog to iAdmin api on url {0}", _BaseUrl);
                IRestResponse response = _Client.Execute<AuditUserLogDto>(request);

                if (response.StatusCode == HttpStatusCode.OK)
                    this.Info(() => string.Format("Successfully Logged to User Audit: {0} by Username {1}", message.CreateAuditUserLogDto, message.CreateAuditUserLogDto.Username));
                else
                    this.Error(() => string.Format("Logging to User Audit: {0} by Username {1} - {2}", message.CreateAuditUserLogDto, message.CreateAuditUserLogDto.Username, response.ErrorMessage));
            }
            catch (Exception ex)
            {
                var error = string.Format("Could not send AuditUserLog with Username '{0}'. Exception details: {1}",
                    message.CreateAuditUserLogDto.Username,
                    new ExceptionPrettyPrinter().Print(ex));
                Log.ErrorFormat("AuditUserLog Transfer Error: {0}", error);
            }
        }

        #region [ Helpers ]

        private IRestRequest CreateRequest<T>(string queryString, T dto)
        {
            RestRequest request = new RestRequest(queryString)
            {
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };
            request.AddParameter("Authorization", _Token, ParameterType.HttpHeader);
            request.AddBody(dto);

            return request;
        }

        #endregion
    }
}
