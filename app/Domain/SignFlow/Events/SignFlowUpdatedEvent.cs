﻿using Domain.Base.Events;
using iPlatform.Api.DTOs.SignFlow;

namespace Domain.SignFlow.Events
{
    public class SignFlowUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public EditSignFlowDocumentDto EditSignFlowDocumentDto { get; private set; }

        public SignFlowUpdatedEvent(SignFlowDocument entity)
        {
            this.EditSignFlowDocumentDto = EditSignFlowDocumentDto;
        }
    }
}
