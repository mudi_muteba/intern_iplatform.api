﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.SignFlow;

namespace Domain.SignFlow.Events
{
    public class SignFlowCorrespondantCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public CreateSignFlowCorrespondantDto CreateSignFlowCorrespondantDto { get; set; }

        public SignFlowCorrespondantCreatedEvent(CreateSignFlowCorrespondantDto createSignFlowCorrespondantDto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            CreateSignFlowCorrespondantDto = createSignFlowCorrespondantDto;
        }
    }
}
