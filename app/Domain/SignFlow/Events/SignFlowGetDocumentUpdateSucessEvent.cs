﻿using Domain.Base.Events;
using iPlatform.Api.DTOs.SignFlow;

namespace Domain.SignFlow.Events
{
    public class SignFlowGetDocumentUpdateSucessEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public UpdateEventHandlerSignFlowDocumentDto UpdateEventHandlerSignFlowDocumentDto { get; private set; }

        public SignFlowGetDocumentUpdateSucessEvent(SignFlowDocument entity)
        {
            this.UpdateEventHandlerSignFlowDocumentDto = UpdateEventHandlerSignFlowDocumentDto;
        }
    }
}
