﻿using Domain.Base.Events;
using iPlatform.Api.DTOs.SignFlow;

namespace Domain.SignFlow.Events
{
    public class SignFlowUpdatedSucessEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public UpdateSignFlowDocumentDto UpdateSignFlowDocumentDto { get; private set; }

        public SignFlowUpdatedSucessEvent(SignFlowDocument entity)
        {
            this.UpdateSignFlowDocumentDto = UpdateSignFlowDocumentDto;
        }
    }
}
