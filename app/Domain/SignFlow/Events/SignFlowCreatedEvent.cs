﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.SignFlow;


namespace Domain.SignFlow.Events
{
    public class SignFlowCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public CreateSignFlowDocumentDto CreateSignFlowDocumentDto { get; set; }

        public SignFlowCreatedEvent(CreateSignFlowDocumentDto createSignFlowDocumentDto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            CreateSignFlowDocumentDto = createSignFlowDocumentDto;
        }
    }
}
