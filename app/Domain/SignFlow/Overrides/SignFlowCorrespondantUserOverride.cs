﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.SignFlow.Overrides
{
    public class SignFlowCorrespondantUserOverride : IAutoMappingOverride<SignFlowCorrespondant>
    {
        public void Override(AutoMapping<SignFlowCorrespondant> mapping)
        {
            mapping.References(x => x.SignFlowDocument);
        }
    }
}
