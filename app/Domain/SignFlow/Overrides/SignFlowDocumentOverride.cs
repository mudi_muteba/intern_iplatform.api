﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.SignFlow.Overrides
{
    public class SignFlowDocumentOverride : IAutoMappingOverride<SignFlowDocument>
    {
        public void Override(AutoMapping<SignFlowDocument> mapping)
        {
            mapping.HasMany(x => x.SignFlowCorrespondants).Cascade.SaveUpdate();
        }
    }
}
