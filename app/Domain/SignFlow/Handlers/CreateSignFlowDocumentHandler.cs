﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Individuals;
using Domain.SignFlow.Events;
using Domain.Users;
using iPlatform.Api.DTOs.SignFlow;
using MasterData.Authorisation;
using IRepository = Domain.Base.Repository.IRepository;

namespace Domain.SignFlow.Handlers
{
    public class CreateSignFlowDocumentHandler : CreationDtoHandler<SignFlowDocument, CreateSignFlowDocumentDto, int> //: BaseDtoHandler<CreateSignFlowDocumentDto, int>
    {
        public CreateSignFlowDocumentHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            Repository = repository;
        }

        private IRepository Repository { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void EntitySaved(SignFlowDocument entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override SignFlowDocument HandleCreation(CreateSignFlowDocumentDto dto, HandlerResult<int> result)
        {
            var user = Repository.GetById<User>(dto.Context.UserId);
            dto.Reference = Guid.NewGuid();
            dto.CreatedAt = DateTime.Now;
            dto.ModifiedAt = DateTime.Now;
            var signFlowDocument = SignFlowDocument.Create(dto);

            if (dto.CorrespondantUser != null)
            {
                foreach (var item in dto.CorrespondantUser)
                {
                    var individual = Repository.GetAll<Individual>().FirstOrDefault(x => x.ContactDetail.Email == item.Email);
                    if (individual == null) continue;
                    item.Email = individual.ContactDetail.Email;
                    item.Name = individual.DisplayName;
                    item.Cell = individual.ContactDetail.Cell;
                }
            }

            signFlowDocument.RaiseEvent(new SignFlowCreatedEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(user.Channels.FirstOrDefault().Id) }));

            return signFlowDocument;
        }
    }
}
