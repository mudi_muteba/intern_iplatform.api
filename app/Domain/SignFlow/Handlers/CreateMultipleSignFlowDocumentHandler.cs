﻿using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.SignFlow;
using MasterData.Authorisation;

namespace Domain.SignFlow.Handlers
{
    public class CreateMultipleSignFlowDocumentHandler : BaseDtoHandler<CreateMultipleSignFlowDocumentDto, string>
    {
        public CreateMultipleSignFlowDocumentHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan) : base(contextProvider)
        {
            Repository = repository;
            ExecutionPlan = executionPlan;
        }

        private IRepository Repository { get; set; }

        private IExecutionPlan ExecutionPlan { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(CreateMultipleSignFlowDocumentDto dto, HandlerResult<string> result)
        {
            try
            {
                foreach (var item in dto.CreateSignFlowDocumentDtos)
                {
                    ExecutionPlan.Execute<CreateSignFlowDocumentDto, int>(item);
                }

                result.Processed("true");
            }
            catch (Exception ex)
            {
                result.Processed(ex.Message);
            }
        }
    }
}

