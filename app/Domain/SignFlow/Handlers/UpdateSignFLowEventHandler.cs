﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.SignFlow;
using MasterData.Authorisation;

namespace Domain.SignFlow.Handlers
{
    public class UpdateSignFLowEventHandler : BaseDtoHandler<UpdateSignFlowDocumentDto, int>
    {
        public UpdateSignFLowEventHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;
        
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(UpdateSignFlowDocumentDto dto, HandlerResult<int> result)
        {
            var entity = _repository.GetAll<SignFlowDocument>().FirstOrDefault(x => x.DocId == dto.DocId);

            if (entity == null) return;
            entity.DocumentStatus = dto.Status;
            entity.ModifiedAt = DateTime.Now;
            var updatedDto = Mapper.Map(entity, dto);

            entity.UpdateStatus(updatedDto);
            result.Processed(entity.Id);
        }
    }
}
