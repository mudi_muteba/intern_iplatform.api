﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.SignFlow;
using MasterData.Authorisation;

namespace Domain.SignFlow.Handlers
{
    public class UpdateSignFLowDocumentEventHandler : BaseDtoHandler<UpdateEventHandlerSignFlowDocumentDto, int>
    {
        public UpdateSignFLowDocumentEventHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(UpdateEventHandlerSignFlowDocumentDto dto, HandlerResult<int> result)
        {
            var entity = _repository.GetAll<SignFlowDocument>().FirstOrDefault(x => x.DocId == dto.Di);

            if (entity == null) return;
            entity.DocumentStatus = dto.Status;
            var updatedDto = Mapper.Map(entity, dto);

            entity.UpdateStatus(updatedDto);
            result.Processed(entity.Id);
        }
    }
}
