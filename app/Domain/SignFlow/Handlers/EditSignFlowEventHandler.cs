﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.SignFlow;
using MasterData.Authorisation;

namespace Domain.SignFlow.Handlers
{
    public class EditSignFlowEventHandler : BaseDtoHandler<EditSignFlowDocumentDto, int>
    {
        public EditSignFlowEventHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.Edit
                };
            }
        }

        protected override void InternalHandle(EditSignFlowDocumentDto dto, HandlerResult<int> result)
        {
            var entity = _repository.GetAll<SignFlowDocument>().FirstOrDefault(x => x.Reference == dto.Reference);

            if (entity == null)
            {
                Log.ErrorFormat("No signflow document exist for {0}", dto.Reference);
                return;
            }

            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
