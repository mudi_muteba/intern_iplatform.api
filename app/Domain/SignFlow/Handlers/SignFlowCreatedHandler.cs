﻿using Domain.Base.Events;
using Domain.SignFlow.Events;
using Domain.SignFlow.Message;
using Workflow.Messages;


namespace Domain.SignFlow.Handlers
{
    public class SignFlowCreatedHandler : BaseEventHandler<SignFlowCreatedEvent>
    {
        public SignFlowCreatedHandler(IWorkflowRouter router)
        {
            _router = router;
        }

        private readonly IWorkflowRouter _router;

        public override void Handle(SignFlowCreatedEvent @event)
        {
            var routingMessage = new WorkflowRoutingMessage();
            routingMessage.AddMessage(new SignFlowWorkflowExecutionMessage(new SignFlowTaskMetaData(@event)));

            _router.Publish(routingMessage);
        }
    }
}
