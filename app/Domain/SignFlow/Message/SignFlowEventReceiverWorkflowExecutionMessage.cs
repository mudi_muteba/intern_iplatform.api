﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.SignFlow.Message
{
    public class SignFlowEventReceiverWorkflowExecutionMessage : WorkflowExecutionMessage
    {
        public SignFlowEventReceiverWorkflowExecutionMessage() { }

        public SignFlowEventReceiverWorkflowExecutionMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }

        public SignFlowEventReceiverWorkflowExecutionMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}
