﻿using MasterData;
using SignFlow.Model;
using Workflow.Messages.Plan.Tasks;

namespace Domain.SignFlow.Message
{
    public class SignFlowEventReceiverTaskMetaData : ITaskMetadata
    {
        public int DocId { get; set; }

        public DocumentStatus Status { get; set; }

        public GetDocStatusResult GetDocStatusResult { get; set; }

        public LoginResult LoginResult { get; set; }

        public SignFlowEventReceiverTaskMetaData()
        {
        }

        public SignFlowEventReceiverTaskMetaData(int docId, DocumentStatus status, GetDocStatusResult getDocStatusResult, LoginResult loginResult)
        {
            DocId = docId;
            Status = status;
            GetDocStatusResult = getDocStatusResult;
            LoginResult = loginResult;
        }
    }
}
