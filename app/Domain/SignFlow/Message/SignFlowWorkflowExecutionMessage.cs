﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.SignFlow.Message
{
    public class SignFlowWorkflowExecutionMessage : WorkflowExecutionMessage
    {
        public SignFlowWorkflowExecutionMessage() { }

        public SignFlowWorkflowExecutionMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) { }

        public SignFlowWorkflowExecutionMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(metadata, messageType) { }
    }
}
