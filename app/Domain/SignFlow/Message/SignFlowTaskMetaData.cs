﻿using Domain.SignFlow.Events;
using Workflow.Messages.Plan.Tasks;

namespace Domain.SignFlow.Message
{
    public class SignFlowTaskMetaData : ITaskMetadata
    {
        public SignFlowCreatedEvent SignFlowCreatedEvent { get; set; }

        public SignFlowTaskMetaData()
        {
        }
        public SignFlowTaskMetaData(SignFlowCreatedEvent signFlowCreatedEvent)
        {
            SignFlowCreatedEvent = signFlowCreatedEvent;
        }
    }
}
