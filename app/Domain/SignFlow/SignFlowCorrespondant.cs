﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.SignFlow;
using MasterData;

namespace Domain.SignFlow
{
    public class SignFlowCorrespondant : Entity
    {
        public virtual string Email { get; set; }
        public virtual SignFlowDocument SignFlowDocument { get; set; }
        public virtual DocumentType DocumentType { get; set; }
        public virtual DocumentDefinition DocumentDefinition { get; set; }

        public static SignFlowCorrespondant Create(CreateSignFlowCorrespondantDto createSignFlowCorrespondantDto)
        {
            var signFlowCorrespondant = Mapper.Map<SignFlowCorrespondant>(createSignFlowCorrespondantDto);
            return signFlowCorrespondant;
        }
    }
}
