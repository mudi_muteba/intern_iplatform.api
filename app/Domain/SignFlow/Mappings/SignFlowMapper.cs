﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Reports.DigitalSignature.BrokerNote;
using Domain.Reports.DigitalSignature.CompanyDetails;
using Domain.Reports.DigitalSignature.DebitOrder;
using Domain.Reports.DigitalSignature.Popi;
using Domain.Reports.DigitalSignature.RecordOfAdvice;
using Domain.Reports.DigitalSignature.Sla;
using Domain.SignFlow.Events;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments;
using iPlatform.Api.DTOs.SignFlow;

namespace Domain.SignFlow.Mappings
{
    public class SignFlowMapper : Profile
    {
        public class SignFlowDocumentDto
        {
        }

        public class DefaultPersonConverter : TypeConverter<SignFlowDocumentDto, SignFlowDocument>
        {
            private readonly IRepository _repository;

            public DefaultPersonConverter(IRepository repository)
            {
                _repository = repository;
            }

            protected override SignFlowDocument ConvertCore(SignFlowDocumentDto source)
            {
                return _repository.GetAll<SignFlowDocument>().OrderBy(x => x.Id).FirstOrDefault();
            }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<SignFlowDocumentDto, SignFlowDocument>()
                .ConvertUsing<ITypeConverter<SignFlowDocumentDto, SignFlowDocument>>()
                ;

            Mapper.CreateMap<CreateSignFlowDocumentDto, SignFlowDocument>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<CreateSignFlowCorrespondantDto, SignFlowCorrespondant>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<EditSignFlowDocumentDto, SignFlowDocument>()
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<UpdateSignFlowDocumentDto, SignFlowDocument>()
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.DocumentStatus, o => o.MapFrom(t => t.Status))
                ;

            Mapper.CreateMap<SignFlowDocument, UpdateSignFlowDocumentDto>();

            Mapper.CreateMap<SignFlowDocument, iPlatform.Api.DTOs.SignFlow.SignFlowDocumentDto>()
                .ForMember(t => t.CorrespondantUserCell, o => o.Ignore())
                .ForMember(t => t.CorrespondantUserEmail, o => o.Ignore())
                .ForMember(t => t.CorrespondantUserName, o => o.Ignore())
                .ForMember(t => t.DocDefinition, o => o.Ignore())
                ;

            Mapper.CreateMap<SignFlowCreatedEvent, CreateSignFlowDocumentDto>();
            Mapper.CreateMap<CreateSignFlowDocumentDto, SignFlowCreatedEvent>();

            Mapper.CreateMap<SignFlowCorrespondantCreatedEvent, CreateSignFlowCorrespondantDto>();
            Mapper.CreateMap<CreateSignFlowCorrespondantDto, SignFlowCorrespondantCreatedEvent>();

            Mapper.CreateMap<BrokerNoteReportExtract, BrokerNoteReportExtractDto>();

            Mapper.CreateMap<QueryResult<IList<BrokerNoteReportExtractDto>>, POSTResponseDto<IList<BrokerNoteReportExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<BrokerNoteReportDto>, POSTResponseDto<BrokerNoteReportDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<PopiDocReportExtract, PopiDocReportExtractDto>();

            Mapper.CreateMap<QueryResult<IList<PopiDocReportExtractDto>>, POSTResponseDto<IList<PopiDocReportExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<PopiDocReportDto>, POSTResponseDto<PopiDocReportDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<DebitOrderReportExtract, DebitOrderReportExtractDto>();

            Mapper.CreateMap<QueryResult<IList<DebitOrderReportExtractDto>>, POSTResponseDto<IList<DebitOrderReportExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<DebitOrderReportDto>, POSTResponseDto<DebitOrderReportDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<SlaReportExtract, SlaReportExtractDto>();

            Mapper.CreateMap<QueryResult<IList<SlaReportExtractDto>>, POSTResponseDto<IList<SlaReportExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<SlaReportDto>, POSTResponseDto<SlaReportDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<RecordOfAdviceReportExtract, RecordOfAdviceReportExtractDto>();

            Mapper.CreateMap<QueryResult<IList<RecordOfAdviceReportExtractDto>>, POSTResponseDto<IList<RecordOfAdviceReportExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<QueryResult<RecordOfAdviceReportDto>, POSTResponseDto<RecordOfAdviceReportDto>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<RecordOfAdviceContactReportExtract, RecordOfAdviceContactReportExtractDto>();

            Mapper.CreateMap<QueryResult<IList<RecordOfAdviceContactReportExtractDto>>, POSTResponseDto<IList<RecordOfAdviceContactReportExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());

            Mapper.CreateMap<ReportHeaderExtract, ReportHeaderExtractDto>();

            Mapper.CreateMap<QueryResult<IList<ReportHeaderExtractDto>>, POSTResponseDto<IList<ReportHeaderExtractDto>>>()
               .ForMember(t => t.StatusCode, o => o.Ignore())
               .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
               .ForMember(t => t.Errors, o => o.Ignore());
        }
    }
}
