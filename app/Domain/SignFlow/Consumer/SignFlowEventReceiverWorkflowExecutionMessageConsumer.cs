﻿using System.Net;
using Domain.SignFlow.Message;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.SignFlow;
using iPlatform.Enums;
using Infrastructure.Configuration;
using Shared.Extentions;
using SignFlow;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.SignFlow.Consumer
{
    public class SignFlowEventReceiverWorkflowExecutionMessageConsumer : AbstractMessageConsumer<SignFlowEventReceiverWorkflowExecutionMessage>
    {
        private readonly IConnector _connector;

        public SignFlowEventReceiverWorkflowExecutionMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector) : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
        }

        public override void Consume(SignFlowEventReceiverWorkflowExecutionMessage message)
        {
            var metaData = (SignFlowEventReceiverTaskMetaData)message.Metadata;
            var signFlowConnector = new SignFlowConnector();
            signFlowConnector.GetDoc(metaData.GetDocStatusResult, metaData.LoginResult, metaData.DocId);

            var updateDigitalSignatureStatus = new UpdateSignFlowDocumentDto
            {
                DocId = metaData.DocId.ToString(),
                Status = metaData.Status
            };

            var response = _connector.SignFlowManagement.SignFlow.UpdateStatus(updateDigitalSignatureStatus);
            if (response.StatusCode == HttpStatusCode.OK)
                this.Info(() => string.Format("Successfully downloaded document and update database for docId {0}", metaData.DocId));
            else
                this.Error(() => string.Format("Error {0} when updating status for docId : {1}", response.Errors[0].DefaultMessage, metaData.DocId));
        }
    }
}
