﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Domain.SignFlow.Message;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.SignFlow;
using iPlatform.Enums;
using Infrastructure.Configuration;
using MasterData;
using Shared.Extentions;
using SignFlow;
using SignFlow.Model;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.SignFlow.Consumer
{
    public class SignFlowWorkflowExecutionMessageConsumer : AbstractMessageConsumer<SignFlowWorkflowExecutionMessage>
    {
        private readonly IConnector _connector;

        public SignFlowWorkflowExecutionMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector) : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
        }

        public override void Consume(SignFlowWorkflowExecutionMessage message)
        {
            var metaData = (SignFlowTaskMetaData)message.Metadata;
            if (metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto == null)
            {
                this.Info(() => "SignFlowWorkflowExecutionMessage is NULL");
                return;
            }

            var signFlowConnector = new SignFlowConnector();
            var loginResult = signFlowConnector.Login();
            var docId = 0;

            if (loginResult.Result == "Success")
            {
                var path = metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto.PathOfDocument;
                var messageToUser = metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto.MessageToUser;
                var uploadDocName = metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto.NameDocUploaded;
                var docType = metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto.DocumentType;
                var dueDate = metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto.DueDate;

                var sfUserDatas = metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto.CorrespondantUser.Select(item => new SignFlowUserData
                {
                    Email = item.Email, Name = item.Name, Cell = item.Cell, DocDefinitions = item.DocDefinitions
                }).ToList();

                var editSignFlowDocumentDto = new EditSignFlowDocumentDto();

                var createWorkFlowResult = signFlowConnector.CreateWorkflow(loginResult, path, messageToUser, uploadDocName, dueDate);
                if (createWorkFlowResult != null && createWorkFlowResult.Result == "Success" && createWorkFlowResult.DocId > 0)
                {
                    var addWorkFlowResult = signFlowConnector.AddWorkFlowStep(createWorkFlowResult, loginResult, sfUserDatas);
                    docId = addWorkFlowResult.FirstOrDefault().DocId;
                    signFlowConnector.DocPrepperAddFields(loginResult, addWorkFlowResult.FirstOrDefault().DocId, sfUserDatas);
                    signFlowConnector.InitiateFlow(addWorkFlowResult, loginResult);

                    var getDocStatusResult = signFlowConnector.GetDocStatus(loginResult, docId);

                    editSignFlowDocumentDto = new EditSignFlowDocumentDto
                    {
                        DocId = addWorkFlowResult.FirstOrDefault().DocId.ToString(),
                        User = metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto.Context.Username,
                        DocName = uploadDocName,
                        CorrespondantUserEmail = metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto.CorrespondantUser.FirstOrDefault().Email,
                        DocumentType = new DocumentTypes().FirstOrDefault(x => x.Id == docType.Id),
                        DocumentStatus = new DocumentStatuses().FirstOrDefault(c => c.Name == getDocStatusResult.DocStatus), //DocumentStatuses.Pending,
                        Reference = metaData.SignFlowCreatedEvent.CreateSignFlowDocumentDto.Reference,
                        ModifiedAt = DateTime.Now
                    };
                }

                var response = _connector.SignFlowManagement.SignFlow.Update(editSignFlowDocumentDto);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    this.Info(() => string.Format("Successfully send document and update database for user {0} docId {1}", sfUserDatas.FirstOrDefault().Email, docId));
                }
                else
                {
                    this.Error(() => string.Format("Error : {0}", response.Errors[0].DefaultMessage));
                }
            }
            else
            {
                this.Error(() => "Error : Login failed for SingFLow API");
            }
        }
    }
}
