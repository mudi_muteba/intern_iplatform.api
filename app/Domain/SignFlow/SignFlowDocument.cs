﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base;
using Domain.SignFlow.Events;
using iPlatform.Api.DTOs.SignFlow;
using MasterData;

namespace Domain.SignFlow
{
    public class SignFlowDocument : EntityWithAudit
    {
        public SignFlowDocument() 
        {
            SignFlowCorrespondants = new List<SignFlowCorrespondant>();
        }

        public virtual Guid Reference { get; set; }
        public virtual string DocId { get; set; }
        public virtual string DocName { get; set; }
        public virtual string User { get; set; }
        public virtual DocumentStatus DocumentStatus { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime ModifiedAt { get; set; }
        public virtual IList<SignFlowCorrespondant> SignFlowCorrespondants { get; set; }

        public static SignFlowDocument Create(CreateSignFlowDocumentDto createSignFlowDocumentDto)
        {
            var signFlowDocument = Mapper.Map<SignFlowDocument>(createSignFlowDocumentDto);
            AddSignFlowCorrespondants(signFlowDocument, createSignFlowDocumentDto.CorrespondantUser);
            return signFlowDocument;
        }

        public static void AddSignFlowCorrespondants(SignFlowDocument entity, List<CorrespondantUser> dto)
        {
            var docDefinitionId = dto.FirstOrDefault().DocDefinitions.FirstOrDefault().Id;
            var docType = new DocumentDefinitions().FirstOrDefault(x => x.Id == docDefinitionId).DocumentTypeId;

            foreach (var item in dto)
            {
                foreach (var docDefinition in item.DocDefinitions.Select(itemDoc => new DocumentDefinitions().FirstOrDefault(x => x.Id == itemDoc.Id)))
                {
                    entity.SignFlowCorrespondants.Add(new SignFlowCorrespondant { Email = item.Email, DocumentType = docType, DocumentDefinition = docDefinition});
                }
            }
        }

        public virtual void Update(EditSignFlowDocumentDto editSignFlowDocumentDto)
        {
            Mapper.Map(editSignFlowDocumentDto, this);
            RaiseEvent(new SignFlowUpdatedEvent(this));
        }

        public virtual void UpdateStatus(UpdateSignFlowDocumentDto updateSignFlowDocumentDto)
        {
            RaiseEvent(new SignFlowUpdatedSucessEvent(this));
        }

        public virtual void UpdateStatus(UpdateEventHandlerSignFlowDocumentDto updateEventHandlerSignFlowDocumentDto)
        {
            RaiseEvent(new SignFlowGetDocumentUpdateSucessEvent(this));
        }
    }
}
