﻿using iPlatform.OutboundCommunication.SMS;
using iPlatform.OutboundCommunication.SMS.CellFind;
using iPlatform.OutboundCommunication.za.co.cellfind.cfwinqa;

namespace OutboundCommunication.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var configuration = new SMSProviderConfiguration("CellFind", "AIGtest2", "@iGt3st2", "http://cfwinqa.cellfind.co.za/mpgproxy/Service.asmx");
            var message = new SMSMessage("0825349935", "This is a test message", "reference");

            var sender = new CellFindSender(new IMPGWS());
            var result = sender.Send(configuration, message);

            System.Console.ReadLine();
        }
    }
}