﻿using System.Collections.Generic;

namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels.Lookups
{
    public static class BuildingLookups
    {
        public static List<Lookup> LookupsList = new List<Lookup>()
        {
            new Lookup("MARITAL_STATE", "Single", 1, "Single", "", true),
            new Lookup("MARITAL_STATE", "Married", 2, "Married", "", true),
            new Lookup("MARITAL_STATE", "Divorced", 3, "Divorced", "", true),
            new Lookup("MARITAL_STATE", "Separated", 4, "Separated", "", true),
            new Lookup("MARITAL_STATE", "Widowed", 5, "Widowed", "", true),
            new Lookup("MARITAL_STATE", "Unknown", 6, "Unknown", "", true),
        };

    }


    public enum RoofConstructionType
    {
        ASBESTOS,
        CONCRETE,
        CORRUGATE,
        FIBRECEM,
        OTHER,
        RD,
        SHINGLES,
        SLATE,
        THATCH,
        TILE,
        U
    }

    public enum WallConstructionType
    {
        ASBESTOS,
        BRICK,
        CONCRETE,
        CORRUGATE,
        FIBRECEM,
        MUD,
        PRECAST,
        RD,
        STONE,
        U,
        WOOD
    }

    public enum ResidenceType
    {
        MAIN,
        OTHER,
        RD,
        SECOND,
        U
    }

    public enum PremisesUseType
    {
        BUS,
        RD,
        RES,
        RES_BUS,
        U
    }


    public enum BusinessNatureType
    {
        Advertising_Design = 1,
        Barber_Hairdresser = 10,
        BeautyTherapist_HygieneConsultant = 11,
        Broker = 12,
        BuildingContractor_Propertydeveloper = 13,
        Printing_Manufacturing = 14,
        Carpenter_Furniturerestoration = 15,
        Ceramics_Glasswaremanufacturing = 16,
        Accounting_Auditor_Bookkeeper_Taxexpert = 17,
        NurserySchools_Daycarecentre = 18,
        ClinicalPhycologist_OccupationalTherapist_Physiotherapist = 19,
        Advocate_Attorney = 2,
        Designerofclothing_Fashionbuyer = 20,
        Dietician_Nutritionist = 21,
        Electrician = 22,
        Engineer = 23,
        Entrepreneur_Other = 24,
        Eventmanager = 25,
        Fitnessinstructor = 26,
        Florist = 27,
        IT_SoftwareDeveloper_Websitedeveloper = 28,
        Jeweller_Diamonddealer = 29,
        AirConditioning = 3,
        Landscaping = 30,
        LossAdjustor_Surveyor = 31,
        Medicaldoctor = 32,
        Musician = 33,
        Officesupplies = 34,
        Photographer_PhotographyStudio = 35,
        Touristoperator = 36,
        Veterinarian = 37,
        Writer_Publisher = 38,
        Other = 39,
        Architect_Graphicdesigning = 4,
        Arms_Ammunition = 5,
        ArtDealer = 6,
        Artist = 7,
        AutomobileMechanic = 8,
        Baker_Catering_Chef = 9,
        RD,
        U
    }

    public enum DwellingType
    {
        COTTAGE,
        FLAT_GL,
        FLAT_UL,
        HOTEL,
        HOUSE,
        PARK,
        RD,
        STORAGE,
        S_TOWNHSE,
        U,
        U_TOWNHSE,

    }

    public enum PerimeterWallType
    {
        BRICK_H,
        BRICK_L,
        BRICK_LE,
        NOFENCE,
        PALISADE_H,
        PALISADE_L,
        PRECAST_H,
        PRECAST_L,
        RD,
        U,
        WIREFENCE,
        WIRE_H,
        WIRE_L,
        WOOD_H,
        WOOD_L,
    }

    public enum BuildingExcess
    {
        R500 = 500,
        R1000 = 1000,
        R3000 = 3000,
        R5000 = 5000,
        R10000 = 10000,
        R15000 = 15000,
        R20000 = 20000
    }

    public enum TitleHolderType
    {
        ABSA,
        FNB,
        INVSTC,
        NEDB,
        OOBA,
        OTHER,
        PLCHDR,
        RD,
        RMB,
        SAHL,
        STD,
        U,
    }

}