﻿using System;

namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels.Lookups
{
    public class Lookup
    {
        public Lookup()
        {
            
        }
        public Lookup(string type, string text, int value, string help, string dependancy, bool visible)
        {
         
        }

        public string Type { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public string Help { get; set; }
        public string Dependency { get; set; }
        public bool Visible { get; set; }
    }
}