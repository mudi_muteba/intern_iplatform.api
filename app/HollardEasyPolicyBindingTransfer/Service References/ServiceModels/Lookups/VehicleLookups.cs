﻿namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels.Lookups
{
    public class VehicleLookups
    {

    }

    public enum MotorExcess
    {
        R0 = 0,
        R750 = 750,
        R1500 = 1500
    }

    public enum MotorColour
    {
        BEIGE,
        BLACK,
        BLUE,
        BRONZE,
        BROWN,
        CERISE,
        CHAMPAGNE,
        GOLD,
        GREEN,
        GREY,
        MAROON,
        ORANGE,
        OTHER,
        PINK,
        PURPLE,
        RD,
        RED,
        SILVER,
        TAN,
        TURQUOISE,
        U,
        WHITE,
        YELLOW,
    }


    public enum EmploymentStatus
    {
        EMPLOY,
        HOME,
        RETIRE,
        SELF,
        STUDENT,
        UNEMPLOY,

    }

    public enum FinancialInstitution
    {
        ABSA,
        AMP,
        AUDI,
        BMW,
        BIDVEST,
        CARFIN,
        CHRYSLER,
        CITROEN,
        FIAT,
        FORD,
        GBS,
        GMSA,
        HONDA,
        HYUNDAI,
        IEMAS,
        INVESTEC,
        JAGUAR,
        MERC,
        MFC,
        NISSAN,
        OTHER,
        PEUGEOT,
        STANB,
        TCFC,
        TOYOTA,
        VW,
        VOLVO,
        WESBANK
    }

    public enum HireDuration
    {
        Days30 = 30,
        Days60 = 60,
    }

    public enum HireGroup
    {
        B,
        D,
        E,
        F,
        H,
        S,
        T,
        Y,
    }

    public enum Immobiliser
    {
        FACFIT,
        NONE,
        RD,
        U,
        VESA,
    }

    public enum InsuredType
    {
        FINANCED,
        FNCD_PLUS,
        RD,
        RETAIL,
        RTL_PLUS,
        SPECIFIED,
        U,
    }

    public enum LicenceType
    {
        A,
        A1,
        B,
        C,
        C1,
        COMMONW,
        EB,
        EC,
        EC1,
        INTERNAT,
        LEARNERS,
        NEIGHBOUR,
        RD,
        U,

    }

    public enum MaritalState
    {
        Single = 1,
        Married = 2,
        Divorced = 3,
        Separated = 4,
        Widowed = 5,
        Unknown = 6,
    }

    public enum TiaOccupation
    {
        FORCE,
        LABOUR,
        MANAGER,
        PUBLIC,
        SERVICE,
        TRANSPORT,
    }

    public enum Parking
    {
        RD,
        SC_GARAGE,
        SC_LOT,
        SC_PORT,
        SO_DRIVE,
        SO_LOT,
        U,
        UC_LOT,
        UC_PORT,
        UO_DRIVE,
        UO_STREET,
    }

    public enum RegisteredOwnerType
    {
        PERSON,
        INSTITUTION,
    }

    public enum ScratchOption
    {
        EXTERIOR,
        EXTINT,
        U,
    }

    public enum Gender
    {
        F,
        M,
        U,
    }

    public enum TimeTravelling
    {
        STATIONARY,
        TRAVEL,
        UNKNOWN,

    }

    public enum TyreOption
    {
        EXTENDED,
        STANDARD,
        U,
    }

    public enum VehicleCode
    {
        CODE3,
        CODE4,
        UNKNOWN,
        NEWPREOWN,
        RD,
        U,
    }

    public enum TiaVehicleType
    {
        KIT,
        NML,
        U,
        UNLIC,
    }

    public enum VehicleUse
    {
        AGR,
        BUS,
        COMM,
        PRSNL,
        TAXI,
        U,
    }

    public enum Citizenship
    {
        FOREIGNER,
        SACITIZEN,
        SARESIDENT,
    }

    public enum TiaCountry
    {
        SOUTH_AFRICA = 1
    }

    public enum RetailAdjustment
    {
        ABOVE,
        BELOW,
        U,
    }

    public enum PaintType
    {
        STD,
        MTL,
        PRL,
        MATT,

    }

    public enum CommunicationMethod
    {
        EMAIL,
        POST
    }

    public enum DependantsCovered
    {
        PH,
        PH_SP,
        PH_SP_4,
        PH_SP_6
    }
}