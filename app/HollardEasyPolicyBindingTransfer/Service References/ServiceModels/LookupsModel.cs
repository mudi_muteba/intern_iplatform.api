﻿using System.Collections.Generic;
using HollardEasyPolicyBindingTransfer.Service_References.ServiceModels.Lookups;

namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public class LookupsModel
    {
        public List<Lookup> Lookups { get; set; }
    }
}