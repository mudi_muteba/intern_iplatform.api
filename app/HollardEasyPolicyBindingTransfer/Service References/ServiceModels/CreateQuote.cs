﻿using System.Collections.Generic;
using System.Linq;

namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public class CreateQuote
    {
        public CreateQuote()
        {
            SelectedProducts = new List<string>();
        }
        public int ClientId { get; set; }
        public List<string> SelectedProducts { get; set; }


        public void AddProducts(HollardTiaProducts[] products)
        {
            if (products.Contains(HollardTiaProducts.Building))
                SelectedProducts.Add("ABLDG");
            if (products.Contains(HollardTiaProducts.Contents))
                SelectedProducts.Add("ACNT");
            if (products.Contains(HollardTiaProducts.Motor))
                SelectedProducts.Add("ACAR");
        }
    }
}