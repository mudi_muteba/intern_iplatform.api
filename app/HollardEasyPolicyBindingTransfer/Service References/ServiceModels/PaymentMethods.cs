﻿namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public enum PaymentMethods
    {
        DirectDebit = 1,
        CreditCard = 2,
        CashCheque = 3,
        Giro = 4,
        BankTransfer = 5,
        BsBasis = 6
    }
}