﻿namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public class AddressApiModel
    {
        public string HouseCoName { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string Floor { get; set; }
        public string FloorExt { get; set; }
        public string PostStreet { get; set; }
        public string BuildingName { get; set; }
        public string BuildingNumber { get; set; }
        public string DepartmentName { get; set; }
        public string PoBox { get; set; }
        public int? SuburbValue { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
    }
}