﻿using System.Collections.Generic;
using HollardEasyPolicyBindingTransfer.Service_References.Response;

namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{

    public class SearchClient
    {
        public string IdNumber { get; set; }
        public string Surname { get; set; }
        //public int PolicyNo { get; set; }
        //public object ClaimNo { get; set; }
        public int ClientNo { get; set; }
        //public string Statuses { get; set; }
        //public int PageNo { get; set; }
        //public int PageSize { get; set; }
    }

    public class ClientSearchDetails
    {
        public int TiaPartyNo { get; set; }
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public object PolicyNo { get; set; }
        public object PolicyStatus { get; set; }
        public string PolicyStartDate { get; set; }
        public string PolicyRenewalDate { get; set; }
        public object QuoteId { get; set; }
        public object QuoteDate { get; set; }
        public object ClaimNo { get; set; }
        public object ClaimId { get; set; }
    }

    public class ClientSearchResponse
    {
        public List<ClientSearchDetails> Clients { get; set; }
    }

    public class ClientSearchResponseModel : IStatusReponse<ClientSearchResponse>
    {
        public int StatusCode { get; set; }
        public List<ResponseMessage> Messages { get; set; }
        public ClientSearchResponse Data { get; set; }
    }

}