﻿namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public class LoginRequestModel
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public LoginRequestModel(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }
}