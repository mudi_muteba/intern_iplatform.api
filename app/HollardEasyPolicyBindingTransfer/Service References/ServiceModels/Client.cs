﻿using System.Collections.Generic;

namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public class Client
    {
        public Client()
        {
            //Values = new ClientValues();
        }
        public int Id { get; set; }
        public int? TiaPartyNo { get; set; }
        public string IdNumber { get; set; }
        public int IdType { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string TelNo { get; set; }
        public string DateOfBirth { get; set; }
        public AddressApiModel PhysicalAddress { get; set; }
        public AddressApiModel PostalAddress { get; set; }
        public Dictionary<string, object> Values { get; set; }
        public double? OldPremium { get; set; }
        public double? NewPremium { get; set; }
    }

    public class ClientValues
    {
        public string MARITAL_STATE { get; set; }
        public string GENDER { get; set; }
        public string OCCUPATION_CODE { get; set; }
        public string OCCUPATION { get; set; }
        public string CURRENT_INSURER { get; set; }
        public string COMM_METHOD_NOTIFICATIONS { get; set; }
        public string COMM_METHOD_DOCUMENTS { get; set; }
        public string MARKETING_POST { get; set; }
        public string MARKETING_SMS { get; set; }
        public string MARKETING_TELEPHONIC { get; set; }
        public string MARKETING_EMAIL { get; set; }
        public string MOTORBIKE_LICENCE_TYPE { get; set; }
        public string MOTORBIKE_LICENSE_24_MONTHS { get; set; }
        public string SKIPPERS_LICENCE { get; set; }
        public string SKIPPER_CERTIFICATE { get; set; }
        public string INSOLVENT_UNDER_ADMIN { get; set; }
        public string ITC_CHECK_CONSENT { get; set; }
        public string ENATIS_CONSENT { get; set; }
        public string DEBT_REVIEW { get; set; }
        public string PREV_INSURANCE_CANCELLED { get; set; }
        public string REASON_FOR_CANCELLATION { get; set; }
        public string VIP_INDICATOR { get; set; }
        public string HOLLARD_EMPLOYEE_NO { get; set; }
        public string EMPLOYMENT_STATUS { get; set; }
        public string TIME_TRAVELLING { get; set; }
        public string DRIVING_CONVICTIONS { get; set; }
        public double? CAR_LICENCE_DATE { get; set; }
        public string CAR_LICENCE_TYPE { get; set; }
        public string CAR_LICENSE_24_MONTHS { get; set; }
        public double? MOTORBIKE_LICENCE_DATE { get; set; }
        public int? CURRENT_PREMIUM { get; set; }
        public int? JUDGEMENTS_NO { get; set; }
        public int? JUDGEMENTS_VALUE { get; set; }
        public int? PREV_BUILDINGS_COVER { get; set; }
        public int? PREV_CAR_COVER { get; set; }
        public int? PREV_CONTENTS_COVER { get; set; }
        public int? PREV_MOTORBIKE_COVER { get; set; }
        public string INITIALS { get; set; }
        public string CITIZENSHIP { get; set; }
        public string CITIZENSHIP_COUNTRY { get; set; }
        public string ADDRESS_TYPE { get; set; }
        public string TAX_REF_NO { get; set; }
        public string PREF_NAME { get; set; }
        public string TITLE { get; set; }
        public int? SUBURB_CODE { get; set; }
        public string HOME_TEL { get; set; }
        public string WORK_TEL { get; set; }
        public string FAX { get; set; }
        public string CONTACT_TEL { get; set; }
        public string CELLPHONE { get; set; }
        public string WORK_MOBILE { get; set; }
        public string ALT_EMAIL { get; set; }


    }
}