﻿using System.Collections.Generic;
using HollardEasyPolicyBindingTransfer.Service_References.Response;

namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{

    public class Brokerage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string Model { get; set; }
        public string BrokerFee { get; set; }
    }

    public class LoginDetails 
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TelNo { get; set; }
        public List<string> Profiles { get; set; }
        public Brokerage Brokerage { get; set; }
        public int TiaPartyNo { get; set; }
    }

    public class LoginResponseModel : IStatusReponse<LoginDetails>
    {
        public int StatusCode { get; set; }
        public List<ResponseMessage> Messages { get; set; }
        public LoginDetails Data { get; set; }
    }

  

}