﻿namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public enum AccountType
    {
        ChequeAccount = 1,
        SavingsAccount = 2,
        TransmissionAccount = 3
    }
}