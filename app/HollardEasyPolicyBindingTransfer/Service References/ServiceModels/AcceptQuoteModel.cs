﻿using System;

namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public class AcceptQuoteModel
    {
        public Client Client { get; set; }
        public PaymentDetailsApiModel PaymentDetails { get; set; }
        public DateTime InceptionDate { get; set; }
        public int StrikeDay { get; set; }
        public DocumentApiModel SignedProposal { get; set; }

    }

    public class PaymentDetailsApiModel
    {
        public PaymentDetailsApiModel()
        {
            Values = new SerializableDictionary<string, object>();
        }
        public int? TiaPaymentDetailsId { get; set; }
        public SerializableDictionary<string, object> Values { get; set; }
    }

    public class DocumentApiModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MimeType { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public byte[] Data { get; set; }
    }
}