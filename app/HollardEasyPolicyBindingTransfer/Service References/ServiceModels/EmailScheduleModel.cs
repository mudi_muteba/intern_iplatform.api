﻿namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public class EmailScheduleModel
    {

        public EmailScheduleModel(string email)
        {
            Email = email;
        }
        public string Email { get; set; }
    }
}