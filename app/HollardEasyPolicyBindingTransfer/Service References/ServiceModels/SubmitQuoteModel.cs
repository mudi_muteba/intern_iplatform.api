﻿namespace HollardEasyPolicyBindingTransfer.Service_References.ServiceModels
{
    public class SubmitQuoteModel
    {
        public QuoteDetails Quote { get; set; }
        public Client Client { get; set; }
        public ProxyApiModel Proxy { get; set; }
        public PaymentDetailsApiModel PaymentDetails { get; set; }
        public object CoPolicyHolders { get; set; }
    }

    public class ProxyApiModel
    {
        public ProxyApiModel()
        {
            IdType = IDType.SouthAfricanID;
            Values = new SerializableDictionary<string, object>();
            Values.AddString("CITIZENSHIP_COUNTRY", "1");
        }

        public ProxyApiModel(int tiaPartyNumber, string idNumber, string name, string surname) : this()
        {
            TiaPartyNo = tiaPartyNumber;
            IdNumber = idNumber;
            Name = name;
            Surname = surname;
        }

        public int Id { get; set; }
        public int TiaPartyNo { get; set; }
        public string IdNumber { get; set; }
        public IDType IdType { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public SerializableDictionary<string, object> Values { get; set; }
    }
}