﻿using System.Collections.Generic;
using HollardEasyPolicyBindingTransfer.Service_References.ServiceModels;

namespace HollardEasyPolicyBindingTransfer.Service_References.Response
{
    public class CreateClientResponse :  IStatusReponse<Client>
    {
        public int StatusCode { get; set; }
        public List<ResponseMessage> Messages { get; set; }
        public Client Data { get; set; }
    }
}