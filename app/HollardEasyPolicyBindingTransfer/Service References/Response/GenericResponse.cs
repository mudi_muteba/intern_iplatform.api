﻿using System.Collections.Generic;

namespace HollardEasyPolicyBindingTransfer.Service_References.Response
{
    public class GenericResponse :  IStatusReponse<string>
    {
        public int StatusCode { get; set; }
        public List<ResponseMessage> Messages { get; set; }
        public string Data { get; set; }
    }
}