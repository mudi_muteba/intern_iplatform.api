﻿namespace HollardEasyPolicyBindingTransfer.Service_References.Response
{
    public class ResponseMessage
    {
        public string Field { get; set; } 
        public string Message { get; set; } 
    }
}