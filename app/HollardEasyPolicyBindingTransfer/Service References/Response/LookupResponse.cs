﻿using System.Collections.Generic;
using HollardEasyPolicyBindingTransfer.Service_References.ServiceModels;
using HollardEasyPolicyBindingTransfer.Service_References.ServiceModels.Lookups;

namespace HollardEasyPolicyBindingTransfer.Service_References.Response
{
    public class LookupResponse : IStatusReponse<LookupsModel>
    {
        public int StatusCode { get; set; }
        public List<ResponseMessage> Messages { get; set; }
        public LookupsModel Data { get; set; }
    }
}