﻿using System.Collections.Generic;
using HollardEasyPolicyBindingTransfer.Service_References.ServiceModels;

namespace HollardEasyPolicyBindingTransfer.Service_References.Response
{
    public class QuoteResponseModel : IStatusReponse<QuoteDetails>
    {
        public int StatusCode { get; set; }
        public List<ResponseMessage> Messages { get; set; }
        public QuoteDetails Data { get; set; }
    }

}