using System;
using System.Linq;
using HollardEasyPolicyBindingTransfer.Service_References.ApiRequests;
using HollardEasyPolicyBindingTransfer.Service_References.Response;
using HollardEasyPolicyBindingTransfer.Service_References.ServiceModels;
using RestSharp;

namespace HollardEasyPolicyBindingTransfer.HollardEasyPolicyBindingService
{
    public class HollardTiaClient : IHollardTiaClient
    {

        private readonly TiaApiService m_TiaClient;

        public HollardTiaClient(string url)
        {
            m_TiaClient = new TiaApiService(new RestClient(url));
        }

        public void SetAuthorisationHeader(string authHeader)
        {
            m_TiaClient.SetAuthorisationHeader(authHeader);
        }

        public IRestRequest CreateLoginRequest(LoginRequestModel input)
        {
            return m_TiaClient.CreateAuthorisationToken(input);
        }

        public IRestRequest SearchClientByTiaPartyNumber(string tiaPartyNo, string authorisationToken)
        {
            return m_TiaClient.SearchClientByTiaPartyNumber(tiaPartyNo, authorisationToken);
        }

        public IRestRequest SearchPolicy(string policyNo, string authorisationToken)
        {
            return m_TiaClient.SearchPolicy(policyNo, authorisationToken);
        }

        public IRestRequest GenerateQuoteProposal(string email, int quoteId, string authorisationToken)
        {
            return m_TiaClient.GenerateQuoteProposal(email, quoteId, authorisationToken);
        }

        public IRestRequest GetProposal(int quoteId, string authorisationToken)
        {
            return m_TiaClient.GetProposal(quoteId, authorisationToken);
        }

        public IRestRequest SubmitQuote(SubmitQuoteModel submitQuoteModel, string authorisationToken)
        {
            return m_TiaClient.SubmitQuote(submitQuoteModel, authorisationToken);
        }

        public IRestRequest AcceptQuote(int quoteId, AcceptQuoteModel acceptQuote, string authorisationToken)
        {
            return m_TiaClient.AcceptQuote(quoteId, acceptQuote, authorisationToken);
        }

        public IRestRequest GetBanks()
        {
            return m_TiaClient.GetBanks();
        }

        public IRestRequest EmailSchedule(int quoteId, string email, string authorisationToken)
        {
            return m_TiaClient.EmailSchedule(quoteId, authorisationToken, new EmailScheduleModel(email));
        }

        public T ExecuteRequest<T, TObj>(IRestRequest request, bool handleResponse = true) where T : IStatusReponse<TObj>, new() where TObj : new()
        {
            var response = m_TiaClient.Execute<T, TObj>(request);
            if (handleResponse)
            { HandleResponse(response); }
            return response;
        }

        private void HandleResponse<T>(IStatusReponse<T> respReponse)
        {
            if (respReponse == null)
            { ThrowException<T>(typeof(IStatusReponse<T>).ToString(), "Null response received"); }

            if (respReponse.Messages != null && respReponse.Messages.All(x => x.Message != "Success"))
            {
                var errors = string.Join(" " + Environment.NewLine, respReponse.Messages.Select(msg => msg.Message).ToList());
                ThrowException<T>(typeof(IStatusReponse<T>).ToString(), errors);
            }
        }

        private void ThrowException<T>(string responseType, string error)
        {
            var errorBuilder = string.Format("Error getting {0} from the Hollard Tia Client:{1}",
                responseType,
                error);
            throw new Exception(errorBuilder);
        }


    }

    public interface IHollardTiaClient
    {
        IRestRequest CreateLoginRequest(LoginRequestModel input);
        IRestRequest GenerateQuoteProposal(string email, int quoteId, string authorisationToken);
        IRestRequest GetProposal(int quoteId, string authorisationToken);
        IRestRequest SearchPolicy(string policyNo, string authorisationToken);
        T ExecuteRequest<T, TObj>(IRestRequest request, bool handleResponse = true) where T : IStatusReponse<TObj>, new() where TObj : new();
        IRestRequest SubmitQuote(SubmitQuoteModel submitQuoteModel, string authorisationToken);
        IRestRequest AcceptQuote(int quoteId, AcceptQuoteModel acceptQuote, string authorisationToken);
        IRestRequest SearchClientByTiaPartyNumber(string tiaPartyNo, string authorisationToken);
        IRestRequest GetBanks();
        IRestRequest EmailSchedule(int quoteId, string email, string authorisationToken);
        void SetAuthorisationHeader(string authHeader);
    }
}