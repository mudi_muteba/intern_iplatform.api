﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common.Logging;
using Domain.Policies.Message;
using Domain.PolicyBindings.Messages;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using HollardEasyPolicyBindingTransfer.Service_References.Response;
using HollardEasyPolicyBindingTransfer.Service_References.ServiceModels;
using HollardEasyPolicyBindingTransfer.Service_References.ServiceModels.Lookups;
using iPlatform.Api.DTOs.PolicyBindings;
using iPlatform.Api.DTOs.PolicyBindings.Request;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using Newtonsoft.Json;
using RestSharp;
using Workflow.Messages;
using Workflow.PolicyBinding.Domain;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;

namespace HollardEasyPolicyBindingTransfer.HollardEasyPolicyBindingService
{
    public class HollardEasyPolicyBindingServiceProvider : ITransferPolicyBindingToInsurer
    {
        public ExecutionPlan ExecutionPlan { get; set; }
        public IRetryStrategy RetryStrategy { get; set; }
        private IHollardTiaClient m_Client { get; set; }
        private readonly CommunicationMetadata m_Mail;
        private string m_AcceptanceMailAddress;
        private readonly Guid m_ChannelId;
        private static readonly ILog m_Log = LogManager.GetLogger<HollardEasyPolicyBindingServiceProvider>();

        public HollardEasyPolicyBindingServiceProvider(IHollardTiaClient mClient, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId, string mailAddress)
        {
            m_Mail = mail;
            m_ChannelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
            m_Client = mClient;
            m_AcceptanceMailAddress = mailAddress;
        }

        public PolicyBindingTransferResult Transfer(SubmitPolicyBindingMessage message)
        {
            var quote = message.Dto;
            var result = Execute(quote);

            if (result.Error.HasError)
            {
                var error = result.Error.Error;
                m_Log.ErrorFormat("Quote [{0}] failed to upload to Hollard Easy with failure reason {1}", message.Dto.RequestId, error);
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}", error), error, message.Dto.RequestId, InsurerName.HollardEasy.Name(), m_ChannelId, m_Mail, quote.Environment));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, m_AcceptanceMailAddress));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.HollardEasy.Name(), message.Dto.RequestId, string.Format("Failure Reason {0}", error), m_ChannelId));
                return new PolicyBindingTransferResult(false);
            }

            m_Log.InfoFormat("Quote [{0}] successfully uploaded to Hollard Easy", message.Dto.RequestId);
            return new PolicyBindingTransferResult(!result.Error.HasError);
        }

        private QuoteDetails Execute(PolicyBindingRequestDto dto)
        {
            var isSuccess = false;
            var acceptQuoteResult = new QuoteDetails();
            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(dto.RequestInfo.ProductCode, InsurerName.HollardEasy.Name(), dto.RequestId, m_ChannelId));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(dto, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, m_AcceptanceMailAddress));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationPdfEmailMessage(dto.ChannelId, dto.QuoteId, dto.LeadId, dto.PartyId, dto.AgentId, dto.AgentDetail, dto.AgentPartyId));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeletePolicyBindingCompletedMessage.Create(dto.QuoteId, dto.RequestId));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(dto.RequestInfo.ProductCode, InsurerName.HollardEasy.Name(), dto.RequestId, m_ChannelId));
                    })
                .OnStart(
                    () => ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(dto.RequestInfo.ProductCode, InsurerName.HollardEasy.Name(), dto.RequestId, dto.SerializeAsXml(), m_ChannelId))
                )
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(dto.RequestInfo.ProductCode, InsurerName.HollardEasy.Name(), dto.RequestId, response.SerializeAsXml(), m_ChannelId))
                )
                .While(() => !isSuccess)
                .Execute(() =>
                {
                    try
                    {

                        //1.Login Get Auth Token
                        var authToken = GetAuthenticationToken(dto, dto.RequestInfo.ProductCode);

                        //2.Get the Quote using quoteId
                        int quoteId;
                        if (!int.TryParse(dto.RequestInfo.InsurerReference, out quoteId))
                        {
                            throw new Exception("Invalid quote id: " + dto.RequestInfo.InsurerReference);
                        }
                        var result = m_Client.ExecuteRequest<QuoteResponseModel, QuoteDetails>(m_Client.GetProposal(quoteId, authToken));

                        //3.Update the quote with the selected cover Orange/Purple
                        var isOrange = string.Equals(dto.RequestInfo.ProductCode, "HOLEASYO", StringComparison.InvariantCultureIgnoreCase);
                        foreach (var line in result.Data.Lines)
                        {
                            SetSelectedProduct(line, isOrange);
                        }

                        //4.Submit the quote
                        var client = GetClient(result.Data, authToken);
                        var paymentDetails = GeneratePaymentDetails(dto);
                        var submitQuoteModel = GenerateSubmitQuoteModel(client.Data, result.Data, paymentDetails);

                        LogTransfer(submitQuoteModel);

                        var submittedQuote = m_Client.ExecuteRequest<QuoteResponseModel, QuoteDetails>(m_Client.SubmitQuote(submitQuoteModel, authToken));

                        //5.Generate Proposal document
                        //m_Client.ExecuteRequest<QuoteResponseModel, QuoteDetails>(m_Client.GenerateQuoteProposal(m_Mail.CommunicationMessage.Address, quoteId, authToken));

                        //6.Accept the quote
                        var acceptQuote = GenerateAcceptQuoteModel(client.Data, paymentDetails, dto);
                        var acceptResult = m_Client.ExecuteRequest<QuoteResponseModel, QuoteDetails>(m_Client.AcceptQuote(quoteId, acceptQuote, authToken));
                        var emailResult = m_Client.ExecuteRequest<QuoteResponseModel, QuoteDetails>(m_Client.EmailSchedule(quoteId, dto.InsuredInfo.EmailAddress, authToken));

                        isSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        m_Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.HollardEasy.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.HollardEasy.Name(), dto.QuotePlatformId, ex, m_ChannelId));
                        acceptQuoteResult.Error = new Errors(true, ex.Message);
                        return false;
                    }
                    return true;
                });
            return acceptQuoteResult;
        }

        private string GetAuthenticationToken(PolicyBindingRequestDto request, string productCode)
        {
            var userSetting = request.UserRatingSettings.FirstOrDefault(x => string.Equals(x.Product.ProductCode, productCode, StringComparison.InvariantCultureIgnoreCase));

            if (userSetting == null)
            { throw new Exception("Invalid user credentials settings supplied. Please ensure that this user has Rating Settings for this product."); }

            var loginResult = Login(new LoginRequestModel(userSetting.RatingUsername, userSetting.RatingPassword));
            return loginResult.Data.Token;
        }

        private LoginResponseModel Login(LoginRequestModel input)
        {
            var result = m_Client.ExecuteRequest<LoginResponseModel, LoginDetails>(m_Client.CreateLoginRequest(input));
            return result;
        }

        private void SetSelectedProduct(Line section, bool isOrangeSelected)
        {
            var orangeRiskItem = section.Risks.FirstOrDefault(o => string.Equals(o.Name, "Comprehensive cover - Orange", StringComparison.InvariantCultureIgnoreCase));
            var purpuleRiskItem = section.Risks.FirstOrDefault(o => string.Equals(o.Name, "Comprehensive cover - Purple", StringComparison.InvariantCultureIgnoreCase));

            if (orangeRiskItem != null)
            { orangeRiskItem.Selected = isOrangeSelected; }

            if (purpuleRiskItem != null)
            { purpuleRiskItem.Selected = !isOrangeSelected; }
        }

        private AcceptQuoteModel GenerateAcceptQuoteModel(Client client, PaymentDetailsApiModel paymentDetails, PolicyBindingRequestDto dto)
        {
            var acceptQuote = new AcceptQuoteModel();
            acceptQuote.Client = client;
            acceptQuote.InceptionDate = DateTime.Now;
            acceptQuote.PaymentDetails = paymentDetails;
            acceptQuote.StrikeDay = GetDebitDate(dto);
            acceptQuote.SignedProposal = GetDefaultDocumentModel();

            return acceptQuote;
        }

        private SubmitQuoteModel GenerateSubmitQuoteModel(Client client, QuoteDetails quote, PaymentDetailsApiModel paymentDetails)
        {
            var acceptQuote = new SubmitQuoteModel();
            acceptQuote.Client = client;
            acceptQuote.Quote = quote;
            acceptQuote.PaymentDetails = paymentDetails;
            return acceptQuote;
        }

        private PaymentDetailsApiModel GeneratePaymentDetails(PolicyBindingRequestDto dto)
        {
            var policyBindingQuestions = dto.RequestInfo.PolicyBindingQuestions;
            if (policyBindingQuestions != null && policyBindingQuestions.Any())
            {
                var paymentDetails = new PaymentDetailsApiModel();
                paymentDetails.Values.Add("ACCOUNT_HOLDER_NAME", string.Format("{0} {1}", dto.InsuredInfo.Firstname, dto.InsuredInfo.Surname));
                paymentDetails.Values.Add("ACCOUNT_TYPE", GetPolicyBankAccountType(policyBindingQuestions).ToString());
                paymentDetails.Values.Add("BANK_ACCOUNT_NO", GetPolicyBindingQuestionAnswer(policyBindingQuestions, "Account Number"));
                paymentDetails.Values.Add("BANK_ID_NO", GetBankId(policyBindingQuestions));
                paymentDetails.Values.Add("ENABLE_NAEDO", GetNaedoStatus(policyBindingQuestions));
                //paymentDetails.Values.Add("PAYMENT_METHOD", GetPaymentType(policyBindingQuestions));
                return paymentDetails;
            }
            return new PaymentDetailsApiModel();
        }

        private CreateClientResponse GetClient(QuoteDetails quote, string authToken)
        {
            var searchClient = m_Client.ExecuteRequest<CreateClientResponse, Client>(m_Client.SearchClientByTiaPartyNumber(quote.TiaClientPartyNo.ToString(), authToken));

            return searchClient;
        }

        private string GetPolicyBindingQuestionAnswer(List<ItemPolicyBindingQuestionAnswerDto> policyBindingQuestions, string questionName)
        {
            var bankQuestion = policyBindingQuestions.FirstOrDefault(x => string.Equals(x.Question.Name.Trim(), questionName.Trim(), StringComparison.InvariantCultureIgnoreCase));
            return bankQuestion != null ? bankQuestion.Answer.Trim() : "";
        }

        private int GetPolicyBankAccountType(List<ItemPolicyBindingQuestionAnswerDto> policyBindingQuestions)
        {
            var accountType = 1;
            var answer = GetPolicyBindingQuestionAnswer(policyBindingQuestions, "Account Type");
            if (string.Equals(answer, "Current", StringComparison.CurrentCultureIgnoreCase))
                accountType = (int)AccountType.ChequeAccount;

            if (string.Equals(answer, "Savings", StringComparison.CurrentCultureIgnoreCase))
                accountType = (int)AccountType.SavingsAccount;

            if (string.Equals(answer, "Transmission", StringComparison.CurrentCultureIgnoreCase))
                accountType = (int)AccountType.TransmissionAccount;

            return accountType;
        }

        private string GetNaedoStatus(List<ItemPolicyBindingQuestionAnswerDto> policyBindingQuestions)
        {
            var answer = GetPolicyBindingQuestionAnswer(policyBindingQuestions, "Do you agree to NAEDO and flexible debit order collections?");
            return string.Equals(answer, "Yes", StringComparison.InvariantCultureIgnoreCase) ? "1" : "0";
        }

        private string GetBankId(List<ItemPolicyBindingQuestionAnswerDto> policyBindingQuestions)
        {
            var bankName = DefaultBankCodes.GetHollardBankCode(GetPolicyBindingQuestionAnswer(policyBindingQuestions, "Bank Name"));
            var branchCode = GetPolicyBindingQuestionAnswer(policyBindingQuestions, "Branch Name");

            var selectedBank = LookupBank(bankName, branchCode);

            if (selectedBank == null && DefaultBankCodes.DefaultBranchCode.TryGetValue(bankName, out branchCode))
            {
                selectedBank = LookupBank(bankName, branchCode);
            }

            return selectedBank != null ? selectedBank.Value : "";
        }

        private Lookup LookupBank(string bankName, string branchName)
        {
            var banks = m_Client.ExecuteRequest<LookupResponse, LookupsModel>(m_Client.GetBanks());

            var selectedBank = banks.Data.Lookups.FirstOrDefault(x => string.Equals(x.Dependency, bankName, StringComparison.CurrentCultureIgnoreCase) &&
                     x.Text.ToLower().Contains(branchName.ToLower()));
            return selectedBank;
        }

        private int GetPaymentType(List<ItemPolicyBindingQuestionAnswerDto> policyBindingQuestions)
        {
            var answer = GetPolicyBindingQuestionAnswer(policyBindingQuestions, "Payment Type");
            return (int)PaymentMethods.DirectDebit; //.ToString(); //Only one available for now
        }

        private int GetDebitDate(PolicyBindingRequestDto dto)
        {
            var policyBindingQuestions = dto.RequestInfo.PolicyBindingQuestions;
            if (policyBindingQuestions != null && policyBindingQuestions.Any())
            {
                int day;
                var answer = GetPolicyBindingQuestionAnswer(policyBindingQuestions, "Debit Date");
                return int.TryParse(answer, out day) ? day : 1;
            }
            return 1;
        }

        private void LogTransfer(SubmitQuoteModel dto)
        {
            string output = JsonConvert.SerializeObject(dto);
            m_Log.Info(output);
        }

        private DocumentApiModel GetDefaultDocumentModel()
        {
            var filename = "iplatform.pdf";
            var file = File.ReadAllBytes(filename);
            var document = new DocumentApiModel
            {
                Name = filename,
                MimeType = "application/pdf",
                Type = "BRK_PROP_SGN",
                Size = file.Length,
                Data = file
            };


            return document;
        }

    }
}
