﻿using System.Collections.Generic;

namespace HollardEasyPolicyBindingTransfer.HollardEasyPolicyBindingService
{
    public static class DefaultBankCodes
    {
        public static Dictionary<string, string> DefaultBranchCode = new Dictionary<string, string>
        {
            {"Absa Bank"    ,"632005"},
            {"Bank of Athens"   ,"410506"},
            {"Bidvest Bank" ,"462005"},
            {"Capitec Bank" ,"470010"},
            {"First National Bank"  ,"250655"},
            {"Investec Bank"    ,"580105"},
            {"Nedbank"  ,"198765"},
            {"SA Post Office"   ,"460005"},
            {"Standard Bank"    ,"051001"}
        };

        public static Dictionary<string, string> HollardBankCodes = new Dictionary<string, string>
        {
            {"Absa Bank"    ,"Absa"},
            {"Bidvest Bank" ,"Bidvest Bank Limited"},
            {"Capitec Bank" ,"Capitec Bank Limited"},
            {"First National Bank"  ,"Firstrand Bank"},
            {"Investec Bank"    ,"Investec Bank Limited"},
            {"Nedbank"  ,"Nedbank"},
            {"SA Post Office"   ,"South African Post Office"},
            {"Standard Bank"    ,"Standard Bank"}
        };
        
        public static string GetHollardBankCode(string bankName)
        {
            string outBank;
            if (HollardBankCodes.TryGetValue(bankName, out outBank))
            {
                return outBank;
            }
            return bankName;
        }

    }
}