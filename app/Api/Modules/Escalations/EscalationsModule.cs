﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Escalations;
using Domain.Escalations.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Escalations;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Escalations
{
    public class EscalationsModule : SecureModule
    {
        private readonly IPaginateResults _paginator;
        private readonly IExecutionPlan _executionPlan;

        public EscalationsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Post[SystemRoutes.EscalationPlans.Post.Route] = p => Save();
            Post[SystemRoutes.EscalationPlans.SearchWithPagination.Route] = p => GetAll();
        }

        private Negotiator Save()
        {
            var dto = this.Bind<EscalationPlanDto>();
            var result = _executionPlan.Execute<EscalationPlanDto, int>(dto);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.EscalationPlans.CreateGetById(result.Response));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAll()
        {
            var dto = this.Bind<EscalationPlanDto>();

            var queryResult = _executionPlan
                .Query<FindEscalationPlanQuery, EscalationPlan, PagedResultDto<EscalationPlanDto>, EscalationPlanDto>()
                .Configure(x => x.WithParameters(dto.Channels))
                .OnSuccess(result => Mapper.Map<PagedResultDto<EscalationPlanDto>>(result.Paginate(_paginator, dto)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}