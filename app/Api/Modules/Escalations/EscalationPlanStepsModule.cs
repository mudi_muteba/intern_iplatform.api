﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Escalations;
using Domain.Escalations.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Escalations;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Escalations
{
    public class EscalationPlanStepsModule : SecureModule
    {
        private readonly IPaginateResults _paginator;
        private readonly IExecutionPlan _executionPlan;

        public EscalationPlanStepsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Post[SystemRoutes.EscalationPlanSteps.Post.Route] = p => Save();
            Post[SystemRoutes.EscalationPlanSteps.SearchWithPagination.Route] = p => GetAll();
        }

        private Negotiator Save()
        {
            var dto = this.Bind<EscalationPlanStepDto>();
            var result = _executionPlan.Execute<EscalationPlanStepDto, int>(dto);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.EscalationPlans.CreateGetById(result.Response));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAll()
        {
            var dto = this.Bind<EscalationPlanStepDto>();

            var queryResult = _executionPlan
                .Query<FindEscalationPlanStepQuery, EscalationPlanStep, PagedResultDto<EscalationPlanStepDto>, EscalationPlanStepDto>()
                .Configure(x => x.WithParameters(dto.EscalationPlanId))
                .OnSuccess(result => Mapper.Map<PagedResultDto<EscalationPlanStepDto>>(result.Paginate(_paginator, dto)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}