﻿using Api.Infrastructure;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Escalations;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Escalations
{
    public class EscalationPlanExecutionHistoryModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;

        public EscalationPlanExecutionHistoryModule(IExecutionPlan executionPlan)
        {
            _executionPlan = executionPlan;

            Put[SystemRoutes.EscalationPlanExecutionHistory.Put.Route] = _ => EditEscalationPlanExecutionHistoryStatus();
        }

        private Negotiator EditEscalationPlanExecutionHistoryStatus()
        {
            var dto = this.Bind<EditEscalationDto>();
            var result = _executionPlan.Execute<EditEscalationDto, int>(dto);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}