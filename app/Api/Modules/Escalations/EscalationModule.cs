using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Escalations;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Escalations
{
    public class EscalationModule : SecureModule
    {
        private readonly IPaginateResults _paginator;
        private readonly IExecutionPlan _executionPlan;

        public EscalationModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Put[SystemRoutes.EscalationPlans.SearchWithPagination.Route] = p => Edit();
        }

        private Negotiator Edit()
        {
            var dto = this.Bind<EscalationPlanDto>();
            var result = _executionPlan.Execute<_executionPlan, int>(dto);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}