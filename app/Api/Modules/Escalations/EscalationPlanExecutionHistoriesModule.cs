﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Escalations;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Escalations;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Escalations
{
    public class EscalationPlanExecutionHistoriesModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;
        private readonly IRepository _repository;

        public EscalationPlanExecutionHistoriesModule(IExecutionPlan executionPlan, IPaginateResults paginator, IRepository repository)
        {
            _executionPlan = executionPlan;
            _paginator = paginator;
            _repository = repository;

            Post[SystemRoutes.EscalationPlanExecutionHistory.GetAllWithPagination.Route] = _ => GetAll(new Pagination(_.pageNumber, _.pageSize));
            Post[SystemRoutes.EscalationPlanExecutionHistory.Post.Route] = _ => CreateEscalation();
        }

        private Negotiator GetAll(Pagination pagination = null)
        {
            pagination = pagination ?? this.Bind<Pagination>();

            var pagedResults = _repository.GetAll<EscalationPlanExecutionHistory>().Paginate(_paginator, pagination);
            var pagedResultDto = Mapper.Map<PagedResultDto<EscalationHistoryDto>>(pagedResults);
            var queryResult = new QueryResult<PagedResultDto<EscalationHistoryDto>>(pagedResultDto);

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response).CreateLinks(c => SystemRoutes.EscalationPlanExecutionHistory.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateEscalation()
        {
            var dto = this.Bind<CreateEscalationDto>();
            var result = _executionPlan.Execute<CreateEscalationDto, int>(dto);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}