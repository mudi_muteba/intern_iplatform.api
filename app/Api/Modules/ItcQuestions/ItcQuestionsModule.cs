﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.ItcQuestionDefinition;
using Domain.ItcQuestionDefinition.Queries;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.ItcQuestion;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Linq;

namespace Api.Modules.ItcQuestions
{
    public class ItcQuestionsModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;

        public ItcQuestionsModule(IExecutionPlan executionPlan)
        {
            m_ExecutionPlan = executionPlan;

            Options[SystemRoutes.ItcQuestions.Options.Route] = p => GetOptions();

            Get[SystemRoutes.ItcQuestions.GetById.Route] = p => GetItcQuestionById(p.id);
            Get[SystemRoutes.ItcQuestions.GetByChannelId.Route] = p => GetItcQuestionByChannelId(p.id);
            Get[SystemRoutes.ItcQuestions.GetByChannelCode.Route] = p => GetItcQuestionByChannelCode(p.channelCode);

            Post[SystemRoutes.ItcQuestions.Post.Route] = p => CreateItcQuestion();
            Put[SystemRoutes.ItcQuestions.Put.Route] = p => UpdateItcQuestion(p.id);
            Delete[SystemRoutes.ItcQuestions.Delete.Route] = p => RemoveItcQuestion(p.id);
        }

        private Negotiator GetOptions()
        {
            return Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ItcQuestions));
        }

        private Negotiator GetItcQuestionById(int id)
        {
            var result = GetItcQuestionByIdQuery(id);

            var response = result.CreateGETResponse()
                .CreateLinks(r => SystemRoutes.ItcQuestions.CreateLinks(r));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetItcQuestionByChannelCode(string channelCode)
        {
            var result = GetItcQuestionByChannelCodeQuery(channelCode);

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ItcQuestions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetItcQuestionByChannelId(int channelId)
        {
            var result = GetItcQuestionByChannelIdQuery(channelId);

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ItcQuestions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateItcQuestion()
        {
            var itcquestion = this.Bind<CreateItcQuestionDto>();

            var existingQuestionResult = CheckForItcQuestionByChannelIdQuery(itcquestion.ChannelId);

            var existingQuestion = existingQuestionResult.Response ?? null;

            if (existingQuestion != null)
            {
                var itcquestionToDelete = this.Bind<RemoveItcQuestionDto>();
                itcquestionToDelete.Id = existingQuestion.Id;

                m_ExecutionPlan.Execute<RemoveItcQuestionDto, int>(itcquestionToDelete);
            }

            var result = m_ExecutionPlan.Execute<CreateItcQuestionDto, int>(itcquestion);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ItcQuestions.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateItcQuestion(int id)
        {
            var itcquestion = this.Bind<ItcQuestionDefinitionDto>();
            itcquestion.Id = id;

            var updateDto = Mapper.Map<ItcQuestionDefinitionDto, UpdateItcQuestionDto>(itcquestion);

            var result = m_ExecutionPlan.Execute<UpdateItcQuestionDto, int>(updateDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ItcQuestions.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator RemoveItcQuestion(int id)
        {
            var itcquestion = this.Bind<RemoveItcQuestionDto>();
            itcquestion.Id = id;

            var result = m_ExecutionPlan.Execute<RemoveItcQuestionDto, int>(itcquestion);

            var response = result.DeletePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private QueryResult<ItcQuestionDefinitionDto> GetItcQuestionByIdQuery(int id)
        {
            return m_ExecutionPlan
                .Query<ItcQuestionByIdQuery, ItcQuestionDefinition, ItcQuestionDefinitionDto>()
                .Configure(q => q.WithCriteria(id))
                .OnSuccess(r => Mapper.Map<ItcQuestionDefinition, ItcQuestionDefinitionDto>(r.FirstOrDefault()))
                .Execute();
        }

        private QueryResult<ItcQuestionDefinitionDto> GetItcQuestionByChannelIdQuery(int channelId)
        {
            return m_ExecutionPlan
                .Query<ItcQuestionByChannelIdQuery, ItcQuestionDefinition, ItcQuestionDefinitionDto>()
                .Configure(q => q.WithChannelId(channelId))
                .OnSuccess(r => Mapper.Map<ItcQuestionDefinition, ItcQuestionDefinitionDto>(r.FirstOrDefault()))
                .Execute();
        }

        private QueryResult<ItcQuestionDefinitionDto> CheckForItcQuestionByChannelIdQuery(int? channelId)
        {
            return m_ExecutionPlan
                .Query<CheckItcQuestionByChannelIdQuery, ItcQuestionDefinition, ItcQuestionDefinitionDto>()
                .Configure(q => q.WithChannelId(channelId))
                .OnSuccess(r => Mapper.Map<ItcQuestionDefinition, ItcQuestionDefinitionDto>(r.FirstOrDefault()))
                .Execute();
        }

        private QueryResult<ItcQuestionDefinitionDto> GetItcQuestionByChannelCodeQuery(string channelCode)
        {
            return m_ExecutionPlan
                .Query<ItcQuestionByChannelCodeQuery, ItcQuestionDefinition, ItcQuestionDefinitionDto>()
                .Configure(q => q.WithChannelCode(channelCode))
                .OnSuccess(r => Mapper.Map<ItcQuestionDefinition, ItcQuestionDefinitionDto>(r.FirstOrDefault()))
                .Execute();
        }
    }
}