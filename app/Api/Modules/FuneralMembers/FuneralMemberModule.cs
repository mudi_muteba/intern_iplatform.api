﻿using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FuneralMembers;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy.ModelBinding;
using Domain.FuneralMembers.Queries;
using Domain.FuneralMembers;
using AutoMapper;

namespace Api.Modules.FuneralMembers
{
    public class FuneralMemberModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public FuneralMemberModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.FuneralMembers.GetById.Route] = p => GetFuneralMember(p.id);

            Put[SystemRoutes.FuneralMembers.Put.Route] = p => Update(p.id);

            Put[SystemRoutes.FuneralMembers.PutDisable.Route] = p => DisableFuneralMember(p.id);
        }

        private Negotiator GetFuneralMember(int id)
        {
            var result = executionPlan
                .GetById<FuneralMember, ListFuneralMemberDto>(() => repository.GetById<FuneralMember>(id))
                .OnSuccess(Mapper.Map<FuneralMember, ListFuneralMemberDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.FuneralMembers.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator Update(int id)
        {
            var editTeamDto = this.Bind<EditFuneralMemberDto>();
            editTeamDto.Id = id;

            var result = executionPlan.Execute<EditFuneralMemberDto, int>(editTeamDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.FuneralMembers.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator DisableFuneralMember(int id)
        {
            var disableFuneralMemberDto = new DisableFuneralMemberDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableFuneralMemberDto, int>(disableFuneralMemberDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.FuneralMembers.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}