﻿using Api.Models;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FuneralMembers;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using Nancy.ModelBinding;
using Domain.FuneralMembers.Queries;
using Domain.FuneralMembers;
using AutoMapper;
using Domain.Activities.Builder;
using Domain.Leads.Builder;
using iPlatform.Api.DTOs.Base;

namespace Api.Modules.FuneralMembers
{
    public class FuneralMembersModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public FuneralMembersModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Post[SystemRoutes.FuneralMembers.Post.Route] = p => CreateFuneralMember();

            Get[SystemRoutes.FuneralMembers.GetByProposalDefinitionId.Route] = p => GetByProposalId(p.id);

            Options[SystemRoutes.FuneralMembers.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.FuneralMembers));
        }

        private Negotiator CreateFuneralMember()
        {
            var createFuneralMemberDto = this.Bind<CreateFuneralMemberDto>();

            var result = executionPlan.Execute<CreateFuneralMemberDto, int>(createFuneralMemberDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.FuneralMembers.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetByProposalId(int id)
        {

            var result = executionPlan
                .Query<GetFuneralMembersByProposalDefinitionIdQuery, FuneralMember, ListResultDto<ListFuneralMemberDto>, ListFuneralMemberDto>()
                .Configure(q => q.ByProposalDefinitionId(id))
                .OnSuccess(r => Mapper.Map<List<FuneralMember>, ListResultDto<ListFuneralMemberDto>>(r.ToList()))
                .Execute();

            LISTResponseDto<ListFuneralMemberDto> response = result.CreateLISTResponse(result.Response)
                .CreateLinks(c => SystemRoutes.FuneralMembers.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}