﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.SecondLevel.Answers;
using iPlatform.Api.DTOs.SecondLevel.Individual;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.SecondLevel
{
    public class SecondLevelModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public SecondLevelModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;
            Put[SystemRoutes.SecondLevel.Answers.SaveForQuote.Route] = p => SaveAnswersForQuote(p.quoteId);
            Get[SystemRoutes.SecondLevel.Questions.GetForQuoteId.Route] = p => GetQuestionsByQuoteId(p.quoteId);

            Options[SystemRoutes.SecondLevel.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.SecondLevel));
        }

        public Negotiator GetQuestionsByQuoteId(int quoteId)
        {
            GetByIdResult<SecondLevelQuoteDto> quote = _executionPlan
                .GetById<Quote, SecondLevelQuoteDto>(() => _repository.GetById<Quote>(quoteId))
                .OnSuccess(Mapper.Map<Quote, SecondLevelQuoteDto>)
                .Execute();

            GetByIdResult<SecondLevelIndividualDto> individual = _executionPlan
                .GetById<Individual, SecondLevelIndividualDto>(() => _repository.GetById<Individual>(quote.Response.PartyId))
                .OnSuccess(Mapper.Map<Individual, SecondLevelIndividualDto>)
                .Execute();

            quote.Response.SetIndividual(individual.Response);

            HandlerResult<SecondLevelQuestionsForQuoteDto> result = _executionPlan.Execute<SecondLevelQuoteDto, SecondLevelQuestionsForQuoteDto>(quote.Response);
            
            GETResponseDto<SecondLevelQuestionsForQuoteDto> response = result.CreateGETResponse()
                 .CreateLinks(c => SystemRoutes.SecondLevel.Questions.CreateGetQuestionsByQuoteId(quoteId));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SaveAnswersForQuote(int quoteId)
        {
            var savedto = this.Bind<SecondLevelQuestionsAnswersSaveDto>();
            savedto.QuoteId = quoteId;

            HandlerResult<bool> result = _executionPlan.Execute<SecondLevelQuestionsAnswersSaveDto, bool>(savedto);
            PUTResponseDto<bool> response = result.CreatePUTResponse();
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}