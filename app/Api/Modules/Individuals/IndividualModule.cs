﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Individuals;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Individuals
{
    public class IndividualModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public IndividualModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.Individuals.GetById.Route] = p => GetIndividual(p.id);
            Put[SystemRoutes.Individuals.GetById.Route] = p => UpdateIndividual(p.id);

            Options[SystemRoutes.Individuals.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Individuals));
        }


        private Negotiator GetIndividual(int id)
        {
            var value = repository.GetById<Individual>(id);
            GetByIdResult<IndividualDto> result = executionPlan
                .GetById<Individual, IndividualDto>(() => repository.GetById<Individual>(id))
                .OnSuccess(Mapper.Map<Individual, IndividualDto>)
                .Execute();

            GETResponseDto<IndividualDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Individuals.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateIndividual(int id)
        {
            var editIndividualDto = this.Bind<EditIndividualDto>();
            editIndividualDto.Id = id;

            HandlerResult<int> result = executionPlan.Execute<EditIndividualDto, int>(editIndividualDto);
            PUTResponseDto<int> response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Individuals.CreateGetById(id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}