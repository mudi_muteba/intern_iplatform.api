﻿using System;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Individual;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Newtonsoft.Json;

namespace Api.Modules.Individuals
{
    public class IndividualsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public IndividualsModule(IExecutionPlan executionPlan, IPaginateResults paginator)
        {
            _executionPlan = executionPlan;
            _paginator = paginator;

            Post[SystemRoutes.Individuals.Post.Route] = p => CreateIndividual();
            Post[SystemRoutes.Individuals.PostSearch.Route] = p => SearchIndividual();

            Options[SystemRoutes.Individuals.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Individuals));
        }

        private Negotiator CreateIndividual()
        {
            var createIndividualDto = this.Bind<CreateIndividualDto>();

            createIndividualDto.Userid = (Context.CurrentUser as ApiUser).UserId;

            var result = _executionPlan.Execute<CreateIndividualDto, int>(createIndividualDto);
            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.Individuals.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchIndividual()
        {
            var criteria = this.Bind<IndividualSearchDto>();

            var queryResult = _executionPlan
                .Search<SearchIndividualsQuery, Individual, IndividualSearchDto, PagedResultDto<ListIndividualDto>, ListIndividualDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListIndividualDto>>(result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();


            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Individuals.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        

    }
}