﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Reports.DigitalSignature.BrokerNote;
using Domain.Reports.DigitalSignature.BrokerNote.Queries;
using Domain.Reports.DigitalSignature.BrokerNote.StoredProcedures;
using Domain.Reports.DigitalSignature.CompanyDetails;
using Domain.Reports.DigitalSignature.CompanyDetails.Queries;
using Domain.Reports.DigitalSignature.CompanyDetails.StoredProcedures;
using Domain.Reports.DigitalSignature.DebitOrder;
using Domain.Reports.DigitalSignature.DebitOrder.Queries;
using Domain.Reports.DigitalSignature.DebitOrder.StoredProcedures;
using Domain.Reports.DigitalSignature.Popi;
using Domain.Reports.DigitalSignature.Popi.Queries;
using Domain.Reports.DigitalSignature.Popi.StoredProcedures;
using Domain.Reports.DigitalSignature.RecordOfAdvice;
using Domain.Reports.DigitalSignature.RecordOfAdvice.Queries;
using Domain.Reports.DigitalSignature.RecordOfAdvice.StoredProcedures;
using Domain.Reports.DigitalSignature.Sla;
using Domain.Reports.DigitalSignature.Sla.Queires;
using Domain.Reports.DigitalSignature.Sla.StoredProcedures;
using Domain.SignFlow;
using Domain.SignFlow.Message;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;
using iPlatform.Api.DTOs.SignFlow;
using Infrastructure.Configuration;
using MasterData;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using SignFlow;
using Workflow.Messages;

namespace Api.Modules.SignFlow
{
    public class SignFlowModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IWorkflowRouter _router;
        private readonly IRepository _repository;

        public SignFlowModule(IExecutionPlan executionPlan, IWorkflowRouter router, IRepository repository)
        {
            _executionPlan = executionPlan;
            _router = router;
            _repository = repository;

            Get[SystemRoutes.SignFlow.GetById.Route] = p => GetSignFlowDocument(p.id);
            Post[SystemRoutes.SignFlow.Post.Route] = p => CreateDigitalSignature();
            Post[SystemRoutes.SignFlow.PostMultiple.Route] = p => CreateMultipleDigitalSignature();
            Put[SystemRoutes.SignFlow.Put.Route] = p => UpdateDigitalSignature();
            Put[SystemRoutes.SignFlow.PutUpdateStatus.Route] = p => UpdateDigitalSignatureStatus();
            Post[SystemRoutes.SignFlow.GetDigitalSignatureDocumentUpdateStatus.Route] = p => GetDigitalSignatureDocumentUpdateStatus();
            Post[SystemRoutes.SignFlow.GetBrokerNoteReport.Route] = p => GetBrokersNoteReport();
            Post[SystemRoutes.SignFlow.GetPopiDocReport.Route] = p => GetPopiDocReport();
            Post[SystemRoutes.SignFlow.GetDebitOrderReport.Route] = p => GetDebitOrderReport();
            Post[SystemRoutes.SignFlow.GetSlaReport.Route] = p => GetSlaReport();
            Post[SystemRoutes.SignFlow.GetRecordOfAdviceReport.Route] = p => GetRecordOfAdviceReport();
        }

        private Negotiator GetBrokersNoteReport()
        {
            var criteriaBrokerNote = this.Bind<BrokerNoteReportCriteriaDto>();

            var reportBrokerNoteQueryResult = _executionPlan
                .QueryStoredProcedure<GetBrokerNoteReportQuery, BrokerNoteReportExtract, BrokerNoteReportStoredProcedure, BrokerNoteReportExtractDto>(new BrokerNoteReportStoredProcedure(criteriaBrokerNote))
                .OnSuccess(Mapper.Map<IList<BrokerNoteReportExtract>, IList<BrokerNoteReportExtractDto>>)
                .Execute();

            var companyHeaderQueryResult = GetCompanyDetails(criteriaBrokerNote.InsurerCode);

            var result = new QueryResult<BrokerNoteReportDto>(new BrokerNoteReportDto
            {
                BrokerNoteData = reportBrokerNoteQueryResult.Response.FirstOrDefault(),
                HeaderData = companyHeaderQueryResult
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetDebitOrderReport()
        {
            var criteriaDebitOrder = this.Bind<DebitOrderReportCriteriaDto>();

            var reportDeditOrderQueryResult = _executionPlan
                .QueryStoredProcedure<GetDebitOrderReportQuery, DebitOrderReportExtract, DebitOrderReportStoredProcedure, DebitOrderReportExtractDto>(new DebitOrderReportStoredProcedure(criteriaDebitOrder))
                .OnSuccess(Mapper.Map<IList<DebitOrderReportExtract>, IList<DebitOrderReportExtractDto>>)
                .Execute();

            var companyHeaderQueryResult = GetCompanyDetails(criteriaDebitOrder.InsurerCode);

            var result = new QueryResult<DebitOrderReportDto>(new DebitOrderReportDto
            {
                DebitOrderData = reportDeditOrderQueryResult.Response.FirstOrDefault(),
                HeaderData = companyHeaderQueryResult
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetPopiDocReport()
        {
            var criteriaPopiDoc = this.Bind<PopiDocReportCriteriaDto>();

            var companyHeaderQueryResult = GetCompanyDetails(criteriaPopiDoc.InsurerCode);

            var result = new QueryResult<PopiDocReportDto>(new PopiDocReportDto
            {
                // PopiDocData = reportPopiDocQueryResult.Response.ToList(),
                HeaderData = companyHeaderQueryResult
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetSlaReport()
        {
            var criteriaSla = this.Bind<SlaReportCriteriaDto>();

            var reportSlaQueryResult = _executionPlan
               .QueryStoredProcedure<GetSlaReportQuery, SlaReportExtract, SlaReportStoredProcedure, SlaReportExtractDto>(new SlaReportStoredProcedure(criteriaSla))
               .OnSuccess(Mapper.Map<IList<SlaReportExtract>, IList<SlaReportExtractDto>>)
               .Execute();

            var companyHeaderQueryResult = GetCompanyDetails(criteriaSla.InsurerCode);

            var result = new QueryResult<SlaReportDto>(new SlaReportDto
            {
                SlaData = reportSlaQueryResult.Response.FirstOrDefault(),
                HeaderData = companyHeaderQueryResult
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetRecordOfAdviceReport()
        {
            var criteriaRecordOfAdvice = this.Bind<RecordOfAdviceReportCriteriaDto>();

            var criteriaRecordOfAdviceContact = new RecordOfAdviceContactReportCriteriaDto
            {
                Id = criteriaRecordOfAdvice.Id
            };

            var reportRecordOfAdviceQueryResult = _executionPlan
                .QueryStoredProcedure<GetRecordOfAdviceReportQuery, RecordOfAdviceReportExtract, RecordOfAdviceReportStoredProcedure, RecordOfAdviceReportExtractDto>(new RecordOfAdviceReportStoredProcedure(criteriaRecordOfAdvice))
                .OnSuccess(Mapper.Map<IList<RecordOfAdviceReportExtract>, IList<RecordOfAdviceReportExtractDto>>)
                .Execute();

            var reportRecordOfAdviceContactQueryResult = _executionPlan
                .QueryStoredProcedure<GetRecordOfAdviceContactReportQuery, RecordOfAdviceContactReportExtract, RecordOfAdviceContactReportStoredProcedure, RecordOfAdviceContactReportExtractDto>(new RecordOfAdviceContactReportStoredProcedure(criteriaRecordOfAdviceContact))
                .OnSuccess(Mapper.Map<IList<RecordOfAdviceContactReportExtract>, IList<RecordOfAdviceContactReportExtractDto>>)
                .Execute();

            var companyHeaderQueryResult = GetCompanyDetails(criteriaRecordOfAdvice.InsurerCode);

            var result = new QueryResult<RecordOfAdviceReportDto>(new RecordOfAdviceReportDto
            {
                RecordOfAdviceData = reportRecordOfAdviceQueryResult.Response.ToList(),
                RecordOfAdviceContactData = reportRecordOfAdviceContactQueryResult.Response.FirstOrDefault(),
                HeaderData = companyHeaderQueryResult
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetSignFlowDocument(int id)
        {
            var result = _executionPlan
                .GetById<SignFlowDocument, SignFlowDocumentDto>(() => _repository.GetById<SignFlowDocument>(id))
                .OnSuccess(Mapper.Map<SignFlowDocument, SignFlowDocumentDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.SignFlow.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator CreateDigitalSignature()
        {
            var createSignFlowDocumentDto = this.Bind<CreateSignFlowDocumentDto>();

            var result = _executionPlan.Execute<CreateSignFlowDocumentDto, int>(createSignFlowDocumentDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.SignFlow.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator CreateMultipleDigitalSignature()
        {
            var createMultipleSignFlowDocumentDto = this.Bind<CreateMultipleSignFlowDocumentDto>();

            var result = _executionPlan.Execute<CreateMultipleSignFlowDocumentDto, string>(createMultipleSignFlowDocumentDto);

            var response = result.CreatePOSTResponse(); //.CreateLinks(SystemRoutes.SignFlow.CreateGetById(result.Response));

           // return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator UpdateDigitalSignature()
        {
            var editSignFlowDocumentDto = this.Bind<EditSignFlowDocumentDto>();

            var result = _executionPlan.Execute<EditSignFlowDocumentDto, int>(editSignFlowDocumentDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.SignFlow.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator UpdateDigitalSignatureStatus()
        {
            var updateSignFlowDocumentDto = this.Bind<UpdateSignFlowDocumentDto>();

            var result = _executionPlan.Execute<UpdateSignFlowDocumentDto, int>(updateSignFlowDocumentDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.SignFlow.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetDigitalSignatureDocumentUpdateStatus()
        {
            var updateEventHandlerSignFlowDocumentDto = this.Bind<UpdateEventHandlerSignFlowDocumentDto>();

            var recieverResult = "Suc: Event Handled";

            var secretKey = ConfigurationReader.SignFlowSetting.SignFlowEventReceiverSecretKey;
            if (string.IsNullOrEmpty(secretKey))
            {
                recieverResult = "Err: SignFlowSecret missing from Application Settings";
                return Negotiate.WithLocationHeader(recieverResult);
            }

            if (updateEventHandlerSignFlowDocumentDto.Sfs != secretKey)
            {
                recieverResult = "Err: Access Denied";
                return Negotiate.WithLocationHeader(recieverResult);
            }

            if (updateEventHandlerSignFlowDocumentDto.Et.ToLower() == "document completed")
            {
                if (!string.IsNullOrEmpty(updateEventHandlerSignFlowDocumentDto.Di))
                {
                    var signFlowConnector = new SignFlowConnector();

                    var loginResult = signFlowConnector.Login();
                    if (loginResult.Result == "Success")
                    {
                        var getDocStatusResult = signFlowConnector.GetDocStatus(loginResult,
                            int.Parse(updateEventHandlerSignFlowDocumentDto.Di));

                        if (getDocStatusResult.DocStatus == "Completed")
                        {
                            updateEventHandlerSignFlowDocumentDto.Status = new DocumentStatuses().FirstOrDefault(c => c.Name == getDocStatusResult.DocStatus);
                            //signFlowConnector.GetDoc(getDocStatusResult, loginResult, int.Parse(updateEventHandlerSignFlowDocumentDto.Di));

                            HttpContext.Current.Response.Output.WriteLine(recieverResult);
                            HttpContext.Current.Response.End();

                            // Raise message to download document and update Singflow row in database
                            var routingMessage = new WorkflowRoutingMessage();
                            routingMessage.AddMessage( new SignFlowEventReceiverWorkflowExecutionMessage( new SignFlowEventReceiverTaskMetaData( int.Parse(updateEventHandlerSignFlowDocumentDto.Di), updateEventHandlerSignFlowDocumentDto.Status, getDocStatusResult, loginResult)));
                            _router.Publish(routingMessage);
                        }
                    }
                }
            }

            return Negotiate.WithLocationHeader(recieverResult);
        }


        public ReportHeaderExtractDto GetCompanyDetails(string insurerCode)
        {
            var companyHeaderQueryResult = new ReportHeaderExtractDto();

            var queryResult = _executionPlan
                .QueryStoredProcedure<GetCompanyHeaderReportQuery, ReportHeaderExtract, GetCompanyDetailsStoredProcedure, ReportHeaderExtractDto>(new GetCompanyDetailsStoredProcedure(insurerCode))
                 .OnSuccess(Mapper.Map<IList<ReportHeaderExtract>, IList<ReportHeaderExtractDto>>)
                .Execute();

            if (queryResult.Response != null)
            {
                companyHeaderQueryResult = queryResult.Response.FirstOrDefault();
            }

            return companyHeaderQueryResult;
        }
    }
}