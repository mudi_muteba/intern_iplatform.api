﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Compression;
using Domain.Base.Encryption;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.DocumentManagement;
using Domain.DocumentManagement.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.DocumentManagement;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.DocumentManagement
{
    public class DocumentManagementModule : SecureModule
    {

        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public DocumentManagementModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Post[SystemRoutes.DocumentManagementRoutes.Post.Route] = p => CreateDocument();

            Get[SystemRoutes.DocumentManagementRoutes.GetByCreatorId.Route] = p => GetUserDocumentsByCreatorId(p.id);

            Get[SystemRoutes.DocumentManagementRoutes.GetByPartyId.Route] = p => GetUserDocumentsByPartyId(p.id);

            Get[SystemRoutes.DocumentManagementRoutes.GetByDocumentId.Route] = p => GetDocumentById(p.document);

            Put[SystemRoutes.DocumentManagementRoutes.PutDelete.Route] = p => DeleteUserDocument();

            Options[SystemRoutes.SettingsQuoteUploads.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.DocumentManagementRoutes));
            
        }

        private Negotiator CreateDocument()
        {
            var document = this.Bind<CreateDocumentDto>();

            var result = _executionPlan.Execute<CreateDocumentDto, int>(document);

            var queryResult = _executionPlan
                .Query<GetAllUserFiles, UserDocument, List<DocumentDto>, DocumentDto>()
                .Configure(q => q.WithId(result.Response))
                .OnSuccess(r => Mapper.Map<List<UserDocument>, List<DocumentDto>>(r.ToList()))
                .Execute();

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.DocumentManagementRoutes.CreatePost());


            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetUserDocumentsByCreatorId(int creatorId)
        {
            var queryResult = _executionPlan
            .Query<GetAllUserFiles, UserDocument, List<DocumentDto>, DocumentDto>()
            .Configure(q => q.WithCreatedById(creatorId))
            .OnSuccess(r => Mapper.Map<List<UserDocument>, List<DocumentDto>>(r.ToList()))
            .Execute();

            var response = queryResult.CreateLISTResponse(Mapper.Map<ListResultDto<DocumentDto>>(queryResult.Response));
            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }

        private Negotiator GetUserDocumentsByPartyId(int partyId)
        {
            var queryResult = _executionPlan
            .Query<GetAllUserFiles, UserDocument, List<DocumentDto>, DocumentDto>()
            .Configure(q => q.WithPartyId(partyId))
            .OnSuccess(r => Mapper.Map<List<UserDocument>, List<DocumentDto>>(r.ToList()))
            .Execute();

            var response = queryResult.CreateLISTResponse(Mapper.Map<ListResultDto<DocumentDto>>(queryResult.Response));
            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }


        private Negotiator GetDocumentById(int documentId)
        {
            var queryResult = _executionPlan
            .Query<GetDocumentData, Document, DocumentResultDto>()
            .Configure(q => q.WithId(documentId))
            .OnSuccess(r =>
            {
                var document = r.FirstOrDefault();
                var documentResultDto =  Mapper.Map<Document, DocumentResultDto>(document);
                if (document != null && document.DocumentData.Contents != null)
                {
                    // Mapper.Map(document.DocumentData, documentResultDto);
                    var decrypted = Encryption.DecryptString<byte[]>(document.DocumentData.Contents);
                    var decompress = decrypted.Decompress();
                    documentResultDto.ContentBytes = decompress;
                }

                return documentResultDto;
            })
            .Execute();

            var response = queryResult.CreateGETResponse();
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator DeleteUserDocument()
        {
            var document = this.Bind<DeleteDocumentDto>();

            var result = _executionPlan.Execute<DeleteDocumentDto, int>(document);
          
            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.DocumentManagementRoutes.CreatePutDeleted());

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}