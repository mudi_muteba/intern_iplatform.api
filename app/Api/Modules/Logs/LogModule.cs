﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Leads;
using Domain.Leads.Builder;
using Domain.Leads.Queries;
using Domain.Party.Contacts;
using Domain.Party.RegisteredOwnerContacts;
using Domain.Party.Relationships;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Campaign;
using iPlatform.Api.DTOs.Leads.Quality;
using iPlatform.Api.DTOs.Logs;
using iPlatform.Api.DTOs.Party.Contacts;
using iPlatform.Api.DTOs.SectionsPerChannel;
using MasterData;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Logs
{
    public class LogModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;
        private readonly IProvideContext contextProvider;

        public LogModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository, IProvideContext contextProvider)
        {
            this.executionPlan = executionPlan;
            this.paginator = paginator;
            this.repository = repository;
            this.contextProvider = contextProvider;

            Post[SystemRoutes.LogReader.CreatePost()] = p => GetApiLogs();

            Options[SystemRoutes.LogReader.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.LogReader));
        }

        #region Lead
      

        private Negotiator GetApiLogs()
        {
            var getLogsDto = this.Bind<GetLogsDto>();

            var result = executionPlan.Execute<GetLogsDto, LogsResultDto>(getLogsDto); 

            var response = Mapper.Map<POSTResponseDto<LogsResultDto>>(result);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
      
        #endregion
    }
}