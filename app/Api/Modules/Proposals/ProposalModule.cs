﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Proposals;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Proposals
{
    //Why is this class here and what is it used for??
    // There is already a Module for proposals and where is there a get quote method on this conroller? 

    public class ProposalModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public ProposalModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Post[SystemRoutes.ProposalHeaders.GetRatingsById.Route] = p => GetQuotes(p.Id);
            Put[SystemRoutes.ProposalHeaders.EditShouldQuote.Route] = p => EditShouldQuote(p.Id);
            Put[SystemRoutes.ProposalHeaders.EditValidateProposal.Route] = p => EditValidateProposal(p.Id);
            Post[SystemRoutes.ProposalHeaders.ImportProposals.Route] = p => Import();

            Options[SystemRoutes.ProposalHeaders.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ProposalHeaders));
        }

        private Negotiator GetQuotes(int id)
        {
            var getQuoteDto = this.Bind<GetQuotesByProposalDto>();

            var result = _executionPlan.Execute<GetQuotesByProposalDto, ProposalQuoteDto>(getQuoteDto);

            var response = Mapper.Map<POSTResponseDto<ProposalQuoteDto>>(result);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator EditShouldQuote(int id)
        {
            var shouldRequoteDto = this.Bind<EditProposalHeaderShouldQuoteDto>();
            shouldRequoteDto.Id = id;

            var result = _executionPlan.Execute<EditProposalHeaderShouldQuoteDto, int>(shouldRequoteDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ProposalHeaders.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator EditValidateProposal(int id)
        {
            var validateProposalDto = this.Bind<EditProposalHeaderValidateProposalDto>();
            validateProposalDto.Id = id;

            var result = _executionPlan.Execute<EditProposalHeaderValidateProposalDto, int>(validateProposalDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ProposalHeaders.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }



        private Negotiator Import()
        {
            var submitLeadDto = this.Bind<SubmitLeadDto>();

            var result = _executionPlan.Execute<SubmitLeadDto, int>(submitLeadDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ProposalHeaders.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}