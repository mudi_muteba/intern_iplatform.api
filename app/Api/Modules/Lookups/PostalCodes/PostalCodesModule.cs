﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.PostalCodes;
using Domain.Lookups.PostalCodes.Mapping;
using Domain.Lookups.PostalCodes.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Lookups.PostalCodes;
using Nancy.Responses.Negotiation;
using System.Collections.Generic;
using Nancy.ModelBinding;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Api.Modules.Lookups.PostalCodes
{
    public class PostalCodesModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public PostalCodesModule(IExecutionPlan executionPlan, IPaginateResults paginator)
        {
            _executionPlan = executionPlan;
            _paginator = paginator;

            Get[SystemRoutes.Lookups.PostalCodes.Get.Route] =
                _ => GetAllPostalCodes(new Pagination());

            Get[SystemRoutes.Lookups.PostalCodes.GetWithPagination.Route] =
                p => GetAllPostalCodes(new Pagination(p.pageNumber, p.PageSize));

            Get[SystemRoutes.Lookups.PostalCodes.GetWithSearchTerm.Route] =
                p => GetAllPostalCodes(new Pagination(), p.searchTerm);

            Get[SystemRoutes.Lookups.PostalCodes.GetWithSearchTermAndPagination.Route] =
                p => GetAllPostalCodes(new Pagination(p.pageNumber, p.PageSize), p.searchTerm);

            Get[SystemRoutes.Lookups.PostalCodes.GetSimplified.Route] =
                _ => GetAllSimplifiedPostalCodes(new Pagination());

            Get[SystemRoutes.Lookups.PostalCodes.GetSimplifiedWithPagination.Route] =
                p => GetAllSimplifiedPostalCodes(new Pagination(p.pageNumber, p.PageSize));

            Get[SystemRoutes.Lookups.PostalCodes.GetSimplifiedWithSearchTerm.Route] =
                p => GetAllSimplifiedPostalCodes(new Pagination(), p.searchTerm);

            Get[SystemRoutes.Lookups.PostalCodes.GetSimplifiedWithSearchTermAndPagination.Route] =
                p => GetAllSimplifiedPostalCodes(new Pagination(p.pageNumber, p.PageSize), p.searchTerm);

            Get[SystemRoutes.Lookups.PostalCodes.SearchSuburbsAndPostalCodes.Route] =
                p => SearchSuburbsAndPostalCodes(p.search);

            Options[SystemRoutes.Lookups.PostalCodes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Lookups.PostalCodes));
        }

        private Negotiator GetAllPostalCodes(Pagination pagination, string searchTerm = "")
        {
            var queryResult = _executionPlan
                .Query<GetPostalCodesQuery, PostalCode, PagedResults<PostalCode>>()
                .Configure(q => q.WithSearchTerm(searchTerm))
                .OnSuccess(result => result.Paginate(_paginator, pagination))
                .Execute();

            var postalCodes = string.IsNullOrEmpty(searchTerm)
                ? Mapper.Map<PagedResultDto<PostalCodeDto>>(queryResult.Response)
                : new PostalCodeConverter().Convert(searchTerm, queryResult.Response);

            var response = queryResult.CreateLISTPagedResponse(postalCodes);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllSimplifiedPostalCodes(Pagination pagination, string searchTerm = "")
        {
            var queryResult = _executionPlan
                .Query<GetPostalCodesQuery, PostalCode, PagedResults<PostalCode>>()
                .Configure(q => q.WithSearchTerm(searchTerm))
                .OnSuccess(result => result.Paginate(_paginator, pagination))
                .Execute();

            var postalCodes = string.IsNullOrEmpty(searchTerm)
                ? Mapper.Map<PagedResultDto<MasterTypeDto>>(queryResult.Response)
                : new PostalCodeConverter().ConvertToSimplified(searchTerm, queryResult.Response);

            var response = queryResult.CreateLISTPagedResponse(postalCodes);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        //v2
        private Negotiator SearchSuburbsAndPostalCodes(string search = "")
        {
            var dto = this.Bind<SearchPostCodeDto>();
            dto.Country = this.Request.GetCountry();

            var result = _executionPlan.Execute<SearchPostCodeDto, List<PostalCodeDto>>(dto);

            var response = new GETResponseDto<List<PostalCodeDto>>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode); ;
        }
    }

}