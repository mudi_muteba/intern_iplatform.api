﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Infrastructure;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;
using iPlatform.Api.DTOs.Occupation;
using Domain.Occupations.Queries;
using AutoMapper;
using Domain.Occupations;
using Api.Models;
using SiteMap = Api.Models.SiteMap;


namespace Api.Modules.Lookups.Occupations
{
    public class OccupationLookupModule: SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public OccupationLookupModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Lookups.Occupation.Get.Route] = p => GetOccupation(p.search);


            Options[SystemRoutes.Lookups.Occupation.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Lookups.Occupation));
        }

        public Negotiator GetOccupation(string search = "")
        {


            var criteria = new OccupationSearchDto { Name = search == "NOOCCUPATION" ? string.Empty : search };

            var queryResult = executionPlan
                .Search<SearchOccupationsQuery, Occupation, OccupationSearchDto, List<OccupationDto>, OccupationDto>(criteria)
                .OnSuccess(result =>
                {
                    return Mapper.Map<List<OccupationDto>>(result.ToList()).ToList();
                })
                .Execute();

            var response = queryResult.CreateGETResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }
    }
}