﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Imports.Leads;
using Domain.Imports.Leads.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Excel;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Lookups.Imports.Leads
{
    public class LeadImportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public LeadImportModule(IExecutionPlan executionPlan, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.paginator = paginator;

            Post[SystemRoutes.LeadImport.PostSeriti.Route] = p => ImportSeritiLead();

            Post[SystemRoutes.LeadImport.PostExcel.Route] = p => ImportExcelLeads();

            Get[SystemRoutes.LeadImport.Get.Route] = p => GetImportedLeadByReference(p.id);

            Get[SystemRoutes.LeadImport.GetAll.Route] = p => GetAllImportedLead(new Pagination());

            Post[SystemRoutes.LeadImport.SubmitLead.Route] = p => SubmitLead();

            Get[SystemRoutes.LeadImport.GetWithPagination.Route] =
                p => GetAllImportedLead(new Pagination(p.PageNumber, p.PageSize));

            Options[SystemRoutes.LeadImport.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.LeadImport));
        }

        #region Excel Import
        private Negotiator ImportExcelLeads()
        {
            var importLeadDto = GetImportLeadDto(Request);

            HandlerResult<Guid> result = executionPlan.Execute<LeadImportExcelDto, Guid>(importLeadDto);

            POSTResponseDto<Guid> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.LeadImport.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


        public LeadImportExcelDto GetImportLeadDto(Request request)
        {
            var importLeadDto = this.Bind<LeadImportExcelDto>();

            if (importLeadDto.File != null && !string.IsNullOrEmpty(importLeadDto.FileName))
                return importLeadDto;

            if (request.Headers == null)
                return null;

            var content = request.Files.FirstOrDefault();
            if (content == null) return null;

            if (content.ContentType.Contains("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
            {
                //Set filename
                importLeadDto.FileName = content.Name;

                //Set file as byte
                string xlsx = string.Empty;
                var result = new byte[content.Value.Length];
                using (var binaryReader = new BinaryReader(content.Value))
                {
                    result = binaryReader.ReadBytes((int)content.Value.Length);
                    importLeadDto.Leads = BuildExcel(result);
                    return importLeadDto.AddFile(result);
                }
            }

            return null;
        }

        public static List<LeadImportExcelRowDto> BuildExcel(Byte[] file)
        {
            List<LeadImportExcelRowDto> Excel = new List<LeadImportExcelRowDto>();
            MemoryStream memoryStream = new MemoryStream(file);
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(memoryStream, true))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                SheetData sheetData = spreadsheetDocument.WorkbookPart.WorksheetParts.FirstOrDefault()
                    .Worksheet.GetFirstChild<SheetData>();

                OpenXmlReader reader = OpenXmlReader.Create(sheetData);
                String rowNum = string.Empty;
                while (reader.Read())
                {
                    if (reader.ElementType == typeof(Row))
                    {
                        LeadImportExcelRowDto ExcelRow = new LeadImportExcelRowDto();

                        if (reader.HasAttributes)
                            rowNum = reader.Attributes.First(a => a.LocalName == "r").Value;

                        reader.ReadFirstChild();
                        do
                        {
                            if (reader.ElementType == typeof(Cell))
                            {
                                Cell cell = (Cell)reader.LoadCurrentElement();
                                string cellValue;
                                if (cell.DataType != null && cell.DataType == CellValues.SharedString)
                                    cellValue = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>()
                                        .ElementAt(int.Parse(cell.CellValue.InnerText)).Text.Text;
                                else
                                    cellValue = cell.CellValue.InnerText;

                                //add to row
                                ExcelRow.Cell.Add(cellValue);
                            }

                        } while (reader.ReadNextSibling());

                        if (rowNum != "1")
                            Excel.Add(ExcelRow);
                    }
                }
            }

            return Excel;
        }

        public Negotiator GetImportedLeadByReference(Guid reference)
        {
            var queryResult = executionPlan.Query<GetLeadImportQuery, LeadImport, List<ListLeadImportDto>>()
                .Configure(c => c.ByReference(reference))
                .OnSuccess(results =>
                {
                    return results.ToList().Select(s => Mapper.Map<LeadImport, ListLeadImportDto>(s)).ToList();
                })
                .Execute();

            var result = new ListResultDto<ListLeadImportDto> { Results = queryResult.Response };

            LISTResponseDto<ListLeadImportDto> response = queryResult.CreateLISTResponse(result)
                .CreateLinks(c => SystemRoutes.LeadImport.CreateLinks(c));


            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetAllImportedLead(Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetLeadImportQuery, LeadImport, PagedResults<LeadImport>>()
                .OnSuccess(result => result.Paginate(paginator, pagination))
                .Execute();

            var listLeadImportDto = Mapper.Map<PagedResultDto<ListLeadImportDto>>(queryResult.Response);

            var response = queryResult.CreateLISTPagedResponse(listLeadImportDto)
                .CreateLinks(c => SystemRoutes.LeadImport.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        #endregion
        #region Seriti Import
        private Negotiator ImportSeritiLead()
        {
            var leadImportXMLDto = this.Bind<LeadImportXMLDto>();

            if(string.IsNullOrEmpty(leadImportXMLDto.XMLString))
                leadImportXMLDto.XMLString = GetLeadImportDto(this.Request);

            var result = executionPlan.Execute<LeadImportXMLDto, Guid>(leadImportXMLDto);

            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode)
                ;
        }

        public static string GetLeadImportDto(Request request)
        {
            RequestHeaders headers = request.Headers;

            if (headers == null)
                return string.Empty;

            var content = request.Files.FirstOrDefault();

            if (content == null) return string.Empty;

            if (content.ContentType.Contains("xml"))
            {
                using (var binaryReader = new BinaryReader(content.Value))
                {
                    var result = binaryReader.ReadBytes((int)content.Value.Length);
                    return System.Text.Encoding.ASCII.GetString(result).Replace("???", "");
                }
            }

            return string.Empty;
        }

        public Negotiator SubmitLead()
        {
            var submitLeadDto = this.Bind<SubmitLeadDto>();

            var result = executionPlan.Execute<SubmitLeadDto, string>(submitLeadDto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        #endregion
    }
}