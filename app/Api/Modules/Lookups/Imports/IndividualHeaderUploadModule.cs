﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Imports.Leads;
using Domain.Imports.Leads.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Lookups.Imports
{
    public class IndividualHeaderUploadModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public IndividualHeaderUploadModule(IExecutionPlan executionPlan, IPaginateResults paginator)
        {
            _executionPlan = executionPlan;
            _paginator = paginator;

            Post[SystemRoutes.IndividualHeaderUpload.Create.Route] = _ => Create();
            Put[SystemRoutes.IndividualHeaderUpload.Edit.Route] = _ => Edit();
            Get[SystemRoutes.IndividualHeaderUpload.GetAllWithPagination.Route] = p => GetAll(new Pagination());

        }

        private Negotiator Create()
        {
            var model = this.Bind<CreateIndividualUploadHeaderDto>();
            var result = _executionPlan.Execute<CreateIndividualUploadHeaderDto, int>(model);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.IndividualHeaderUpload.CreateGetById(result.Response));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Edit()
        {
            var model = this.Bind<EditIndividualUploadHeaderDto>();
            var result = _executionPlan.Execute<EditIndividualUploadHeaderDto, int>(model);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.IndividualHeaderUpload.CreateGetById(result.Response));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetAll(Pagination pagination)
        {
            var queryResult = _executionPlan
                .Query<GetIndividualUploadHeaderQuery, IndividualUploadHeader, PagedResults<IndividualUploadHeader>>()
                .OnSuccess(result => result.Paginate(_paginator, pagination))
                .Execute();

            var transformedResponse = Mapper.Map<PagedResultDto<CreateIndividualUploadHeaderDto>>(queryResult.Response);

            var response = queryResult.CreateLISTPagedResponse(transformedResponse).CreateLinks(c => SystemRoutes.IndividualHeaderUpload.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}