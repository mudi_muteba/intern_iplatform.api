﻿using System;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Lookups.Imports.Individuals
{
    public class IndividualImportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public IndividualImportModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Post[SystemRoutes.IndividualImport.Post.Route] = p => ImportIndividual();
        }


        private Negotiator ImportIndividual()
        {
            var importIndividualDto = this.Bind<ImportIndividualDto>();

            HandlerResult<Guid> result = executionPlan.Execute<ImportIndividualDto, Guid>(importIndividualDto);

            POSTResponseDto<Guid> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualImport.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}