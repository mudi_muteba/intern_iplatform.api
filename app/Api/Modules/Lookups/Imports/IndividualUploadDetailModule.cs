﻿using System;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Imports.Leads;
using Domain.Imports.Leads.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Linq;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Api.Modules.Lookups.Imports
{
    public class IndividualUploadDetailModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public IndividualUploadDetailModule(IExecutionPlan executionPlan, IPaginateResults paginator)
        {
            _executionPlan = executionPlan;
            _paginator = paginator;

            Post[SystemRoutes.IndividualUploadDetail.Create.Route] = _ => Create();
            Get[SystemRoutes.IndividualUploadDetail.GetAllWithPagination.Route] = p => GetAll(new Pagination(), p.leadImportReference);
            Put[SystemRoutes.IndividualUploadDetail.Edit.Route] = p => Edit(p.leadImportReference);
            Get[SystemRoutes.IndividualUploadDetail.GetByLeadImportReference.Route] = p => GetByLeadImportReference(p.leadImportReference);
        }

        private Negotiator Create()
        {
            var model = this.Bind<IndividualUploadDetailDto>();
            var result = _executionPlan.Execute<IndividualUploadDetailDto, int>(model);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.LeadImport.CreateGetById(result.Response));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetAll(Pagination pagination, Guid reference)
        {
            var queryResult = _executionPlan
                .Query<GetIndividualUploadDetailQuery, IndividualUploadDetail, PagedResults<IndividualUploadDetail>>()
                .Configure(c => c.WithCriteria(reference))
                .OnSuccess(result => result.Paginate(_paginator, pagination))

                .Execute();

            var transformedResponse = Mapper.Map<PagedResultDto<IndividualUploadDetailDto>>(queryResult.Response);

            var response = queryResult.CreateLISTPagedResponse(transformedResponse).CreateLinks(c => SystemRoutes.IndividualUploadDetail.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Edit(Guid leadImportReference)
        {
            var editIndividualUploadDetailDto = this.Bind<EditIndividualUploadDetailDto>();

            var result = _executionPlan.Execute<EditIndividualUploadDetailDto, Guid>(editIndividualUploadDetailDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.LeadImport.CreateGetById(result.Response));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetByLeadImportReference(Guid leadImportReference)
        {
            QueryResult<IndividualUploadDetailDto> queryResult = _executionPlan
                .Query<GetIndividualUploadDetailQuery, IndividualUploadDetail, IndividualUploadDetailDto, IndividualUploadDetailDto>()
                .Configure(c => c.WithCriteria(leadImportReference))
                .OnSuccess(r => Mapper.Map<IndividualUploadDetail, IndividualUploadDetailDto>(r.FirstOrDefault()))
                .Execute();

            GETResponseDto<IndividualUploadDetailDto> response = queryResult.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.IndividualUploadDetail.CreateGetByLeadImportReference(c.LeadImportReference));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}