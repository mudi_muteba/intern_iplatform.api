﻿using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.VehicleGuide.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Lookups.VehicleGuide.VehicleGuideSetting;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.VehicleGuide.VehicleGuideSetting
{
    public class VehicleGuideSettingsModule : SecureModule
    {
        private readonly IExecutionPlan _ExecutionPlan;
        private readonly IPaginateResults _Paginator;

        public VehicleGuideSettingsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _Paginator = paginator;
            _ExecutionPlan = executionPlan;

            Get[SystemRoutes.VehicleGuideSetting.Get.Route] = _ => GetAllVehicleGuideSettings(new Pagination());
            Get[SystemRoutes.VehicleGuideSetting.GetWithPagination.Route] = p => GetAllVehicleGuideSettings(new Pagination(p.pageNumber, p.pageSize));
            Post[SystemRoutes.VehicleGuideSetting.Post.Route] = p => Create();
            Post[SystemRoutes.VehicleGuideSetting.PostMultiple.Route] = p => SaveMultiple();
            Get[SystemRoutes.VehicleGuideSetting.GetNoPagination.Route] = p => GetAllNoPagination();
            Post[SystemRoutes.VehicleGuideSetting.Search.Route] = _ => Search();
        }

        public Negotiator SaveMultiple()
        {
            var saveMultipleSettingsiRateDto = this.Bind<SaveMultipleVehicleGuideSettingDto>();

            var result = _ExecutionPlan.Execute<SaveMultipleVehicleGuideSettingDto, bool>(saveMultipleSettingsiRateDto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<SearchVehicleGuideSettingDto>();

            var queryResult = _ExecutionPlan
                .Search<SearchVehicleGuideSettingsQuery, Domain.Lookups.VehicleGuide.VehicleGuideSetting, SearchVehicleGuideSettingDto, PagedResultDto<VehicleGuideSettingDto>>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<VehicleGuideSettingDto>>(
                    result.Paginate(_Paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.VehicleGuideSetting.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator Create()
        {
            var createVehicleGuideSettingDto = this.Bind<CreateVehicleGuideSettingDto>();

            var result = _ExecutionPlan.Execute<CreateVehicleGuideSettingDto, int>(createVehicleGuideSettingDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.VehicleGuideSetting.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllVehicleGuideSettings(Pagination pagination)
        {
            var queryResult = _ExecutionPlan
                .Query<GetAllVehicleGuideSettingsQuery, Domain.Lookups.VehicleGuide.VehicleGuideSetting, PagedResultDto<ListVehicleGuideSettingDto>, ListVehicleGuideSettingDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListVehicleGuideSettingDto>>(result.Paginate(_Paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.VehicleGuideSetting.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = _ExecutionPlan
                .Query<GetAllVehicleGuideSettingsQuery, Domain.Lookups.VehicleGuide.VehicleGuideSetting, ListResultDto<ListVehicleGuideSettingDto>>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListVehicleGuideSettingDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.VehicleGuideSetting.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}