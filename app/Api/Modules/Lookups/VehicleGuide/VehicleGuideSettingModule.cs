﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Raven.Client.Linq.Indexing;

namespace Api.Modules.VehicleGuide.VehicleGuideSetting
{
    public class VehicleGuideSettingModule : SecureModule
    {
        private readonly IExecutionPlan _ExecutionPlan;
        private readonly IRepository _Repository;

        public VehicleGuideSettingModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _ExecutionPlan = executionPlan;
            _Repository = repository;

            Get[SystemRoutes.VehicleGuideSetting.GetById.Route] = p => GetVehicleGuideSetting(p.Id);
            Put[SystemRoutes.VehicleGuideSetting.Put.Route] = p => UpdateVehicleGuidSetting(p.Id);
            Put[SystemRoutes.VehicleGuideSetting.Delete.Route] = p => DeleteVehicleGuidSetting(p.Id);
        }

        private Negotiator GetVehicleGuideSetting(int id)
        {
            var result = _ExecutionPlan.GetById<Domain.Lookups.VehicleGuide.VehicleGuideSetting, VehicleGuideSettingDto>(
                    () => _Repository.GetById<Domain.Lookups.VehicleGuide.VehicleGuideSetting>(id))
                    .OnSuccess(Mapper.Map<Domain.Lookups.VehicleGuide.VehicleGuideSetting, VehicleGuideSettingDto>)
                    .Execute();

            var response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.VehicleGuideSetting.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateVehicleGuidSetting(int id)
        {
            var editVehicleGuideSettingDto = this.Bind<EditVehicleGuideSettingDto>();
            editVehicleGuideSettingDto.Id = id;

            var result = _ExecutionPlan.Execute<EditVehicleGuideSettingDto, int>(editVehicleGuideSettingDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.VehicleGuideSetting.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteVehicleGuidSetting(int id)
        {
            var disableVehicleGuideSettingDto = this.Bind<DisableVehicleGuideSettingDto>();
            disableVehicleGuideSettingDto.Id = id;

            var result = _ExecutionPlan.Execute<DisableVehicleGuideSettingDto, int>(disableVehicleGuideSettingDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.VehicleGuideSetting.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}