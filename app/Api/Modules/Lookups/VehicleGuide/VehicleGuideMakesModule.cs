﻿using System;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Lookups.VehicleGuide.Queries;
using iGuide.DTOs.Makes;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Lookups.VehicleGuide
{
    public class VehicleGuideMakesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public VehicleGuideMakesModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Lookups.VehicleGuide.Makes.GetAll.Route] = _ => GetMakes();
            Get[SystemRoutes.Lookups.VehicleGuide.Makes.ByYear.Route] = p => GetMakes(int.Parse(p.year));
            Get[SystemRoutes.Lookups.VehicleGuide.Makes.BySearch.Route] = p => GetMakes(p.make.ToString());
            Get[SystemRoutes.Lookups.VehicleGuide.Makes.BySearchAndYear.Route] = p => GetMakes(p.make.ToString(), int.Parse(p.year));

            Options[SystemRoutes.Lookups.VehicleGuide.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Lookups.VehicleGuide));
        }

        private Negotiator GetMakes()
        {
            return ExecuteQuery(q => { });
        }

        private Negotiator GetMakes(int year)
        {
            return ExecuteQuery(q => { q.ForYear(year); });
        }

        private Negotiator GetMakes(string search)
        {
            return ExecuteQuery(q => { q.SearchBy(search); });
        }

        private Negotiator GetMakes(string search, int year)
        {
            return ExecuteQuery(q => { q.SearchBy(search).ForYear(year); });
        }

        private Negotiator ExecuteQuery(Action<VehicleMakesQuery> configure)
        {
            var queryResult = executionPlan
                .ExternalQuery<VehicleMakesQuery, MakeSearchResultsDto, MakeSearchResultsDto>()
                .Configure(configure)
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}