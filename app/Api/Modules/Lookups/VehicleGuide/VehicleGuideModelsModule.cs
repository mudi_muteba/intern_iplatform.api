using System;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Lookups.VehicleGuide.Queries;
using iGuide.DTOs.Models;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Lookups.VehicleGuide
{
    public class VehicleGuideModelsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public VehicleGuideModelsModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Lookups.VehicleGuide.Models.GetByMake.Route] = p
                => GetModels(p.make.ToString());

            Get[SystemRoutes.Lookups.VehicleGuide.Models.GetByMakeAndYear.Route] = p
                => GetModels(p.make.ToString(), int.Parse(p.year));

            Get[SystemRoutes.Lookups.VehicleGuide.Models.SearchByMake.Route] = p
                => GetModels(p.make.ToString(), p.model.ToString());

            Get[SystemRoutes.Lookups.VehicleGuide.Models.SearchByMakeAndYear.Route] = p
                => GetModels(int.Parse(p.year), p.make.ToString(), p.model.ToString());

            Options[SystemRoutes.Lookups.VehicleGuide.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Lookups.VehicleGuide));
        }

        private Negotiator GetModels(int year, string make, string search)
        {
            return ExecuteQuery(q => { q.ForMake(make).ForYear(year).SearchBy(search); });
        }

        private Negotiator GetModels(string make, int year)
        {
            return ExecuteQuery(q => { q.ForMake(make).ForYear(year); });
        }

        private Negotiator GetModels(string make, string search)
        {
            return ExecuteQuery(q => { q.ForMake(make).SearchBy(search); });
        }

        private Negotiator GetModels(string make)
        {
            return ExecuteQuery(q => { q.ForMake(make); });
        }

        private Negotiator ExecuteQuery(Action<VehicleModelsQuery> configure)
        {
            var queryResult = executionPlan
                .ExternalQuery<VehicleModelsQuery, ModelSearchResultsDto, ModelSearchResultsDto>()
                .Configure(configure)
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}