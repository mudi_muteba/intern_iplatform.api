using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Lookups.VehicleGuide.Queries;
using iGuide.DTOs.Details;
using iGuide.DTOs.Specifications;
using iGuide.DTOs.Values;
using iGuide.DTOs.Years;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Lookups.VehicleGuide
{
    public class VehicleGuideVehiclesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public VehicleGuideVehiclesModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Lookups.VehicleGuide.Vehicles.GetValues.Route] = p
                => GetVehicleValue(p.mmCode, p.year);

            Get[SystemRoutes.Lookups.VehicleGuide.Vehicles.GetAbout.Route] = p
                => GetVehicleAbout(p.mmCode, p.year);

            Get[SystemRoutes.Lookups.VehicleGuide.Vehicles.GetExtras.Route] = p
                => GetVehicleExtras(p.mmCode, p.year);

            Get[SystemRoutes.Lookups.VehicleGuide.Vehicles.GetYears.Route] = p
                => GetVehicleYears(p.mmCode);

            Get[SystemRoutes.Lookups.VehicleGuide.Vehicles.GetByRegistrationNumber.Route] = p
                => GetVehicleRegistrationNumber(p.registrationNumber);

            Get[SystemRoutes.Lookups.VehicleGuide.Vehicles.GetByLicensePlateNumber.Route] = p
                => GetVehicleLicensePlate(p.licensePlateNumber);

            Get[SystemRoutes.Lookups.VehicleGuide.Vehicles.GetByLicensePlateNumberAndIdNumber.Route] = p
                => GetVehicleLicensePlateAndIdNumber(p.licensePlateNumber, p.idNumber);

            Options[SystemRoutes.Lookups.VehicleGuide.Options.Route] =
                   _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Lookups.VehicleGuide));
        }

        private Negotiator GetVehicleYears(string mmCode)
        {
            var queryResult = executionPlan
                .ExternalQuery<VehicleYearsQuery, VehicleYearsDto, VehicleYearsDto>()
                .Configure(q => { q.ForMMCode(mmCode); })
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetVehicleValue(string mmCode, int year)
        {
            var queryResult = executionPlan
                .ExternalQuery<VehicleValueQuery, VehicleValuesDto, VehicleValuesDto>()
                .Configure(q => { q.ForYear(year).ForMMCode(mmCode); })
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetVehicleExtras(string mmCode, int year)
        {
            var queryResult = executionPlan
                .ExternalQuery<VehicleExtrasQuery, VehiclesOptionalExtrasDto, VehiclesOptionalExtrasDto>()
                .Configure(q => { q.ForYear(year).ForMMCode(mmCode); })
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetVehicleAbout(string mmCode, int year)
        {
            var queryResult = executionPlan
                .ExternalQuery<VehicleAboutQuery, VehicleDetailsDto, VehicleDetailsDto>()
                .Configure(q => { q.ForYear(year).ForMMCode(mmCode); })
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetVehicleLicensePlate(string licensePlateNumber)
        {
            var queryResult = executionPlan
                .ExternalQuery<VehicleSpecificationsQuery, VehicleSpecsResultsDto, VehicleSpecsResultsDto>()
                .Configure(q => { q.ForLicensePlate(licensePlateNumber); })
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetVehicleLicensePlateAndIdNumber(string licensePlateNumber, string idNumber)
        {
            var queryResult = executionPlan
                .ExternalQuery<VehicleSpecificationsQuery, VehicleSpecsResultsDto, VehicleSpecsResultsDto>()
                .Configure(q => { q.ForLicensePlateAndIdNumber(licensePlateNumber, idNumber); })
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetVehicleRegistrationNumber(string registrationNumber)
        {
            var queryResult = executionPlan
                .ExternalQuery<VehicleSpecificationsQuery, VehicleSpecsResultsDto, VehicleSpecsResultsDto>()
                .Configure(q => { q.ForRegistrationNumber(registrationNumber); })
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}