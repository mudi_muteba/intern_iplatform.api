﻿using System.Collections.Generic;
using System.Linq;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.Person;
using Domain.Lookups.Person.CompanyLookup;
using Domain.Lookups.Person.Queries;
using iPerson.Api.DTOs.CompanyInformation.ResponseDtos;
using iPerson.Api.DTOs.Connector;
using iPerson.Api.DTOs.PersonInformation;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Lookups.Person;
using iPlatform.Api.DTOs.Products;

namespace Api.Modules.Lookups.Person
{
    public class CompanyLookupModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPersonConnector connector;


        public CompanyLookupModule(IExecutionPlan executionPlan, IPersonConnector connector)
        {
            this.executionPlan = executionPlan;
            this.connector = connector;

            Get[SystemRoutes.Lookups.Company.Get.Route] = _ => GetCompany();
            Get[SystemRoutes.Lookups.Company.Search.Route] = _ => SearchCompanies();
            Options[SystemRoutes.Lookups.Person.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Lookups.Company));
        }

        private Negotiator GetCompany()
        {
            var request = this.Bind<PersonLookupRequest>();

            var queryResult = executionPlan
               .ExternalQuery<CompanyLookupSearchQuery<GetCompanyInformationDto>, CompanyInformationSearchResultCollection<GetCompanyInformationDto>, CompanyInformationSearchResultCollection<GetCompanyInformationDto>>()
               .Configure(q => q.WithRequest(request, (s, context) => connector.CompanyInformation.GetByCompanyNumber(context, s.Trim())))
               .OnSuccess(result => result)
               .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchCompanies()
        {
            var request = this.Bind<PersonLookupRequest>();

            var queryResult = executionPlan
                .ExternalQuery<CompanyLookupSearchQuery<CompanySearchResultDto>, CompanyInformationSearchResultCollection<CompanySearchResultDto>, CompanyInformationSearchResultCollection<CompanySearchResultDto>>()
                .Configure(q => q.WithRequest(request, (s, context) => connector.CompanyInformation.Search(context, s.Trim())))
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}