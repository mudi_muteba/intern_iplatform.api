﻿using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;
using iPlatform.Api.DTOs.Lookups.Address;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Api.Modules.Lookups.Address
{
    public class AddressLookupModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public AddressLookupModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            this._executionPlan = executionPlan;
            this._paginator = paginator;

            Get[SystemRoutes.Lookups.Address.GetProvinceByPostCode.Route] = p => GetProvinceByPostcodeAndCountry(p.postCode);
            Get[SystemRoutes.Lookups.Address.GetProvinceByPostCodeAndCountryCode.Route] = p => GetProvinceByPostcodeAndCountry(p.postCode, p.countryCode);

            Get[SystemRoutes.Lookups.Address.GetProvinceBySuburb.Route] = p => GetProvinceBySuburbAndCountry(p.postCode);
            Get[SystemRoutes.Lookups.Address.GetProvinceBySuburbAndCountryCode.Route] = p => GetProvinceBySuburbAndCountry(p.postCode, p.countryCode);

            Options[SystemRoutes.Lookups.Address.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Lookups.Address));
        }

        private Negotiator GetProvinceByPostcodeAndCountry(string postCode, string country = "ZA")
        {
            var dto = new GetProvinceByPostCodeDto(postCode, country);

            var result = _executionPlan.Execute<GetProvinceByPostCodeDto, AddressLookupDto>(dto);

            var response = new GETResponseDto<AddressLookupDto>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetProvinceBySuburbAndCountry(string suburb, string country = "ZA")
        {
            var dto = new GetProvinceBySuburbDto(suburb, country);

            var result = _executionPlan.Execute<GetProvinceBySuburbDto, AddressLookupDto>(dto);

            var response = new GETResponseDto<AddressLookupDto>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}