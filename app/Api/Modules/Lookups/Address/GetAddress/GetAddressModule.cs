﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iGuide.DTOs.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Lookups.Address.GetAddress;
using iPlatform.Api.DTOs.Party.Address;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Lookups.Address.GetAddress
{
    public class GetAddressModule : SecureModule
    {
        private readonly IPaginateResults _PaginateResults;
        private readonly IExecutionPlan _ExecutionPLan;

        public GetAddressModule(IPaginateResults paginateResults, IExecutionPlan executionPlan)
        {
            _PaginateResults = paginateResults;
            _ExecutionPLan = executionPlan;

            Get[SystemRoutes.GetAddress.GetAddressesByPostCode.Route] = g => GetAddressByPostCode(g.PostCode);
        }

        private Negotiator GetAddressByPostCode(string postCode)
        {
            GetAddressByPostCodeDto getAddressByPostCodeDtodto = new GetAddressByPostCodeDto(postCode);
            HandlerResult<List<ListAddressDto>> result = _ExecutionPLan.Execute<GetAddressByPostCodeDto, List<ListAddressDto>>(getAddressByPostCodeDtodto);
            GETResponseDto<List<ListAddressDto>> response = new GETResponseDto<List<ListAddressDto>>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}