﻿using System.Collections.Generic;
using System.Linq;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.Person;
using Domain.Lookups.Person.Queries;

using iPerson.Api.DTOs.PersonInformation;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Lookups.Person;
using iPlatform.Api.DTOs.Products;

namespace Api.Modules.Lookups.Person
{
    public class PersonLookupModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public PersonLookupModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Lookups.Person.Get.Route] = _ => GetPerson();
            Get[SystemRoutes.Lookups.Person.GetBySource.Route] = _ => GetPerson();
            Get[SystemRoutes.Lookups.Person.GetByIdNumber.Route] = _ => GetPerson();
            Get[SystemRoutes.Lookups.Person.GetByPhoneNumber.Route] = _ => GetPerson();
            Get[SystemRoutes.Lookups.Person.GetByEmail.Route] = _ => GetPerson();
            Get[SystemRoutes.Lookups.Person.GetPersonLeadById.Route] = _ => GetPersonLead();
            Get[SystemRoutes.Lookups.Person.GetByPropertyId.Route] = _ => GetPropertyValuation();

            Options[SystemRoutes.Lookups.Person.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Lookups.Person));
        }

        private Negotiator GetPerson()
        {
            var request = this.Bind<PersonLookupRequest>();

            var queryResult = executionPlan
                .ExternalQuery<PersonLookupQuery, PersonInformationResponsesDto, PersonInformationResponsesDto>()
                .Configure(q => q.WithRequest(request))
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        
        private Negotiator GetPersonLead()
        {
            var request = this.Bind<PersonLookupRequest>();

            var queryResult = executionPlan
                .ExternalQuery<PersonLeadLookupQuery, IndividualDto, IndividualDto>()
                .Configure(q => q.WithRequest(request))
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetPropertyValuation()
        {
            var request = this.Bind<PersonLookupRequest>();
            request.Source = "property";
            var queryResult = executionPlan
                 .ExternalQuery<PersonLookupQuery, PersonInformationResponsesDto, PersonInformationResponsesDto>()
                .Configure(q => q.WithRequest(request))
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}