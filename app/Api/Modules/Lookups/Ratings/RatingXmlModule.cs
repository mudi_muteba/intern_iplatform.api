﻿using System;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Lookups.Ratings;
using Domain.Lookups.VehicleGuide.Queries;
using iGuide.DTOs.Makes;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups;
using iPlatform.Api.DTOs.Lookups.Ratings;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Lookups.Ratings
{
    public class RatingXmlModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public RatingXmlModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;


            Get[SystemRoutes.Lookups.Ratings.GetRequest.Route] = p => GetRequest(int.Parse(p.quoteId));
            Get[SystemRoutes.Lookups.Ratings.GetResponse.Route] = p => GetResponse(int.Parse(p.quoteId));

            Options[SystemRoutes.Lookups.Ratings.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Lookups.Ratings));
        }

        private Negotiator GetRequest(int quoteId)
        {
            var criteria = new RatingSearchDto()
            {
                QuoteId = quoteId,
                GetRatingRequest = true
            };
            return ExecuteQuery(q => { q.WithCriteria(criteria); });
        }

        private Negotiator GetResponse(int quoteId)
        {
            var criteria = new RatingSearchDto()
            {
                QuoteId = quoteId,
                GetRatingRequest = false
            };
            return ExecuteQuery(q => { q.WithCriteria(criteria); });
        }

        private Negotiator ExecuteQuery(Action<RatingsXmlQuery> configure)
        {
            var queryResult = executionPlan
                .ExternalQuery<RatingsXmlQuery, string, string>()
                .Configure(configure)
                .OnSuccess(result => result)
                .Execute();

            var response = queryResult.CreateGetResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}