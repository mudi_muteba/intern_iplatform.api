﻿using AutoMapper;
using Nancy;

using Api.Infrastructure;

using Domain.Banks;
using Domain.Banks.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.IO;

namespace Api.Modules.Lookups.Banks
{
    public class BanksModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IPaginateResults m_Paginator;
        private readonly IRootPathProvider m_RootPathProvider;

        public BanksModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRootPathProvider rootPathProvider)
        {
            this.m_Paginator = paginator;
            this.m_ExecutionPlan = executionPlan;
            this.m_RootPathProvider = rootPathProvider;

            Get[SystemRoutes.Bank.Get.Route] = p => GetAllBanks(new Pagination());

            Get[SystemRoutes.Bank.GetWithPagination.Route] =
                p => GetAllBanks(new Pagination(p.PageNumber, p.PageSize));

            Post[SystemRoutes.Bank.PostSearch.Route] = p => SearchBanks();

            Post[SystemRoutes.Bank.ValidateBankDetails.Route] = p => ValidateBankDetails();
            Post[SystemRoutes.Bank.ValidateBankDetailsLegacy.Route] = p => ValidateBankDetailsLegacy();
        }

        public Negotiator SearchBanks()
        {
            var criteria = this.Bind<BankSearchDto>();

            var queryResult = m_ExecutionPlan
                .Search<SearchBanksQuery, Bank, BankSearchDto, PagedResults<Bank>>(criteria)
                .OnSuccess(result => result.Paginate(m_Paginator, criteria.CreatePagination()))
                .Execute();

            var bankDtos = Mapper.Map<PagedResultDto<ListBankDto>>(queryResult.Response);

            var response = queryResult.CreateLISTPagedResponse(bankDtos)
                .CreateLinks(c => SystemRoutes.Bank.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetAllBanks(Pagination pagination)
        {
            var queryResult = m_ExecutionPlan
                .Query<GetAllBanksQuery, Bank, PagedResults<Bank>>()
                .OnSuccess(result => result.Paginate(m_Paginator, pagination))
                .Execute();

            var bankDtos = Mapper.Map<PagedResultDto<ListBankDto>>(queryResult.Response);

            var response = queryResult.CreateLISTPagedResponse(bankDtos)
                .CreateLinks(c => SystemRoutes.Bank.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator ValidateBankDetails()
        {
            var criteria = this.Bind<BankDetailValidationDto>();
                criteria.LookupPath = Path.Combine(m_RootPathProvider.GetRootPath(), "content/lookups/banks.csv");
            
            //criteria.LookupPath = string.Format("{0}/content/lookups/banks.csv", Request.Url.SiteBase);
            //criteria.LookupPath = HttpContext.Current.Server.MapPath("~/content/lookups/banks.csv");

            var result = m_ExecutionPlan.Execute<BankDetailValidationDto, bool>(criteria);
            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.Bank.ValidateBankDetails.Route);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        public Negotiator ValidateBankDetailsLegacy()
        {
            var criteria = this.Bind<BankDetailValidationLegacyDto>();
            criteria.LookupPath = Path.Combine(m_RootPathProvider.GetRootPath(), "content/lookups/banks.csv");

            var result = m_ExecutionPlan.Execute<BankDetailValidationLegacyDto, bool>(criteria);
            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.Bank.ValidateBankDetails.Route);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}