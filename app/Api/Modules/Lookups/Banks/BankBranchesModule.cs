﻿using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Domain.Banks;
using Domain.Banks.Queries;
using AutoMapper;

namespace Api.Modules.Lookups.Banks
{
    public class BankBranchesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public BankBranchesModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;


            Post[SystemRoutes.BankBranch.PostSearch.Route] = p => SearchBanks();
        }

        public Negotiator SearchBanks()
        {
            var criteria = this.Bind<BankBranchesSearchDto>();

            var queryResult = executionPlan
                .Search<SearchBankBranchesQuery, BankBranch, BankBranchesSearchDto, PagedResults<BankBranch>>(criteria)
                .OnSuccess(result => result.Paginate(paginator, criteria.CreatePagination()))
                .Execute();

            var campaignDtos = Mapper.Map<PagedResultDto<ListBankBranchesDto>>(queryResult.Response);

            var response = queryResult.CreateLISTPagedResponse(campaignDtos)
                .CreateLinks(c => SystemRoutes.BankBranch.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}