﻿using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Quotes;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.QuoteItem
{
    public class QuoteItemsModule : SecureModule
    {
        private readonly IPaginateResults Paginator;
        private IExecutionPlan ExecutionPlan;

        public QuoteItemsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            Paginator = paginator;
            ExecutionPlan = executionPlan;


            Put[SystemRoutes.QuoteItems.Put.Route] = p => UpdateQuoteItems(p.UserId);
        }

        #region [ Negotiators ]

        private Negotiator UpdateQuoteItems(int userId)
        {
            ListEditQuoteItemDto listEditQuoteItemDto = this.Bind<ListEditQuoteItemDto>();
            HandlerResult<QuoteDiscountResultDto> result = ExecutionPlan.Execute<ListEditQuoteItemDto, QuoteDiscountResultDto>(listEditQuoteItemDto);
            PUTResponseDto<QuoteDiscountResultDto> response = result.CreatePUTResponse().CreateLinks(SystemRoutes.QuoteItems.CreateGetById(1));

            return Negotiate.WithLocationHeader(response.Link)
                    .ForSupportedFormats(response, response.StatusCode);
        }

        #endregion
    }
}