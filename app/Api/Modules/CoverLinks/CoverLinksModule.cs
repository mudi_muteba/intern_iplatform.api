﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.CoverLinks;
using Domain.CoverLinks.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.CoverLinks;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.CoverLinks
{
    public class CoverLinksModule : NancyModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;
        private readonly IPaginateResults _paginator;

        public CoverLinksModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            _paginator = paginator;

            Get[SystemRoutes.CoverLinks.GetByChannelId.Route] =p => GetCoverLink(p.channelId);

            //Delete[SystemRoutes.CoverLinks.Delete.Route] = p => DeleteCoverLinks(p.Id);

            //Put[SystemRoutes.CoverLinks.Put.Route] = p => EditCoverLinks(p.Id);

            Options[SystemRoutes.CoverLinks.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.CoverLinks));


            Get[SystemRoutes.CoverLinks.Get.Route] = _ => GetAll(new Pagination());
            Get[SystemRoutes.CoverLinks.GetNoPagination.Route] = _ => GetAllNoPagination();
            Get[SystemRoutes.CoverLinks.GetWithPagination.Route] = p => GetAll(new Pagination(p.PageNumber, p.PageSize));
            Post[SystemRoutes.CoverLinks.Search.Route] = _ => Search();
            Post[SystemRoutes.CoverLinks.Post.Route] = p => CreateCoverLink();
        }

        private Negotiator GetCoverLink(int channelId)
        {
            var result = executionPlan
            .Query<GetCoverLinksByChannel, CoverLink, List<ListCoverLinkDto>>()
            .OnSuccess(links => Mapper.Map< List<CoverLink>, List<ListCoverLinkDto>>(links.ToList()))
            .Configure(q => q.WithId(channelId)).Execute();

            var res = new ListResultDto<ListCoverLinkDto> { Results = result.Response };

            var response = result.CreateLISTResponse(res);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<SearchCoverLinksDto>();

            var queryResult = executionPlan
                .Search<SearchCoverLinksQuery, CoverLink, SearchCoverLinksDto, PagedResultDto<ListCoverLinkDto>>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListCoverLinkDto>>(
                    result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.CoverLinks.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAll(Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetAllCoverLinksQuery, CoverLink, PagedResultDto<ListCoverLinkDto>>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListCoverLinkDto>>(result.Paginate(_paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.CoverLinks.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = executionPlan
                .Query<GetAllCoverLinksQuery, CoverLink, ListResultDto<ListCoverLinkDto>>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListCoverLinkDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.CoverLinks.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateCoverLink()
        {
            var createDto = this.Bind<CreateCoverLinkDto>();

            var result = executionPlan.Execute<CreateCoverLinkDto, int>(createDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.CoverLinks.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

    }
}