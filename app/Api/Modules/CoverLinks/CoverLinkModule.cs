﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.CoverLinks;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.CoverLinks;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.CoverLinks
{
    public class CoverLinkModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public CoverLinkModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.CoverLinks.GetById.Route] = p => GetCoverLink(p.id);
            Put[SystemRoutes.CoverLinks.Put.Route] = p => EditCoverLink(p.id);
            Put[SystemRoutes.CoverLinks.Delete.Route] = p => DeleteCoverLink(p.id);
        }

        private Negotiator GetCoverLink(int id)
        {
            var result = _executionPlan
                .GetById<CoverLink, ListCoverLinkDto>(() => _repository.GetById<CoverLink>(id))
                .OnSuccess(Mapper.Map<CoverLink, ListCoverLinkDto>)
                .Execute();

            var response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.CoverLinks.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator EditCoverLink(int id)
        {
            var editDto = this.Bind<EditCoverLinkDto>();

            var result = _executionPlan.Execute<EditCoverLinkDto, int>(editDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.CoverLinks.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteCoverLink(int id)
        {
            var result = _executionPlan.Execute<DeleteCoverLinkDto, int>(new DeleteCoverLinkDto(id));

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.CoverLinks.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}