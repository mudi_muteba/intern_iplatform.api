﻿using System.Collections.Generic;
using System.Linq;

using Api.Infrastructure;
using Api.Models;

using AutoMapper;

using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Leads;
using Domain.Leads.Builder;

using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Leads.Quality;

using Domain.Leads.Queries;
using Domain.Party.Contacts;
using Domain.Party.RegisteredOwnerContacts;
using Domain.Party.Relationships;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Campaign;
using iPlatform.Api.DTOs.Party.Contacts;
using iPlatform.Api.DTOs.SectionsPerChannel;

using MasterData;

using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Leads
{
    public class LeadModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IPaginateResults m_Paginator;
        private readonly IRepository m_Repository;
        private readonly IProvideContext m_ContextProvider;

        public LeadModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository, IProvideContext contextProvider)
        {
            this.m_ExecutionPlan = executionPlan;
            this.m_Paginator = paginator;
            this.m_Repository = repository;
            this.m_ContextProvider = contextProvider;

            Get[SystemRoutes.Leads.GetSectionsByActiveChannelId.Route] = p => GetSections();
            Get[SystemRoutes.Leads.GetById.Route] = p => GetLead(p.id);
            Get[SystemRoutes.Leads.GetByIdDetailed.Route] = p => GetLeadDetailed(p.id);
            Get[SystemRoutes.Leads.GetByIdDetailedSimplified.Route] = p => GetLeadDetailedSimplified(p.id);
            Get[SystemRoutes.Leads.GetByPartyId.Route] = p => GetLeadByPartyId(p.id);
            Post[SystemRoutes.Leads.Post.Route] = p => SaveLead();
            Post[SystemRoutes.Leads.Import.Route] = p => ImportLead();

            Put[SystemRoutes.Leads.Dead.Route] = p => DeadLead(p.id);
            Put[SystemRoutes.Leads.Loss.Route] = p => LossLead(p.id);
            Put[SystemRoutes.Leads.Delay.Route] = p => DelayLead(p.id);
            Put[SystemRoutes.Leads.RejectedAtUnderwriting.Route] = p => RejectAtUnderwritingLead(p.id);
            Put[SystemRoutes.Leads.Sold.Route] = p => SoldLead(p.id);
            Put[SystemRoutes.Leads.Imported.Route] = p => ImportedLead(p.id);
            Put[SystemRoutes.Leads.PolicyBindingCreated.Route] = p => PolicyBindingCreatedLead(p.id);

            Post[SystemRoutes.Leads.Quality.Route] = p => SaveQuality(p.id);
            Put[SystemRoutes.Leads.Quality.Route] = p => EditQuality(p.id);
            Get[SystemRoutes.Leads.Quality.Route] = p => GetQuality(p.id);

            Put[SystemRoutes.Leads.PutLeadStatus.Route] = p => UpdateLeadStatus(p.id);

            Post[SystemRoutes.Leads.GetRegOwnerIdContacts.Route] = p => GetRegOwnerContacts();

            Options[SystemRoutes.Leads.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Leads));
        }

        #region Lead
        public Negotiator GetLead(int id)
        {
            var result = m_ExecutionPlan
                .GetById<Lead, LeadDto>(() => m_Repository.GetById<Lead>(id))
                .OnSuccess(Mapper.Map<Lead, LeadDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Leads.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetSections()
        {
            var getSectionsDto = m_ContextProvider.Get().ActiveChannelId;

            var result = m_ExecutionPlan.Execute<GetSectionsPerChannelDto, ResultChannelSectionDto>(new GetSectionsPerChannelDto() { ChannelId = getSectionsDto}); 

            var response = Mapper.Map<POSTResponseDto<ResultChannelSectionDto>>(result);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetLeadByPartyId(int partyId)
        {
            var queryResult = m_ExecutionPlan
                            .Query<GetLeadDetailByPartyIdQuery, Lead, LeadDto, LeadDto>()
                            .Configure(q => q.WithPartyId(partyId))
                            .OnSuccess(r => Mapper.Map<Lead, LeadDto>(r.FirstOrDefault()))
                            .Execute();


            var response = queryResult.CreateGETResponse()
                 .CreateLinks(c => SystemRoutes.Leads.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetLeadDetailed(int id)
        {
            var criteria = this.Bind<GetLeadDetailedDto>();

            var result = m_ExecutionPlan
                .Search<GetLeadDetailByIdQuery, Lead, GetLeadDetailedDto, ListLeadAdvanceDto, ListLeadAdvanceDto>(criteria)
                .OnSuccess(r => r.FirstOrDefault().Build(m_Repository))
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Leads.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        public Negotiator GetRegOwnerContacts()
        {
            var partyId = this.Bind<RegOwnerContactDto>();
            var queryResult = m_ExecutionPlan
                .Query<GetRegOwnerContactsByPartyIdQuery, Contact, List<ContactDto>, ContactDto>()
                .Configure(q => q.WithPartyId(partyId.PartyId))
                .OnSuccess(result => Mapper.Map<List<ContactDto>>(result.ToList()))
                .Execute();

            var getRelationShip =
                m_Repository.GetAll<Relationship>().Where(p => p.Party.Id == partyId.PartyId);
            
            foreach (var rpartyId in getRelationShip)
            {
                foreach (var regOwner in queryResult.Response)
                {
                    if (rpartyId.ChildParty.Id == regOwner.Id)
                    {
                        regOwner.RelationshipType = rpartyId.RelationshipType;
                    }
                }
            }

            POSTResponseDto<List<ContactDto>> response = new POSTResponseDto<List<ContactDto>>(queryResult.Response,SystemRoutes.Leads.GetRegOwnerIdContacts.Route);
            
            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }

        public Negotiator GetLeadDetailedSimplified(int id)
        {
            var criteria = this.Bind<GetLeadDetailedDto>();

            var result = m_ExecutionPlan
                .Search<GetLeadDetailSimplifiedByIdQuery, Lead, GetLeadDetailedDto, ListLeadSimplifiedDto, ListLeadSimplifiedDto>(criteria)
                .OnSuccess(r => r.FirstOrDefault().BuildSimplifiedLead(m_Repository))
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Leads.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator SaveLead()
        {
            var createLeadDto = this.Bind<CreateLeadDto>();

            var result = m_ExecutionPlan.Execute<CreateLeadDto, int>(createLeadDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response));

            var foo = Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);

            return foo;
        }

        //This is used by SubmitLeadExecutionMessageConsumer
        //Better use connector.ImportManagement.Lead.SubmitLead()
        public Negotiator ImportLead()
        {
            var submitLeadDto = this.Bind<SubmitLeadDto>();

            var result = m_ExecutionPlan.Execute<SubmitLeadDto, LeadImportResponseDto>(submitLeadDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response.LeadId));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        #endregion

        #region Status
        public Negotiator DeadLead(int id)
        {
            var deadLeadActivityDto = this.Bind<DeadLeadActivityDto>();

            var result = m_ExecutionPlan.Execute<DeadLeadActivityDto, int>(deadLeadActivityDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator LossLead(int id)
        {
            var lossLeadActivityDto = this.Bind<LossLeadActivityDto>();

            var result = m_ExecutionPlan.Execute<LossLeadActivityDto, int>(lossLeadActivityDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator DelayLead(int id)
        {
            var delayLeadActivityDto = this.Bind<DelayLeadActivityDto>();

            var result = m_ExecutionPlan.Execute<DelayLeadActivityDto, int>(delayLeadActivityDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        public Negotiator RejectAtUnderwritingLead(int id)
        {
            var dto = this.Bind<RejectedAtUnderwritingLeadActivityDto>();

            var result = m_ExecutionPlan.Execute<RejectedAtUnderwritingLeadActivityDto, int>(dto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        public Negotiator SoldLead(int id)
        {
            var dto = this.Bind<SoldLeadActivityDto>();

            var result = m_ExecutionPlan.Execute<SoldLeadActivityDto, int>(dto);
            
            if (result.Response > 0)
            {
                var campaignLeadBucketEntryResult = m_ExecutionPlan.Execute<UpdateLeadStatusDto, bool>(new UpdateLeadStatusDto
                {
                    Id = dto.LeadId,
                    CampaignId = dto.CampaignId,
                    LeadCallCentreCodeId = LeadCallCentreCodes.Sold.Id,
                    CallCentreCode = LeadCallCentreCodes.Sold,
                    FaxNumber = "",
                    Comments = "",
                    CallBackDate = System.DateTime.Now,
                });
            }

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        public Negotiator ImportedLead(int id)
        {
            var dto = this.Bind<ImportedLeadActivityDto>();

            var result = m_ExecutionPlan.Execute<ImportedLeadActivityDto, int>(dto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator PolicyBindingCreatedLead(int id)
        {
            var policyBindingCreatedLeadActivityDto = this.Bind<PolicyBindingCreatedLeadActivityDto>();

            var result = m_ExecutionPlan.Execute<PolicyBindingCreatedLeadActivityDto, int>(policyBindingCreatedLeadActivityDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        #endregion

        #region Quality
        public Negotiator SaveQuality(int id)
        {
            var createLeadQualityDto = this.Bind<CreateLeadQualityDto>();

            var result = m_ExecutionPlan.Execute<CreateLeadQualityDto, int>(createLeadQualityDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator EditQuality(int id)
        {
            var editLeadQualityDto = this.Bind<EditLeadQualityDto>();

            var result = m_ExecutionPlan.Execute<EditLeadQualityDto, int>(editLeadQualityDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetQuality(int id)
        {
            var result = m_ExecutionPlan
                .GetById<LeadQuality, LeadQualityDto>(() => m_Repository.GetById<Lead>(id).LeadQuality)
                .OnSuccess(Mapper.Map<LeadQuality, LeadQualityDto>)
                .Execute();

            if (result.Response != null)
                result.Response.LeadId = id;

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Leads.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        #endregion


        #region Campaign Management

        private Negotiator UpdateLeadStatus(int id)
        {
            var updateLeadStatusDto = this.Bind<UpdateLeadStatusDto>();

            if (updateLeadStatusDto.LeadCallCentreCodeId > 0)
            updateLeadStatusDto.CallCentreCode =
                new LeadCallCentreCodes().First(a => a.Id == updateLeadStatusDto.LeadCallCentreCodeId);

            var result = m_ExecutionPlan.Execute<UpdateLeadStatusDto, bool>(updateLeadStatusDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        #endregion
    }
}