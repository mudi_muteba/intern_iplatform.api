﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.RatingRuleHeaderCalculations;
using Domain.RatingRuleHeaderCalculations.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using Nancy.Responses.Negotiation;


namespace Api.Modules.RatingRuleHeaderCalculations
{
    public class RatingRuleHeaderCalculationsModule : SecureModule
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public RatingRuleHeaderCalculationsModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.RatingRuleHeaderCalculations.Get.Route] = p => GetAll(new Pagination());
        }

        private Negotiator GetAll(Pagination pagination)
        {
            var queryResult = executionPlan
                 .Query<GetAllRatingRuleHeaderCalculationsQuery, RatingRuleHeaderCalculation, PagedResultDto<ListRatingRuleHeaderCalculationDto>, ListRatingRuleHeaderCalculationDto>()
                 .OnSuccess(result =>
                 {
                     return Mapper.Map<PagedResultDto<ListRatingRuleHeaderCalculationDto>>(result.Paginate(paginator, pagination));
                 })
                 .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.RatingRuleHeaderCalculations.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}