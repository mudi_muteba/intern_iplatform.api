﻿using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using AutoMapper;
using Domain.RatingRuleHeaderCalculations;

namespace Api.Modules.RatingRuleHeaderCalculations
{
    public class RatingRuleHeaderCalculationModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;
        public RatingRuleHeaderCalculationModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;
            Post[SystemRoutes.RatingRuleHeaderCalculations.Post.Route] = p => Create();
            Put[SystemRoutes.RatingRuleHeaderCalculations.Put.Route] = p => Update(p.id);
            Get[SystemRoutes.RatingRuleHeaderCalculations.GetById.Route] = p => GetById(p.id);
            Put[SystemRoutes.RatingRuleHeaderCalculations.PutDelete.Route] = p => DeleteUpdate(p.id);
        }

        private Negotiator Create()
        {
            var result = _executionPlan.Execute<CreateRatingRuleHeaderCalculationDto, int>(this.Bind<CreateRatingRuleHeaderCalculationDto>());
            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.RatingRuleHeaderCalculations.CreateGetById(result.Response));
            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator Update(int id)
        {
            var result = _executionPlan.Execute<EditRatingRuleHeaderCalculationDto, int>(this.Bind<EditRatingRuleHeaderCalculationDto>());
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.RatingRuleHeaderCalculations.CreateGetById(result.Response));
            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator GetById(int id)
        {
            var result = _executionPlan.GetById<RatingRuleHeaderCalculation, RatingRuleHeaderCalculationDto>(() => _repository
            .GetById<RatingRuleHeaderCalculation>(id))
                .OnSuccess(Mapper.Map<RatingRuleHeaderCalculation, RatingRuleHeaderCalculationDto>).Execute();
            var response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.RatingRuleHeaderCalculations.CreateLinks(c));
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator DeleteUpdate(int id)
        {
            var result = _executionPlan.Execute<DeleteRatingRuleHeaderCalculationDto, int>(this.Bind<DeleteRatingRuleHeaderCalculationDto>());
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.RatingRuleHeaders.CreateGetById(result.Response));
            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
    }
}