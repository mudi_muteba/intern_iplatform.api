﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.DataViews;
using Domain.DataViews.Queries;
using Domain.DataViews.StoredProcedures;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.DataView.DataViewLeadImport;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Modules.DataView
{
    public class DataViewModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;

        public DataViewModule(IExecutionPlan executionPlan)
        {
            m_ExecutionPlan = executionPlan;

            Post[SystemRoutes.DataView.LeadImportDataView.Route] = _ => GetLeadImportDataView();
        }

        #region [ Negotiators ]

        private Negotiator GetLeadImportDataView()
        {
            SearchDataViewLeadImportDto searchDataViewLeadImportDto = this.Bind<SearchDataViewLeadImportDto>();

            QueryResult<IList<DataViewLeadImportDto>> result = m_ExecutionPlan.QueryStoredProcedure<DataViewLeadImportQuery, DataViewLeadImport,
                DataViewLeadImportStoredProcedure, DataViewLeadImportDto>
                (new DataViewLeadImportStoredProcedure(searchDataViewLeadImportDto))
                .OnSuccess(Mapper.Map<IList<DataViewLeadImport>, IList<DataViewLeadImportDto>>)
                .Execute();

            POSTResponseDto<IList<DataViewLeadImportDto>> response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        #endregion
    }
}