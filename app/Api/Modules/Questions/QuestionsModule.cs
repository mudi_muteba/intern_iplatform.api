﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.QuestionDefinitions;
using Domain.QuestionDefinitions.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.Responses.Negotiation;
using System.Linq;
namespace Api.Modules.Questions
{
    public class QuestionsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public QuestionsModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.paginator = paginator;
            this.repository = repository;

            Get[SystemRoutes.Questions.GetById.Route] = p => GetQuestion(p.id);
            Get[SystemRoutes.Questions.GetByCoverId.Route] = p => GetQuestionByCover(p.id, new Pagination());
            Get[SystemRoutes.Questions.GetByCoverIdWithPagination.Route] = p =>
                GetQuestionByCover(p.id, new Pagination(p.PageNumber, p.PageSize));
            Get[SystemRoutes.Questions.GetByCoverIdNoPagination.Route] = p => GetQuestionByCoverNoPagination(p.id);
            Options[SystemRoutes.Questions.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Questions));
            Get[SystemRoutes.Questions.GetByCoverIdAndProductId.Route] = p => GetQuestionsByCoveridAndProductId(p.coverId, p.productId);
        }

        public Negotiator GetQuestion(int id)
        {
            var result = executionPlan
                .GetById<QuestionDefinition, QuestionDefinitionDto>(() => repository.GetById<QuestionDefinition>(id))
                .OnSuccess(Mapper.Map<QuestionDefinition, QuestionDefinitionDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Questions.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetQuestionByCover(int id, Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetAllQuestionsByCoverIdQuery, QuestionDefinition, PagedResultDto<QuestionDefinitionDto>, QuestionDefinitionDto>()
                .Configure(q => q.WithCoverId(id))
                .OnSuccess(result => Mapper.Map<PagedResultDto<QuestionDefinitionDto>>(result.Paginate(paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Questions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetQuestionByCoverNoPagination(int id)
        {
            var queryResult = executionPlan
                .Query<GetAllQuestionsByCoverIdQuery, QuestionDefinition, ListResultDto<QuestionDefinitionDto>, QuestionDefinitionDto>()
                .Configure(q => q.WithCoverId(id))
                .OnSuccess(result => Mapper.Map<ListResultDto<QuestionDefinitionDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Questions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetQuestionsByCoveridAndProductId(int coverId, int productId)
        {
            var queryResult = executionPlan
                .Query<GetAllQuestionsByCoverIdAndProductIdQuery, QuestionDefinition, ListResultDto<QuestionDto>, QuestionDto>()
                .Configure(q => q.WithCoverIdAndProductId(coverId, productId))
                .OnSuccess(result => Mapper.Map<ListResultDto<QuestionDto>>(result.ToList().Select(r => r.Question).Distinct().ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Questions.CreateQuestionDtoLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}