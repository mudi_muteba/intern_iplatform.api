﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Organizations;
using Domain.Organizations.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Insurers;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Insurers
{
    public class InsurerModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public InsurerModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.Insurers.GetById.Route] = p => GetInsurer(p.id);

            //Get[SystemRoutes.Insurers.GetInsurerBranches.Route] = p => GetInsurersBranches(p.id, new Pagination());
            //Get[SystemRoutes.Insurers.GetInsurerBranches.Route] = p => GetInsurersBranches(p.id, new Pagination());
            //Post[SystemRoutes.Insurers.PostInsurerBranch.Route] = p => CreateInsurersBranch(p.id);
            //Put[SystemRoutes.Insurers.PutInsurerBranch.Route] = p => EditInsurersBranch(p.id, p.branchId);
            //Put[SystemRoutes.Insurers.DeleteInsurerBranch.Route] = p => DeleteInsurersBranch(p.id, p.branchId);
        }

        private Negotiator GetInsurer(int id)
        {
            var result = executionPlan
                .GetById<Organization, ListInsurerDto>(() => repository.GetById<Organization>(id))
                .OnSuccess(Mapper.Map<Organization, ListInsurerDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Insurers.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        //private Negotiator GetInsurersBranches(int id, Pagination pagination)
        //{
        //    var queryResult = executionPlan
        //        .Query<GetAllInsurersBranchesQuery, InsurersBranches, PagedResultDto<InsurersBranchesDto>, InsurersBranchesDto>()
        //        .OnSuccess(result => Mapper.Map<PagedResultDto<InsurersBranchesDto>>(result.Paginate(paginator, pagination)))
        //        .Execute();

        //    var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
        //        .CreateLinks(c => SystemRoutes.Insurers.CreateLinks(c));

        //    return Negotiate.ForSupportedFormats(response, response.StatusCode);
        //}

        //private Negotiator CreateInsurersBranch(int id)
        //{
        //    var result = executionPlan
        //        .GetById<Organization, ListInsurerDto>(() => repository.GetById<Organization>(id))
        //        .OnSuccess(Mapper.Map<Organization, ListInsurerDto>)
        //        .Execute();

        //    var response = result.CreateGETResponse()
        //        .CreateLinks(c => SystemRoutes.Insurers.CreateLinks(c))
        //        ;

        //    return Negotiate.ForSupportedFormats(response, response.StatusCode);
        //}

        //private Negotiator EditInsurersBranch(int id, int branchId)
        //{
        //    var result = executionPlan
        //        .GetById<Organization, ListInsurerDto>(() => repository.GetById<Organization>(id))
        //        .OnSuccess(Mapper.Map<Organization, ListInsurerDto>)
        //        .Execute();

        //    var response = result.CreateGETResponse()
        //        .CreateLinks(c => SystemRoutes.Insurers.CreateLinks(c))
        //        ;

        //    return Negotiate.ForSupportedFormats(response, response.StatusCode);
        //}

        //private Negotiator DeleteInsurersBranch(int id, int branchId)
        //{
        //    var result = executionPlan
        //        .GetById<Organization, ListInsurerDto>(() => repository.GetById<Organization>(id))
        //        .OnSuccess(Mapper.Map<Organization, ListInsurerDto>)
        //        .Execute();

        //    var response = result.CreateGETResponse()
        //        .CreateLinks(c => SystemRoutes.Insurers.CreateLinks(c))
        //        ;

        //    return Negotiate.ForSupportedFormats(response, response.StatusCode);
        //}
    }
}