﻿
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Organizations;
using Domain.Organizations.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Insurers;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Insurers
{
    public class InsurersModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public InsurersModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Options[SystemRoutes.Insurers.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Insurers));

            Get[SystemRoutes.Insurers.GetAll.Route] = _ => GetAllInsurers(new Pagination());

            Get[SystemRoutes.Insurers.GetWithPagination.Route] =
                p => GetAllInsurers(new Pagination(p.PageNumber, p.PageSize));

            Post[SystemRoutes.Insurers.PostSearch.Route] = p => SearchInsurers();
        }

        private Negotiator GetAllInsurers(Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetAllOrganizationsQuery, Organization, PagedResultDto<ListInsurerDto>, ListInsurerDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListInsurerDto>>(result.Paginate(paginator, pagination)))
                .Execute();

            LISTPagedResponseDto<ListInsurerDto> response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Insurers.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchInsurers()
        {
            var criteria = this.Bind<InsurerSearchDto>();

            var queryResult = executionPlan
                .Search<SearchOrganizationsQuery, Organization, InsurerSearchDto, PagedResultDto<ListInsurerDto>, ListInsurerDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListInsurerDto>>(result.Paginate(paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Insurers.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}