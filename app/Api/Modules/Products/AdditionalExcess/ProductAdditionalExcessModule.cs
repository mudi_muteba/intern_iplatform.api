﻿using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Products;
using Domain.Products.Queries;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.ClaimsItems;

namespace Api.Modules.Products.AdditionalExcess
{
    public class ProductAdditionalExcessModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public ProductAdditionalExcessModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Products.AdditionalExcessByCoverId.Route] = p => GetAdditionalExcessByCover(p.product, p.id);
        }

        private Negotiator GetAdditionalExcessByCover(int product, int id)
        {
            var queryResult = executionPlan
               .Query<GetAdditonalExcessByCoverIdQuery, ProductAdditionalExcess, ListResultDto<ProductAdditionalExcessDto>>()
               .Configure(q => q.WithCriterias(product,id))
               .OnSuccess(r => Mapper.Map<List<ProductAdditionalExcess>, ListResultDto<ProductAdditionalExcessDto>>(r.ToList()))
               .Execute();

            LISTResponseDto<ProductAdditionalExcessDto> response = queryResult.CreateLISTResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}