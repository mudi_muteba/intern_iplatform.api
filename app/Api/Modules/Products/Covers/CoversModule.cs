using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Products;
using Domain.Products.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Products.Covers
{
    public class CoversModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public CoversModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Products.CoversById.Route] = p => GetProductCoverById(p.product, p.id);

            Get[SystemRoutes.Products.CoversByProductId.Route] = p => GetProductCoverById(p.product);
        }

        private Negotiator GetProductCoverById(int product, int id)
        {
            var queryResult = executionPlan
                .Query<GetAllocatedProductByIdQuery, Product, CoverDefinitionDto, CoverDefinitionDto>()
                .Configure(q => q.WithId(product))
                .OnSuccess(r =>
                {
                    var allocatedProduct = r.FirstOrDefault();
                    if (allocatedProduct == null)
                    {
                        return null;
                    }

                    var cover = allocatedProduct.CoverDefinitions.FirstOrDefault(c => c.Id == id);
                    var requestedCover = Mapper.Map<CoverDefinition, CoverDefinitionDto>(cover);

                    return requestedCover;
                })
                .Execute();

            var result = new GetByIdResult<CoverDefinitionDto>(queryResult.Response);

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetProductCoverById(int product)
        {
            var queryResult = executionPlan
                .Query<GetAllocatedProductByIdQuery, Product, ListResultDto<ListCoverDefinitionDto>>()
                .Configure(q => q.WithId(product))
                .OnSuccess(r =>
                {
                    var allocatedProduct = r.FirstOrDefault();
                    if (allocatedProduct == null)
                    {
                        return null;
                    }

                    var covers = Mapper.Map<List<CoverDefinition>, ListResultDto<ListCoverDefinitionDto>>(allocatedProduct.CoverDefinitions.ToList());

                    return covers;
                })
                .Execute();

            var response = queryResult.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(product, c.Results))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


    }
}