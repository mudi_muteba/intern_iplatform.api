﻿using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Products;
using Domain.Products.Queries;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.ClaimsItems;

namespace Api.Modules.Products.Benefits
{
    public class ProductBenefitModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public ProductBenefitModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Products.BenefitsByCoverId.Route] = p => GetBenefitsByCover(p.product, p.id);
        }


        private Negotiator GetBenefitsByCover(int product, int id)
        {
            var queryResult = executionPlan
               .Query<GetBenefitsByCoverIdQuery, ProductBenefit, ListResultDto<ProductBenefitDto>>()
               .Configure(q => q.WithCriterias(product,id))
               .OnSuccess(r => Mapper.Map<List<ProductBenefit>, ListResultDto<ProductBenefitDto>>(r.ToList()))
               .Execute();

            LISTResponseDto<ProductBenefitDto> response = queryResult.CreateLISTResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}