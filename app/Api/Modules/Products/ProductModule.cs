using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;
using Api.Models;

using Domain.Base.Execution;
using Domain.Products;
using Domain.Products.Queries;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;


using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Campaigns.Queries;

using iPlatform.Api.DTOs.Campaigns;

namespace Api.Modules.Products
{
    public class ProductModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public ProductModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Products.GetProducts.Route] = p => GetProducts();
            Get[SystemRoutes.Products.GetById.Route] = p => GetProductById(p.id);
            Get[SystemRoutes.Products.GetByCode.Route] = p => GetProductByCode(p.code);
            Get[SystemRoutes.Products.ProposalForm.Route] = p => GetProposalForm(p.id);

            Options[SystemRoutes.Products.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Products));
        }

        private Negotiator GetProducts()
        {
            var queryResult = executionPlan
                .Query<GetAllAllocatedProductsQuery, AllocatedProduct, List<AllocatedProductDto>, AllocatedProductDto>()
                .OnSuccess(result => Mapper.Map<List<AllocatedProductDto>>(result))
                .Execute();

            var response = queryResult.CreateGETResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetProductByCode(string code)
        {
            var queryResult = executionPlan
                .Query<GetAllocatedProductByCodeQuery, Product, AllocatedProductDto, AllocatedProductDto>()
                .Configure(q => q.WithCode(code))
                .OnSuccess(r => Mapper.Map<Product, AllocatedProductDto>(r.FirstOrDefault()))
                .Execute();

            var result = new GetByIdResult<AllocatedProductDto>(queryResult.Response);

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }

        private Negotiator GetProposalForm(int id)
        {
            var queryResult = executionPlan
                .Query<GetAllocatedProductByIdQuery, Product, ProposalFormDto, ProposalFormDto>()
                .Configure(q => q.WithId(id))
                .OnSuccess(r => Mapper.Map<Product, ProposalFormDto>(r.FirstOrDefault()))
                .Execute();

            var result = new GetByIdResult<ProposalFormDto>(queryResult.Response);

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetProductById(int id)
        {
            var queryResult = executionPlan
                .Query<GetAllocatedProductByIdQuery, Product, AllocatedProductDto, AllocatedProductDto>()
                .Configure(q => q.WithId(id))
                .OnSuccess(r => Mapper.Map<Product, AllocatedProductDto>(r.FirstOrDefault()))
                .Execute();

            var result = new GetByIdResult<AllocatedProductDto>(queryResult.Response);

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}