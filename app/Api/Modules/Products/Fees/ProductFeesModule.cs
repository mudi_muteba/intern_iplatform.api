﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Products.Fees;
using Domain.Products.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Products.Fees
{
    public class ProductFeesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;
        private readonly IPaginateResults paginator;
        public ProductFeesModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Get[SystemRoutes.Products.Fees.Route] = p => GetFees(p.productid);
            Get[SystemRoutes.Products.FeesById.Route] = p => GetFee(p.productid, p.id);
            Get[SystemRoutes.Products.FeesByChannelId.Route] = p => GetFees(p.productid, p.channelId);
            Get[SystemRoutes.Products.FeesByIdChannelId.Route] = p => GetFee(p.productid, p.id, p.channelId);
            Get[SystemRoutes.Products.GetAllFees.Route] = p => GetAllProductFees(new Pagination(p.pageNumber, p.pageSize));
            Get[SystemRoutes.Products.GetProductFeeById.Route] = p => GetProductFeeById(p.id);
        }


        private Negotiator GetFees(int productid)
        {
            var queryResult = executionPlan
               .Query<GetFeesByProductQuery, ProductFee, ListResultDto<ProductFeeDto>, ProductFeeDto>()
               .Configure(q => q.WithProductId(productid))
               .OnSuccess(r => Mapper.Map<List<ProductFee>, ListResultDto<ProductFeeDto>>(r.ToList()))
               .Execute();

            LISTResponseDto<ProductFeeDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetFee(int productid, int id)
        {
            var queryResult = executionPlan
               .Query<GetFeesByProductQuery, ProductFee, ProductFeeDto, ProductFeeDto>()
               .Configure(q => q.WithCriterias(productid, id))
               .OnSuccess(r => Mapper.Map<ProductFee, ProductFeeDto>(r.FirstOrDefault()))
               .Execute();

            var response = queryResult.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetFees(int productid, int channelId)
        {
            var queryResult = executionPlan
               .Query<GetFeesByProductQuery, ProductFee, ListResultDto<ProductFeeDto>, ProductFeeDto>()
               .Configure(q => q.WithProductIdChannelId(productid, channelId))
               .OnSuccess(r => Mapper.Map<List<ProductFee>, ListResultDto<ProductFeeDto>>(r.ToList()))
               .Execute();

            LISTResponseDto<ProductFeeDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetFee(int productid, int id, int channelId)
        {
            var queryResult = executionPlan
               .Query<GetFeesByProductQuery, ProductFee, ProductFeeDto, ProductFeeDto>()
               .Configure(q => q.WithProductFeeIdProductIdChannelId(productid, id, channelId))
               .OnSuccess(r => Mapper.Map<ProductFee, ProductFeeDto>(r.FirstOrDefault()))
               .Execute();

            var response = queryResult.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllProductFees(Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetAllProductFeesQuery, ProductFee, PagedResultDto<ListProductFeeDto>, ListProductFeeDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListProductFeeDto>>(result.Paginate(paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response).CreateLinks(c => SystemRoutes.Products.CreateFeesUrl(c.Product.Id));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetProductFeeById(int id)
        {
            var queryResult = executionPlan
                .Query<GetProductFeeByIdQuery, ProductFee, ListProductFeeDto, ListProductFeeDto>()
                .Configure(q => q.WithCriterias(id))
                .OnSuccess(result => Mapper.Map<ListProductFeeDto>(result.ToList().FirstOrDefault()))
                .Execute();

            var response = queryResult.CreateGETResponse().CreateLinks(c => SystemRoutes.Products.CreateFeesUrl(c.Id));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}