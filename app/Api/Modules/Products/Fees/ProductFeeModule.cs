﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Admin.SettingsiPersons;
using Domain.Admin.SettingsiPersons.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Products.Fees;
using Domain.Products.Queries;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;

namespace Api.Modules.Products.Fees
{
    public class ProductFeeModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;
        private readonly IPaginateResults _paginator;

        public ProductFeeModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            _executionPlan = executionPlan;
            _repository = repository;
            _paginator = paginator;

            Post[SystemRoutes.Products.Create.Route] = p => CreateProductFee();
            Put[SystemRoutes.Products.EditProductFee.Route] = p => UpdateProductFee(p.id);
            Put[SystemRoutes.Products.PutDelete.Route] = p => DeleteProductFee(p.id);
            Post[SystemRoutes.Products.SearchFees.Route] = p => Search();
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<SearchProductFeeDto>();

            var queryResult = _executionPlan
                .Search<SearchProductFeeQuery, ProductFee, SearchProductFeeDto, PagedResultDto<ListProductFeeDto>, ListProductFeeDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListProductFeeDto>>(
                    result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator CreateProductFee()
        {
            var createProductFeeDto = this.Bind<CreateProductFeeDto>();

            var result = _executionPlan.Execute<CreateProductFeeDto, int>(createProductFeeDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.Products.CreateFeesUrl(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateProductFee(int id)
        {
            var editProductFeeDto = this.Bind<EditProductFeeDto>();
            editProductFeeDto.Id = id;

            var result = _executionPlan.Execute<EditProductFeeDto, int>(editProductFeeDto);

            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Products.CreateFeesUrl(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteProductFee(int id)
        {
            var disableProductFeeDto = this.Bind<DisableProductFeeDto>();
            disableProductFeeDto.Id = id;

            var result = _executionPlan.Execute<DisableProductFeeDto, int>(disableProductFeeDto);

            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Products.CreateFeesUrl(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
    }
}