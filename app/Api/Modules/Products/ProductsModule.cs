﻿using System.Collections.Generic;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Products;
using Domain.Products.Builders;
using Domain.Products.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.Responses.Negotiation;
using iPlatform.Api.DTOs.Base.Connector;
using System.Linq;

namespace Api.Modules.Products
{
    public class ProductsModule : SecureModule
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public ProductsModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.Products.Get.Route] = _ => GetAllProducts(new Pagination());
            Get[SystemRoutes.Products.GetProductsByCover.Route] = p => GetProductsByCover(p.channelId);
            Get[SystemRoutes.Products.GetAllNoPagination.Route] = _ => GetAllNoPagination();
            Get[SystemRoutes.Products.GetAllocatedProduct.Route] = p => GetAllocatedProductByChannelId(p.id);
            Get[SystemRoutes.Products.GetAllAllocatedProduct.Route] = p => GetAllAllocatedProducts();
            Get[SystemRoutes.Products.GetAllWithPagination.Route] =
                p => GetAllProducts(new Pagination(p.PageNumber, p.PageSize));

            Get[SystemRoutes.Products.GetAllForReporting.Route] = _ => GetAllForReporting();

            Options[SystemRoutes.Products.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Products));
        }

        private Negotiator GetAllProducts(Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetAllAllocatedProductsQuery, AllocatedProduct, PagedResultDto<ListAllocatedProductDto>, ListAllocatedProductDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListAllocatedProductDto>>(
                    result.ToList()))
                .Execute();

            LISTPagedResponseDto<ListAllocatedProductDto> response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetAllNoPagination()
        {
            var queryResult = executionPlan
                .Query<GetAllProductsQuery, Product, ListResultDto<ListProductDto>, ListProductDto>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListProductDto>>(
                    result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator GetAllAllocatedProducts()
        {
            var queryResult = executionPlan
                .Query<GetAllAllocatedProductsQuery, AllocatedProduct, ListResultDto<ProductInfoDto>, ProductInfoDto>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ProductInfoDto>>(
                    result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator GetAllocatedProductByChannelId(int id)
        {
            var queryResult = executionPlan
                .Query<GetAllAllocatedProductsQuery, AllocatedProduct, ListResultDto<ProductInfoDto>, ProductInfoDto>()
                .Configure(q => q.WithChannelId(id))
                .OnSuccess(result => Mapper.Map<ListResultDto<ProductInfoDto>>(
                    result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetProductsByCover(int channelId)
        {
            var queryResult = executionPlan
                .Query<GetAllAllocatedProductsQuery, AllocatedProduct, ListResultDto<ListProductByCoverDto>, ListProductByCoverDto>()
                .OnSuccess(r => new ListResultDto<ListProductByCoverDto>
                {
                    Results = new ProductByCoverBuilder(_repository, channelId).Build(r)
                })
                .Execute();

            LISTResponseDto<ListProductByCoverDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.IndividualAddresses.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllForReporting()
        {
            var queryResult = executionPlan
                .Query<GetAllProductsForReportingQuery, Product, ListResultDto<ListProductDto>, ListProductDto>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListProductDto>>(
                    result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Products.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}