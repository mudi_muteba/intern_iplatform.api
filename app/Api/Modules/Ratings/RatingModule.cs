﻿using System.Linq;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Ratings
{
    public class RatingModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public RatingModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Post[SystemRoutes.Rating.GetRating.Route] = _ => GetRating();

            Options[SystemRoutes.Rating.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Rating));
        }

        private Negotiator GetRating()
        {
            var createDto = this.Bind<RatingRequestDto>();

            var result = executionPlan.Execute<RatingRequestDto, RatingResultDto>(createDto);
            
            var imageBase = string.Format("{0}/content/logos/", Request.Url.SiteBase);

            foreach (var policy in result.Response.Policies)
            {
                var image = policy.AboutProduct.Images.FirstOrDefault();
                if(image == null)
                    continue;

                var imagePath = string.Format("{0}{1}", imageBase, image.Url);
                image.Url = imagePath;
            }

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Rating.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}