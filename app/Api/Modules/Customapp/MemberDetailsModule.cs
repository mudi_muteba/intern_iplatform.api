﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Reports.Base.Overrides;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.CustomApp;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Customapp
{
    public class MemberDetailsModule : SecureModule
    {
        private IExecutionPlan _executionPlan;
        private IRepository _repository;

        public MemberDetailsModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            #region [ Route Builder ]

            Post[SystemRoutes.Customapp.Register.Route] = p => Register();

            Post[SystemRoutes.Customapp.VerifyMember.Route] = p => VerifyMember();

            Post[SystemRoutes.Customapp.GetPolicyInfo.Route] = p => GetPolicyInfo();

            Post[SystemRoutes.Customapp.GetMemberDetails.Route] = p => GetMemberDetails();

            Post[SystemRoutes.Customapp.UpdateMemberDetails.Route] = p => UpdateMemberDetails();

            #endregion
        }

        #region [ Negotiators ]

        private Negotiator Register()
        {
            RegisterSearchDto registerSearchDto = this.Bind<RegisterSearchDto>();

            HandlerResult<List<RegisterDto>> result =_executionPlan.Execute<RegisterSearchDto, List<RegisterDto>>(registerSearchDto);
            CustomAppResponseDto<List<RegisterDto>> response = result.CreateCustomAppResponse();
            if (String.IsNullOrWhiteSpace(response.LastErrorDescription))
            {
                response.Success = true;
            }

            return Negotiate.ForSupportedFormats(response, HttpStatusCode.OK);
        }

        private Negotiator VerifyMember()
        {
            VerifyMemberSearchDto verifyMemberSearchDto = this.Bind<VerifyMemberSearchDto>();
            HandlerResult<List<VerifyMemberDto>> result = _executionPlan.Execute<VerifyMemberSearchDto, List<VerifyMemberDto>>(verifyMemberSearchDto);
            CustomAppResponseDto<List<VerifyMemberDto>> response = result.CreateCustomAppResponse();
            if (String.IsNullOrWhiteSpace(response.LastErrorDescription))
            {
                response.Success = true;
            }

            return Negotiate.ForSupportedFormats(response, HttpStatusCode.OK);
        }

        private Negotiator GetPolicyInfo()
        {
            GetPolicyInfoSearchDto getPolicyInfoSearchDto = this.Bind<GetPolicyInfoSearchDto>();
            HandlerResult<List<GetPolicyInfoDto>> result = _executionPlan.Execute<GetPolicyInfoSearchDto, List<GetPolicyInfoDto>>(getPolicyInfoSearchDto);
            CustomAppResponseDto<List<GetPolicyInfoDto>> response = result.CreateCustomAppResponse();
            if (String.IsNullOrWhiteSpace(response.LastErrorDescription))
            {
                response.Success = true;
            }

            return Negotiate.ForSupportedFormats(response, HttpStatusCode.OK);
        }

        private Negotiator GetMemberDetails()
        {
            var searchDto = this.Bind<GetMemberDetailsSearchDto>();
            HandlerResult<List<GetMemberDetailsDto>> result = _executionPlan.Execute<GetMemberDetailsSearchDto, List<GetMemberDetailsDto>>(searchDto);
            CustomAppResponseDto<List<GetMemberDetailsDto>> response = result.CreateCustomAppResponse();
            if (String.IsNullOrWhiteSpace(response.LastErrorDescription))
            {
                response.Success = true;
            }

            return Negotiate.ForSupportedFormats(response, HttpStatusCode.OK);
        }

        private Negotiator UpdateMemberDetails()
        {
            UpdateMemberDetailsSearchDto updateMemberDetailsSearchDto = this.Bind<UpdateMemberDetailsSearchDto>();
            HandlerResult<List<UpdateMemberDetailsDto>> result = _executionPlan.Execute<UpdateMemberDetailsSearchDto, List<UpdateMemberDetailsDto>>(updateMemberDetailsSearchDto);
            CustomAppResponseDto<List<UpdateMemberDetailsDto>> response = result.CreateCustomAppResponse();
            if (String.IsNullOrWhiteSpace(response.LastErrorDescription))
            {
                response.Success = true;
            }
            return Negotiate.ForSupportedFormats(response, HttpStatusCode.OK);
        }

        #endregion
    }
}