﻿using System;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;
using iPlatform.Api.DTOs.PolicyBindings;
using Domain.PolicyBindings.Queries;
using Domain.PolicyBindings;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Party.Quotes;
using Domain.Reports.PolicyBinding;
using Domain.Reports.PolicyBinding.Queries;
using Domain.Reports.PolicyBinding.StoredProcedures;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.PolicyBinding;
using iPlatform.Api.DTOs.Reports.PolicyBinding.Criteria;

namespace Api.Modules.PolicyBindings
{
    public class PolicyBindingsModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IPaginateResults m_Paginator;
        private readonly IRepository m_Repository;

        public PolicyBindingsModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.m_Paginator = paginator;
            this.m_ExecutionPlan = executionPlan;
            this.m_Repository = repository;

            Options[SystemRoutes.PolicyBinding.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.PolicyBinding));
            Post[SystemRoutes.PolicyBinding.Search.Route] = p => SearchPolicyBindings();
            Post[SystemRoutes.PolicyBinding.Save.Route] = p => SavePolicyBinding();
            Post[SystemRoutes.PolicyBinding.GetByQuoteId.Route] = p => GetByQuoteId();
            Post[SystemRoutes.PolicyBinding.GetFirstAsset.Route] = p => GetAssetInfo();
            Post[SystemRoutes.PolicyBinding.SaveAnswers.Route] = p => SavePolicyBindingAnswers();
            Post[SystemRoutes.PolicyBinding.GetNavigation.Route] = p => GetNavigation();
            Post[SystemRoutes.PolicyBinding.ValidateConfiguration.Route] = p => ValidateConfiguration();
            Post[SystemRoutes.PolicyBinding.Submit.Route] = p => SubmitPolicyBinding();
            Post[SystemRoutes.PolicyBinding.GetPolicyBindingReport.Route] = p => GetPolicyBindingReport();
            Post[SystemRoutes.PolicyBinding.PolicyBindingNotification.Route] = p => PolicyBindingNotification();
        }


        private Negotiator GetPolicyBindingReport()
        {
            var criteria = this.Bind<PolicyBindingReportCriteriaDto>();

            var channel = m_Repository.GetById<Channel>(criteria.CurrentChannelId);
            var channelQueryResult = new PolicyBindingReportChannelDto();

            if (channel != null)
            {
                channelQueryResult.Name = channel.Name;
            }

            var agentQueryResult = new PolicyBindingReportAgentDto();
            if (criteria.AgentId > 0)
            {
                var agent = m_Repository.GetById<Domain.Party.Party>(criteria.AgentId);

                if (agent != null)
                {
                    agentQueryResult.DisplayName = agent.DisplayName;

                    var address = agent.Addresses.FirstOrDefault();
                    if (address != null)
                    {
                        agentQueryResult.Address = address.Description;
                    }

                    agentQueryResult.Phone = agent.ContactDetail.Cell;
                    agentQueryResult.Email = agent.ContactDetail.Email;
                }
            }

            var client = m_Repository.GetById<Domain.Party.Party>(criteria.PartyId);
            var clientQueryResult = new PolicyBindingReportClientDto();

            if (client != null)
            {
                clientQueryResult.DisplayName = client.DisplayName;

                var address = client.Addresses.FirstOrDefault();
                if (address != null)
                {
                    clientQueryResult.Address = address.Description;
                }

                clientQueryResult.Phone = client.ContactDetail.Cell;
                clientQueryResult.Email = client.ContactDetail.Email;
            }

            var questionAnswerQueryResult = m_ExecutionPlan
                .QueryStoredProcedure<GetPolicyBindingReportQuestionAnwserQuery, PolicyBindingReportQuestionAnwser, PolicyBindingReportQuestionAnwserStoredProcedure, PolicyBindingReportQuestionAnswerDto>(new PolicyBindingReportQuestionAnwserStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<PolicyBindingReportQuestionAnwser>, IList<PolicyBindingReportQuestionAnswerDto>>)
                .Execute();

            var proposalCriteria = new ProposalCoverReportCriteriaDto { QuoteId = criteria.QuoteId };
            var proposalCoverQuestionAnswerQueryResult = m_ExecutionPlan
                .QueryStoredProcedure<GetProposalCoverReportQuestionAnwserQuery, ProposalCoverReportQuestionAnwser, ProposalCoverReportQuestionAnwserStoredProcedure, ProposalCoverReportQuestionAnswerDto>(new ProposalCoverReportQuestionAnwserStoredProcedure(proposalCriteria))
                .OnSuccess(Mapper.Map<IList<ProposalCoverReportQuestionAnwser>, IList<ProposalCoverReportQuestionAnswerDto>>)
                .Execute();

            var product = m_Repository.GetById<Quote>(criteria.QuoteId).Product;
            var productName = string.Empty;
            if (product != null)
            {
                productName = product.Name;
            }

            var policyBindingContainer = new PolicyBindingReportContainerDto
            {
                Channel = channelQueryResult,
                Client = clientQueryResult,
                QuestionAnswer = questionAnswerQueryResult.Response.ToList(),
                Agent = agentQueryResult,
                QuestionAnswerProposal = proposalCoverQuestionAnswerQueryResult.Response.ToList(),
                ProductName = productName,
                DateSubmitted = DateTime.UtcNow.ToString("D")
            };

            var result = new QueryResult<PolicyBindingReportContainerDto>(policyBindingContainer);

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchPolicyBindings()
        {
            var criteria = this.Bind<SearchPolicyBindingDto>();

            var queryResult = m_ExecutionPlan
                .Search<SearchPolicyBindingsQuery, PolicyBinding, SearchPolicyBindingDto, PagedResultDto<PolicyBindingListDto>>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<PolicyBindingListDto>>(result.Paginate(m_Paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.PolicyBinding.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SavePolicyBinding()
        {
            var createDto = this.Bind<SavePolicyBindingDto>();

            var result = m_ExecutionPlan.Execute<SavePolicyBindingDto, int>(createDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.PolicyBinding.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetByQuoteId()
        {
            var dto = this.Bind<GetPolicyBindingByQuoteDto>();
            var result = m_ExecutionPlan.Execute<GetPolicyBindingByQuoteDto, PolicyBindingDetailsDto>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAssetInfo()
        {
            var dto = this.Bind<GetPolicyBindingFirstAssetDto>();
            var result = m_ExecutionPlan.Execute<GetPolicyBindingFirstAssetDto, int>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SavePolicyBindingAnswers()
        {
            var dto = this.Bind<SavePolicyBindingAnswerDto>();
            var result = m_ExecutionPlan.Execute<SavePolicyBindingAnswerDto, bool>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetNavigation()
        {
            var dto = this.Bind<GetPolicyBindingNavigationDto>();
            var result = m_ExecutionPlan.Execute<GetPolicyBindingNavigationDto, PolicyBindingNavigationInfoDto>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator ValidateConfiguration()
        {
            var dto = this.Bind<ValidatePolicyBindingConfigurationDto>();
            var result = m_ExecutionPlan.Execute<ValidatePolicyBindingConfigurationDto, bool>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SubmitPolicyBinding()
        {
            var dto = this.Bind<SubmitPolicyBindingDto>();
            var result = m_ExecutionPlan.Execute<SubmitPolicyBindingDto, SubmitPolicyBindingResponseDto>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator PolicyBindingNotification()
        {
            var criteria = this.Bind<PolicyBindingNotificationDto>();

            var result = m_ExecutionPlan.Execute<PolicyBindingNotificationDto, ReportEmailResponseDto>(criteria);
            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}