using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.PolicyBindings;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.PolicyBindings
{
    public class PolicyBindingModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IRepository m_Repository;

        public PolicyBindingModule(IExecutionPlan executionPlan, IRepository repository)
        {
            m_ExecutionPlan = executionPlan;
            m_Repository = repository; 
        }
    }
}