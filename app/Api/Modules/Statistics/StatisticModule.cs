﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using Domain.Activities;
using Domain.Base.Execution;
using Domain.Statistics;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;
using iPlatform.Api.DTOs.Statistics;
using Nancy.ModelBinding;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Api.Modules.Statistics
{
    public class StatisticModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;

        public StatisticModule(IExecutionPlan executionPlan)
        {
            _executionPlan = executionPlan;

            Get[SystemRoutes.LeadConversionStatistics.GetByAgentId.Route] = p => GetStatistics(p.agentId);
            Get[SystemRoutes.LeadConversionStatistics.GetMonthlyByAgentId.Route] = p => GetMonthlyStatistics(p.agentId);

            Options[SystemRoutes.LeadConversionStatistics.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.LeadConversionStatistics));
        }

        private Negotiator GetStatistics(int agentId)
        {
            var getDayStatsDto = this.Bind<GetDayStatsDto>();

            var result = _executionPlan.Execute<GetDayStatsDto, StatisticsDto>(getDayStatsDto);

            var response = new GETResponseDto<StatisticsDto>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetMonthlyStatistics(int agentId)
        {
            var getMonthlyStatsDto = this.Bind<GetMonthlyStatsDto>();

            var result = _executionPlan.Execute<GetMonthlyStatsDto, StatisticsDto>(getMonthlyStatsDto);

            var response = new GETResponseDto<StatisticsDto>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}