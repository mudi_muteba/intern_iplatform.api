﻿using System.Linq;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalDefinitions.Builders;
using MasterData;
using AutoMapper;

namespace Api.Modules.Party.ProposalDefinitionQuestions
{

    //What us this Module used for and why is it here???

    public class ProposalDefinitionQuestionsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public ProposalDefinitionQuestionsModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Post[SystemRoutes.ProposalDefinitionQuestions.Search.Route] = p => GetByCriteria(p.proposalDefinitionId);
            Post[SystemRoutes.ProposalDefinitionQuestions.Save.Route] = p => SaveQuestionAnswer(p.proposalDefinitionId);
            Get[SystemRoutes.ProposalDefinitionQuestions.Get.Route] = p => GetQuestionAnswer(p.proposalDefinitionId);
            Post[SystemRoutes.ProposalDefinitionQuestions.SaveAnswer.Route] = p => SaveAnswerFromPolicyBinding(p.proposalDefinitionId);
            Options[SystemRoutes.ProposalDefinitionQuestions.Options.Route] =
            _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ProposalDefinitionQuestions));
        }

        private Negotiator GetByCriteria(int proposalDefinitionId)
        {
            var criteria = this.Bind<ProposalDefinitionQuestionsCriteria>();

            GetByIdResult<ProposalDefinitionDto> result =
                _executionPlan.GetById<ProposalDefinition, ProposalDefinitionDto>(() =>
                    _repository.GetById<ProposalDefinition>(proposalDefinitionId))
               .OnSuccess(definition => new ProposalDefinitionBuilderDto(_repository).Build(definition))
               .Execute();

            ProposalDefinitionQuestionsDto dto = result.Response.BuildByCriteriaDto(criteria);

            var response = new POSTResponseDto<ProposalDefinitionQuestionsDto>(dto, SystemRoutes.ProposalDefinitionQuestions.GetLinks(dto));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator SaveQuestionAnswer(int proposalDefinitionId)
        {
            var dto = this.Bind<SaveProposalDefinitionAnswersDto>();
            dto.Id = proposalDefinitionId;
            var result = _executionPlan.Execute<SaveProposalDefinitionAnswersDto, bool>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SaveAnswerFromPolicyBinding(int proposalDefinitionId)
        {
            var dto = this.Bind<UpdateProposalAnswerFromPolicyBindingDto>();
            var result = _executionPlan.Execute<UpdateProposalAnswerFromPolicyBindingDto, bool>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetQuestionAnswer(int proposalDefinitionId)
        {
            var result = _executionPlan
            .GetById<ProposalDefinition, ProposalDefinitionQuestionsDto>(() => _repository.GetById<ProposalDefinition>(proposalDefinitionId))
            .OnSuccess(definition => new ProposalDefinitionBuilderDto(_repository).BuildPDefinitionQuestions(definition))
            .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ProposalDefinitionQuestions.GetLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }


    // There is already a builder for this grouping 
    public static class ProposalDefinitionQuestionsExtension
    {
        //Group index is used to group questions to gather for example "Work Address VS Home address" on the frontend 
        //What is method doing? 
        public static ProposalDefinitionQuestionsDto BuildByCriteriaDto(this ProposalDefinitionDto dto,
            ProposalDefinitionQuestionsCriteria criteria)
        {
            var result = new ProposalDefinitionQuestionsDto { ProposalDefinition = dto };

            if (dto != null)  // Maybe put this in a validation class?
                foreach (var group in dto.QuestionGroups)
                {
                    var questions = group.QuestionDefinitions;

                    if (criteria.GroupIndex != null)
                        questions = questions.Where(a => a.GroupIndex == criteria.GroupIndex.Value).ToList();

                    if (criteria.GroupType != null && criteria.GroupType != 0)
                    {
                        var groupType = new QuestionDefinitionGroupTypes().First(a => a.Id == criteria.GroupType);
                        questions = questions.Where(a => a.GroupType == groupType).ToList();
                    }

                    foreach (var question in questions)
                    {
                        result.QuestionDefinitions.Add(question);
                    }
                }
            return result;
        }


    }

}