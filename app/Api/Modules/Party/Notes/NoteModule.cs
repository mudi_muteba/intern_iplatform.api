﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Note;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Note;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.Notes
{
    public class NoteModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public NoteModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.IndividualNotes.GetById.Route] = p => GetNote(p.individualId, p.Id);
            Delete[SystemRoutes.IndividualNotes.GetById.Route] = p => DeleteNote(p.individualId);

        }


        private Negotiator GetNote(int indivdualId, int id)
        {
            var result = executionPlan
                .GetById<Note, NoteDto>(() => repository.GetByPartyId<Note>(id, indivdualId))
                .OnSuccess(Mapper.Map<Note, NoteDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.IndividualNotes.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }

        private Negotiator DeleteNote(int individualId)
        {
            var deleteNoteDto = this.Bind<DeleteNoteDto>();
            deleteNoteDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<DeleteNoteDto, int>(deleteNoteDto);

            DELETEResponseDto<int> response = result.DeletePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualNotes.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

    }
}