﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Note;
using Domain.Party.Note.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Note;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.Notes
{
    public class NotesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public NotesModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Post[SystemRoutes.IndividualNotes.PostCreateNote.Route] = p => CreateNote(p.individualId);
            Get[SystemRoutes.IndividualNotes.GetNotes.Route] = p => GetNotes(p.individualId);

            Options[SystemRoutes.IndividualNotes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualNotes));
        }


        private Negotiator CreateNote(int individualId)
        {
            var createNoteDto = this.Bind<CreateNoteDto>();
            createNoteDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<CreateNoteDto, int>(createNoteDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualNotes.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetNotes(int indivdualId)
        {
            var queryResult = executionPlan
                .Query<GetNotesByPartyIdQuery, Note, ListResultDto<ListNoteDto>, ListNoteDto>()
                .Configure(q => q.WithPartyId(indivdualId))
                .OnSuccess(r => Mapper.Map<List<Note>, ListResultDto<ListNoteDto>>(r.ToList()))
                .Execute();

           // var result = new ListResultDto<ListNoteDto>() {Results = queryResult.Response};

            LISTResponseDto<ListNoteDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.IndividualNotes.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}