﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Addresses;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Lookups.Address;
using iPlatform.Api.DTOs.Party.Address;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.Addresses
{
    public class AddressModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public AddressModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.IndividualAddresses.GetById.Route] = p => GetAddress(p.individualId, p.Id);
            Delete[SystemRoutes.IndividualAddresses.GetById.Route] = p => DeleteAddress(p.individualId);
            Put[SystemRoutes.IndividualAddresses.GetById.Route] = p => UpdateAddress(p.individualId);
            Options[SystemRoutes.IndividualAddresses.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualAddresses));

        }

        private Negotiator GetAddress(int indivdualId, int id)
        {
            GetByIdResult<AddressDto> result = executionPlan
                .GetById<Address, AddressDto>(() => repository.GetByPartyId<Address>(id, indivdualId))
                .OnSuccess(Mapper.Map<Address, AddressDto>)
                .Execute();

            GETResponseDto<AddressDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.IndividualAddresses.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteAddress(int individualId)
        {
            var deleteAddressDto = this.Bind<DeleteAddressDto>();
            deleteAddressDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<DeleteAddressDto, int>(deleteAddressDto);

            DELETEResponseDto<int> response = result.DeletePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualAddresses.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateAddress(int individualId)
        {
            var editAddressDto = this.Bind<EditAddressDto>();
            editAddressDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<EditAddressDto, int>(editAddressDto);

            PUTResponseDto<int> response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.IndividualAddresses.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}