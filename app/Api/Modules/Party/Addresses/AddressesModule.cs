﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Party.Addresses;
using Domain.Party.Addresses.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Address;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.Addresses
{
    public class AddressesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;


        public AddressesModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;


            Post[SystemRoutes.IndividualAddresses.PostCreateAddress.Route] = p => CreateAddress(p.individualId);
            Get[SystemRoutes.IndividualAddresses.GetAddresses.Route] = p => GetAddresses(p.individualId);
            Options[SystemRoutes.IndividualAddresses.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualAddresses));
        }


        private Negotiator CreateAddress(int individualId)
        {
            var createAddressDto = this.Bind<CreateAddressDto>();
            createAddressDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<CreateAddressDto, int>(createAddressDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualAddresses.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAddresses(int indivdualId)
        {
            var queryResult = executionPlan
                .Query<GetAddressesByPartyIdQuery, Address, ListResultDto<ListAddressDto>, ListAddressDto>()
                .Configure(q => q.WithPartyId(indivdualId))
                .OnSuccess(r => Mapper.Map<List<Address>, ListResultDto<ListAddressDto>>(r.ToList()))
                .Execute();

            

            LISTResponseDto<ListAddressDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.IndividualAddresses.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}