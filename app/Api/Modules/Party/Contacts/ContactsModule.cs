﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Contacts.Builders;
using Domain.Party.Contacts.Queries;
using Domain.Party.Relationships;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Contacts;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using AutoMapper;

namespace Api.Modules.Party.Contacts
{
    public class ContactsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ContactsModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Post[SystemRoutes.IndividualContacts.PostCreateContact.Route] = p => CreateContact(p.individualId);
            Get[SystemRoutes.IndividualContacts.GetContacts.Route] = p => GetContacts(p.individualId);

            Options[SystemRoutes.IndividualContacts.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualContacts));
        }
        
        private Negotiator CreateContact(int individualId)
        {
            var createContactDto = this.Bind<CreateContactDto>();
            createContactDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<CreateContactDto, int>(createContactDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualContacts.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetContacts(int indivdualId)
        {
            var queryResult = executionPlan
                .Query<GetRelationshipByPartyIdQuery, Relationship, ListResultDto<ListContactDto>,ListContactDto>()
                .Configure(q => q.WithPartyId(indivdualId))
                .OnSuccess(relationship => new ListResultDto<ListContactDto>
                {
                    Results = new ContactListBuilder().Build(relationship.ToList())
                })
                .Execute();

            LISTResponseDto<ListContactDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.IndividualContacts.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}