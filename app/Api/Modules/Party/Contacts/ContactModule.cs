﻿using System.Linq;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Contacts;
using Domain.Party.Contacts.Builders;
using Domain.Party.Contacts.Queries;
using Domain.Party.Relationships;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Contacts;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using AutoMapper;
using Domain.Party.Relationships.Queries;
using iPlatform.Api.DTOs.Party.Relationships;

namespace Api.Modules.Party.Contacts
{
    public class ContactModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public ContactModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.IndividualContacts.GetById.Route] = p => GetContact(p.individualId, p.Id);
            Delete[SystemRoutes.IndividualContacts.GetById.Route] = p => DeleteContact(p.individualId, p.Id);
            Put[SystemRoutes.IndividualContacts.GetById.Route] = p => UpdateContact(p.individualId, p.Id);
        }


        private Negotiator GetContact(int individualId, int contactId)
        {
            var queryResult = new QueryResult<ContactDto>();

            //Get the contact
            if (individualId > 0)
            {
                queryResult = executionPlan
                    .Query<GetContactByPartyIdQuery, Contact, ContactDto, ContactDto>()
                    .Configure(q => q.WithContactId(contactId).WithPartyId(individualId))
                    .OnSuccess(r => Mapper.Map<ContactDto>(r.First()))
                    .Execute();
            }
            else
            {
                queryResult = executionPlan
                    .Query<GetContactByPartyIdQuery, Contact, ContactDto, ContactDto>()
                    .Configure(q => q.WithContactId(contactId))
                    .OnSuccess(r => Mapper.Map<ContactDto>(r.First()))
                    .Execute();
            }

            //Get the contact's relationship to party
            var relationshiptype = executionPlan
                .Query<GetExistingRelationshipQuery, Relationship, RelationshipDto>()
                .Configure(q => q.WithChildPartyId(contactId).WithPartyId(individualId))
                .OnSuccess(r => Mapper.Map<RelationshipDto>(r.First()))
                .Execute();

            queryResult.Response.RelationshipType = relationshiptype.Response.RelationshipType;

            var result = new GETResponseDto<ContactDto> {Response = queryResult.Response};

            GETResponseDto<ContactDto> response = result.CreateLinks(c => SystemRoutes.IndividualContacts.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteContact(int individualId, int id)
        {
            var deleteContactDto = new DeleteContactDto
            {
                Id = id,
            };

            HandlerResult<int> result = executionPlan.Execute<DeleteContactDto, int>(deleteContactDto);

            DELETEResponseDto<int> response = result.DeletePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualContacts.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateContact(int individualId, int id)
        {
            var editContactDto = this.Bind<EditContactDto>();
            editContactDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<EditContactDto, int>(editContactDto);

            PUTResponseDto<int> response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.IndividualContacts.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}