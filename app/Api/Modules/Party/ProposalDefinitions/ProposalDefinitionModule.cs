﻿using System.Collections.Generic;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalDefinitions.Builders;
using Domain.Party.ProposalDefinitions.Queries;
using Domain.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.ProposalDefinitions
{
    public class ProposalDefinitionModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public ProposalDefinitionModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.ProposalDefinitions.GetById.Route] =
                p => GetProposalDefinition(p.proposalHeaderId, p.Id);

            Delete[SystemRoutes.ProposalDefinitions.Delete.Route] =
                p => DeleteProposalDefinition(p.proposalHeaderId, p.Id);

            Put[SystemRoutes.ProposalDefinitions.Put.Route] =
                p => EditProposalDefinition(p.proposalHeaderId, p.Id);


            Options[SystemRoutes.ProposalDefinitions.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ProposalDefinitions));
        }

        private Negotiator GetProposalDefinition(int proposalHeaderId, int id) 
        {
            GetByIdResult<ProposalDefinitionDto> result = executionPlan
                .GetById<ProposalDefinition, ProposalDefinitionDto>(
                    () => repository.GetById<ProposalDefinition>(id))
                .OnSuccess(definition => new ProposalDefinitionBuilderDto(repository).Build(definition))
                .Execute();

            GETResponseDto<ProposalDefinitionDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ProposalDefinitions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteProposalDefinition(int proposalHeaderId, int id)
        {
            var deleteProposalDefinitionDto = this.Bind<DeleteProposalDefinitionDto>();
            deleteProposalDefinitionDto.Id = id;
            deleteProposalDefinitionDto.ProposalHeaderId = proposalHeaderId;

            HandlerResult<DeleteProposalDefinitionResponseDto> result =
                executionPlan.Execute<DeleteProposalDefinitionDto, DeleteProposalDefinitionResponseDto>(
                    deleteProposalDefinitionDto);

            DELETEResponseDto<DeleteProposalDefinitionResponseDto> response = result.DeletePOSTResponse();
            response.CreateLinks(SystemRoutes.IndividualProposalHeaders.CreateLinks(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator EditProposalDefinition(int proposalHeaderId, int id)
        {
            var editProposalDefinitionDto = this.Bind<EditProposalDefinitionDto>();

            editProposalDefinitionDto.ProposalHeaderId = proposalHeaderId;
            editProposalDefinitionDto.Id = id;

            HandlerResult<ProposalDefinitionDto> result =
                executionPlan.Execute<EditProposalDefinitionDto, ProposalDefinitionDto>(editProposalDefinitionDto);

            PUTResponseDto<ProposalDefinitionDto> response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ProposalDefinitions.CreateGetById(proposalHeaderId,
                    editProposalDefinitionDto.Id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}