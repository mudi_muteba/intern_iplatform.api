﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalDefinitions.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.ProposalDefinitions
{
    public class ProposalDefinitionsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ProposalDefinitionsModule(IExecutionPlan executionPlan, IRepository repository,
            IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Post[SystemRoutes.ProposalDefinitions.PostCreateProposalDefinition.Route] =
                p => CreateProposalDefinition(p.proposalHeaderId);

            Get[SystemRoutes.ProposalDefinitions.GetProposalDefinitions.Route] =
                p => GetProposalDefinitions(p.proposalHeaderId);

            Options[SystemRoutes.ProposalDefinitions.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ProposalDefinitions));
        }


        private Negotiator CreateProposalDefinition(int proposalHeaderId)
        {
            var createProposalDefinitionDto = this.Bind<CreateProposalDefinitionDto>();
            createProposalDefinitionDto.ProposalHeaderId = proposalHeaderId;

            HandlerResult<int> result =
                executionPlan.Execute<CreateProposalDefinitionDto, int>(createProposalDefinitionDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ProposalDefinitions.CreateGetById(proposalHeaderId,
                    result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetProposalDefinitions(int proposalHeaderId)
        {
            var queryResult = executionPlan
                .Query<GetProposalDefinitionsByHeaderIdQuery, ProposalDefinition, ListResultDto<ListProposalDefinitionDto>, ListProposalDefinitionDto>()
                .Configure(q => q.WithHeaderId(proposalHeaderId))
                .OnSuccess(r => Mapper.Map<List<ProposalDefinition>, ListResultDto<ListProposalDefinitionDto>>(r.ToList()))
                .Execute();

            //var result = new ListResultDto<ListProposalDefinitionDto> {Results = queryResult.Response};

            LISTResponseDto<ListProposalDefinitionDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.ProposalDefinitions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}