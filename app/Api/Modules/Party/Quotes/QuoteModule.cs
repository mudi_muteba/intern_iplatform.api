﻿using System;
using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Party.Quotes;
using Domain.Party.Quotes.Queries;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Quotes;

namespace Api.Modules.Party.Quotes
{
    public class QuoteModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public QuoteModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.Quotes.GetByExternalReference.Route] = p => GetQuoteByExternalReference(p.ExternalReference);

            Post[SystemRoutes.Quotes.DistributeQuoteUsingExternalReference.Route] = _ => DistributeQuote();

            Put[SystemRoutes.Quotes.TrackQuoteDistribution.Route] = _ => TrackDistribution();

            Post[SystemRoutes.Quotes.AcceptQuote.Route] = p => AcceptQuoteById(p.quoteId);
            Post[SystemRoutes.Quotes.IntentToBuyQuote.Route] = p => IntentToBuy(p.quoteId);

            Post[SystemRoutes.Quotes.LogQuoteUploadSuccess.Route] = p => LogQuoteUploadSuccess(p.quoteId);
            Get[SystemRoutes.Quotes.GetQuoteUploadLog.Route] = p => GetQuoteUploadLog(p.quoteId);

            Post[SystemRoutes.Quotes.DeleteQuoteAccept.Route] = p => DeleteQuoteAccept(p.quoteId);
            Post[SystemRoutes.Quotes.DeleteQuoteAccept.Route] = p => DeletePolicyBindingCompleted();
            
        }

        private Negotiator IntentToBuy(int quoteId)
        {
            var dto = this.Bind<IntentToBuyQuoteDto>();
            dto.Id = quoteId;

            var result = executionPlan.Execute<IntentToBuyQuoteDto, int>(dto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Quotes.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator AcceptQuoteById(int quoteId)
        {
            var dto = this.Bind<AcceptQuoteDto>();

            var result = executionPlan.Execute<AcceptQuoteDto, QuoteAcceptanceResponseDto>(dto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Quotes.CreateGetById(quoteId));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator DeleteQuoteAccept(int quoteId)
        {
            var dto = new DeleteQuoteAcceptDto(quoteId);

            var result = executionPlan.Execute<DeleteQuoteAcceptDto, bool>(dto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Quotes.CreateGetById(quoteId));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator DeletePolicyBindingCompleted()
        {
            var dto = this.Bind<DeletePolicyBindingCompletedDto>();

            var result = executionPlan.Execute<DeletePolicyBindingCompletedDto, bool>(dto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Quotes.CreateGetById(dto.Id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator TrackDistribution()
        {
            var dto = this.Bind<iPlatform.Api.DTOs.Party.Quotes.TrackQuoteDistributionDto>();

            var result = executionPlan.Execute<iPlatform.Api.DTOs.Party.Quotes.TrackQuoteDistributionDto, int>(dto);

            var response = result.CreatePUTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DistributeQuote()
        {
            var dto = this.Bind<DistributeQuoteDto>();

            var result = executionPlan.Execute<DistributeQuoteDto, Guid>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetQuoteByExternalReference(string externalReference)
        {
            var queryResult = executionPlan
                .Query<GetQuoteByExternalReference, QuoteHeader, QuoteHeaderDto, QuoteHeaderDto>()
                .Configure(q => q.WithExternalReference(externalReference))
                .OnSuccess(r => Mapper.Map<QuoteHeader, QuoteHeaderDto>(r.FirstOrDefault()))
                .Execute();

            var result = new GetByIdResult<QuoteHeaderDto>(queryResult.Response);

            var response = result.CreateGETResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator LogQuoteUploadSuccess(int quoteid)
        {
            var dto = this.Bind<CreateQuoteUploadLogDto>();

            var result = executionPlan.Execute<CreateQuoteUploadLogDto, int>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetQuoteUploadLog(int quoteid)
        {
            var queryResult = executionPlan
                .Query<GetQuoteUploadLogByQuoteIdQuery, QuoteUploadLog, QuoteUploadLogDto, QuoteUploadLogDto>()
                .Configure(q => q.WithQuoteId(quoteid))
                .OnSuccess(r => Mapper.Map<QuoteUploadLog, QuoteUploadLogDto>(r.FirstOrDefault()))
                .Execute();

            var result = new GetByIdResult<QuoteUploadLogDto>(queryResult.Response);

            var response = result.CreateGETResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}