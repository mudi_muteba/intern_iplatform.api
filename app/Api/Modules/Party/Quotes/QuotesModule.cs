﻿using System;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Response;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.Quotes
{
    public class QuotesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public QuotesModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Post[SystemRoutes.Quotes.Accept.Route] = _ => AcceptQuote();

            Post[SystemRoutes.Quotes.Create.Route] = _ => CreateQuote();

            Post[SystemRoutes.Quotes.AcceptExternalQuote.Route] = _ => ExternalQuoteAcceptance();

            Options[SystemRoutes.Quotes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Quotes));

        }

        private Negotiator CreateQuote()
        {
            var ratingResult = this.Bind<RatingResultDto>();

            var result = executionPlan.Execute<RatingResultDto, int>(ratingResult);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator AcceptQuote()
        {
            var createDto = this.Bind<PublishQuoteUploadMessageDto>();

            var result = executionPlan.Execute<PublishQuoteUploadMessageDto, QuoteAcceptanceResponseDto> (createDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Quotes.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator ExternalQuoteAcceptance()
        {
            var ratingResult = this.Bind<RatingResultDto>();
            
            var result = executionPlan.Execute<RatingResultDto, bool>(ratingResult);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}