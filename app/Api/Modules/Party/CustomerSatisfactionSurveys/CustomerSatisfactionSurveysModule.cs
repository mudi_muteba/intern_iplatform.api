﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.CorrespondencePreferences;
using Domain.Party.CorrespondencePreferences.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.CustomerSatisfactionSurveys
{
    public class CustomerSatisfactionSurveysModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public CustomerSatisfactionSurveysModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;

            Post[SystemRoutes.CustomerSatisfactionSurvey.Save.Route] = p => Save();
            Post[SystemRoutes.CustomerSatisfactionSurvey.Post.Route] = p => PostToLightStone();
        }

        private Negotiator Save()
        {
            var dto = this.Bind<CreateCustomerSatisfactionSurveyDto>();

            HandlerResult<int> result = executionPlan.Execute<CreateCustomerSatisfactionSurveyDto, int>(dto);

            POSTResponseDto<int> response = result.CreatePOSTResponse();
                //.CreateLinks(SystemRoutes.CustomerSatisfactionSurvey.CreateGetById(result.Response));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        //this is used only for testing lightstone api
        private Negotiator PostToLightStone()
        {
            var dto = this.Bind<CustomerSatisfactionSurveyRequestDto>();

            HandlerResult<bool> result = executionPlan.Execute<CustomerSatisfactionSurveyRequestDto, bool>(dto);

            POSTResponseDto<bool> response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}