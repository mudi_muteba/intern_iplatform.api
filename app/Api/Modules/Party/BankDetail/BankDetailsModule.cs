﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Bank;
using Domain.Party.Bank.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Bank;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.BankDetail
{
    public class BankDetailsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public BankDetailsModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Post[SystemRoutes.IndividualBankDetails.PostCreateBankDetail.Route] = p => CreateBankDetails(p.individualId);
            Get[SystemRoutes.IndividualBankDetails.GetBankDetails.Route] = p => GetBankDetails(p.individualId);

            Options[SystemRoutes.IndividualBankDetails.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualBankDetails));
        }


        private Negotiator CreateBankDetails(int individualId)
        {
            var createBankDetailsDto = this.Bind<CreateBankDetailsDto>();
            createBankDetailsDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<CreateBankDetailsDto, int>(createBankDetailsDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualBankDetails.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetBankDetails(int indivdualId)
        {
            var queryResult = executionPlan
                .Query<GetBankDetailsByPartyIdQuery, BankDetails, ListResultDto<ListBankDetailsDto>, ListBankDetailsDto>()
                .Configure(q => q.WithPartyId(indivdualId))
                .OnSuccess(r => Mapper.Map<List<BankDetails>, ListResultDto<ListBankDetailsDto>>(r.ToList()))
                .Execute();

            LISTResponseDto<ListBankDetailsDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.IndividualBankDetails.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}