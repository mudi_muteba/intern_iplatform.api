﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Bank;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Bank;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.BankDetail
{
    public class BankDetailModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public BankDetailModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.IndividualBankDetails.GetById.Route] = p => GetBankDetail(p.individualId, p.Id);
            Delete[SystemRoutes.IndividualBankDetails.GetById.Route] = p => DeleteBankDetail(p.individualId);
            Put[SystemRoutes.IndividualBankDetails.GetById.Route] = p => UpdateBankDetails(p.individualId);

        }


        private Negotiator GetBankDetail(int indivdualId, int id)
        {
            GetByIdResult<BankDetailsDto> result = executionPlan
                .GetById<BankDetails, BankDetailsDto>(() => repository.GetByPartyId<BankDetails>(id, indivdualId))
                .OnSuccess(Mapper.Map<BankDetails, BankDetailsDto>)
                .Execute();

            GETResponseDto<BankDetailsDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.IndividualBankDetails.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteBankDetail(int individualId)
        {
            var deleteBankDetailDto = this.Bind<DeleteBankDetailsDto>();
            deleteBankDetailDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<DeleteBankDetailsDto, int>(deleteBankDetailDto);

            DELETEResponseDto<int> response = result.DeletePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualBankDetails.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateBankDetails(int individualId)
        {
            var editBankDetailsDto = this.Bind<EditBankDetailsDto>();
            editBankDetailsDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<EditBankDetailsDto, int>(editBankDetailsDto);

            PUTResponseDto<int> response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.IndividualAddresses.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}