﻿using System.Linq;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.ProposalHeaders;
using Domain.Party.ProposalHeaders.Builders;
using Domain.Party.ProposalHeaders.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.ProposalHeaders
{
    public class ProposalHeadersModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ProposalHeadersModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Post[SystemRoutes.IndividualProposalHeaders.PostCreateProposalHeader.Route] = p => CreateProposalHeader(p.individualId);
            Get[SystemRoutes.IndividualProposalHeaders.GetProposalHeaders.Route] = p => GetProposalHeaders(p.individualId);

            Options[SystemRoutes.IndividualProposalHeaders.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualProposalHeaders));
        }

        private Negotiator CreateProposalHeader(int individualId)
        {
            var createProposalHeaderDto = this.Bind<CreateProposalHeaderDto>();
            createProposalHeaderDto.PartyId = individualId;

            var result = executionPlan.Execute<CreateProposalHeaderDto, int>(createProposalHeaderDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualProposalHeaders.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetProposalHeaders(int indivdualId)
        {
            var queryResult = executionPlan
                .Query<GetProposalHeadersByPartyIdQuery, ProposalHeader, ListResultDto<ListProposalHeaderDto>, ListProposalHeaderDto>()
                .Configure(q => q.WithPartyId(indivdualId))
                .OnSuccess(result => new ListResultDto<ListProposalHeaderDto>
                {
                    Results = result.ToList().Select(la => la.BuildList(repository)).ToList()
                })
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.IndividualProposalHeaders.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}