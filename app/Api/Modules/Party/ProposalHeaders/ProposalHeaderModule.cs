﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using Domain.Party.ProposalDefinitions.Queries;
using Domain.Party.ProposalHeaders;
using Domain.Party.ProposalHeaders.Builders;
using Domain.Party.ProposalHeaders.Queries;
using Domain.Party.Quotes;
using Domain.Party.Quotes.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Newtonsoft.Json;

namespace Api.Modules.Party.ProposalHeaders
{
    public class ProposalHeaderModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public ProposalHeaderModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.IndividualProposalHeaders.GetById.Route] = p => GetProposalHeader(p.IndividualId, p.Id);
            Get[SystemRoutes.IndividualProposalHeaders.GetDetailedById.Route] = p => GetProposalHeaderDetailed(p.IndividualId, p.Id);

            Get[SystemRoutes.IndividualProposalHeaders.GetByExternalReference.Route] = p => GetProposalHeader(p.ExternalReference);
            Post[SystemRoutes.IndividualProposalHeaders.GetContainsVehilceType.Route] = p => GetVehicleTypes(p.IndividualId, p.Id);
            Delete[SystemRoutes.IndividualProposalHeaders.GetById.Route] = p => DeleteProposalHeader(p.IndividualId);

            Options[SystemRoutes.IndividualProposalHeaders.Options.Route] =
              _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualProposalHeaders));
        }

        private Negotiator GetProposalHeader(string externalReference)
        {
            var queryResult = _executionPlan
                .Query<GetProposalHeaderByExternalReferenceQuery, ProposalHeader, ProposalHeaderDto, ProposalHeaderDto>()
                .Configure(q => q.WithExternalReference(externalReference))
                .OnSuccess(r => r.FirstOrDefault().Build(_repository))
                .Execute();

            var result = new GetByIdResult<ProposalHeaderDto>(queryResult.Response);

            var response = result.CreateGETResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetProposalHeader(int indivdualId, int id)
        {
            GetByIdResult<ProposalHeaderDto> result = _executionPlan
                .GetById<ProposalHeader, ProposalHeaderDto>(() => _repository.GetByPartyId<ProposalHeader>(id, indivdualId))
                .OnSuccess(r => r.Build(_repository))
                .Execute();

            GETResponseDto<ProposalHeaderDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.IndividualProposalHeaders.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteProposalHeader(int partyId)
        {
            var deleteProposalHeaderDto = this.Bind<DeleteProposalHeaderDto>();
            deleteProposalHeaderDto.PartyId = partyId;

            HandlerResult<int> result = _executionPlan.Execute<DeleteProposalHeaderDto, int>(deleteProposalHeaderDto);

            DELETEResponseDto<int> response = result.DeletePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualProposalHeaders.CreateGetById(partyId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetProposalHeaderDetailed(int indivdualId, int id)
        {
            GetByIdResult<ProposalHeaderDetailedDto> result = _executionPlan
                .GetById<ProposalHeader, ProposalHeaderDetailedDto>(() => _repository.GetByPartyId<ProposalHeader>(id, indivdualId))
                .OnSuccess(r => r.BuildDetailed(_repository))
                .Execute();


            GETResponseDto<ProposalHeaderDetailedDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.IndividualProposalHeaders.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetVehicleTypes(int indivdualId, int proposalHeaderId)
        {

            var proposalQuestionAnswerDto = this.Bind<ContainsVehicleTypesDto>();

           QueryResult<int> queryResult = _executionPlan
               .Query<GetVehicleTypesQuery, ProposalQuestionAnswer, int>()
               .Configure(q => q.WithCriteria(proposalQuestionAnswerDto))
               .OnSuccess(r => r.Count())
               .Execute();

            var response = queryResult.CreateGETResponse()
                 .CreateLinks(c => SystemRoutes.IndividualProposalHeaders.CreateLinks(indivdualId, proposalHeaderId));
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}