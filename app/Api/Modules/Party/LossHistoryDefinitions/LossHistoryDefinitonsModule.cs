﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.LossHistoryDefinitions;
using Domain.Party.LossHistoryDefinitions.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.LossHistoryDefinitions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;


namespace Api.Modules.Party.LossHistoryDefinitions
{
    public class LossHistoryDefinitonsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;
        private readonly IRepository _repository;

        public LossHistoryDefinitonsModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository)
        {
            this._executionPlan = executionPlan;
            this._paginator = paginator;
            this._repository = repository;

            Options[SystemRoutes.LossHistoryDefinitions.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.LossHistoryDefinitions));

            Post[SystemRoutes.LossHistoryDefinitions.Post.Route] = p => SaveLossHistory();
        }

        private Negotiator SaveLossHistory()
        {
            var createLossHistoryDefinitionDto = this.Bind<CreateLossHistoryDefinitionDto>();

            var result = _executionPlan.Execute<CreateLossHistoryDefinitionDto, int>(createLossHistoryDefinitionDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.LossHistoryDefinitions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}