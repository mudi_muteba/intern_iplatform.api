﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.LossHistoryDefinitions;
using Domain.Party.LossHistoryDefinitions.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.LossHistoryDefinitions;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.LossHistoryDefinitions
{
    public class LossHistoryDefinitionModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public LossHistoryDefinitionModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;
            Get[SystemRoutes.LossHistoryDefinitions.GetByPartyId.Route] = p => GetByPartyId(p.id);
        }

        private Negotiator GetByPartyId(int partyId)
        {
            var queryResult = _executionPlan
                .Query
                <GetLossHistoryDefinitionByPartyId, LossHistoryDefinition, ListResultDto<LossHistoryDefinitionDto>,
                    LossHistoryDefinitionDto>()
                .Configure(q => q.WithPartyId(partyId))
                .OnSuccess(r => Mapper.Map<List<LossHistoryDefinition>, ListResultDto<LossHistoryDefinitionDto>>(r.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.LossHistoryDefinitions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}