﻿using Api.Models;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Relationships;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using Nancy.ModelBinding;
using Domain.Party.Relationships;
using AutoMapper;
using Domain.Party.Relationships.Queries;
using iPlatform.Api.DTOs.Base;

namespace Api.Modules.Party.Relationships
{
    public class RelationshipModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;


        public RelationshipModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan
            , IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Post[SystemRoutes.Relationships.Post.Route] = p => CreateRelationship();
            Get[SystemRoutes.Relationships.GetById.Route] = p => GetRelationship(p.id);
            Get[SystemRoutes.Relationships.GetByPartyId.Route] = p => GetRelationshipByPartyId(p.id);
            Put[SystemRoutes.Relationships.PutDeleteByPartyId.Route] = p => DeleteRelationship(p.id);
            Put[SystemRoutes.Relationships.PutUpdate.Route] = p => UpdateRelationship(p.id);

            Options[SystemRoutes.Relationships.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Relationships));
        }


        private Negotiator CreateRelationship()
        {
            var createRelationshipDto = this.Bind<CreateRelationshipDto>();

            var result = executionPlan.Execute<CreateRelationshipDto, int>(createRelationshipDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Relationships.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator UpdateRelationship(int id)
        {
            var updateRelationshipDto = this.Bind<EditRelationshipDto>();

            var result = executionPlan.Execute<EditRelationshipDto, int>(updateRelationshipDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Relationships.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetRelationship(int id)
        {
            var value = repository.GetById<Relationship>(id);
            var result = executionPlan
                .GetById<Relationship, RelationshipDto>(() => repository.GetById<Relationship>(id))
                .OnSuccess(Mapper.Map<Relationship, RelationshipDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Relationships.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetRelationshipByPartyId(int partyId)
        {
            var queryResult = executionPlan
                .Query<GetRelationshipByPartyIdQuery, Relationship, ListResultDto<RelationshipDto>, RelationshipDto>()
                .Configure(q => q.WithPartyId(partyId))
                .OnSuccess(result => Mapper.Map<ListResultDto<RelationshipDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Relationships.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteRelationship(int id)
        {
            var deletedto = this.Bind<DeleteRelationshipDto>();

            var result = executionPlan.Execute<DeleteRelationshipDto, int>(deletedto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Relationships.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}