﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.HistoryLoss.LossHistory.Queries;
using Domain.Party.LossHistoryQuestionDefinitions;
using Domain.Party.LossHistoryQuestionDefinitions.Builder;
using Domain.Party.LossHistoryQuestionDefinitions.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.LossHistoryQuestionDefinitions
{
    public class LossHistoryQuestionsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public LossHistoryQuestionsModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this._executionPlan = executionPlan;
            this._repository = repository;

            Options[SystemRoutes.LossHistoryQuestionDefinition.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.LossHistoryQuestionDefinition));

            Post[SystemRoutes.LossHistoryQuestionDefinition.PostSearchQuestions.Route] = p => Search();
            Post[SystemRoutes.LossHistoryQuestionDefinition.PostSearchQuestionWithAnswers.Route] = p => SearchWithAnswers();
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<LossHistoryQuestionDefinitionSearchDto>();

            var queryResult = _executionPlan
                .Search<SearchQuestionDefinitionsByCriteriaQuery, 
                            LossHistoryQuestionDefinition, 
                            LossHistoryQuestionDefinitionSearchDto, 
                            ListResultDto<LossHistoryQuestionDefinition>>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(r => Mapper.Map<List<LossHistoryQuestionDefinition>, ListResultDto<LossHistoryQuestionDefinition>>(r.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchWithAnswers()
        {
            var criteria = this.Bind<LossHistoryQuestionDefinitionSearchDto>();

            var searchResult = _executionPlan
                .Search<SearchQuestionDefinitionsByCriteriaQuery,
                            LossHistoryQuestionDefinition,
                            LossHistoryQuestionDefinitionSearchDto,
                            ListResultDto<LossHistoryQuestionDefinition>>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(r => Mapper.Map<List<LossHistoryQuestionDefinition>, ListResultDto<LossHistoryQuestionDefinition>>(r.ToList()))
                .Execute();

            var queryResult = new LossHistoryQuestionsOptionBuilder(_repository).BuilDefinitionAnswersDtos(searchResult.Response.Results);
            
            var response = searchResult.CreateLISTResponse(queryResult);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }
    }
}