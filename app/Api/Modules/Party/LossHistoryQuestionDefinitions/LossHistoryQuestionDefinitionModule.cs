﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.LossHistoryQuestionDefinitions;
using Domain.Party.LossHistoryQuestionDefinitions.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.LossHistoryQuestionDefinitions
{
    public class LossHistoryQuestionModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public LossHistoryQuestionModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Options[SystemRoutes.LossHistoryQuestionDefinition.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.LossHistoryQuestionDefinition));

            Get[SystemRoutes.LossHistoryQuestionDefinition.GetById.Route] = p => GetById(p.id);
            Post[SystemRoutes.LossHistoryQuestionDefinition.PostSearchQuestion.Route] = p => Search();
        }

        public Negotiator GetById(int id)
        {
            var result = _executionPlan
                .GetById<LossHistoryQuestionDefinition, LossHistoryQuestionDefinitionDto>(() => _repository.GetById<LossHistoryQuestionDefinition>(id))
                .OnSuccess(Mapper.Map<LossHistoryQuestionDefinition, LossHistoryQuestionDefinitionDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(x => SystemRoutes.LossHistoryQuestionDefinition.CreateLinks(x))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<LossHistoryQuestionDefinitionSearchDto>();

            var queryResult = _executionPlan
                .Search
                <SearchQuestionDefinitionsByCriteriaQuery, LossHistoryQuestionDefinition, LossHistoryQuestionDefinitionSearchDto, ListResultDto<LossHistoryQuestionDefinition>>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(r => Mapper.Map<List<LossHistoryQuestionDefinition>, ListResultDto<LossHistoryQuestionDefinition>>(r.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}