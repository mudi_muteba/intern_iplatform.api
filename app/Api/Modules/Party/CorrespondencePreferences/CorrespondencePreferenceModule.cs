﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.CorrespondencePreferences
{
    public class CorrespondencePreferenceModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public CorrespondencePreferenceModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.CorrespondencePreferences.GetById.Route] = p => GetCorrespondencePreference(p.individualId, p.Id);
            //Delete[SystemRoutes.CorrespondencePreferences.GetById.Route] = p => DeleteCorrespondencePreference(p.individualId, p.Id);
            //Put[SystemRoutes.CorrespondencePreferences.GetById.Route] = p => UpdateCorrespondencePreference(p.individualId, p.Id);

        }


        private Negotiator GetCorrespondencePreference(int indivdualId, int id)
        {
            GetByIdResult<PartyCorrespondencePreferenceDto> result = executionPlan
                .GetById<PartyCorrespondencePreference, PartyCorrespondencePreferenceDto>(() => repository.GetById<PartyCorrespondencePreference>(id))
                .OnSuccess(Mapper.Map<PartyCorrespondencePreference, PartyCorrespondencePreferenceDto>)
                .Execute();

            GETResponseDto<PartyCorrespondencePreferenceDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.CorrespondencePreferences.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        //private Negotiator DeleteCorrespondencePreference(int individualId, int id)
        //{
        //    var deleteBankDetailDto = this.Bind<DeleteCorrespondencePreferenceDto>();
        //    deleteBankDetailDto.PartyId = individualId;

        //    HandlerResult<int> result = executionPlan.Execute<DeleteCorrespondencePreferenceDto, int>(deleteBankDetailDto);

        //    DELETEResponseDto<int> response = result.DeletePOSTResponse()
        //        .CreateLinks(SystemRoutes.CorrespondencePreferences.CreateGetById(individualId, id));

        //    return Negotiate
        //        .WithLocationHeader(response.Link)
        //        .ForSupportedFormats(response, response.StatusCode);
        //}

        //private Negotiator UpdateCorrespondencePreference(int individualId, int id)
        //{
        //    var editCorrespondencePrefDto = this.Bind<EditCorrespondencePreferenceDto>();
        //    editCorrespondencePrefDto.PartyId = individualId;

        //    HandlerResult<int> result = executionPlan.Execute<EditCorrespondencePreferenceDto, int>(editCorrespondencePrefDto);

        //    PUTResponseDto<int> response = result.CreatePUTResponse()
        //        .CreateLinks(SystemRoutes.CorrespondencePreferences.CreateGetById(individualId, id));

        //    return Negotiate
        //        .WithLocationHeader(response.Link)
        //        .ForSupportedFormats(response, response.StatusCode);
        //}
    }
}