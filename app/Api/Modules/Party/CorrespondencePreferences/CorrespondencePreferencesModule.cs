﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Bank;
using Domain.Party.Bank.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Bank;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using Domain.Party.CorrespondencePreferences.Queries;
using Domain.Party.CorrespondencePreferences;

namespace Api.Modules.Party.CorrespondencePreferences
{
    public class CorrespondencePreferencesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public CorrespondencePreferencesModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Post[SystemRoutes.CorrespondencePreferences.PostCorrespondencePreference.Route] = p => EditCorrespondencePreferences(p.individualId);
            Get[SystemRoutes.CorrespondencePreferences.Get.Route] = p => GetCorrespondencePreferences(p.individualId);

            Options[SystemRoutes.CorrespondencePreferences.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.CorrespondencePreferences));
        }

        private Negotiator EditCorrespondencePreferences(int individualId)
        {
            var createPDDto = this.Bind<EditCorrespondencePreferenceDto>();
            createPDDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<EditCorrespondencePreferenceDto, int>(createPDDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.CorrespondencePreferences.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetCorrespondencePreferences(int indivdualId)
        {
            var queryResult = executionPlan
                .Query<GetCorrespondencePrefByPartyIdQuery, PartyCorrespondencePreference, ListResultDto<PartyCorrespondencePreferenceDto>, PartyCorrespondencePreferenceDto>()
                .Configure(q => q.WithPartyId(indivdualId))
                .OnSuccess(r => 
                    {
                        var result = Mapper.Map<List<PartyCorrespondencePreference>, ListResultDto<PartyCorrespondencePreferenceDto>>(r.ToList());
                        return result;
                    })
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.CorrespondencePreferences.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}