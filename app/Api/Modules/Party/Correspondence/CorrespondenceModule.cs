﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;
using Api.Models;

using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Correspondence;
using Domain.Party.Correspondence.Queries;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Correspondence;

namespace Api.Modules.Party.CorrespondencePreferences
{
    public class CorrespondenceModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public CorrespondenceModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Post[SystemRoutes.IndividualCorrespondence.PostCorrespondence.Route] = p => CreateCorrespondence(p.individualId);
            Get[SystemRoutes.IndividualCorrespondence.Get.Route] = p => GetCorrespondence(p.individualId);

            Options[SystemRoutes.IndividualCorrespondence.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualCorrespondence));
        }

        private Negotiator CreateCorrespondence(int individualId)
        {
            var createCorrespondenceDto = this.Bind<CreatePartyCorrespondenceDto>();
                createCorrespondenceDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<CreatePartyCorrespondenceDto, int>(createCorrespondenceDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualCorrespondence.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetCorrespondence(int individualId)
        {
            var queryResult = executionPlan
                .Query<GetCorrespondenceByPartyIdQuery, PartyCorrespondence, ListResultDto<PartyCorrespondenceDto>, PartyCorrespondenceDto>()
                .Configure(q => q.WithPartyId(individualId))
                .OnSuccess(r => 
                    {
                        var result = Mapper.Map<List<PartyCorrespondence>, ListResultDto<PartyCorrespondenceDto>>(r.ToList());
                        return result;
                    })
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.IndividualCorrespondence.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}