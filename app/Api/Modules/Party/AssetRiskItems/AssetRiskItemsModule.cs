﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Assets.AssetRiskItems;
using Domain.Party.Assets.AssetRiskItems.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.AssetRiskItems
{
    public class AssetRiskItemsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public AssetRiskItemsModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Post[SystemRoutes.IndividualAssetRiskItem.PostCreateAssetRiskItem.Route] =
                p => CreateAssetRiskItems(p.individualId);
            Get[SystemRoutes.IndividualAssetRiskItem.GetAssetRiskItems.Route] = p => GetAssetRiskItems(p.individualId);
            Options[SystemRoutes.IndividualAssetRiskItem.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualAssetRiskItem));
        }


        private Negotiator CreateAssetRiskItems(int individualId)
        {
            var createAssetRiskItemDto = this.Bind<CreateAssetRiskItemDto>();
            createAssetRiskItemDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<CreateAssetRiskItemDto, int>(createAssetRiskItemDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualAssetRiskItem.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAssetRiskItems(int indivdualId)
        {
            var queryResult = executionPlan
                .Query<GetAssetRiskItemByPartyIdQuery, AssetRiskItem, ListResultDto<AssetRiskItemDto>, AssetRiskItemDto>()
                .Configure(q => q.WithPartyId(indivdualId))
                .OnSuccess(r => Mapper.Map<List<AssetRiskItem>, ListResultDto<AssetRiskItemDto>>(r.ToList()))
                .Execute();

            LISTResponseDto<AssetRiskItemDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.IndividualAssetRiskItem.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}