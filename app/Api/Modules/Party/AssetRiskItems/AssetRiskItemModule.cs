﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Assets.AssetRiskItems;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.AssetRiskItems
{
    public class AssetRiskItemModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public AssetRiskItemModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.IndividualAssetRiskItem.GetById.Route] = p => GetAssetRiskItem(p.individualId, p.Id);
            Delete[SystemRoutes.IndividualAssetRiskItem.GetById.Route] = p => DeleteAssetRiskItem(p.individualId);
            Put[SystemRoutes.IndividualAssetRiskItem.GetById.Route] = p => UpdateAssetRiskItem(p.individualId);

        }


        private Negotiator GetAssetRiskItem(int indivdualId, int id)
        {
            GetByIdResult<AssetRiskItemDto> result = executionPlan
                .GetById<AssetRiskItem, AssetRiskItemDto>(() => repository.GetByPartyId<AssetRiskItem>(id, indivdualId))
                .OnSuccess(Mapper.Map<AssetRiskItem, AssetRiskItemDto>)
                .Execute();

            GETResponseDto<AssetRiskItemDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.IndividualAssetRiskItem.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteAssetRiskItem(int individualId)
        {
            var deleteAssetRiskItemDto = this.Bind<DeleteAssetRiskItemDto>();
            deleteAssetRiskItemDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<DeleteAssetRiskItemDto, int>(deleteAssetRiskItemDto);

            DELETEResponseDto<int> response = result.DeletePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualAssetRiskItem.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateAssetRiskItem(int individualId)
        {
            var editAssetRiskItemDto = this.Bind<EditAssetRiskItemDto>();
            editAssetRiskItemDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<EditAssetRiskItemDto, int>(editAssetRiskItemDto);

            PUTResponseDto<int> response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.IndividualAddresses.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}