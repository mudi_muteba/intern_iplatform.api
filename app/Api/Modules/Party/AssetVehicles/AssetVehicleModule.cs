﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Assets.AssetVehicles;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.AssetVehicles
{
    public class AssetVehicleModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public AssetVehicleModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.IndividualAssetVehicle.GetById.Route] = p => GetAssetVehicle(p.individualId, p.Id);
            Delete[SystemRoutes.IndividualAssetVehicle.GetById.Route] = p => DeleteAssetVehicle(p.individualId);
            Put[SystemRoutes.IndividualAssetVehicle.GetById.Route] = p => UpdateAssetVehicle(p.individualId);

        }


        private Negotiator GetAssetVehicle(int indivdualId, int id)
        {
            GetByIdResult<AssetVehicleDto> result = executionPlan
                .GetById<AssetVehicle, AssetVehicleDto>(() => repository.GetByPartyId<AssetVehicle>(id, indivdualId))
                .OnSuccess(Mapper.Map<AssetVehicle, AssetVehicleDto>)
                .Execute();

            GETResponseDto<AssetVehicleDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.IndividualAssetVehicle.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteAssetVehicle(int individualId)
        {
            var deleteAssetVehicleDto = this.Bind<DeleteAssetVehicleDto>();
            deleteAssetVehicleDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<DeleteAssetVehicleDto, int>(deleteAssetVehicleDto);

            DELETEResponseDto<int> response = result.DeletePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualAssetVehicle.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateAssetVehicle(int individualId)
        {
            var editAssetVehicleDto = this.Bind<EditAssetVehicleDto>();
            editAssetVehicleDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<EditAssetVehicleDto, int>(editAssetVehicleDto);

            PUTResponseDto<int> response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.IndividualAddresses.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}