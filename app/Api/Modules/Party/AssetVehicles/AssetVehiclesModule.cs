﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Assets.AssetVehicles;
using Domain.Party.Assets.AssetVehicles.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Party.AssetVehicles
{
    public class AssetVehiclesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public AssetVehiclesModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.paginator = paginator;

            Post[SystemRoutes.IndividualAssetVehicle.PostCreateAssetVehicle.Route] =
                p => CreateAssetVehicles(p.individualId);
            Get[SystemRoutes.IndividualAssetVehicle.GetAssetVehicles.Route] = p => GetAssetVehicles(p.individualId);

            Options[SystemRoutes.IndividualAssetVehicle.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.IndividualAssetVehicle));
        }


        private Negotiator CreateAssetVehicles(int individualId)
        {
            var createAssetVehicleDto = this.Bind<CreateAssetVehicleDto>();
            createAssetVehicleDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<CreateAssetVehicleDto, int>(createAssetVehicleDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.IndividualAssetVehicle.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAssetVehicles(int indivdualId)
        {
            var queryResult = executionPlan
                .Query<GetAssetVehicleByPartyIdQuery, AssetVehicle, ListResultDto<ListAssetVehicleDto>, ListAssetVehicleDto>()
                .Configure(q => q.WithPartyId(indivdualId))
                .OnSuccess(r => Mapper.Map<List<AssetVehicle>, ListResultDto<ListAssetVehicleDto>>(r.ToList()))
                .Execute();

            //var result = new ListResultDto<ListAssetVehicleDto> {Results = queryResult.Response};

            LISTResponseDto<ListAssetVehicleDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.IndividualAssetVehicle.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}