﻿using System.Collections.Generic;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;
using Domain.Party.Payments.Queries;
using Domain.Party.Payments;
using iPlatform.Api.DTOs.Party.Payments;
using System.Linq;

namespace Api.Modules.Party.Payments
{
    public class PaymentDetailModule : SecureModule
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public PaymentDetailModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.PaymentDetails.GetById.Route] = p => GetPaymentDetail(p.individualId, p.id);

            Put[SystemRoutes.PaymentDetails.Put.Route] = p => UpdatePaymentDetail(p.individualId, p.id);
        }


        private Negotiator GetPaymentDetail(int individualId,  int id)
        {
            var queryResult = executionPlan
                .Query<GetPaymentDetailByIdQuery, PaymentDetails, PaymentDetailsDto, PaymentDetailsDto>()
                .Configure(q => { 
                    q.WithId(id); 
                    q.WithPartyId(individualId); 
                })
                .OnSuccess(result => Mapper.Map<PaymentDetailsDto>(result.FirstOrDefault()))
                .Execute();

            var response = queryResult.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.PaymentDetails.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdatePaymentDetail(int individualId, int id)
        {
            var editPaymentDetailsDto = this.Bind<EditPaymentDetailsDto>();
            editPaymentDetailsDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<EditPaymentDetailsDto, int>(editPaymentDetailsDto);

            PUTResponseDto<int> response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.PaymentDetails.CreateGetById(individualId, id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}