﻿using System.Collections.Generic;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;
using Domain.Party.Payments;
using iPlatform.Api.DTOs.Party.Payments;
using Domain.Party.Payments.Queries;
using System.Linq;
using Api.Models;

namespace Api.Modules.Party.Payments
{
    public class PaymentDetailsModule : SecureModule
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public PaymentDetailsModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            _repository = repository;

            Post[SystemRoutes.PaymentDetails.Search.Route] = _ => SearchPaymentDetails();
            Get[SystemRoutes.PaymentDetails.Get.Route] = p => GetPaymentsByPartyId(p.individualId);
            Post[SystemRoutes.PaymentDetails.Post.Route] = p => CreatePaymentDetail(p.individualId);

            Options[SystemRoutes.PaymentDetails.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.PaymentDetails));
        }


        private Negotiator SearchPaymentDetails()
        {
            var criteria = this.Bind<PaymentDetailSearchDto>();

            var queryResult = executionPlan
                .Search<SearchPaymentDetailsQuery, PaymentDetails, PaymentDetailSearchDto, PagedResultDto<PaymentDetailsDto>,PaymentDetailsDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<PaymentDetailsDto>>(
                    result.Paginate(paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.PaymentDetails.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetPaymentsByPartyId(int partyId)
        {
            var queryResult = executionPlan
                .Query<GetPaymentDetailByPartyIdQuery, PaymentDetails, ListResultDto<PaymentDetailsDto>, PaymentDetailsDto>()
                .Configure(q => q.WithPartyId(partyId))
                .OnSuccess(r => Mapper.Map<List<PaymentDetails>, ListResultDto<PaymentDetailsDto>>(r.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.PaymentDetails.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreatePaymentDetail(int individualId)
        {
            var createPDDto = this.Bind<CreatePaymentDetailsDto>();
            createPDDto.PartyId = individualId;

            HandlerResult<int> result = executionPlan.Execute<CreatePaymentDetailsDto, int>(createPDDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.PaymentDetails.CreateGetById(individualId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}