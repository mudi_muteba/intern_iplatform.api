﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Party.Addresses;
using Domain.Party.Addresses.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Tia.Parties;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Tia.Party
{
    public class TiaPartiesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;


        public TiaPartiesModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;


            Post[SystemRoutes.Tia.GetByTiaPartiesSearchDto.Route] = p => GetByTiaPartiesSearchDto();

            Options[SystemRoutes.Tia.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Tia));
        }


 
        private Negotiator GetByTiaPartiesSearchDto()
        {

            var searchDto = this.Bind<TiaPartiesSearchDto>();
            var result = executionPlan.Execute<TiaPartiesSearchDto, TiaPartySearchResultDto>(searchDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Tia.GetByTiaPartiesSearchDto.Route);


            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}