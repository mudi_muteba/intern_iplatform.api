﻿using Api.Models;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;
using System.Collections.Generic;
using System.Linq;
using Nancy.ModelBinding;
using AutoMapper;
using Domain.AdditionalMember.Queries;
using iPlatform.Api.DTOs.AdditionalMembers;
using iPlatform.Api.DTOs.Base;

namespace Api.Modules.AdditionalMembers
{
    public class AdditionalMembersModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public AdditionalMembersModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Post[SystemRoutes.AdditionalMembers.Post.Route] = p => CreateAdditionalMember();

            Get[SystemRoutes.AdditionalMembers.GetByProposalDefinitionId.Route] = p => GetByProposalId(p.id);

            Options[SystemRoutes.AdditionalMembers.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.AdditionalMembers));
        }

        private Negotiator CreateAdditionalMember()
        {
            var createAdditionalMemberDto = this.Bind<CreateAdditionalMemberDto>();

            var result = executionPlan.Execute<CreateAdditionalMemberDto, int>(createAdditionalMemberDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.AdditionalMembers.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetByProposalId(int id)
        {

            var result = executionPlan
                .Query<GetAdditionalMembersByProposalDefinitionIdQuery, Domain.Party.ProposalDefinitions.AdditionalMembers, ListResultDto<ListAdditionalMemberDto>, ListAdditionalMemberDto>()
                .Configure(q => q.ByProposalDefinitionId(id))
                .OnSuccess(r => Mapper.Map<List<Domain.Party.ProposalDefinitions.AdditionalMembers>, ListResultDto<ListAdditionalMemberDto>>(r.ToList()))
                .Execute();

            LISTResponseDto<ListAdditionalMemberDto> response = result.CreateLISTResponse(result.Response)
                .CreateLinks(c => SystemRoutes.AdditionalMembers.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}