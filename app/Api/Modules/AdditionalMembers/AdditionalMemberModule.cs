﻿using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;
using AutoMapper;
using iPlatform.Api.DTOs.AdditionalMembers;

namespace Api.Modules.AdditionalMembers
{
    public class AdditionalMemberModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public AdditionalMemberModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.AdditionalMembers.GetById.Route] = p => GetAdditionalMember(p.id);

            Put[SystemRoutes.AdditionalMembers.Put.Route] = p => Update(p.id);

            Put[SystemRoutes.AdditionalMembers.PutDisable.Route] = p => DisableAdditionalMember(p.id);
        }

        private Negotiator GetAdditionalMember(int id)
        {
            var result = executionPlan
                .GetById<Domain.Party.ProposalDefinitions.AdditionalMembers, ListAdditionalMemberDto>(() => repository.GetById<Domain.Party.ProposalDefinitions.AdditionalMembers>(id))
                .OnSuccess(Mapper.Map<Domain.Party.ProposalDefinitions.AdditionalMembers, ListAdditionalMemberDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.AdditionalMembers.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator Update(int id)
        {
            var editTeamDto = this.Bind<EditAdditionalMemberDto>();
            editTeamDto.Id = id;

            var result = executionPlan.Execute<EditAdditionalMemberDto, int>(editTeamDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.AdditionalMembers.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator DisableAdditionalMember(int id)
        {
            var disableAdditionalMemberDto = new DisableAdditionalMemberDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableAdditionalMemberDto, int>(disableAdditionalMemberDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.AdditionalMembers.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}