﻿using Nancy;
using Nancy.Routing;
using System.Collections.Generic;
using System.Linq;

namespace Api.Modules.Routes
{
    public class RoutesModule : NancyModule
    {
        public RoutesModule(IRouteCacheProvider routeCacheProvider)
        {
            Get["/routes"] = GetIndex(routeCacheProvider);
        }

        private dynamic GetIndex(IRouteCacheProvider provider)
        {
            var response = new IndexModel();
                response.Routes = provider.GetCache().Values.SelectMany(t => t.Select(t1 => t1.Item2));

            return response;
        }

        public class IndexModel
        {
            public IEnumerable<RouteDescription> Routes { get; set; }
        }
    }
}