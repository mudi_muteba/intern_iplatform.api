﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Proposals;
using iPlatform.Api.DTOs.QuoteBreakdown;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.QuoteBreakdown
{
  

    public class QuoteBreakdownModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public QuoteBreakdownModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;
            
            Put[SystemRoutes.QuoteBreakdown.EditQuoteBreakdown.Route] = p => EditQuoteBreakdown();
           
        }

      
        private Negotiator EditQuoteBreakdown()
        {
            var validateProposalDto = this.Bind<EditQuoteItemBreakDownDto>();
            

            var result = _executionPlan.Execute<EditQuoteItemBreakDownDto, int>(validateProposalDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.QuoteBreakdown.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        
    }
}