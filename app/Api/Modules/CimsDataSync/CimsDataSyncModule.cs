﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.CimsDataSync;
using Domain.CimsDataSync.Queries;
using Domain.CimsDataSync.StoredProcedures;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.CimsDataSync;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Modules.CimsDataSync
{
    public class CimsDataSyncModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;

        public CimsDataSyncModule(IExecutionPlan executionPlan)
        {
            m_ExecutionPlan = executionPlan;

            Post[SystemRoutes.CimsDataSync.CimsDataSyncProductPricingStructure.Route] = p => CimsDataSyncProductPricingStructure(p.dbName);
            Post[SystemRoutes.CimsDataSync.CimsDataSyncChannelUser.Route] = p => CimsDataSyncChannelUser();
        }

        #region [ Negotiator's ]

        private Negotiator CimsDataSyncProductPricingStructure(string dbName)
        {
            var result = m_ExecutionPlan.QueryStoredProcedure<CimsDataSyncProductPricingStructureQuery, CimsDataSyncProductPricingStructure, 
                CimsDataSyncProductPricingStructureStoredProcedure, CimsDataSyncProductPricingStructureStoredProcedureDto>
                (new CimsDataSyncProductPricingStructureStoredProcedure(dbName))
                .OnSuccess(Mapper.Map<IList<CimsDataSyncProductPricingStructure>, IList<CimsDataSyncProductPricingStructureStoredProcedureDto>>)
                .Execute();

            var response = new QueryResult<CimsDataSyncProductPricingStructureStoredProcedureDto>(new CimsDataSyncProductPricingStructureStoredProcedureDto()
                {
                    RowsAffected = result.Response.FirstOrDefault() != null ? result.Response.FirstOrDefault().RowsAffected : 0,
                    Message = result.Response.FirstOrDefault() != null ? result.Response.FirstOrDefault().Message : string.Empty
                })
                .CreateGetPostResponse();



            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CimsDataSyncChannelUser()
        {
            CimsDataSyncChannelUserDto cimsDataSyncChannelUserDto = this.Bind<CimsDataSyncChannelUserDto>();

            var result = m_ExecutionPlan.QueryStoredProcedure<CimsDataSyncChannelUserQuery, CimsDataSyncChannelUser,
                CimsDataSyncChannelUserStoredProcedure, CimsDataSyncChannelUserStoredProcedureDto>
                (new CimsDataSyncChannelUserStoredProcedure(cimsDataSyncChannelUserDto))
                .OnSuccess(Mapper.Map<IList<CimsDataSyncChannelUser>, IList<CimsDataSyncChannelUserStoredProcedureDto>>)
                .Execute();

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        #endregion
    }
}