﻿using System;
using Api.Infrastructure;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Upload;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Policies
{
    public class PolicyUploadConfirmationsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;

        public PolicyUploadConfirmationsModule(IExecutionPlan executionPlan)
        {
            _executionPlan = executionPlan;

            Post[SystemRoutes.PolicyUploadConfirmations.Post.Route] = p => Upload();
        }

        private Negotiator Upload()
        {
            var dto = this.Bind<PolicyUploadDto>();
            var result = _executionPlan.Execute<PolicyUploadDto, Guid>(dto);
            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}