﻿using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Policies;
using Domain.Policies.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Policy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Policies.PolicyHeaders
{
    public class PolicyHeadersModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public PolicyHeadersModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Options[SystemRoutes.PolicyHeaders.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.PolicyHeaders));

            Post[SystemRoutes.PolicyHeaders.Post.Route] = p => CreatePolicyHeader();
            Post[SystemRoutes.PolicyHeaders.PostQuoteAccepted.Route] = p => CreatePolicyWhenQuoteAccepted();
            Post[SystemRoutes.PolicyHeaders.Search.Route] = p => SearchPoliciesHeaders();
            Post[SystemRoutes.PolicyHeaders.SearchWithoutPagination.Route] = p => SearchPoliciesHeadersWithoutPagination();
            Post[SystemRoutes.PolicyHeaders.GetByFilters.Route] = p => GetAllPoliciesByFilter();
            Post[SystemRoutes.PolicyHeaders.GetScheduledDocuments.Route] = p => GetScheduledDocuments();
            Get[SystemRoutes.PolicyHeaders.Getfnol.Route] = p => GetPoliciesForFNOL(p.partyId);
        }



        private Negotiator CreatePolicyWhenQuoteAccepted()
        {
            var createPolicyWhenQuoteAcceptedDto = this.Bind<CreatePolicyWhenQuoteAcceptedDto>();

            var result = executionPlan.Execute<CreatePolicyWhenQuoteAcceptedDto, int>(createPolicyWhenQuoteAcceptedDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.PolicyHeaders.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        
        private Negotiator CreatePolicyHeader()
        {
            var createPolicyHeaderDto = this.Bind<CreatePolicyHeaderDto>();

            var result = executionPlan.Execute<CreatePolicyHeaderDto, int>(createPolicyHeaderDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.PolicyHeaders.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchPoliciesHeaders()
        {
            var criteria = this.Bind<PolicySearchDto>();

            var queryResult = executionPlan
                .Search<SearchPolicyQuery, PolicyHeader, PolicySearchDto, PagedResultDto<PolicyHeaderDto>, PolicyHeaderDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<PolicyHeaderDto>>(
                    result.Paginate(paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.PolicyHeaders.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchPoliciesHeadersWithoutPagination()
        {
            var criteria = this.Bind<PolicySearchDto>();

            var queryResult = executionPlan
                .Search<SearchPolicyQuery, PolicyHeader, PolicySearchDto, ListResultDto<PolicyHeaderDto>, PolicyHeaderDto>(criteria)
                .OnSuccess(result => Mapper.Map<ListResultDto<PolicyHeaderDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.PolicyHeaders.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetAllPoliciesByFilter()
        {
            var criteria = this.Bind<PolicySearchDto>();

            var result = executionPlan.Execute<PolicySearchDto, PolicyHeaderDto>(criteria);

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.PolicyHeaders.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetScheduledDocuments()
        {
            var criteria = this.Bind<PolicySearchDto>();

            var result = executionPlan.Execute<PolicySearchDto, ListResultDto<PolicyScheduleDto>>(criteria);

            var response = result.CreateLISTResponse<PolicyScheduleDto>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetPoliciesForFNOL(int partyId)
        {

            GetPoliciesFNOLDto getPoliciesFNOLDto = new GetPoliciesFNOLDto { PartyId = partyId };

            var result = executionPlan.Execute<GetPoliciesFNOLDto, ListResultDto<PolicyHeaderDto>>(getPoliciesFNOLDto);

            var response = result.CreateLISTResponse<PolicyHeaderDto>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}