﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Policies;
using Domain.Policies.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Policy.FNOL;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Policies.PolicyHeaders
{
    public class PolicyHeaderModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public PolicyHeaderModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan
            , IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Options[SystemRoutes.PolicyHeaders.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.PolicyHeaders));

            Get[SystemRoutes.PolicyHeaders.GetByPartyAndPolicyIds.Route] = p => GetPolicyHeader(p.id, p.partyId);

            Get[SystemRoutes.PolicyHeaders.GetByPolicyId.Route] = p => GetPolicyHeader(p.id);

            Post[SystemRoutes.PolicyHeaders.GetDataForFNOL.Route] = p => GetPolicyDataForFNOL();
            Post[SystemRoutes.PolicyHeaders.GetAvailableItemsForFNOL.Route] = p => GetPolicyAvailableItemsForFNOL();

            Put[SystemRoutes.PolicyHeaders.UpdateByQuoteId.Route] = p => UpdatePolicyHeaderByQuoteId();
        }

        private Negotiator GetPolicyHeader(int id)
        {
            var value = repository.GetById<PolicyHeader>(id);
            var result = executionPlan
                .GetById<PolicyHeader, PolicyHeaderDto>(() => repository.GetById<PolicyHeader>(id))
                .OnSuccess(Mapper.Map<PolicyHeader, PolicyHeaderDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.PolicyHeaders.CreateLinks(c))
                ;
            
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetPolicyHeader(int policyId, int partyId)
        {
            var criteria = this.Bind<PolicySearchDto>();

            var queryResult = executionPlan
                .Search<SearchPolicyQuery, PolicyHeader, PolicySearchDto, PagedResultDto<PolicyHeaderDto>, PolicyHeaderDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<PolicyHeaderDto>>(
                    result.Paginate(paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.PolicyHeaders.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetPolicyDataForFNOL()
        {
            var criteria = this.Bind<GetPolicyDataForFNOLDto>();

            var result = executionPlan.Execute<GetPolicyDataForFNOLDto, PolicyDataFnolDto>(criteria);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetPolicyAvailableItemsForFNOL()
        {
            var criteria = this.Bind<GetPolicyAvailableItemsForFNOLDto>();

            var result = executionPlan.Execute<GetPolicyAvailableItemsForFNOLDto, ListResultDto<PolicyHeaderClaimableItemDto>>(criteria);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdatePolicyHeaderByQuoteId()
        {
            var editPolicyHeaderDto = this.Bind<EditPolicyHeaderDto>();

            var result = executionPlan.Execute<EditPolicyHeaderDto, int>(editPolicyHeaderDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.PolicyHeaders.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

    }
}