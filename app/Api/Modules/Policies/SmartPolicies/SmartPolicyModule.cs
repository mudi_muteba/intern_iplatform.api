﻿using System.Xml.Linq;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Policy.Smart;
using iPlatform.Api.DTOs.Policy.Smart.SoapDtos;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Policies.SmartPolicies
{
    public class SmartPolicyModule : NancyModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRootPathProvider root;

        public SmartPolicyModule(IExecutionPlan executionPlan, IRootPathProvider root)
        {
            this.executionPlan = executionPlan;
            this.root = root;

            Options[SystemRoutes.SmartPolicy.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.SmartPolicy));
            Post[SystemRoutes.SmartPolicy.GetSmartPolicy.Route] = p => GetPolicyData2();
            Post[SystemRoutes.SmartPolicy.GetSmartPolicyPdf.Route] = p => GetPdfUrl();
            Post[SystemRoutes.SmartPolicy.RegisterSmartPolicy.Route] = p => RegisterPolicy();
            Post[SystemRoutes.SmartPolicy.UpdateSmartPolicy.Route] = p => UpdatePolicy();
            Post[SystemRoutes.SmartPolicy.SendPdfSmartPolicy.Route] = p => SendPolicies();
        }

        private Negotiator GetPolicyData2()
        {
            var criteria = this.Bind<GetPolicyRequestEnvelope>();
            var result = executionPlan.Execute<GetPolicyRequestEnvelope, XElement>(criteria);
            return Negotiate.ForXml(result.Response);
        }


        private Negotiator GetPdfUrl()
        {
            var criteria = this.Bind<GetPdfUrlRequestEnvelope>();
            criteria.PathMap = root.GetRootPath(); //Need this to store the CIMS self service pdf if there is one

            var result = executionPlan.Execute<GetPdfUrlRequestEnvelope, XElement>(criteria);
            return Negotiate.ForXml(result.Response);
        }

        private Negotiator RegisterPolicy()
        {
            var criteria = this.Bind<RegisterPolicyRequestEnvelope>();
            var result = executionPlan.Execute<RegisterPolicyRequestEnvelope, XElement>(criteria);
            return Negotiate.ForXml(result.Response);
        }

        private Negotiator UpdatePolicy()
        {
            var criteria = this.Bind<UpdatePolicyRequestEnvelope>();
            var result = executionPlan.Execute<UpdatePolicyRequestEnvelope, XElement>(criteria);
            return Negotiate.ForXml(result.Response);
        }

        private Negotiator SendPolicies()
        {
            var criteria = this.Bind<SendPdfPolicyRequestEnvelope>();
            var result = executionPlan.Execute<SendPdfPolicyRequestEnvelope, XElement>(criteria);
            return Negotiate.ForXml(result.Response);
        }

    }

    
}