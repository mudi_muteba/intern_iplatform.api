﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Components.ComponentDefinitions;
using Domain.Components.ComponentDefinitions.Builders;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using iPlatform.Api.DTOs.Components.ComponentDefinitions.Questions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;


namespace Api.Modules.Components.Definition
{
    public class ComponentDefinitionModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;
        private IProvideContext contextProvider;

        public ComponentDefinitionModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan
            , IRepository repository
            , IProvideContext contextProvider)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.contextProvider = contextProvider;

            Options[SystemRoutes.ComponentDefinitionRoutes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ComponentDefinitionRoutes));

            Get[SystemRoutes.ComponentDefinitionRoutes.GetById.Route] = p => GetComponentDefinition(p.id);
            //Get[SystemRoutes.ComponentDefinitionRoutes.GetComponentDefinitionQuestions.Route] = p => GetComponentDefinitionQuestions(p.id);
            Put[SystemRoutes.ComponentDefinitionRoutes.Put.Route] = p => UpdateComponentDefinition(p.id);
            Put[SystemRoutes.ComponentDefinitionRoutes.PutDisable.Route] = p => DisableComponentDefinition(p.id);

        }

        private dynamic UpdateComponentDefinition(int id)
        {
            var editComponentDefinitionDto = this.Bind<EditComponentDefinitionDto>();
            editComponentDefinitionDto.Id = id;

            var result = executionPlan.Execute<EditComponentDefinitionDto, int>(editComponentDefinitionDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.HomeLossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetComponentDefinition(int id)
        {
            var result = executionPlan
                .GetById<ComponentDefinition, ComponentDefinitionDto>(() => repository.GetById<ComponentDefinition>(id))
                .OnSuccess(Mapper.Map<ComponentDefinition, ComponentDefinitionDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ComponentDefinitionRoutes.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        //private Negotiator GetComponentDefinitionQuestions(int id)
        //{
        //    var result = executionPlan
        //        .GetById<ComponentDefinition, ComponentDefinitionQuestionsDto>(() => repository.GetById<ComponentDefinition>(id))
        //        .OnSuccess(definition => new ComponentDefinitionBuilderBuilder(contextProvider, repository).BuildComponentDefinitionQuestions(definition))
        //        .Execute();

        //    var response = result.CreateGETResponse()
        //        .CreateLinks(c => SystemRoutes.ComponentDefinitionRoutes.GetLinks(c));

        //    return Negotiate.ForSupportedFormats(response, response.StatusCode);
        //}

        private Negotiator DisableComponentDefinition(int id)
        {
            var disableComponentDefinitionDto = new DisableComponentDefinitionDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableComponentDefinitionDto, int>(disableComponentDefinitionDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ComponentDefinitionRoutes.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}