﻿using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Components.ComponentDefinitions;
using Domain.Components.ComponentDefinitions.Builders;
using Domain.Components.ComponentDefinitions.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Components.Definition
{
    public class ComponentDefinitionsModule : SecureModule
    {
        private IProvideContext contenxt;
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ComponentDefinitionsModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan
            , IRepository repository
            ,IProvideContext contenxt)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.contenxt = contenxt;

            Options[SystemRoutes.ComponentDefinitionRoutes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ComponentDefinitionRoutes));

            Post[SystemRoutes.ComponentDefinitionRoutes.Post.Route] = p => CreateComponentDefinitionRoute();
            Post[SystemRoutes.ComponentDefinitionRoutes.SearchDefinitionsAnswers.Route] = p => SearchDefinitionsAnswers();
        }

        private Negotiator CreateComponentDefinitionRoute()
        {
            var createComponentDefinitionDto = this.Bind<CreateComponentDefinitionDto>();

            var result = executionPlan.Execute<CreateComponentDefinitionDto, int>(createComponentDefinitionDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ComponentDefinitionRoutes.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchDefinitionsAnswers()
        {
            var criteria = this.Bind<SearchComponentDefinitionAnswersDto>();

            var queryResult = executionPlan
                .Search<SearchDefinitionQuestionAnswersQuery, ComponentDefinition, SearchComponentDefinitionAnswersDto, ListResultDto<SearchComponentQuestionDefinitionAnswerResultDto>>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(results => new ComponentDefinitionAnswersBuilder(contenxt, repository).Build(results))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response);
            //.CreateLinks(c => SystemRoutes.ComponentQuestionDefinitionsRoutes.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}