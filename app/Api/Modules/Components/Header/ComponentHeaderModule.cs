﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Components.ComponentHeaders;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Api.Modules.Components.Header
{
    public class ComponentHeaderModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ComponentHeaderModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;


            Options[SystemRoutes.ComponentHeaderRoutes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ComponentHeaderRoutes));

            Get[SystemRoutes.ComponentHeaderRoutes.GetById.Route] = p => GetComponentHeaderRoutes(p.id);
            Put[SystemRoutes.ComponentHeaderRoutes.Put.Route] = p => UpdateComponentHeader(p.id);
            Put[SystemRoutes.ComponentHeaderRoutes.PutDisable.Route] = p => DisableComponentHeader(p.id);
        }

        private Negotiator UpdateComponentHeader(int id)
        {
            var editComponentHeaderDto = this.Bind<EditComponentHeaderDto>();
            editComponentHeaderDto.Id = id;

            var result = executionPlan.Execute<EditComponentHeaderDto, int>(editComponentHeaderDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.HomeLossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetComponentHeaderRoutes(int id)
        {
            var result = executionPlan
                .GetById<ComponentHeader, ComponentHeaderDto>(() => repository.GetById<ComponentHeader>(id))
                .OnSuccess(Mapper.Map<ComponentHeader, ComponentHeaderDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ComponentHeaderRoutes.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableComponentHeader(int id)
        {
            var disableComponentHeaderDto = new DisableComponentHeaderDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableComponentHeaderDto, int>(disableComponentHeaderDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ComponentHeaderRoutes.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}