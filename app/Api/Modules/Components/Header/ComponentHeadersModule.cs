﻿using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Modules.Components.Header
{
    public class ComponentHeadersModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ComponentHeadersModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Options[SystemRoutes.ComponentHeaderRoutes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ComponentHeaderRoutes));

            Post[SystemRoutes.ComponentHeaderRoutes.Post.Route] = p => CreateComponentHeaderRoute();
        }

        private Negotiator CreateComponentHeaderRoute()
        {
            var createComponentHeaderDto = this.Bind<CreateComponentHeaderDto>();

            var result = executionPlan.Execute<CreateComponentHeaderDto, int>(createComponentHeaderDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ComponentHeaderRoutes.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}