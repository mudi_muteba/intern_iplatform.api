﻿using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Components.ComponentQuestionDefinitions;
using Domain.Components.ComponentQuestionDefinitions.Builders;
using Domain.Components.ComponentQuestionDefinitions.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Components.QuestionDefinition
{
    public class ComponentQuestionDefinitionModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;
        private IProvideContext contextProvider;

        public ComponentQuestionDefinitionModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan
            , IRepository repository
            , IProvideContext contextProvider)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.contextProvider = contextProvider;

            Options[SystemRoutes.ComponentQuestionDefinitionsRoutes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ComponentQuestionDefinitionsRoutes));
            Post[SystemRoutes.ComponentQuestionDefinitionsRoutes.SearchDefinitions.Route] = p => SearchDefinitions();
        }

        private Negotiator SearchDefinitions()
        {
            var criteria = this.Bind<SearchComponentDefinitionsDto>();

            var queryResult = executionPlan
                .Search<SearchComponentQuestionDefinitionsQuery, ComponentQuestionDefinition, SearchComponentDefinitionsDto, ListResultDto<SearchComponentQuestionDefinitionResultDto>>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(results => new ComponentQuestionDefinitionBuilder(contextProvider, repository).Build(results))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response);
            //.CreateLinks(c => SystemRoutes.ComponentQuestionDefinitionsRoutes.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }        
    }
}
