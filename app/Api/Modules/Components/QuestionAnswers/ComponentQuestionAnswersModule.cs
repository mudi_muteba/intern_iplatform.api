﻿using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Modules.Components.QuestionAnswers
{
    public class ComponentQuestionAnswersModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ComponentQuestionAnswersModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Options[SystemRoutes.ComponentQuestionAnswerRoutes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ComponentQuestionAnswerRoutes));

            Post[SystemRoutes.ComponentQuestionAnswerRoutes.PostMultiple.Route] = p => SaveComponentQuestionAnswers();
            
        }

        private Negotiator SaveComponentQuestionAnswers()
        {
            var dto = this.Bind<SaveComponentQuestionAnswersDto>();

            var result = executionPlan.Execute<SaveComponentQuestionAnswersDto, bool>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}