﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Components.ComponentQuestionAnswers;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Modules.Components.QuestionAnswers
{
    public class ComponentQuestionAnswerModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ComponentQuestionAnswerModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;


            Options[SystemRoutes.ComponentQuestionAnswerRoutes.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ComponentQuestionAnswerRoutes));

            Get[SystemRoutes.ComponentQuestionAnswerRoutes.GetById.Route] = p => GetComponentQuestionAnswerRoutes(p.id);
            Post[SystemRoutes.ComponentQuestionAnswerRoutes.Post.Route] = p => SaveComponentQuestionAnswerRoute();
            

            Put[SystemRoutes.ComponentQuestionAnswerRoutes.Put.Route] = p => UpdateComponentQuestionAnswer(p.id);
            Put[SystemRoutes.ComponentQuestionAnswerRoutes.PutDisable.Route] = p => DisableComponentQuestionAnswer(p.id);

        }

        private Negotiator SaveComponentQuestionAnswerRoute()
        {
            var createComponentQuestionAnswerDto = this.Bind<CreateComponentQuestionAnswerDto>();

            var result = executionPlan.Execute<CreateComponentQuestionAnswerDto, int>(createComponentQuestionAnswerDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ComponentQuestionAnswerRoutes.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateComponentQuestionAnswer(int id)
        {
            var editComponentQuestionAnswerDto = this.Bind<EditComponentQuestionAnswerDto>();
            editComponentQuestionAnswerDto.Id = id;

            var result = executionPlan.Execute<EditComponentQuestionAnswerDto, int>(editComponentQuestionAnswerDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.HomeLossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetComponentQuestionAnswerRoutes(int id)
        {
            var result = executionPlan
                .GetById<ComponentQuestionAnswer, ComponentQuestionAnswerDto>(() => repository.GetById<ComponentQuestionAnswer>(id))
                .OnSuccess(Mapper.Map<ComponentQuestionAnswer, ComponentQuestionAnswerDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ComponentQuestionAnswerRoutes.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableComponentQuestionAnswer(int id)
        {
            var disableComponentQuestionAnswerDto = new DisableComponentQuestionAnswerDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableComponentQuestionAnswerDto, int>(disableComponentQuestionAnswerDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ComponentQuestionAnswerRoutes.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}