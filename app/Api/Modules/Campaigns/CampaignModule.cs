using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Campaigns.Queries;
using Domain.Individuals;
using Domain.Individuals.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Individual;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Campaigns
{
    public class CampaignModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public CampaignModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.Campaigns.GetById.Route] = p => GetCampaign(p.id);

            Put[SystemRoutes.Campaigns.Put.Route] = p => UpdateCampaign(p.id);
            Put[SystemRoutes.Campaigns.PutDisable.Route] = p => DisableCampaign(p.id);
            Put[SystemRoutes.Campaigns.PutDelete.Route] = p => DeleteCampaign(p.id);
            Post[SystemRoutes.Campaigns.AssignLeads.Route] = p => AssignLeads(p.id);
            Get[SystemRoutes.Campaigns.GetLeads.Route] = p => GetLeads(p.id);
            Get[SystemRoutes.Campaigns.GetLeadsByAgent.Route] = p => GetLeadsByAgentId(p.id, p.agentid);
            Get[SystemRoutes.Campaigns.GetACampaignLeadBucket.Route] = p => GetCampaignLeadBucket(p.id, p.leadid);
            Put[SystemRoutes.Campaigns.PutNextLead.Route] = p => GetNextLead(p.id);
            Post[SystemRoutes.Campaigns.GetNextSerityImportLead.Route] = p => GetSeritiImportNextLead(p.id);
            Post[SystemRoutes.Campaigns.PostAssignLeadAgent.Route] = p => AssignLead(p.id);
        }

        private Negotiator UpdateCampaign(int id)
        {
            var editCampaignDto = this.Bind<EditCampaignDto>();
            editCampaignDto.Id = id;

            var result = executionPlan.Execute<EditCampaignDto, int>(editCampaignDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Campaigns.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetCampaign(int id)
        {
            var value = repository.GetById<Campaign>(id);
            var result = executionPlan
                .GetById<Campaign, CampaignDto>(() => repository.GetById<Campaign>(id))
                .OnSuccess(Mapper.Map<Campaign, CampaignDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Campaigns.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteCampaign(int id)
        {
            var deleteCampaign = this.Bind<DeleteCampaignDto>();
            deleteCampaign.Id = id;

            var result = executionPlan.Execute<DeleteCampaignDto, int>(deleteCampaign);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Campaigns.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableCampaign(int id)
        {
            var disableCampaign = new DisableCampaignDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableCampaignDto, int>(disableCampaign);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Campaigns.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator AssignLeads(int id)
        {
            var assignLeadToCampaignDto = this.Bind<AssignLeadToCampaignDto>();

            var result = executionPlan.Execute<AssignLeadToCampaignDto, bool>(assignLeadToCampaignDto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetLeads(int id)
        {
            var criteria = new IndividualSearchDto { CampaignId = id };


            var queryResult = executionPlan
                .Search<SearchCampaignIndividualsQuery, Individual, IndividualSearchDto, List<ListIndividualDto>, ListIndividualDto>(criteria)
                .OnSuccess(r => Mapper.Map<List<Individual>, List<ListIndividualDto>>(r.ToList()))
                .Execute();

            var result = new ListResultDto<ListIndividualDto> { Results = queryResult.Response };

            LISTResponseDto<ListIndividualDto> response = queryResult.CreateLISTResponse(result)
                .CreateLinks(c => SystemRoutes.Individuals.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetLeadsByAgentId(int id, int agentid)
        {
            var criteria = new IndividualSearchDto { CampaignId = id, AgentId = agentid };


            var queryResult = executionPlan
                .Search<SearchCampaignIndividualsQuery, Individual, IndividualSearchDto, List<ListIndividualDto>, ListIndividualDto>(criteria)
                .OnSuccess(r => Mapper.Map<List<Individual>, List<ListIndividualDto>>(r.ToList()))
                .Execute();

            var result = new ListResultDto<ListIndividualDto> { Results = queryResult.Response };

            LISTResponseDto<ListIndividualDto> response = queryResult.CreateLISTResponse(result)
                .CreateLinks(c => SystemRoutes.Individuals.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetCampaignLeadBucket(int id, int leadid)
        {
            var result = executionPlan
                .Query<GetCampaignLeadBucketQuery, CampaignLeadBucket, CampaignLeadBucketDto, CampaignLeadBucketDto>()
                .Configure(q => q.WithDetails(id, leadid))
                .OnSuccess(r => Mapper.Map<CampaignLeadBucket, CampaignLeadBucketDto>(r.OrderByDescending(x => x.Id).FirstOrDefault()))
                .Execute();

            GETResponseDto<CampaignLeadBucketDto> response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Individuals.CreateLinks(c.Lead));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetNextLead(int id)
        {
            var dto = this.Bind<GetNextLeadDto>();

            var result = executionPlan.Execute<GetNextLeadDto, NextLeadResponseDto>(dto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response.Id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetSeritiImportNextLead(int id)
        {
            var dto = this.Bind<GetSeritiImportNextLeadDto>();
            var result = executionPlan.Execute<GetSeritiImportNextLeadDto, NextLeadResponseDto>(dto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response.Id));

            return Negotiate.WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator AssignLead(int id)
        {
            var dto = this.Bind<AssignLeadToAgentDto>();

            var result = executionPlan.Execute<AssignLeadToAgentDto, NextLeadResponseDto>(dto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Leads.CreateGetById(result.Response.Id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}