﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Campaigns;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Collections.Generic;
using System.Linq;

namespace Api.Modules.Campaigns
{
    public class CampaignsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public CampaignsModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Options[SystemRoutes.Campaigns.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Campaigns));

            Get[SystemRoutes.Campaigns.Get.Route] = _ => GetAllCampaigns(new Pagination());

            Get[SystemRoutes.Campaigns.GetWithPagination.Route] = p => GetAllCampaigns(new Pagination(p.PageNumber, p.PageSize));

            Post[SystemRoutes.Campaigns.Post.Route] = p => CreateCampaign();

            Post[SystemRoutes.Campaigns.PostSearch.Route] = p => SearchCampaigns();

            Get[SystemRoutes.Campaigns.GetByPartyId.Route] = p => GetCampaignsByPartyId(p.Id);

            Get[SystemRoutes.Campaigns.GetByChannelPartyId.Route] = p => GetCampaignsByChannelPartyId(p.channelId, p.partyId);

            Get[SystemRoutes.Campaigns.GetByChannelId.Route] = p => GetCampaignsByChannelId(p.Id);

            Options[SystemRoutes.Campaigns.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Campaigns));

            Post[SystemRoutes.Campaigns.PostAssignLeadCampaigns.Route] = p => AssignLeadCampaigns();

            Post[SystemRoutes.Campaigns.PostCampaignReference.Route] = p => SaveCampaignReference();

            Get[SystemRoutes.Campaigns.GetAcrossChannelPartyId.Route] = p => GetCampaignsAcrossChannelPartyId(p.partyId);
        }

        private Negotiator SearchCampaigns()
        {
            var criteria = this.Bind<CampaignSearchDto>();

            var queryResult = executionPlan
                .Search<SearchCampaignsQuery, Campaign, CampaignSearchDto, PagedResultDto<ListCampaignDto>,ListCampaignDto >(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListCampaignDto>>(
                    result.Paginate(paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Campaigns.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateCampaign()
        {
            var createCampaignDto = this.Bind<CreateCampaignDto>();

            var result = executionPlan.Execute<CreateCampaignDto, int>(createCampaignDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Campaigns.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllCampaigns(Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetAllCampaignsQuery, Campaign, PagedResultDto<ListCampaignDto>, ListCampaignDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListCampaignDto>>(result.Paginate(paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Campaigns.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetCampaignsByPartyId(int id)
        {
            var result = executionPlan
                .Query<GetCampaignByPartyId, Campaign, List<ListCampaignDto>, ListCampaignDto>()
                .Configure(q => q.ByPartyId(id))
                .OnSuccess(r => Mapper.Map<List<Campaign>, List<ListCampaignDto>>(r.ToList()))
                .Execute();

            var response = new GETResponseDto<List<ListCampaignDto>>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetCampaignsByChannelPartyId(int channelId, int partyId)
        {
            var result = executionPlan
                .Query<GetCampaignByChannelPartyId, Campaign, List<ListCampaignDto>, ListCampaignDto>()
                .Configure(q => q.ByChannelPartyId(partyId, channelId))
                .OnSuccess(r => Mapper.Map<List<Campaign>, List<ListCampaignDto>>(r.ToList()))
                .Execute();

            var response = new GETResponseDto<List<ListCampaignDto>>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetCampaignsByChannelId(int id)
        {
            var result = executionPlan
                .Query<GetCampaignByChannelId, Campaign, List<ListCampaignDto>, ListCampaignDto>()
                .Configure(q => q.ByChannelId(id))
                .OnSuccess(r => Mapper.Map<List<Campaign>, List<ListCampaignDto>>(r.ToList()))
                .Execute();

            var response = new GETResponseDto<List<ListCampaignDto>>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator AssignLeadCampaigns()
        {
            var assignLeadCampaignsDto = this.Bind<AssignLeadCampaignsDto>();

            var result = executionPlan.Execute<AssignLeadCampaignsDto, bool>(assignLeadCampaignsDto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator SaveCampaignReference()
        {
            var dto = this.Bind<CreateCampaignReferenceDto>();

            var result = executionPlan.Execute<CreateCampaignReferenceDto, int>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetCampaignsAcrossChannelPartyId(int partyId)
        {
            var result = executionPlan
                .Query<GetCampaignAcrossChannelPartyId, Campaign, List<ListCampaignDto>, ListCampaignDto>()
                .Configure(q => q.ByPartyId(partyId))
                .OnSuccess(r => Mapper.Map<List<Campaign>, List<ListCampaignDto>>(r.ToList()))
                .Execute();

            var response = new GETResponseDto<List<ListCampaignDto>>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}