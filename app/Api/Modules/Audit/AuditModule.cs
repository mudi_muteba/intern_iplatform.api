using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Audit;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Audit
{
    public class AuditModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public AuditModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Post[SystemRoutes.Audit.Save.Route] = p => Save();

        }

        private Negotiator Save()
        {
            var createAuditDto = this.Bind<CreateAuditLogDto>();

            var result = executionPlan.Execute<CreateAuditLogDto, int>(createAuditDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Audit.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


    }
}