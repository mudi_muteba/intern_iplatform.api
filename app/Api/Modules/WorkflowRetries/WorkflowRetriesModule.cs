﻿using System.Linq;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.WorkflowRetries;
using Domain.WorkflowRetries.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.WorkflowRetries;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.WorkflowRetries
{
    public class WorkflowRetriesModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;

        public WorkflowRetriesModule(IExecutionPlan executionPlan)
        {
            _executionPlan = executionPlan;

            Post[SystemRoutes.WorkflowRetryLogs.Post.Route] = p => Create();
            Post[SystemRoutes.WorkflowRetryLogs.PostSearch.Route] = _ => Search();
        }

        private Negotiator Create()
        {
            var dto = this.Bind<CreateWorkflowRetryLogDto>();
            var result = _executionPlan.Execute<CreateWorkflowRetryLogDto, int>(dto);
            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var dto = this.Bind<SearchWorkflowRetryLogDto>();

            var queryResult = _executionPlan
                .Query<WorkflowRetryLogQuery, WorkflowRetryLog, ListResultDto<ListWorkflowRetryLogDto>, ListWorkflowRetryLogDto>()
                .Configure(query => query.WithCriteria(dto.CorrelationId, dto.RetryCount))
                .OnSuccess(result =>
                {
                    var list = new ListResultDto<ListWorkflowRetryLogDto>();
                    list.Results.AddRange(result.Select(x => new ListWorkflowRetryLogDto(x.CorrelationId, x.RetryCount, x.RetryLimit)));
                    return list;
                })
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}