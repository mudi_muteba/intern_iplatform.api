﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.ProposalDeclines;
using Domain.ProposalDeclines.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.ProposalDeclines;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.ProposalDeclines
{
    public class ProposalDeclinesModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;
        private readonly IRepository _repository;

        public ProposalDeclinesModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository)
        {
            this._executionPlan = executionPlan;
            this._paginator = paginator;
            this._repository = repository;

            Get[SystemRoutes.ProposalDecline.GetByPartyId.Route] = p => GetByPartyId(p.id);
            Post[SystemRoutes.ProposalDecline.Post.Route] = p => Save();
            Post[SystemRoutes.ProposalDecline.PostSearch.Route] = p => Search();

            Options[SystemRoutes.ProposalDecline.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ProposalDecline));
        }

        private Negotiator GetByPartyId(int partyId)
        {
            var queryResult = _executionPlan
                .Query
                <GetProposalDeclineByPartyIdQuery, ProposalDecline, ListResultDto<ProposalDeclineDto>,
                    ProposalDeclineDto>()
                .Configure(q => q.WithPartyId(partyId))
                .OnSuccess(r => Mapper.Map<List<ProposalDecline>, ListResultDto<ProposalDeclineDto>>(r.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.ProposalDecline.CreateLinks(c));

            return Negotiate
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Save()
        {
            var createProposalDeclineDto = this.Bind<CreateProposalDeclineDto>();

            var result = _executionPlan.Execute<CreateProposalDeclineDto, int>(createProposalDeclineDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ProposalDecline.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<ProposalDeclineSearchDto>();

            var queryResult = _executionPlan
                .Search
                <SearchProposalDeclineQuery, ProposalDecline, ProposalDeclineSearchDto, ListResultDto<ProposalDecline>>(
                    criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(r => Mapper.Map<List<ProposalDecline>, ListResultDto<ProposalDecline>>(r.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response);

            return Negotiate
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}