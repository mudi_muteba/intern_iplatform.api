﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.ProposalDeclines;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.ProposalDeclines;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.ProposalDeclines
{
    public class ProposalDeclineModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ProposalDeclineModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.paginator = paginator;
            this.repository = repository;

            Get[SystemRoutes.ProposalDecline.GetById.Route] = p => GetById(p.Id);
        }

        private Negotiator GetById(int id)
        {
            var result = executionPlan
                .GetById<ProposalDecline, ProposalDeclineDto>(() => repository.GetById<ProposalDecline>(id))
                .OnSuccess(Mapper.Map<ProposalDecline, ProposalDeclineDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ProposalDecline.CreateLinks(c));

            return  Negotiate
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}