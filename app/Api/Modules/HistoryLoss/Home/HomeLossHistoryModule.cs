﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.HistoryLoss.Home;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.HistoryLoss.Home
{
    public class HomeLossHistoryModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public HomeLossHistoryModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan,IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.HomeLossHistory.GetById.Route] = p => GetHomeLossHistory(p.id);

            Put[SystemRoutes.HomeLossHistory.Put.Route] = p => Update(p.id);

            Put[SystemRoutes.HomeLossHistory.PutDisable.Route] = p => DisableHomeLossHistory(p.id);
        }

        private Negotiator GetHomeLossHistory(int id)
        {
            var result = executionPlan
                .GetById<HomeLossHistory, ListHomeLossHistoryDto>(() => repository.GetById<HomeLossHistory>(id))
                .OnSuccess(Mapper.Map<HomeLossHistory, ListHomeLossHistoryDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.HomeLossHistory.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Update(int id)
        {
            var editHomeLossHistoryDto = this.Bind<EditHomeLossHistoryDto>();
            editHomeLossHistoryDto.Id = id;

            var result = executionPlan.Execute<EditHomeLossHistoryDto, int>(editHomeLossHistoryDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.HomeLossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableHomeLossHistory(int id)
        {
            var disableHomeLossHistoryDto = new DisableHomeLossHistoryDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableHomeLossHistoryDto, int>(disableHomeLossHistoryDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.HomeLossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}