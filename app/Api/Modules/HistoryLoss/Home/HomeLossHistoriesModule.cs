﻿
using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Campaigns.Queries;
using Domain.HistoryLoss.Home;
using Domain.HistoryLoss.Home.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.HistoryLoss.Home
{
    public class HomeLossHistoriesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public HomeLossHistoriesModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Options[SystemRoutes.HomeLossHistory.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.HomeLossHistory));

            Get[SystemRoutes.HomeLossHistory.GetAll.Route] = _ => GetAllHomeLossHistory(new Pagination());

            Post[SystemRoutes.HomeLossHistory.PostSearch.Route] = p  => Search();

            Get[SystemRoutes.Campaigns.GetWithPagination.Route] =
                p => GetAllHomeLossHistory(new Pagination(p.PageNumber, p.PageSize));

            Post[SystemRoutes.HomeLossHistory.Post.Route] = p => CreateHomeLossHistory();
        }

        private Negotiator CreateHomeLossHistory()
        {
            var createHomeLossHistoryDto = this.Bind<CreateHomeLossHistoryDto>();

            var result = executionPlan.Execute<CreateHomeLossHistoryDto, int>(createHomeLossHistoryDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.HomeLossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllHomeLossHistory(Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetAllHomeLossHistoryQuery, HomeLossHistory, PagedResults<HomeLossHistory>>()
                .OnSuccess(result => result.Paginate(paginator, pagination))
                .Execute();

            var campaignDtos = Mapper.Map<PagedResultDto<ListHomeLossHistoryDto>>(queryResult.Response);
            campaignDtos.Results =
                queryResult.Response.Results.Select(x => Mapper.Map<ListHomeLossHistoryDto>(x)).ToList();
            var response = queryResult.CreateLISTPagedResponse(campaignDtos)
                .CreateLinks(c => SystemRoutes.HomeLossHistory.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<SearchHomeLossHistoryDto>();

            var queryResult = executionPlan
                .Search
                <SearchHomeLossHistoryQuery, HomeLossHistory, SearchHomeLossHistoryDto,
                    PagedResultDto<ListHomeLossHistoryDto>>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(r =>
                {
                    var val = new PagedResultDto<ListHomeLossHistoryDto>();

                    r.ToList().Each(x =>
                    {
                        val.Results.Add(Mapper.Map<ListHomeLossHistoryDto>(x));
                    });

                    return val;
                })
                 .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.HomeLossHistory.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        //private Negotiator GetHomeLossHistoryByPartyId(int partyId)
        //{
        //    var result = executionPlan
        //        .Query<GetHomeLossHistoryByPartyIdQuery, HomeLossHistory, ListResultDto<ListHomeLossHistoryDto>, ListHomeLossHistoryDto>()
        //        .Configure(q => q.WithPartyId(partyId))
        //        .OnSuccess(r =>
        //        {
        //            var val = new ListResultDto<ListHomeLossHistoryDto>();
        //            r.ToList().Each(x =>
        //            {
        //                val.Results.Add(Mapper.Map<ListHomeLossHistoryDto>(x));
        //            });

        //            return val;
        //        })
        //        .Execute();

        //    LISTResponseDto<ListHomeLossHistoryDto> response = result.CreateLISTResponse(result.Response)
        //        .CreateLinks(c => SystemRoutes.HomeLossHistory.CreateLinks(c));

        //    return Negotiate.ForSupportedFormats(response, response.StatusCode);
        //}

    }
}