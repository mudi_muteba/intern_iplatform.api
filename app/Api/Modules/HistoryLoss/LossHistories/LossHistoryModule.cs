﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.HistoryLoss.LossHistory;
using Domain.JsonDataStores;
using Domain.JsonDataStores.Queries;
using Domain.Party.Addresses.Handlers;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.JsonDataStores;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.HistoryLoss.LossHistories
{
    public class LossHistoryModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;
        private readonly IRepository _repository;

        public LossHistoryModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository)
        {
            this._executionPlan = executionPlan;
            this._paginator = paginator;
            this._repository = repository;

            Options[SystemRoutes.LossHistory.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.LossHistory));

            Get[SystemRoutes.LossHistory.GetById.Route] = p => GetById(p.Id);
            Put[SystemRoutes.LossHistory.Put.Route] = p => UpdatebyId(p.Id);
            Put[SystemRoutes.LossHistory.PutDisable.Route] = p => DisableLossHistory(p.id);
        }

        public Negotiator GetById(int id)
        {
            var result = _executionPlan
                .GetById<LossHistory, LossHistoryDto>(() => _repository.GetById<LossHistory>(id))
                .OnSuccess(Mapper.Map<LossHistory, LossHistoryDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.LossHistory.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator UpdatebyId(int id)
        {
            var editLossHistoryDto = this.Bind<EditLossHistoryDto>();
            editLossHistoryDto.Id = id;

            var result = _executionPlan.Execute<EditLossHistoryDto, int>(editLossHistoryDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.LossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableLossHistory(int id)
        {
            var disableLossHistoryDto = new DisableLossHistoryDto()
            {
                Id = id
            };

            var result = _executionPlan.Execute<DisableLossHistoryDto, int>(disableLossHistoryDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.LossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}