﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.HistoryLoss.LossHistory;
using Domain.HistoryLoss.LossHistory.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.HistoryLoss.LossHistories
{
    public class LossHistoriesModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;
        private readonly IRepository _repository;

        public LossHistoriesModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository)
        {
            this._executionPlan = executionPlan;
            this._paginator = paginator;
            this._repository = repository;

            Options[SystemRoutes.LossHistory.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.LossHistory));

            Post[SystemRoutes.LossHistory.PostSearch.Route] = p => Search();
            Get[SystemRoutes.LossHistory.GetByPartyId.Route] = p => GetByPartyId(p.id);
            Post[SystemRoutes.LossHistory.Post.Route] = p => SaveLossHistory();
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<LossHistorySearchDto>();

            var queryResult = _executionPlan
                 .Search<SearchLossHistoryQuery, LossHistory, LossHistorySearchDto, PagedResultDto<LossHistoryDto>, LossHistoryDto>(criteria)
                 .Configure(query => query.WithCriteria(criteria))
                 .OnSuccess(result => Mapper.Map<PagedResultDto<LossHistoryDto>>(result.Paginate(_paginator, criteria.CreatePagination())))
                 .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.LossHistory.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetByPartyId(int partyId)
        {
            var queryResult = _executionPlan
                            .Query<GetLossHistoryByPartyId, LossHistory, ListResultDto<LossHistoryDto>, LossHistoryDto>()
                            .Configure(q => q.WithPartyId(partyId))
                            .OnSuccess(r => Mapper.Map<List<LossHistory>, ListResultDto<LossHistoryDto>>(r.ToList()))
                            .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                 .CreateLinks(c => SystemRoutes.LossHistory.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SaveLossHistory()
        {
            var createLossHistoryDto = this.Bind<CreateLossHistoryDto>();

            var result = _executionPlan.Execute<CreateLossHistoryDto, int>(createLossHistoryDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.LossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

    }
}