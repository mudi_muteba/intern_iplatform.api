﻿
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.HistoryLoss.Motor;
using Domain.HistoryLoss.Motor.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Collections.Generic;
using System.Linq;
using Domain.HistoryLoss.LossHistory.Queries;

namespace Api.Modules.HistoryLoss.Motor
{
    public class MotorLossHistoriesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public MotorLossHistoriesModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Options[SystemRoutes.MotorLossHistory.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.MotorLossHistory));

            Get[SystemRoutes.MotorLossHistory.GetAll.Route] = _ => GetAllMotorLossHistory(new Pagination());

            Get[SystemRoutes.Campaigns.GetWithPagination.Route] =
                p => GetAllMotorLossHistory(new Pagination(p.PageNumber, p.PageSize));

            //Get[SystemRoutes.MotorLossHistory.GetByPartyId.Route] = p => GetMotorLossHistoryByPartyId(p.id);

            Post[SystemRoutes.MotorLossHistory.Post.Route] = p => CreateMotorLossHistory();

            Post[SystemRoutes.MotorLossHistory.PostSearch.Route] = p => Search();

            //Post[SystemRoutes.Campaigns.PostSearch.Route] = p => SearchCampaigns();
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<SearchMotorLossHistoryDto>();

            var queryResult = executionPlan
                .Search
                <SearchMotorLossHistoryQuery, MotorLossHistory, SearchMotorLossHistoryDto,
                    PagedResultDto<ListMotorLossHistoryDto>>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(r =>
                {
                    var val = new PagedResultDto<ListMotorLossHistoryDto>();

                    r.ToList().Each(x =>
                    {
                        val.Results.Add(Mapper.Map<ListMotorLossHistoryDto>(x));
                    });

                    return val;
                })
                 .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.MotorLossHistory.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator CreateMotorLossHistory()
        {
            var createMotorLossHistoryDto = this.Bind<CreateMotorLossHistoryDto>();

            var result = executionPlan.Execute<CreateMotorLossHistoryDto, int>(createMotorLossHistoryDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.MotorLossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllMotorLossHistory(Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetAllMotorLossHistoryQuery, MotorLossHistory, PagedResults<MotorLossHistory>>()
                .OnSuccess(result => result.Paginate(paginator, pagination))
                .Execute();

            var campaignDtos = Mapper.Map<PagedResultDto<ListMotorLossHistoryDto>>(queryResult.Response);

            var response = queryResult.CreateLISTPagedResponse(campaignDtos)
                .CreateLinks(c => SystemRoutes.MotorLossHistory.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        //Deprecated use search
        //private Negotiator GetMotorLossHistoryByPartyId(int partyId)
        //{
        //    var result = executionPlan
        //        .Query<GetMotorLossHistoryByPartyIdQuery, MotorLossHistory, ListResultDto<ListMotorLossHistoryDto>, ListMotorLossHistoryDto>()
        //        .Configure(q => q.WithPartyId(partyId))
        //        .OnSuccess(r =>
        //        {
        //            var val = new ListResultDto<ListMotorLossHistoryDto>();
        //            r.ToList().Each(x =>
        //            {
        //                val.Results.Add(Mapper.Map<ListMotorLossHistoryDto>(x));
        //            });

        //            return val;
        //        })
        //        .Execute();

        //    LISTResponseDto<ListMotorLossHistoryDto> response = result.CreateLISTResponse(result.Response)
        //        .CreateLinks(c => SystemRoutes.MotorLossHistory.CreateLinks(c));

        //    return Negotiate.ForSupportedFormats(response, response.StatusCode);
        //}
    }
}