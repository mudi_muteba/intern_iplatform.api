﻿
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.HistoryLoss.Motor;
using Domain.HistoryLoss.Motor.Queries;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.HistoryLoss.Motor
{
    public class MotorLossHistoryModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public MotorLossHistoryModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan,IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.MotorLossHistory.GetById.Route] = p => GetMotorLossHistory(p.id);
            
            Put[SystemRoutes.MotorLossHistory.Put.Route] = p => Update(p.id);

            Put[SystemRoutes.MotorLossHistory.PutDisable.Route] = p => DisableMotorLossHistory(p.id);
        }


        private Negotiator GetMotorLossHistory(int id)
        {
            var result = executionPlan
                .GetById<MotorLossHistory, ListMotorLossHistoryDto>(() => repository.GetById<MotorLossHistory>(id))
                .OnSuccess(Mapper.Map<MotorLossHistory, ListMotorLossHistoryDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.MotorLossHistory.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Update(int id)
        {
            var editMotorLossHistoryDto = this.Bind<EditMotorLossHistoryDto>();
            editMotorLossHistoryDto.Id = id;

            var result = executionPlan.Execute<EditMotorLossHistoryDto, int>(editMotorLossHistoryDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.MotorLossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableMotorLossHistory(int id)
        {
            var disableMotorLossHistoryDto = new DisableMotorLossHistoryDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableMotorLossHistoryDto, int>(disableMotorLossHistoryDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.MotorLossHistory.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}