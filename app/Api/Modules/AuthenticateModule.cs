﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Extentions;
using Domain.Base.Repository;
using Domain.Users;
using Domain.Users.Authentication;
using iPlatform.Api.DTOs.ApiVersion;
using iPlatform.Api.DTOs.Audit;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Statistics;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Shared.Extentions;
using Workflow.Messages;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Api.Modules
{
    public class AuthenticateModule : NancyModule
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan _executionPlan;
        private readonly IWorkflowRouter _router;
        private readonly IProvideContext _contextProvider;

        public AuthenticateModule(IRepository repository, IExecutionPlan executionPlan, IWorkflowRouter router, IProvideContext contextProvider)
        {
            _repository = repository;
            _executionPlan = executionPlan;
            _router = router;
            _contextProvider = contextProvider;

            Options[SystemRoutes.Authenticate.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Authenticate));
            Post[SystemRoutes.Authenticate.Post.Route] = p => GenerateAuthentication(this.Bind<AuthenticationRequestDto>());
            Post[SystemRoutes.Authenticate.ByToken.Route] = p => GenerateAuthenticationByToken(this.Bind<AuthenticationRequestDto>());
            Get[SystemRoutes.Authenticate.GetIsAlive.Route] = p => GetIsAlive();
            Get[SystemRoutes.Authenticate.GetApiVersions.Route] = p => GetApiVersion();
        }

        private Negotiator GetIsAlive()
        {
            var getDayStatsDto = new GetDayStatsDto();
            getDayStatsDto.AgentId = 137;

            var result = _executionPlan.Execute<GetDayStatsDto, StatisticsDto>(getDayStatsDto);

            return Negotiate
                .WithLocationHeader("").
                ForJson("True");
        }

        private Negotiator GetApiVersion()
        {
            var packageVersion = ConfigurationManager.AppSettings[@"version/PackageVersion"];
            var nugetPackageVersion = ConfigurationManager.AppSettings[@"version/NugetPackageVersion"];
            var previousPackageVersion = ConfigurationManager.AppSettings[@"version/PreviousPackageVersion"];
            var environmentRelease = ConfigurationManager.AppSettings[@"version/EnvironmentRelease"];


            Assembly assembly = typeof(AuthenticateModule).Assembly;
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = versionInfo.FileVersion;


            var versions = new ApiVersionDto()
            {
                EnvironmentRelease = environmentRelease,
                PreviousPackageVersion = previousPackageVersion,
                NugetPackageVersion = nugetPackageVersion,
                PackageVersion = packageVersion,
                VersionApi = version
            };


            GETResponseDto< ApiVersionDto> response = new GETResponseDto<ApiVersionDto>(versions);
            return Negotiate.ForSupportedFormats(response, System.Net.HttpStatusCode.OK);
        }

        private Negotiator GenerateAuthenticationByToken(AuthenticationRequestDto dto)
        {
            var context = _contextProvider.Get();
            var entityFullName = typeof(User).FullName;

            var activeChannelId = dto.ActiveChannelId;
            if (activeChannelId < 1)
            {
                var error = "No active channel id supplied to authenticate";
                this.Error(() => error);
                _router.AddAuditLogEntry(new CreateAuditLogDto(context, "UserAuthenticationFailure", error, entityFullName, 0, DateTime.UtcNow));
                return Negotiate.WithStatusCode(HttpStatusCode.Unauthorized);
            }

            var oldToken = dto.Token;
            if (string.IsNullOrEmpty(oldToken))
            {
                var error = "No token for supplied to authenticate";
                this.Error(() => error);
                _router.AddAuditLogEntry(new CreateAuditLogDto(context, "UserAuthenticationFailure", error, entityFullName, 0, DateTime.UtcNow));
                return Negotiate.WithStatusCode(HttpStatusCode.Unauthorized);
            }

            var apiUser = new ApiUserBuilder().ExtractToken(oldToken).Set(_repository);
            if (!apiUser.IsAuthenticated)
            {
                var error = string.Format("Failed to get user by token '{0}'", oldToken);
                this.Error(() => error);
                _router.AddAuditLogEntry(new CreateAuditLogDto(context, "UserAuthenticationFailure", error, entityFullName, 0, DateTime.UtcNow));
                return Negotiate.WithStatusCode(HttpStatusCode.Unauthorized);
            }

            var user = _repository.GetById<User>(apiUser.UserId);
            if (user == null)
            {
                var error = string.Format("Failed to find user with user id '{0}' for authentication", apiUser.UserId);
                this.Error(() => error);
                _router.AddAuditLogEntry(new CreateAuditLogDto(context, "UserAuthenticationFailure", error, entityFullName, 0, DateTime.UtcNow));
                return Negotiate.WithStatusCode(HttpStatusCode.Unauthorized);
            }

            var newToken = user.GenerateToken(dto.RequestType, activeChannelId);
            if (dto.UserId != 0 && !string.IsNullOrEmpty(dto.Email))
                newToken = user.GenerateToken(dto.RequestType, activeChannelId);
            if (string.IsNullOrEmpty(newToken))
            {
                var error = string.Format("Failed to re-generate token for ActiveChannelId '{0}' token {1}", activeChannelId, newToken);
                _router.AddAuditLogEntry(new CreateAuditLogDto(context, "UserAuthenticationFailure", error, entityFullName, user.Id, DateTime.UtcNow));
                return Negotiate.WithStatusCode(HttpStatusCode.Unauthorized);
            }

            _repository.Save(user);

            var responseDto = new AuthenticateResponseDto
            {
                Token = newToken,
                User = Mapper.Map<UserDto>(user),
                IsAuthenticated = true
            };

            return Negotiate.WithHeader("Authorization", responseDto.Token).WithModel(responseDto);
        }

        private Negotiator GenerateAuthentication(AuthenticationRequestDto authenticationRequest)
        {
            authenticationRequest.IPAddress = Request.UserHostAddress;

            var username = authenticationRequest.Email;

            this.Info(() => string.Format("Finding user with username '{0}' for authentication", username));

            var queryResult = _executionPlan
                .Query<FindUserByUserNameForAuthenticationQuery, User, User>()
                .OnSuccess(r => r.FirstOrDefault())
                .Configure(q => q.WithUserName(username))
                .Execute();

            var context = _contextProvider.Get();
            var entityFullName = typeof(User).FullName;

            var user = queryResult.Response;
            if (user == null)
            {
                var error = string.Format("Failed to find user with user name '{0}' for authentication", username);
                this.Error(() => error);
                _router.AddAuditLogEntry(new CreateAuditLogDto(context, "UserAuthenticationFailure", error, entityFullName, 0, DateTime.UtcNow));
                return Negotiate.WithStatusCode(HttpStatusCode.Unauthorized);
            }

            var authenticateResponse = user.Authenticate(authenticationRequest);
            if (!authenticateResponse.IsAuthenticated)
            {
                var error = string.Format("Failed to validate password for user name '{0}'", username);
                _router.AddAuditLogEntry(new CreateAuditLogDto(context, "UserAuthenticationFailure", error, entityFullName, user.Id, DateTime.UtcNow));
                return Negotiate.WithStatusCode(HttpStatusCode.Unauthorized);
            }

            _repository.Save(user);

            var responseDto = new AuthenticateResponseDto
            {
                Token = authenticateResponse.Token,
                User = Mapper.Map<UserDto>(authenticateResponse.User),
                IsAuthenticated = authenticateResponse.IsAuthenticated
            };

            _router.AddAuditLogEntry(new CreateAuditLogDto(context, "UserAuthenticationSuccess", "User authenticated successfully", entityFullName, 
                user.Id, user.UserName, user.ChannelIdsString, 
                user.DefaultChannel == null ? 0 : user.DefaultChannel.Id,
                user.DefaultChannel == null ? Guid.Empty: user.DefaultChannel.SystemId, 
                DateTime.UtcNow));

            return Negotiate.WithHeader("Authorization", authenticateResponse.Token).WithModel(responseDto);
        }
    }
}