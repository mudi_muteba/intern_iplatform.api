﻿using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Users;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Search;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using iPlatform.Api.DTOs.Users.UserAuthorisationGroups;

namespace Api.Modules.Users
{
    public class UserAuthorisationsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public UserAuthorisationsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Post[SystemRoutes.UserAuthorisation.Post.Route] = p => Create();

            Options[SystemRoutes.Users.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.UserAuthorisation));

        }

        private Negotiator Create()
        {
            var dto = this.Bind<CreateUserAuthorisationGroupDto>();
            var result = _executionPlan.Execute<CreateUserAuthorisationGroupDto, int>(dto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.UserAuthorisation
                .CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

    }
}