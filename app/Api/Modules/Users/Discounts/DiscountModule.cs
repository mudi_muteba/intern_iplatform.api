﻿using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Users.Discounts;
using Domain.Users.Discounts.Queries;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Users.Discount;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Users.Discounts
{
    public class DiscountModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public DiscountModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            
            Delete[SystemRoutes.UserDiscounts.GetById.Route] = p => DeleteDiscount(p.userId, p.id);
            Put[SystemRoutes.UserDiscounts.GetById.Route] = p => UpdateDiscount(p.userId, p.id);
            Post[SystemRoutes.UserDiscounts.GetByCriteria.Route] = p => GetDiscountsByCriteria(p.userId);
        }
        
        private Negotiator DeleteDiscount(int userId, int id)
        {
            var deleteDiscountDto = new DeleteDiscountDto {UserId = userId, Id = id};

            HandlerResult<int> result = executionPlan.Execute<DeleteDiscountDto, int>(deleteDiscountDto);

            DELETEResponseDto<int> response = result.DeletePOSTResponse()
                .CreateLinks(SystemRoutes.UserDiscounts.GetDiscountsUrl(userId));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateDiscount(int userId, int id)
        {
            var editDiscountDto = this.Bind<EditDiscountDto>();
            editDiscountDto.UserId = userId;
            editDiscountDto.Id = id;

            HandlerResult<int> result = executionPlan.Execute<EditDiscountDto, int>(editDiscountDto);

            PUTResponseDto<int> response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.UserDiscounts.CreateGetById(userId, result.Response));


            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetDiscountsByCriteria(int userId)
        {
            var criteria = this.Bind<SearchDiscountDto>();
            criteria.UserId = userId;

            QueryResult<DiscountDto> queryResult = executionPlan
                .Query<GetDiscountsByCoverAndProductQuery, UserDiscountCoverDefinition, DiscountDto>()
                .Configure(q => q.WithCriteria(criteria))
                .OnSuccess(r => Mapper.Map<UserDiscountCoverDefinition, DiscountDto>(r.FirstOrDefault()))
                .Execute();

            GETResponseDto<DiscountDto> response = queryResult.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.UserDiscounts.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


    }
}