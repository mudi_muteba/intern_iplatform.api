﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Users.Discounts;
using Domain.Users.Discounts.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Users.Discount;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Users.Discounts
{
    public class DiscountsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;


        public DiscountsModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Post[SystemRoutes.UserDiscounts.PostCreateDiscount.Route] = p => CreateDiscount(p.userId);
            Get[SystemRoutes.UserDiscounts.GetDiscountsByUserAndChannel.Route] = p => GetDiscounts(p.userId, p.channelId);
        }


        private Negotiator CreateDiscount(int userId)
        {
            var createDiscountDto = this.Bind<CreateDiscountDto>();
            createDiscountDto.UserId = userId;

            HandlerResult<int> result = executionPlan.Execute<CreateDiscountDto, int>(createDiscountDto);

            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.UserDiscounts.CreateGetById(userId, result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetDiscounts(int userId, int channelId)
        {
            QueryResult<List<ListDiscountDto>> queryResult = executionPlan
                .Query<GetDiscountsByUserIdQuery, UserDiscountCoverDefinition, List<ListDiscountDto>>()
                .Configure(q => q.WithUserIdAndChannelId(userId, channelId))
                .OnSuccess(r => Mapper.Map<List<UserDiscountCoverDefinition>, List<ListDiscountDto>>(r.ToList()))
                .Execute();

            var result = new ListResultDto<ListDiscountDto> {Results = queryResult.Response};

            LISTResponseDto<ListDiscountDto> response = queryResult.CreateLISTResponse(result)
                .CreateLinks(c => SystemRoutes.UserDiscounts.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}