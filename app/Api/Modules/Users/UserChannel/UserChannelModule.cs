﻿using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Users;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Users.UserChannel
{
    public class UserChannelModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public UserChannelModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Post[SystemRoutes.UserChannel.Save.Route] = p => SaveMultiple();
            Post[SystemRoutes.UserChannel.Post.Route] = p => CreateUserChannel();
            Get[SystemRoutes.UserChannel.GetByUserId.Route] = p => GetUserChannel(p.userid);
            Post[SystemRoutes.UserChannel.Search.Route] = p => Search();
        }

        public Negotiator CreateUserChannel()
        {
            var createUserChannelDto = this.Bind<CreateUserChannelDto>();

            var result = _executionPlan.Execute<CreateUserChannelDto, int>(createUserChannelDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.UserChannel.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetUserChannel(int userId)
        {
            var criteria = new UserChannelSearchDto { UserId = userId };
            var queryResult = _executionPlan
                .Search<GetUserChannelByUserIdQuery, Domain.Users.UserChannel, UserChannelSearchDto, ListResultDto<ListUserChannelDto>, ListUserChannelDto>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(result => Mapper.Map<ListResultDto<ListUserChannelDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateGETResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<UserChannelSearchDto>();
            var queryResult = _executionPlan
                .Search<GetUserChannelByUserIdQuery, Domain.Users.UserChannel, UserChannelSearchDto, ListResultDto<ListUserChannelDto>, ListUserChannelDto>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(result => Mapper.Map<ListResultDto<ListUserChannelDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator SaveMultiple()
        {
            var saveUserChannelsDto = this.Bind<SaveUserChannelsDto>();

            var result = _executionPlan.Execute<SaveUserChannelsDto, bool>(saveUserChannelsDto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        
    }
}