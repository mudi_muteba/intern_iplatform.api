﻿using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Users;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Search;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Collections.Generic;

namespace Api.Modules.Users
{
    public class UsersModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;
        private readonly IProvideContext _contextProvider;

        public UsersModule(IPaginateResults paginator, IExecutionPlan executionPlan, IProvideContext contextProvider)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;
            _contextProvider = contextProvider;

            Post[SystemRoutes.Users.Post.Route] = p => CreateUser();

            Get[SystemRoutes.Users.GetAllUsers.Route] = _ => GetAllUsers();

            Get[SystemRoutes.Users.GetAll.Route] = _ => GetAll(new Pagination());

            Get[SystemRoutes.Users.GetAllWithPagination.Route] = p => GetAll(new Pagination(p.PageNumber, p.PageSize));

            Post[SystemRoutes.Users.Search.Route] = _ => SearchUsers();

            Post[SystemRoutes.Users.SearchNoDefaultChannel.Route] = _ => SearchUsersNoDefaultChannel();

            Post[SystemRoutes.Users.Register.Route] = p => Register();

            Post[SystemRoutes.Users.RegisterAPIUser.Route] = p => RegisterAPIUser();

            Put[SystemRoutes.Users.ValidatePasswordResetToken.Route] = p => ValidatePasswordResetToken();

            Put[SystemRoutes.Users.RequestPasswordReset.Route] = p => RequestPasswordReset();

            Put[SystemRoutes.Users.ResetPassword.Route] = p => ResetPassword();

            Get[SystemRoutes.Users.GetAllBrokerUsers.Route] = p => GetAllBrokerUsers(p.channelId);

            Get[SystemRoutes.Users.GetAllAccountExecutiveUsers.Route] = p => GetAllAccountExecutiveUsers(p.channelId);

            Options[SystemRoutes.Users.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Users));

            Get[SystemRoutes.Users.GetAllUsersByChannelId.Route] = p => GetAllUsersByChannelId(p.channelId);

            Post[SystemRoutes.Users.GetCampaignUsers.Route] = p => GetCampaignUsers();
            
        }

        private Negotiator SearchUsers()
        {
            var criteria = this.Bind<UserSearchDto>();

            var queryResult = _executionPlan
                .Search<SearchUsersQuery, User, UserSearchDto, PagedResultDto<ListUserDto>, ListUserDto>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(result => 
                {
                    var users = Mapper.Map<PagedResultDto<ListUserDto>>(result.Paginate(_paginator, criteria.CreatePagination()));
                    //Remove discounts that are associated to the user but not the current channel
                    users.Results.ForEach(u => 
                    {
                        u.Discounts = u.Discounts.Where(d => d.ChannelId == _contextProvider.Get().ActiveChannelId).ToList();
                    });                    
                    
                    return users;
                })
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Users.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchUsersNoDefaultChannel()
        {
            var criteria = this.Bind<UserSearchDto>();

            var queryResult = _executionPlan
                .Search<SearchUsersNoDefaultChannelQuery, User, UserSearchDto, PagedResultDto<ListUserDto>, ListUserDto>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(result =>
                {
                    var users = Mapper.Map<PagedResultDto<ListUserDto>>(result.Paginate(_paginator, criteria.CreatePagination()));
                    //Remove discounts that are associated to the user but not the current channel
                    users.Results.ForEach(u =>
                    {
                        u.Discounts = u.Discounts.Where(d => d.ChannelId == _contextProvider.Get().ActiveChannelId).ToList();
                    });

                    return users;
                })
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Users.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAll(Pagination pagination = null)
        {
            pagination = pagination ?? this.Bind<Pagination>();

            var queryResult = _executionPlan
                .Query<GetAllUsersQuery, User, PagedResultDto<ListUserDto>, ListUserDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListUserDto>>(result.Paginate(_paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Users.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllUsers()
        {
            var queryResult = _executionPlan
                .Query<GetAllUsersQuery, User, ListResultDto<ListUserDto>, ListUserDto>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListUserDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Users.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateUser()
        {
            var createUser = this.Bind<CreateUserDto>();
            var result = _executionPlan.Execute<CreateUserDto, int>(createUser);
            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Register()
        {
            var registerRequest = this.Bind<RegisterUserDto>();
            registerRequest.ChannelId = (Context.CurrentUser as ApiUser).Channels.FirstOrDefault();
            var result = _executionPlan.Execute<RegisterUserDto, int>(registerRequest);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator RegisterAPIUser()
        {
            var registerRequest = this.Bind<RegisterApiUserDto>();
            var result = _executionPlan.Execute<RegisterApiUserDto, int>(registerRequest);
            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator ValidatePasswordResetToken()
        {
            var model = this.Bind<ValidatePasswordResetTokenDto>();
            var result = _executionPlan.Execute<ValidatePasswordResetTokenDto, ValidatePasswordResetTokenDto>(model);

            return Negotiate.ForSupportedFormats(result.Response, result.Response.StatusCode);
        }

        private Negotiator RequestPasswordReset()
        {
            var passwordResetRequest = this.Bind<RequestPasswordResetDto>();

            var result = _executionPlan.Execute<RequestPasswordResetDto, RequestPasswordResetResponseDto>(passwordResetRequest);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response.Id));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator ResetPassword()
        {
            var resetPasswordDto = this.Bind<ResetPasswordDto>();

            var result = _executionPlan.Execute<ResetPasswordDto, int>(resetPasswordDto);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllBrokerUsers(int channelId)
        {
            var queryResult = _executionPlan
                .Query<GetAllBrokerUsersQuery, User, ListResultDto<ListUserDto>, ListUserDto>()
                .Configure(q => q.WithChannelId(channelId))
                .OnSuccess(result => Mapper.Map<ListResultDto<ListUserDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Users.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllAccountExecutiveUsers(int channelId)
        {
            var queryResult = _executionPlan
                .Query<GetAllAccountExecutiveUsersQuery, User, ListResultDto<ListUserDto>, ListUserDto>()
                .Configure(q => q.WithChannelId(channelId))
                .OnSuccess(result => Mapper.Map<ListResultDto<ListUserDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Users.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllUsersByChannelId(int channelId)
        {
            var queryResult = _executionPlan
                .Query<GetAllUsersQuery, User, ListResultDto<ListUserDto>, ListUserDto>()
                .Configure(c => c.WithChannelId(channelId))
                .OnSuccess(result => Mapper.Map<ListResultDto<ListUserDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Users.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetCampaignUsers()
        {
            var dto = this.Bind<GetCampaignUsersDto>();

            var result = _executionPlan.Execute<GetCampaignUsersDto, ListResultDto<UserInfoDto>>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}