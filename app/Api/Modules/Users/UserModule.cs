﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Users;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Users;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Collections.Generic;
using System.Linq;
namespace Api.Modules.Users
{
    public class UserModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public UserModule(IRepository repository, IExecutionPlan executionPlan)
        {
            _repository = repository;
            _executionPlan = executionPlan;

            Get[SystemRoutes.Users.GetById.Route] = p => GetUserById(p.id);

            Get[SystemRoutes.Users.GetByName.Route] = p => { return null; };

            Put[SystemRoutes.Users.Approve.Route] = p => ApproveUser(p.id);

            Put[SystemRoutes.Users.Disable.Route] = p => DisableUser(p.id);

            Put[SystemRoutes.Users.Put.Route] = p => EditUser();
        }


        private Negotiator GetUserByUserName(string username)
        {
            var result = _executionPlan
                .Query<FindUserByUserNameQuery, User, ListResultDto<UserInfoDto>>()
                .Configure(q => q.WithUserName(username))
                .OnSuccess(r => Mapper.Map<List<User>, ListResultDto<UserInfoDto>>(r.ToList()))
                .Execute();

            var response = result.CreateLISTResponse(result.Response)
                .CreateLinks(c => SystemRoutes.Users.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator EditUser()
        {
            var editUser = this.Bind<EditUserDto>();
            var result = _executionPlan.Execute<EditUserDto, int>(editUser);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableUser(int id)
        {
            var disableUser = new DisableUserDto()
            {
                Id = id
            };

            var result = _executionPlan.Execute<DisableUserDto, int>(disableUser);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator ApproveUser(int id)
        {
            var approveUserDto = this.Bind<ApproveUserDto>();
            approveUserDto.Id = id;

            var result = _executionPlan.Execute<ApproveUserDto, int>(approveUserDto);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetUserById(int id)
        {
            var result = _executionPlan
                .GetById<User, UserDto>(() => _repository.GetById<User>(id))
                .OnSuccess(Mapper.Map<User, UserDto>)
                .Execute();

            var response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.Users.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}