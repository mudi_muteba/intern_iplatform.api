﻿using Api.Models;
using Api.Infrastructure;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FuneralDualCover;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Nancy.ModelBinding;

namespace Api.Modules.FuneralCover
{
    public class FuneralDualCoverModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;

        public FuneralDualCoverModule(IExecutionPlan executionPlan)
        {
            this.executionPlan = executionPlan;

            Get[SystemRoutes.FuneralDualCover.Get.Route] = p => GetFuneralDualCover(p.idNumber, p.sumInsured);

            Options[SystemRoutes.FuneralDualCover.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.FuneralDualCover));
        }

        public Negotiator GetFuneralDualCover(string idNumber, string sumInsured)
        {
            var dto = this.Bind<GetFuneralDualCoverDto>();

            var result = executionPlan.Execute<GetFuneralDualCoverDto, FuneralDualCoverDto>(dto);

            var response = new GETResponseDto<FuneralDualCoverDto>(result.Response)
                .CreateLinks(c => SystemRoutes.FuneralDualCover.CreateGet(idNumber, Convert.ToDecimal(sumInsured)));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}