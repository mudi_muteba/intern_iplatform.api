﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Escalations;
using Domain.Escalations.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Canaries;
using iPlatform.Api.DTOs.Escalations;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Canaries
{
    /// <summary>
    /// Testing purposes, should not affect live data
    /// </summary>
    public class CanariesModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public CanariesModule(IExecutionPlan executionPlan, IPaginateResults paginator)
        {
            _executionPlan = executionPlan;
            _paginator = paginator;

            Post[SystemRoutes.Canaries.GetAllWithPagination.Route] = _ => GetAll(new Pagination(_.pageNumber, _.pageSize));
            Post[SystemRoutes.Canaries.Post.Route] = _ => Create();
        }

        private Negotiator GetAll(Pagination pagination = null)
        {
            pagination = pagination ?? this.Bind<Pagination>();

            var queryResult = _executionPlan
                .Query<FindEscalationPlanExecutionHistoryQuery, EscalationPlanExecutionHistory, PagedResultDto<EscalationHistoryDto>, EscalationHistoryDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<EscalationHistoryDto>>(result.Paginate(_paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.EscalationPlanExecutionHistory.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Create()
        {
            var dto = this.Bind<CreateCanaryConsumerResultDto>();
            var result = _executionPlan.Execute<CreateCanaryConsumerResultDto, int>(dto);
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Users.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}