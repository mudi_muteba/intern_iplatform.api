﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FormBuilder;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.FormBuilder
{
    public class FormBuilderModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IRepository m_Repository;

        public FormBuilderModule(IExecutionPlan executionPlan, IRepository repository)
        {
            m_ExecutionPlan = executionPlan;
            m_Repository = repository;

            Get[SystemRoutes.FormBuilder.GetById.Route] = p => GetById(p.id);
            Get[SystemRoutes.FormBuilderDetail.GetById.Route] = p => GetFormDetailById(p.idformdetail);
            Put[SystemRoutes.FormBuilder.Put.Route] = p => UpdateFormBuilder(p.id);
            Put[SystemRoutes.FormBuilderDetail.Put.Route] = p => UpdateFormBuilderDetail(p.id, p.idformdetail);
            Put[SystemRoutes.FormBuilder.PutDelete.Route] = p => DeleteFormBuilder(p.id);
            Put[SystemRoutes.FormBuilderDetail.PutDelete.Route] = p => DeleteFormBuilderDetail(p.idformdetail);
        }

        private Negotiator GetById(int id)
        {
            var result = m_ExecutionPlan
                .GetById<Domain.FormBuilder.FormBuilder, FormBuilderDto>(() => m_Repository.GetById<Domain.FormBuilder.FormBuilder>(id))
                .OnSuccess(Mapper.Map<Domain.FormBuilder.FormBuilder, FormBuilderDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.FormBuilder.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetFormDetailById(int idformdetail)
        {
            var result = m_ExecutionPlan
                .GetById<Domain.FormBuilder.FormBuilderDetail, FormBuilderDetailDto>(() => m_Repository.GetById<Domain.FormBuilder.FormBuilderDetail>(idformdetail))
                .OnSuccess(Mapper.Map<Domain.FormBuilder.FormBuilderDetail, FormBuilderDetailDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.FormBuilderDetail.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateFormBuilder(int id)
        {
            var editFormBuilderDto = this.Bind<EditFormBuilderDto>();
            editFormBuilderDto.Id = id;

            var result = m_ExecutionPlan.Execute<EditFormBuilderDto, int>(editFormBuilderDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.FormBuilder.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateFormBuilderDetail(int id, int idformdetail)
        {
            var editFormBuilderDetailDto = this.Bind<EditFormBuilderDetailDto>();
            editFormBuilderDetailDto.Id = idformdetail;
            editFormBuilderDetailDto.IdForm = id;

            var result = m_ExecutionPlan.Execute<EditFormBuilderDetailDto, int>(editFormBuilderDetailDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.FormBuilderDetail.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteFormBuilder(int id)
        {
            var disableFormBuilderDto = this.Bind<DisableFormBuilderDto>();
            disableFormBuilderDto.Id = id;

            var result = m_ExecutionPlan.Execute<DisableFormBuilderDto, int>(disableFormBuilderDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.FormBuilder.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteFormBuilderDetail(int idformdetail)
        {
            var disableFormBuilderDetailDto = this.Bind<DisableFormBuilderDetailDto>();
            disableFormBuilderDetailDto.Id = idformdetail;

            var result = m_ExecutionPlan.Execute<DisableFormBuilderDetailDto, int>(disableFormBuilderDetailDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.FormBuilderDetail.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}