﻿using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.FormBuilder.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FormBuilder;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.FormBuilder
{
    public class FormBuildersModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IPaginateResults m_Paginator;

        public FormBuildersModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            m_Paginator = paginator;
            m_ExecutionPlan = executionPlan;

            Post[SystemRoutes.FormBuilder.Post.Route] = p => Create();
            Post[SystemRoutes.FormBuilderDetail.Post.Route] = p => CreateFormBuilderDetail();
            Get[SystemRoutes.FormBuilder.GetNoPagination.Route] = p => GetAllNoPagination();
            Post[SystemRoutes.FormBuilder.Search.Route] = _ => Search();
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<SearchFormBuilderDto>();

            var queryResult = m_ExecutionPlan
                .Search<SearchFormBuilderQuery, Domain.FormBuilder.FormBuilder, SearchFormBuilderDto, PagedResultDto<FormBuilderDto>, FormBuilderDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<FormBuilderDto>>(
                    result.Paginate(m_Paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.FormBuilder.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = m_ExecutionPlan
                .Query<GetAllFormBuilderQuery, Domain.FormBuilder.FormBuilder, ListResultDto<FormBuilderDto>>()
                .OnSuccess(result => Mapper.Map<ListResultDto<FormBuilderDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.FormBuilder.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator Create()
        {
            var createFormBuilderDto = this.Bind<CreateFormBuilderDto>();

            var result = m_ExecutionPlan.Execute<CreateFormBuilderDto, int>(createFormBuilderDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.FormBuilder.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator CreateFormBuilderDetail()
        {
            var createFormBuilderDetailDto = this.Bind<CreateFormBuilderDetailDto>();

            var result = m_ExecutionPlan.Execute<CreateFormBuilderDetailDto, int>(createFormBuilderDetailDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.FormBuilderDetail.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
    }
}