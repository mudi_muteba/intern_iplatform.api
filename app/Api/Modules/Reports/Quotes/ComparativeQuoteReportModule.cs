﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;

using AutoMapper;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Reports.Base;
using Domain.Reports.Base.Queries;
using Domain.Reports.Base.StoredProcedures;
using Domain.Reports.ComparativeQuote;
using Domain.Reports.ComparativeQuote.Queries;
using Domain.Reports.ComparativeQuote.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.ContactUs;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;
using iPlatform.Api.DTOs.Reports.Base;

using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Domain.Base.Extentions;
using Shared;
using MasterData;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Base;
using Common.Logging;

namespace Api.Modules.Reports.Quotes
{
    public class ComparativeQuoteReportModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IRepository m_Repository;
        private readonly IRootPathProvider m_Root;
        private readonly IProvideContext m_Context;
        private static readonly ILog m_Log = LogManager.GetLogger(typeof(ComparativeQuoteReportModule));

        public ComparativeQuoteReportModule(IExecutionPlan executionPlan, IRepository repository, IRootPathProvider root, IProvideContext context)
        {
            this.m_ExecutionPlan = executionPlan;
            this.m_Repository = repository;
            this.m_Root = root;
            this.m_Context = context;

            Post[SystemRoutes.ComparativeQuoteReports.GetComparativeQuoteReport.Route] = x => GetComparativeQuoteReport();
            Post[SystemRoutes.ComparativeQuoteReports.EmailComparativeQuoteReport.Route] = x => EmailComparativeQuoteReport();
            Post[SystemRoutes.ComparativeQuoteReports.ContactUsEmail.Route] = x => ContactUsEmail();
        }

        private Negotiator GetComparativeQuoteReport()
        {
            var criteria = this.Bind<ComparativeQuoteReportCriteriaDto>();

            OverrideCurrencySymbol(criteria, Thread.CurrentThread.CurrentCulture);

            var reportLayoutQueryResult = m_ExecutionPlan
                .QueryStoredProcedure<GetReportLayoutQuery, ReportLayout, ReportLayoutStoredProcedure, ReportLayoutDto>(new ReportLayoutStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<ReportLayout>, IList<ReportLayoutDto>>)
                .Execute();

            var headerQueryResult = m_ExecutionPlan
                .QueryStoredProcedure<GetComparativeQuoteReportHeaderQuery, ComparativeQuoteReportHeader, ComparativeQuoteReportHeaderStoredProcedure, ComparativeQuoteReportHeaderDto>(new ComparativeQuoteReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportHeader>, IList<ComparativeQuoteReportHeaderDto>>)
                .Execute();

            var summaryQueryResult = m_ExecutionPlan
                .QueryStoredProcedure<GetComparativeQuoteReportSummaryQuery, ComparativeQuoteReportSummary, ComparativeQuoteReportSummaryStoredProcedure, ComparativeQuoteReportSummaryDto>(new ComparativeQuoteReportSummaryStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportSummary>, IList<ComparativeQuoteReportSummaryDto>>)
                .Execute();

            var vapsQueryResult = m_ExecutionPlan
                .QueryStoredProcedure<GetComparativeQuoteReportValueAddedProductQuery, ComparativeQuoteReportValueAddedProduct, ComparativeQuoteReportValueAddedProductStoredProcedure, ComparativeQuoteReportValueAddedProductDto>(new ComparativeQuoteReportValueAddedProductStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportValueAddedProduct>, IList<ComparativeQuoteReportValueAddedProductDto>>)
                .Execute();

            var coversQueryResult = m_ExecutionPlan
                .QueryStoredProcedure<GetComparativeQuoteReportCoverQuery, ComparativeQuoteReportCover, ComparativeQuoteReportCoverStoredProcedure, ComparativeQuoteReportCoverDto>(new ComparativeQuoteReportCoverStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportCover>, IList<ComparativeQuoteReportCoverDto>>)
                .Execute();

            var productAddendumQueryResult = m_ExecutionPlan
                .QueryStoredProcedure<GetComparativeQuoteReportProductAddendumQuery, ComparativeQuoteReportProductAddendum, ComparativeQuoteReportProductAddendumStoredProcedure, ComparativeQuoteReportProductAddendumDto>(new ComparativeQuoteReportProductAddendumStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportProductAddendum>, IList<ComparativeQuoteReportProductAddendumDto>>)
                .Execute();

            if (criteria.OrganizationCode.ToLower() == "uap")
            {
                var questions = new string[] { "forced atm withdrawal", "loss of personal effects", "out of station accommodation" };
                criteria.QuestionIds = string.Join(",", new MasterData.Questions().Where(x => questions.Contains(x.Name, StringComparer.OrdinalIgnoreCase)).Select(x => x.Id).ToList());
            }

            var additionalBenefistQueryResult = m_ExecutionPlan
                .QueryStoredProcedure<GetComparativeQuoteReportAdditionalBenefitQuery, ComparativeQuoteReportAdditionalBenefit, ComparativeQuoteReportAdditionalBenefitStoredProcedure, ComparativeQuoteReportAdditionalBenefitDto>(new ComparativeQuoteReportAdditionalBenefitStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportAdditionalBenefit>, IList<ComparativeQuoteReportAdditionalBenefitDto>>)
                .Execute();

            List<ComparativeQuoteReportAdditionalBenefitDto> additionalBenefitsList = additionalBenefistQueryResult.Response == null ?
                                        new List<ComparativeQuoteReportAdditionalBenefitDto>() :
                                        additionalBenefistQueryResult.Response.ToList();



            var products = summaryQueryResult.Response.GroupBy(x => x.Product).Select(x => x.First()).ToList();
            var distinctSummary = new List<ComparativeQuoteReportSummaryDto>();

            products.ForEach(x =>
            {
                var queryFees = summaryQueryResult.Response.Where(y => y.Product == x.Product && y.Fees > 0).FirstOrDefault();
                decimal fees = (queryFees != null) ? queryFees.Fees : 0;

                distinctSummary.Add(new ComparativeQuoteReportSummaryDto
                {
                    Insurer = x.Insurer,
                    Code = x.Code,
                    Product = x.Product,
                    BasicExcess = summaryQueryResult.Response.Where(y => y.Product == x.Product).Sum(y => y.BasicExcess),
                    Premium = summaryQueryResult.Response.Where(y => y.Product == x.Product).Sum(y => y.Premium),
                    SASRIA = summaryQueryResult.Response.Where(y => y.Product == x.Product).Sum(y => y.SASRIA),
                    Fees = fees,
                    Total = summaryQueryResult.Response.Where(y => y.Product == x.Product).Sum(y => y.Total),
                    Commission = summaryQueryResult.Response.Where(y => y.Product == x.Product).Sum(y => y.Commission)
                });
            });

            

            ComparativeQuoteReportContainerDto comparativequote = new ComparativeQuoteReportContainerDto();
            comparativequote.Settings = Mapper.Map<IList<ReportLayoutDto>, ComparativeQuoteReportSettingsDto>(reportLayoutQueryResult.Response) ?? default(ComparativeQuoteReportSettingsDto);
            comparativequote.Header = headerQueryResult.Response.FirstOrDefault() ?? default(ComparativeQuoteReportHeaderDto);
            comparativequote.Summary = distinctSummary;
            comparativequote.ValueAddedProducts = vapsQueryResult.Response.ToList() ?? default(List<ComparativeQuoteReportValueAddedProductDto>);
            comparativequote.Covers = GetCovers(coversQueryResult.Response.ToList(), criteria);
            comparativequote.ProductAddendums = productAddendumQueryResult.Response.ToList() ?? default(List<ComparativeQuoteReportProductAddendumDto>);
            comparativequote.AdditionalBenefits = FormatAdditionalBenefits(additionalBenefitsList);

            var result = new QueryResult<ComparativeQuoteReportContainerDto>(comparativequote);


            result.Response.DisplayExcess = (result.Response.Covers.Any(x => x.Organizations.Count > 0)) ? true : false;
            result.Response.DisplayValueAddedProducts = (result.Response.ValueAddedProducts.Count > 0) ? true : false;

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private List<ComparativeQuoteReportInsurerCoverBenefitDto> FormatTotals(List<ComparativeQuoteReportInsurerCoverBenefitDto> totals)
        {
            var formatted = totals.Select(x =>
            {
                decimal first, second, third;

                var culture = Thread.CurrentThread.CurrentCulture;
                var seperator = culture.NumberFormat.CurrencyDecimalSeparator;

                if (decimal.TryParse(x.FirstSelectedInsurerBenefitValue.Replace(".", seperator), out first))
                {
                    x.FirstSelectedInsurerBenefitValue = (x.FirstSelectedInsurerBenefitValue == "0.00") ? "" : string.Format("{0:C}", first);
                }

                if (decimal.TryParse(x.SecondSelectedInsurerBenefitValue.Replace(".", seperator), out second))
                {
                    x.SecondSelectedInsurerBenefitValue = (x.SecondSelectedInsurerBenefitValue == "0.00") ? "" : string.Format("{0:C}", second);
                }

                if (decimal.TryParse(x.ThirdSelectedInsurerBenefitValue.Replace(".", seperator), out third))
                {
                    x.ThirdSelectedInsurerBenefitValue = (x.ThirdSelectedInsurerBenefitValue == "0.00") ? "" : string.Format("{0:C}", third);
                }

                return x;
            }).ToList();

            return formatted;
        }

        private decimal GetDecimalValue(string value)
        {
            decimal result = 0;

            var culture = Thread.CurrentThread.CurrentCulture;
            var seperator = culture.NumberFormat.CurrencyDecimalSeparator;

            decimal.TryParse(value.Replace(".", seperator), out result);

            return result;
        }

        private string GetTextDescription(string value)
        {
            int n;
            return (int.TryParse(value, out n)) ? string.Empty : value;
        }

        public virtual void OverrideCurrencySymbol(ComparativeQuoteReportCriteriaDto criteria, CultureInfo cultureInfo)
        {
            switch (criteria.OrganizationCode.ToLower())
            {
                case "uap":
                    var culture = new CultureInfo("sw-KE");
                    culture.NumberFormat.CurrencySymbol = "Ksh";

                    Thread.CurrentThread.CurrentCulture = culture;

                    break;
            }
        }

        private List<ComparativeQuoteReportAdditionalBenefitDto> FormatAdditionalBenefits(List<ComparativeQuoteReportAdditionalBenefitDto> totals)
        {
            if(!totals.Any())
            {
                return totals;
            }

            var formatted = totals.Select(x =>
            {
                decimal value;

                var culture = Thread.CurrentThread.CurrentCulture;
                var seperator = culture.NumberFormat.CurrencyDecimalSeparator;

                var isNumeric = decimal.TryParse(x.Value.Replace(" ", "").Replace(".", seperator), out value);

                if (isNumeric)
                    x.Value = string.Format("{0:C}", value);

                return x;
            }).ToList();

            return formatted;
        }

        private Negotiator EmailComparativeQuoteReport()
        {
            var criteria = this.Bind<ComparativeQuoteReportEmailDto>();
            criteria.PathMap = m_Root.GetRootPath();

            var result = m_ExecutionPlan.Execute<ComparativeQuoteReportEmailDto, ReportEmailResponseDto>(criteria);
            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator ContactUsEmail()
        {
            var criteria = this.Bind<ContactUsEmailDto>();

            var result = m_ExecutionPlan.Execute<ContactUsEmailDto, ReportEmailResponseDto>(criteria);
            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private List<ComparativeQuoteReportCoverDto> GetCovers(List<ComparativeQuoteReportCoverDto> coversQueryResult, ComparativeQuoteReportCriteriaDto criteria)
        {
            List<ComparativeQuoteReportCoverDto> covers = new List<ComparativeQuoteReportCoverDto>();

            foreach(ComparativeQuoteReportCoverDto cover in coversQueryResult)
            { 
                criteria.CoverId = cover.CoverId;

                cover.Organizations = m_ExecutionPlan
                    .QueryStoredProcedure<GetComparativeQuoteReportOrganizationQuery, ComparativeQuoteReportOrganization, ComparativeQuoteReportOrganizationStoredProcedure, ComparativeQuoteReportOrganizationDto>(new ComparativeQuoteReportOrganizationStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportOrganization>, IList<ComparativeQuoteReportOrganizationDto>>)
                    .Execute().Response.ToList();

                cover.DisplayExcess = (cover.Organizations.Count > 0) ? true : false;

                if (cover.Organizations.Count != 0)
                {
                    cover.Organizations.Each(y =>
                    {
                        var coverDefinitionIds = new List<int>();

                        if (y.CoverDefinitionIds != null && y.CoverDefinitionIds.Length > 0 && y.CoverDefinitionIds.IndexOf(',') > 0)
                        {
                            coverDefinitionIds = y.CoverDefinitionIds.Split(',').Select(int.Parse).ToList();
                        }
                        else if (y.CoverDefinitionIds != null && y.CoverDefinitionIds.Length > 0 && y.CoverDefinitionIds.IndexOf(',') == -1)
                        {
                            coverDefinitionIds.Add(int.Parse(y.CoverDefinitionIds));
                        }
                            

                        coverDefinitionIds.Each(d =>
                        {
                            criteria.CoverDefinitionId = d;

                            y.AdditionalExcessCategories = m_ExecutionPlan
                                .QueryStoredProcedure<GetComparativeQuoteReportInsurerCoverAdditionalExcessCategoriesQuery, ComparativeQuoteReportProductAdditionalExcessCategory, ComparativeQuoteReportInsurerCoverAdditionalExcessCategoriesStoredProcedure, ComparativeQuoteReportProductAdditionalExcessCategoryDto>(new ComparativeQuoteReportInsurerCoverAdditionalExcessCategoriesStoredProcedure(criteria))
                                .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportProductAdditionalExcessCategory>, IList<ComparativeQuoteReportProductAdditionalExcessCategoryDto>>)
                                .Execute().Response.ToList();

                            y.AdditionalExcessCategories.Each(z =>
                            {
                                z.AdditionalExcess = m_ExecutionPlan
                                    .QueryStoredProcedure<GetComparativeQuoteReportInsurerCoverAdditionalExcessQuery, ComparativeQuoteReportProductAdditionalExcess, ComparativeQuoteReportInsurerCoverAdditionalExcessStoredProcedure, ComparativeQuoteReportProductAdditionalExcessDto>(new ComparativeQuoteReportInsurerCoverAdditionalExcessStoredProcedure(criteria))
                                    .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportProductAdditionalExcess>, IList<ComparativeQuoteReportProductAdditionalExcessDto>>)
                                    .Execute().Response.Where(c => c.Category == z.Category).ToList();

                                z.ShouldApplySelectedExcess = z.AdditionalExcess.Any(f => f.ShouldApplySelectedExcess) ? true : false;
                                z.ShouldDisplayExcessValues = z.AdditionalExcess.Any(f => f.ShouldDisplayExcessValues) ? true : false;
                            });
                        });
                    });
                }
                    

                cover.Assets = m_ExecutionPlan
                    .QueryStoredProcedure<GetComparativeQuoteReportInsurerCoverAssetQuery, ComparativeQuoteReportInsurerCoverAsset, ComparativeQuoteReportInsurerCoverAssetStoredProcedure, ComparativeQuoteReportInsurerCoverAssetDto>(new ComparativeQuoteReportInsurerCoverAssetStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportInsurerCoverAsset>, IList<ComparativeQuoteReportInsurerCoverAssetDto>>)
                    .Execute().Response.ToList();

                cover.All = m_ExecutionPlan
                    .QueryStoredProcedure<GetComparativeQuoteReportInsurerCoverBenefitQuery, ComparativeQuoteReportInsurerCoverBenefit, ComparativeQuoteReportInsurerCoverBenefitStoredProcedure, ComparativeQuoteReportInsurerCoverBenefitDto>(new ComparativeQuoteReportInsurerCoverBenefitStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<ComparativeQuoteReportInsurerCoverBenefit>, IList<ComparativeQuoteReportInsurerCoverBenefitDto>>)
                    .Execute().Response.ToList();


                
                if (cover.All.Any())
                {
                    var index = 1;
                    var indexArray = new List<int>();
                    var processed = new ComparativeQuoteReportCoverDto();
                    var unique = new List<List<ComparativeQuoteReportInsurerCoverBenefitDto>>();
                    var totals = new List<List<ComparativeQuoteReportInsurerCoverBenefitDto>>();

                    var calculate = true;

                    cover.All.Each(y =>
                    {
                        if (y.Description == string.Empty)
                        {
                            indexArray.Add(index);
                        }
                        index++;
                    });

                    indexArray.Each(y =>
                    {
                        unique.Add(cover.All.Skip(y).TakeWhile(a => a.Description != string.Empty).ToList());
                    });

                    GetCoverValues(unique, cover.AssetList);

                    unique.Each(y =>
                    {
                        if (!processed.Header.Any())
                        {
                            processed.Header.Add(y.FirstOrDefault());

                            processed.Header = processed.Header.Select(z =>
                            {
                                string description = z.Description.ReplaceCharacters().TrimEnd(',', ' ', '\n');

                                z.Description = "<ul><li>" + description + "</li>";

                                if (z.FirstSelectedInsurerBenefitValue == string.Empty)
                                {
                                    z.FirstSelectedInsurerBenefitValue = "blank";
                                }

                                if (z.SecondSelectedInsurerBenefitValue == string.Empty)
                                {
                                    z.SecondSelectedInsurerBenefitValue = "blank";
                                }

                                if (z.ThirdSelectedInsurerBenefitValue == string.Empty)
                                {
                                    z.ThirdSelectedInsurerBenefitValue = "blank";
                                }
                                    


                                return z;
                            }).ToList();
                        }
                        else
                        {
                            processed.Header = processed.Header.Select(z =>
                            {
                                z.Description += "<li>" + y.FirstOrDefault().Description.ReplaceCharacters().TrimEnd(',', ' ', '\n') + "</li>";
                                return z;
                            }).ToList();
                        }

                        totals.Add(y.Where(a => a.Description == "Premium" || a.Description == "Basic Excess" || a.Description == "SASRIA" || a.Description == "Total Payment").ToList());

                        if (!calculate)
                        {
                            return;
                        }
                            
                        calculate = false;

                        processed.CommonBenefits = y.Skip(5).Take(y.Count - 5).Where(a => a.FirstSelectedInsurerBenefitValue == a.SecondSelectedInsurerBenefitValue && a.FirstSelectedInsurerBenefitValue == a.ThirdSelectedInsurerBenefitValue).ToList();
                        processed.DifferingBenefits = y.Skip(5).Take(y.Count - 5).Where(a => a.FirstSelectedInsurerBenefitValue != a.SecondSelectedInsurerBenefitValue || a.FirstSelectedInsurerBenefitValue != a.ThirdSelectedInsurerBenefitValue).ToList();

                        processed.CommonBenefits = processed.CommonBenefits.Select(a =>
                        {
                            a.SecondSelectedInsurerBenefitValue = (a.SecondSelectedInsurerBenefitValue == "0.00") ? "" : a.SecondSelectedInsurerBenefitValue;
                            a.ThirdSelectedInsurerBenefitValue = (a.ThirdSelectedInsurerBenefitValue == "0.00") ? "" : a.ThirdSelectedInsurerBenefitValue;

                            return a;
                        }).ToList();

                        processed.DifferingBenefits = processed.DifferingBenefits.Select(a =>
                        {
                            a.SecondSelectedInsurerBenefitValue = (a.SecondSelectedInsurerBenefitValue == "0.00") ? "" : a.SecondSelectedInsurerBenefitValue;
                            a.ThirdSelectedInsurerBenefitValue = (a.ThirdSelectedInsurerBenefitValue == "0.00") ? "" : a.ThirdSelectedInsurerBenefitValue;

                            return a;
                        }).ToList();
                    });

                    processed.Header = processed.Header.Select(z =>
                    {
                        z.Description += "</ul>";
                        return z;
                    }).ToList();

                    decimal totalPremium_FirstSelectedInsurerBenefitValue = 0;
                    decimal totalBasicExcess_FirstSelectedInsurerBenefitValue = 0;
                    decimal totalSASRIA_FirstSelectedInsurerBenefitValue = 0;
                    decimal totalPayment_FirstSelectedInsurerBenefitValue = 0;

                    decimal totalPremium_SecondSelectedInsurerBenefitValue = 0;
                    decimal totalBasicExcess_SecondSelectedInsurerBenefitValue = 0;
                    decimal totalSASRIA_SecondSelectedInsurerBenefitValue = 0;
                    decimal totalPayment_SecondSelectedInsurerBenefitValue = 0;

                    decimal totalPremium_ThirdSelectedInsurerBenefitValue = 0;
                    decimal totalBasicExcess_ThirdSelectedInsurerBenefitValue = 0;
                    decimal totalSASRIA_ThirdSelectedInsurerBenefitValue = 0;
                    decimal totalPayment_ThirdSelectedInsurerBenefitValue = 0;

                    string firstBasicExcessDescription = "";
                    string secondBasicExcessDescription = "";
                    string thirdBasicExcessDescription = "";

                    totals.Each(y =>
                    {
                        y.Where(a => a.Description == "Premium").Each(z =>
                        {
                            totalPremium_FirstSelectedInsurerBenefitValue += GetDecimalValue(z.FirstSelectedInsurerBenefitValue);
                            totalPremium_SecondSelectedInsurerBenefitValue += GetDecimalValue(z.SecondSelectedInsurerBenefitValue);
                            totalPremium_ThirdSelectedInsurerBenefitValue += GetDecimalValue(z.ThirdSelectedInsurerBenefitValue);
                        });

                        y.Where(a => a.Description == "Basic Excess").Each(z =>
                        {
                            firstBasicExcessDescription = GetTextDescription(z.FirstSelectedInsurerBenefitValue);
                            secondBasicExcessDescription = GetTextDescription(z.SecondSelectedInsurerBenefitValue);
                            thirdBasicExcessDescription = GetTextDescription(z.ThirdSelectedInsurerBenefitValue);

                            totalBasicExcess_FirstSelectedInsurerBenefitValue += GetDecimalValue(z.FirstSelectedInsurerBenefitValue);
                            totalBasicExcess_SecondSelectedInsurerBenefitValue += GetDecimalValue(z.SecondSelectedInsurerBenefitValue);
                            totalBasicExcess_ThirdSelectedInsurerBenefitValue += GetDecimalValue(z.ThirdSelectedInsurerBenefitValue);
                        });

                        y.Where(a => a.Description == "SASRIA").Each(z =>
                        {
                            totalSASRIA_FirstSelectedInsurerBenefitValue += GetDecimalValue(z.FirstSelectedInsurerBenefitValue);
                            totalSASRIA_SecondSelectedInsurerBenefitValue += GetDecimalValue(z.SecondSelectedInsurerBenefitValue);
                            totalSASRIA_ThirdSelectedInsurerBenefitValue += GetDecimalValue(z.ThirdSelectedInsurerBenefitValue);
                        });

                        y.Where(a => a.Description == "Total Payment").Each(z =>
                        {
                            totalPayment_FirstSelectedInsurerBenefitValue += GetDecimalValue(z.FirstSelectedInsurerBenefitValue);
                            totalPayment_SecondSelectedInsurerBenefitValue += GetDecimalValue(z.SecondSelectedInsurerBenefitValue);
                            totalPayment_ThirdSelectedInsurerBenefitValue += GetDecimalValue(z.ThirdSelectedInsurerBenefitValue);
                        });
                    });

                    processed.Totals.Add(new ComparativeQuoteReportInsurerCoverBenefitDto
                    {
                        Description = "Premium",
                        FirstSelectedInsurerBenefitValue = totalPremium_FirstSelectedInsurerBenefitValue.ToString(),
                        SecondSelectedInsurerBenefitValue = totalPremium_SecondSelectedInsurerBenefitValue.ToString(),
                        ThirdSelectedInsurerBenefitValue = totalPremium_ThirdSelectedInsurerBenefitValue.ToString(),
                    });

                    processed.Totals.Add(new ComparativeQuoteReportInsurerCoverBenefitDto
                    {
                        Description = "Basic Excess",
                        FirstSelectedInsurerBenefitValue = string.IsNullOrEmpty(firstBasicExcessDescription) ? totalBasicExcess_FirstSelectedInsurerBenefitValue.ToString() : firstBasicExcessDescription,
                        SecondSelectedInsurerBenefitValue = string.IsNullOrEmpty(secondBasicExcessDescription) ? totalBasicExcess_SecondSelectedInsurerBenefitValue.ToString() : secondBasicExcessDescription,
                        ThirdSelectedInsurerBenefitValue = string.IsNullOrEmpty(thirdBasicExcessDescription) ? totalBasicExcess_ThirdSelectedInsurerBenefitValue.ToString() : thirdBasicExcessDescription,
                    });

                    processed.Totals.Add(new ComparativeQuoteReportInsurerCoverBenefitDto
                    {
                        Description = "SASRIA",
                        FirstSelectedInsurerBenefitValue = totalSASRIA_FirstSelectedInsurerBenefitValue.ToString(),
                        SecondSelectedInsurerBenefitValue = totalSASRIA_SecondSelectedInsurerBenefitValue.ToString(),
                        ThirdSelectedInsurerBenefitValue = totalSASRIA_ThirdSelectedInsurerBenefitValue.ToString(),
                    });

                    processed.Totals.Add(new ComparativeQuoteReportInsurerCoverBenefitDto
                    {
                        Description = "Total Payment",
                        FirstSelectedInsurerBenefitValue = totalPayment_FirstSelectedInsurerBenefitValue.ToString(),
                        SecondSelectedInsurerBenefitValue = totalPayment_SecondSelectedInsurerBenefitValue.ToString(),
                        ThirdSelectedInsurerBenefitValue = totalPayment_ThirdSelectedInsurerBenefitValue.ToString(),
                    });

                    cover.Header = processed.Header;
                    cover.Totals = FormatTotals(processed.Totals);
                    cover.CommonBenefits = processed.CommonBenefits;
                    cover.DifferingBenefits = processed.DifferingBenefits;
                }

                covers.Add(cover);
            }

            return covers.Any() ? covers : default(List<ComparativeQuoteReportCoverDto>);
        }


        private void GetCoverValues(List<List<ComparativeQuoteReportInsurerCoverBenefitDto>> unique, List<ComparativeQuoteReportInsurerAssetDto> assetList)
        {
            unique.Each(y =>
            {
                var totals = new List<List<ComparativeQuoteReportInsurerCoverBenefitDto>>();
                var header = y.FirstOrDefault();
                ComparativeQuoteReportInsurerAssetDto asset = new ComparativeQuoteReportInsurerAssetDto();

                asset.Description = header.Description;
                m_Log.DebugFormat("-------------------------------{0}-------------------------------", asset.Description);
                totals.Add(y.Where(a => a.Description == "Premium" || a.Description == "Basic Excess" || a.Description == "SASRIA" || a.Description == "Total Payment").ToList());

                totals.Each(t =>
                {
                    m_Log.DebugFormat("PREMIUM", asset.Description);
                    ComparativeQuoteReportInsurerCoverBenefitDto premium = t.FirstOrDefault(a => a.Description == "Premium");
                    premium = FormatCurrencyValues(premium, "Premium");
                    asset.Details.Add(premium);

                    m_Log.DebugFormat("Basic Excess", asset.Description);
                    ComparativeQuoteReportInsurerCoverBenefitDto basicExcess = t.FirstOrDefault(a => a.Description == "Basic Excess");
                    basicExcess = FormatCurrencyValues(basicExcess, "Basic Excess");
                    asset.Details.Add(basicExcess);

                    m_Log.DebugFormat("SASRIA", asset.Description);
                    ComparativeQuoteReportInsurerCoverBenefitDto sasria = t.FirstOrDefault(a => a.Description == "SASRIA");
                    sasria = FormatCurrencyValues(sasria, "SASRIA");
                    asset.Details.Add(sasria);

                    m_Log.DebugFormat("Total Payment", asset.Description);
                    ComparativeQuoteReportInsurerCoverBenefitDto totalPayment = t.FirstOrDefault(a => a.Description == "Total Payment");
                    totalPayment = FormatCurrencyValues(totalPayment, "Total Payment");
                    asset.Details.Add(totalPayment);
                });

                m_Log.DebugFormat("-------------------------------{0}-------------------------------", asset.Description);
                assetList.Add(asset);
           });
        }

        private ComparativeQuoteReportInsurerCoverBenefitDto FormatCurrencyValues(ComparativeQuoteReportInsurerCoverBenefitDto value, string parameter)
        {
            
            if (value == null)
            {
                return new ComparativeQuoteReportInsurerCoverBenefitDto
                {
                    Description = parameter,
                    Id = 0,
                    FirstSelectedInsurerBenefitValue = string.Empty,
                    SecondSelectedInsurerBenefitValue = string.Empty,
                    ThirdSelectedInsurerBenefitValue = string.Empty
                };
            }

            m_Log.DebugFormat("QUERY {0} --- {1} --- {2}", value.FirstSelectedInsurerBenefitValue, value.SecondSelectedInsurerBenefitValue, value.ThirdSelectedInsurerBenefitValue);


            //Retrieve currency
            CultureVisitor cultureVisitor = new CultureVisitor(m_Context.Get().CultureInfo);
            decimal decimalValue = 0;

            decimal firstValue = decimal.TryParse(value.FirstSelectedInsurerBenefitValue, NumberStyles.Any, CultureInfo.InvariantCulture, out decimalValue) ? decimalValue : 0;
            decimal secondValue = decimal.TryParse(value.SecondSelectedInsurerBenefitValue, NumberStyles.Any, CultureInfo.InvariantCulture, out decimalValue) ? decimalValue : 0;
            decimal thirdValue = decimal.TryParse(value.ThirdSelectedInsurerBenefitValue, NumberStyles.Any, CultureInfo.InvariantCulture, out decimalValue) ? decimalValue : 0;

            m_Log.DebugFormat("NOT FORMATTED {0} --- {1} --- {2}", firstValue, secondValue, thirdValue);
            m_Log.DebugFormat("FORMATTED {0} --- {1} --- {2}", new MoneyDto(firstValue, cultureVisitor).ToString(), new MoneyDto(secondValue, cultureVisitor).ToString(), new MoneyDto(thirdValue, cultureVisitor).ToString());

            return new ComparativeQuoteReportInsurerCoverBenefitDto
            {
                Description = value.Description,
                Id = 1,
                FirstSelectedInsurerBenefitValue = new MoneyDto(firstValue, cultureVisitor).ToString(),
                SecondSelectedInsurerBenefitValue = new MoneyDto(secondValue, cultureVisitor).ToString(),
                ThirdSelectedInsurerBenefitValue = new MoneyDto(thirdValue, cultureVisitor).ToString()
            };

        }
    }
}