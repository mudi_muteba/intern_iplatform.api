﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.Base;
using Common.Logging;
using Newtonsoft.Json;

namespace Api.Modules.Reports.Base
{
    public class ReportGeneratorModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IRepository m_Repository;
        private readonly IRootPathProvider m_Root;

        public ReportGeneratorModule(IExecutionPlan executionPlan, IRepository repository, IRootPathProvider root)
        {
            this.m_ExecutionPlan = executionPlan;
            this.m_Repository = repository;
            this.m_Root = root;

            Post[SystemRoutes.Reports.GenerateReports.Route] = x => GenerateReports();
        }

        private Negotiator GenerateReports()
        {
            var criteria = this.Bind<ReportGeneratorDto>();
                criteria.PathMap = m_Root.GetRootPath();


            var result = m_ExecutionPlan.Execute<ReportGeneratorDto, ReportGeneratorResponseDto>(criteria);
            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}