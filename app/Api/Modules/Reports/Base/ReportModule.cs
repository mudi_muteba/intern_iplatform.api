﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Reports.Base;
using Domain.Reports.Base.Queries;
using Domain.Reports.Base.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.Base.Criteria;
using iPlatform.Api.DTOs.Reports;
using iPlatform.Api.DTOs.Users;

namespace Api.Modules.Reports.Base
{
    public class ReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public ReportModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.Reports.GetReports.Route] = x => GetReports();
            Post[SystemRoutes.Reports.GetReportsByCriteria.Route] = x => GetReportsByCriteria();
            Post[SystemRoutes.Reports.Post.Route] = x => CreateMultipleReports();
            Post[SystemRoutes.Reports.ValidateReportCampaigns.Route] = x => ValidateReportCampaigns();
        }

        private Negotiator GetReports()
        {
            var result = executionPlan
                .QueryStoredProcedure<GetReportListQuery, Report, ReportListStoredProcedure, ReportDto>(new ReportListStoredProcedure())
                .OnSuccess(Mapper.Map<IList<Report>, IList<ReportDto>>)
                .Execute();

            var reports = result.Response.ToList().Select(x =>
            {
                var criteria = new ReportParamsCriteriaDto { ReportId = x.Id };

                var criteriaReportDocumentDefinition = new ReportDocumentDefinitionCriteriaDto { ReportId = x.Id };

                var reportParameters = executionPlan
                    .QueryStoredProcedure<GetReportParamsQuery, ReportParam, ReportParamsStoredProcedure, ReportParamDto>(new ReportParamsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<ReportParam>, IList<ReportParamDto>>)
                    .Execute();

                var reportDocumentDefinitions = executionPlan
                    .QueryStoredProcedure<GetReportDocumentDefinitionsQuery, ReportDocumentDefinition, ReportDocumentDefinitionStoredProcedure, ReportDocumentDefinitionDto>(new ReportDocumentDefinitionStoredProcedure(criteriaReportDocumentDefinition))
                    .OnSuccess(Mapper.Map<IList<ReportDocumentDefinition>, IList<ReportDocumentDefinitionDto>>)
                    .Execute();

                x.Parameters = reportParameters.Response.ToList() ?? default(IList<ReportParamDto>);
                x.DocumentDefinitions = reportDocumentDefinitions.Response.ToList() ?? default(IList<ReportDocumentDefinitionDto>);

                return x;
            }).ToList();

            var response = new QueryResult<IList<ReportDto>>(reports).CreateGETResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetReportsByCriteria()
        {
            var criteria = this.Bind<ReportCriteriaDto>();

            var result = executionPlan
                .Query<GeReportsByChannelId, Report, List<ReportDto>>()
                .Configure(x => x.WithCriteria(criteria))
                .OnSuccess(x => Mapper.Map<List<ReportDto>>(x.ToList()))
                .Execute();

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateMultipleReports()
        {
            var createMultipleReportsDto = this.Bind<CreateMultipleReportsDto>();

            var result = executionPlan.Execute<CreateMultipleReportsDto, int>(createMultipleReportsDto);

            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator ValidateReportCampaigns()
        {
            var dto = this.Bind<ValidateReportCampaignsDto>();

            var result = executionPlan.Execute<ValidateReportCampaignsDto, ValidatedReportCampaignsResponseDto>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        
    }
}