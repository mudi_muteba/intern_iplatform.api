﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.ReportSchedules;
using Domain.ReportSchedules.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.ReportSchedules;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Reports.Base
{
    public class ReportScheduleModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IPaginateResults m_Paginator;
        private readonly IRepository m_Repository;
        private readonly IRootPathProvider m_RootPathProvider;

        public ReportScheduleModule(IExecutionPlan executionPlan, IPaginateResults paginator, IRepository repository, IRootPathProvider rootPathProvider)
        {
            m_ExecutionPlan = executionPlan;
            m_Paginator = paginator;
            m_Repository = repository;
            m_RootPathProvider = rootPathProvider;

            Get[SystemRoutes.ReportSchedules.GetById.Route] = x => GetById(x.id);
            Post[SystemRoutes.ReportSchedules.Save.Route] = x => Save();
            Post[SystemRoutes.ReportSchedules.Search.Route] = x => Search();
            Post[SystemRoutes.ReportSchedules.Invoke.Route] = x => Invoke();
            Delete[SystemRoutes.ReportSchedules.Delete.Route] = x => DeleteSchedule();
        }

        private Negotiator GetById(int id)
        {
            var result = m_ExecutionPlan
                .GetById<ReportSchedule, ReportScheduleDto>(() => m_Repository.GetById<ReportSchedule>(id))
                .OnSuccess(Mapper.Map<ReportSchedule, ReportScheduleDto>)
                .Execute();

            var response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.ReportSchedules.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var dto = this.Bind<SearchReportScheduleDto>();

            var queryResult = m_ExecutionPlan
                .Search<SearchReportScheduleQuery, ReportSchedule, SearchReportScheduleDto, PagedResultDto<ReportScheduleDto>, ReportScheduleDto>(dto)
                .Configure(query => query.WithCriteria(dto))
                .OnSuccess(result => Mapper.Map<PagedResultDto<ReportScheduleDto>>(result.Paginate(m_Paginator, dto.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response).CreateLinks(c => SystemRoutes.ReportSchedules.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Save()
        {
            var result = m_ExecutionPlan.Execute<SaveReportScheduleDto, int>(this.Bind<SaveReportScheduleDto>());
            var response = result.CreatePOSTResponse();

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Invoke()
        {
            var dto = this.Bind<InvokeReportScheduleDto>();
            dto.RootPath = m_RootPathProvider.GetRootPath();

            var result = m_ExecutionPlan.Execute<InvokeReportScheduleDto, int>(dto);
            var response = result.CreatePOSTResponse();

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteSchedule()
        {
            var result = m_ExecutionPlan.Execute<DeleteReportScheduleDto, int>(this.Bind<DeleteReportScheduleDto>());
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.ReportSchedules.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
    }
}