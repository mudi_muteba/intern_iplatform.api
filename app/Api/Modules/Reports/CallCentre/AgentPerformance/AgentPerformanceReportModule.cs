﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.AgentPerformance.Queries;
using Domain.Reports.CallCentre.AgentPerformance;
using Domain.Reports.CallCentre.AgentPerformance.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformance;

namespace Api.Modules.Reports.CallCentre
{
    public class AgentPerformanceReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;
        private readonly IProvideContext context;

        public AgentPerformanceReportModule(IExecutionPlan executionPlan, IRepository repository, IProvideContext context)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.context = context;

            Post[SystemRoutes.CallCentreReports.GetCallCentreAgentPerformanceReport.Route] = x => GetAgentPerformanceReport();
        }

        private Negotiator GetAgentPerformanceReport()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            if (criteria.CampaignIds.Length > 0)
            {
                var campaignIds = criteria.CampaignIds.Split(',');
                criteria.CampaignId = Convert.ToInt32(campaignIds[0]);
            }

            var headerQueryResult = executionPlan
                .QueryStoredProcedure<GetAgentPerformanceReportHeaderQuery, AgentPerformanceReportHeader, AgentPerformanceReportHeaderStoredProcedure, AgentPerformanceReportHeaderDto>(new AgentPerformanceReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<AgentPerformanceReportHeader>, IList<AgentPerformanceReportHeaderDto>>)
                .Execute();

            var staticsQueryResult = executionPlan
                .QueryStoredProcedure<GetAgentPerformanceReportAgentQuoteStatisticsQuery, AgentPerformanceReportAgentQuoteStatistics, AgentPerformanceReportAgentQuoteStatisticsStoredProcedure, AgentPerformanceReportAgentQuoteStatisticsDto>(new AgentPerformanceReportAgentQuoteStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<AgentPerformanceReportAgentQuoteStatistics>, IList<AgentPerformanceReportAgentQuoteStatisticsDto>>)
                .Execute();

            var result = new QueryResult<AgentPerformanceReportContainerDto>(new AgentPerformanceReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault() ?? default(AgentPerformanceReportHeaderDto),
                Statistics = staticsQueryResult.Response.ToList() ?? default(List<AgentPerformanceReportAgentQuoteStatisticsDto>)
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}