﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.SalesManagement.Queries;
using Domain.Reports.CallCentre.SalesManagement;
using Domain.Reports.CallCentre.SalesManagement.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.SalesManagement;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Api.Modules.Reports.CallCentre
{
    public class CallCentreSalesManagementReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public CallCentreSalesManagementReportModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Post[SystemRoutes.CallCentreReports.GetCallCentreSalesManagementReport.Route] = x => GetCallCentreSalesManagementReportModule();
        }

        private Negotiator GetCallCentreSalesManagementReportModule()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            if (criteria.CampaignIds.Length > 0)
            {
                var campaignIds = criteria.CampaignIds.Split(',');
                criteria.CampaignId = Convert.ToInt32(campaignIds[0]);
            }

            var headerQueryResult = executionPlan
                .QueryStoredProcedure<GetCallCentreSalesManagementReportHeaderQuery, CallCentreSalesManagementReportHeader, CallCentreSalesManagementReportHeaderStoredProcedure, CallCentreSalesManagementReportHeaderDto>(new CallCentreSalesManagementReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<CallCentreSalesManagementReportHeader>, IList<CallCentreSalesManagementReportHeaderDto>>)
                .Execute();

            var staticsQueryResult = executionPlan
                .QueryStoredProcedure<GetCallCentreSalesManagementReportAgentLeadStatisticsQuery, CallCentreSalesManagementReportAgentLeadStatistics, CallCentreSalesManagementReportAgentLeadStatisticsStoredProcedure, CallCentreSalesManagementReportAgentLeadStatisticsDto>(new CallCentreSalesManagementReportAgentLeadStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<CallCentreSalesManagementReportAgentLeadStatistics>, IList<CallCentreSalesManagementReportAgentLeadStatisticsDto>>)
                .Execute();

            var result = new QueryResult<CallCentreSalesManagementReportContainerDto>(new CallCentreSalesManagementReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault() ?? default(CallCentreSalesManagementReportHeaderDto),
                Statistics = staticsQueryResult.Response.ToList() ?? default(List<CallCentreSalesManagementReportAgentLeadStatisticsDto>)
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}