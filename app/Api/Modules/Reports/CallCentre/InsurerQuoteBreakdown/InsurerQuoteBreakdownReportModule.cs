﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.InsurerQuoteBreakdown.Queries;
using Domain.Reports.CallCentre.InsurerQuoteBreakdown;
using Domain.Reports.CallCentre.InsurerQuoteBreakdown.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerQuoteBreakdown;

namespace Api.Modules.Reports.CallCentre
{
    public class InsurerQuoteBreakdownReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public InsurerQuoteBreakdownReportModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Post[SystemRoutes.CallCentreReports.GetCallCentreInsurerQuoteBreakdownReport.Route] = x => GetInsurerQuoteBreakdownReport();
        }

        private Negotiator GetInsurerQuoteBreakdownReport()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            if (criteria.CampaignIds.Length > 0)
            {
                var campaignIds = criteria.CampaignIds.Split(',');
                criteria.CampaignId = Convert.ToInt32(campaignIds[0]);
            }

            var headerQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerQuoteBreakdownReportHeaderQuery, InsurerQuoteBreakdownReportHeader, InsurerQuoteBreakdownReportHeaderStoredProcedure, InsurerQuoteBreakdownReportHeaderDto>(new InsurerQuoteBreakdownReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerQuoteBreakdownReportHeader>, IList<InsurerQuoteBreakdownReportHeaderDto>>)
                .Execute();

            var staticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerQuoteBreakdownReportInsurerQuoteStatisticsQuery, InsurerQuoteBreakdownReportInsurerQuoteStatistics, InsurerQuoteBreakdownReportInsurerQuoteStatisticsStoredProcedure, InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto>(new InsurerQuoteBreakdownReportInsurerQuoteStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerQuoteBreakdownReportInsurerQuoteStatistics>, IList<InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto>>)
                .Execute();

            var result = new QueryResult<InsurerQuoteBreakdownReportContainerDto>(new InsurerQuoteBreakdownReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault() ?? default(InsurerQuoteBreakdownReportHeaderDto),
                Statistics = staticsQueryResult.Response.ToList() ?? default(List<InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto>)
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}