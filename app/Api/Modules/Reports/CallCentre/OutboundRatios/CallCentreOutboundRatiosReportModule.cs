﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Reports.CallCentre.OutboundRatios.Queries;
using Domain.Reports.CallCentre.OutboundRatios;
using Domain.Reports.CallCentre.OutboundRatios.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.OutboundRatios;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace Api.Modules.Reports.CallCentre
{
    public class CallCentreOutboundRatiosReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public CallCentreOutboundRatiosReportModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Post[SystemRoutes.CallCentreReports.GetCallCentreOutboundRatiosReport.Route] = x => GetCallCentreOutboundRatiosReportModule();
        }

        private Negotiator GetCallCentreOutboundRatiosReportModule()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            if (criteria.CampaignIds.Length > 0)
            {
                var campaignIds = criteria.CampaignIds.Split(',');
                criteria.CampaignId = Convert.ToInt32(campaignIds[0]);
            }

            var headerQueryResult = executionPlan
                .QueryStoredProcedure<GetCallCentreOutboundRatiosReportHeaderQuery, CallCentreOutboundRatiosReportHeader, CallCentreOutboundRatiosReportHeaderStoredProcedure, CallCentreOutboundRatiosReportHeaderDto>(new CallCentreOutboundRatiosReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<CallCentreOutboundRatiosReportHeader>, IList<CallCentreOutboundRatiosReportHeaderDto>>)
                .Execute();

            var staticsQueryResult = executionPlan
                .QueryStoredProcedure<GetCallCentreOutboundRatiosReportAgentLeadStatisticsQuery, CallCentreOutboundRatiosReportAgentLeadStatistics, CallCentreOutboundRatiosReportAgentLeadStatisticsStoredProcedure, CallCentreOutboundRatiosReportAgentLeadStatisticsDto>(new CallCentreOutboundRatiosReportAgentLeadStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<CallCentreOutboundRatiosReportAgentLeadStatistics>, IList<CallCentreOutboundRatiosReportAgentLeadStatisticsDto>>)
                .Execute();

            var result = new QueryResult<CallCentreOutboundRatiosReportContainerDto>(new CallCentreOutboundRatiosReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault() ?? default(CallCentreOutboundRatiosReportHeaderDto),
                Statistics = staticsQueryResult.Response.ToList() ?? default(List<CallCentreOutboundRatiosReportAgentLeadStatisticsDto>)
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}