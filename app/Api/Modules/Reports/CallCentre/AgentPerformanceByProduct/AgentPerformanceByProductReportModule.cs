﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.AgentPerformanceByProduct.Queries;
using Domain.Reports.CallCentre.AgentPerformanceByProduct;
using Domain.Reports.CallCentre.AgentPerformanceByProduct.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformanceByProduct;

namespace Api.Modules.Reports.CallCentre
{
    public class AgentPerformanceByProductReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public AgentPerformanceByProductReportModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Post[SystemRoutes.CallCentreReports.GetCallCentreAgentPerformanceByProductReport.Route] = x => GetAgentPerformanceByProductReport();
        }

        private Negotiator GetAgentPerformanceByProductReport()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            if (criteria.CampaignIds.Length > 0)
            {
                var campaignIds = criteria.CampaignIds.Split(',');
                criteria.CampaignId = Convert.ToInt32(campaignIds[0]);
            }

            var headerQueryResult = executionPlan
                .QueryStoredProcedure<GetAgentPerformanceByProductReportHeaderQuery, AgentPerformanceByProductReportHeader, AgentPerformanceByProductReportHeaderStoredProcedure, AgentPerformanceByProductReportHeaderDto>(new AgentPerformanceByProductReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<AgentPerformanceByProductReportHeader>, IList<AgentPerformanceByProductReportHeaderDto>>)
                .Execute();

            var staticsQueryResult = executionPlan
                .QueryStoredProcedure<GetAgentPerformanceByProductReportAgentQuoteStatisticsQuery, AgentPerformanceByProductReportAgentQuoteStatistics, AgentPerformanceByProductReportAgentQuoteStatisticsStoredProcedure, AgentPerformanceByProductReportAgentQuoteStatisticsDto>(new AgentPerformanceByProductReportAgentQuoteStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<AgentPerformanceByProductReportAgentQuoteStatistics>, IList<AgentPerformanceByProductReportAgentQuoteStatisticsDto>>)
                .Execute();

            var result = new QueryResult<AgentPerformanceByProductReportContainerDto>(new AgentPerformanceByProductReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault() ?? default(AgentPerformanceByProductReportHeaderDto),
                Statistics = staticsQueryResult.Response.ToList() ?? default(List<AgentPerformanceByProductReportAgentQuoteStatisticsDto>)
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}