﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Reports.CallCentre.BrokerQuoteUpload;
using Domain.Reports.CallCentre.BrokerQuoteUpload.Queries;
using Domain.Reports.CallCentre.BrokerQuoteUpload.StoredProcedures;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Reports.CallCentre
{
    public class BrokerQuoteUploadReportModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;

        public BrokerQuoteUploadReportModule(IExecutionPlan executionPlan)
        {
            _executionPlan = executionPlan;

            Post[SystemRoutes.CallCentreReports.GetCallCentreBrokerQuoteUploadsReport.Route] = x => GetBrokerQuoteUploadsReport();
        }

        private Negotiator GetBrokerQuoteUploadsReport()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            var headerQueryResult = _executionPlan
                .QueryStoredProcedure<GetBrokerQuoteUploadReportHeaderQuery, BrokerQuoteUploadReportHeader, BrokerQuoteUploadReportHeaderStoredProcedure, BrokerQuoteUploadReportHeaderDto>(new BrokerQuoteUploadReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<BrokerQuoteUploadReportHeader>, IList<BrokerQuoteUploadReportHeaderDto>>)
                .Execute();

            var summaryQueryResult = _executionPlan
                .QueryStoredProcedure<GetBrokerQuoteUploadReportSummaryQuery, BrokerQuoteUploadReportSummary, BrokerQuoteUploadReportSummaryStoredProcedure, BrokerQuoteUploadReportSummaryDto>(new BrokerQuoteUploadReportSummaryStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<BrokerQuoteUploadReportSummary>, IList<BrokerQuoteUploadReportSummaryDto>>)
                .Execute();

            var quotesQueryResult = _executionPlan
                .QueryStoredProcedure<GetBrokerQuoteUploadReportQuotesQuery, BrokerQuoteUploadReportQuotes, BrokerQuoteUploadReportQuotesStoredProcedure, BrokerQuoteUploadReportQuotesDto>(new BrokerQuoteUploadReportQuotesStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<BrokerQuoteUploadReportQuotes>, IList<BrokerQuoteUploadReportQuotesDto>>)
                .Execute();

            var uploadsQueryResult = _executionPlan
                .QueryStoredProcedure<GetBrokerQuoteUploadReportUploadsQuery, BrokerQuoteUploadReportUploads, BrokerQuoteUploadReportUploadsStoredProcedure, BrokerQuoteUploadReportUploadsDto>(new BrokerQuoteUploadReportUploadsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<BrokerQuoteUploadReportUploads>, IList<BrokerQuoteUploadReportUploadsDto>>)
                .Execute();

            var result = new QueryResult<BrokerQuoteUploadReportContainerDto>(new BrokerQuoteUploadReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault(),
                Summary = summaryQueryResult.Response.ToList(),
                Quotes = quotesQueryResult.Response.ToList(),
                Uploads = uploadsQueryResult.Response.ToList()
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}