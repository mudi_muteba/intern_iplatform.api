﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.ManagerLevelQuoteUpload.Queries;
using Domain.Reports.CallCentre.ManagerLevelQuoteUpload;
using Domain.Reports.CallCentre.ManagerLevelQuoteUpload.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.ManagerLevelQuoteUpload;

namespace Api.Modules.Reports.CallCentre
{
    public class ManagerLevelQuoteUploadReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;
        private readonly IProvideContext context;

        public ManagerLevelQuoteUploadReportModule(IExecutionPlan executionPlan, IRepository repository, IProvideContext context)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;
            this.context = context;

            Post[SystemRoutes.CallCentreReports.GetCallCentreManagerLevelQuoteUploadsReport.Route] = x => GetManagerLevelQuoteUploadsReport();
        }

        private Negotiator GetManagerLevelQuoteUploadsReport()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            if (criteria.CampaignIds.Length > 0)
            {
                var campaignIds = criteria.CampaignIds.Split(',');
                criteria.CampaignId = Convert.ToInt32(campaignIds[0]);
            }

            var headerQueryResult = executionPlan
                .QueryStoredProcedure<GetManagerLevelQuoteUploadReportHeaderQuery, ManagerLevelQuoteUploadReportHeader, ManagerLevelQuoteUploadReportHeaderStoredProcedure, ManagerLevelQuoteUploadReportHeaderDto>(new ManagerLevelQuoteUploadReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<ManagerLevelQuoteUploadReportHeader>, IList<ManagerLevelQuoteUploadReportHeaderDto>>)
                .Execute();

            var staticsQueryResult = executionPlan
                .QueryStoredProcedure<GetManagerLevelQuoteUploadReportUploadStatisticsQuery, ManagerLevelQuoteUploadReportUploadStatistics, ManagerLevelQuoteUploadReportUploadStatisticsStoredProcedure, ManagerLevelQuoteUploadReportUploadStatisticsDto>(new ManagerLevelQuoteUploadReportUploadStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<ManagerLevelQuoteUploadReportUploadStatistics>, IList<ManagerLevelQuoteUploadReportUploadStatisticsDto>>)
                .Execute();

            var result = new QueryResult<ManagerLevelQuoteUploadReportContainerDto>(new ManagerLevelQuoteUploadReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault() ?? default(ManagerLevelQuoteUploadReportHeaderDto),
                Statistics = staticsQueryResult.Response.ToList() ?? default(List<ManagerLevelQuoteUploadReportUploadStatisticsDto>)
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}