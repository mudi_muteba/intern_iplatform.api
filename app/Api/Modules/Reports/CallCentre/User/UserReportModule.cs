﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Reports.CallCentre.User;
using Domain.Reports.CallCentre.User.Queries;
using Domain.Reports.CallCentre.User.StoredProcedures;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.User;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Reports.CallCentre
{
    public class UserReportModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;

        public UserReportModule(IExecutionPlan executionPlan)
        {
            _executionPlan = executionPlan;

            Post[SystemRoutes.CallCentreReports.GetCallCentreUserReport.Route] = x => GetUserReport();
        }

        private Negotiator GetUserReport()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            var headerQueryResult = _executionPlan
                .QueryStoredProcedure<GetUserReportHeaderQuery, UserReportHeader, UserReportHeaderStoredProcedure, UserReportHeaderDto>(new UserReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<UserReportHeader>, IList<UserReportHeaderDto>>)
                .Execute();

            var staticsQueryResult = _executionPlan
                .QueryStoredProcedure<GetUserReportUserStatisticsQuery, UserReportUserStatistics, UserReportUserStatisticsStoredProcedure, UserReportUserStatisticsDto>(new UserReportUserStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<UserReportUserStatistics>, IList<UserReportUserStatisticsDto>>)
                .Execute();

            var result = new QueryResult<UserReportContainerDto>(new UserReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault(),
                Statistics = staticsQueryResult.Response.ToList()
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}