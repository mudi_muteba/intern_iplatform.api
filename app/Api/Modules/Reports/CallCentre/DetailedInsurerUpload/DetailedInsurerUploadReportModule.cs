﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;

using Domain.Reports.CallCentre.DetailedInsurerUpload.Queries;
using Domain.Reports.CallCentre.DetailedInsurerUpload;
using Domain.Reports.CallCentre.DetailedInsurerUpload.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.DetailedInsurerUpload;

namespace Api.Modules.Reports.CallCentre
{
    public class DetailedInsurerUploadReportModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository repository;
        private readonly IProvideContext context;

        public DetailedInsurerUploadReportModule(IExecutionPlan executionPlan, IRepository repository, IProvideContext context)
        {
            _executionPlan = executionPlan;
            this.repository = repository;
            this.context = context;

            Post[SystemRoutes.CallCentreReports.GetCallCentreDetailedInsurerUploadReport.Route] = x => GetDetailedInsurerUploadReport();
        }

        private Negotiator GetDetailedInsurerUploadReport()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            if (criteria.CampaignIds.Length > 0)
            {
                var campaignIds = criteria.CampaignIds.Split(',');
                criteria.CampaignId = Convert.ToInt32(campaignIds[0]);
            }

            var headerQueryResult = _executionPlan
                .QueryStoredProcedure<GetDetailedInsurerUploadReportHeaderQuery, DetailedInsurerUploadReportHeader, DetailedInsurerUploadReportHeaderStoredProcedure, DetailedInsurerUploadReportHeaderDto>(new DetailedInsurerUploadReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<DetailedInsurerUploadReportHeader>, IList<DetailedInsurerUploadReportHeaderDto>>)
                .Execute();

            var staticsQueryResult = _executionPlan
                .QueryStoredProcedure<GetDetailedInsurerUploadReportUploadStatisticsQuery, DetailedInsurerUploadReportUploadStatistics, DetailedInsurerUploadReportUploadStatisticsStoredProcedure, DetailedInsurerUploadReportUploadStatisticsDto>(new DetailedInsurerUploadReportUploadStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<DetailedInsurerUploadReportUploadStatistics>, IList<DetailedInsurerUploadReportUploadStatisticsDto>>)
                .Execute();

            var totalsQueryResult = _executionPlan
                .QueryStoredProcedure<GetDetailedInsurerUploadReportUploadTotalsQuery, DetailedInsurerUploadReportUploadTotals, DetailedInsurerUploadReportUploadTotalsStoredProcedure, DetailedInsurerUploadReportUploadTotalsDto>(new DetailedInsurerUploadReportUploadTotalsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<DetailedInsurerUploadReportUploadTotals>, IList<DetailedInsurerUploadReportUploadTotalsDto>>)
                .Execute();

            var result = new QueryResult<DetailedInsurerUploadReportContainerDto>(new DetailedInsurerUploadReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault() ?? default(DetailedInsurerUploadReportHeaderDto),
                Statistics = staticsQueryResult.Response.ToList() ?? default(List<DetailedInsurerUploadReportUploadStatisticsDto>),
                Totals = totalsQueryResult.Response.ToList() ?? default(List<DetailedInsurerUploadReportUploadTotalsDto>)
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}