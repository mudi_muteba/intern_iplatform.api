﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;

using AutoMapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Reports.CallCentre.InsurerData.Queries;
using Domain.Reports.CallCentre.InsurerData;
using Domain.Reports.CallCentre.InsurerData.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerData;

namespace Api.Modules.Reports.CallCentre
{
    public class InsurerDataReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public InsurerDataReportModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Post[SystemRoutes.CallCentreReports.GetCallCentreInsurerDataReport.Route] = x => GetInsurerDataReport();
        }

        private Negotiator GetInsurerDataReport()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            if (criteria.CampaignIds.Length > 0)
            {
                var campaignIds = criteria.CampaignIds.Split(',');
                criteria.CampaignId = Convert.ToInt32(campaignIds[0]);
            }

            List<int> productIds = (criteria.ProductIds.Length > 0) ? criteria.ProductIds.Split(',').Select(x => Convert.ToInt32(x)).ToList() : new List<int>();

            criteria.ProductIds = string.Join(",", productIds);
            criteria.CoverId = 218;

            var headerQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportHeaderQuery, InsurerDataReportHeader, InsurerDataReportHeaderStoredProcedure, InsurerDataReportHeaderDto>(new InsurerDataReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportHeader>, IList<InsurerDataReportHeaderDto>>)
                .Execute();

            var productQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportCampaignProductsQuery, InsurerDataReportCampaignProducts, InsurerDataReportCampaignProductsByCoverId, InsurerDataReportCampaignProductsDto>(new InsurerDataReportCampaignProductsByCoverId(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportCampaignProducts>, IList<InsurerDataReportCampaignProductsDto>>)
                .Execute();

            var staticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportInsurerQuoteStatisticsQuery, InsurerDataReportInsurerQuoteStatistics, InsurerDataReportInsurerQuoteStatisticsStoredProcedure, InsurerDataReportInsurerQuoteStatisticsDto>(new InsurerDataReportInsurerQuoteStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportInsurerQuoteStatistics>, IList<InsurerDataReportInsurerQuoteStatisticsDto>>)
                .Execute();

            var ageStaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportAgeStatisticsQuery, InsurerDataReportAgeStatistics, InsurerDataReportAgeStatisticsStoredProcedure, InsurerDataReportAgeStatisticsDto>(new InsurerDataReportAgeStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportAgeStatistics>, IList<InsurerDataReportAgeStatisticsDto>>)
                .Execute();

            var classUsestaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportClassUseStatisticsQuery, InsurerDataReportClassUseStatistics, InsurerDataReportClassUseStatisticsStoredProcedure, InsurerDataReportClassUseStatisticsDto>(new InsurerDataReportClassUseStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportClassUseStatistics>, IList<InsurerDataReportClassUseStatisticsDto>>)
                .Execute();

            var coverTypeStaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportCoverTypeStatisticsQuery, InsurerDataReportCoverTypeStatistics, InsurerDataReportCoverTypeStatisticsStoredProcedure, InsurerDataReportCoverTypeStatisticsDto>(new InsurerDataReportCoverTypeStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportCoverTypeStatistics>, IList<InsurerDataReportCoverTypeStatisticsDto>>)
                .Execute();

            var genderStaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportGenderStatisticsQuery, InsurerDataReportGenderStatistics, InsurerDataReportGenderStatisticsStoredProcedure, InsurerDataReportGenderStatisticsDto>(new InsurerDataReportGenderStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportGenderStatistics>, IList<InsurerDataReportGenderStatisticsDto>>)
                .Execute();

            var generalStaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportGeneralStatisticsQuery, InsurerDataReportGeneralStatistics, InsurerDataReportGeneralStatisticsStoredProcedure, InsurerDataReportGeneralStatisticsDto>(new InsurerDataReportGeneralStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportGeneralStatistics>, IList<InsurerDataReportGeneralStatisticsDto>>)
                .Execute();

            var insuranceTypeStaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportInsuranceTypeStatisticsQuery, InsurerDataReportInsuranceTypeStatistics, InsurerDataReportInsuranceTypeStatisticsStoredProcedure, InsurerDataReportInsuranceTypeStatisticsDto>(new InsurerDataReportInsuranceTypeStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportInsuranceTypeStatistics>, IList<InsurerDataReportInsuranceTypeStatisticsDto>>)
                .Execute();

            var maritalStatusStaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportMaritalStatusStatisticsQuery, InsurerDataReportMaritalStatusStatistics, InsurerDataReportMaritalStatusStatisticsStoredProcedure, InsurerDataReportMaritalStatusStatisticsDto>(new InsurerDataReportMaritalStatusStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportMaritalStatusStatistics>, IList<InsurerDataReportMaritalStatusStatisticsDto>>)
                .Execute();

            var provinceStaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportProvinceStatisticsQuery, InsurerDataReportProvinceStatistics, InsurerDataReportProvinceStatisticsStoredProcedure, InsurerDataReportProvinceStatisticsDto>(new InsurerDataReportProvinceStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportProvinceStatistics>, IList<InsurerDataReportProvinceStatisticsDto>>)
                .Execute();

            var vehicleColourStaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportVehicleColourStatisticsQuery, InsurerDataReportVehicleColourStatistics, InsurerDataReportVehicleColourStatisticsStoredProcedure, InsurerDataReportVehicleColourStatisticsDto>(new InsurerDataReportVehicleColourStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportVehicleColourStatistics>, IList<InsurerDataReportVehicleColourStatisticsDto>>)
                .Execute();

            var vehicleMakeStaticsQueryResult = executionPlan
                .QueryStoredProcedure<GetInsurerDataReportVehicleMakeStatisticsQuery, InsurerDataReportVehicleMakeStatistics, InsurerDataReportVehicleMakeStatisticsStoredProcedure, InsurerDataReportVehicleMakeStatisticsDto>(new InsurerDataReportVehicleMakeStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<InsurerDataReportVehicleMakeStatistics>, IList<InsurerDataReportVehicleMakeStatisticsDto>>)
                .Execute();

            var result = new QueryResult<InsurerDataReportContainerDto>(new InsurerDataReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault() ?? default(InsurerDataReportHeaderDto),
                Statistics = staticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportInsurerQuoteStatisticsDto>),

                AgeStatistics = ageStaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportAgeStatisticsDto>),
                ClassUseStatistics = classUsestaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportClassUseStatisticsDto>),
                CoverTypeStatistics = coverTypeStaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportCoverTypeStatisticsDto>),
                GenderStatistics = genderStaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportGenderStatisticsDto>),
                GeneralStatistics = generalStaticsQueryResult.Response.FirstOrDefault() ?? default(InsurerDataReportGeneralStatisticsDto),
                InsuranceTypeStatistics = insuranceTypeStaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportInsuranceTypeStatisticsDto>),
                MaritalStatusStatistics = maritalStatusStaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportMaritalStatusStatisticsDto>),
                ProvinceStatistics = provinceStaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportProvinceStatisticsDto>),
                VehicleColourStatistics = vehicleColourStaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportVehicleColourStatisticsDto>),
                VehicleMakeStatistics = vehicleMakeStaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportVehicleMakeStatisticsDto>),
                StatisticsByProduct = new List<InsurerDataReportContainerDto>()
            });

            if (productIds.Count() == 0)
                productIds = productQueryResult.Response.Select(x => x.ProductId).ToList();

            foreach (var productId in productIds)
            {
                criteria.ProductIds = productId.ToString();

                var subHeaderQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportHeaderQuery, InsurerDataReportHeader, InsurerDataReportHeaderStoredProcedure, InsurerDataReportHeaderDto>(new InsurerDataReportHeaderStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportHeader>, IList<InsurerDataReportHeaderDto>>)
                    .Execute();

                var subStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportInsurerQuoteStatisticsQuery, InsurerDataReportInsurerQuoteStatistics, InsurerDataReportInsurerQuoteStatisticsStoredProcedure, InsurerDataReportInsurerQuoteStatisticsDto>(new InsurerDataReportInsurerQuoteStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportInsurerQuoteStatistics>, IList<InsurerDataReportInsurerQuoteStatisticsDto>>)
                    .Execute();

                var subAgeStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportAgeStatisticsQuery, InsurerDataReportAgeStatistics, InsurerDataReportAgeStatisticsStoredProcedure, InsurerDataReportAgeStatisticsDto>(new InsurerDataReportAgeStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportAgeStatistics>, IList<InsurerDataReportAgeStatisticsDto>>)
                    .Execute();

                var subClassUsestaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportClassUseStatisticsQuery, InsurerDataReportClassUseStatistics, InsurerDataReportClassUseStatisticsStoredProcedure, InsurerDataReportClassUseStatisticsDto>(new InsurerDataReportClassUseStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportClassUseStatistics>, IList<InsurerDataReportClassUseStatisticsDto>>)
                    .Execute();

                var subCoverTypeStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportCoverTypeStatisticsQuery, InsurerDataReportCoverTypeStatistics, InsurerDataReportCoverTypeStatisticsStoredProcedure, InsurerDataReportCoverTypeStatisticsDto>(new InsurerDataReportCoverTypeStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportCoverTypeStatistics>, IList<InsurerDataReportCoverTypeStatisticsDto>>)
                    .Execute();

                var subGenderStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportGenderStatisticsQuery, InsurerDataReportGenderStatistics, InsurerDataReportGenderStatisticsStoredProcedure, InsurerDataReportGenderStatisticsDto>(new InsurerDataReportGenderStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportGenderStatistics>, IList<InsurerDataReportGenderStatisticsDto>>)
                    .Execute();

                var subGeneralStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportGeneralStatisticsQuery, InsurerDataReportGeneralStatistics, InsurerDataReportGeneralStatisticsStoredProcedure, InsurerDataReportGeneralStatisticsDto>(new InsurerDataReportGeneralStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportGeneralStatistics>, IList<InsurerDataReportGeneralStatisticsDto>>)
                    .Execute();

                var subInsuranceTypeStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportInsuranceTypeStatisticsQuery, InsurerDataReportInsuranceTypeStatistics, InsurerDataReportInsuranceTypeStatisticsStoredProcedure, InsurerDataReportInsuranceTypeStatisticsDto>(new InsurerDataReportInsuranceTypeStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportInsuranceTypeStatistics>, IList<InsurerDataReportInsuranceTypeStatisticsDto>>)
                    .Execute();

                var subMaritalStatusStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportMaritalStatusStatisticsQuery, InsurerDataReportMaritalStatusStatistics, InsurerDataReportMaritalStatusStatisticsStoredProcedure, InsurerDataReportMaritalStatusStatisticsDto>(new InsurerDataReportMaritalStatusStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportMaritalStatusStatistics>, IList<InsurerDataReportMaritalStatusStatisticsDto>>)
                    .Execute();

                var subProvinceStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportProvinceStatisticsQuery, InsurerDataReportProvinceStatistics, InsurerDataReportProvinceStatisticsStoredProcedure, InsurerDataReportProvinceStatisticsDto>(new InsurerDataReportProvinceStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportProvinceStatistics>, IList<InsurerDataReportProvinceStatisticsDto>>)
                    .Execute();

                var subVehicleColourStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportVehicleColourStatisticsQuery, InsurerDataReportVehicleColourStatistics, InsurerDataReportVehicleColourStatisticsStoredProcedure, InsurerDataReportVehicleColourStatisticsDto>(new InsurerDataReportVehicleColourStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportVehicleColourStatistics>, IList<InsurerDataReportVehicleColourStatisticsDto>>)
                    .Execute();

                var subVehicleMakeStaticsQueryResult = executionPlan
                    .QueryStoredProcedure<GetInsurerDataReportVehicleMakeStatisticsQuery, InsurerDataReportVehicleMakeStatistics, InsurerDataReportVehicleMakeStatisticsStoredProcedure, InsurerDataReportVehicleMakeStatisticsDto>(new InsurerDataReportVehicleMakeStatisticsStoredProcedure(criteria))
                    .OnSuccess(Mapper.Map<IList<InsurerDataReportVehicleMakeStatistics>, IList<InsurerDataReportVehicleMakeStatisticsDto>>)
                    .Execute();

                var subResult = new QueryResult<InsurerDataReportContainerDto>(new InsurerDataReportContainerDto
                {
                    Header = subHeaderQueryResult.Response.FirstOrDefault() ?? default(InsurerDataReportHeaderDto),
                    Statistics = subStaticsQueryResult.Response.ToList() ?? default(List<InsurerDataReportInsurerQuoteStatisticsDto>),

                    AgeStatistics = subAgeStaticsQueryResult.Response.ToList() ?? new List<InsurerDataReportAgeStatisticsDto>(),
                    ClassUseStatistics = subClassUsestaticsQueryResult.Response.ToList() ?? new List<InsurerDataReportClassUseStatisticsDto>(),
                    CoverTypeStatistics = subCoverTypeStaticsQueryResult.Response.ToList() ?? new List<InsurerDataReportCoverTypeStatisticsDto>(),
                    GenderStatistics = subGenderStaticsQueryResult.Response.ToList() ?? new List<InsurerDataReportGenderStatisticsDto>(),
                    GeneralStatistics = subGeneralStaticsQueryResult.Response.FirstOrDefault() ?? new InsurerDataReportGeneralStatisticsDto(),
                    InsuranceTypeStatistics = subInsuranceTypeStaticsQueryResult.Response.ToList() ?? new List<InsurerDataReportInsuranceTypeStatisticsDto>(),
                    MaritalStatusStatistics = subMaritalStatusStaticsQueryResult.Response.ToList() ?? new List<InsurerDataReportMaritalStatusStatisticsDto>(),
                    ProvinceStatistics = subProvinceStaticsQueryResult.Response.ToList() ?? new List<InsurerDataReportProvinceStatisticsDto>(),
                    VehicleColourStatistics = subVehicleColourStaticsQueryResult.Response.ToList() ?? new List<InsurerDataReportVehicleColourStatisticsDto>(),
                    VehicleMakeStatistics = subVehicleMakeStaticsQueryResult.Response.ToList() ?? new List<InsurerDataReportVehicleMakeStatisticsDto>(),
                });

                subResult.Response.GeneralStatistics.AverageSumInsured = FormatCurrency(subResult.Response.GeneralStatistics.AverageSumInsured);
                subResult.Response.Product = (subStaticsQueryResult.Response != null && subStaticsQueryResult.Response.Count > 0) ? productQueryResult.Response.Where(x => x.ProductId == productId).FirstOrDefault() : new InsurerDataReportCampaignProductsDto();

                if (subStaticsQueryResult.Response != null && subStaticsQueryResult.Response.Count > 0)
                    result.Response.StatisticsByProduct.Add(subResult.Response);
            }

            result.Response.GeneralStatistics.AverageSumInsured = FormatCurrency(result.Response.GeneralStatistics.AverageSumInsured);

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private string FormatCurrency(string value)
        {
            if (Thread.CurrentThread.CurrentCulture.Name.ToLower() != "en-za")
                return string.Format("{0:C}", value);

            var culture = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
                culture.NumberFormat.CurrencyDecimalSeparator = ".";

            decimal final;
            decimal.TryParse(value, out final);

            Thread.CurrentThread.CurrentCulture = culture;

            return string.Format("{0:C}", final);
        }
    }
}