﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Reports.CallCentre.LeadManagement.Queries;
using Domain.Reports.CallCentre.LeadManagement;
using Domain.Reports.CallCentre.LeadManagement.StoredProcedures;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.LeadManagement;

namespace Api.Modules.Reports.CallCentre
{
    public class LeadManagementReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public LeadManagementReportModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Post[SystemRoutes.CallCentreReports.GetCallCentreLeadManagementReport.Route] = x => GetLeadManagementReport();
        }

        private Negotiator GetLeadManagementReport()
        {
            var criteria = this.Bind<CallCentreReportCriteriaDto>();

            if (criteria.CampaignIds.Length > 0)
            {
                var campaignIds = criteria.CampaignIds.Split(',');
                criteria.CampaignId = Convert.ToInt32(campaignIds[0]);
            }

            var headerQueryResult = executionPlan
                .QueryStoredProcedure<GetLeadManagementReportHeaderQuery, LeadManagementReportHeader, LeadManagementReportHeaderStoredProcedure, LeadManagementReportHeaderDto>(new LeadManagementReportHeaderStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<LeadManagementReportHeader>, IList<LeadManagementReportHeaderDto>>)
                .Execute();

            var staticsQueryResult = executionPlan
                .QueryStoredProcedure<GetLeadManagementReportAgentLeadStatisticsQuery, LeadManagementReportAgentLeadStatistics, LeadManagementReportAgentLeadStatisticsStoredProcedure, LeadManagementReportAgentLeadStatisticsDto>(new LeadManagementReportAgentLeadStatisticsStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<LeadManagementReportAgentLeadStatistics>, IList<LeadManagementReportAgentLeadStatisticsDto>>)
                .Execute();

            var result = new QueryResult<LeadManagementReportContainerDto>(new LeadManagementReportContainerDto
            {
                Header = headerQueryResult.Response.FirstOrDefault() ?? default(LeadManagementReportHeaderDto),
                Statistics = staticsQueryResult.Response.ToList() ?? default(List<LeadManagementReportAgentLeadStatisticsDto>)
            });

            var response = result.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}