﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Party.Addresses;
using Domain.Reports.AIG;
using Domain.Reports.AIG.Queries;
using Domain.Reports.AIG.StoredProcedures;

using iGuide.DTOs.Responses;

using iPlatform.Api.DTOs.AdditionalMembers;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Reports.AIG;
using iPlatform.Api.DTOs.Reports.AIG.AllRisk;
using iPlatform.Api.DTOs.Reports.AIG.AllRisk.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Building;
using iPlatform.Api.DTOs.Reports.AIG.Building.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Content;
using iPlatform.Api.DTOs.Reports.AIG.Content.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.DisasterCash;
using iPlatform.Api.DTOs.Reports.AIG.DisasterCash.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Funeral;
using iPlatform.Api.DTOs.Reports.AIG.Funeral.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.IdentityTheft;
using iPlatform.Api.DTOs.Reports.AIG.IdentityTheft.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Lead;
using iPlatform.Api.DTOs.Reports.AIG.Lead.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Motor;
using iPlatform.Api.DTOs.Reports.AIG.Motor.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.PersonalLegalLiability;
using iPlatform.Api.DTOs.Reports.AIG.PersonalLegalLiability.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.ReportCriteria;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery.Criteria;

namespace Api.Modules.Reports.AIG
{
    public class AigReportModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository _repository;
        private readonly IRootPathProvider root;

        private string assemblyPath = string.Empty;

        public AigReportModule(IExecutionPlan executionPlan, IRepository repository, IRootPathProvider root)
        {
            this.executionPlan = executionPlan;
            this._repository = repository;
            this.root = root;

            this.assemblyPath = root.GetRootPath();

            Get[SystemRoutes.AigReport.GetReportList.Route] = x => GetAigReportList();
            Post[SystemRoutes.AigReport.GetAigReport.Route] = x => GetAigReport();
            Post[SystemRoutes.AigReport.GetAigReportLeadExtract.Route] = x => GetLeadExtractReport();
            Post[SystemRoutes.AigReport.GetAigReportMotorExtract.Route] = x => GetMotorExtractReport();
            Post[SystemRoutes.AigReport.GetAigReportAllRiskExtract.Route] = x => GetAllRiskExtractReport();
            Post[SystemRoutes.AigReport.GetAigReportContentExtract.Route] = x => GetContentExtractReport();
            Post[SystemRoutes.AigReport.GetAigReportBuildingExtract.Route] = x => GetBuildingExtractReport();
            Post[SystemRoutes.AigReport.GetAigReportSalesForceIntegrationExtract.Route] = x => GetSalesForceIntegrationExtractReport();
            Post[SystemRoutes.AigReport.GetAigReportSalesForceLogSummeryExtract.Route] = x => GetSalesForceIntegrationLogSummeryExtractReport();
        }

        private Negotiator GetAigReportList()
        {
            var reportList = new List<string>
            {
                "Lead report",
                "Motor proposal report",
                "All Risk proposal report",
                "Content proposal report",
                "Building proposal report",
                "Funeral Proposal report",
                "Identity theft report",
                "Personal legal liability proposal report",
                "Disaster cash proposal report",
                "SalesForceIntegrationLog report",
                "SalesForceSummeryLog report"
            };

            var response = new POSTResponseDto<List<string>>(reportList, "");

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAigReport()
        {
            var excelFileName = string.Empty;
            var excelGenerator = new ExcelGenerator.CreateExcel(assemblyPath);
            var criteria = this.Bind<AigReportCriteriaDto>();

            switch (criteria.Name)
            {
                case "Lead report":
                    var aigReportLeadExtractCriteriaDto = new AigReportLeadExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataLead = executionPlan
                        .QueryStoredProcedureObject<AigReportLeadExtractQuery, AigReportLeadExtract, AigReportLeadExtractStoredProcedure, AigReportLeadExtractDto>(new AigReportLeadExtractStoredProcedure(aigReportLeadExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportLeadExtract>, IList<AigReportLeadExtractDto>>)
                        .Execute();

                    var partyIds = queryResultDataLead.Response.Select(item => item.PartyId).Distinct().ToList();

                    const int blockSize = 1700;
                    var group = partyIds.Select((x, index) => new {x, index}).GroupBy(x => x.index/blockSize, y => y.x);
                   
                    var queryAddressResult = new List<Address>();
                    foreach (var block in group)
                    {
                        queryAddressResult.AddRange(_repository.GetAll<Address>().Where(c => block.Contains(c.Party.Id)));
                    }

                    var addressResult = queryAddressResult.ToList().GroupBy(row => row.Party)
                                         .SelectMany(g => g.OrderBy(row => row.Party).Take(10))
                                         .ToList();

                    var queryResultMotorPreviousLossData = executionPlan
                        .QueryStoredProcedureObject<AigReportLeadMotorPreviousLossQuery, AigReportLeadQuoteMotorPreviousLossExtract, AigReportLeadMotorPreviousLossStoredProcedure, AigReportLeadQuotePreviousLossExtractDto>(new AigReportLeadMotorPreviousLossStoredProcedure(aigReportLeadExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportLeadQuoteMotorPreviousLossExtract>, IList<AigReportLeadQuotePreviousLossExtractDto>>)
                        .Execute().Response.Where(c => partyIds.Contains(c.PartyId));

                    var queryResultHomePreviousLossData = executionPlan
                        .QueryStoredProcedureObject<AigReportLeadHomePreviousLossQuery, AigReportLeadQuoteHomePreviousLossExtract, AigReportLeadHomePreviousLossStoredProcedure, AigReportLeadQuotePreviousLossExtractDto>(new AigReportLeadHomePreviousLossStoredProcedure(aigReportLeadExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportLeadQuoteHomePreviousLossExtract>, IList<AigReportLeadQuotePreviousLossExtractDto>>)
                        .Execute().Response.Where(c => partyIds.Contains(c.PartyId));

                    var losses = queryResultMotorPreviousLossData.ToList();
                    losses.AddRange(queryResultHomePreviousLossData.ToList());

                      var data = new AigLeadExcelData { Leads = (List<AigReportLeadExtractDto>)queryResultDataLead.Response };

                    var basicAddresses = AddressMapping(addressResult).ToList();

                    for (var i = 0; i < data.Leads.Count; i++)
                    {
                        data.Leads[i].Addresses = basicAddresses.Where(c => c.PartyId == data.Leads[i].PartyId).ToList(); // new List<BasicAddressDto>(); //Mapper.Map<List<BasicAddressDto>>(addressResult).Where(c => c.PartyId == data.Leads[i].PartyId).ToList();
                        data.Leads[i].Losses = losses.Where(c => c.PartyId == data.Leads[i].PartyId).ToList();
                    }


                    excelFileName = excelGenerator.GenerateLeadExtractExcelFile(data);

                    break;

                case "Motor proposal report":
                    var aigReportMotorExtractCriteriaDto = new AigReportMotorExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var aigReportDriverLossCriteriaDto = new AigReportLeadExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataMotors = executionPlan
                        .QueryStoredProcedureObject<AigReportMotorExtractQuery, AigReportMotorExtract, AigReportMotorExtractStoredProcedure, AigReportMotorExtractDto>(new AigReportMotorExtractStoredProcedure(aigReportMotorExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportMotorExtract>, IList<AigReportMotorExtractDto>>)
                        .Execute();

                    var partyIdsForMotorLoss = queryResultDataMotors.Response.Select(item => item.PartyId).Distinct().ToList();

                    var queryResultDriverLossData = executionPlan
                        .QueryStoredProcedureObject<AigReportLeadMotorPreviousLossQuery, AigReportLeadQuoteMotorPreviousLossExtract, AigReportLeadMotorPreviousLossStoredProcedure, AigReportLeadQuoteMotorPreviousLossExtractDto>(new AigReportLeadMotorPreviousLossStoredProcedure(aigReportDriverLossCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportLeadQuoteMotorPreviousLossExtract>, IList<AigReportLeadQuoteMotorPreviousLossExtractDto>>)
                        .Execute().Response.Where(c => partyIdsForMotorLoss.Contains(c.PartyId)).ToList();

                    var resultMotors = new AigReportMotorDto { Motors = (List<AigReportMotorExtractDto>)queryResultDataMotors.Response };

                    for (var i = 0; i < resultMotors.Motors.Count; i++)
                    {
                        resultMotors.Motors[i].MotorLosses = queryResultDriverLossData.Where(c => c.PartyId == resultMotors.Motors[i].PartyId).ToList();
                    }

                    excelFileName = excelGenerator.GenerateMotorExtractExcelFile(resultMotors.Motors);
                    break;


                case "All Risk proposal report":
                    var aigReportAllRiskExtractCriteriaDto = new AigReportAllRiskExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataAllRisk = executionPlan
                        .QueryStoredProcedureObject<AigReportAllRiskExtractQuery, AigReportAllRiskExtract, AigReportAllRiskExtractStoredProcedure, AigReportAllRiskExtractDto>(new AigReportAllRiskExtractStoredProcedure(aigReportAllRiskExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportAllRiskExtract>, IList<AigReportAllRiskExtractDto>>)
                        .Execute();

                    var resultAllRisk = new QueryResult<AigReportAllRiskDto>(new AigReportAllRiskDto
                    {
                        AllRisks = queryResultDataAllRisk.Response.ToList() ?? new List<AigReportAllRiskExtractDto>()
                    });


                    excelFileName = excelGenerator.GenerateAllRiskExtractExcelFile(resultAllRisk.Response.AllRisks);
                    break;

                case "Content proposal report":
                    var aigReportContentExtractCriteriaDto = new AigReportContentExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataContent = executionPlan
                        .QueryStoredProcedureObject<AigReportContentExtractQuery, AigReportContentExtract, AigReportContentExtractStoredProcedure, AigReportContentExtractDto>(new AigReportContentExtractStoredProcedure(aigReportContentExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportContentExtract>, IList<AigReportContentExtractDto>>)
                        .Execute();

                    var resultContent = new QueryResult<AigReportContentDto>(new AigReportContentDto
                    {
                        Contents = queryResultDataContent.Response.ToList() ?? new List<AigReportContentExtractDto>()
                    });

                    excelFileName = excelGenerator.GenerateContentExtractExcelFile(resultContent.Response.Contents);
                    break;


                case "Building proposal report":
                    var aigReportBuildingExtractCriteriaDto = new AigReportBuildingExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataBuilding = executionPlan
                        .QueryStoredProcedureObject<AigReportBuildingExtractQuery, AigReportBuildingExtract, AigReportBuildingExtractStoredProcedure, AigReportBuildingExtractDto>(new AigReportBuildingExtractStoredProcedure(aigReportBuildingExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportBuildingExtract>, IList<AigReportBuildingExtractDto>>)
                        .Execute();

                    var resultBuilding = new QueryResult<AigReportBuildingDto>(new AigReportBuildingDto
                    {
                        Buildings = queryResultDataBuilding.Response.ToList() ?? new List<AigReportBuildingExtractDto>()
                    });

                    excelFileName = excelGenerator.GenerateBuildingExtractExcelFile(resultBuilding.Response.Buildings);
                    break;

                case "Funeral Proposal report":
                    var aigReportFuneralExtractCriteriaDto = new AigReportFuneralExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataFuneral = executionPlan
                        .QueryStoredProcedureObject<AigReportFuneralExtractQuery, AigReportFuneralExtract, AigReportFuneralExtractStoredProcedure, AigReportFuneralExtractDto>(new AigReportFuneralExtractStoredProcedure(aigReportFuneralExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportFuneralExtract>, IList<AigReportFuneralExtractDto>>)
                        .Execute();

                    var proposalDefinitionIds = queryResultDataFuneral.Response.Select(item => item.ProposalDefinitionId).Distinct().ToList();

                    var additionalMembers = _repository.GetAll<Domain.Party.ProposalDefinitions.AdditionalMembers>().Where(c => proposalDefinitionIds.Contains(c.ProposalDefinition.Id));

                    var dataFuneral = new AigReportFuneralDto { Funerals = (List<AigReportFuneralExtractDto>)queryResultDataFuneral.Response };

                    foreach (var item in dataFuneral.Funerals)
                    {
                        item.AdditionalMembers = Mapper.Map<List<ListAdditionalMemberDto>>(additionalMembers).Where(c => c.ProposalDefinition.Id == item.ProposalDefinitionId).ToList();
                    }

                    var resultFuneral = new QueryResult<AigReportFuneralDto>(new AigReportFuneralDto
                    {
                        Funerals = queryResultDataFuneral.Response.ToList() ?? new List<AigReportFuneralExtractDto>()
                    });

                    excelFileName = excelGenerator.GenerateFuneralExtractExcelFile(resultFuneral.Response.Funerals);
                    break;

                case "Identity theft report":
                    var aigReportIdentityTheftExtractCriteriaDto = new AigReportIdentityTheftExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataIdentityTheft = executionPlan
                        .QueryStoredProcedureObject<AigReportIdentityTheftExtractQuery, AigReportIdentityTheftExtract, AigReportIdentityTheftExtractStoredProcedure, AigReportIdentityTheftExtractDto>(new AigReportIdentityTheftExtractStoredProcedure(aigReportIdentityTheftExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportIdentityTheftExtract>, IList<AigReportIdentityTheftExtractDto>>)
                        .Execute();

                    var resultIdentityTheft = new QueryResult<AigReportIdentityTheftDto>(new AigReportIdentityTheftDto
                    {
                        IdentityThefts = queryResultDataIdentityTheft.Response.ToList() ?? new List<AigReportIdentityTheftExtractDto>()
                    });

                    excelFileName = excelGenerator.GenerateIdentityTheftExtractExcelFile(resultIdentityTheft.Response.IdentityThefts);
                    break;

                case "Personal legal liability proposal report":
                    var aigReportPersonalLegalLiabilityExtractCriteriaDto = new AigReportPersonalLegalLiabilityExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataPersonalLegalLiability = executionPlan
                        .QueryStoredProcedureObject<AigReportPersonalLegalLiabilityExtractQuery, AigReportPersonalLegalLiabilityExtract, AigReportPersonalLegalLiabilityExtractStoredProcedure, AigReportPersonalLegalLiabilityExtractDto>(new AigReportPersonalLegalLiabilityExtractStoredProcedure(aigReportPersonalLegalLiabilityExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportPersonalLegalLiabilityExtract>, IList<AigReportPersonalLegalLiabilityExtractDto>>)
                        .Execute();

                    var resultPersonalLegalLiability = new QueryResult<AigReportPersonalLegalLiabilityDto>(new AigReportPersonalLegalLiabilityDto
                    {
                        PersonalLegalLiabilities = queryResultDataPersonalLegalLiability.Response.ToList() ?? new List<AigReportPersonalLegalLiabilityExtractDto>()
                    });

                    excelFileName = excelGenerator.GeneratePersonalLegalLiabilityExtractExcelFile(resultPersonalLegalLiability.Response.PersonalLegalLiabilities);
                    break;

                case "Disaster cash proposal report":
                    var aigReportDisasterCashExtractCriteriaDto = new AigReportDisasterCashExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataDisasterCash = executionPlan
                        .QueryStoredProcedureObject<AigReportDisasterCashExtractQuery, AigReportDisasterCashExtract, AigReportDisasterCashExtractStoredProcedure, AigReportDisasterCashExtractDto>(new AigReportDisasterCashExtractStoredProcedure(aigReportDisasterCashExtractCriteriaDto))
                        .OnSuccess(Mapper.Map<IList<AigReportDisasterCashExtract>, IList<AigReportDisasterCashExtractDto>>)
                        .Execute();

                    var resultDisasterCash = new QueryResult<AigReportDisasterCashDto>(new AigReportDisasterCashDto
                    {
                        DisasterCashes = queryResultDataDisasterCash.Response.ToList() ?? new List<AigReportDisasterCashExtractDto>()
                    });

                    excelFileName = excelGenerator.GenerateDisasterCashExtractExcelFile(resultDisasterCash.Response.DisasterCashes);
                    break;


                case "SalesForceIntegrationLog report":
                    var SalesForceIntegrationExtractCriteriaDto = new AigReportSalesForceIntegrationExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };

                    var queryResultDataSalesForce = executionPlan
                    .QueryStoredProcedureObject<AigReportSalesForceExtractQuery, AigReportSalesForceIntegrationLog, AigReportSalesForceIntegrationExtractStoredProcedure, AigReportSalesForceIntegrationLogExtractDto>(new AigReportSalesForceIntegrationExtractStoredProcedure(SalesForceIntegrationExtractCriteriaDto))
                    .OnSuccess(Mapper.Map<IList<AigReportSalesForceIntegrationLog>, IList<AigReportSalesForceIntegrationLogExtractDto>>)
                    .Execute();

                    var resultSalesForce = new QueryResult<AigReportSalesForceIntegrationDto>(new AigReportSalesForceIntegrationDto
                    {
                        SalesForceIntegration = queryResultDataSalesForce.Response.ToList() ?? new List<AigReportSalesForceIntegrationLogExtractDto>()
                    });

                    excelFileName = excelGenerator.GenerateSalesForceIntegrationExtractExcelFile(resultSalesForce.Response.SalesForceIntegration);
                    break;


                case "SalesForceSummeryLog report":
                    var SalesForceLogSummeryExtractCriteriaDto = new AigReportSalesForceLogSummeryExtractCriteriaDto
                    {
                        StartDate = criteria.StartDate,
                        EndDate = criteria.EndDate
                    };
                    var queryResultDataSummeryLog = executionPlan
                               .QueryStoredProcedureObject<AigReportSalesForceLogSummeryExtractQuery, AigReportSalesForceLogSummery, AigReportSalesForceLogSummeryStoredProcesure, AigReportSalesForceLogSummeryExtractDto>(new AigReportSalesForceLogSummeryStoredProcesure(SalesForceLogSummeryExtractCriteriaDto))
                               .OnSuccess(Mapper.Map<IList<AigReportSalesForceLogSummery>, IList<AigReportSalesForceLogSummeryExtractDto>>)
                               .Execute();

                    var resultSummeryLog = new QueryResult<AigReportSaleForceLogSummeryDto>(new AigReportSaleForceLogSummeryDto
                    {
                        SalesForceLogSummery = queryResultDataSummeryLog.Response.ToList() ?? new List<AigReportSalesForceLogSummeryExtractDto>()
                    });

                    excelFileName = excelGenerator.GenerateSalesForceLogSummeryExtractExcelFile(resultSummeryLog.Response.SalesForceLogSummery);

                    break;

                default:
                    {
                        break;
                    }
            }

            var resultExcel = new AigReportFileDto { ExcelFileName = excelFileName };

            var response = new POSTResponseDto<AigReportFileDto>(resultExcel, excelFileName);

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }


        private List<BasicAddressDto> AddressMapping (List<Address> address)
        {
            var basicAddress = new List<BasicAddressDto>();

            if (address != null)
            {
                for (int i = 0; i < address.Count; i++)
                {
                    basicAddress.Add(new BasicAddressDto
                    {
                        Line1 = address[i].Line1,
                        Line2 = address[i].Line2,
                        Line3 = address[i].Line3,
                        Line4 = address[i].Line4,
                        Code = address[i].Code,
                        StateProvince = address[i].StateProvince.Name,
                        AddressType = address[i].AddressType.Name,
                        PartyId = address[i].Party.Id
                    });
                }
            }

            return basicAddress;
        }

        private Negotiator GetBuildingExtractReport()
        {
            var criteria = this.Bind<AigReportBuildingExtractCriteriaDto>();

            var queryResultData = executionPlan
            .QueryStoredProcedureObject<AigReportBuildingExtractQuery, AigReportBuildingExtract, AigReportBuildingExtractStoredProcedure, AigReportBuildingExtractDto>(new AigReportBuildingExtractStoredProcedure(criteria))
            .OnSuccess(Mapper.Map<IList<AigReportBuildingExtract>, IList<AigReportBuildingExtractDto>>)
            .Execute();

            var result = new QueryResult<AigReportBuildingDto>(new AigReportBuildingDto
            {
                Buildings = queryResultData.Response.ToList() ?? new List<AigReportBuildingExtractDto>()
            });

            var excelGenerator = new ExcelGenerator.CreateExcel(assemblyPath);
            var reportExtract = excelGenerator.GenerateBuildingExtractExcelFile(result.Response.Buildings);

            var resultExcel = new AigReportBuildingResultDto { ExcelFileName = reportExtract };

            var response = new POSTResponseDto<AigReportBuildingResultDto>(resultExcel, reportExtract);

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetContentExtractReport()
        {
            var criteria = this.Bind<AigReportContentExtractCriteriaDto>();

            var queryResultData = executionPlan
            .QueryStoredProcedureObject<AigReportContentExtractQuery, AigReportContentExtract, AigReportContentExtractStoredProcedure, AigReportContentExtractDto>(new AigReportContentExtractStoredProcedure(criteria))
            .OnSuccess(Mapper.Map<IList<AigReportContentExtract>, IList<AigReportContentExtractDto>>)
            .Execute();

            var result = new QueryResult<AigReportContentDto>(new AigReportContentDto
            {
                Contents = queryResultData.Response.ToList() ?? new List<AigReportContentExtractDto>()
            });

            var excelGenerator = new ExcelGenerator.CreateExcel(assemblyPath);
            var reportExtract = excelGenerator.GenerateContentExtractExcelFile(result.Response.Contents);

            var resultExcel = new AigReportContentResultDto { ExcelFileName = reportExtract };

            var response = new POSTResponseDto<AigReportContentResultDto>(resultExcel, reportExtract);

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllRiskExtractReport()
        {
            var criteria = this.Bind<AigReportAllRiskExtractCriteriaDto>();

            var queryResultData = executionPlan
            .QueryStoredProcedureObject<AigReportAllRiskExtractQuery, AigReportAllRiskExtract, AigReportAllRiskExtractStoredProcedure, AigReportAllRiskExtractDto>(new AigReportAllRiskExtractStoredProcedure(criteria))
            .OnSuccess(Mapper.Map<IList<AigReportAllRiskExtract>, IList<AigReportAllRiskExtractDto>>)
            .Execute();

            var result = new QueryResult<AigReportAllRiskDto>(new AigReportAllRiskDto
            {
                AllRisks = queryResultData.Response.ToList() ?? new List<AigReportAllRiskExtractDto>()
            });

            var excelGenerator = new ExcelGenerator.CreateExcel(assemblyPath);
            var reportExtract = excelGenerator.GenerateAllRiskExtractExcelFile(result.Response.AllRisks);

            var resultExcel = new AigReportAllRiskResultDto { ExcelFileName = reportExtract };

            var response = new POSTResponseDto<AigReportAllRiskResultDto>(resultExcel, reportExtract);

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetMotorExtractReport()
        {
            var criteria = this.Bind<AigReportMotorExtractCriteriaDto>();

            var queryResultData = executionPlan
            .QueryStoredProcedureObject<AigReportMotorExtractQuery, AigReportMotorExtract, AigReportMotorExtractStoredProcedure, AigReportMotorExtractDto>(new AigReportMotorExtractStoredProcedure(criteria))
            .OnSuccess(Mapper.Map<IList<AigReportMotorExtract>, IList<AigReportMotorExtractDto>>)
            .Execute();

            var result = new QueryResult<AigReportMotorDto>(new AigReportMotorDto
            {
                Motors = queryResultData.Response.ToList() ?? new List<AigReportMotorExtractDto>()
            });

            var excelGenerator = new ExcelGenerator.CreateExcel(assemblyPath);
            var reportExtract = excelGenerator.GenerateMotorExtractExcelFile(result.Response.Motors);

            var resultExcel = new AigReportMotorResultDto { ExcelFileName = reportExtract };

            var response = new POSTResponseDto<AigReportMotorResultDto>(resultExcel, reportExtract);

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetSalesForceIntegrationExtractReport()
        {
            var criteria = this.Bind<AigReportSalesForceIntegrationExtractCriteriaDto>();

            var queryResultData = executionPlan
            .QueryStoredProcedureObject<AigReportSalesForceExtractQuery, AigReportSalesForceIntegrationLog, AigReportSalesForceIntegrationExtractStoredProcedure, AigReportSalesForceIntegrationLogExtractDto>(new AigReportSalesForceIntegrationExtractStoredProcedure(criteria))
            .OnSuccess(Mapper.Map<IList<AigReportSalesForceIntegrationLog>, IList<AigReportSalesForceIntegrationLogExtractDto>>)
            .Execute();

            var result = new QueryResult<AigReportSalesForceIntegrationDto>(new AigReportSalesForceIntegrationDto
            {
                SalesForceIntegration = queryResultData.Response.ToList() ?? new List<AigReportSalesForceIntegrationLogExtractDto>()
            });

            var excelGenerator = new ExcelGenerator.CreateExcel(assemblyPath);
            var reportExtract = excelGenerator.GenerateSalesForceIntegrationExtractExcelFile(result.Response.SalesForceIntegration);

            var resultExcel = new AigReportSalesForceIntegrationResultDto { ExcelFileName = reportExtract };

            var response = new POSTResponseDto<AigReportSalesForceIntegrationResultDto>(resultExcel, reportExtract);

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator GetSalesForceIntegrationLogSummeryExtractReport()
        {
            var criteria = this.Bind<AigReportSalesForceLogSummeryExtractCriteriaDto>();

            var queryResultData = executionPlan
            .QueryStoredProcedureObject<AigReportSalesForceLogSummeryExtractQuery, AigReportSalesForceLogSummery, AigReportSalesForceLogSummeryStoredProcesure, AigReportSalesForceLogSummeryExtractDto>(new AigReportSalesForceLogSummeryStoredProcesure(criteria))
            .OnSuccess(Mapper.Map<IList<AigReportSalesForceLogSummery>, IList<AigReportSalesForceLogSummeryExtractDto>>)
            .Execute();

            var result = new QueryResult<AigReportSaleForceLogSummeryDto>(new AigReportSaleForceLogSummeryDto
            {
                SalesForceLogSummery = queryResultData.Response.ToList() ?? new List<AigReportSalesForceLogSummeryExtractDto>()
            });

            var excelGenerator = new ExcelGenerator.CreateExcel(assemblyPath);
            var reportExtract = excelGenerator.GenerateSalesForceLogSummeryExtractExcelFile(result.Response.SalesForceLogSummery);

            var resultExcel = new AigReportSalesForceLogSummeryResultDto { ExcelFileName = reportExtract };

            var response = new POSTResponseDto<AigReportSalesForceLogSummeryResultDto>(resultExcel, reportExtract);

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetLeadExtractReport()
        {
            var criteria = this.Bind<AigReportLeadExtractCriteriaDto>();

            var queryResultData = executionPlan
            .QueryStoredProcedureObject<AigReportLeadExtractQuery, AigReportLeadExtract, AigReportLeadExtractStoredProcedure, AigReportLeadExtractDto>(new AigReportLeadExtractStoredProcedure(criteria))
            .OnSuccess(Mapper.Map<IList<AigReportLeadExtract>, IList<AigReportLeadExtractDto>>)
            .Execute();

            var partyIds = queryResultData.Response.Select(item => item.PartyId).Distinct().ToList();

            var queryAddressResult = _repository.GetAll<Address>().Where(c => partyIds.Contains(c.Party.Id));

            var queryResultMotorPreviousLossData = executionPlan
                .QueryStoredProcedureObject<AigReportLeadMotorPreviousLossQuery, AigReportLeadQuoteMotorPreviousLossExtract, AigReportLeadMotorPreviousLossStoredProcedure, AigReportLeadQuoteMotorPreviousLossExtractDto>(new AigReportLeadMotorPreviousLossStoredProcedure(criteria))
                .OnSuccess(Mapper.Map<IList<AigReportLeadQuoteMotorPreviousLossExtract>, IList<AigReportLeadQuoteMotorPreviousLossExtractDto>>)
                .Execute().Response.Where(c => partyIds.Contains(c.PartyId));

            var queryResultHomePreviousLossData = executionPlan
            .QueryStoredProcedureObject<AigReportLeadHomePreviousLossQuery, AigReportLeadQuoteHomePreviousLossExtract, AigReportLeadHomePreviousLossStoredProcedure, AigReportLeadQuoteHomePreviousLossExtractDto>(new AigReportLeadHomePreviousLossStoredProcedure(criteria))
            .OnSuccess(Mapper.Map<IList<AigReportLeadQuoteHomePreviousLossExtract>, IList<AigReportLeadQuoteHomePreviousLossExtractDto>>)
            .Execute().Response.Where(c => partyIds.Contains(c.PartyId));

            var data = new AigLeadExcelData { Leads = (List<AigReportLeadExtractDto>)queryResultData.Response };

            foreach (var item in data.Leads)
            {
                item.Addresses = Mapper.Map<List<BasicAddressDto>>(queryAddressResult).Where(c => c.PartyId == item.PartyId).ToList();
                item.MotorLosses = queryResultMotorPreviousLossData.Where(c => c.PartyId == item.PartyId).ToList();
                item.HomeLosses = queryResultHomePreviousLossData.Where(c => c.PartyId == item.PartyId).ToList();
            }


            var excelGenerator = new ExcelGenerator.CreateExcel(assemblyPath);
            var reportExtract = excelGenerator.GenerateLeadExtractExcelFile(data);

            var resultExcel = new AigReportLeadResultDto { ExcelFileName = reportExtract };

            var response = new POSTResponseDto<AigReportLeadResultDto>(resultExcel, reportExtract);

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
    }
}