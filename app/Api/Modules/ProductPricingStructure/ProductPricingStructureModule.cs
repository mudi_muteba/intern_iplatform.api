﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.ProductPricingStructure.Queries;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.ProductPricingStructure;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Modules.ProductPricingStructure
{
    public class ProductPricingStructureModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IRepository m_Repository;

        public ProductPricingStructureModule(IExecutionPlan executionPlan, IRepository repository)
        {
            m_ExecutionPlan = executionPlan;
            m_Repository = repository;

            Get[SystemRoutes.ProductPricingStructure.GetByChannelIdProductIdAndCoverId.Route] = p => GetByChannelIProductIdAndCoverId(p.channelId, p.productId, p.coverId);
        }

        private Negotiator GetByChannelIProductIdAndCoverId(int channelId, int productId, int coverId)
        {
            QueryResult<ProductPricingStructureDto> queryResult = m_ExecutionPlan
                .Query<GetProductPricingStructureByChannelIdProductIdAndCoverIdQuery, Domain.ProductPricingStructure.ProductPricingStructure, ProductPricingStructureDto, Domain.ProductPricingStructure.ProductPricingStructure>()
                .Configure(c => c.WIthCriteria(channelId, productId, coverId))
                .OnSuccess(r => Mapper.Map<Domain.ProductPricingStructure.ProductPricingStructure, ProductPricingStructureDto>(r.FirstOrDefault()))
                .Execute();


            GETResponseDto<ProductPricingStructureDto> response = queryResult.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ProductPricingStructure.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}