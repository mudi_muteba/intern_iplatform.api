﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.RatingRuleHeaders;
using Domain.RatingRuleHeaders.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using Nancy.Responses.Negotiation;
using System.Linq;

namespace Api.Modules.RatingRuleHeaders
{
    public class RatingRuleHeadersModule : SecureModule
    {
        private readonly IRepository _repository;
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public RatingRuleHeadersModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.RatingRuleHeaders.Get.Route] = p => GetAll(new Pagination());
        }

        private Negotiator GetAll(Pagination pagination)
        {
            var queryResult = executionPlan
                 .Query<GetAllRatingRuleHeadersQuery, RatingRuleHeader, PagedResultDto<ListRatingRuleHeaderDto>, ListRatingRuleHeaderDto>()
                 .OnSuccess(result => 
                 {
                     return Mapper.Map<PagedResultDto<ListRatingRuleHeaderDto>>(result.Paginate(paginator,pagination));
                 })
                 .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.RatingRuleHeaders.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}