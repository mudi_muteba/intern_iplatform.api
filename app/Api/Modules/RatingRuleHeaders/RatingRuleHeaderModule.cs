﻿using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using AutoMapper;
using Domain.RatingRuleHeaders;

namespace Api.Modules.RatingRuleHeaders
{
    public class RatingRuleHeaderModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public RatingRuleHeaderModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;
            Post[SystemRoutes.RatingRuleHeaders.Post.Route] = p => Create();
            Put[SystemRoutes.RatingRuleHeaders.Put.Route] = p => Update(p.id);
            Get[SystemRoutes.RatingRuleHeaders.GetById.Route] = p => GetById(p.id);
            Put[SystemRoutes.RatingRuleHeaders.PutDelete.Route] = p => DeleteUpdate(p.id);
        }

        private Negotiator Create()
        {
            var result = _executionPlan.Execute<CreateRatingRuleHeaderDto, int>(this.Bind<CreateRatingRuleHeaderDto>());
            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.RatingRuleHeaders.CreateGetById(result.Response));
            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator Update(int id)
        {
            var result = _executionPlan.Execute<EditRatingRuleHeaderDto, int>(this.Bind<EditRatingRuleHeaderDto>());
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.RatingRuleHeaders.CreateGetById(result.Response));
            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator GetById(int id)
        {
            var result = _executionPlan.GetById<RatingRuleHeader, RatingRuleHeaderDto>(() => _repository
            .GetById<RatingRuleHeader>(id))
            .OnSuccess(Mapper.Map<RatingRuleHeader, RatingRuleHeaderDto>).Execute();
            var response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.RatingRuleHeaders.CreateLinks(c));
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator DeleteUpdate(int id)
        {
            var result = _executionPlan.Execute<DeleteRatingRuleHeaderDto, int>(new DeleteRatingRuleHeaderDto() { Id = id});
            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.RatingRuleHeaders.CreateGetById(result.Response));
            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }
    }
}