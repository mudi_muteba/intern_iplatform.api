﻿using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.SalesForce;
using Domain.SalesForce.Queries;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.SalesForce;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.SalesForce
{
    public class SalesForceModule : SecureModule
    {
        private readonly IProvideContext _contextProvider;
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public SalesForceModule(IExecutionPlan executionPlan, IRepository repository, IProvideContext contextProvider)
        {
            _executionPlan = executionPlan;
            _contextProvider = contextProvider;
            _repository = repository;
            Post[SystemRoutes.SalesForce.Post.Route] = p => PostModel();
            Post[SystemRoutes.SalesForce.PostStatus.Route] = p => PostStatus();
            Post[SystemRoutes.SalesForce.PostQuoteStatus.Route] = p => PostQuoteStatus();
            Post[SystemRoutes.SalesForce.PostExternal.Route] = p => PostExternal();
            Post[SystemRoutes.SalesForce.PostRetry.Route] = p => PostRetry();
            Post[SystemRoutes.SalesForce.PostSalesForceEntry.Route] = p => RepostSalesForceEntry();
            Put[SystemRoutes.SalesForce.PutStatus.Route] = p => UpdateEntry();
            Get[SystemRoutes.SalesForce.GetStatus.Route] = p => GetEntry(p.id);
        }

        private Negotiator RepostSalesForceEntry()
        {
            var dto = this.Bind<RepostSalesForceEntryDto>();

            var result = _executionPlan.Execute<RepostSalesForceEntryDto, int>(dto);
            var response = result.CreatePOSTResponse();
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator PostRetry()
        {
            var dto = this.Bind<SalesForceRetryDto>();

            var result = _executionPlan.Execute<SalesForceRetryDto, int>(dto);
            var response = result.CreatePOSTResponse();
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator PostExternal()
        {
            var dto = this.Bind<SalesForceExternalDto>();

            var result = _executionPlan.Execute<SalesForceExternalDto, SalesForceExternalResponseDto>(dto);
            var response = result.CreatePOSTResponse();
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator PostStatus()
        {
            var dto = this.Bind<SalesForceLeadStatusDto>();

            var result = _executionPlan.Execute<SalesForceLeadStatusDto, int>(dto);
            var response = result.CreatePOSTResponse();
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator PostQuoteStatus()
        {
            var dto = this.Bind<SalesForceQuoteStatusDto>();

            var result = _executionPlan.Execute<SalesForceQuoteStatusDto, int>(dto);
            var response = result.CreatePOSTResponse();
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetEntry(int id)
        {
            var queryResult =
                _executionPlan.Query<GetSalesForceEntryByCriteriaQuery, SalesForceEntry, SalesForceEntryDto>()
                    .Configure(c => c.WithPartyId(id))
                    .OnSuccess(
                        r =>
                        {
                            return
                                r.ToList()
                                    .Select(s => Mapper.Map<SalesForceEntry, SalesForceEntryDto>(s))
                                    .FirstOrDefault();
                        })
                    .Execute();

            var response = queryResult.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.SalesForce.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        //update delay / suspend from widget 
        private Negotiator UpdateEntry()
        {
            var dto = this.Bind<EditSalesForceEntryDto>();

            var result = _executionPlan.Execute<EditSalesForceEntryDto, int>(dto);
            var response = result.CreatePOSTResponse();
            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator PostModel()
        {
            var dto = this.Bind<SalesForceLeadDto>();
            var result = _executionPlan.Execute<SalesForceLeadDto, int>(dto);
            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}