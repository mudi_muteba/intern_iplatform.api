﻿
using System.Collections.Generic;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Claims.ClaimsHeaders;
using Domain.Claims.ClaimsHeaders.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using MasterData;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Security.Claims;
using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.ClaimTypes;
using iPlatform.Api.DTOs.Claims.Questions;

namespace Api.Modules.Claims.ClaimTypes
{
    public class ClaimTypeModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public ClaimTypeModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;


            Get[SystemRoutes.ClaimType.GetClaimTypeByProductId.Route] = p => GetClaimTypeByProduct(p.id, p.productId);
        }

        private Negotiator GetClaimTypeByProduct(int id, int? productId = 0)
        {
            var criteria = new GetClaimTypeByProductDto
            {
                Id = id,
                ProductId = productId
            };

            var result = executionPlan.Execute<GetClaimTypeByProductDto, ClaimsTypeDetailDto>(criteria);

            var response = new GETResponseDto<ClaimsTypeDetailDto>(result.Response);

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}