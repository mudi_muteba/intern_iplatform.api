﻿
using System.Collections.Generic;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Claims.ClaimsHeaders;
using Domain.Claims.ClaimsHeaders.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using MasterData;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Security.Claims;
using System.Linq;
using iPlatform.Api.DTOs.Claims.ClaimTypes;

namespace Api.Modules.Claims.ClaimTypes
{
    public class ClaimTypesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public ClaimTypesModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Options[SystemRoutes.ClaimType.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ClaimType));

            Get[SystemRoutes.ClaimType.GetByOrganisationId.Route] = p => GetClaimType(p.organizationId);
        } 

        private Negotiator GetClaimType(int? organizationId)
        {
            var result = executionPlan.Execute<ClaimTypeSearchDto, List<ClaimsTypeDto>>(new ClaimTypeSearchDto(organizationId));

             var response = result.CreateGETResponse()
                //.CreateLinks(c => SystemRoutes.ClaimType.CreateLinks(c))
            ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}