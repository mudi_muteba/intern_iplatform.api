using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Claims.ClaimsHeaders
{
    public class ClaimsHeaderModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IRepository repository;

        public ClaimsHeaderModule(IExecutionPlan executionPlan, IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.repository = repository;

            Put[SystemRoutes.ClaimsHeader.PutAccept.Route] = p => SubmitClaimsHeader(p.id);
            Put[SystemRoutes.ClaimsHeader.PutDisable.Route] = p => DisableClaimsHeader(p.id);

            Get[SystemRoutes.ClaimsHeader.GetById.Route] = p => GetClaimsHeader(p.id);
            
        }
        private Negotiator SubmitClaimsHeader(int id)
        {
            var acceptClaimDto = this.Bind<AcceptClaimDto>();
            acceptClaimDto.Id = id;

            var result = executionPlan.Execute<AcceptClaimDto, int>(acceptClaimDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ClaimsHeader.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator DisableClaimsHeader(int id)
        {
            var disableClaim = this.Bind<DisableClaimDto>();
            disableClaim.Id = id;

            var result = executionPlan.Execute<DisableClaimDto, int>(disableClaim);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ClaimsHeader.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetClaimsHeader(int id)
        {
            var result = executionPlan
                .GetById<ClaimsHeader, ClaimsHeaderDto>(() => repository.GetById<ClaimsHeader>(id))
                .OnSuccess(Mapper.Map<ClaimsHeader, ClaimsHeaderDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ClaimsHeader.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}