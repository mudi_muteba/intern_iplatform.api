﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Claims.ClaimsHeaders;
using Domain.Claims.ClaimsHeaders.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Claims.ClaimsHeaders
{
    public class ClaimsHeadersModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public ClaimsHeadersModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Options[SystemRoutes.ClaimsHeader.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ClaimsHeader));

            Post[SystemRoutes.ClaimsHeader.Post.Route] = p => CreateClaimHeader();
            Post[SystemRoutes.ClaimsHeader.PostAccept.Route] = p => AcceptClaim();
            Post[SystemRoutes.ClaimsHeader.PostSearch.Route] = p => SearchClaims();
        }

        private Negotiator SearchClaims()
        {
            var criteria = this.Bind<ClaimSearchDto>();

            var queryResult = _executionPlan
                .Search<SearchClaimsQuery, ClaimsHeader, ClaimSearchDto, PagedResultDto<ClaimsHeaderDto>, ClaimsHeaderDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ClaimsHeaderDto>>(
                    result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.ClaimsHeader.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateClaimHeader()
        {
            var createCampaignDto = this.Bind<CreateClaimsHeaderDto>();

            var result = _executionPlan.Execute<CreateClaimsHeaderDto, int>(createCampaignDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ClaimsHeader.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        //Used by workflow. do not use this
        private Negotiator AcceptClaim()
        {
            var acceptClaimDto = this.Bind<AcceptClaimDto>();

            var result = _executionPlan.Execute<AcceptClaimDto, AcceptClaimResponseDto>(acceptClaimDto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}