﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Claims.ClaimsItems;
using Domain.Claims.ClaimsItems.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Claims.ClaimsItems
{
    public class ClaimsItemsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ClaimsItemsModule(IExecutionPlan executionPlan, IRepository repository,
            IPaginateResults paginator)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Options[SystemRoutes.ClaimsItem.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ClaimsItem));

            Get[SystemRoutes.ClaimsItem.GetClaimsItems.Route] = p => GetClaimsItems(p.claimsHeaderId);

            Post[SystemRoutes.ClaimsItem.CreateClaimsItems.Route] = p => CreateClaimsItem(p.claimsHeaderId);

            Put[SystemRoutes.ClaimsItem.UpdateClaimsItems.Route] = p => UpdateClaimsItem(p.claimsHeaderId);
        }

        private Negotiator GetClaimsItems(int claimsHeaderId)
        {
            QueryResult<List<ListClaimsItemDto>> queryResult = executionPlan
                .Query<GetClaimsItemsByHeaderIdQuery, ClaimsItem, List<ListClaimsItemDto>, ListClaimsItemDto>()
                .Configure(q => q.WithHeaderId(claimsHeaderId))
                .OnSuccess(r => Mapper.Map<List<ClaimsItem>, List<ListClaimsItemDto>>(r.ToList()))
                .Execute();

            var result = new ListResultDto<ListClaimsItemDto> { Results = queryResult.Response };

            LISTResponseDto<ListClaimsItemDto> response = queryResult.CreateLISTResponse(result)
                .CreateLinks(c => SystemRoutes.ClaimsItem.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateClaimsItem(int claimsHeaderId)
        {
            var createClaimsItemsDto = this.Bind<CreateClaimsItemsDto>();

            var result = executionPlan.Execute<CreateClaimsItemsDto, int>(createClaimsItemsDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ClaimsItem.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateClaimsItem(int claimsHeaderId)
        {
            var updateClaimsItemsDto = this.Bind<UpdateClaimsItemsDto>();

            var result = executionPlan.Execute<UpdateClaimsItemsDto, int>(updateClaimsItemsDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ClaimsItem.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

    }
}