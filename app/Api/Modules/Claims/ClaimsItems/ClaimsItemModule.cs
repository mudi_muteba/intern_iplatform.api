﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Claims.ClaimsItems;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Claims.ClaimsItems
{
    public class ClaimItemModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ClaimItemModule(IExecutionPlan executionPlan, IRepository repository,
            IPaginateResults paginator)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Options[SystemRoutes.ClaimsItem.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ClaimsItem));

            Get[SystemRoutes.ClaimsItem.GetClaimsItem.Route] = p => GetClaimsItem(p.claimsHeaderId, p.Id);

            Put[SystemRoutes.ClaimsItem.DeleteClaimsItem.Route] = p => DeleteClaimsItem(p.claimsHeaderId, p.Id);

            Post[SystemRoutes.ClaimsItem.SaveClaimsItemAnswers.Route] = p => SaveClaimItemAnswers(p.claimsHeaderId, p.Id);
        }

        private Negotiator GetClaimsItem(int claimsHeaderId, int id)
        {
            var value = repository.GetById<ClaimsItem>(id);
            var result = executionPlan
                .GetById<ClaimsItem, ClaimsItemDto>(() => repository.GetById<ClaimsItem>(id))
                .OnSuccess(Mapper.Map<ClaimsItem, ClaimsItemDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.ClaimsItem.CreateLinks(c))
                ;

            var result1 = Negotiate.ForSupportedFormats(response, response.StatusCode);
            return result1;
        }

        private Negotiator DeleteClaimsItem(int claimsHeaderId, int id)
        {
            var deleteClaimsItem = this.Bind<DisableClaimItemDto>();

            var result = executionPlan.Execute<DisableClaimItemDto, int>(deleteClaimsItem);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ClaimsItem.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator SaveClaimItemAnswers(int claimsHeaderId, int id)
        {
            var claimsItemDto = this.Bind<SaveClaimsItemAnswersDto>();

            var result = executionPlan.Execute<SaveClaimsItemAnswersDto, int>(claimsItemDto);


            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ClaimsItem.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

    }
}