﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Teams;
using Nancy;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;
using Domain.Teams.Queries;
using Domain.Teams;
using AutoMapper;
using iPlatform.Api.DTOs.Base.Culture;



namespace Api.Modules.Teams
{
    public class TeamModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public TeamModule(IPaginateResults paginator, IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Put[SystemRoutes.Teams.Put.Route] = p => Update(p.id);

            Get[SystemRoutes.Teams.GetById.Route] = p => GetTeam(p.id);

            Put[SystemRoutes.Teams.PutDisable.Route] = p => DisableTeam(p.id);

        }

        private Negotiator Update(int id)
        {
            var editTeamDto = this.Bind<EditTeamDto>();
            editTeamDto.Id = id;

            var result = executionPlan.Execute<EditTeamDto, int>(editTeamDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Teams.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetTeam(int id)
        {
            var value = repository.GetById<Team>(id);
            var result = executionPlan
                .GetById<Team, ListTeamDto>(() => repository.GetById<Team>(id))
                .OnSuccess(Mapper.Map<Team, ListTeamDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Teams.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableTeam(int id)
        {
            var disableTeamDto = new DisableTeamDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableTeamDto, int>(disableTeamDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Teams.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}