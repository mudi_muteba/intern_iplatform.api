﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Teams;
using Nancy;
using Api.Infrastructure;
using Api.Models;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;
using Domain.Teams.Queries;
using Domain.Teams;
using AutoMapper;

namespace Api.Modules.Teams
{
    public class TeamsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public TeamsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Post[SystemRoutes.Teams.Post.Route] = p => CreateTeam();

            Post[SystemRoutes.Teams.PostSearch.Route] = p => Search();

            Options[SystemRoutes.Teams.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Teams));
        }

        private Negotiator CreateTeam()
        {
            var createTeamDto = this.Bind<CreateTeamDto>();

            var result = executionPlan.Execute<CreateTeamDto, int>(createTeamDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Teams.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<SearchTeamDto>();

            var queryResult = executionPlan
                .Search<SearchTeamsQuery, Team, SearchTeamDto, PagedResultDto<ListTeamDto>, ListTeamDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListTeamDto>>(result.Paginate(paginator, criteria.CreatePagination())))
                .Execute();


            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Teams.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

    }
}