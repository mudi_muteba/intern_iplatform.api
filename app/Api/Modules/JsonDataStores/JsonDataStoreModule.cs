﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.JsonDataStores;
using iPlatform.Api.DTOs.JsonDataStores;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.JsonDataStores
{
    public class JsonDataStoreModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public JsonDataStoreModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository)
        {
            this.executionPlan = executionPlan;
            this.paginator = paginator;
            this.repository = repository;

            Get[SystemRoutes.JsonDataStores.GetById.Route] = p => GetById(p.Id);
        }

        public Negotiator GetById(int id)
        {
            var result = executionPlan
                .GetById<JsonDataStore, JsonDataStoreDto>(() => repository.GetById<JsonDataStore>(id))
                .OnSuccess(Mapper.Map<JsonDataStore, JsonDataStoreDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.JsonDataStores.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}