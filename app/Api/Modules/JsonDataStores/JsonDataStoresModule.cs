﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.JsonDataStores;
using Domain.JsonDataStores.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.JsonDataStores;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.JsonDataStores
{
    public class JsonDataStoresModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;
        private readonly IRepository _repository;

        public JsonDataStoresModule(IExecutionPlan executionPlan, IPaginateResults paginator,
            IRepository repository)
        {
            this._executionPlan = executionPlan;
            this._paginator = paginator;
            this._repository = repository;

            Get[SystemRoutes.JsonDataStores.GetByPartyId.Route] = p => GetByPartyId(p.id);
            Post[SystemRoutes.JsonDataStores.Post.Route] = p => SaveJsonRecord();
            Post[SystemRoutes.JsonDataStores.PostSearch.Route] = p => Search();

            Options[SystemRoutes.JsonDataStores.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.JsonDataStores));
        }

        private Negotiator GetByPartyId(int partyId)
        {
            var queryResult = _executionPlan
                            .Query<GetJsonDataStoreByPartyId, JsonDataStore, ListResultDto<JsonDataStoreDto>, JsonDataStoreDto>()
                            .Configure(q => q.WithPartyId(partyId))
                            .OnSuccess(r => Mapper.Map<List<JsonDataStore>, ListResultDto<JsonDataStoreDto>>(r.ToList()))
                            .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                 .CreateLinks(c => SystemRoutes.JsonDataStores.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator SaveJsonRecord()
        {
            var createJsonStorageDto = this.Bind<CreateJsonDataStoreDto>();

            var result = _executionPlan.Execute<CreateJsonDataStoreDto, int>(createJsonStorageDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.JsonDataStores.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<JsonDataStoreSearchDto>();

            var queryResult = _executionPlan
                .Search<SearchJsonDataStoreQuery, JsonDataStore, JsonDataStoreSearchDto, ListResultDto<JsonDataStore>>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(r => Mapper.Map<List<JsonDataStore>, ListResultDto<JsonDataStore>>(r.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response);
                 //.CreateLinks(c => SystemRoutes.JsonDataStores.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}