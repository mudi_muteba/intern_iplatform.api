﻿using System;
using Api.Infrastructure;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy;

namespace Api.Modules
{
    public class TestModule : SecureModule
    {
        public TestModule()
        {
            Get[SystemRoutes.Test.Get.Route] = p =>
            {
                return Negotiate
                    .WithStatusCode(HttpStatusCode.OK)
                    .ForSupportedFormats(new object(), System.Net.HttpStatusCode.OK);
            };

            Get[SystemRoutes.Test.Failure.Route] = p =>
            {
                throw new Exception();
            };
        }
    }
}