﻿using Api.Infrastructure;
using Api.Models;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy;
using Nancy.Responses.Negotiation;

namespace Api.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Options[SystemRoutes.Home.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap().GetOptionSiteNodes(Context));
            Get[SystemRoutes.Home.GetSiteMap.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap().GetAllSiteNodes());
        }
    }
}