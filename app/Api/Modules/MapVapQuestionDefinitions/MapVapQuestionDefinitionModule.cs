﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Domain.Base.Repository;
using Domain.QuestionDefinitions;

namespace Api.Modules.MapVapQuestionDefinitions
{
    public class MapVapQuestionDefinitionModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public MapVapQuestionDefinitionModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.MapVapQuestionDefinition.GetById.Route] = p => GetById(p.id);
            Put[SystemRoutes.MapVapQuestionDefinition.Put.Route] = p => Update(p.id);
            Put[SystemRoutes.MapVapQuestionDefinition.PutDelete.Route] = p => DeleteMapVapQuestionsDefinition(p.id);
        }

        #region [ Negotiators ]

        /// <summary>
        /// Get a MapVapQuestionDefinition by it's unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Negotiator GetById(int id)
        {
            var result = _executionPlan
                .GetById<MapVapQuestionDefinition, MapVapQuestionDefinitionDto>(() => _repository.GetById<MapVapQuestionDefinition>(id))
                .OnSuccess(Mapper.Map<MapVapQuestionDefinition, MapVapQuestionDefinitionDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.MapVapQuestionDefinition.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Updates a MapVapQuestionDefinition by it's unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Negotiator Update(int id)
        {
            var editDto = this.Bind<EditMapVapQuestionDefinitionDto>();

            var result = _executionPlan.Execute<EditMapVapQuestionDefinitionDto, int>(editDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Permissions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Deletes a MapVapQuestionDefinition by it's unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Negotiator DeleteMapVapQuestionsDefinition(int id)
        {
            var deleteDto = new DeleteMapVapQuestionDefinitionDto { Id  = id };

            var result = _executionPlan.Execute<DeleteMapVapQuestionDefinitionDto, bool>(deleteDto);

            var response = result.CreatePUTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        #endregion
    }
}