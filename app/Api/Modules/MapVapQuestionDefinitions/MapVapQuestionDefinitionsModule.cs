﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.QuestionDefinitions;
using Domain.QuestionDefinitions.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.MapVapQuestionDefinitions
{
    public class MapVapQuestionDefinitionsModule : SecureModule
    {
        private readonly IPaginateResults _Paginator;
        private readonly IExecutionPlan _ExecutionPlan;

        public MapVapQuestionDefinitionsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _Paginator = paginator;
            _ExecutionPlan = executionPlan;

            Post[SystemRoutes.MapVapQuestionDefinition.Post.Route] = _ => Create();
            Post[SystemRoutes.MapVapQuestionDefinition.Search.Route] = _ => Search();
            Get[SystemRoutes.MapVapQuestionDefinition.GetList.Route] = p => GetByProductAndChannel(p.productId, p.channelId);
            Get[SystemRoutes.MapVapQuestionDefinition.GetListByEnabled.Route] = p => GetEnabledByProductAndChannel(p.productId, p.channelId, p.enabled);
        }

        #region [ Negotiators ]

        /// <summary>
        /// Create a new MapVapQuestionDefinition
        /// </summary>
        /// <returns></returns>
        private Negotiator Create()
        {
            var createDto = this.Bind<CreateMapVapQuestionDefinitionDto>();
            HandlerResult<int> result = _ExecutionPlan.Execute<CreateMapVapQuestionDefinitionDto, int>(createDto);
            POSTResponseDto<int> response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.MapVapQuestionDefinition.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Get all MapVapQuestionDefinitions with the passed search criteria() that have not been deleted.
        /// </summary>
        /// <returns></returns>
        private Negotiator Search()
        {
            SearchMapVapQuestionDefinitionDto searchMapVapQuestionDefinitionDto = this.Bind<SearchMapVapQuestionDefinitionDto>();

            QueryResult<PagedResultDto<ListMapVapQuestionDefinitionDto>> queryResult =
                _ExecutionPlan.Search<SearchMapVapQuestionDefinitionQuery, MapVapQuestionDefinition,
                        SearchMapVapQuestionDefinitionDto, PagedResultDto<ListMapVapQuestionDefinitionDto>>(
                            searchMapVapQuestionDefinitionDto)
                    .OnSuccess(
                        result =>
                            Mapper.Map<PagedResultDto<ListMapVapQuestionDefinitionDto>>(result.Paginate(_Paginator,
                                searchMapVapQuestionDefinitionDto.CreatePagination())))
                    .Execute();

            LISTPagedResponseDto<ListMapVapQuestionDefinitionDto> response =
                queryResult.CreateLISTPagedResponse(queryResult.Response)
                    .CreateLinks(c => SystemRoutes.MapVapQuestionDefinition.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Get all MapVapQuestionDefinition by productId and ChannelId that have not been deleted.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="channelId"></param>
        /// <returns></returns>
        private Negotiator GetByProductAndChannel(int productId, int channelId)
        {
            var queryResult = _ExecutionPlan
                .Query<GetMapVapQuestionDefinitionQuery, MapVapQuestionDefinition, ListResultDto<ListMapVapQuestionDefinitionDto>, ListMapVapQuestionDefinitionDto>()
                .Configure(q => q.WithCriteria(productId, channelId))
                .OnSuccess(r => Mapper.Map<List<MapVapQuestionDefinition>, ListResultDto<ListMapVapQuestionDefinitionDto>>(r.ToList()))
                .Execute();


            LISTResponseDto<ListMapVapQuestionDefinitionDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.MapVapQuestionDefinition.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }

        /// <summary>
        /// Get all MapVapQuestionDefinition by productId and ChannelId that have not been deleted.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="channelId"></param>
        /// <param name="enabled"></param>
        /// <returns></returns>
        private Negotiator GetEnabledByProductAndChannel(int productId, int channelId, bool enabled)
        {
            var queryResult = _ExecutionPlan
                .Query<GetMapVapQuestionDefinitionQuery, MapVapQuestionDefinition, ListResultDto<ListMapVapQuestionDefinitionDto>, ListMapVapQuestionDefinitionDto>()
                .Configure(q => q.WithCriteria(productId, channelId, enabled))
                .OnSuccess(r => Mapper.Map<List<MapVapQuestionDefinition>, ListResultDto<ListMapVapQuestionDefinitionDto>>(r.ToList()))
                .Execute();


            LISTResponseDto<ListMapVapQuestionDefinitionDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.MapVapQuestionDefinition.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }

        #endregion
    }
}