﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Notifications;
using Domain.Notifications.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Notifications;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Notifications
{
    public class NotificationModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;

        public NotificationModule(IExecutionPlan executionPlan)
        {
            m_ExecutionPlan = executionPlan;

            Options[SystemRoutes.Notification.Options.Route] = p => GetOptions();
            Post[SystemRoutes.Notification.Post.Route] = p => CreateNotification();
            Get[SystemRoutes.Notification.GetBySenderId.Route] = p => GetNotificationsBySenderId(p.senderId);
            Get[SystemRoutes.Notification.GetUnreadByRecipientId.Route] = p => GetUnreadNotificationsByRecipientId(p.recipientId);
            Get[SystemRoutes.Notification.GetAllByRecipientId.Route] = p => GetAllNotificationsByRecipientId(p.recipientId);
            Put[SystemRoutes.Notification.Put.Route] = p => MarkNotificationAsRead(p.notificationUserId);
        }

        private Negotiator GetOptions()
        {
            return Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Notification));
        }


        private Negotiator CreateNotification()
        {
            var model = this.Bind<CreateNotificationDto>();
            var result = m_ExecutionPlan.Execute<CreateNotificationDto, int>(model);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetNotificationsBySenderId(int senderId)
        {
            var queryResult = m_ExecutionPlan.Query<GetNotificationsQuery, NotificationUser, List<NotificationDto>>()
                .Configure(c => c.BySenderUserId(senderId).GetUnreadReadMessagesOnly(false).OnlyGetWhereDateTimeToNotifyIsValid(false))
                .OnSuccess(results => results.ToList().Select(Mapper.Map<NotificationUser, NotificationDto>).ToList())
                .Execute();

            var result = new ListResultDto<NotificationDto> { Results = queryResult.Response };

            LISTResponseDto<NotificationDto> response = queryResult.CreateLISTResponse(result)
                .CreateLinks(c => SystemRoutes.Notification.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetUnreadNotificationsByRecipientId(int recipientId)
        {
            var queryResult = m_ExecutionPlan.Query<GetNotificationsQuery, NotificationUser, List<NotificationDto>>()
                .Configure(c => c.ByRecipientUserId(recipientId))
                .OnSuccess(results => results.Select(Mapper.Map<NotificationUser, NotificationDto>).ToList())
                .Execute();

            var result = new ListResultDto<NotificationDto> { Results = queryResult.Response };

            LISTResponseDto<NotificationDto> response = queryResult.CreateLISTResponse(result)
                .CreateLinks(c => SystemRoutes.Notification.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator GetAllNotificationsByRecipientId(int recipientId)
        {
            var queryResult = m_ExecutionPlan.Query<GetNotificationsQuery, NotificationUser, List<NotificationDto>>()
                .Configure(c => c.ByRecipientUserId(recipientId).GetUnreadReadMessagesOnly(false).OnlyGetWhereDateTimeToNotifyIsValid(false))
                .OnSuccess(results => results.Select(Mapper.Map<NotificationUser, NotificationDto>).ToList())
                .Execute();

            var result = new ListResultDto<NotificationDto> { Results = queryResult.Response };

            LISTResponseDto<NotificationDto> response = queryResult.CreateLISTResponse(result)
                .CreateLinks(c => SystemRoutes.Notification.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator MarkNotificationAsRead(int notificationUserId)
        {
            var editNotificationUserDto = new EditNotificationUserDto(notificationUserId);

            var result = m_ExecutionPlan.Execute<EditNotificationUserDto, bool>(editNotificationUserDto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }
    }
}