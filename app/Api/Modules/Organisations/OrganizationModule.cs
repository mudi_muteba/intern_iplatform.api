﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Organizations;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Organisations;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Organisations
{
    public class OrganizationModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public OrganizationModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.Organisations.GetById.Route] = p => GetOrganization(p.id);
            Put[SystemRoutes.Organisations.Edit.Route] = p => EditOrganization();
        }

        public Negotiator GetOrganization(int id)
        {
            var result = _executionPlan
                .GetById<Organization, OrganisationDto>(() => _repository.GetById<Organization>(id))
                .OnSuccess(Mapper.Map<Organization, OrganisationDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Organisations.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator EditOrganization()
        {
            var editDto = this.Bind<EditOrganizationDto>();

            var result = _executionPlan.Execute<EditOrganizationDto, int>(editDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Organisations.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}