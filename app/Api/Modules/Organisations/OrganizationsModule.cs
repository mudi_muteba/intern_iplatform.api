﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Organizations;
using Domain.Organizations.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Organisations;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;
using System.Linq;

namespace Api.Modules.Organisations
{
    public class OrganizationsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;
        private readonly IPaginateResults _paginator;

        public OrganizationsModule(IExecutionPlan executionPlan, IRepository repository, IPaginateResults paginator)
        {
            _executionPlan = executionPlan;
            _repository = repository;
            _paginator = paginator;

            Get[SystemRoutes.Organisations.GetOrganizations.Route] = _ => GetAllOrganizations(new Pagination());
            Get[SystemRoutes.Organisations.GetWithPagination.Route] = p => GetAllOrganizations(new Pagination(p.PageNumber, p.PageSize));
            Get[SystemRoutes.Organisations.GetAll.Route] = p => GetAllOrganizationsNoPagination();

            Post[SystemRoutes.Organisations.Post.Route] = _ => CreateOrganization();
        }

        private Negotiator GetAllOrganizations(Pagination pagination)
        {
            var queryResult = _executionPlan
                .Query<GetAllOrganizationsQuery, Organization, PagedResultDto<ListOrganizationDto>, ListOrganizationDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListOrganizationDto>>(result.Paginate(_paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Organisations.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator GetAllOrganizationsNoPagination()
        {
            var queryResult = _executionPlan
                .Query<GetAllOrganizationsQuery, Organization, ListResultDto<ListOrganizationDto>, ListOrganizationDto>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListOrganizationDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Organisations.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateOrganization()
        {
            var createOrganizationDto = this.Bind<CreateOrganizationDto>();

            var result = _executionPlan.Execute<CreateOrganizationDto, int>(createOrganizationDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Organisations.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}