﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Sales;
using Domain.Sales.Queries;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Sales;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Workflow.Messages;

namespace Api.Modules.Commercials
{
    public class SalesModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IWorkflowRouter _router;

        public SalesModule(IExecutionPlan executionPlan, IWorkflowRouter router)
        {
            _executionPlan = executionPlan;
            _router = router;

            Post[SystemRoutes.Sales.CreateSalesFNI.Route] = p => CreateSalesFni();
            Post[SystemRoutes.Sales.SaveSalesStructure.Route] = p => CreateSaleStructure();
            Post[SystemRoutes.Sales.SaveSalesDetails.Route] = p => CreateSalesDetails();
            Get[SystemRoutes.Sales.GetSalesDetailByLeadId.Route] = p => GetSalesDetailByLeadId(p.leadId);
        }

        public Negotiator CreateSalesFni()
        {
            var createSalesFniDto = this.Bind<CreateSalesFNIDto>();

            var result = _executionPlan.Execute<CreateSalesFNIDto, int>(createSalesFniDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.Sales.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


        public Negotiator CreateSaleStructure()
        {
            var dto = this.Bind<CreateSalesStructuresDto>();

            var result = _executionPlan.Execute<CreateSalesStructuresDto, int>(dto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.Sales.CreateGetBySalesStructureId(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator CreateSalesDetails()
        {
            var dto = this.Bind<CreateSalesDetailsDto>();

            var result = _executionPlan.Execute<CreateSalesDetailsDto, bool>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetSalesDetailByLeadId(int leadId)
        {
            QueryResult<SalesDetailDto> queryResult = _executionPlan
                .Query<GetSalesDetailByLeadIdQuery, SalesDetails, SalesDetailDto, SalesDetailDto>()
                .Configure(c => c.WithCriteria(leadId))
                .OnSuccess(r => Mapper.Map<SalesDetails, SalesDetailDto>(r.FirstOrDefault()))
                .Execute();

            GETResponseDto<SalesDetailDto> response = queryResult.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Sales.CreateGetSalesDetailByLeadId(c.Lead.Id));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}