﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Sales;
using Domain.Sales.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Sales;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Sales
{
    public class SalesTagModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public SalesTagModule(IExecutionPlan executionPlan, IPaginateResults paginator)
        {
            _executionPlan = executionPlan;
            _paginator = paginator;

            Post[SystemRoutes.SalesTag.Post.Route] = p => CreateSalesTag();
            Post[SystemRoutes.SalesTag.PostSearch.Route] = p => SearchSalesTag();
        }

        public Negotiator CreateSalesTag()
        {
            var createSalesTagDto = this.Bind<CreateSalesTagDto>();

            var result = _executionPlan.Execute<CreateSalesTagDto, int>(createSalesTagDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.Sales.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchSalesTag()
        {
            var criteria = this.Bind<SalesTagSearchDto>();

            var queryResult = _executionPlan
                .Search<SearchSalesTagQuery, SalesTag, SalesTagSearchDto, PagedResultDto<SalesTagDto>>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<SalesTagDto>>(
                    result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SalesTag.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}