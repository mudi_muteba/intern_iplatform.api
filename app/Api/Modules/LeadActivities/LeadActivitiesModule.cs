﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Activities;
using Domain.Activities.Builder;
using Domain.Activities.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Performance;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.LeadActivities
{
    public class LeadActivitiesModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private IRepository repository;

        public LeadActivitiesModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan,IRepository repository, SearchLeadActivitiesQuery searchLeadActivitiesQuery)
        {
            this.repository = repository;
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.searchLeadActivitiesQuery = searchLeadActivitiesQuery;

            Options[SystemRoutes.LeadActivities.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.LeadActivities));

            Post[SystemRoutes.LeadActivities.Get.Route] = p => GetLeadActivities(new Pagination());
            Get[SystemRoutes.LeadActivities.Get.Route] = p => GetLeadActivities(new Pagination());

            Get[SystemRoutes.LeadActivities.GetWithPagination.Route] =
                p => GetLeadActivities(new Pagination(p.PageNumber, p.PageSize));

            Post[SystemRoutes.LeadActivities.PostSearch.Route] = p => SearchLeadActivities();
            Post[SystemRoutes.LeadActivities.PostSearchNoPagination.Route] = p => SearchLeadActivitiesNoPagination();
            Post[SystemRoutes.LeadActivities.GetCount.Route] = p => GetLeadActivitiesCount();
        }

        private  SearchLeadActivitiesQuery searchLeadActivitiesQuery { get; set; }
        private Negotiator SearchLeadActivities()
        {
            var criteria = this.Bind<LeadActivitySearchDto>();

            PerformanceCounters.Instance.WithRepository(repository).Create("SearchLeadActivities");
            PerformanceCounters.Instance.StartCounter("SearchLeadActivities");

                var queryResult = executionPlan
                    .Search<SearchLeadActivitiesQuery, LeadActivity, LeadActivitySearchDto, PagedResultDto<LeadActivityDto>, LeadActivityDto>(criteria)
                    .OnSuccess(result => Mapper.Map<PagedResultDto<LeadActivityDto>>(result.Paginate(paginator, criteria.CreatePagination())))
                    .Execute();

                PerformanceCounters.Instance.WithRepository(repository).StopCounter("SearchLeadActivities");

                var response = queryResult.CreateLISTPagedResponse(queryResult.Response).CreateLinks(c => SystemRoutes.LeadActivities.CreateLinks(c));

                return Negotiate.ForSupportedFormats(response, response.StatusCode);
     
        }

        private Negotiator SearchLeadActivitiesNoPagination()
        {
            var criteria = this.Bind<LeadActivitySearchDto>();

            var queryResult = executionPlan
                .Search<SearchLeadActivitiesQuery, LeadActivity, LeadActivitySearchDto, ListResultDto<LeadActivityDto>, LeadActivityDto>(criteria)
                .OnSuccess(result => Mapper.Map<ListResultDto<LeadActivityDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response).CreateLinks(c => SystemRoutes.LeadActivities.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);

        }

        private Negotiator GetLeadActivities(Pagination pagination)
        {
            var criteria = this.Bind<LeadActivitySearchDto>();

            var queryResult = executionPlan
                .Search<SearchLeadActivitiesQuery, LeadActivity, LeadActivitySearchDto, PagedResultDto<LeadActivityDto>, LeadActivityDto>(criteria)
                .OnSuccess(result => new PagedResultDto<LeadActivityDto>()
                {
                    Results = result.Select(la => la.Build()).ToList()
                })
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(Paginate(queryResult.Response, pagination))
                .CreateLinks(c => SystemRoutes.LeadActivities.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetLeadActivitiesCount()
        {
            var criteria = this.Bind<LeadActivitySearchDto>();

            var queryResult = executionPlan
                .Search<SearchLeadActivitiesQuery, LeadActivity, LeadActivitySearchDto, LeadActivityCountDto>(new LeadActivitySearchDto { CampaignId = criteria.CampaignId})
                .OnSuccess(result => new LeadActivityCountDto() { Count = result.Count() })
                .Execute();

            var response = queryResult.CreateGetPostResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private PagedResultDto<TDto> Paginate<TDto>(PagedResultDto<TDto> result,
            Pagination pagination)
        {
            var totalPages = (result.Results.Count() + pagination.PageSize - 1) / pagination.PageSize;

            List<TDto> values = result.Results
                                        .Skip((pagination.PageNumber - 1) * pagination.PageSize)
                                        .Take(pagination.PageSize).ToList();

            var response = new PagedResultDto<TDto>()
            {
                PageNumber = pagination.PageNumber,
                TotalCount = result.Results.Count(),
                Results = values,
                TotalPages = totalPages,
            };

            return response;
        }
    }
}