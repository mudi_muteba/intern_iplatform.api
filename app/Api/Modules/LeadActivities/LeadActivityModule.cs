﻿using System;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy;
using Api.Infrastructure;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Api.Models;
using AutoMapper;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;
using Domain.Campaigns.Queries;
using Domain.Campaigns;
using Domain.Activities.Queries;
using System.Collections.Generic;
using System.Linq;
using Domain.Activities.Builder;
using System.Collections;
using Domain.Activities;


namespace Api.Modules.LeadActivities
{
    public class LeadActivityModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public LeadActivityModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.LeadActivities.GetById.Route] = p => GetLeadActivity(p.id);

            Put[SystemRoutes.LeadActivities.PutDisable.Route] = p => DisableLeadActivity(p.id);
        }

        

        private Negotiator GetLeadActivity(int id)
        {
            var result = executionPlan
                .GetById<LeadActivity, LeadActivityDto>(() => repository.GetById<LeadActivity>(id))
                .OnSuccess(Mapper.Map<LeadActivity, LeadActivityDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.LeadActivities.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableLeadActivity(int id)
        {
            var disableLeadActivityDto = new DisableLeadActivityDto()
            {
                Id = id
            };

            var result = executionPlan.Execute<DisableLeadActivityDto, int>(disableLeadActivityDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Campaigns.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

    }
}