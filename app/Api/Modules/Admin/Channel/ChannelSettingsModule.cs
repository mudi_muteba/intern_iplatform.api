﻿using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Admin.ChannelSettings;
using Domain.Admin.ChannelSettings.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelSettings;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.Channel
{
    public class ChannelSettingsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public ChannelSettingsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Post[SystemRoutes.ChannelSettings.Post.Route] = p => CreateChannelSetting();
            Get[SystemRoutes.ChannelSettings.GetByChannelId.Route] = p => GetChannelSettingsById(p.ChannelId);
            Put[SystemRoutes.ChannelSettings.Put.Route] = p => UpdateChannelSetting(p.id);
            Post[SystemRoutes.ChannelSettings.Search.Route] = _ => Search();
            Post[SystemRoutes.ChannelSettings.SearchNoPagination.Route] = _ => SearchNoPagination();
            Put[SystemRoutes.ChannelSettings.Delete.Route] = p => DeleteChannelSettings(p.id);
        }

        private Negotiator DeleteChannelSettings(int id)
        {
            var deleteSettingsQuoteUploadsDto = this.Bind<DeleteChannelSettingDto>();

            var result = _executionPlan.Execute<DeleteChannelSettingDto, int>(deleteSettingsQuoteUploadsDto);

            var response = result.CreatePUTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchNoPagination()
        {
            var criteria = this.Bind<SearchChannelSettingDto>();

            var queryResult = _executionPlan
                .Search<SearchChannelSettingsQuery, ChannelSetting, SearchChannelSettingDto, ListResultDto<ChannelSettingDto>>(criteria)
                .OnSuccess(result => Mapper.Map<ListResultDto<ChannelSettingDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.ChannelSettings.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<SearchChannelSettingDto>();

            var queryResult = _executionPlan
                .Search<SearchChannelSettingsQuery, ChannelSetting, SearchChannelSettingDto, PagedResultDto<ListChannelSettingDto>, ListChannelSettingDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListChannelSettingDto>>(result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response).CreateLinks(c => SystemRoutes.ChannelSettings.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateChannelSetting()
        {
            var createChannelSettingDto = this.Bind<CreateChannelSettingDto>();

            var result = _executionPlan.Execute<CreateChannelSettingDto, int>(createChannelSettingDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ChannelSettings.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetChannelSettingsById(int channelId)
        {
            var criteria = new ChannelSettingSearchDto { ChannelId = channelId};
            var queryResult = _executionPlan
                .Search<GetChannelSettingsByChannelIdQuery, ChannelSetting, ChannelSettingSearchDto, ListResultDto<ListChannelSettingDto>, ListChannelSettingDto>(criteria)
                .Configure(query => query.WithCriteria(criteria))
                .OnSuccess(result => Mapper.Map<ListResultDto<ListChannelSettingDto>>(result.ToList()))
                .Execute();

            var x = queryResult.Response;

            var response = queryResult.CreateGETResponse(); 

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateChannelSetting(int id)
        {
            var updateChannelSettingDto = this.Bind<UpdateChannelSettingDto>();
            updateChannelSettingDto.Id = id;

            var result = _executionPlan.Execute<UpdateChannelSettingDto, int>(updateChannelSettingDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ChannelSettings.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}