﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.Base;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace Api.Modules.Admin.Channel
{
    public class ChannelModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public ChannelModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.Channels.GetById.Route] = p => GetChannel(p.id);
            Put[SystemRoutes.Channels.Put.Route] = p => EditChannel(p.id);
            Post[SystemRoutes.Channels.PublishChannelNotification.Route] = p => PublishChannelNotification();
            Post[SystemRoutes.Channels.PublishAgencyApplicationNotification.Route] = p => PublishAgencyApplicationNotification();

        }

        private Negotiator GetChannel(int id)
        {
            var result = _executionPlan
                .GetById<Domain.Admin.Channel, ListChannelDto>(() => _repository.GetById<Domain.Admin.Channel>(id))
                .OnSuccess(Mapper.Map<Domain.Admin.Channel, ListChannelDto>)
                .Execute();

            var response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.Channels.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator EditChannel(int id)
        {
            var editChannelDto = this.Bind<EditChannelDto>();

            var result = _executionPlan.Execute<EditChannelDto, int>(editChannelDto);

            var response = result.CreatePUTResponse().CreateLinks(SystemRoutes.Channels.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator PublishChannelNotification()
        {
            var criteria = this.Bind<ChannelPublishingNotificationDto>();
            
            var result = _executionPlan.Execute<ChannelPublishingNotificationDto, ReportEmailResponseDto>(criteria);
            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator PublishAgencyApplicationNotification()
        {
            var criteria = this.Bind<PublishingAgencyApplicationNotificationDto>();

            var result = _executionPlan.Execute<PublishingAgencyApplicationNotificationDto, ReportEmailResponseDto>(criteria);
            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}