﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Linq;

namespace Api.Modules.Admin.Channel
{
    public class ChannelsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public ChannelsModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Options[SystemRoutes.Channels.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Channels));

            Get[SystemRoutes.Channels.Get.Route] = _ => GetAllChannels(new Pagination());
            Get[SystemRoutes.Channels.GetNoPagination.Route] = _ => GetAllNoPagination();
            Get[SystemRoutes.Channels.GetAllWithNoVehicleGuideSetting.Route] = _ => GetAllWithNoVehicleGuideSetting();
            Get[SystemRoutes.Channels.GetWithPagination.Route] = p => GetAllChannels(new Pagination(p.PageNumber, p.PageSize));
            Post[SystemRoutes.Channels.Search.Route] = _ => Search();
            Post[SystemRoutes.Channels.Post.Route] = p => CreateChannel();
        }


        private Negotiator Search()
        {
            var criteria = this.Bind<SearchChannelsDto>();

            var queryResult = _executionPlan
                .Search<SearchChannelsQuery, Domain.Admin.Channel , SearchChannelsDto, PagedResultDto<ListChannelDto>, ListChannelDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListChannelDto>>(
                    result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Channels.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /*Gets all channels by user id*/
        private Negotiator GetAllChannels(Pagination pagination)
        {
            var queryResult = _executionPlan
                .Query<GetChannelsByUserIdQuery, Domain.Admin.Channel, PagedResultDto<ListChannelDto>, ListChannelDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListChannelDto>>(result.Paginate(_paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Channels.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = _executionPlan
                .Query<GetAllChannelsQuery, Domain.Admin.Channel, ListResultDto<ListChannelDto>, ListChannelDto>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListChannelDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Channels.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateChannel()
        {
            var createChannelDto = this.Bind<CreateChannelDto>();

            var result = _executionPlan.Execute<CreateChannelDto, int>(createChannelDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Channels.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllWithNoVehicleGuideSetting()
        {
            var queryResult = _executionPlan
                .Query<GetAllChannelsWithNoVehicleGuideSettingQuery, Domain.Admin.Channel, ListResultDto<ListChannelDto>, ListChannelDto>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListChannelDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.Channels.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}