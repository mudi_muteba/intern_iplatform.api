﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Admin.SettingsQuoteUploads;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.ChannelPermission
{
    public class SettingsQuoteUploadModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public SettingsQuoteUploadModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.SettingsQuoteUploads.GetById.Route] = p => GetSettingsQuoteUpload(p.id);
            Put[SystemRoutes.SettingsQuoteUploads.Put.Route] = p => EditSettingsQuoteUpload(p.id);
            Put[SystemRoutes.SettingsQuoteUploads.Delete.Route] = p => DeleteSettingsQuoteUpload(p.id);
        }

        private Negotiator GetSettingsQuoteUpload(int id)
        {
            var result = executionPlan
                .GetById<SettingsQuoteUpload, SettingsQuoteUploadDto>(() => repository.GetById<SettingsQuoteUpload>(id))
                .OnSuccess(Mapper.Map<SettingsQuoteUpload, SettingsQuoteUploadDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.SettingsQuoteUploads.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator EditSettingsQuoteUpload(int id)
        {
            var editSettingsQuoteUploadDto = this.Bind<EditSettingsQuoteUploadDto>();

            var result = executionPlan.Execute<EditSettingsQuoteUploadDto, int>(editSettingsQuoteUploadDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Permissions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteSettingsQuoteUpload(int id)
        {
            var deleteSettingsQuoteUploadsDto = this.Bind<DeleteSettingsQuoteUploadDto>();

            var result = executionPlan.Execute<DeleteSettingsQuoteUploadDto, int>(deleteSettingsQuoteUploadsDto);

            var response = result.CreatePUTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}