﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Domain.Admin.SettingsQuoteUploads.Queries;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using Domain.Admin.SettingsQuoteUploads;

namespace Api.Modules.Admin.ChannelPermission
{
    public class SettingsQuoteUploadsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public SettingsQuoteUploadsModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.SettingsQuoteUploads.GetByChannelId.Route] = p => GetSettingsQuoteUploads(p.channelId);
            Post[SystemRoutes.SettingsQuoteUploads.Post.Route] = p => CreateSettingsQuoteUpload();
            Post[SystemRoutes.SettingsQuoteUploads.SaveMultiple.Route] = p => SaveMultipleSettingsQuoteUpload();
            Post[SystemRoutes.SettingsQuoteUploads.Search.Route] = _ => Search();
            Post[SystemRoutes.SettingsQuoteUploads.SearchNoPagination.Route] = _ => SearchNoPagination();
            Options[SystemRoutes.SettingsQuoteUploads.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.SettingsQuoteUploads));

            Get[SystemRoutes.SettingsiPerson.GetNoPagination.Route] = p => GetAllNoPagination();
        }

        private Negotiator GetSettingsQuoteUploads(int channelId)
        {
            var result = executionPlan
                .Query<GetSettingsQuoteUploadsQuery, SettingsQuoteUpload, ListResultDto<SettingsQuoteUploadDto>>()
                .Configure(q => q.WithChannelId(channelId))
                .OnSuccess(r => Mapper.Map<List<SettingsQuoteUpload>, ListResultDto<SettingsQuoteUploadDto>>(r.ToList()))
                .Execute();

            LISTResponseDto<SettingsQuoteUploadDto> response = result.CreateLISTResponse(result.Response)
                .CreateLinks(c => SystemRoutes.SettingsQuoteUploads.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SaveMultipleSettingsQuoteUpload()
        {
            var saveSettingsQuoteUploadsDto = this.Bind<SaveSettingsQuoteUploadsDto>();

            var result = executionPlan.Execute<SaveSettingsQuoteUploadsDto, bool>(saveSettingsQuoteUploadsDto);

            var response = result.CreatePOSTResponse();

            return Negotiate
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateSettingsQuoteUpload()
        {
            var createSettingsQuoteUploadDto = this.Bind<CreateSettingsQuoteUploadDto>();

            var result = executionPlan.Execute<CreateSettingsQuoteUploadDto, int>(createSettingsQuoteUploadDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Permissions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator Search()
        {
            var criteria = this.Bind<SearchSettingsQuoteUploadDto>();

            var queryResult = executionPlan
                .Search<SearchSettingsQuoteUploadQuery, SettingsQuoteUpload, SearchSettingsQuoteUploadDto, PagedResultDto<SettingsQuoteUploadDto>, SettingsQuoteUploadDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<SettingsQuoteUploadDto>>(
                    result.Paginate(paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsQuoteUploads.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SearchNoPagination()
        {
            var criteria = this.Bind<SearchSettingsQuoteUploadDto>();

            var queryResult = executionPlan
                .Search<SearchSettingsQuoteUploadQuery, SettingsQuoteUpload, SearchSettingsQuoteUploadDto, ListResultDto<SettingsQuoteUploadDto>>(criteria)
                .OnSuccess(result => Mapper.Map<ListResultDto<SettingsQuoteUploadDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsQuoteUploads.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = executionPlan
                .Query<GetSettingsQuoteUploadsQuery, SettingsQuoteUpload, ListResultDto<SettingsQuoteUploadDto>>()
                .OnSuccess(result => Mapper.Map<ListResultDto<SettingsQuoteUploadDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsQuoteUploads.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}