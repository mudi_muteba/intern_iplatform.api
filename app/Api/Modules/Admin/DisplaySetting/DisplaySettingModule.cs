﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.DisplaySetting
{
    public class DisplaySettingModule : SecureModule
    {
        private readonly IExecutionPlan _ExecutionPlan;
        private readonly IRepository _Repository;

        public DisplaySettingModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _ExecutionPlan = executionPlan;
            _Repository = repository;

            Get[SystemRoutes.DisplaySettings.GetById.Route] = p => GetById(p.Id);
            Put[SystemRoutes.DisplaySettings.Put.Route] = p => Update(p.Id);
            Put[SystemRoutes.DisplaySettings.PutDelete.Route] = p => DeleteDisplaySetting(p.Id);
        }

        #region [ Negotiators ]

        /// <summary>
        /// Get a DisplaySetting by it's unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Negotiator GetById(int id)
        {
            GetByIdResult<DisplaySettingDto> result = _ExecutionPlan.GetById<Domain.Admin.DisplaySetting.DisplaySetting, DisplaySettingDto>(
                    () => _Repository.GetById<Domain.Admin.DisplaySetting.DisplaySetting>(id))
                    .OnSuccess(Mapper.Map<Domain.Admin.DisplaySetting.DisplaySetting, DisplaySettingDto>)
                    .Execute();

            GETResponseDto<DisplaySettingDto> response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.DisplaySettings.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Updates a DisplaySetting by it's unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Negotiator Update(int id)
        {
            EditDisplaySettingDto editDisplaySettingDto = this.Bind<EditDisplaySettingDto>();
            editDisplaySettingDto.Id = id;

            HandlerResult<int> result = _ExecutionPlan.Execute<EditDisplaySettingDto, int>(editDisplaySettingDto);
            PUTResponseDto<int> response = result.CreatePUTResponse().CreateLinks(SystemRoutes.DisplaySettings.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Deletes a DisplaySetting by it's unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Negotiator DeleteDisplaySetting(int id)
        {
            DeleteDisplaySettingDto deleteDisplaySettingDto = this.Bind<DeleteDisplaySettingDto>();
            deleteDisplaySettingDto.Id = id;

            HandlerResult<int> result = _ExecutionPlan.Execute<DeleteDisplaySettingDto, int>(deleteDisplaySettingDto);
            PUTResponseDto<int> response = result.CreatePUTResponse().CreateLinks(SystemRoutes.DisplaySettings.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        #endregion
    }
}