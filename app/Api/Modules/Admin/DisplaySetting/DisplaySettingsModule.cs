﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Infrastructure;
using AutoMapper;
using Domain.Admin.DisplaySetting.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.DisplaySetting
{
    public class DisplaySettingsModule : SecureModule
    {
        private readonly IPaginateResults _Paginator;
        private readonly IExecutionPlan _ExecutionPlan;

        public DisplaySettingsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _Paginator = paginator;
            _ExecutionPlan = executionPlan;

            Post[SystemRoutes.DisplaySettings.Post.Route] = _ => Create();
            Get[SystemRoutes.DisplaySettings.Get.Route] = _ => GetAll();
            Get[SystemRoutes.DisplaySettings.GetWithPagination.Route] = p => GetAllWithPagination(new Pagination(p.pageNumber, p.pageSize));
            Post[SystemRoutes.DisplaySettings.Search.Route] = _ => Search();
            Post[SystemRoutes.DisplaySettings.GetRequiresThirdPartyIntegration.Route] = _ => GetRequiresThirdPartyIntegration();
        }

        #region [ Negotiators ]

        /// <summary>
        /// Create a new DisplaySetting.
        /// </summary>
        /// <returns></returns>
        public Negotiator Create()
        {
            CreateDisplaySettingDto createDisplaySettingDto = this.Bind<CreateDisplaySettingDto>();
            HandlerResult<int> result = _ExecutionPlan.Execute<CreateDisplaySettingDto, int>(createDisplaySettingDto);
            POSTResponseDto<int> response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.DisplaySettings.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Get all DisplaySettings that have not been deleted.
        /// </summary>
        /// <returns></returns>
        public Negotiator GetAll()
        {
            QueryResult<ListResultDto<ListDisplaySettingDto>> queryResult = _ExecutionPlan
                    .Query<GetAllDisplaySettingsQuery, Domain.Admin.DisplaySetting.DisplaySetting,
                        ListResultDto<ListDisplaySettingDto>, ListDisplaySettingDto>()
                    .OnSuccess(result => Mapper.Map<ListResultDto<ListDisplaySettingDto>>(result.ToList()))
                    .Execute();

            LISTResponseDto<ListDisplaySettingDto> response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.DisplaySettings.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Get all DisplaySettings with pagination that have not been deleted.
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public Negotiator GetAllWithPagination(Pagination pagination)
        {
            QueryResult<PagedResultDto<ListDisplaySettingDto>> queryResult = _ExecutionPlan
                    .Query<GetAllDisplaySettingsQuery, Domain.Admin.DisplaySetting.DisplaySetting,
                        PagedResultDto<ListDisplaySettingDto>, ListDisplaySettingDto>()
                    .OnSuccess(result => Mapper.Map<PagedResultDto<ListDisplaySettingDto>>(result.Paginate(_Paginator, pagination)))
                    .Execute();

            LISTPagedResponseDto<ListDisplaySettingDto> response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.DisplaySettings.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Get all DisplaySettings with the passed search criteria(ChannelName, ProductName) that have not been deleted.
        /// </summary>
        /// <returns></returns>
        public Negotiator Search()
        {
            SearchDisplaySettingDto searchDisplaySettingDto = this.Bind<SearchDisplaySettingDto>();

            QueryResult<PagedResultDto<ListDisplaySettingDto>> queryResult = _ExecutionPlan
                    .Search<SearchDisplaySettingQuery, Domain.Admin.DisplaySetting.DisplaySetting, SearchDisplaySettingDto,
                        PagedResultDto<ListDisplaySettingDto>>(searchDisplaySettingDto)
                    .OnSuccess(result => Mapper.Map<PagedResultDto<ListDisplaySettingDto>>(result.Paginate(_Paginator, searchDisplaySettingDto.CreatePagination())))
                    .Execute();

            LISTPagedResponseDto<ListDisplaySettingDto> response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                    .CreateLinks(c => SystemRoutes.DisplaySettings.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Gets all DisplaySettings by PartyId, ChannelId and ThirdPartyDispolay is true
        /// </summary>
        /// <returns></returns>
        public Negotiator GetRequiresThirdPartyIntegration()
        {
            IndividualDto individualDto = this.Bind<IndividualDto>();

            QueryResult<ListResultDto<ListDisplaySettingDto>> queryResult = _ExecutionPlan
                    .Query<RequiresThirdPartyQuery, Domain.Admin.DisplaySetting.DisplaySetting,
                        ListResultDto<ListDisplaySettingDto>, ListDisplaySettingDto>()
                        .Configure(c => c.WithPartyIdAndChannelId(individualDto.PartyId, individualDto.ChannelId))
                    .OnSuccess(result => Mapper.Map<ListResultDto<ListDisplaySettingDto>>(result.ToList()))
                    .Execute();

            LISTResponseDto<ListDisplaySettingDto> response =
                queryResult.CreateLISTResponse(queryResult.Response)
                    .CreateLinks(c => SystemRoutes.DisplaySettings.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        #endregion
    }
}