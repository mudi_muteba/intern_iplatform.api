﻿using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin.ChannelTemplates;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

namespace Api.Modules.Admin.ChannelTemplates
{
    public class ChannelTemplatesModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public ChannelTemplatesModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Post[SystemRoutes.ChannelTemplates.Post.Route] = x => CreateMultipleChannelTemplates();
        }

        private Negotiator CreateMultipleChannelTemplates()
        {
            var createMultipleChannelTemplatesDto = this.Bind<CreateMultipleChannelTemplatesDto>();

            var result = _executionPlan.Execute<CreateMultipleChannelTemplatesDto, int>(createMultipleChannelTemplatesDto);

            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}