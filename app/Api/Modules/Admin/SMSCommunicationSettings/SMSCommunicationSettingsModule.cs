﻿using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin.SMSCommunicationSettings;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.SMSCommunicationSettings
{
    public class SMSCommunicationSettingsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public SMSCommunicationSettingsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            this._paginator = paginator;
            this._executionPlan = executionPlan;

            Post[SystemRoutes.SMSCommunicationSettings.Post.Route] = x => CreateMultipleSMSCommunicationSettings();
        }

        private Negotiator CreateMultipleSMSCommunicationSettings()
        {
            var createMultipleSMSCommunicationSettingsDto = this.Bind<CreateMultipleSMSCommunicationSettingsDto>();

            var result = _executionPlan.Execute<CreateMultipleSMSCommunicationSettingsDto, int>(createMultipleSMSCommunicationSettingsDto);

            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}