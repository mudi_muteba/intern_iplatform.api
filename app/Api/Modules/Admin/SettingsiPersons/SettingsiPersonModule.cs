﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Admin.SettingsiPersons;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.SettingsiPersons
{
    public class SettingsiPersonModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public SettingsiPersonModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.SettingsiPerson.GetById.Route] = p => GetSettingsiPerson(p.id);
            Put[SystemRoutes.SettingsiPerson.Put.Route] = p => UpdateSettingsiPerson(p.id);
            Put[SystemRoutes.SettingsiPerson.PutDelete.Route] = p => DeleteSettingsiPerson(p.id);

        }

        private Negotiator GetSettingsiPerson(int id)
        {
            var result = _executionPlan
                .GetById<SettingsiPerson, SettingsiPersonDto>(() => _repository.GetById<SettingsiPerson>(id))
                .OnSuccess(Mapper.Map<SettingsiPerson, SettingsiPersonDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.SettingsiPerson.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateSettingsiPerson(int id)
        {
            var editSettingsiPersonDto = this.Bind<EditSettingsiPersonDto>();
            editSettingsiPersonDto.Id = id;

            var result = _executionPlan.Execute<EditSettingsiPersonDto, int>(editSettingsiPersonDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.SettingsiPerson.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteSettingsiPerson(int id)
        {
            var dto = this.Bind<DeleteSettingsiPersonDto>();

            var result = _executionPlan.Execute<DeleteSettingsiPersonDto, int>(dto);

            var response = result.CreatePUTResponse();

            return Negotiate
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}