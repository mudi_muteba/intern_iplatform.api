﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Admin.SettingsiPersons.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using Domain.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.Responses.Negotiation;
using System.Linq;
using Nancy.ModelBinding;

namespace Api.Modules.SettingsiPersons
{
    public class SettingsiPersonsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;

        public SettingsiPersonsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;

            Post[SystemRoutes.SettingsiPerson.Search.Route] = _ => Search();
            Get[SystemRoutes.SettingsiPerson.Get.Route] = _ => GetAllSettingsiPersons(new Pagination());
            Get[SystemRoutes.SettingsiPerson.GetWithPagination.Route] = p => GetAllSettingsiPersons(new Pagination(p.pageNumber, p.pageSize));
            Get[SystemRoutes.SettingsiPerson.GetNoPagination.Route] = p => GetAllNoPagination();
            Post[SystemRoutes.SettingsiPerson.Post.Route] = p => Create();
            Post[SystemRoutes.SettingsiPerson.SaveMultiple.Route] = p => SaveMultiple();
        }

        private Negotiator Search()
        {
            var criteria = this.Bind<SearchSettingsiPersonDto>();

            var queryResult = executionPlan
                .Search<SearchSettingsiPersonsQuery, SettingsiPerson, SearchSettingsiPersonDto, PagedResultDto<ListSettingsiPersonDto>, ListSettingsiPersonDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListSettingsiPersonDto>>(
                    result.Paginate(paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsiPerson.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllSettingsiPersons(Pagination pagination)
        {
            var queryResult = executionPlan
                .Query<GetAllSettingsiPersonsQuery, SettingsiPerson, PagedResultDto<ListSettingsiPersonDto>>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListSettingsiPersonDto>>(result.Paginate(paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsiPerson.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = executionPlan
                .Query<GetAllSettingsiPersonsQuery, SettingsiPerson, ListResultDto<ListSettingsiPersonDto>>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListSettingsiPersonDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsiPerson.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Create()
        {
            var dto = this.Bind<CreateSettingsiPersonDto>();

            var result = executionPlan.Execute<CreateSettingsiPersonDto, int>(dto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.SettingsiPerson.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator SaveMultiple()
        {
            var dto = this.Bind<SaveMultipleSettingsiPersonDto>();

            var result = executionPlan.Execute<SaveMultipleSettingsiPersonDto, bool>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}