﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.ChannelPermission
{
    public class ChannelPermissionModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ChannelPermissionModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.Permissions.GetById.Route] = p => GetPermission(p.id);
            Put[SystemRoutes.Permissions.Put.Route] = p => EditChannelPermission(p.id);
            Put[SystemRoutes.Permissions.Disable.Route] = p => DisableChannelPermission(p.id);
        }

        private Negotiator GetPermission(int id)
        {
            var result = executionPlan
                .GetById<Domain.Admin.ChannelPermissions.ChannelPermission, ChannelPermissionDto>(() => repository.GetById<Domain.Admin.ChannelPermissions.ChannelPermission>(id))
                .OnSuccess(Mapper.Map<Domain.Admin.ChannelPermissions.ChannelPermission, ChannelPermissionDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.Permissions.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator EditChannelPermission(int id)
        {
            var editChannelPermissionDto = this.Bind<EditChannelPermissionDto>();

            var result = executionPlan.Execute<EditChannelPermissionDto, int>(editChannelPermissionDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Permissions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DisableChannelPermission(int id)
        {
            var disableChannelPermissionDto = this.Bind<DisableChannelPermissionDto>();

            var result = executionPlan.Execute<DisableChannelPermissionDto, int>(disableChannelPermissionDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.Permissions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

    }
}