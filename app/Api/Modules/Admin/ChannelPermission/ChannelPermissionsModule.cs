﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Admin.ChannelPermissions.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.ChannelPermission
{
    public class ChannelPermissionsModule : SecureModule
    {
        private readonly IExecutionPlan executionPlan;
        private readonly IPaginateResults paginator;
        private readonly IRepository repository;

        public ChannelPermissionsModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan, IRepository repository)
        {
            this.paginator = paginator;
            this.executionPlan = executionPlan;
            this.repository = repository;

            Get[SystemRoutes.Permissions.GetByChannelId.Route] = p => GetPermissions(p.channelId);
            Post[SystemRoutes.Permissions.Post.Route] = p => CreateChannelPermission();
            Post[SystemRoutes.Permissions.PostSearch.Route] = p => SearchPermissions();

            Options[SystemRoutes.Permissions.Options.Route] =
                _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.Permissions));
        }

        private Negotiator GetPermissions(string channelId)
        {
            var result = executionPlan
                .Query<GetChannelPermissionsQuery, Domain.Admin.ChannelPermissions.ChannelPermission, ListResultDto<ChannelPermissionDto>, ChannelPermissionDto>()
                .Configure(q => q.WithChannelId(Guid.Parse(channelId)))
                .OnSuccess(r => Mapper.Map<List<Domain.Admin.ChannelPermissions.ChannelPermission>, ListResultDto<ChannelPermissionDto>>(r.ToList()))
                .Execute();

            LISTResponseDto<ChannelPermissionDto> response = result.CreateLISTResponse(result.Response)
                .CreateLinks(c => SystemRoutes.Permissions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }


        private Negotiator CreateChannelPermission()
        {
            var createChannelPermissionDto = this.Bind<CreateChannelPermissionDto>();

            var result = executionPlan.Execute<CreateChannelPermissionDto, int>(createChannelPermissionDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.Permissions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator SearchPermissions()
        {
            var criteria = this.Bind<ChannelPermissionSearchDto>();

            var result = executionPlan
                .Search<SearchChannelPermissionsQuery, Domain.Admin.ChannelPermissions.ChannelPermission, ChannelPermissionSearchDto, 
                ListResultDto<ChannelPermissionDto>, ChannelPermissionDto>(criteria)
                .OnSuccess(r => Mapper.Map<List<Domain.Admin.ChannelPermissions.ChannelPermission>, ListResultDto<ChannelPermissionDto>>(r.ToList()))
                .Execute();

            var response = result.CreateLISTResponse(result.Response)
                .CreateLinks(c => SystemRoutes.Permissions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}