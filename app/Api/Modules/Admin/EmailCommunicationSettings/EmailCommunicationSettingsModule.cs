﻿using Api.Infrastructure;

using Domain.Base.Execution;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

using Nancy.Responses.Negotiation;

using Nancy.ModelBinding;

using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;

namespace Api.Modules.EmailCommunicationSettings
{
    public class EmailCommunicationSettingsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public EmailCommunicationSettingsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            this._paginator = paginator;
            this._executionPlan = executionPlan;

            Post[SystemRoutes.EmailCommunicationSettings.Post.Route] = x => CreateMultipleEmailCommunicationSettings();
        }

        private Negotiator CreateMultipleEmailCommunicationSettings()
        {
            var createMultipleEmailCommunicationSettingsDto = this.Bind<CreateMultipleEmailCommunicationSettingsDto>();

            var result = _executionPlan.Execute<CreateMultipleEmailCommunicationSettingsDto, int>(createMultipleEmailCommunicationSettingsDto);

            var response = result.CreatePOSTResponse();

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}