﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Admin.ChannelEvents;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Admin.ChannelEvents;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.ChannelEvents
{
    public class ChannelEventModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public ChannelEventModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.ChannelEvent.GetById.Route] = p => GetChannelEvent(p.id);
            Put[SystemRoutes.ChannelEvent.Put.Route] = p => EditChannelEvent(p.id);
            Put[SystemRoutes.ChannelEvent.Delete.Route] = p => DeleteChannelEvent(p.id);
        }

        private Negotiator GetChannelEvent(int id)
        {
            var result = _executionPlan
                .GetById<ChannelEvent, ChannelEventDto>(() => _repository.GetById<ChannelEvent>(id))
                .OnSuccess(Mapper.Map<ChannelEvent, ChannelEventDto>)
                .Execute();

            var response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.ChannelEvent.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator EditChannelEvent(int id)
        {
            var editDto = this.Bind<EditChannelEventDto>();

            var result = _executionPlan.Execute<EditChannelEventDto, int>(editDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ChannelEvent.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteChannelEvent(int id)
        {
            var result = _executionPlan.Execute<DeleteChannelEventDto, int>(new DeleteChannelEventDto(id));

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.ChannelEvent.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}