﻿using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Admin.ChannelEvents;
using Domain.Admin.ChannelEvents.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelEvents;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using System.Linq;

namespace Api.Modules.Admin.ChannelEvents
{
    public class ChannelEventsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public ChannelEventsModule(
            IPaginateResults paginator
            , IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Options[SystemRoutes.ChannelEvent.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.ChannelEvent));

            Get[SystemRoutes.ChannelEvent.Get.Route] = _ => GetAll(new Pagination());
            Get[SystemRoutes.ChannelEvent.GetNoPagination.Route] = _ => GetAllNoPagination();
            Get[SystemRoutes.ChannelEvent.GetWithPagination.Route] = p => GetAll(new Pagination(p.PageNumber, p.PageSize));
            Post[SystemRoutes.ChannelEvent.Search.Route] = _ => Search();
            Post[SystemRoutes.ChannelEvent.SaveMultiple.Route] = p => SaveMultiple();
            Post[SystemRoutes.ChannelEvent.Post.Route] = p => CreateChannelEvent();
        }


        private Negotiator Search()
        {
            var criteria = this.Bind<SearchChannelEventsDto>();

            var queryResult = _executionPlan
                .Search<SearchChannelEventsQuery, ChannelEvent , SearchChannelEventsDto, PagedResultDto<ChannelEventDto>>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ChannelEventDto>>(
                    result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.ChannelEvent.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAll(Pagination pagination)
        {
            var queryResult = _executionPlan
                .Query<GetAllChannelEventsQuery, ChannelEvent, PagedResultDto<ChannelEventDto>>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ChannelEventDto>>(result.Paginate(_paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.ChannelEvent.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = _executionPlan
                .Query<GetAllChannelEventsQuery, ChannelEvent, ListResultDto<ChannelEventDto>>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ChannelEventDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.ChannelEvent.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateChannelEvent()
        {
            var createDto = this.Bind<CreateChannelEventDto>();

            var result = _executionPlan.Execute<CreateChannelEventDto, int>(createDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.ChannelEvent.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator SaveMultiple()
        {
            var dto = this.Bind<SaveChannelEventsDto>();

            var result = _executionPlan.Execute<SaveChannelEventsDto, bool>(dto);

            var response = result.CreatePOSTResponse();

            return Negotiate
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}