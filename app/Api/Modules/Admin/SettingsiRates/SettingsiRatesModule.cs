﻿using System.Linq;
using Api.Infrastructure;
using AutoMapper;
using Domain.Admin.SettingsiRates;
using Domain.Admin.SettingsiRates.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.SettingsiRates
{
    public class SettingsiRatesModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public SettingsiRatesModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            this._paginator = paginator;
            this._executionPlan = executionPlan;

            Get[SystemRoutes.SettingsiRate.Get.Route] = _ => GetAllSettingsiRates(new Pagination());
            Get[SystemRoutes.SettingsiRate.GetWithPagination.Route] = p => GetAllSettingsiRates(new Pagination(p.pageNumber, p.pageSize));
            Post[SystemRoutes.SettingsiRate.Search.Route] = _ => Search();
            Post[SystemRoutes.SettingsiRate.Post.Route] = p => Create();
            Post[SystemRoutes.SettingsiRate.PostMultiple.Route] = p => SaveMultiple();
            Get[SystemRoutes.SettingsiRate.GetNoPagination.Route] = p => GetAllNoPagination();
        }


        public Negotiator SaveMultiple()
        {
            var saveMultipleSettingsiRateDto = this.Bind<SaveMultipleSettingsiRateDto>();

            var result = _executionPlan.Execute<SaveMultipleSettingsiRateDto, bool>(saveMultipleSettingsiRateDto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        public Negotiator Create()
        {
            var createSettingsiRateDto = this.Bind<CreateSettingsiRateDto>();

            var result = _executionPlan.Execute<CreateSettingsiRateDto, int>(createSettingsiRateDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.SettingsiRate.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllSettingsiRates(Pagination pagination)
        {
            var queryResult = _executionPlan
                .Query<GetAllSettingsiRatesQuery, SettingsiRate, PagedResultDto<ListSettingsiRateDto>, ListSettingsiRateDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListSettingsiRateDto>>(result.Paginate(_paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsiRate.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator Search()
        {
            var criteria = this.Bind<SearchSettingsiRateDto>();

            var queryResult = _executionPlan
                .Search<SearchSettingsiRatesQuery, SettingsiRate, SearchSettingsiRateDto, PagedResultDto<ListSettingsiRateDto>, ListSettingsiRateDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListSettingsiRateDto>>(
                    result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsiRate.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = _executionPlan
                .Query<GetAllSettingsiRatesQuery, SettingsiRate, ListResultDto<ListSettingsiRateDto>>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListSettingsiRateDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsiRate.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}