﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Admin.SettingsiRates;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.SettingsiRates
{
    public class SettingsiRateModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public SettingsiRateModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            
            Get[SystemRoutes.SettingsiRate.GetById.Route] = p => GetSettingsiRate(p.id);
            Put[SystemRoutes.SettingsiRate.Put.Route] = p => UpdateSettingsiRate(p.id);
            Put[SystemRoutes.SettingsiRate.PutDelete.Route] = p => DeleteSettingsiRate(p.id);

        }

        

        private Negotiator GetSettingsiRate(int id)
        {
            var result = _executionPlan
                .GetById<SettingsiRate, SettingsiRateDto>(() => _repository.GetById<Domain.Admin.SettingsiRates.SettingsiRate>(id))
                .OnSuccess(Mapper.Map<Domain.Admin.SettingsiRates.SettingsiRate, SettingsiRateDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.SettingsiRate.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateSettingsiRate(int id)
        {
            var editSettingsiRateDto = this.Bind<EditSettingsiRateDto>();
            editSettingsiRateDto.Id = id;

            var result = _executionPlan.Execute<EditSettingsiRateDto, int>(editSettingsiRateDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.SettingsiRate.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteSettingsiRate(int id)
        {
            var disableSettingsiRateDto = this.Bind<DisableSettingsiRateDto>();
            disableSettingsiRateDto.Id = id;

            var result = _executionPlan.Execute<DisableSettingsiRateDto, int>(disableSettingsiRateDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.SettingsiRate.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}