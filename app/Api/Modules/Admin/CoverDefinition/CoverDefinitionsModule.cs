﻿using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Admin.CoverDefinitions.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.CoverDefinition
{
    public class CoverDefinitionsModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IPaginateResults _paginator;

        public CoverDefinitionsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _paginator = paginator;
            _executionPlan = executionPlan;

            Options[SystemRoutes.CoverDefinitions.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.CoverDefinitions));

            Get[SystemRoutes.CoverDefinitions.Get.Route] = _ => GetAll(new Pagination());
            Get[SystemRoutes.CoverDefinitions.GetNoPagination.Route] = _ => GetAllNoPagination();
            Get[SystemRoutes.CoverDefinitions.GetWithPagination.Route] = p => GetAll(new Pagination(p.PageNumber, p.PageSize));
            Post[SystemRoutes.CoverDefinitions.Search.Route] = _ => Search();
            Post[SystemRoutes.CoverDefinitions.Post.Route] = p => CreateCoverDefinition();
        }


        private Negotiator Search()
        {
            var criteria = this.Bind<SearchCoverDefinitionsDto>();

            var queryResult = _executionPlan
                .Search<SearchCoverDefinitionsQuery, Domain.Products.CoverDefinition, SearchCoverDefinitionsDto, PagedResultDto<CoverDefinitionDto>>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<CoverDefinitionDto>>(
                    result.Paginate(_paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.CoverDefinitions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAll(Pagination pagination)
        {
            var queryResult = _executionPlan
                .Query<GetAllCoverDefinitionsQuery, Domain.Products.CoverDefinition, PagedResultDto<CoverDefinitionDto>>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<CoverDefinitionDto>>(result.Paginate(_paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.CoverDefinitions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = _executionPlan
                .Query<GetAllCoverDefinitionsQuery, Domain.Products.CoverDefinition, ListResultDto<CoverDefinitionDto>>()
                .OnSuccess(result => Mapper.Map<ListResultDto<CoverDefinitionDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.CoverDefinitions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator CreateCoverDefinition()
        {
            var createDto = this.Bind<CreateCoverDefinitionDto>();

            var result = _executionPlan.Execute<CreateCoverDefinitionDto, int>(createDto);

            var response = result.CreatePOSTResponse()
                .CreateLinks(SystemRoutes.CoverDefinitions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}