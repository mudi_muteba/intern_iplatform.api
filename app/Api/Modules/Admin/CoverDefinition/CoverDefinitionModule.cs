﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.CoverDefinition
{
    public class CoverDefinitionModule : SecureModule
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly IRepository _repository;

        public CoverDefinitionModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _executionPlan = executionPlan;
            _repository = repository;

            Get[SystemRoutes.CoverDefinitions.GetById.Route] = p => GetCoverDefinition(p.id);
            Put[SystemRoutes.CoverDefinitions.Put.Route] = p => EditCoverDefinition(p.id);
            Put[SystemRoutes.CoverDefinitions.Delete.Route] = p => DeleteCoverDefinition(p.id);
        }

        private Negotiator GetCoverDefinition(int id)
        {
            var result = _executionPlan
                .GetById<Domain.Products.CoverDefinition, CoverDefinitionDto>(() => _repository.GetById<Domain.Products.CoverDefinition>(id))
                .OnSuccess(Mapper.Map<Domain.Products.CoverDefinition, CoverDefinitionDto>)
                .Execute();

            var response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.CoverDefinitions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator EditCoverDefinition(int id)
        {
            var editDto = this.Bind<EditCoverDefinitionDto>();

            var result = _executionPlan.Execute<EditCoverDefinitionDto, int>(editDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.CoverDefinitions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteCoverDefinition(int id)
        {
            var result = _executionPlan.Execute<DeleteCoverDefinitionDto, int>(new DeleteCoverDefinitionDto(id));

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.CoverDefinitions.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}