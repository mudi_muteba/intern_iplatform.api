﻿using System.Linq;
using Api.Infrastructure;
using Api.Models;
using AutoMapper;
using Domain.Admin.SettingsiRateUser.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.SettingsiRateUser
{
    public class SettingsiRateUsersModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IPaginateResults m_Paginator;

        public SettingsiRateUsersModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            this.m_Paginator = paginator;
            this.m_ExecutionPlan = executionPlan;

            Get[SystemRoutes.SettingsiRateUser.Get.Route] = _ => GetAllSettingsiRateUsers(new Pagination());
            Get[SystemRoutes.SettingsiRateUser.GetWithPagination.Route] = p => GetAllSettingsiRateUsers(new Pagination(p.pageNumber, p.pageSize));
            Post[SystemRoutes.SettingsiRateUser.Search.Route] = _ => Search();
            Post[SystemRoutes.SettingsiRateUser.Post.Route] = p => Create();
            Post[SystemRoutes.SettingsiRateUser.PostMultiple.Route] = p => SaveMultiple();
            Get[SystemRoutes.SettingsiRateUser.GetNoPagination.Route] = p => GetAllNoPagination();

            Options[SystemRoutes.SettingsiRateUser.Options.Route] = _ => Negotiate.SiteForSupportedFormats(new SiteMap(Context, SystemRoutes.SettingsiRateUser));
        }


        private Negotiator SaveMultiple()
        {
            var saveMultipleSettingsiRateUserDto = this.Bind<SaveMultipleSettingsiRateUserDto>();

            var result = m_ExecutionPlan.Execute<SaveMultipleSettingsiRateUserDto, bool>(saveMultipleSettingsiRateUserDto);

            var response = result.CreatePOSTResponse();

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator Create()
        {
            var createSettingsiRateUserDto = this.Bind<CreateSettingsiRateUserDto>();

            var result = m_ExecutionPlan.Execute<CreateSettingsiRateUserDto, int>(createSettingsiRateUserDto);

            var response = result.CreatePOSTResponse().CreateLinks(SystemRoutes.SettingsiRateUser.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllSettingsiRateUsers(Pagination pagination)
        {
            var queryResult = m_ExecutionPlan
                .Query<GetAllSettingsiRateUsersQuery, Domain.Admin.SettingsiRateUser.SettingsiRateUser, PagedResultDto<ListSettingsiRateUserDto>, ListSettingsiRateUserDto>()
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListSettingsiRateUserDto>>(result.Paginate(m_Paginator, pagination)))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsiRateUser.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
        private Negotiator Search()
        {
            var criteria = this.Bind<SearchSettingsiRateUserDto>();

            var queryResult = m_ExecutionPlan
                .Search<SearchSettingsiRateUsersQuery, Domain.Admin.SettingsiRateUser.SettingsiRateUser, SearchSettingsiRateUserDto, PagedResultDto<ListSettingsiRateUserDto>, ListSettingsiRateUserDto>(criteria)
                .OnSuccess(result => Mapper.Map<PagedResultDto<ListSettingsiRateUserDto>>(
                    result.Paginate(m_Paginator, criteria.CreatePagination())))
                .Execute();

            var response = queryResult.CreateLISTPagedResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsiRateUser.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator GetAllNoPagination()
        {
            var queryResult = m_ExecutionPlan
                .Query<GetAllSettingsiRateUsersQuery, Domain.Admin.SettingsiRateUser.SettingsiRateUser, ListResultDto<ListSettingsiRateUserDto>>()
                .OnSuccess(result => Mapper.Map<ListResultDto<ListSettingsiRateUserDto>>(result.ToList()))
                .Execute();

            var response = queryResult.CreateLISTResponse(queryResult.Response)
                .CreateLinks(c => SystemRoutes.SettingsiRateUser.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }
    }
}