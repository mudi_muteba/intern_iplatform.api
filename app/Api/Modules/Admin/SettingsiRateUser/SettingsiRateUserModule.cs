﻿using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.Admin.SettingsiRateUser
{
    public class SettingsiRateUserModule : SecureModule
    {
        private readonly IExecutionPlan m_ExecutionPlan;
        private readonly IRepository m_Repository;

        public SettingsiRateUserModule(IExecutionPlan executionPlan, IRepository repository)
        {
            m_ExecutionPlan = executionPlan;
            m_Repository = repository;

            
            Get[SystemRoutes.SettingsiRateUser.GetById.Route] = p => GetSettingsiRateUser(p.id);
            Put[SystemRoutes.SettingsiRateUser.Put.Route] = p => UpdateSettingsiRateUser(p.id);
            Put[SystemRoutes.SettingsiRateUser.PutDelete.Route] = p => DeleteSettingsiRateUser(p.id);

        }

        private Negotiator GetSettingsiRateUser(int id)
        {
            var result = m_ExecutionPlan
                .GetById<Domain.Admin.SettingsiRateUser.SettingsiRateUser, SettingsiRateUserDto>(() => m_Repository.GetById<Domain.Admin.SettingsiRateUser.SettingsiRateUser>(id))
                .OnSuccess(Mapper.Map<Domain.Admin.SettingsiRateUser.SettingsiRateUser, SettingsiRateUserDto>)
                .Execute();

            var response = result.CreateGETResponse()
                .CreateLinks(c => SystemRoutes.SettingsiRateUser.CreateLinks(c))
                ;

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator UpdateSettingsiRateUser(int id)
        {
            var editSettingsiRateUserDto = this.Bind<EditSettingsiRateUserDto>();
            editSettingsiRateUserDto.Id = id;

            var result = m_ExecutionPlan.Execute<EditSettingsiRateUserDto, int>(editSettingsiRateUserDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.SettingsiRateUser.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        private Negotiator DeleteSettingsiRateUser(int id)
        {
            var disableSettingsiRateUserDto = this.Bind<DisableSettingsiRateUserDto>();
            disableSettingsiRateUserDto.Id = id;

            var result = m_ExecutionPlan.Execute<DisableSettingsiRateUserDto, int>(disableSettingsiRateUserDto);

            var response = result.CreatePUTResponse()
                .CreateLinks(SystemRoutes.SettingsiRateUser.CreateGetById(result.Response));

            return Negotiate
                .WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }
    }
}