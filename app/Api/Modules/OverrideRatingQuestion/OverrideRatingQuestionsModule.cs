﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.OverrideRatingQuestion.Queries;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.OverrideRatingQuestion
{
    public class OverrideRatingQuestionsModule : SecureModule
    {
        private readonly IPaginateResults _Paginator;
        private readonly IExecutionPlan _ExecutionPlan;

        public OverrideRatingQuestionsModule(IPaginateResults paginator, IExecutionPlan executionPlan)
        {
            _Paginator = paginator;
            _ExecutionPlan = executionPlan;

            Post[SystemRoutes.OverrideRatingQuestions.Post.Route] = _ => Create();
            Get[SystemRoutes.OverrideRatingQuestions.Get.Route] = _ => GetAll();
            Get[SystemRoutes.OverrideRatingQuestions.GetWithPagination.Route] = p => GetAllWithPagination(new Pagination(p.pageNumber, p.pageSize));
            Post[SystemRoutes.OverrideRatingQuestions.Search.Route] = _ => Search();
        }

        #region [ Negotiators ]

        /// <summary>
        /// Create a new OverrideRatingQuestion
        /// </summary>
        /// <returns></returns>
        private Negotiator Create()
        {
            CreateOverrideRatingQuestionDto createOverrideRatingQuestionDto = this.Bind<CreateOverrideRatingQuestionDto>();
            HandlerResult<int> result = _ExecutionPlan.Execute<CreateOverrideRatingQuestionDto, int>(createOverrideRatingQuestionDto);
            POSTResponseDto<int> response = result.CreatePOSTResponse()
                    .CreateLinks(SystemRoutes.OverrideRatingQuestions.CreateGetById(result.Response));
            
            return Negotiate.WithLocationHeader(response.Link).ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Get all OverrideRatingQuestions that have not been deleted.
        /// </summary>
        /// <returns></returns>
        public Negotiator GetAll()
        {
            QueryResult<ListResultDto<ListOverrideRatingQuestionDto>> queryResult =
                _ExecutionPlan
                    .Query
                    <GetAllOverrideRatingQuestionsQuery, Domain.OverrideRatingQuestion.OverrideRatingQuestion,
                        ListResultDto<ListOverrideRatingQuestionDto>, ListOverrideRatingQuestionDto>()
                    .OnSuccess(result => Mapper.Map<ListResultDto<ListOverrideRatingQuestionDto>>(result.ToList()))
                    .Execute();

            LISTResponseDto<ListOverrideRatingQuestionDto> response =
                queryResult.CreateLISTResponse(queryResult.Response)
                    .CreateLinks(c => SystemRoutes.OverrideRatingQuestions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Get all OverrideRatingQuestions with pagination that have not been deleted.
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public Negotiator GetAllWithPagination(Pagination pagination)
        {
            QueryResult<PagedResultDto<ListOverrideRatingQuestionDto>> queryResult =
                _ExecutionPlan
                    .Query
                    <GetAllOverrideRatingQuestionsQuery, Domain.OverrideRatingQuestion.OverrideRatingQuestion,
                        PagedResultDto<ListOverrideRatingQuestionDto>, ListOverrideRatingQuestionDto>()
                    .OnSuccess(result => Mapper.Map<PagedResultDto<ListOverrideRatingQuestionDto>>(result.Paginate(_Paginator, pagination)))
                    .Execute();

            LISTPagedResponseDto<ListOverrideRatingQuestionDto> response =
                queryResult.CreateLISTPagedResponse(queryResult.Response)
                    .CreateLinks(c => SystemRoutes.OverrideRatingQuestions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Get all OverrideRatingQuestions with the passed search criteria() that have not been deleted.
        /// </summary>
        /// <returns></returns>
        public Negotiator Search()
        {
            SearchOverrideRatingQuestionDto searchOverrideRatingQuestionDto =
                this.Bind<SearchOverrideRatingQuestionDto>();

            QueryResult<PagedResultDto<ListOverrideRatingQuestionDto>> queryResult =
                _ExecutionPlan.Search<SearchOverrideRatingQuestionQuery, Domain.OverrideRatingQuestion.OverrideRatingQuestion,
                        SearchOverrideRatingQuestionDto, PagedResultDto<ListOverrideRatingQuestionDto>>(
                            searchOverrideRatingQuestionDto)
                    .OnSuccess(
                        result =>
                            Mapper.Map<PagedResultDto<ListOverrideRatingQuestionDto>>(result.Paginate(_Paginator,
                                searchOverrideRatingQuestionDto.CreatePagination())))
                    .Execute();

            LISTPagedResponseDto<ListOverrideRatingQuestionDto> response =
                queryResult.CreateLISTPagedResponse(queryResult.Response)
                    .CreateLinks(c => SystemRoutes.OverrideRatingQuestions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        #endregion
    }
}