﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Infrastructure;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace Api.Modules.OverrideRatingQuestion
{
    public class OverrideRatingQuestionModule : SecureModule
    {
        private readonly IExecutionPlan _ExecutionPlan;
        private readonly IRepository _Repository;

        public OverrideRatingQuestionModule(IExecutionPlan executionPlan, IRepository repository)
        {
            _ExecutionPlan = executionPlan;
            _Repository = repository;

            Get[SystemRoutes.OverrideRatingQuestions.GetById.Route] = p => GetById(p.Id);
            Put[SystemRoutes.OverrideRatingQuestions.Put.Route] = p => Update(p.Id);
            Put[SystemRoutes.OverrideRatingQuestions.PutDelete.Route] = p => Delete(p.Id);
        }

        #region [ Negotiators ]

        /// <summary>
        /// Get a OverrideRatingQuestion by it's unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Negotiator GetById(int id)
        {
            GetByIdResult<OverrideRatingQuestionDto> result = _ExecutionPlan.GetById<Domain.OverrideRatingQuestion.OverrideRatingQuestion, OverrideRatingQuestionDto>(
                    () => _Repository.GetById<Domain.OverrideRatingQuestion.OverrideRatingQuestion>(id))
                    .OnSuccess(Mapper.Map<Domain.OverrideRatingQuestion.OverrideRatingQuestion, OverrideRatingQuestionDto>)
                    .Execute();

            GETResponseDto<OverrideRatingQuestionDto> response = result.CreateGETResponse().CreateLinks(c => SystemRoutes.OverrideRatingQuestions.CreateLinks(c));

            return Negotiate.ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Updates a OverrideRatingQuestion by it's unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Negotiator Update(int id)
        {
            EditOverrideRatingQuestionDto editOverrideRatingQuestionDto = this.Bind<EditOverrideRatingQuestionDto>();
            editOverrideRatingQuestionDto.Id = id;

            HandlerResult<int> result = _ExecutionPlan.Execute<EditOverrideRatingQuestionDto, int>(editOverrideRatingQuestionDto);
            PUTResponseDto<int> response = result.CreatePUTResponse().CreateLinks(SystemRoutes.OverrideRatingQuestions.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        /// <summary>
        /// Deletes a OverrideRatingQuestion by it's unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Negotiator Delete(int id)
        {
            DeleteOverrideRatingQuestionDto deleteOverrideRatingQuestionDto = this.Bind<DeleteOverrideRatingQuestionDto>();
            deleteOverrideRatingQuestionDto.Id = id;

            HandlerResult<int> result = _ExecutionPlan.Execute<DeleteOverrideRatingQuestionDto, int>(deleteOverrideRatingQuestionDto);
            PUTResponseDto<int> response = result.CreatePUTResponse().CreateLinks(SystemRoutes.OverrideRatingQuestions.CreateGetById(result.Response));

            return Negotiate.WithLocationHeader(response.Link)
                .ForSupportedFormats(response, response.StatusCode);
        }

        #endregion
    }
}