﻿using System.Diagnostics;
using System.Reflection;

namespace Api.Models
{
    public class AboutApi
    {
        public AboutApi()
        {
            var apiAssembly = Assembly.GetExecutingAssembly();
            var apiFileInfo = FileVersionInfo.GetVersionInfo(apiAssembly.Location);
            Application = apiFileInfo.ProductName;
            Version = apiFileInfo.FileVersion;
        }

        public string Version { get; set; }

        public string Application { get; set; }
    }
}