﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using FluentNHibernate.Utils;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using Nancy;

using System.Reflection;

namespace Api.Models
{
    public class SiteMap
    {
        public AboutApi About { get; set; }
        public readonly List<SiteMapNode> Nodes = new List<SiteMapNode>();
        private static readonly ILog log = LogManager.GetLogger<SiteMap>();

        #region Consturctors 
        public SiteMap()
        {
            About = new AboutApi();
        }

        public SiteMap(NancyContext context, IDefineRoutes route) : this()
        {
            var homeEntryPointUrl = string.Format("{0}{1}", context.Request.Url.SiteBase, SystemRoutes.Home.Options.Route);
            Nodes.Add(new SiteMapNode("GET", "Home", "Api entry point", homeEntryPointUrl));

            Nodes.AddRange(route.Routes().Select(r =>
            {
                var entryPointUrl = string.Format("{0}{1}", context.Request.Url.SiteBase, r.Route);

                return new SiteMapNode(r.Verb, string.Empty, r.Description, entryPointUrl, r.Parameter);
            }));
        }
        #endregion

        #region GET
        public List<SiteMapNode> GetOptionSiteNodes(NancyContext context)
        {
            return new List<SiteMapNode>((SystemRoutes.Routes.Select(r =>
            {
                var entryPointUrl = string.Format("{0}{1}", context.Request.Url.SiteBase, r.Definition.EntryPoint);

                return new SiteMapNode(r.Definition.Verb, r.Definition.Name, r.Definition.Description, entryPointUrl);
            })));
        }

        public List<SiteMapBaseNode> GetAllSiteNodes()
        {
            var baseRouteNodes = new List<SiteMapBaseNode>();
            //Assembly assembly = Assembly.LoadFrom(assemblyPath);

            //var q = from t in assembly.GetTypes()
            //        where t.IsClass
            //        select t;
            //var ns = q.ToList().FirstOrDefault(x => x.Name == "Domain.HistoryLoss.LossHistory.LossHistory");

            //ns.
            try
            {
                foreach (var systemRoute in SystemRoutes.Routes)
                {
                    var baseNode = new SiteMapBaseNode(systemRoute.Definition.Name, systemRoute.Definition.Description,
                        systemRoute.Definition.EntryPoint, new List<SiteMapNode>());

                    foreach (var systemSubRoute in systemRoute.Routes())
                    {
                        baseNode.Nodes.Add(new SiteMapNode(systemSubRoute.Verb, "", systemSubRoute.Description,
                            systemSubRoute.Route, systemSubRoute.Parameter));
                    }

                    baseRouteNodes.Add(baseNode);
                }
                ;

                // return baseRouteNodes.OrderBy(x => x.Name).ToList();

                return baseRouteNodes.Select(x => x)
                    .Where(y => y.Name == "LossHistorydefinitions").ToList();
                //.Where(y => y.Name == "PostalCodeLookups").ToList();
            }
            catch (Exception exception)
            {
                log.FatalFormat("Fatal Error occurred in SiteMap when GetAllSiteNodes, one or more nodes are invalid",
                    exception, null);

                baseRouteNodes.Clear();
                baseRouteNodes.Add(new SiteMapBaseNode("Error", "Error", "unable to retrieve sitemap of API", null));
                return baseRouteNodes;
            }
        }

        #endregion
    };
}