﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace Api.Models
{
    public class SiteMapNode
    {
        public SiteMapNode()
        {
        }

        public SiteMapNode(string verb, string name, string description, string entryPointUrl)
            : this(verb, name, description, entryPointUrl, null)
        {
        }

        public SiteMapNode(string verb, string name, string description, string entryPointUrl,
            RouteParameterDescriber parameter)
        {
            Verb = verb;
            Name = name;
            Description = description;
            EntryPointUrl = entryPointUrl;
            Parameter = parameter;
        }

        public string Verb { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string EntryPointUrl { get; set; }
        public RouteParameterDescriber Parameter { get; set; }
    }
}