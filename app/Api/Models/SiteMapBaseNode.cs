﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class SiteMapBaseNode
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string EntryPointUrl { get; private set; }
        public List<SiteMapNode> Nodes { get; private set; }

        public SiteMapBaseNode(string name, string description, string entryPointUrl, List<SiteMapNode> nodes)
        {
            Name = name;
            Description = description;
            EntryPointUrl = entryPointUrl;
            Nodes = nodes;
        }
    }
}