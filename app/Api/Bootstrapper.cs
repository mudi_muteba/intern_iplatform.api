﻿using System.Collections.Generic;
using Api.Infrastructure;
using Api.Installers;
using AutoMapper;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Domain.Admin;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Bootstrappers.Windsor;
using Infrastructure.NHibernate.ContractResolvers;
using Newtonsoft.Json;

namespace Api
{
    public class Bootstrapper : WindsorNancyBootstrapper
    {
        protected override void RequestStartup(IWindsorContainer container, IPipelines pipelines, NancyContext context)
        {
            pipelines.AddLogging();
            pipelines.AddExecutionContext(context, container);
            pipelines.AddErrorHandling(context);
            AddTransactionScope(container, pipelines, context);

            base.RequestStartup(container, pipelines, context);
        }

        protected virtual void AddTransactionScope(IWindsorContainer container, IPipelines pipelines, NancyContext context)
        {
            pipelines.AddTransactionScope(container, context);
        }

        protected override void ConfigureApplicationContainer(IWindsorContainer container)
        {
            base.ConfigureApplicationContainer(container);

            AutoMapperConfiguration.Configure();
            
            InstallDataAccessLayer(container);

            container.Install(new WindsorInstallerCollection().Installers);
            var repository = container.Resolve<IRepository>();
            container.Release(repository);
            var systemChannels = Mapper.Map<IEnumerable<Channel>, SystemChannels>(repository.GetAll<Channel>());
            container.Register(Component.For<SystemChannels>().Instance(systemChannels).LifestyleSingleton());
            container.Register(Component.For<JsonSerializer>().ImplementedBy<CustomJSONContractResolver>().LifestyleTransient());
        }

        protected virtual void InstallDataAccessLayer(IWindsorContainer existingContainer)
        {
            existingContainer.Install(new NhibernateInterceptorInstaller());
            existingContainer.Install(new DataAccessInstaller());
        }
    }

    public class CustomJSONContractResolver : JsonSerializer
    {
        public CustomJSONContractResolver()
        {
            this.ContractResolver = new NHibernateContractResolver();
            this.Formatting = Formatting.Indented;
        }
    }
}