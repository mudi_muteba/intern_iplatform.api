using System;
using Nancy;
using Nancy.Responses.Negotiation;
using HttpStatusCode = System.Net.HttpStatusCode;

namespace Api.Infrastructure
{
    public static class NegotiateExtensions
    {
        public static Negotiator ForJson(this Negotiator negotiator, object model)
        {
            var jsonMediaRange = new MediaRange("application/json");

            return negotiator
                .WithMediaRangeModel(jsonMediaRange, model)
                .WithAllowedMediaRange(jsonMediaRange);
        }

        public static Negotiator ForXml(this Negotiator negotiator, object model)
        {
            var xmlMediaRange = new MediaRange("application/xml");

            return negotiator
                .WithMediaRangeModel(xmlMediaRange, model)
                .WithAllowedMediaRange(xmlMediaRange);
        }

        public static Negotiator SiteForSupportedFormats(this Negotiator negotiator, object model)
        {
            return negotiator
                .WithModel(model)
                .ForJson(model)
                .ForXml(model);
        }

        public static Negotiator WithLocationHeader(this Negotiator negotiator, string location)
        {
            return negotiator
                .WithHeader("Location", location);
        }

        public static Negotiator ForSupportedFormats<T>(this Negotiator negotiator, T model, HttpStatusCode statusCode)
            where T : class
        {
            return negotiator
                .WithStatusCode(statusCode.Convert())
                .WithModel(model)
                .ForJson(model)
                .ForXml(model);
        }

        private static Nancy.HttpStatusCode Convert(this HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.Accepted:
                    return Nancy.HttpStatusCode.Accepted;
                case HttpStatusCode.Created:
                    return Nancy.HttpStatusCode.Created;
                case HttpStatusCode.OK:
                    return Nancy.HttpStatusCode.OK;
                case HttpStatusCode.BadRequest:
                    return Nancy.HttpStatusCode.BadRequest;
                case HttpStatusCode.Unauthorized:
                    return Nancy.HttpStatusCode.Unauthorized;
                case HttpStatusCode.NotFound:
                    return Nancy.HttpStatusCode.NotFound;
                case HttpStatusCode.InternalServerError:
                    return Nancy.HttpStatusCode.InternalServerError;
                default:
                    throw new Exception(string.Format("Don't understand {0} HTTP status code", statusCode));
            }
        }
    }
}