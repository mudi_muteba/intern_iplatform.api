﻿using System.Collections.Generic;
using Domain.Users;
using Nancy.Security;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Enums;
using System;

namespace Api.Infrastructure
{
    public class ApiUser : IUserIdentity
    {
        private ApiUser() { }

        public ApiUser(int id, ApiRequestType apiRequestType, int activeChannelId, Guid activeChannelSystemId, CultureParameter culture)
        {
            UserId = id;
            Claims = new List<string>();
            IsAuthenticated = true;
            ActiveChannelId = activeChannelId;
            RequestType = apiRequestType.ToString();
            Culture = culture;
            ActiveChannelSystemId = activeChannelSystemId;
        }

        public int UserId { get; private set; }
        public bool IsAuthenticated { get; private set; }
        public List<UserAuthorisationGroup> Groups { get; private set; }
        public IEnumerable<int> Channels { get; private set; }
        public int ActiveChannelId { get; set; }
        public string RequestType { get; private set; }
        public string UserName { get; private set; }
        public string UserFullname { get; private set; }
        public IEnumerable<string> Claims { get; private set; }
        public Guid ActiveChannelSystemId { get; private set; }
        public CultureParameter Culture { get; private set; }

        public static ApiUser CreateUnauthenticatedUser()
        {
            return new ApiUser
            {
                IsAuthenticated = false
            };
        }

        public void Set(int userId, string username, string fullName, List<int> channelIds, List<UserAuthorisationGroup> userAuthorisationGroups)
        {
            UserId = userId;
            UserName = username;
            UserFullname = fullName;
            Channels = channelIds;
            Groups = userAuthorisationGroups;
        }
    }
}