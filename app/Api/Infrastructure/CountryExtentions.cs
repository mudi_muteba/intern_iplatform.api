﻿using MasterData;
using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Infrastructure
{
    public static class CountryExtentions
    {
        public static Country GetCountry(this Request request)
        {
            var countryCode = GetCountryCode(request);
            var country = new Countries().FirstOrDefault(c => c.Code.Equals(countryCode, StringComparison.InvariantCultureIgnoreCase));

            return country ?? Countries.SouthAfrica;
        }


        private static string GetCountryCode(Request request)
        {
            var za = "ZA";
            var countryHeaderKey = "X-iPlatform-Country";

            if (request == null)
                return za;

            var matchingHeader = request.Headers.FirstOrDefault(h => countryHeaderKey.Equals(h.Key, StringComparison.InvariantCultureIgnoreCase));

            if (matchingHeader.Key == null)
            {
                return za;
            }

            var countryCode = matchingHeader.Value.FirstOrDefault();

            return string.IsNullOrWhiteSpace(countryCode)
                ? za
                : countryCode;

        }
    }
}