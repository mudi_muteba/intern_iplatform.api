using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Domain.Base.Execution;
using Domain.Users;
using Domain.Users.Authorisation;
using Nancy;

namespace Api.Infrastructure
{
    public class DefaultContextCreator : ICreateContext
    {
        public ExecutionContext Create(string token, ApiUser user, Request request, Guid requestId, List<int> channelIds, List<UserAuthorisationGroup> userAuthorisationGroups)
        {
            var calculator = new EffectiveAuthorisationCalculator();
            var authorisation = calculator.Calculate(userAuthorisationGroups);

            var headersToDictionary = new Dictionary<string, IEnumerable<string>>();
            if (request != null)
                request.Headers.ForEach(a => headersToDictionary.Add(a.Key, a.Value));

            log4net.LogicalThreadContext.Properties["RequestId"] = requestId;
            log4net.LogicalThreadContext.Properties["UserId"] = user.UserId;
            log4net.LogicalThreadContext.Properties["ActiveChannelSystemId"] = user.ActiveChannelSystemId;
            log4net.LogicalThreadContext.Properties["RequestMethod"] = request != null ? request.Method : "";
            log4net.LogicalThreadContext.Properties["RequestUrl"] = request != null ? request.Url.ToString() : "";

            var executionContext = new ExecutionContext(
                token,
                user.UserId,
                user.UserName,
                user.UserFullname,
                user.Culture,
                channelIds,
                user.ActiveChannelId,
                authorisation, headersToDictionary,
                request != null ? request.Headers.UserAgent : "",
                request != null ? request.Headers.Referrer : "", 
                requestId,
                user.RequestType,
                request != null ? request.UserHostAddress : "", 
                DateTime.UtcNow,
                request != null ? request.Method : "",
                request != null ? request.Url.ToString() : "",
                user.ActiveChannelSystemId
                );

            return executionContext;
        }
    }
}