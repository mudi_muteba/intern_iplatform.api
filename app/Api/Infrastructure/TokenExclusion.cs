using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using Nancy;

namespace Api.Infrastructure
{
    internal class RouteIdentifier
    {
        public string Verb { get; private set; }
        public string Path { get; private set; }

        public RouteIdentifier(string verb, string path)
        {
            Verb = verb.ToLowerInvariant();
            Path = path.ToLowerInvariant();
        }

        public bool Match(Request request)
        {
            return request.Method.ToLowerInvariant().Equals(Verb) && request.Path.ToLowerInvariant().TrimEnd('/').Equals(Path.TrimEnd('/'));
        }
    }

    internal static class TokenExclusion
    {
        private static readonly List<RouteIdentifier> excluded = new List<RouteIdentifier>
        {
            new RouteIdentifier("GET", SystemRoutes.Authenticate.GetIsAlive.Route),
            new RouteIdentifier("POST", SystemRoutes.Authenticate.Post.Route),
            new RouteIdentifier("POST", SystemRoutes.Authenticate.ByToken.Route),
            new RouteIdentifier("POST", "/audits"),
            new RouteIdentifier("POST", SystemRoutes.Users.RegisterAPIUser.Route),
            new RouteIdentifier("PUT", SystemRoutes.Users.ValidatePasswordResetToken.Route),
            new RouteIdentifier("PUT", SystemRoutes.Users.RequestPasswordReset.Route),
            new RouteIdentifier("PUT", SystemRoutes.Users.ResetPassword.Route),
            new RouteIdentifier("GET", "/"),
            new RouteIdentifier("POST", SystemRoutes.SignFlow.GetDigitalSignatureDocumentUpdateStatus.Route),
            new RouteIdentifier("POST",SystemRoutes.SmartPolicy.GetSmartPolicy.Route),
            new RouteIdentifier("POST", SystemRoutes.SmartPolicy.GetSmartPolicyPdf.Route),
            new RouteIdentifier("POST", SystemRoutes.SmartPolicy.RegisterSmartPolicy.Route),
            new RouteIdentifier("POST", SystemRoutes.SmartPolicy.UpdateSmartPolicy.Route),
            new RouteIdentifier("POST", SystemRoutes.SmartPolicy.SendPdfSmartPolicy.Route),
    };

        public static bool Excluded(Request request)
        {
            var match = excluded.FirstOrDefault(e => e.Match(request));

            return match != null;
        }
    }
}