using Nancy;

namespace Api.Infrastructure
{
    public abstract class SecureModule : NancyModule
    {
        protected SecureModule()
        {
        }

        protected SecureModule(string modulePath) : base(modulePath)
        {
        }
    }
}