﻿using System;
using System.IO;
using System.Linq;
using Nancy;
using Nancy.ErrorHandling;
using Nancy.Responses.Negotiation;

namespace Api.Infrastructure
{
    public class NotFoundStatusCodeHandler : IStatusCodeHandler
    {
        public bool HandlesStatusCode(HttpStatusCode statusCode, NancyContext context)
        {
            return statusCode == HttpStatusCode.NotFound;
        }

        public void Handle(HttpStatusCode statusCode, NancyContext context)
        {
            if (!HandlesStatusCode(statusCode, context))
                return;

            var defaultContentType = new MediaRange("application/json");

            var accept = context.Request.Headers.Accept;

            var contentType = accept.OrderByDescending(o => o.Item2).Select(o => MediaRange.FromString(o.Item1)).ToList().FirstOrDefault();

            if (contentType == null)
                contentType = defaultContentType;
            else
            {
                if (contentType.IsWildcard)
                    contentType = defaultContentType;
            }

            Action<Stream> contents = stream =>
            {
                using (var writer = new StreamWriter(stream))
                {
                    writer.Flush();
                }
            };

            context.Response = new Response
            {
                StatusCode = HttpStatusCode.NotFound,
                ContentType = contentType.ToString(),
                Contents = contents
            };

            context.Response.Headers.Add("Content-Type", contentType.ToString());
        }
    }
}