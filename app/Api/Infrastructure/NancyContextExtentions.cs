using iPlatform.Api.DTOs.Base.Culture;
using Nancy;

namespace Api.Infrastructure
{
    public static class NancyContextExtentions
    {
        public static string CreateUrl(this NancyContext context, string path)
        {
            return string.Format("{0}{1}", context.Request.Url.SiteBase, path);
        }

    }
}