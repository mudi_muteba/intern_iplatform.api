using System;
using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Users;
using Nancy;

namespace Api.Infrastructure
{
    public interface ICreateContext
    {
        ExecutionContext Create(string token, ApiUser user, Request request, Guid requestId, List<int> channelIds, List<UserAuthorisationGroup> userAuthorisationGroups);
    }
}