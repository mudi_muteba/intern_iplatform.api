﻿using System;
using System.Linq;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;
using Domain.Users;
using Domain.Users.Authentication;
using JWT;
using Shared;
using Shared.Extentions;

namespace Api.Infrastructure
{
    public static class ApiUserExtensions
    {
        public static ApiUser Set(this ApiUser apiUser, IRepository repository)
        {
            var user = repository.GetById<User>(apiUser.UserId);
            apiUser.Set(user.Id, user.UserName, user.GetFullname(), user.Channels.Select(x => x.Channel.Id).ToList(), user.AuthorisationGroups.ToList());
            return apiUser;
        }
    }

    public class ApiUserBuilder
    {
        private ApiUser BuildUser(TokenGenerator.TokenReader reader)
        {
            return new ApiUser(reader.Id, reader.RequestType, reader.ActiveChannelId, reader.ActiveChannelSystemId, reader.Culture);
        }

        public ApiUser ExtractToken(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                this.Error(() => "No token provided during authentication");
                return ApiUser.CreateUnauthenticatedUser();
            }

            try
            {
                return BuildUser(new TokenGenerator.TokenReader(token));
            }
            catch (TokenValidationException)
            {
                this.Error(() => string.Format("Invalid token '{0}'", token));
                return ApiUser.CreateUnauthenticatedUser();
            }
            catch (SignatureVerificationException)
            {
                this.Error(() => string.Format("Invalid token '{0}'", token));
                return ApiUser.CreateUnauthenticatedUser();
            }
            catch (Exception e)
            {
                var message = new ExceptionPrettyPrinter().Print(e);
                this.Error(() => string.Format("Invalid token {0}. Failure {1}", token, message));
                return ApiUser.CreateUnauthenticatedUser();
            }
        }
    }
}