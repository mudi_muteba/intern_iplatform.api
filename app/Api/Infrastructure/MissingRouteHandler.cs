using System;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Nancy;
using Nancy.ErrorHandling;
using Nancy.Responses;


namespace Api.Infrastructure
{
    public class MissingRouteHandler : IStatusCodeHandler
    {
        public bool HandlesStatusCode(HttpStatusCode statusCode, NancyContext context)
        {
            return statusCode == HttpStatusCode.NotFound;
        }

        public void Handle(HttpStatusCode statusCode, NancyContext context)
        {
            if (!HandlesStatusCode(statusCode, context))
                return;

            var accept = context.Request.Headers.Accept;
            var contentType = "application/json";
            if (accept != null && accept.Any())
                contentType = accept.First().Item1;

            var response = new BaseResponseDto
            {
                StatusCode = System.Net.HttpStatusCode.NotFound
            };

            context.Response = CreateResponse(response, contentType);
            context.Response.StatusCode = statusCode;
        }

        public Response CreateResponse(BaseResponseDto response, string contextType)
        {
            if("application/json".Equals(contextType, StringComparison.InvariantCultureIgnoreCase))
                return new JsonResponse(response, new DefaultJsonSerializer());

            if ("application/xml".Equals(contextType, StringComparison.InvariantCultureIgnoreCase))
                return new XmlResponse<BaseResponseDto>(response, new DefaultXmlSerializer());

            return new HtmlResponse(HttpStatusCode.NotFound, Response.NoBody);
        }
    }
}