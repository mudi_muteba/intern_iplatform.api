﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Castle.Windsor;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Performance;
using Domain.Users;
using Domain.Users.Queries;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Enums;
using Nancy;
using Nancy.Bootstrapper;
using NHibernate;
using NHibernate.Context;
using Shared;
using Shared.Extentions;

namespace Api.Infrastructure
{
    public static class PipelineExtensions
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PipelineExtensions));

        public static void AddPerformanceCounter(this IPipelines pipelines, NancyContext context, IWindsorContainer container)
        {
            //pipelines.BeforeRequest += ctx =>
            //{
            //    var authorisationToken = GetAuthorisationToken(context, ctx);
            //    var user = GetApiUser(authorisationToken);

                //var repository = container.Resolve<IRepository>();
                //container.Release(repository);
                //PerformanceCounters.Instance.Create(ctx.Request.Path).WithUserId(user.UserId).WithRepository(repository).WithDescription(ctx.Request.Url.Path);
                //PerformanceCounters.Instance.StartCounter(ctx.Request.Path);

            //    return null;
            //};

            pipelines.AfterRequest += ctx =>
            {
                PerformanceCounters.Instance.StopCounter(ctx.Request.Path);
            };
        }

        public static void AddLogging(this IPipelines pipelines)
        {
            pipelines.BeforeRequest += ctx =>
            {
                Log.InfoFormat("Received {0}", ctx.Request.Url);
                return null;
            };

            pipelines.AfterRequest += ctx =>
            {
                Log.InfoFormat("Completed {0} with status code {1}", ctx.Request.Url, ctx.Response == null ? "?" : ctx.Response.StatusCode.ToString());
            };
        }

        public static void AddExecutionContext(this IPipelines pipelines, NancyContext context, IWindsorContainer container)
        {
            pipelines.BeforeRequest += ctx =>
            {
                if (TokenExclusion.Excluded(ctx.Request))
                {
                    typeof(PipelineExtensions).Debug(() => string.Format("Excluding {0} ({1}) from authentication", ctx.Request.Path, ctx.Request.Method));
                    return SetContextAsRootUser(context, container);
                }

                var repo = container.Resolve<IRepository>();
                container.Release(repo);
                var authorisationToken = GetAuthorisationToken(context, ctx);
                var apiUser = GetApiUser(authorisationToken, repo);
                if (!apiUser.IsAuthenticated)
                    return new Response {StatusCode = HttpStatusCode.Unauthorized};
                
                var user = repo.GetById<User>(apiUser.UserId);
                if (apiUser.ActiveChannelId == 0)
                    apiUser.ActiveChannelId = user.DefaultChannelId;
                SetContext(context, container, apiUser, authorisationToken, user.ChannelIds.ToList(), user.AuthorisationGroups.ToList());

                return null;
            };
        }

        private static ApiUser GetApiUser(string authorisationToken, IRepository repository)
        {
            return new ApiUserBuilder().ExtractToken(authorisationToken).Set(repository);
        }

        private static string GetAuthorisationToken(NancyContext context, NancyContext ctx)
        {
            var authorisationToken = context.Request.Headers.Authorization;
            if (string.IsNullOrWhiteSpace(authorisationToken))
                typeof(PipelineExtensions).Debug(() => string.Format("API called with no token. Request: {0}", ctx.Request.Url));
            return authorisationToken;
        }

        private static Response SetContextAsRootUser(NancyContext context, IWindsorContainer container)
        {
            const string username = "root@iplatform.co.za";
            Log.InfoFormat("Setting root user: {0} in context", username);

            var query = container.Resolve<FindUserByUserNameQuery>();
            container.Release(query);

            var rootUser = query.WithUserName(username).ExecuteQuery().FirstOrDefault();
            if (rootUser == null)
            {
                Log.ErrorFormat("User {0} not found", username);
                return new Response { StatusCode = HttpStatusCode.Unauthorized };
            }

            var systemId = rootUser.DefaultChannel == null ? Guid.Empty : rootUser.DefaultChannel.SystemId;
            var user = new ApiUser(rootUser.Id, ApiRequestType.Api, rootUser.DefaultChannelId, systemId, CultureParameter.CreateDefault());
            SetContext(context, container, user, rootUser.GenerateToken(ApiRequestType.Api, rootUser.DefaultChannelId), rootUser.ChannelIds.ToList(), rootUser.AuthorisationGroups.ToList());

            return null;
        }

        private static void SetContext(NancyContext context, IWindsorContainer container, ApiUser user, string authorisationToken, List<int> channelIds, List<UserAuthorisationGroup> userAuthorisationGroups)
        {
            context.CurrentUser = user;

            var contextCreator = container.Resolve<ICreateContext>();
            var provideContext = container.Resolve<IProvideContext>();
            container.Release(contextCreator);
            container.Release(provideContext);
            var executionContext = contextCreator.Create(authorisationToken, user, context.Request, Guid.NewGuid(), channelIds, userAuthorisationGroups);

            provideContext.SetContext(executionContext);
        }

        public static void AddErrorHandling(this IPipelines pipelines, NancyContext context)
        {
            pipelines.OnError += (ctx, err) =>
            {
                var exceptionDetails = new ExceptionPrettyPrinter().Print(err);
                Log.Error("Unhandled error on request: " + context.Request.Url + " : " + exceptionDetails);

                return HttpStatusCode.InternalServerError;
            };

            pipelines.OnError.AddItemToEndOfPipeline((z, a) =>
            {
                var exceptionDetails = new ExceptionPrettyPrinter().Print(a);
                Log.Error("Unhandled error on request: " + context.Request.Url + " : " + exceptionDetails);

                return new Response { StatusCode = HttpStatusCode.InternalServerError };
            });
        }

        public static void AddTransactionScope(this IPipelines pipelines, IWindsorContainer container,
            TransactionScope scope)
        {
            pipelines.AfterRequest.AddItemToEndOfPipeline(ctx =>
            {
                if (ctx.Response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    scope.Dispose();
                    return;
                }

                scope.Complete();
                scope.Dispose();
            });
        }

        public static void AddTransactionScope(this IPipelines pipelines, IWindsorContainer container, NancyContext context)
        {
            pipelines.BeforeRequest += ctx =>
            {
                var session = container.Resolve<ISession>();
                container.Release(session);
                if (session == null) return null;

                CurrentSessionContext.Bind(session);
                session.BeginTransaction();

                return null;
            };

            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx =>
            {
                var sessionFactory = container.Resolve<ISessionFactory>();
                container.Release(sessionFactory);
                if (!CurrentSessionContext.HasBind(sessionFactory))
                    return;

                var session = sessionFactory.GetCurrentSession();
                if (ctx.Response.StatusCode == HttpStatusCode.InternalServerError)
                    session.Transaction.Rollback();
                else
                    session.Transaction.Commit();

                CurrentSessionContext.Unbind(sessionFactory);
            }));
        }
    }
}