using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Nancy.Bootstrappers.Windsor;

namespace Api.Infrastructure
{
    public static class WindsorContainerExtentions
    {
        public static void SetLifeCycle<T>(this IWindsorContainer container, ComponentRegistration<T> registration)
            where T : class
        {
            container.Register(registration.LifestyleScoped<NancyPerWebRequestScopeAccessor>());
        }

        public static void Install<TType>(this IWindsorContainer container, IRegistration registration)
        {
            if (!container.Kernel.HasComponent(typeof (TType)))
                container.Register(registration);
        }
    }
}