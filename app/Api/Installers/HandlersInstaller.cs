using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain;
using Domain.Base.Handlers;
using Nancy.Bootstrappers.Windsor;

namespace Api.Installers
{
    public class HandlersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<DomainMarker>()
                .BasedOn(typeof (IHandleDto<, >))
                .WithServiceAllInterfaces()
                .LifestyleScoped<NancyPerWebRequestScopeAccessor>());
        }
    }
}