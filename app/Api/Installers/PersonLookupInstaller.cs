using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using iPerson.Api.DTOs.Connector;

namespace Api.Installers
{
    public class PersonLookupInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IPersonConnector>().ImplementedBy<PersonConnector>().LifestyleTransient());
        }
    }
}