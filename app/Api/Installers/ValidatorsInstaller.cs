using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain;
using Domain.Base.Validation;

namespace Api.Installers
{
    public class ValidatorsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<DomainMarker>()
                .BasedOn(typeof (IValidateDto<>))
                .WithServiceAllInterfaces()
                .LifestyleTransient());
        }
    }
}