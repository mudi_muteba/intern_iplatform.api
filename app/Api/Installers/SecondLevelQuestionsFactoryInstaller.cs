﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.SecondLevel.Factory;

namespace Api.Installers
{
    public class SecondLevelQuestionsFactoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var assembly = typeof(AutoGetSecondLevelQuestions).Assembly;

            var mappertoInstall = (from type in assembly.GetTypes()
                                   where typeof(ICreateSecondLevelQuestions).IsAssignableFrom(type)
                                        select new { Type = type });

            foreach (var mapper in mappertoInstall)
            {
                container.Register(
                    Component.For(mapper.Type)
                        .ImplementedBy(mapper.Type)
                        .LifeStyle.Transient.Named(GetMapperName(mapper.Type))
                    );
            }
        }

        private static string GetMapperName(Type mapperType)
        {
            if (mapperType == null)
                throw new ArgumentNullException("MapperType");

            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                (?<=[^A-Z])(?=[A-Z]) |
                (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            var mapperFullName = mapperType.FullName.Split('.');

            return r.Split(mapperFullName[mapperFullName.Length - 1])
                .First()
                .ToUpper();
        }
    }
}