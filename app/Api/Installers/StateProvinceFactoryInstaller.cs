﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Lookups.Addresses;
using iPlatform.PolicyEngine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Installers
{
    public class StateProvinceFactoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var assembly = typeof(ZAStateProvinceMapper).Assembly;

            var mappertoInstall = (from type in assembly.GetTypes()
                                   where typeof(IMapToStateProvince).IsAssignableFrom(type)
                                        select new { Type = type });


            foreach (var mapper in mappertoInstall)
            {
                container.Register(
                    Component.For(mapper.Type)
                        .ImplementedBy(mapper.Type)
                        .LifeStyle.Transient.Named(ConvertToMapperName(mapper.Type))
                    );
            }
        }

        private static string ConvertToMapperName(Type mapperType)
        {
            if (mapperType == null)
                throw new ArgumentNullException("MapperType");

            var mapperFullName = mapperType.FullName.Split('.');

            return mapperFullName[mapperFullName.Length - 1]
                .Substring(0,2)
                .ToUpper();
        }

    }
}