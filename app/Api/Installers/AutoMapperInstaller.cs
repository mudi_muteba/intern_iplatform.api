﻿using System.Linq;
using AutoMapper;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Party.Quotes.Mapping;
using Domain.Base;
using Domain.Base.Mapping;

namespace Api.Installers
{
    public class AutoMapperInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<SMSQuoteMessageConverter>().BasedOn(typeof(ITypeConverter<,>)).WithServiceAllInterfaces().LifestyleTransient());

            RegisterIdToEntityConverters(container);

            Mapper.Configuration.ConstructServicesUsing(type =>
            {
                var r = container.Resolve(type);
                container.Release(r);
                return r;
            });
        }

        private static void RegisterIdToEntityConverters(IWindsorContainer container)
        {
            var entityTypes = typeof (Entity).Assembly.GetTypes().Where(type => type.IsSubclassOf(typeof (Entity)));
            foreach (var entityType in entityTypes)
            {
                var generic = typeof (IdToEntityConverter<>);
                var constructed = generic.MakeGenericType(entityType);

                var typeConverterType = typeof (ITypeConverter<,>);
                var typeConverterGenericType = typeConverterType.MakeGenericType(typeof (int), entityType);
                container.Register(Component.For(typeConverterGenericType).ImplementedBy(constructed).LifestyleTransient());
            }
        }

    }
}