﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using iPlatform.PolicyEngine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Installers
{
    public class PolicyServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var assembly = typeof(DefaultPolicyService).Assembly;

            var ServicetoInstall = (from type in assembly.GetTypes()
                                        where typeof(IPolicyDataService).IsAssignableFrom(type)
                                        select new { Type = type });


            foreach (var serviceType in ServicetoInstall)
            {
                container.Register(
                    Component.For(serviceType.Type)
                        .ImplementedBy(serviceType.Type)
                        .LifeStyle.Transient.Named(ConvertToServiceName(serviceType.Type))
                    );
            }
        }

        private static string ConvertToServiceName(Type serviceType)
        {
            if (serviceType == null)
                throw new ArgumentNullException("ServiceType");

            var serviceFullName = serviceType.FullName.Split('.');

            if (serviceFullName[serviceFullName.Length - 1].ToUpper().Contains("DEFAULT"))
                return "DEFAULT";


            return serviceFullName[serviceFullName.Length - 1]
                .Replace("PolicyService", string.Empty)
                .ToUpper();
        }

    }
}