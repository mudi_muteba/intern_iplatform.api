﻿using Api.Infrastructure;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using iPlatform.Api.DTOs.Base.Connector;

namespace Api.Installers
{
    public class ConnectorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.SetLifeCycle(Component
                .For<IConnector>()
                .ImplementedBy<Connector>()
                .UsingFactoryMethod(() => new Connector()));
        }
    }
}