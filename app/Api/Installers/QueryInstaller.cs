using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain;
using Domain.Base.Queries;
using Nancy.Bootstrappers.Windsor;

namespace Api.Installers
{
    public class QueryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<DomainMarker>()
                .BasedOn(typeof (IQuery))
                .WithServiceSelf()
                .LifestyleScoped<NancyPerWebRequestScopeAccessor>());

            container.Register(Classes.FromAssemblyContaining<DomainMarker>()
                .BasedOn(typeof(IStoredProcedureQuery))
                .WithServiceSelf()
                .LifestyleScoped<NancyPerWebRequestScopeAccessor>());

            container.Register(Classes.FromAssemblyContaining<DomainMarker>()
                .BasedOn(typeof (IExternalSystemQuery))
                .WithServiceSelf()
                .LifestyleScoped<NancyPerWebRequestScopeAccessor>());

            container.Register(Classes.FromAssemblyContaining<DomainMarker>()
                .BasedOn(typeof(IQueryProcedure))
                .WithServiceSelf()
                .LifestyleScoped<NancyPerWebRequestScopeAccessor>());
        }
    }
}