﻿using Api.Infrastructure;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Workflow;
using EasyNetQ;
using Workflow.Domain;
using Workflow.Messages;
using WorkflowRetryMessageHandler = Domain.Base.Workflow.WorkflowRetryMessageHandler;

namespace Api.Installers
{
    public class WorkflowInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.SetLifeCycle(Component.For<ICreateWorkflowTasks>().ImplementedBy<WorkflowTasksHandlerResolver>());
            container.SetLifeCycle(Component.For<IWorkflowBus>().ImplementedBy<WorkflowBus>());
            container.SetLifeCycle(Component.For<IWorkflowRouter>().UsingFactoryMethod(CreateWorkflowRouter));
            container.Register(Component.For<IWorkflowRetryMessageHandler>().ImplementedBy<WorkflowRetryMessageHandler>().LifestyleTransient());
            container.Register(
                Classes.FromAssemblyContaining<SendEmailWorkflowTaskFactory>()
                    .BasedOn(typeof (ICreateWorkflowTasks<>))
                    .WithServiceAllInterfaces()
                    .LifestyleTransient());

            container.SetLifeCycle(Component.For<IPublishEventWorkflowMessages>().ImplementedBy<EventWorkflowMessagePublisher>());
        }

        private IWorkflowRouter CreateWorkflowRouter(IKernel kernel)
        {
            var bus = kernel.Resolve<IBus>("RouterBus");
            kernel.ReleaseComponent(bus);
            return new WorkflowRouter(new WorkflowBus(bus));
        }
    }
}