using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using iGuide.DTOs.Connector;

namespace Api.Installers
{
    public class VehicleGuideInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IGuideConnector>().ImplementedBy<GuideConnector>().LifestyleTransient());
        }
    }
}