using System;
using FluentNHibernate.Automapping;
using FluentNHibernate.Conventions.Helpers;
using Infrastructure.NHibernate.Attributes;
using Infrastructure.NHibernate.Convensions;
using MasterData;

namespace Api.Installers
{
    public class MasterDataConfiguration : DefaultAutomappingConfiguration
    {
        public override bool ShouldMap(Type type)
        {
            var baseType = typeof(BaseEntity);

            if (type.IsAbstract && type != baseType)
                return false;

            return baseType.IsAssignableFrom(type);
        }

        public AutoPersistenceModel GetAutoPersistenceModel()
        {
            return AutoMap.AssemblyOf<Title>(this)
                .Conventions.AddFromAssemblyOf<TableNameConvention>()
                //.Conventions.AddFromAssemblyOf<AccessoryTypeConvention>() // Prevents ForeignKeyReferenceConvention from being invoked
                .Conventions.Add
                (
                    ForeignKey.EndsWith("Id"),
                    DefaultCascade.SaveUpdate()
                )
                .Override(new Action<AutoMapping<StateProvince>>(mapping =>
                {
                    mapping.Table(mapping.GetType().GetGenericArguments()[0].Name);
                }))
                .OverrideAll(x =>
                {
                    x.IgnoreProperties(member => member.MemberInfo.GetCustomAttributes(typeof(DoNotMapAttribute), false).Length > 0);
                });
        }
    }
}