﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using ToolBox.Templating.Interfaces;
using ToolBox.Templating.NVelocity;

namespace Api.Installers
{
    public class TemplateEngineInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ITemplateEngine>().ImplementedBy<NVelocityTemplateEngine>());
        }
    }
}