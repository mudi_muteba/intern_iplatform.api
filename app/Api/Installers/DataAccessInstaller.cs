﻿using System.IO;
using System.Reflection;
using Api.Infrastructure;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Repository;
using FluentNHibernate.Cfg;
using Infrastructure.NHibernate.Extensions;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Event;
using NHibernate.Tool.hbm2ddl;
using Environment = System.Environment;

namespace Api.Installers
{
    public class DataAccessInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.SetLifeCycle(Component.For<Configuration>().UsingFactoryMethod(() =>
            {
                var configure = Configure(container);
                return configure;
            }).LifestyleSingleton());
            
            container.SetLifeCycle(Component.For<IRepository>().ImplementedBy<NHibernateRepository>());

            container.Register(Component.For<ISessionFactory>()
                .UsingFactoryMethod(kernel =>
                {
                    var configuration = container.Resolve<Configuration>();
                    container.Release(configuration);
                    return configuration.BuildSessionFactory();
                })
                .LifestyleSingleton());

            RegisterSession(container);

            container.SetLifeCycle(Component.For<IPaginateResults>().ImplementedBy<NHibernatePaginator>());
        }

        protected virtual void RegisterSession(IWindsorContainer container)
        {
            container.SetLifeCycle(Component.For<ISession>()
                .UsingFactoryMethod(kernel =>
                {
                    var sessionFactory = kernel.Resolve<ISessionFactory>();
                    container.Release(sessionFactory);
                    return sessionFactory.OpenSession();
                })
                .OnDestroy(s =>
                {
                    if (s.IsOpen)
                    s.Close();
                    s.Dispose();
                }));
        }

        private Configuration Configure(IWindsorContainer container)
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            var configurationFileLocation = Path.Combine(path, @"Configuration\hibernate.cfg.xml");
            var configuration = new Configuration().Configure(configurationFileLocation);
            //configuration.BeforeBindMapping += configure_BeforeBindMapping;
            //configuration.AfterBindMapping += configure_AfterBindMapping;

			var storedProcedureDefinitionsLocation = Path.Combine(path, "Configuration/mapping.hbm.xml");
            configuration.AddFile(storedProcedureDefinitionsLocation);
#if DEBUG
            var key = string.Format("{0}-{1}", Environment.MachineName, "iBrokerConnectionString");
            configuration.SetProperty("connection.connection_string_name", key);
#endif
            OverrideDataBaseConnection(configuration);

            var fluentConfiguration = Fluently.Configure(configuration)
                .Mappings(cfg =>
                {
                    cfg.AutoMappings.Add(new EntityConfiguration().GetAutoPersistenceModel());
                    cfg.AutoMappings.Add(new MasterDataConfiguration().GetAutoPersistenceModel());
                }).ExposeConfiguration(cfg =>
                {
                    SchemaMetadataUpdater.QuoteTableAndColumns(cfg);
                    //cfg.EventListeners.PostUpdateEventListeners = new IPostUpdateEventListener[] { new AuditUpdateListener() };
                    cfg.AddAllListenersOfType<IPostInsertEventListener>(ListenerType.PostInsert, container);
                    cfg.AddAllListenersOfType<IPostUpdateEventListener>(ListenerType.PostUpdate, container);
                }).BuildConfiguration();

            return fluentConfiguration;
        }

        protected virtual void OverrideDataBaseConnection(Configuration configuration)
        {
            
        }

        static void configure_AfterBindMapping(object sender, BindMappingEventArgs e)
        {
            // inspect your mapping here
        }

        static void configure_BeforeBindMapping(object sender, BindMappingEventArgs e)
        {
            //if (Test)
                //e.Mapping.schema = "dbo";
            //else
            //    e.Mapping.schema = "Test.dbo";
        }
    }
}