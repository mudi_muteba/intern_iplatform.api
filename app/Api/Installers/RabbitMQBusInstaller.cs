using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using EasyNetQ;
using Workflow.Publisher;

namespace Api.Installers
{
    public class RabbitMQBusInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var bus = BusBuilder.CreateRouterBus();

            container.Register(Component.For<IBus>().Instance(bus).LifestyleSingleton().Named("RouterBus"));
        }
    }
}