using Api.Infrastructure;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain;
using Domain.Base.Events;
using Domain.Base.Workflow;
using Nancy.Bootstrappers.Windsor;

namespace Api.Installers
{
    public class EventHandlingInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.SetLifeCycle(Component.For<IPublishEvents>()
                .ImplementedBy<InMemoryEventPublisher>()
                .Named("InMemoryEventPublisher")
                .DependsOn(Dependency.OnComponent("DelayedEventPublisher", typeof(EventWorkflowMessagePublisher))));

            //container.SetLifeCycle(Component.For<IPublishEvents>()
            //    .ImplementedBy<DelayedEventPublisher>()
            //    .Named("DelayedEventPublisher")
            //    .DependsOn(Dependency.OnComponent("WorkflowPublisher", typeof(EventWorkflowMessagePublisher))));

            container.SetLifeCycle(Component.For<IPublishEvents>()
                .ImplementedBy<EventWorkflowMessagePublisher>()
                .Named("WorkflowPublisher"));

            container.SetLifeCycle(Component.For<ICreateEventHandlers>()
                    .UsingFactoryMethod(() => new WindsorEventHandlerFactory(container)));

            container.Register(Classes.FromAssemblyContaining<DomainMarker>()
                .BasedOn(typeof (IHandleEvent<>))
                .WithServiceAllInterfaces()
                .LifestyleScoped<NancyPerWebRequestScopeAccessor>());
        }
    }
}