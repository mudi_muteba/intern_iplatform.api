﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Ratings.Engines;
using Domain.Ratings.Engines.Acme;
using Domain.Ratings.Engines.iRate;
using Domain.Ratings.Engines.RatingRequestProcessors;

namespace Api.Installers
{
    public class RatingInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IProcessRatingResult>().ImplementedBy<RatingResultPopulateAboutProduct>().LifestyleTransient());
            container.Register(Component.For<IProcessRatingResult>().ImplementedBy<RatingResultWorkflowTrigger>().LifestyleTransient());
            container.Register(Component.For<IProcessRatingResult>().ImplementedBy<RatingFailureHandler>().LifestyleTransient());
            container.Register(Component.For<IProcessRatingResult>().ImplementedBy<RatingResultHandler>().LifestyleTransient());

            /* irate */
            //container.Register(Component.For<IRatingEngine>().ImplementedBy<AcmeRatingEngine>());
            container.Register(Component.For<IRatingEngine>().ImplementedBy<RatingEngine>());
            container.Register(Component.For<IIRateProvider>().ImplementedBy<RatingProvider>());
        }
    }
}