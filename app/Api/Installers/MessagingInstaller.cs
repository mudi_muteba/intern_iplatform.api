﻿using Api.Infrastructure;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Messaging;

namespace Api.Installers
{
    public class MessagingInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.SetLifeCycle(Component.For<ISendMessages>().ImplementedBy<RhinoServiceBusMessagePublisher>());
        }
    }
}