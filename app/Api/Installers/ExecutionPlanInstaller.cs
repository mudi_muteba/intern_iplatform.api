using Api.Infrastructure;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Execution;

namespace Api.Installers
{
    public class ExecutionPlanInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.SetLifeCycle(Component.For<IExecutionPlan>().ImplementedBy<ExecutionPlan>());
        }
    }
}