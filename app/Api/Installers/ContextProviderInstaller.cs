using Api.Infrastructure;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Execution;

namespace Api.Installers
{
    public class ContextProviderInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.SetLifeCycle(Component.For<ICreateContext>()
                .ImplementedBy<DefaultContextCreator>());

            container.SetLifeCycle(Component.For<IProvideContext>()
                .ImplementedBy<DefaultContextProvider>());
        }
    }
}