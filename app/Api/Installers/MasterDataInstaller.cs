﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MasterData;

namespace Api.Installers
{
    public class MasterDataInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<MasterDataMarker>().BasedOn<iMasterDataCollection>().WithServiceAllInterfaces().LifestyleTransient());
        }
    }
}