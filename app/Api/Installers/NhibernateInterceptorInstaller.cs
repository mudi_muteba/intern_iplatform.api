﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.AuditLogs;
using NHibernate.Event;

namespace Api.Installers
{
    public class NhibernateInterceptorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IPostInsertEventListener, IPostUpdateEventListener>().ImplementedBy<AuditUpdateListener>());
        }
    }
}