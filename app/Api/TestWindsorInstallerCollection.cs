﻿using Api.Installers;
using Castle.MicroKernel.Registration;

namespace Api
{
    public class TestWindsorInstallerCollection
    {
        public IWindsorInstaller[] Installers
        {
            get
            {
                AutoMapperConfiguration.Configure();

                return new IWindsorInstaller[]
                {
                    new RatingInstaller(), 
                    new ContainerInstaller(), 
                    new EventHandlingInstaller(),
                    new MessagingInstaller(),
                    new ExecutionPlanInstaller(),
                    new ValidatorsInstaller(),
                    new HandlersInstaller(),
                    new QueryInstaller(),
                    new ContextProviderInstaller(),
                    new WorkflowInstaller(),
                    new AutoMapperInstaller(), 
                    new WorkflowQuoteAcceptanceInstallers(), 
                };
            }
        }
    }
}