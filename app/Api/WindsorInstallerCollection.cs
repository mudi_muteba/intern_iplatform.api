﻿using Api.Installers;
using Castle.MicroKernel.Registration;

namespace Api
{
    public class WindsorInstallerCollection
    {
        public IWindsorInstaller[] Installers
        {
            get
            {
                return new IWindsorInstaller[]
                {
                    new RatingInstaller(),
                    new EventHandlingInstaller(),
                    new MessagingInstaller(),
                    new ExecutionPlanInstaller(),
                    new ValidatorsInstaller(),
                    new HandlersInstaller(),
                    new QueryInstaller(),
                    new ContextProviderInstaller(),
                    new WorkflowInstaller(), 
                    new RabbitMQBusInstaller(), 
                    new VehicleGuideInstaller(), 
                    new PersonLookupInstaller(), 
                    new AutoMapperInstaller(), 
                    new WorkflowQuoteAcceptanceInstallers(),
                    new PolicyServiceInstaller(),
                    new StateProvinceFactoryInstaller(),
                    new SecondLevelQuestionsFactoryInstaller(), 
					new MasterDataInstaller(),
                    new ServiceLocatorInstaller(),
                    new TemplateEngineInstaller()
                };
            }
        }
    }
}