namespace TestHelper
{
    public enum ConnectionType
    {
        File,
        InMemory,
        InMemoryShared,
        SqlServer
    }
}