﻿using System;
using System.Linq;
using Api.Infrastructure;
using Domain.Users.Authentication;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Enums;
using TestObjects.Mothers.Users;

namespace TestHelper
{
    public class ApiUserObjectMother
    {
        public static ApiUser AsAdmin()
        {
            //Tests are failing:
            //Users Authorisation groups not being pulled back after Extract and therefore the user has no 
            //authorisation to run the tests
            //var token = new TokenGenerator().CreateToken(UserObjectMother.AdminUser(), ApiRequestType.Api, 1);



            //return new ApiUserBuilder()
            //    .ExtractToken(token);

            var adminUser = UserObjectMother.AdminUser();
            var apiUser = new ApiUser(adminUser.Id, ApiRequestType.Api, 1, Guid.NewGuid(), CultureParameter.CreateDefault());
            apiUser.Set(adminUser.Id, "Test", "Test", adminUser.ChannelIds.ToList(),
                adminUser.AuthorisationGroups.ToList());
            return apiUser;
        }
    }
}