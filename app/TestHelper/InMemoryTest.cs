﻿using NHibernate;
using NHibernate.Cfg;
using TestHelper.Helpers.Database;

namespace TestHelper
{
    public abstract class InMemoryTest : ApiTest
    {
        protected InMemoryTest(bool useSingleSession = false) : base(useSingleSession)
        {
            var configuration = Container.Resolve<Configuration>();
            currentConnectionString = "Data Source=iplatform.sqlite3;Version=3";

            configuration.SetProperty("connection.provider", "NHibernate.Connection.DriverConnectionProvider");
            configuration.SetProperty("connection.driver_class", "NHibernate.Driver.SQLite20Driver");
            configuration.SetProperty("dialect", "NHibernate.Dialect.SQLiteDialect");
            configuration.SetProperty("connection.connection_string", currentConnectionString);
            configuration.SetProperty("connection.release_mode", "on_close");
        }

        public override void RefreshDb()
        {
            var databaseRebuilder = new RebuildSqliteDatabase();
            databaseRebuilder.Rebuild(currentConnectionString);

            Session = Container.Resolve<ISessionFactory>().OpenSession();
            var configuration = Container.Resolve<Configuration>();
            configuration.SetProperty("connection.connection_string_name", currentConnectionString);
        }
    }
}