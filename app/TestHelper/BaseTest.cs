﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using Api.Infrastructure;
using Api.Installers;
using AutoMapper;
using Castle.Core;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Domain.Admin;
using Domain.Base;
using Domain.Base.Execution;
using Domain.Base.Repository;
using EasyNetQ;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Connector;
using MasterData;
using MasterData.Authorisation;
using MasterData.Authorisation.Groups;
using Nancy.Hosting.Self;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using TestHelper.Helpers;
using TestHelper.Helpers.Extensions;
using TestHelper.Installers;
using TestObjects.Mothers.Context;
using TestObjects.Mothers.Users;
using Xunit.Extensions;
using log4net;

namespace TestHelper
{
    public abstract class BaseTest : Specification, IDisposable
    {
        private readonly ApiUser _user;
        private bool _isSqlite;
        public bool CreateBlankDb, EnableAuditLog;
        public Connector Connector = new Connector();
        protected IExecutionPlan ExecutionPlan; 
        protected ISession Session;
        protected IRepository Repository; // todo: remove
        protected readonly WindsorContainer Container = new WindsorContainer();
        private readonly NancyHost _nancy;
        private IDbConnection _testConnection = TestConnection.Connection(ConnectionType.InMemory);
        protected readonly TestData _testData = new TestData();
        readonly TestBootstrapper _testBootstrapper;
        private static readonly ILog _Log = LogManager.GetLogger(typeof(BaseTest));

        /// <summary>
        /// Used for PersistenceSpecification tests
        /// </summary>
        /// <param name="createBlankDb"></param>
        protected BaseTest(bool createBlankDb = true, bool enableAuditLog = false)
        {
            CreateBlankDb = createBlankDb;
            EnableAuditLog = enableAuditLog;
            Install();
        }

        /// <summary>
        /// Used for API tests
        /// </summary>
        /// <param name="user"></param>
        protected BaseTest(ApiUser user)
        {
            Install();

            _testBootstrapper = new TestBootstrapper(Session);
            _nancy = new NancyHost(new Uri("http://localhost:2121"), _testBootstrapper);
            _nancy.Start();

            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);
        }
        protected BaseTest(ApiUser user, ConnectionType connectionType)
        {
            _user = user;
            InstallWithMssql();

            _testBootstrapper = new TestBootstrapper(Session);
            _nancy = new NancyHost(new Uri("http://localhost:2121"), _testBootstrapper);
            _nancy.Start();

            var token = Connector.Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token.Token);
        }

        /// <summary>
        /// Please decorate with [Observation]
        /// </summary>
        protected abstract void assert_all();

        private void Install()
        {
            _isSqlite = _testConnection is SQLiteConnection;
            _testConnection.Open();

            InstallAuditLogDependancies();
                
            Container.Install(new TestDataAccessInstaller(_isSqlite, _testConnection), new MasterDataInstaller());
            Container.Kernel.ComponentModelCreated += OverrideContainerLifestyle;
            Container.BeginScope();

            RebuildDb();
        }
        private void InstallWithMssql()
        {
            _testConnection = TestConnection.Connection(ConnectionType.SqlServer);
            _isSqlite = false;
            _testConnection.Open();

            Container.Install(new TestDataAccessInstaller(_isSqlite, _testConnection), new MasterDataInstaller());
            Container.Kernel.ComponentModelCreated += OverrideContainerLifestyle;
            Container.BeginScope();

            Session = Container.Resolve<ISession>();
            Container.Release(Session);
        }

        /// <summary>
        /// This is only required for tests that assert logging info to the AuditLog table
        /// </summary>
        private void InstallAuditLogDependancies()
        {
            if (EnableAuditLog)
            {
                var context = new ContextObjectMother()
                    .WithAuthorisation(new ChannelAuthorisation(1, new CallCentreAgent().Authorisation.ToArray()))
                    .WithAuthorisation(new ChannelAuthorisation(2, new CallCentreAgent().Authorisation.ToArray()))
                    .Build();

                var contextProvider = new DefaultContextProvider();
                contextProvider.SetContext(context);

                Container.Register(Component.For<IProvideContext>().Instance(contextProvider));
                Container.Install(new NhibernateInterceptorInstaller());
            }
        }

        private void RebuildDb()
        {
            Session = CreateSchema();
            if (CreateBlankDb)
                return;
            ReCreateVersionTable();
            CreateMasterDataSchema();
            GenerateMasterData();
            AddTestData(_testData.Channels);
            AddTestData(_testData.Organizations, _testData.Users, _testData.UserChannels, _testData.UserAuthorisationGroups, _testData.Products, _testData.CoverDefinitions);
            AddTestData(_testData.QuestionDefinitions, _testData.MapVapQuestionDefinitions);
        }

        private ISession CreateSchema()
        {
            var session = Container.Resolve<ISession>();
            var configuration = Container.Resolve<Configuration>();
            Container.Release(Session);
            Container.Release(configuration);

            if (!_isSqlite)
            {
                new SchemaExport(configuration).Drop(true, true);
                new SchemaExport(configuration).Create(true, true);
            }
            else
                new SchemaExport(configuration).Execute(false, true, false, session.Connection, null);

            return session;
        }        

        private void ReCreateVersionTable()
        {
            if (_isSqlite) return;
            var sql = string.Format("IF EXISTS (SELECT * FROM sys.tables WHERE name = '{0}') BEGIN EXEC('DROP TABLE [{0}]') END", "_Version_iBroker");
            Session.CreateSQLQuery(sql).ExecuteUpdate();
        }

        private void CreateMasterDataSchema()
        {
            if (_isSqlite) return;
            var sql = string.Format("IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = '{0}') BEGIN EXEC('CREATE SCHEMA [{0}]') END", "md");
            Session.CreateSQLQuery(sql).ExecuteUpdate();
        }

        private void GenerateMasterData()
        {
            var collections = Container.ResolveAll<iMasterDataCollection>();
            foreach (var collection in collections)
                MasterDataGenerator.MasterDataGenerator.Generate(Session, collection as IReadOnlyCollection<BaseEntity>, _isSqlite);
        }

        private void AddTestData(params IReadOnlyCollection<Entity>[] collections)
        {
            var entities = collections.SelectMany(collection => collection).ToArray();
            SaveAndFlush(entities);
        }

        private void OverrideContainerLifestyle(ComponentModel model)
        {
            if (model.LifestyleType == LifestyleType.Undefined || model.LifestyleType == LifestyleType.PerWebRequest || model.LifestyleType == LifestyleType.Scoped)
                model.LifestyleType = LifestyleType.Transient;
        }

        protected void SaveAndFlush(params object[] objects)
        {
            SaveAndFlush(Session, objects);
        }

        protected void SaveAndFlush(ISession session, params object[] objects)
        {
            try
            {
                foreach (var o in objects)
                    session.SaveOrUpdate(o);

                session.Flush();
            }
            catch (Exception ex)
            {
                _Log.Error(String.Format("An error occurred whilst trying to Save the object or whilst trying to flush the session. Message: ", ex.Message), ex);
                throw;
            }

        }


        protected void Transaction(Action<ISession> action)
        {
            try
            {
                Session.EncloseInTransaction(action);
            }
            catch (Exception ex)
            {
                if (ex is ObjectDisposedException && (ex as ObjectDisposedException).ObjectName == "AdoTransaction")
                    throw new Exception("System doesn't support nested transactions. Ensure that a transaction is not currently running.", ex);
                throw;
            }
        }

        ~BaseTest()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_testBootstrapper != null)
            {
                var bus = _testBootstrapper.Container.Resolve<IBus>();
                _testBootstrapper.Container.Release(bus);
                bus.Dispose();
            }

            _testData.Dispose();

            if (Session != null)
            {
                Session.Close();
                Session.Dispose();
                Session = null;
            }

            if (_testConnection.GetType().FullName != "System.Data.SqlClient.SqlConnection")
            {
                _testConnection.Close();
                _testConnection.Dispose();
            }

            Container.Release(ExecutionPlan);
            Container.Dispose();

            DisposeNancy();

            Helpers.Engine.Stop();

            GC.SuppressFinalize(this);
        }

        private void DisposeNancy()
        {
            if (_nancy == null) return;
            _nancy.Stop();
            _nancy.Dispose();
        }
    }
}