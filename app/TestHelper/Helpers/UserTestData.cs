using System.Diagnostics;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Users;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Individual.ProposalHeader;
using TestObjects.Mothers.Users;

namespace TestHelper.Helpers
{
    public class ProposalHeaderTestData
    {
        private readonly Connector connector;

        public ProposalHeaderTestData(Connector connector)
        {
            this.connector = connector;
        }

        public int Create(int individualId)
        {
            return Create(NewCreateProposalHeaderDtoObjectMother.ValidCreateProposalHeaderDto(), individualId);
        }

        public int Create(CreateProposalHeaderDto dtos, int individualId)
        {
            POSTResponseDto<int> response =
                connector.IndividualManagement.Individual(individualId).Proposals().CreateProposalHeader(dtos);

            Debug.Assert(response.StatusCode == HttpStatusCode.OK);

            return response.Response;
        }
    }

    public class IndividualTestData
    {
        private readonly Connector connector;

        public IndividualTestData(Connector connector)
        {
            this.connector = connector;
        }

        public int Create()
        {
            return Create(NewIndividualDtoObjectMother.ValidIndividualDto());
        }

        public int CreateAddress(int individualId)
        {
            return CreateAddress(NewIndividualDtoObjectMother.ValidAddressDto(individualId));
        }


        private int CreateAddress(CreateAddressDto validAddressDto)
        {
            POSTResponseDto<int> response =
                connector.IndividualManagement.Individual(validAddressDto.PartyId).CreateAddress(validAddressDto);

            Debug.Assert(response.StatusCode == HttpStatusCode.OK);

            return response.Response;
        }

        public int Create(CreateIndividualDto dtos)
        {
            POSTResponseDto<int> response = connector.IndividualManagement.Individuals.CreateIndividual(dtos);

            Debug.Assert(response.StatusCode == HttpStatusCode.OK);

            return response.Response;
        }
    }

    public class UserTestData
    {
        private readonly Connector connector;

        public UserTestData(Connector connector)
        {
            this.connector = connector;
        }

        public int CreateUser(CreateUserDto user)
        {
            POSTResponseDto<int> response = connector.UserManagement.Users.CreateUser(user);

            Debug.Assert(response.StatusCode == HttpStatusCode.OK);

            return response.Response;
        }

        public RequestPasswordResetResponseDto RequestPasswordReset(CreateUserDto validNewUser)
        {
            var userId = CreateUser(validNewUser);

            var response = connector.UserManagement.Users.RequestPasswordReset(validNewUser.UserName);

            Debug.Assert(response.StatusCode == HttpStatusCode.OK);

            return response.Response;
        }

        public int Create()
        {
            CreateUserDto validCreateUser = UserDtoObjectMother.ValidApprovedNewUser();
            int id = CreateUser(validCreateUser);
            return id;
        }
    }
}