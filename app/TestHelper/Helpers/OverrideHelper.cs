﻿using Castle.Core;
using Castle.Windsor;
using NHibernate.Cfg;

namespace TestHelper.Helpers
{
    public class OverrideHelper
    {
        public static void OverrideNhibernateSessionLifestyle(ComponentModel model)
        {
            if (model.ComponentName.Name.Equals("Late bound NHibernate.ISession"))
                model.LifestyleType = LifestyleType.Singleton;
        }
        public static void OverrideNhibernateCfg(IWindsorContainer container)
        {
            var configuration = container.Resolve<Configuration>();
            container.Release(configuration);
            configuration.SetProperty("cache.provider_class", "NHibernate.Cache.NoCacheProvider, NHibernate.Cache");
            configuration.SetProperty("cache.use_second_level_cache", "false");
            configuration.SetProperty("cache.use_query_cache", "false");
            configuration.SetProperty("current_session_context_class", "thread_static");
            configuration.SetProperty("show_sql", "true");
        }
    }
}