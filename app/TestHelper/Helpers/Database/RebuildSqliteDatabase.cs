﻿using System;
using System.IO;
using System.Reflection;
using Database;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SQLite;

namespace TestHelper.Helpers.Database
{
    public class RebuildSqliteDatabase
    {
        public void Rebuild(string connectionString)
        {
            CopyDatabase();

            var assembly = typeof (DatabaseMarker).Assembly;

            var runner = CreateProfileRunner(assembly, connectionString);

            runner.MigrateUp();
        }

        private void CopyDatabase()
        {
            var templateDbFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "template_iplatform.sqlite3");
            var activeDbFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "iplatform.sqlite3");

            File.Copy(templateDbFile, activeDbFile, true);
        }

        private MigrationRunner CreateProfileRunner(Assembly migrationAssembly, string connectionString)
        {
            var announcer = new ConsoleAnnouncer();

            var factory = new SqliteProcessorFactory();
            var processor = factory.Create(connectionString,
                announcer,
                new ProcessorOptions());

            var context = new RunnerContext(announcer);
            var runner = new MigrationRunner(migrationAssembly, context, processor);

            return runner;
        }
    }
}