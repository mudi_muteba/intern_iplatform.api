﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Castle.Windsor;
using Domain.Admin;
using Domain.Base;
using Domain.Campaigns;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;
using FluentMigrator.Runner.Processors.SQLite;
using MasterData;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using TestHelper.TestEntities;

namespace TestHelper.Helpers.Database
{
    public class RebuildSqlServerDatabase
    {
        readonly IWindsorContainer _container;
        ISession _session;
        readonly bool _useInMemoryDb;

        public RebuildSqlServerDatabase(WindsorContainer container, ISession session, bool useInMemoryDataBase)
        {
            _container = container;
            _useInMemoryDb = useInMemoryDataBase;
            _session = session;
        }
        
        private void InsertDefaultChannel()
        {
            var sql = "INSERT INTO Channel (SystemId, IsActive, ActivatedOn, IsDefault, CurrencyId, DateFormat, LanguageId) VALUES(:SystemId, 1, :ActivatedOn, 0, 1, :DateFormat, 1)";
            _session.CreateSQLQuery(sql)
                .SetParameter("SystemId", "16f67549-b713-4558-a38d-a399bee346f0")
                .SetParameter("ActivatedOn", DateTime.UtcNow + "")
                .SetParameter("DateFormat", "dd MMM yyyy")
                .ExecuteUpdate();
        }

        private void AddTestData(Assembly assembly, ISession session, bool useInMemoryDataBase)
        {
            var runner = CreateProfileRunner(assembly, session.Connection.ConnectionString, "Development");
            runner.MigrateUp(true);

            var campaign = session.Get("Campaign", 1);
            ((Campaign)campaign).SetChannel((Channel)session.Get("Channel", 1)); 
            session.Update(campaign);
            session.Flush();
        }

        private void MigrateDatabaseToLatestVersion(string connectionString, Assembly assembly)
        {
            var runner = CreateProfileRunner(assembly, connectionString);
            
            runner.MigrateUp(true);
        }

        public MigrationRunner CreateProfileRunner(Assembly migrationAssembly, string connectionString, string profile = "")
        {
            var announcer = string.IsNullOrWhiteSpace(profile)
                ? (IAnnouncer) new NullAnnouncer()
                : new ConsoleAnnouncer();
            
            var factory = new SqlServer2008ProcessorFactory();
            var processor = factory.Create(connectionString,
                announcer,
                new ProcessorOptions());

            var context = new RunnerContext(announcer);
            var runner = new MigrationRunner(migrationAssembly, context, processor);

            if (!string.IsNullOrWhiteSpace(profile))
            {
                context.Profile = profile;
                runner.ProfileLoader = new ProfileLoader(context, runner, new MigrationConventions());
            }

            return runner;
        }
    }
}