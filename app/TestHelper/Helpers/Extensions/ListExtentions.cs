﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestHelper.Helpers.Extensions
{
    public static class ListExtentions
    {
        public static bool IsEqual<T>(this IEnumerable<T> one, IEnumerable<T> two, Func<T, T, bool> comparison)
        {
            var oneList = one.ToList();
            var twoList = two.ToList();

            var equal = oneList.Count() == twoList.Count();

            foreach (var oneItem in oneList)
            {
                equal &= twoList.Any(t => comparison(oneItem, t));
            }

            foreach (var twoItem in twoList)
            {
                equal &= oneList.Any(t => comparison(twoItem, t));
            }

            return equal;
        }

        public static bool ContainsAll<TOne, TTwo>(this List<TOne> source, List<TTwo> target,
            Func<TOne, TTwo, bool> compare)
        {
            var all = source.Count == target.Count;

            return source.Aggregate(all, (current, one) => current & target.Any(t => compare(one, t)));
        }

        public static bool IsEqual<TOne, TTwo>(this IEnumerable<TOne> one, IEnumerable<TTwo> two, Func<TOne, TTwo, bool> comparison)
        {
            var oneList = one.ToList();
            var twoList = two.ToList();

            var equal = oneList.Count() == twoList.Count();

            foreach (var oneItem in oneList)
            {
                equal &= twoList.Any(t => comparison(oneItem, t));
            }

            foreach (var twoItem in twoList)
            {
                equal &= oneList.Any(t => comparison(t, twoItem));
            }

            return equal;
        }

    }
}