﻿using System;
using NHibernate;
using log4net;

namespace TestHelper.Helpers.Extensions
{
    public static class SessionExtensions
    {
        private static readonly ILog _Log = LogManager.GetLogger(typeof(SessionExtensions));

        public static void EncloseInTransaction(this ISession session, Action<ISession> work)
        {
            var tx = session.BeginTransaction();
            try
            {
                work(session);
                tx.Commit();
                tx.Dispose();
            }
            catch (Exception ex)
            {
                _Log.Error(String.Format("An error occurred whilst trying to enclose in transaction. Message: ", ex.Message), ex);
                tx.Rollback();
                throw;
            }
        }
    }
}