﻿using System;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using Workflow.Infrastructure;

namespace TestHelper.Helpers
{
    public class Workflow
    {
        public void Start()
        {
            Start(ConfigurationReader.Engine.ServiceName);

            Thread.Sleep(1000);
        }

        public void Stop()
        {
            Stop(ConfigurationReader.Engine.ServiceName);
        }

        public static void Start(string serviceName)
        {
            var matchingService = ServiceController.GetServices(".").FirstOrDefault(s => s.ServiceName.Equals(serviceName, StringComparison.OrdinalIgnoreCase));

            Console.WriteLine("Starting service {0}", serviceName);
            if (matchingService == null)
            {
                Console.WriteLine("Failed to find a service called {0} on the local computer", serviceName);
                return;
            }

            ManipulateService(matchingService,
                serviceName,
                sc => sc.Status != ServiceControllerStatus.Running && sc.Status != ServiceControllerStatus.StartPending,
                sc => sc.Start(),
                sc => sc.Status != ServiceControllerStatus.Running
                );
        }


        public static void Stop(string serviceName)
        {
            var matchingService = ServiceController.GetServices(".").FirstOrDefault(s => s.ServiceName.Equals(serviceName, StringComparison.OrdinalIgnoreCase));

            Console.WriteLine("Stopping service {0}", serviceName);
            if (matchingService == null)
            {
                Console.WriteLine("Failed to find a service called {0} on the local computer", serviceName);
                return;
            }

            ManipulateService(matchingService,
                serviceName,
                sc =>
                    sc.Status != ServiceControllerStatus.Stopped && sc.Status != ServiceControllerStatus.StopPending,
                sc => sc.Stop(),
                sc => sc.Status != ServiceControllerStatus.Stopped
                );
        }

        private static void ManipulateService(ServiceController service,
            string serviceName,
            Func<ServiceController, bool> criteria,
            Action<ServiceController> action,
            Func<ServiceController, bool> statusCheck)
        {
            if (criteria(service))
            {
                action(service);
                var counter = 0;
                while (counter < 100 && statusCheck(service))
                {
                    Thread.Sleep(100);
                    counter++;
                }

                if (!statusCheck(service))
                {
                    Console.WriteLine("Failed to manipulate the '{0}' service", serviceName);
                    throw new Exception(string.Format("Failed to manipulate the '{0}' service", serviceName));
                }
            }
        }
    }
}