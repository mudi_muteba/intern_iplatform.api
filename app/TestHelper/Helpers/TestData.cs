using System;
using TestHelper.TestEntities;

namespace TestHelper.Helpers
{
    public class TestData : IDisposable
    {
        public Channels Channels = new Channels();
        public EmailCommunications EmailCommunications = new EmailCommunications();
        public Users Users = new Users();
        public UserChannels UserChannels = new UserChannels();
        public UserAuthorisationGroups UserAuthorisationGroups = new UserAuthorisationGroups();
        public Organizations Organizations = new Organizations();
        public Products Products = new Products();
        public CoverDefinitions CoverDefinitions = new CoverDefinitions();
        public ContactDetails ContactDetails = new ContactDetails();
        public QuestionDefinitions QuestionDefinitions = new QuestionDefinitions();
        public OverrideRatingQuestions OverrideRatingQuestions = new OverrideRatingQuestions();
        public OverrideRatingQuestionChannels OverrideRatingQuestionChannels = new OverrideRatingQuestionChannels();
        public MapVapQuestionDefinitions MapVapQuestionDefinitions = new MapVapQuestionDefinitions();
        public Covers Covers = new Covers();

        public void Dispose()
        {
            Organizations.Dispose();
            Channels.Dispose();
            Users.Dispose();
            UserChannels.Dispose();
            UserAuthorisationGroups.Dispose();
            Products.Dispose();
            CoverDefinitions.Dispose();
            ContactDetails.Dispose();
            EmailCommunications.Dispose();
            QuestionDefinitions.Dispose();
            OverrideRatingQuestions.Dispose();
            OverrideRatingQuestionChannels.Dispose();
            MapVapQuestionDefinitions.Dispose();
            Covers.Dispose();
        }
    }
}