using System.Diagnostics;
using System.IO;
using System.Threading;
using NHibernate.Util;

namespace TestHelper.Helpers
{
    public class Engine
    {
        private static Process _process;

        public static void Start()
        {
            var configuration = "debug";
#if (!DEBUG)
            configuration = "release";
#endif
            var appPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            var fileName = Path.Combine(string.Format("{0}\\Test.Engine\\bin\\{1}", appPath, configuration), "Test.Engine.exe");
            var start = new ProcessStartInfo
            {
                FileName = fileName,
                WindowStyle = ProcessWindowStyle.Normal
            };

            _process = new Process {StartInfo = start};
            _process.Start();
            Thread.Sleep(5000); // Allow for enough time for registering services etc.
        }

        public static void Stop()
        {
            var processes = Process.GetProcessesByName("iPlatform.Workflow.Engine");
            if (processes.Any())
                processes[0].Kill();
        }
    }
}