
using iPlatform.Api.DTOs.Base.Connector;

namespace TestHelper.Helpers
{
    public class ConnectorTestData
    {
        public UserTestData Users { get; private set; }
        public IndividualTestData Individuals { get; private set; }
        public ProposalHeaderTestData ProposalHeaders { get; private set; }
        public AuthenticationTestData Authentication { get; private set; }

        public ConnectorTestData(Connector connector)
        {
            if (Users == null)
                Users = new UserTestData(connector);
            if (Authentication == null)
                Authentication = new AuthenticationTestData(connector);
            if (Individuals == null)
                Individuals = new IndividualTestData(connector);
            if (ProposalHeaders == null)
                ProposalHeaders = new ProposalHeaderTestData(connector);
        }
    }
}