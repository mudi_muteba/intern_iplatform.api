using System.Diagnostics;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Users.Authentication;

namespace TestHelper.Helpers
{
    public class AuthenticationTestData
    {
        private readonly Connector connector;

        public AuthenticationTestData(Connector connector)
        {
            this.connector = connector;
        }

        public string Authenticate(AuthenticationRequestDto request)
        {
            var authenticationResponse = connector.Authentication.Authenticate(request);

            Debug.Assert(authenticationResponse.IsAuthenticated == true, string.Format("Failed authentication for '{0}' with password '{1}'", request.Email, request.Password));

            return authenticationResponse.Token;
        }
    }
}