﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Workflow;
using EasyNetQ;
using TestHelper.Stubs;
using TestObjects.Mothers.Ratings.stubs;

namespace TestHelper.Installers
{
    public class TestWorkflowInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromAssemblyContaining<QuoteAcceptanceTaskFactory1>()
                    .BasedOn(typeof (ICreateWorkflowTasks<>))
                    .WithServiceAllInterfaces()
                    .LifestyleTransient());

            container.Register(Component.For<IBus>().ImplementedBy<FakeBus>().IsDefault());
        }
    }
}