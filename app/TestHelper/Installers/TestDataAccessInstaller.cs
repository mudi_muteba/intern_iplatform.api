using System.Data;
using Api.Installers;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Infrastructure.NHibernate;
using NHibernate;
using NHibernate.Cache;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;

namespace TestHelper.Installers
{
    public class TestDataAccessInstaller : DataAccessInstaller
    {
        private readonly IDbConnection _testConnection;
        private readonly ISession _session;
        private readonly bool _isSqlite;

        public TestDataAccessInstaller(bool isSqlite, IDbConnection testConnection)
        {
            _isSqlite = isSqlite;
            _testConnection = testConnection;
        }

        public TestDataAccessInstaller(ISession session)
        {
            _session = session;
        }

        protected override void RegisterSession(IWindsorContainer container)
        {
            container.Register(_session == null
                ? Component.For<ISession>().UsingFactoryMethod(kernel =>
                {
                    var sessionFactory = kernel.Resolve<ISessionFactory>();
                    container.Release(sessionFactory);
                    return sessionFactory.OpenSession(_testConnection, new SqlDebugOutput());
                })
                : Component.For<ISession>().Instance(_session));
        }

        protected override void OverrideDataBaseConnection(Configuration configuration)
        {
            if (!_isSqlite) return;

            configuration.SetProperty(Environment.Dialect, typeof(SQLiteDialect).AssemblyQualifiedName);
            configuration.SetProperty(Environment.ConnectionDriver, typeof(SQLite20Driver).AssemblyQualifiedName);
            configuration.SetProperty(Environment.ConnectionString, _testConnection.ConnectionString);
            configuration.SetProperty(Environment.ReleaseConnections, "on_close");
            configuration.SetProperty(Environment.CacheProvider, typeof(NoCacheProvider).AssemblyQualifiedName);
            configuration.SetProperty(Environment.UseSecondLevelCache, "false");
            configuration.SetProperty(Environment.UseQueryCache, "false");
            //configuration.SetProperty(Environment.CurrentSessionContextClass, typeof(ThreadLocalSessionContext).AssemblyQualifiedName);
            configuration.SetProperty(Environment.ShowSql, "true");
            configuration.SetProperty(Environment.PrepareSql, "true");
        }
    }
}