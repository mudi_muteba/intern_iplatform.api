using System;
using System.Linq;
using AutoMapper;

namespace TestHelper.Installers
{
    public class TestAutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x => AddProfiles(Mapper.Configuration));
        }

        private static void AddProfiles(IConfiguration configuration)
        {
            var domainProfiles = typeof(TestAutoMapperConfiguration).Assembly.GetTypes().Where(x => typeof (Profile).IsAssignableFrom(x));
            foreach (var profile in domainProfiles)
            {
                configuration.AddProfile(Activator.CreateInstance(profile) as Profile);
            }
        }
    }
}