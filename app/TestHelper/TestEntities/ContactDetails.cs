using System;
using System.Collections.ObjectModel;
using Domain.Party.ContactDetails;

namespace TestHelper.TestEntities
{
    public class ContactDetails : ReadOnlyCollection<ContactDetail>, IDisposable
    {
        public static ContactDetail ContactDetail1 = new ContactDetail();

        public ContactDetails() : base(new [] { ContactDetail1 })
        {
        }

        public void Dispose()
        {
            ContactDetail1.Id = 0;
        }
    }
}