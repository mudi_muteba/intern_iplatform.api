﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData;

namespace TestHelper.TestEntities
{
    public class Countries : ReadOnlyCollection<Country>, IDisposable
    {
        public static Country Country1 = new Country() { Id = 1, Name = "Afghanistan", Code = "AF" };
        public static Country Country2 = new Country() { Id = 2, Name = "Albania", Code = "AL" };
        public static Country Country3 = new Country() { Id = 3, Name = "Algeria", Code = "DZ" };
        public static Country Country4 = new Country() { Id = 4, Name = "American Samoa", Code = "AS" };

        public Countries() : base(new[] { Country1, Country2, Country3, Country4 })
        {
        }

        public void Dispose()
        {
            Country1.Id = 1;
            Country2.Id = 2;
            Country3.Id = 3;
            Country4.Id = 4;
        }
    }
}
