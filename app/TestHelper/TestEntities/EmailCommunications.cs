﻿using System;
using System.Collections.ObjectModel;
using Domain.Admin;
using Domain.Admin.Channels.EmailCommunicationSettings;

namespace TestHelper.TestEntities
{
    public class EmailCommunications : ReadOnlyCollection<EmailCommunicationSetting>, IDisposable
    {
        public static EmailCommunicationSetting EmailCommunication1 = new EmailCommunicationSetting
        { Channel = new Channel { Id = 1}, Host = "smtp.gmail.com", Port = 587, DefaultContactNumber = "+27 11 300 1100", UserName = "iplatform@iplatform.co.za", Password = "p8nynyocRLhoCeN2wdoN8A", DefaultFrom = "iPlatform<iplatform@platform.co.za>", SubAccountID = "QuoteAcceptance", UseDefaultCredentials = false, UseSSL = true, DefaultBCC = "" };
        public static EmailCommunicationSetting EmailCommunication2 = new EmailCommunicationSetting
        { Channel = new Channel { Id = 1 }, Host = "smtp.gmail.com", Port = 587, DefaultContactNumber = "+27 11 300 1100", UserName = "iplatform@iplatform.co.za", Password = "p8nynyocRLhoCeN2wdoN8A", DefaultFrom = "iPlatform<iplatform@platform.co.za>", SubAccountID = "QuoteAcceptance", UseDefaultCredentials = false, UseSSL = true, DefaultBCC = "" };
        public static EmailCommunicationSetting EmailCommunication3 = new EmailCommunicationSetting
        { Channel = new Channel { Id = 1 }, Host = "smtp.gmail.com", Port = 587, DefaultContactNumber = "+27 11 300 1100", UserName = "iplatform@iplatform.co.za", Password = "p8nynyocRLhoCeN2wdoN8A", DefaultFrom = "iPlatform<iplatform@platform.co.za>", SubAccountID = "QuoteAcceptance", UseDefaultCredentials = false, UseSSL = true, DefaultBCC = "" };
        public static EmailCommunicationSetting EmailCommunication4 = new EmailCommunicationSetting
        { Channel = new Channel { Id = 1 }, Host = "smtp.gmail.com", Port = 587, DefaultContactNumber = "+27 11 300 1100", UserName = "iplatform@iplatform.co.za", Password = "p8nynyocRLhoCeN2wdoN8A", DefaultFrom = "iPlatform<iplatform@platform.co.za>", SubAccountID = "QuoteAcceptance", UseDefaultCredentials = false, UseSSL = true, DefaultBCC = "" };

        public EmailCommunications() : base(new[] { EmailCommunication1, EmailCommunication2, EmailCommunication3, EmailCommunication4 })
        {
        }

        public void Dispose()
        {
            EmailCommunication1.Id = 1;
            EmailCommunication2.Id = 2;
            EmailCommunication3.Id = 3;
            EmailCommunication4.Id = 4;
        }
    }
}