﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.OverrideRatingQuestion;
using Domain.Products;
using Domain.QuestionDefinitions;

namespace TestHelper.TestEntities
{
    public class OverrideRatingQuestions : ReadOnlyCollection<OverrideRatingQuestion>, IDisposable
    {
        public static OverrideRatingQuestion OverrideRatingQuestion1 = new OverrideRatingQuestion() { Id = 1, CoverDefinition = new CoverDefinition() { Id = 1 }, QuestionDefinition = new QuestionDefinition() { Id = 1 }, OverrideValue = "test1" };
        public static OverrideRatingQuestion OverrideRatingQuestion2 = new OverrideRatingQuestion() { Id = 2, CoverDefinition = new CoverDefinition() { Id = 1 }, QuestionDefinition = new QuestionDefinition() { Id = 1 }, OverrideValue = "test2" };
        public static OverrideRatingQuestion OverrideRatingQuestion3 = new OverrideRatingQuestion() { Id = 3, CoverDefinition = new CoverDefinition() { Id = 1 }, QuestionDefinition = new QuestionDefinition() { Id = 1 }, OverrideValue = "test3" };
        public static OverrideRatingQuestion OverrideRatingQuestion4 = new OverrideRatingQuestion() { Id = 4, CoverDefinition = new CoverDefinition() { Id = 1 }, QuestionDefinition = new QuestionDefinition() { Id = 1 }, OverrideValue = "test4" };

        public OverrideRatingQuestions() : base(new[] { OverrideRatingQuestion1, OverrideRatingQuestion2, OverrideRatingQuestion3, OverrideRatingQuestion4 })
        {
        }

        public void Dispose()
        {
            OverrideRatingQuestion1.Id = 1;
            OverrideRatingQuestion2.Id = 2;
            OverrideRatingQuestion3.Id = 3;
            OverrideRatingQuestion4.Id = 4;
        }
    }
}
