﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData;

namespace TestHelper.TestEntities
{
    public class Covers : ReadOnlyCollection<Cover>, IDisposable
    {
        public static Cover Cover1 = new Cover() { Id = 1, Name = "Motor", Code = "MOTOR" };
        public static Cover Cover2 = new Cover() { Id = 2, Name = "House Owners", Code = "HOUSE_OWNERS" };
        public static Cover Cover3 = new Cover() { Id = 3, Name = "All Risk", Code = "ALL_RISK" };
        public static Cover Cover4 = new Cover() { Id = 4, Name = "Tyre & Rim", Code = "TYRE_RIM" };
        
        public Covers() : base(new []
        {
            Cover1,
            Cover2,
            Cover3,
            Cover4
        })
        {
        }

        public void Dispose()
        {
            Cover1.Id = 1;
            Cover2.Id = 2;
            Cover3.Id = 3;
            Cover4.Id = 4;
        }
    }
}
