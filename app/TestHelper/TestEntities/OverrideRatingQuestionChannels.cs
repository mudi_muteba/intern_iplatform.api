﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Admin;
using Domain.OverrideRatingQuestion;

namespace TestHelper.TestEntities
{
    public class OverrideRatingQuestionChannels : ReadOnlyCollection<OverrideRatingQuestionChannel>, IDisposable
    {
        public static OverrideRatingQuestionChannel OverrideRatingQuestionChannel = new OverrideRatingQuestionChannel() { Id = 1, OverrideRatingQuestion = new OverrideRatingQuestion() { Id = 1 }, Channel = new Channel() { Id = 1 } };
        public static OverrideRatingQuestionChannel OverrideRatingQuestionChanne2 = new OverrideRatingQuestionChannel() { Id = 2, OverrideRatingQuestion = new OverrideRatingQuestion() { Id = 1 }, Channel = new Channel() { Id = 2 } };
        public static OverrideRatingQuestionChannel OverrideRatingQuestionChanne3 = new OverrideRatingQuestionChannel() { Id = 3, OverrideRatingQuestion = new OverrideRatingQuestion() { Id = 2 }, Channel = new Channel() { Id = 3 } };
        public static OverrideRatingQuestionChannel OverrideRatingQuestionChanne4 = new OverrideRatingQuestionChannel() { Id = 4, OverrideRatingQuestion = new OverrideRatingQuestion() { Id = 2 }, Channel = new Channel() { Id = 4 } };

        public OverrideRatingQuestionChannels() : base(new[] { OverrideRatingQuestionChannel, OverrideRatingQuestionChanne2, OverrideRatingQuestionChanne3, OverrideRatingQuestionChanne4 })
        {
        }

        public void Dispose()
        {
            OverrideRatingQuestionChannel.Id = 1; 
            OverrideRatingQuestionChanne2.Id = 2; 
            OverrideRatingQuestionChanne3.Id = 3;
            OverrideRatingQuestionChanne4.Id = 4;
        }
    }
}
