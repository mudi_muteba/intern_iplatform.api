using System;
using System.Collections.ObjectModel;
using Domain.Users;

namespace TestHelper.TestEntities
{
    public class UserChannels : ReadOnlyCollection<UserChannel>, IDisposable
    {
        public static UserChannel UserChannel1 = new UserChannel(Users.Root, Channels.Channel1);
        public static UserChannel UserChannel2 = new UserChannel(Users.Root, Channels.Channel2);
        public static UserChannel UserChannel3 = new UserChannel(Users.Root, Channels.Channel3);

        public UserChannels() : base(new[] { UserChannel1, UserChannel2, UserChannel3 })
        {
        }

        public void Dispose()
        {
            UserChannel1.Id = 0;
            UserChannel2.Id = 0;
            UserChannel3.Id = 0;
        }
    }
}