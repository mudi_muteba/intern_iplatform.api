﻿using System;
using System.Collections.ObjectModel;
using Domain.Users;
using MasterData;

namespace TestHelper.TestEntities
{
    public class UserAuthorisationGroups : ReadOnlyCollection<UserAuthorisationGroup>, IDisposable
    {
        public static UserAuthorisationGroup RootGroup1 = new UserAuthorisationGroup(Channels.Channel1, Users.Root, AuthorisationGroups.Admin);
        public static UserAuthorisationGroup RootGroup2 = new UserAuthorisationGroup(Channels.Channel2, Users.Root, AuthorisationGroups.Admin);
        public static UserAuthorisationGroup RootGroup3 = new UserAuthorisationGroup(Channels.Channel3, Users.Root, AuthorisationGroups.Admin);

        public UserAuthorisationGroups() : base(new[] { RootGroup1, RootGroup2, RootGroup3 })
        {
        }

        public void Dispose()
        {
            RootGroup1.Id = 0;
            RootGroup2.Id = 0;
            RootGroup3.Id = 0;
        }
    }
}