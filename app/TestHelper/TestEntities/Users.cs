﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Domain.Base.Encryption;
using Domain.Users;

namespace TestHelper.TestEntities
{
    public class Users : ReadOnlyCollection<User>, IDisposable
    {
        public static User Root = new User
        {
            UserName = "root@iplatform.co.za",
            Password = PasswordHash.CreateHash("q1w2e3r4t5 ?_"),
            Status = new UserStatus {IsActive = true, IsApproved = true},
            Channels = new List<UserChannel> { UserChannels.UserChannel1, UserChannels.UserChannel2, UserChannels.UserChannel3 },
            AuthorisationGroups = new[] {UserAuthorisationGroups.RootGroup1, UserAuthorisationGroups.RootGroup2, UserAuthorisationGroups.RootGroup3 }
        };

        public Users() : base(new[] { Root })
        {
        }

        public void Dispose()
        {
            Root.Id = 0;
        }
    }
}