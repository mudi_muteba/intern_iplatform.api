﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Admin;
using Domain.Products;
using Domain.QuestionDefinitions;

namespace TestHelper.TestEntities
{
    public class MapVapQuestionDefinitions : ReadOnlyCollection<MapVapQuestionDefinition>, IDisposable
    {
        public static MapVapQuestionDefinition MapVapQuestionDefinition1 = new MapVapQuestionDefinition() { Product = new Product() { Id = 1 }, Channel = new Channel(1), QuestionDefinition = new QuestionDefinition() { Id = 1 }, Premium = 100, Enabled = true, ExternalVapProductCode = "test1", PerSectionVap = true, AnnualizedMultiplier = 1, SumInsured = 10000 };
        public static MapVapQuestionDefinition MapVapQuestionDefinition2 = new MapVapQuestionDefinition() { Product = new Product() { Id = 2 }, Channel = new Channel(2), QuestionDefinition = new QuestionDefinition() { Id = 2 }, Premium = 130, Enabled = false, ExternalVapProductCode = "test2", PerSectionVap = true, AnnualizedMultiplier = 1, SumInsured = 10000 };
        public static MapVapQuestionDefinition MapVapQuestionDefinition3 = new MapVapQuestionDefinition() { Product = new Product() { Id = 3 }, Channel = new Channel(3), QuestionDefinition = new QuestionDefinition() { Id = 3 }, Premium = 120, Enabled = true, ExternalVapProductCode = "test3", PerSectionVap = false, AnnualizedMultiplier = 6, SumInsured = 10000 };
        public static MapVapQuestionDefinition MapVapQuestionDefinition4 = new MapVapQuestionDefinition() { Product = new Product() { Id = 4 }, Channel = new Channel(4), QuestionDefinition = new QuestionDefinition() { Id = 4 }, Premium = 50, Enabled = true, ExternalVapProductCode = "test4", PerSectionVap = true, AnnualizedMultiplier = 1, SumInsured = 10000 };
        public MapVapQuestionDefinitions() : base(new[]
        {
            MapVapQuestionDefinition1,
            MapVapQuestionDefinition2,
            MapVapQuestionDefinition3,
            MapVapQuestionDefinition4
        })
        {
        }

        public void Dispose()
        {
            MapVapQuestionDefinition1.Id = 0;
            MapVapQuestionDefinition2.Id = 0;
            MapVapQuestionDefinition3.Id = 0;
            MapVapQuestionDefinition4.Id = 0;
        }
    }
}
