﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Domain.Admin;
using Domain.Admin.Channels.EmailCommunicationSettings;

namespace TestHelper.TestEntities
{
    public class Channels : ReadOnlyCollection<Channel>, IDisposable
    {
        public static Channel Channel1 = new Channel() { Id = 0, IsActive = true, SystemId = new Guid("16f67549-b713-4558-a38d-a399bee346f0"), ActivatedOn = DateTime.UtcNow, IsDefault = true, Code = "CodeA", Country = Countries.Country1, EmailCommunicationSettings = new List<EmailCommunicationSetting>() };
        public static Channel Channel2 = new Channel() { Id = 0, IsActive = true, SystemId = new Guid("6b526364-cf69-4adf-8d79-b26b5d66fab5"), ActivatedOn = DateTime.UtcNow, Code = "CodeB", Country = Countries.Country2, EmailCommunicationSettings = new List<EmailCommunicationSetting>() };
        public static Channel Channel3 = new Channel() { Id = 0, IsActive = true, SystemId = new Guid("5b3892c0-c013-44f9-920b-6122e6d0ff42"), ActivatedOn = DateTime.UtcNow, Code = "CodeC", Country = Countries.Country3, EmailCommunicationSettings = new List<EmailCommunicationSetting>() };
        public static Channel Channel4 = new Channel() { Id = 0, IsActive = true, SystemId = new Guid("d1454cb3-8f89-46d1-9aa4-e1d11a3c6774"), ActivatedOn = DateTime.UtcNow, Code = "CodeD", Country = Countries.Country4, EmailCommunicationSettings = new List<EmailCommunicationSetting>() };

        public Channels() : base(new[] { Channel1, Channel2, Channel3, Channel4 })
        {
        }

        public void Dispose()
        {
            Channel1.Id = 0;
            Channel2.Id = 0;
            Channel3.Id = 0;
            Channel4.Id = 0;
        }
    }
}