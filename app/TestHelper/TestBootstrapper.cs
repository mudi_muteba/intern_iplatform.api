﻿using Api;
using Castle.Windsor;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Serialization.JsonNet;
using NHibernate;
using TestHelper.Installers;

namespace TestHelper
{
    public class TestBootstrapper : Bootstrapper
    {
        private readonly ISession _session;
        public IWindsorContainer Container;

        public TestBootstrapper(ISession session)
        {
            _session = session;
        }

        protected override NancyInternalConfiguration InternalConfiguration
        {
            get { return NancyInternalConfiguration.WithOverrides(c => c.Serializers.Insert(0, typeof(JsonNetSerializer))); }
        }

        protected override void AddTransactionScope(IWindsorContainer container, IPipelines pipelines, NancyContext context)
        {
            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx =>
            {
                _session.Flush();
            }));
        }

        protected override void ConfigureApplicationContainer(IWindsorContainer container)
        {
            base.ConfigureApplicationContainer(container);

            Container = ApplicationContainer;
        }

        protected override void InstallDataAccessLayer(IWindsorContainer container)
        {
            container.Install(new TestDataAccessInstaller(_session));
        }
    }
}