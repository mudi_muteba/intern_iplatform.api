﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EasyNetQ;
using EasyNetQ.Consumer;
using EasyNetQ.FluentConfiguration;
using EasyNetQ.Producer;
using Workflow.Messages;

namespace TestHelper.Stubs
{
    public class FakeBus : IBus
    {
        public bool PublishWasCalled { get; private set; }
        public Dictionary<Type, int> MessageTypesPublished { get; private set; }
        public Dictionary<Type, List<IWorkflowRoutingMessage>> MessagesPublished { get; private set; }
        public Dictionary<string, Dictionary<Type, int>> TopicMesagesPublished { get; private set; }

        public FakeBus()
        {
            MessageTypesPublished = new Dictionary<Type, int>();
            TopicMesagesPublished = new Dictionary<string, Dictionary<Type, int>>();
            MessagesPublished = new Dictionary<Type, List<IWorkflowRoutingMessage>>();
        }

        public void Dispose()
        {
            
        }

        public void Publish<T>(T message) where T : class
        {
            PublishWasCalled = true;

            var messageType = message.GetType();

            if (!MessageTypesPublished.ContainsKey(messageType))
            {
                MessageTypesPublished.Add(messageType, 0);
            }

            if (!MessagesPublished.ContainsKey(messageType))
            {
                MessagesPublished.Add(messageType, new List<IWorkflowRoutingMessage>());
            }

            MessageTypesPublished[messageType]++;

            AddPublishedMessage(message);
        }

        public void Publish<T>(T message, string topic) where T : class
        {
            PublishWasCalled = true;

            var normalisedTopic = topic.ToLower();
            var messageType = message.GetType();

            if (!TopicMesagesPublished.ContainsKey(normalisedTopic))
            {
                TopicMesagesPublished.Add(normalisedTopic, new Dictionary<Type, int>());

                if (!TopicMesagesPublished[normalisedTopic].ContainsKey(messageType))
                {
                    TopicMesagesPublished[normalisedTopic].Add(messageType, 0);
                }

                TopicMesagesPublished[normalisedTopic][messageType]++;
            }

            AddPublishedMessage(message);
        }

        private void AddPublishedMessage<T>(T message)
        {
            var messageType = message.GetType();
            if (!MessagesPublished.ContainsKey(messageType))
            {
                MessagesPublished.Add(messageType, new List<IWorkflowRoutingMessage>());
            }

            MessagesPublished[messageType].Add(message as IWorkflowRoutingMessage);
        }

        public Task PublishAsync<T>(T message) where T : class
        {
            throw new NotImplementedException();
        }

        public Task PublishAsync<T>(T message, string topic) where T : class
        {
            throw new NotImplementedException();
        }

        public ISubscriptionResult Subscribe<T>(string subscriptionId, Action<T> onMessage) where T : class
        {
            throw new NotImplementedException();
        }

        public ISubscriptionResult Subscribe<T>(string subscriptionId, Action<T> onMessage,
            Action<ISubscriptionConfiguration> configure) where T : class
        {
            throw new NotImplementedException();
        }

        public ISubscriptionResult SubscribeAsync<T>(string subscriptionId, Func<T, Task> onMessage) where T : class
        {
            throw new NotImplementedException();
        }

        public ISubscriptionResult SubscribeAsync<T>(string subscriptionId, Func<T, Task> onMessage,
            Action<ISubscriptionConfiguration> configure) where T : class
        {
            throw new NotImplementedException();
        }

        public TResponse Request<TRequest, TResponse>(TRequest request) where TRequest : class where TResponse : class
        {
            throw new NotImplementedException();
        }

        public Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest request) where TRequest : class
            where TResponse : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Respond<TRequest, TResponse>(Func<TRequest, TResponse> responder) where TRequest : class
            where TResponse : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Respond<TRequest, TResponse>(Func<TRequest, TResponse> responder,
            Action<IResponderConfiguration> configure) where TRequest : class where TResponse : class
        {
            throw new NotImplementedException();
        }

        public IDisposable RespondAsync<TRequest, TResponse>(Func<TRequest, Task<TResponse>> responder)
            where TRequest : class where TResponse : class
        {
            throw new NotImplementedException();
        }

        public IDisposable RespondAsync<TRequest, TResponse>(Func<TRequest, Task<TResponse>> responder,
            Action<IResponderConfiguration> configure) where TRequest : class where TResponse : class
        {
            throw new NotImplementedException();
        }

        public void Send<T>(string queue, T message) where T : class
        {
            throw new NotImplementedException();
        }

        public Task SendAsync<T>(string queue, T message) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Receive<T>(string queue, Action<T> onMessage) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Receive<T>(string queue, Action<T> onMessage, Action<IConsumerConfiguration> configure)
            where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Receive<T>(string queue, Func<T, Task> onMessage) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Receive<T>(string queue, Func<T, Task> onMessage, Action<IConsumerConfiguration> configure)
            where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Receive(string queue, Action<IReceiveRegistration> addHandlers)
        {
            throw new NotImplementedException();
        }

        public IDisposable Receive(string queue, Action<IReceiveRegistration> addHandlers,
            Action<IConsumerConfiguration> configure)
        {
            throw new NotImplementedException();
        }

        public bool IsConnected { get; set; }
        public IAdvancedBus Advanced { get; set; }
    }
}