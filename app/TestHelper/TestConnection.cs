using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;

namespace TestHelper
{
    public class TestConnection
    {
        public const string File = @"Data Source=c:\iPlatform.db;Version=3;";
        public const string InMemory = "Data Source=:memory:;Version=3;New=True;";
        public const string InMemoryShared = "FullUri=file::memory:?cache=shared";

        public static IDbConnection Connection(ConnectionType connectionType)
        {
            if (connectionType == ConnectionType.File)
                return new SQLiteConnection(File);

            if (connectionType == ConnectionType.InMemory)
                return new SQLiteConnection(InMemory);

            if (connectionType == ConnectionType.InMemoryShared)
                return new SQLiteConnection(InMemoryShared);

            if (connectionType == ConnectionType.SqlServer)
            {
                var key = string.Format("{0}-{1}", Environment.MachineName, "iBrokerConnectionString");
                return new SqlConnection(ConfigurationManager.ConnectionStrings[key].ConnectionString);
            }

            return null;
        }
    }
}