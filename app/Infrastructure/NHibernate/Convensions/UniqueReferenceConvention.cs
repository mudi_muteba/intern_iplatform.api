﻿using System;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using Infrastructure.NHibernate.Attributes;

namespace Infrastructure.NHibernate.Convensions
{
    public class UniqueReferenceConvention : IReferenceConvention
    {
        public void Apply(IManyToOneInstance instance)
        {
            var p = instance.Property.MemberInfo;
            if (Attribute.IsDefined(p, typeof(UniqueAttribute)))
                instance.UniqueKey("UQ" + instance.EntityType.Name);
        }
    }
}