using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using MasterData;

namespace Infrastructure.NHibernate.Convensions
{
    public class PrimaryKeyConvention : IIdConvention
    {
        public void Apply(IIdentityInstance instance)
        {
            if (instance.EntityType.Namespace == typeof(MasterDataMarker).Namespace)
                instance.GeneratedBy.Assigned();

            if (instance.EntityType.Name == "AuthorisationPoint")
                instance.GeneratedBy.Assigned();

            if (instance.EntityType.Name == "AuthorisationPointCategory")
                instance.GeneratedBy.Assigned();
        }
    }
}