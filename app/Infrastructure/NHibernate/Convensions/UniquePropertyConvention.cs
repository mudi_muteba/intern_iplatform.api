using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using Infrastructure.NHibernate.Attributes;

namespace Infrastructure.NHibernate.Convensions
{
    public class UniquePropertyConvention : AttributePropertyConvention<UniqueAttribute>
    {
        protected override void Apply(UniqueAttribute attribute, IPropertyInstance instance)
        {
            instance.UniqueKey("UQ" + instance.EntityType.Name);
        }
    }
}