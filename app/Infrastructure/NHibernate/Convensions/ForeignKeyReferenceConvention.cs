﻿using System;
using System.ComponentModel.DataAnnotations;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Infrastructure.NHibernate.Convensions
{
    public class ForeignKeyReferenceConvention : IReferenceConvention
    {
        public void Apply(IManyToOneInstance instance)
        {
            if (Attribute.IsDefined(instance.Property.MemberInfo, typeof(RequiredAttribute)))
                instance.Not.Nullable();

            instance.ForeignKey(string.Format("FK_{0}_{1}_{2}Id", instance.EntityType.Name, instance.Class.Name, instance.Property.Name));
        }
    }
}