﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using MasterData;

namespace Infrastructure.NHibernate.Convensions
{
    public class SchemaConvention : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            if (instance.EntityType.Namespace == typeof(MasterDataMarker).Namespace)
                instance.Schema("md");
        }
    }
}