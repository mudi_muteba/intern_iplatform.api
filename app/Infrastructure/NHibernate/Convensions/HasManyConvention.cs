﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using MasterData;

namespace Infrastructure.NHibernate.Convensions
{
    public class HasManyConvention : IHasManyConvention
    {
        public void Apply(IOneToManyCollectionInstance instance)
        {
            if (instance.EntityType.Namespace != typeof(MasterDataMarker).Namespace)
                instance.Where("IsDeleted <> 1");
        }
    }
}