﻿using System;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using MasterData;

namespace Infrastructure.NHibernate.Convensions
{
    public class TableNameConvention : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            var name = instance.EntityType.Name
                .Replace("State", string.Empty)
                .Replace("Reference", string.Empty);

            instance.Table(name);

            if (instance.EntityType.Namespace != typeof (MasterDataMarker).Namespace && instance.EntityType.Name != "AuditLog")
                instance.Where("IsDeleted <> 1");
        }
    }
}