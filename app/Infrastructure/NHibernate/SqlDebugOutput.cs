﻿using System.Diagnostics;
using NHibernate;
using NHibernate.SqlCommand;

namespace Infrastructure.NHibernate
{
    public class SqlDebugOutput : EmptyInterceptor
    {
        public override SqlString OnPrepareStatement(SqlString sql)
        {
            Debug.WriteLine("NH: " + sql);

            return base.OnPrepareStatement(sql);
        }
    }
}