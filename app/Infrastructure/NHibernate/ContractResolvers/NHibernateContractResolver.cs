﻿using System;

using Newtonsoft.Json.Serialization;
using NHibernate.Proxy;

namespace Infrastructure.NHibernate.ContractResolvers
{
    public class NHibernateContractResolver : DefaultContractResolver
    {
        protected override JsonContract CreateContract(Type objectType)
        {
            if (typeof(INHibernateProxy).IsAssignableFrom(objectType))
                return base.CreateContract(objectType.BaseType);
            else
                return base.CreateContract(objectType);
        }
    }
}