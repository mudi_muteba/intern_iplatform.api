﻿using System.Linq;
using Castle.Windsor;
using NHibernate.Event;

namespace Infrastructure.NHibernate.Extensions
{
    public static class ConfigurationExtensions
    {
        public static void AddAllListenersOfType<T>(this global::NHibernate.Cfg.Configuration config, ListenerType type, IWindsorContainer container)
        {
            var listeners = container.ResolveAll<T>().Cast<object>();
            var enumerable = listeners as object[] ?? listeners.ToArray();
            if (!enumerable.Any())
                return;
            config.AppendListeners(type, enumerable);
        }
    }
}