﻿using System;
using System.Linq.Expressions;
using FluentNHibernate.Automapping;

namespace Infrastructure.NHibernate.Extensions
{
    public static class NHibernateAutoMappingExtensions
    {
        public static void MapForeignKey<T, T1>(this AutoMapping<T> mapping, Expression<Func<T, T1>> memberExpression)
        {
            mapping.References(memberExpression)
                   .ForeignKey(string.Format("FK_{0}_{1}_{1}Id", mapping.GetType().GetGenericArguments()[0].Name, memberExpression.ReturnType.Name));
        }
    }
}