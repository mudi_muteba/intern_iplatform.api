﻿namespace Infrastructure.Configuration
{
    public class ConnectorConfigurationReader : AbstractConfigurationReader
    {
        public string BaseUrl
        {
            get { return ReadAppSettings("iPlatform/serviceLocator/iPlatform/baseUrl"); }
        }

        public string Email
        {
            get { return ReadAppSettings("iPlatform/connector/userName"); }
        }

        public string Password
        {
            get { return ReadAppSettings("iPlatform/connector/apiKey"); }
        }
    }
}