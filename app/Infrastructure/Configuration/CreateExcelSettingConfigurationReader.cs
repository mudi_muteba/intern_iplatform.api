﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Configuration
{
    public class CreateExcelSettingConfigurationReader : AbstractConfigurationReader
    {
        public string OutputPath
        {
            get { return ReadAppSettings("CreateExcel/OutputPath"); }
        }
    }
}
