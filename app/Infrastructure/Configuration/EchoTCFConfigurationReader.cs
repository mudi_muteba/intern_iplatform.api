﻿namespace Infrastructure.Configuration
{
    public class EchoTCFConfigurationReader : AbstractConfigurationReader
    {
        public string BaseUrl
        {
            get { return ReadAppSettings("echotcf/connector/baseUrl"); }
        }


        public string Token
        {
            get { return ReadAppSettings("echotcf/connector/token"); }
        }
        public string ByPass
        {
            get { return ReadAppSettings("echotcf/connector/bypass"); }
        }

    }
}
