﻿namespace Infrastructure.Configuration
{
    public class TestingConfigurationReader : AbstractConfigurationReader
    {
        public string Queues
        {
            get { return ReadAppSettings("testing/queues"); }
        }

        public string ServiceName
        {
            get { return ReadAppSettings("testing/serviceName"); }
        }
        public string SelenoUrl
        {
            get { return ReadAppSettings("SelenoURL"); }
        }
    }
}