﻿namespace Infrastructure.Configuration
{
    public class AdminConfigurationReader : AbstractConfigurationReader
    {
        public string BaseUrl
        {
            get { return ReadAppSettings("iadmin/connector/baseUrl"); }
        }


        public string UserName
        {
            get { return ReadAppSettings("iadmin/connector/userName"); }
        }


        public string apiKey
        {
            get { return ReadAppSettings("iadmin/connector/apiKey"); }
        }

        public string AdminUsername
        {
            get { return ReadAppSettings("iadmin/connector/adminUsername"); }
        }

        public string AdminApiKey
        {
            get { return ReadAppSettings("iadmin/connector/adminApiKey"); }
        }

        public string EnvironmentTypeKey
        {
            get { return ReadAppSettings("iadmin/connector/environmentTypeKey"); }
        }

        public string AdminList
        {
            get { return ReadAppSettings("admin/users"); }
        }
    }
}
