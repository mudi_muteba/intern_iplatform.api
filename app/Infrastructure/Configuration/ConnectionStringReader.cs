﻿using System;

namespace Infrastructure.Configuration
{
    public class ConnectionStringReader : AbstractConfigurationReader
    {
        public string iPlatform
        {
            get
            {
                string key = iPlatformKey;
                #if DEBUG
                    key = string.Format("{0}-{1}",Environment.MachineName,iPlatformKey) ;
                #endif


                var machineSpecificConnectionString = ReadConnectionStringValue(key);

                if (!string.IsNullOrWhiteSpace(machineSpecificConnectionString))
                {
                    return machineSpecificConnectionString;
                }

                return ReadConnectionStringValue(iPlatformKey);
            }
        }

        public string iPlatformKey
        {
            get
            {
                return "iBrokerConnectionString";
            }
        }

        public string LocalDbConnectionString
        {
            get
            {
                return
                    "data source=localhost;Persist Security Info=False;User ID=iBroker_user; Password=iBroker_user; Initial Catalog=iPlatform";
            }
        }
    }
}