﻿namespace Infrastructure.Configuration
{
    public class iGuideConnectorConfigurationReader : AbstractConfigurationReader
    {
        public string APIKey
        {
            get { return ReadAppSettings("iPlatform/connector/iguide/apiKey"); }
        }

        public string Email
        {
            get { return ReadAppSettings("iPlatform/connector/iguide/email"); }
        }

        public string LsAutoUserId
        {
            get { return ReadAppSettings("iPlatform/connector/lsAuto/userId"); }
        }

        public string LsAutoContractId
        {
            get { return ReadAppSettings("iPlatform/connector/lsAuto/contractId"); }
        }

        public string LsAutoCustomerId
        {
            get { return ReadAppSettings("iPlatform/connector/lsAuto/customerId"); }
        }

        public string LsAutoPackageId
        {
            get { return ReadAppSettings("iPlatform/connector/lsAuto/packageId"); }
        }

        public string LsAutoUsername
        {
            get { return ReadAppSettings("iPlatform/connector/lsAuto/username"); }
        }
        public string LsAutoPassword
        {
            get { return ReadAppSettings("iPlatform/connector/lsAuto/password"); }
        }

        public string KeAutoKey
        {
            get { return ReadAppSettings("iPlatform/connector/keAuto/key"); }
        }

        public string KeAutoSecret
        {
            get { return ReadAppSettings("iPlatform/connector/keAuto/secret"); }
        }
    }
}