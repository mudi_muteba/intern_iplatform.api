﻿namespace Infrastructure.Configuration
{
    public class SecurityConfigurationReader : AbstractConfigurationReader
    {
        public string JWTSecretKey
        {
            get { return ReadAppSettings("iPlatform/security/secretKey"); }
        }
    }
}