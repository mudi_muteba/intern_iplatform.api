﻿namespace Infrastructure.Configuration
{
    public class ExternalServiceConfigurationReader : AbstractConfigurationReader
    {
        public string iRateUrl
        {
            get { return ReadAppSettings("iPlatform/serviceLocator/iRate/baseUrl"); }
        }
    }
}