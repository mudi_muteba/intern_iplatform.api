﻿namespace Infrastructure.Configuration
{
    public static class ConfigurationReader
    {
        static ConfigurationReader()
        {
            Security = new SecurityConfigurationReader();
            Connector = new ConnectorConfigurationReader();
            Connections = new ConnectionStringReader();
            Testing = new TestingConfigurationReader();
            Platform = new PlatformConfigurationReader();
            ExternalServices = new ExternalServiceConfigurationReader();
            GuideConnector = new iGuideConnectorConfigurationReader();
            Appsetting = new AppSettingConfigurationReader();
            PersonSetting = new iPersonSettingConfigurationReader();
            Admin = new AdminConfigurationReader();
            EchoTCF = new EchoTCFConfigurationReader();
            SignFlowSetting = new SignFlowSettingConfigurationReader();
            CreateExcelSetting = new CreateExcelSettingConfigurationReader();

        }

        public static iPersonSettingConfigurationReader PersonSetting { get; set; }
        public static iGuideConnectorConfigurationReader GuideConnector { get; set; }
        public static readonly ExternalServiceConfigurationReader ExternalServices;
        public static readonly PlatformConfigurationReader Platform;
        public static readonly TestingConfigurationReader Testing;
        public static readonly SecurityConfigurationReader Security;
        public static readonly ConnectorConfigurationReader Connector;
        public static readonly ConnectionStringReader Connections;
        public static readonly AppSettingConfigurationReader Appsetting;
        public static readonly EchoTCFConfigurationReader EchoTCF;
        public static readonly AdminConfigurationReader Admin;
        public static readonly SignFlowSettingConfigurationReader SignFlowSetting;
        public static readonly CreateExcelSettingConfigurationReader CreateExcelSetting;
    }
}