﻿namespace Infrastructure.Configuration
{
    public class PlatformConfigurationReader : AbstractConfigurationReader
    {
        public string BaseUrl
        {
            get { return ReadAppSettings("iPlatform/serviceLocator/iPlatform/baseUrl"); }
        }

        public string UnderwritingLeadStatus
        {
            get { return ReadAppSettings("iPlatform/leadstatus/underwriting"); }
        }

        public string InsurerCode
        {
            get { return ReadAppSettings("InsurerCode"); }
        }
        public string QuoteAcceptanceEnvironment
        {
            get { return ReadAppSettings("quoteacceptance/environment"); }
        }
        public string WorkflowEnvironment
        {
            get { return ReadAppSettings("workflow/environment"); }
        }
        public string iPlatformEnvironment
        {
            get { return ReadAppSettings("iPlatform/environment"); }
        }
    }
}