using System;
using System.Configuration;

namespace Infrastructure.Configuration
{
    public abstract class AbstractConfigurationReader
    {
        protected string ReadAppSettings(string key, bool throwOnMissing = true)
        {
            var value = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrEmpty(value) && throwOnMissing)
            {
                throw new Exception(string.Format("Could not find a value for key '{0}'", key));
            }

            return value;
        }

        public string ReadConnectionStringValue(string key, bool throwOnMissing = true)
        {
            var value = ConfigurationManager.ConnectionStrings[key];

            if (value == null && throwOnMissing)
            {
                throw new Exception(string.Format("Could not find a value for key '{0}'", key));
            }

            if (value == null)
                return string.Empty;

            return value.ConnectionString;
        }

        public ConnectionStringSettings ReadConnectionString(string key)
        {
            var value = ConfigurationManager.ConnectionStrings[key];

            if (value == null)
            {
                throw new Exception(string.Format("Could not find a value for key '{0}'", key));
            }

            return value;
        }
    }
}