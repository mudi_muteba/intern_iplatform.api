﻿namespace Infrastructure.Configuration
{
    public class SignFlowSettingConfigurationReader : AbstractConfigurationReader
    {
        public string SignFlowApiServiceUrl
        {
            get { return ReadAppSettings("SignFlow/ServiceURL"); }
        }

        public string SignFlowApiUserName
        {
            get { return ReadAppSettings("SignFlow/UserName"); }
        }

        public string SignFlowApiPassword
        {
            get { return ReadAppSettings("SignFlow/Password"); }
        }

        public string SignFlowEventReceiverSecretKey
        {
            get { return ReadAppSettings("SignFlow/EventReceiver/SecretKey"); }
        }

        public string SignFlowCompletePath
        {
            get { return ReadAppSettings("SignFlow/Storage/Completed"); }
        }
    }
}
