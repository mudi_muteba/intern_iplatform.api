﻿namespace Infrastructure.Configuration
{
    public class AppSettingConfigurationReader : AbstractConfigurationReader
    {
        public string Read(string key, bool throwOnMissing)
        {
            return ReadAppSettings(key, throwOnMissing);
        }
    }
}