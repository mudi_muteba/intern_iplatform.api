﻿namespace Infrastructure.Configuration
{
    public class iPersonSettingConfigurationReader : AbstractConfigurationReader
    {
        public string LightStonePropertyUserId
        {
            get { return ReadAppSettings("iPlatform/connector/iPerson/lightStonePropertyUserId"); }
        }
    }
}