﻿using System;
using System.ServiceModel;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Ids.IDSService;
using Workflow.QuoteAcceptance.Ids.Lead;

namespace Workflow.QuoteAcceptance.Transferer.Leads
{
    public class TransferLeadToIds : ITransferLead
    {
        private static readonly ILog Log = LogManager.GetLogger<TransferLeadToIds>();
        private readonly ITransferLeadToInsurer _serviceProvider;
        private readonly QuoteUploadMessage _message;
        private readonly WorkflowEngine _workflow;

        private TransferLeadToIds(QuoteUploadMessage message, ITransferLeadToInsurer serviceProvider, WorkflowEngine workflow)
        {
            _message = message;
            _serviceProvider = serviceProvider;
            _workflow = workflow;
        }

        public ITransferLead Transfer()
        {
            try
            {
                Log.InfoFormat("Transferring Lead to IDS with Product {0} and Request ID {1}", _message.Quote.Policy.ProductCode,_message.Quote.Request.Id);
                Result = _serviceProvider.Transfer(_message) ?? new LeadTransferResult(false);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("An error occurred transferring lead to IDS because of {0}", ex, ex.Message);
                _serviceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(),_message.Quote.Request.Id, InsurerName.Ids.Name(), _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId, _message.GetMetadata<QuoteAcceptedMetadata>().Mail, _message.Quote.QuoteAcceptanceEnvironment));
                _serviceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name, _message.Quote.Request.Id, ex, _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId));
                Result = new LeadTransferResult(false);
            }

            _serviceProvider.ExecutionPlan.Execute(_workflow);
            return this;
        }

        private static IdsServiceProvider GetProvider(IIdsServiceClient client, QuoteUploadMessage message)
        {
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
            Log.InfoFormat("Using URL: {0}", message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url);
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

            return new IdsServiceProvider(client, new LeadFactory(message), message.RetryStrategy, message.ExecutionPlan,
                message.GetMetadata<QuoteAcceptedMetadata>().Mail, message.Quote.Request.RatingContext.ChannelId);
        }

        public LeadTransferResult Result { get; private set; }

        public static readonly Func<QuoteUploadMessage, WorkflowEngine, ITransferLead> ToIds =
            (message, workflow) =>
                new TransferLeadToIds(message,GetProvider(new CommandClient(new BasicHttpBinding(), new EndpointAddress(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url)),
                        message), workflow);
    }
}