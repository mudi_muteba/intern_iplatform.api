﻿using System;
using System.IdentityModel.Tokens;
using System.ServiceModel;
using System.ServiceModel.Description;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Thinktecture.IdentityModel.WSTrust;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.KingPrice.KingPriceService;

namespace Workflow.QuoteAcceptance.Transferer.Leads
{
    public class TransferLeadToKingPrice : ITransferLead
    {
        private static readonly ILog Log = LogManager.GetLogger<TransferLeadToKingPrice>();
        private readonly ITransferLeadToInsurer _serviceProvider;
        private readonly QuoteUploadMessage _message;
        private readonly WorkflowEngine _workflow;

        private TransferLeadToKingPrice(QuoteUploadMessage message, ITransferLeadToInsurer serviceProvider, WorkflowEngine workflow)
        {
            _message = message;
            _serviceProvider = serviceProvider;
            _workflow = workflow;
        }

        private TransferLeadToKingPrice()
        {

        }

        private static KingPriceServiceProvider GetProvider(PartnerService client, QuoteUploadMessage message)
        {
            return new KingPriceServiceProvider(client,
                GetContext(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata, message.GetMetadata<QuoteAcceptedMetadata>().TaskMetadata.Get<LeadTransferKingPriceTaskMetadata>()), message.RetryStrategy, message.ExecutionPlan, message.GetMetadata<QuoteAcceptedMetadata>().Mail, message.Quote.Request.RatingContext.ChannelId);
        }

        public ITransferLead Transfer()
        {
            try
            {
                Log.InfoFormat("Transferring Lead to King Price with Product {0}", _message.Quote.Policy.ProductCode);
                Result = _serviceProvider.Transfer(_message) ?? new LeadTransferResult(false);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("An error transferring uploading lead to King Price because of {0}", ex, ex.Message);
                _serviceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(),
                    _message.Quote.Request.Id, InsurerName.KingPrice.Name(), _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId,_message.GetMetadata<QuoteAcceptedMetadata>().Mail, _message.Quote.QuoteAcceptanceEnvironment));
                _serviceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name,_message.Quote.Request.Id, ex, _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId));
                Result = new LeadTransferResult(false);
            }

            _serviceProvider.ExecutionPlan.Execute(_workflow);
            return this;
        }

        public LeadTransferResult Result { get; private set; }

        private static PartnerService BuildClient(string url, string username, string password, string wsTrustClientIdentityServerUrl,
            string wsTrustClientServicesUrl)
        {
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
            Log.InfoFormat("Using URL: {0}", url);
            Log.InfoFormat("Using Client Identity Server URL: {0}", wsTrustClientIdentityServerUrl);
            Log.InfoFormat("Using Trust Client Service URL: {0}", wsTrustClientServicesUrl);
            Log.InfoFormat("Using Username: {0}", username);
            Log.InfoFormat("Using Password: {0}", password);
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

            var binding = new WS2007FederationHttpBinding(WSFederationHttpSecurityMode.TransportWithMessageCredential);
            binding.Security.Message.EstablishSecurityContext = false;
            binding.Security.Message.IssuedKeyType = SecurityKeyType.BearerKey;

            var factory = new ChannelFactory<PartnerService>(binding,
                new EndpointAddress(url));

            return factory.CreateChannelWithIssuedToken(Token(username, password, wsTrustClientIdentityServerUrl, wsTrustClientServicesUrl));
        }

        private static SecurityContext GetContext(ApiMetadata metadata, LeadTransferKingPriceTaskMetadata taskMetadata)
        {
            return new SecurityContext
            {
                ApiKey = metadata.AuthenticationToken,
                Source = taskMetadata.AgentCode,
            };
        }

        private static SecurityToken Token(string username, string password, string wsTrustClientIdentityServerUrl, string wsTrustClientServicesUrl)
        {
            var binding = new UserNameWSTrustBinding(SecurityMode.TransportWithMessageCredential);

            var credentials = new ClientCredentials();
            credentials.UserName.UserName = username;
            credentials.UserName.Password = password;

            return WSTrustClient.Issue(new EndpointAddress(wsTrustClientIdentityServerUrl),
                new EndpointAddress(wsTrustClientServicesUrl),
                binding,
                credentials);
        }

        public static readonly Func<QuoteUploadMessage, WorkflowEngine, ITransferLead> ToKingPrice =
            (message, workflow) =>
                new TransferLeadToKingPrice(message,
                    GetProvider(
                        BuildClient(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url,
                            message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.User,
                            message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Password,
                            message.GetMetadata<QuoteAcceptedMetadata>().TaskMetadata.Get<LeadTransferKingPriceTaskMetadata>()
                                .WsTrustClientIdentityServerUrl,
                            message.GetMetadata<QuoteAcceptedMetadata>()
                                .TaskMetadata.Get<LeadTransferKingPriceTaskMetadata>()
                                .WsTrustClientServicesUrl), message), workflow);
    }
}