﻿using System;
using System.ServiceModel;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Oakhurst.Lead;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Transferer.Leads
{
    public class TransferLeadToOakhurst : ITransferLead
    {
        private static readonly ILog Log = LogManager.GetLogger<TransferLeadToOakhurst>();
        private readonly ITransferLeadToInsurer _serviceProvider;
        private readonly QuoteUploadMessage _message;
        private readonly WorkflowEngine _workflow;
        private TransferLeadToOakhurst(QuoteUploadMessage message, ITransferLeadToInsurer serviceProvider, WorkflowEngine workflow)
        {
            _message = message;
            _serviceProvider = serviceProvider;
            _workflow = workflow;
        }

        private TransferLeadToOakhurst()
        {
            
        }

        private static OakhurstServiceProvider GetProvider(ApiMetadata metadata, IOakhurstServiceClient client,QuoteUploadMessage message)
        {
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
            Log.InfoFormat("Using URL: {0}", metadata.Url);
            Log.InfoFormat("Using User: {0}", metadata.User);
            Log.InfoFormat("Using Password: {0}", metadata.Password);
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

            return new OakhurstServiceProvider(client, new LeadFactory(message),
                Authentication.AuthHeader(metadata.User, metadata.Password), message.RetryStrategy, message.ExecutionPlan, message.GetMetadata<QuoteAcceptedMetadata>().Mail, message.Quote.Request.RatingContext.ChannelId);
        }

        public ITransferLead Transfer()
        {
            try
            {
                Log.InfoFormat("Transferring lead to Oakhurst with Product {0}", _message.Quote.Policy.ProductCode);
                Result = _serviceProvider.Transfer(_message) ?? new LeadTransferResult(false);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("An error transferring uploading lead to Oakhurst because of {0}", ex, ex.Message);

                _serviceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(),_message.Quote.Request.Id, InsurerName.Oakhurst.Name(), _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId,
                   _message.GetMetadata<QuoteAcceptedMetadata>().Mail, _message.Quote.QuoteAcceptanceEnvironment));
                _serviceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name,_message.Quote.Request.Id, ex, _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId));
                Result = new LeadTransferResult(false);
            }
            _serviceProvider.ExecutionPlan.Execute(_workflow);
            return this;
        }

        public LeadTransferResult Result { get; private set; }

        public static readonly Func<QuoteUploadMessage, WorkflowEngine, ITransferLead> ToOakhurst =
           (message, workflow) =>
               new TransferLeadToOakhurst(message, GetProvider(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata,
                   new ServiceSoapClient(new BasicHttpsBinding(), new EndpointAddress(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url)), message), workflow);
    }
}