﻿using System;
using System.ServiceModel;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Hollard.HollardService;
using Workflow.QuoteAcceptance.HollardEasy.HollardEasyService;
using Workflow.QuoteAcceptance.HollardEasy.Lead;


namespace Workflow.QuoteAcceptance.Transferer.Leads
{
    public class TransferLeadToHollardEasy : ITransferLead
    {
        private static readonly ILog Log = LogManager.GetLogger<TransferLeadToHollard>();
        private readonly ITransferLeadToInsurer m_ServiceProvider;
        private readonly QuoteUploadMessage m_Message;
        private readonly WorkflowEngine m_Workflow;


        private TransferLeadToHollardEasy(QuoteUploadMessage message, ITransferLeadToInsurer serviceProvider, WorkflowEngine workflow)
        {
            m_Message = message;
            m_ServiceProvider = serviceProvider;
            m_Workflow = workflow;
        }

        private TransferLeadToHollardEasy()
        {

        }

        private static HollardEasyServiceProvider GetProvider(IHollardTiaClient client, ApiMetadata metadata, QuoteUploadMessage message)
        {
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
            Log.InfoFormat("Using URL: {0}", metadata.Url);
            Log.InfoFormat("Using AuthenticationToken: {0}", metadata.AuthenticationToken);
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

            return new HollardEasyServiceProvider(
                client,
                message.RetryStrategy,
                message.ExecutionPlan,
                message.GetMetadata<QuoteAcceptedMetadata>().Mail,
                message.Quote.Request.RatingContext.ChannelId);
            // new LeadFactory(message), metadata.AuthenticationToken, 
        }

        public ITransferLead Transfer()
        {
            try
            {
                Log.InfoFormat("Transferring Lead to Hollard with Easy Product {0} and Request ID {1}", m_Message.Quote.Policy.ProductCode,
                    m_Message.Quote.Request.Id);
                Result = m_ServiceProvider.Transfer(m_Message) ?? new LeadTransferResult(false);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("An error occurred transferring lead to Hollard Easy because of {0}", ex, ex.Message);
                m_ServiceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(),
                    m_Message.Quote.Request.Id, InsurerName.HollardEasy.Name(), m_Message.GetMetadata<QuoteAcceptedMetadata>().ChannelId, m_Message.GetMetadata<QuoteAcceptedMetadata>().Mail, m_Message.Quote.QuoteAcceptanceEnvironment));
                m_ServiceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name, m_Message.Quote.Request.Id, ex, m_Message.GetMetadata<QuoteAcceptedMetadata>().ChannelId));
                Result = new LeadTransferResult(false);
            }

            m_ServiceProvider.ExecutionPlan.Execute(m_Workflow);

            return this;
        }

        public LeadTransferResult Result { get; private set; }

        public static readonly Func<QuoteUploadMessage, WorkflowEngine, ITransferLead> ToHollardEasy =
            (message, workflow) =>
                new TransferLeadToHollardEasy(  message,
                                                GetProvider(new HollardTiaClient(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url),message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata, message),
                                                workflow);


        //new HollardEasyServiceProvider(new HollardTiaClient(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url), message

    }
}
