﻿using System;
using System.ServiceModel;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Telesure.Lead;
using Workflow.QuoteAcceptance.Telesure.TelesureService;

namespace Workflow.QuoteAcceptance.Transferer.Leads
{
    public class TransferLeadToTelesure : ITransferLead
    {
        private static readonly ILog Log = LogManager.GetLogger<TransferLeadToTelesure>();
        private readonly ITransferLeadToInsurer _serviceProvider;
        private readonly QuoteUploadMessage _message;
        private readonly WorkflowEngine _workflow;

        private TransferLeadToTelesure(QuoteUploadMessage message, ITransferLeadToInsurer serviceProvider, WorkflowEngine workflow)
        {
            _message = message;
            _serviceProvider = serviceProvider;
            _workflow = workflow;
        }

        private TransferLeadToTelesure()
        {

        }

        private static TelesureServiceProvider GetProvider(ApiMetadata metadata, ITelesureServiceClient client, QuoteUploadMessage message)
        {
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
            Log.InfoFormat("Using URL: {0}", metadata.Url);
            Log.InfoFormat("Using Password: {0}", metadata.Password);
            Log.InfoFormat("Using Username: {0}", metadata.User);
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

            return new TelesureServiceProvider(client, new LeadFactory(message),
                new AuthenticationHeader() { Password = metadata.Password, Username = metadata.User }, message.RetryStrategy, message.ExecutionPlan, message.GetMetadata<QuoteAcceptedMetadata>().Mail, message.Quote.Request.RatingContext.ChannelId);
        }

        public ITransferLead Transfer()
        {
            try
            {
                Log.InfoFormat("Transferring Lead to Telesure with Product Code {0} and Request ID {1}", _message.Quote.Policy.ProductCode,
                    _message.Quote.Request.Id);
                Result = _serviceProvider.Transfer(_message) ?? new LeadTransferResult(false);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("An error  transferring lead to Telesure because of {0}", ex, ex.Message);
                _serviceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(),_message.Quote.Request.Id, InsurerName.Telesure.Name(), _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId,_message.GetMetadata<QuoteAcceptedMetadata>().Mail, _message.Quote.QuoteAcceptanceEnvironment));
                _serviceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name,_message.Quote.Request.Id, ex, _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId));
                Result = new LeadTransferResult(false);
            }
            _serviceProvider.ExecutionPlan.Execute(_workflow);

            return this;
        }

        public LeadTransferResult Result { get; private set; }

        public static readonly Func<QuoteUploadMessage, WorkflowEngine, ITransferLead> ToTelesure =
            (message, workflow) =>
                new TransferLeadToTelesure(message,
                    GetProvider(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata,
                        new APIServiceSoapClient(new BasicHttpBinding(BasicHttpSecurityMode.Transport),
                            new EndpointAddress(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url)), message), workflow);
    }
}