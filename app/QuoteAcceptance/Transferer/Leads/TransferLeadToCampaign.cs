﻿using System;
using System.ServiceModel;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Campaign.Lead;
using Workflow.QuoteAcceptance.Campaign.LeadWebService;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;

namespace Workflow.QuoteAcceptance.Transferer.Leads
{
    public sealed class TransferLeadToCampaign : ITransferLead
    {
        private static readonly ILog Log = LogManager.GetLogger<TransferLeadToCampaign>();
        private readonly ITransferLeadToInsurer _serviceProvider;
        private readonly QuoteUploadMessage _message;
        private readonly WorkflowEngine _workflow;

        private TransferLeadToCampaign(QuoteUploadMessage message, ITransferLeadToInsurer serviceProvider, WorkflowEngine workflow)
        {
            _message = message;
            _serviceProvider = serviceProvider;
            _workflow = workflow;
        }

        private TransferLeadToCampaign()
        {

        }

        private static CampaignServiceProvider GetProvider(ICampaignServiceClient client, QuoteUploadMessage message)
        {
            return new CampaignServiceProvider(client, new LeadFactory(message), message.RetryStrategy, message.ExecutionPlan,message.GetMetadata<QuoteAcceptedMetadata>().Mail, message.Quote.Request.RatingContext.ChannelId);
        }

        public ITransferLead Transfer()
        {
            try
            {
                Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
                Log.InfoFormat("Using URL: {0}", _message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url);
                Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

                Log.InfoFormat("Transferring Lead to Campaign Service with Product {0} and Request ID {1}", _message.Quote.Policy.ProductCode,
                    _message.Quote.Request.Id);
                Result = _serviceProvider.Transfer(_message) ?? new LeadTransferResult(false);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("An error occurred transferring lead to Campaign because of {0}", ex, ex.Message);

                _serviceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(),_message.Quote.Request.Id,InsurerName.Campaign.Name(), _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId,
                    _message.GetMetadata<QuoteAcceptedMetadata>().Mail, _message.Quote.QuoteAcceptanceEnvironment));
                _serviceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name,_message.Quote.Request.Id, ex, _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId));
                Result = new LeadTransferResult(false);
            }

            _serviceProvider.ExecutionPlan.Execute(_workflow);
            return this;
        }

        public LeadTransferResult Result { get; private set; }

        public static readonly Func<QuoteUploadMessage, WorkflowEngine, ITransferLead> ToCampaign =
            (message, workflow) =>
                new TransferLeadToCampaign(message,
                    GetProvider(new LeadWebServiceSoapClient(new BasicHttpBinding(), new EndpointAddress(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url)),
                        message), workflow);
    }
}