﻿using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Workflow.QuoteAcceptance.MiWay.MiWayService;
using System;
using System.ServiceModel;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.MiWay.MiWayService;

namespace Workflow.QuoteAcceptance.Transferer.Leads
{
    public class TransferLeadToMiWay : ITransferLead
    {
        private static readonly ILog Log = LogManager.GetLogger<TransferLeadToMiWay>();
        private readonly ITransferLeadToInsurer _serviceProvider;
        private readonly QuoteUploadMessage _message;
        private readonly WorkflowEngine _workflow;

        private TransferLeadToMiWay(QuoteUploadMessage message, ITransferLeadToInsurer serviceProvider, WorkflowEngine workflow)
        {
            _message = message;
            _serviceProvider = serviceProvider;
            _workflow = workflow;
        }

        private static MiWayServiceProvider GetProvider(WowWebWS client, QuoteUploadMessage message)
        {
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
            Log.InfoFormat("Using URL: {0}", message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url);
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

            return new MiWayServiceProvider(client,
                message.RetryStrategy, 
                message.ExecutionPlan, 
                message.GetMetadata<QuoteAcceptedMetadata>().Mail, 
                message.Quote.Request.RatingContext.ChannelId);
        }

        public ITransferLead Transfer()
        {
            try
            {
                Log.InfoFormat("Transferring Lead to MiWay with Product {0}", _message.Quote.Policy.ProductCode);
                Result = _serviceProvider.Transfer(_message) ?? new LeadTransferResult(false);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("An error transferring uploading lead to MiWay because of {0}", ex, ex.Message);
                _serviceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(),
                    _message.Quote.Request.Id, InsurerName.MiWay.Name(), _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId, _message.GetMetadata<QuoteAcceptedMetadata>().Mail));
                _serviceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name, _message.Quote.Request.Id, ex, _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId));
                Result = new LeadTransferResult(false);
            }

            _serviceProvider.ExecutionPlan.Execute(_workflow);
            return this;
        }

        public LeadTransferResult Result { get; private set; }

        private static WowWebWS BuildClient(string url)
        {
            ChannelFactory<WowWebWS> factory = null;

            if (url.ToLower().StartsWith("https://"))
            {
                factory = new ChannelFactory<WowWebWS>(new BasicHttpsBinding(),
                new EndpointAddress(url));
            }
            else
            {
                factory = new ChannelFactory<WowWebWS>(new BasicHttpBinding(), 
                    new EndpointAddress(url));
                factory.Open();
            }

            return factory.CreateChannel();
        }

        public static readonly Func<QuoteUploadMessage, WorkflowEngine, ITransferLead> ToMiWay =
            (message, workflow) =>
                new TransferLeadToMiWay(message,
                    GetProvider(
                        BuildClient(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url), message), workflow);
    }
}