﻿using System;
using System.ServiceModel;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Hollard.HollardService;
using Workflow.QuoteAcceptance.Hollard.Lead;

namespace Workflow.QuoteAcceptance.Transferer.Leads
{
    public class TransferLeadToHollard : ITransferLead
    {
        private static readonly ILog Log = LogManager.GetLogger<TransferLeadToHollard>();
        private readonly ITransferLeadToInsurer _serviceProvider;
        private readonly QuoteUploadMessage _message;
        private readonly WorkflowEngine _workflow;
       

        private TransferLeadToHollard(QuoteUploadMessage message, ITransferLeadToInsurer serviceProvider, WorkflowEngine workflow)
        {
            _message = message;
            _serviceProvider = serviceProvider;
            _workflow = workflow;
        }

        private TransferLeadToHollard()
        {

        }

        private static HollardServiceProvider GetProvider(ApiMetadata metadata, IHollardServiceClient client, QuoteUploadMessage message)
        {
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
            Log.InfoFormat("Using URL: {0}", metadata.Url);
            Log.InfoFormat("Using AuthenticationToken: {0}", metadata.AuthenticationToken);
            Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

            return new HollardServiceProvider(client, new LeadFactory(message), metadata.AuthenticationToken, message.RetryStrategy,
                message.ExecutionPlan, message.GetMetadata<QuoteAcceptedMetadata>().Mail, message.Quote.Request.RatingContext.ChannelId);
        }

        public ITransferLead Transfer()
        {
            try
            {
                Log.InfoFormat("Transferring Lead to Hollard with Product {0} and Request ID {1}", _message.Quote.Policy.ProductCode,
                    _message.Quote.Request.Id);
                Result = _serviceProvider.Transfer(_message) ?? new LeadTransferResult(false);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("An error occurred transferring lead to Hollard because of {0}", ex, ex.Message);
                _serviceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(),
                    _message.Quote.Request.Id, InsurerName.Hollard.Name(), _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId,_message.GetMetadata<QuoteAcceptedMetadata>().Mail, _message.Quote.QuoteAcceptanceEnvironment));
                _serviceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name,_message.Quote.Request.Id, ex, _message.GetMetadata<QuoteAcceptedMetadata>().ChannelId));
                Result = new LeadTransferResult(false);
            }

            _serviceProvider.ExecutionPlan.Execute(_workflow);

            return this;
        }

        public LeadTransferResult Result { get; private set; }

        public static readonly Func<QuoteUploadMessage, WorkflowEngine, ITransferLead> ToHollard =
            (message, workflow) =>
                new TransferLeadToHollard(message,GetProvider(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata,
                        new HollardSwitchingClient(new BasicHttpBinding(), new EndpointAddress(message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata.Url)), message), workflow);
    }
}