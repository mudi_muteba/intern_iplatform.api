﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.QuoteAcceptance.Leads;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;

namespace Workflow.QuoteAcceptance.Transferer.Factories
{
    public class CreateLeadTransferDestinationFactory : AbstractTransferOfLeadFactory<QuoteUploadMessage, WorkflowEngine>
    {
        private readonly IConfigureTransferOfAcceptedQuote _configuration;

        public CreateLeadTransferDestinationFactory(IConfigureTransferOfAcceptedQuote configuration)
        {
            _configuration = configuration;
        }

        public override IEnumerable<ITransferLead> Create(QuoteUploadMessage message, WorkflowEngine workflow)
        {
            var insurer = _configuration.Insurers
                .Where(w => w.Key.Any(productCode => productCode.Equals(message.Quote.Policy.ProductCode, StringComparison.CurrentCultureIgnoreCase)))
                .ToList();

            //var insurer = TransferLeadConfiguration.Insurers
            //     .Where(w => w.Key.Any(productCode => productCode.Equals(message.Quote.Policy.ProductCode, StringComparison.CurrentCultureIgnoreCase))).ToList();

            if (!insurer.Any())
                throw new Exception(string.Format("Cannot find an insurer to upload lead with product code {0}", message.Quote.Policy.ProductCode));

            return insurer.Select(s => s.Value(message, workflow));
        }
    }
}