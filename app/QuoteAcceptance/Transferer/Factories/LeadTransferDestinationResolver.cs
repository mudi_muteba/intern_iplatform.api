﻿using System.Collections.Generic;
using Castle.Windsor;
using Workflow.QuoteAcceptance;
using Workflow.QuoteAcceptance.Domain;

namespace Workflow.QuoteAcceptance.Transferer.Factories
{
    public class LeadTransferDestinationResolver : ICreateTransferOfLead
    {
        private readonly IWindsorContainer _container;

        public LeadTransferDestinationResolver(IWindsorContainer container)
        {
            _container = container;
        }

        public IEnumerable<ITransferLead> Create(object message, object workflow)
        {
            var type = message.GetType();
            var executoryType = typeof (ICreateTransferOfLead).MakeGenericType(type);
            var executor = (ICreateTransferOfLead) _container.Resolve(executoryType);
            return ExecuteHandler(message, workflow, executor);
        }

        private IEnumerable<ITransferLead> ExecuteHandler(object message, object workflow, ICreateTransferOfLead executor)
        {
            try
            {
                return executor.Create(message, workflow);
            }
            finally
            {
                _container.Release(executor);
            }
        }

    }
}