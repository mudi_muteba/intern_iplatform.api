﻿using System;
using System.Collections.Generic;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Products;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Transferer.Leads;

namespace Workflow.QuoteAcceptance.Transferer
{
    public class TransferLeadConfiguration : IConfigureTransferOfAcceptedQuote
    {
        public IDictionary<IEnumerable<string>, Func<QuoteUploadMessage, WorkflowEngine, ITransferLead>> Insurers
        {
            get
            {
                return new Dictionary
                    <IEnumerable<string>, Func<QuoteUploadMessage, WorkflowEngine, ITransferLead>>()
                {
                    {CampaignProducts.Codes, TransferLeadToCampaign.ToCampaign},
                    {DotsureProducts.Codes, TransferLeadToDotsure.ToDotsure},
                    {HollardProducts.Codes, TransferLeadToHollard.ToHollard},
                    {KingPriceProducts.Codes, TransferLeadToKingPrice.ToKingPrice},
                    {KingPriceV2Products.Codes, TransferLeadToKingPriceV2.ToKingPriceV2},
                    {OakhurstProducts.Codes, TransferLeadToOakhurst.ToOakhurst},
                    {RegentProducts.Codes, TransferLeadToRegent.ToRegent},
                    {TelesureProducts.Codes, TransferLeadToTelesure.ToTelesure},
                    {IdsProducts.Codes, TransferLeadToIds.ToIds},
                    {SAUProducts.Codes, TransferLeadToSAU.ToSAU},
                    {MiWayProducts.Codes, TransferLeadToMiWay.ToMiWay},
                    {AAProducts.Codes, TransferLeadToAA.ToAA},
                    {TelesureWarantyProducts.Codes, TransferLeadToIds.ToIds},
                    {HollardEasyProducts.Codes, TransferLeadToHollardEasy.ToHollardEasy},
                };
            }
        }
    }
}