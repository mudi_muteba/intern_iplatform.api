﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Transferer.Factories;

namespace Workflow.QuoteAcceptance.Transferer.Installers
{
    public class LeadTransferInstallers : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ICreateTransferOfLead>().ImplementedBy<LeadTransferDestinationResolver>());
            container.Register(Component.For<IConfigureTransferOfAcceptedQuote>().ImplementedBy<TransferLeadConfiguration>());
            container.Register(Classes.FromAssemblyContaining<CreateLeadTransferDestinationFactory>()
            .BasedOn(typeof(ICreateTransferOfLead< , >))
            .WithServiceAllInterfaces()
            .LifestyleTransient());
        }
    }
}