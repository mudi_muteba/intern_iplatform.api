﻿using AutoMapper;
using Workflow.QuoteAcceptance.Oakhurst.Mappings;

namespace Workflow.QuoteAcceptance.Transferer.Installers
{
    public static class TransferLeadMappersInstaller
    {
        public static void Install(IConfiguration configuration)
        {
            //Add your Transfer lead mapper profiles here
            configuration.AddProfile<OakhurstQuoteMappers>();
        }
    }
}