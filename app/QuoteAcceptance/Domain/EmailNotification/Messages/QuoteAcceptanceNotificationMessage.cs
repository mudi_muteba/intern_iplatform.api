﻿using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Workflow.QuoteAcceptance.Domain.EmailNotification.Messages
{
    public class QuoteAcceptanceNotificationMessage : WorkflowExecutionMessage
    {
        public QuoteAcceptanceNotificationMessage()
        {
        }
        public QuoteAcceptanceNotificationMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
        }

        public QuoteAcceptanceNotificationMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default)
            : base(metadata, messageType)
        {
        }

    }
}
