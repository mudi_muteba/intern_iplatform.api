﻿using iPlatform.Api.DTOs.QuoteAcceptance;
using ToolBox.Communication.Interfaces;

namespace Workflow.QuoteAcceptance.Domain.EmailNotification.Messages
{
    public class QuoteAcceptanceEmailMessageData : ICommunicationMessageData
    {
        public QuoteAcceptanceEmailMessageData(QuoteNotificationDto quote, string insurer, string product, string insurerReference = "")
        {
            Quote = quote;
            Insurer = insurer;
            Product = product;
            InsurerReference = insurerReference;
        }

        public QuoteNotificationDto Quote { get; private set; }
        public string Insurer { get; private set; }
        public string Product { get; private set; }
        public string InsurerReference { get; set; }
    }
}
