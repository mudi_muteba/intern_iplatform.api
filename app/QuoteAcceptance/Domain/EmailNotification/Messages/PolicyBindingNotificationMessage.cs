﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Workflow.QuoteAcceptance.Domain.EmailNotification.Messages
{
    public class PolicyBindingNotificationMessage : WorkflowExecutionMessage
    {
        public PolicyBindingNotificationMessage()
        {
        }
        public PolicyBindingNotificationMessage(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
        }

        public PolicyBindingNotificationMessage(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default)
            : base(metadata, messageType)
        {
        }
    }
}
