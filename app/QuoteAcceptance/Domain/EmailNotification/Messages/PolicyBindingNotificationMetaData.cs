﻿using System.Runtime.Serialization;
using iPlatform.Api.DTOs.QuoteAcceptance;
using Workflow.Messages.Plan.Tasks;

namespace Workflow.QuoteAcceptance.Domain.EmailNotification.Messages
{
    public class PolicyBindingNotificationMetaData : ITaskMetadata
    {
        public PolicyBindingNotificationMetaData() { }

        [DataMember]
        public int ChannelId { get; set; }

        [DataMember]
        public int QuoteId { get; set; }

        [DataMember]
        public int LeadId { get; set; }

        [DataMember]
        public int PartyId { get; set; }

        [DataMember]
        public int AgentId { get; set; }

        [DataMember]
        public int AgentPartyId { get; set; }

        [DataMember]
        public AgentDetailDto LoggedAgent { get; set; }

        public PolicyBindingNotificationMetaData(int channelId, int quoteId, int leadId, int partyId, int agentId, AgentDetailDto loggedAgent, int agentPartyId)
        {
            ChannelId = channelId;
            QuoteId = quoteId;
            LeadId = leadId;
            PartyId = partyId;
            AgentId = agentId;
            LoggedAgent = loggedAgent;
            AgentPartyId = agentPartyId;
        }
    }
}