﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData;
using ToolBox.Communication.Interfaces;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.EmailNotification.Messages
{
    public class QuoteAcceptanceEmailMessage : CommunicationMessage<QuoteAcceptanceEmailMessageData>
    {
        private readonly QuoteAcceptanceEmailMessageData data;
        private readonly string notificationAddress;
        private readonly string subject;

        public QuoteAcceptanceEmailMessage(QuoteAcceptanceEmailMessageData data, string notificationAddress) : base("quote", data)
        {
            this.notificationAddress = notificationAddress;
        }

        public QuoteAcceptanceEmailMessage(QuoteAcceptanceEmailMessageData data, string notificationAddress, QuoteAcceptanceNotificationMetaData.QuoteUploadResult uploadResult)
            : base("quote", data)
        {
            this.data = data;
            this.notificationAddress = notificationAddress;
            switch (uploadResult)
            {
                case QuoteAcceptanceNotificationMetaData.QuoteUploadResult.EmailOnly:
                    {
                        subject = string.Format("({0}) Quote Accepted", data.Quote.Environment);
                        break;
                    }
                case QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success:
                    {
                        subject = string.Format("({0}) Accepted Quote Successfully Uploaded", data.Quote.Environment);
                        break;
                    }
                case QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure:
                    {
                        subject = string.Format("({0}) Accepted Quote Failed to Upload", data.Quote.Environment);
                        break;
                    }
            }

            if (!string.IsNullOrWhiteSpace(data.InsurerReference))
            {
                subject += " (" + data.InsurerReference + ")";
            }

            subject += " - ";
        }

        public override string Address
        {
            get { return notificationAddress; }
            set { }
        }

        public override string Subject
        {
            get
            {
                return subject + (data.Quote.Person.Title ?? Titles.Mr).Abbreviation
                       + " " + data.Quote.Person.Initials
                       + " " + data.Quote.Person.Surname
                       + " - " + data.Quote.Person.IdNumber.ToString(CultureInfo.InvariantCulture);
            }
            set { }
        }
    }
}
