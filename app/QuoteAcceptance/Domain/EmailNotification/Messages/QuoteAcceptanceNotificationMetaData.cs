﻿using iPlatform.Api.DTOs.QuoteAcceptance;
using iPlatform.Api.DTOs.Ratings.Quoting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Workflow.Messages.Plan.Tasks;

namespace Workflow.QuoteAcceptance.Domain.EmailNotification.Messages
{
    [DataContract]
    public class QuoteAcceptanceNotificationMetaData : ITaskMetadata
    {
        public QuoteAcceptanceNotificationMetaData() { }
        [DataMember] public string NotificationAddress { get; private set; }
        [DataMember] public QuoteNotificationDto Quote { get; private set; }
        [DataMember] public QuoteUploadResult UploadResult { get; set; }

        public QuoteAcceptanceNotificationMetaData(QuoteNotificationDto quote, string notificationAddress, QuoteUploadResult uploadResult)
        {
            UploadResult = uploadResult;
            Quote = quote;
            NotificationAddress = string.Format("{0},{1}", quote.AgentDetail.EmailAddress, notificationAddress);
        }

        public enum QuoteUploadResult
        {
            EmailOnly,
            Success,
            Failure
        }
    }
}
