﻿using System.Linq;
using Common.Logging;
using Domain.Emailing.Factory;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.QuoteAcceptance;
using ToolBox.Communication.Emailing;
using ToolBox.Templating.Interfaces;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.Domain.EmailNotification
{
    public class QuoteAcceptanceMailer
    {
        private static readonly ILog log = LogManager.GetLogger<QuoteAcceptanceMailer>();
        private readonly ITemplateEngine _templateEngine;
        public QuoteAcceptanceMailer(ITemplateEngine templateEngine)
        {
            _templateEngine = templateEngine;
        }

        public void Send(QuoteNotificationDto quote, string notificationAddress, QuoteAcceptanceNotificationMetaData.QuoteUploadResult uploadResult)
        {
            var sendCommunication = new EmailCommunicator(_templateEngine, new SmtpClientAdapter(quote.EmailCommunicationSetting.Get()), quote.EmailCommunicationSetting.Get());

            foreach (var cover in quote.Covers)
            {
                foreach(var questionGroup in cover.QuestionGroups)
                {
                    questionGroup.QuestionAnswers.RemoveAll(x => x.Question.Name.ToLower().Trim() == "im a dummy question");
                }
            }

            var productCode = quote.ProductCode;
            log.InfoFormat("Getting product information [{0}]", productCode);

            var details = new
            {
                Insurer = quote.InsurerName,
                Product = quote.ProductName
            };

            var data = new QuoteAcceptanceEmailMessageData(quote, details.Insurer, details.Product, quote.InsurerReference);
            var message = new QuoteAcceptanceEmailMessage(data, notificationAddress, uploadResult);

            log.Info("Calling email communicator");
            sendCommunication.Send(message);

            log.InfoFormat("Quote sent to [{0}] using template [{1}] with subject [{2}]",
                message.Address, message.TemplateName, message.Subject);
        }
    }

    public static class EmailSettingExtensions
    {
        public static AppSettingsEmailSetting Get(this EmailCommunicationSettingDto dto)
        {
            return new AppSettingsEmailSetting
            {
                CustomFrom = "",
                DefaultBCC = dto.DefaultBCC,
                DefaultContactNumber = dto.DefaultContactNumber,
                DefaultFrom = dto.DefaultFrom,
                Host = dto.Host,
                Password = dto.Password,
                Port = dto.Port,
                SubAccountID = dto.SubAccountID,
                UseDefaultCredentials = dto.UseDefaultCredentials,
                UserName = dto.Username,
                UseSSL = dto.UseSSL
            };
        }
    }
}
