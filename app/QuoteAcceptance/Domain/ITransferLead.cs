﻿namespace Workflow.QuoteAcceptance.Domain
{
    public interface ITransferLead
    {
        ITransferLead Transfer();
        LeadTransferResult Result { get; }
    }
}