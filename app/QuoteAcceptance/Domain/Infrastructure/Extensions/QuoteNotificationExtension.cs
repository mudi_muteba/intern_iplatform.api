﻿using System.Linq;
using iPlatform.Api.DTOs.QuoteAcceptance;
using iPlatform.Api.DTOs.Ratings.Quoting;
using System.Collections.Generic;
using MasterData;
using iPlatform.Api.DTOs.Extensions;
using System.Globalization;
using System;

namespace Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions
{
    public static class QuoteNotificationExtension
    {
        public static QuoteNotificationDto GetQuoteNotificationDto(this PublishQuoteUploadMessageDto quote)
        {
            var dto = new QuoteNotificationDto();

            dto.Person = quote.Request.Policy.Persons.FirstOrDefault(x => x.PolicyHolder);
            dto.ProductCode = quote.Policy.ProductCode;
            dto.InsurerCode = quote.Policy.InsurerCode;
            dto.InsurerName = quote.Policy.InsurerName;
            dto.ProductName = quote.Policy.ProductName;
            dto.InsurerReference = quote.Policy.InsurerReference;
            dto.NumberFormatInfo = quote.NumberFormatInfo;
            dto.Covers = GetCovers(quote);
            dto.InsuredInfo = quote.InsuredInfo;
            dto.PaymentPlan = quote.Request.Policy.PaymentPlan;
            dto.Environment = quote.QuoteAcceptanceEnvironment;
            dto.AgentDetail = quote.AgentDetail;
            dto.EmailCommunicationSetting = quote.ChannelInfo.EmailCommunicationSetting;
            dto.Fees = quote.Policy.Fees;
            return dto;
        }

        private static List<QuoteNotificationCoverDto> GetCovers(PublishQuoteUploadMessageDto quote)
        {
            List<QuoteNotificationCoverDto> Covers = new List<QuoteNotificationCoverDto>();

            foreach(var item in quote.Request.Items.GroupBy(x => x.Cover).SelectMany(covers => covers).OrderBy(x => x.AssetNo))
            {
                var itemResult = quote.Policy.Items.FirstOrDefault(x => x.Cover.Id.Equals(item.Cover.Id) && x.AssetNo == item.AssetNo);

                if (itemResult == null)
                {
                    continue;
                }
                    

                var cover = new QuoteNotificationCoverDto();
                cover.Cover = item.Cover;
                cover.Premium = itemResult.Premium;
                cover.Sasria = itemResult.Sasria;
                cover.Excess = itemResult.Excess.Basic;
                cover.AssetNo = item.AssetNo;

                foreach (var questionGroup in item.Answers.GroupBy(x => x.Question.QuestionGroup).OrderBy(x => x.Key.VisibleIndex))
                {
                    var questionGroupDto = new QuoteNotificationQuestionGroupDto();
                    questionGroupDto.QuestionGroup = new QuestionGroups().FirstOrDefault(x => x.Id == questionGroup.Key.Id);

                    foreach (var answerDto in questionGroup)
                    {
                        var questionAnswerDto = new QuoteNotificationQuestionAnswerDto();
                        questionAnswerDto.Question = answerDto.Question;

                        var questionAnswer = answerDto.QuestionAnswer + "";

                        if (answerDto.Question.QuestionType.Id == QuestionTypes.Dropdown.Id)
                        {
                            questionAnswerDto.Answer = !string.IsNullOrEmpty(questionAnswer) ? answerDto.QuestionAnswer.CastAsQuestionAnswer().Answer : string.Empty;

                            // Format Currency Values
                            if (answerDto.Question.Id.Equals(Questions.VoluntaryExcess.Id))
                            {
                                var value = 0m;
                                questionAnswerDto.Answer = decimal.TryParse(questionAnswerDto.Answer, out value) ? value.ToString("C", quote.NumberFormatInfo) : 0m.ToString("C", quote.NumberFormatInfo);
                            }
                        }
                        else if (answerDto.Question.QuestionType.Id == QuestionTypes.Date.Id)
                        {
                            DateTime date;
                            questionAnswerDto.Answer = DateTime.TryParse(questionAnswer, out date) ? date.ToShortDateString() : "";
                        }
                        else if (answerDto.Question.QuestionType.Id == QuestionTypes.Checkbox.Id)
                        {
                            questionAnswerDto.Answer = !string.IsNullOrEmpty(questionAnswer) ? Convert.ToBoolean(answerDto.QuestionAnswer) ? "Yes" : "No" : "No";
                        }
                        else
                        {
                            questionAnswerDto.Answer = questionAnswer;

                            // Format Currency Values
                            if (answerDto.Question.Id.Equals(Questions.SumInsured.Id) ||
                                answerDto.Question.Id.Equals(Questions.VehicleExtrasValue.Id) ||
                                answerDto.Question.Id.Equals(Questions.RadioValue.Id) ||
                                answerDto.Question.Id.Equals(Questions.ToolsAndSparePartsValue.Id))
                            {
                                var value = 0m;
                                questionAnswerDto.Answer = decimal.TryParse(questionAnswerDto.Answer, out value) ? value.ToString("C", quote.NumberFormatInfo) : 0m.ToString("C", quote.NumberFormatInfo);
                            }
                        }
                        questionGroupDto.QuestionAnswers.Add(questionAnswerDto);
                    }
                    cover.QuestionGroups.Add(questionGroupDto);
                }
                Covers.Add(cover);
            }
            return Covers;
        }
    }
}
