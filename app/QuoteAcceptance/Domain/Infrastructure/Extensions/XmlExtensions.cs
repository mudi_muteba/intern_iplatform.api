﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using MasterData;
using log4net;

namespace Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions
{
    public static class XmlExtensions
    {
        private static readonly ILog _Log = LogManager.GetLogger(typeof(XmlExtensions));
        private static readonly Dictionary<string, XmlSerializer> SerializerDictionary = new Dictionary<string, XmlSerializer>();

        public static string SerializeAsXml(this object obj)
        {
            try
            {
                if (obj == null)
                    return "";

                using (var writer = new StringWriter())
                {
                    var xns = new XmlSerializerNamespaces();
                    xns.Add(string.Empty, string.Empty);
                    XmlSerializer serializer;
                    if (!SerializerDictionary.TryGetValue(obj.GetType().FullName, out serializer))
                    {
                        serializer = new XmlSerializer(obj.GetType(), new[] { typeof(DomainMarker) }); // HACK
                        SerializerDictionary.Add(obj.GetType().FullName, serializer);
                    }

                    serializer.Serialize(writer, obj, xns);

                    return writer.ToString().Replace(@" encoding=""utf-16""", string.Empty);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(String.Format("An error occurred whilst trying to serialize object to XML. Message: ", ex.Message), ex);
                return "";
            }

        }

        public static T DeSerializeToObject<T>(this string xmlString)
        {
            T result = default(T);

            try
            {
                if (!String.IsNullOrEmpty(xmlString))
                {
                    using (var reader = new StringReader(xmlString))
                    {
                        var serializer = new XmlSerializer(typeof(T));
                        result = (T)serializer.Deserialize(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                _Log.Error(String.Format("An error occurred whilst trying to de-serialize XML to object. Message: ", ex.Message), ex);
                throw;
            }

            return result;
        }
    }

}
