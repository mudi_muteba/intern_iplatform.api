﻿using System.Linq;
using iPlatform.Api.DTOs.QuoteAcceptance;
using System.Collections.Generic;
using MasterData;
using iPlatform.Api.DTOs.Extensions;
using System;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions
{
    public static class PolicyBindingNotificationExtension
    {
        public static QuoteNotificationDto GetNotificationDto(this PolicyBindingRequestDto request)
        {
            var dto = new QuoteNotificationDto();

            dto.Person = request.RequestInfo.Persons.FirstOrDefault();
            dto.ProductCode = request.RequestInfo.ProductCode;
            dto.InsurerCode = request.RequestInfo.InsurerCode;
            dto.InsurerName = request.RequestInfo.InsurerName;
            dto.ProductName = request.RequestInfo.ProductName;
            dto.InsurerReference = request.RequestInfo.InsurerReference;

            dto.NumberFormatInfo = request.NumberFormatInfo;
            dto.Covers = GetCovers(request);
            dto.InsuredInfo = request.InsuredInfo;
            dto.PaymentPlan = PaymentPlans.Monthly;
            dto.Environment = request.Environment;
            dto.AgentDetail = request.AgentDetail;
            dto.EmailCommunicationSetting = request.ChannelInfo.EmailCommunicationSetting;
            dto.Fees = request.RequestInfo.Fees;
            return dto;
        }

        private static List<QuoteNotificationCoverDto> GetCovers(PolicyBindingRequestDto dto)
        {
            List<QuoteNotificationCoverDto> Covers = new List<QuoteNotificationCoverDto>();

            foreach(var item in dto.RequestInfo.Items.GroupBy(x => x.Cover).SelectMany(covers => covers).OrderBy(x => x.AssetNumber))
            {
                var itemResult = dto.RequestInfo.Items.FirstOrDefault(x => x.Cover.Id.Equals(item.Cover.Id) && x.AssetNumber == item.AssetNumber);

                if (itemResult == null)
                {
                    continue;
                }
                    

                var cover = new QuoteNotificationCoverDto();
                cover.Cover = item.Cover;
                cover.Premium = itemResult.Premium;
                cover.Sasria = itemResult.Sasria;
                cover.Excess = itemResult.ExcessBasic;
                cover.AssetNo = item.AssetNumber;

                foreach (var questionGroup in item.RatingRequestItem.Answers.GroupBy(x => x.Question.QuestionGroup).OrderBy(x => x.Key.VisibleIndex))
                {
                    var questionGroupDto = new QuoteNotificationQuestionGroupDto();
                    questionGroupDto.QuestionGroup = new QuestionGroups().FirstOrDefault(x => x.Id == questionGroup.Key.Id);

                    foreach (var answerDto in questionGroup)
                    {
                        var questionAnswerDto = new QuoteNotificationQuestionAnswerDto();
                        questionAnswerDto.Question = answerDto.Question;

                        var questionAnswer = answerDto.QuestionAnswer + "";

                        if (answerDto.Question.QuestionType.Id == QuestionTypes.Dropdown.Id)
                        {
                            questionAnswerDto.Answer = !string.IsNullOrEmpty(questionAnswer) ? answerDto.QuestionAnswer.CastAsQuestionAnswer().Answer : string.Empty;

                            // Format Currency Values
                            if (answerDto.Question.Id.Equals(Questions.VoluntaryExcess.Id))
                            {
                                var value = 0m;
                                questionAnswerDto.Answer = decimal.TryParse(questionAnswerDto.Answer, out value) ? value.ToString("C", dto.NumberFormatInfo) : 0m.ToString("C", dto.NumberFormatInfo);
                            }
                        }
                        else if (answerDto.Question.QuestionType.Id == QuestionTypes.Date.Id)
                        {
                            DateTime date;
                            questionAnswerDto.Answer = DateTime.TryParse(questionAnswer, out date) ? date.ToShortDateString() : "";
                        }
                        else if (answerDto.Question.QuestionType.Id == QuestionTypes.Checkbox.Id)
                        {
                            questionAnswerDto.Answer = !string.IsNullOrEmpty(questionAnswer) ? Convert.ToBoolean(answerDto.QuestionAnswer) ? "Yes" : "No" : "No";
                        }
                        else
                        {
                            questionAnswerDto.Answer = questionAnswer;

                            // Format Currency Values
                            if (answerDto.Question.Id.Equals(Questions.SumInsured.Id) ||
                                answerDto.Question.Id.Equals(Questions.VehicleExtrasValue.Id) ||
                                answerDto.Question.Id.Equals(Questions.RadioValue.Id) ||
                                answerDto.Question.Id.Equals(Questions.ToolsAndSparePartsValue.Id))
                            {
                                var value = 0m;
                                questionAnswerDto.Answer = decimal.TryParse(questionAnswerDto.Answer, out value) ? value.ToString("C", dto.NumberFormatInfo) : 0m.ToString("C", dto.NumberFormatInfo);
                            }
                        }
                        questionGroupDto.QuestionAnswers.Add(questionAnswerDto);
                    }
                    cover.QuestionGroups.Add(questionGroupDto);
                }
                Covers.Add(cover);
            }
            return Covers;
        }
    }
}
