﻿using System;
using Shared;

namespace Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions
{
    public static class ExceptionExtensions
    {
        public static string Print(this Exception e)
        {
            return new ExceptionPrettyPrinter().Print(e);
        }
    }
}
