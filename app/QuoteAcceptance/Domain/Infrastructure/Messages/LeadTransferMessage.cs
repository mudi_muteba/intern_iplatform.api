﻿using System;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Shared;
using iPlatform.Api.DTOs.Monitoring;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Domain.Emailing.Factory;
using iPlatform.Api.DTOs.PolicyBindings;
using iPlatform.Api.DTOs.QuoteAcceptance;

namespace Workflow.QuoteAcceptance.Domain.Infrastructure.Messages
{
    public static class LeadTransferMessage
    {
        private const string ProductCode = "{ProductCode}";
        private const string InsurerName = "{InsurerName}";
        private const string RatingRequestId = "{RatingRequestId}";
        private const string Message = "{Message}";
        private const string Request = "{Request}";
        private const string Response = "{Response}";
        private const string Date = "{Date}";


        private const string Successful =
            "Successfully uploaded Lead for Quote to {InsurerName}. Product Code {ProductCode} Rating Request Id {RatingRequestId}";

        private const string Failure =
            "Failed to uploaded Lead for Quote to {InsurerName}. Product Code {ProductCode} Rating Request Id {RatingRequestId}";

        private const string Exception =
            "An Exception Occured to uploaded Lead for Quote to {InsurerName} and Rating Request Id {RatingRequestId} because {Message} ";

        private const string Start =
            "Started uploaded Lead for Quote to {InsurerName}. Product Code {ProductCode} Rating Request Id {RatingRequestId}. Request: {Request}";

        private const string Finish =
            "Finished uploading Lead for Quote to {InsurerName}. Product Code {ProductCode} Rating Request Id {RatingRequestId}. Response: {Response}";

        private const string FailNoQuote =
            "Quote with Rating Request Id {RatingRequestId} failed to upload to {InsurerName}.  No details were found in the Quote";

        private const string FailMessage =
            "Quote with Rating Request Id {RatingRequestId} failed to upload to {InsurerName} because of {Message}";

        private const string WarningMessage =
            "Warning for uploaded Lead for Quote to {InsurerName}. Rating Request Id {RatingRequestId} because of {Message} ";

        private const string CustomMessage = "Information for {InsurerName} with Rating ID: {RatingRequestId}. Message: {Message}";
        private const string DescribeRating = "Rating Request ID:  {RatingRequestId}";
        private const string DescribeDate = "Date:  {Date}";

        public static LeadTransferSuccessful CreateSuccesfull(string productCode, string insurerName, Guid ratingRequestId, Guid channelId)
        {
            return LeadTransferSuccessful
                    .WithChannelId(
                                Successful.Replace(ProductCode, productCode).Replace(InsurerName, insurerName).Replace(RatingRequestId, ratingRequestId.ToString()),
                                channelId);
        }

        public static LeadTransferFailed CreateFailure(string productCode, string insurerName, Guid ratingRequestId, Guid channelId)
        {
            return
                LeadTransferFailed.WithChannelId(
                    Failure.Replace(ProductCode, productCode).Replace(InsurerName, insurerName).Replace(RatingRequestId, ratingRequestId.ToString()), channelId);
        }

        public static LeadTransferFailed CreateFailureWithMessage(string insurerName, Guid ratingRequestId, string message, Guid channelId)
        {
            return LeadTransferFailed
                .WithChannelId(FailMessage.Replace(InsurerName, insurerName).Replace(RatingRequestId, ratingRequestId.ToString()).Replace(Message, message),
                    channelId);
        }

        public static LeadTransferInformation CreateWarningWithMessage(string insurerName, Guid ratingRequestId, string message, Guid channelId)
        {
            return LeadTransferInformation
                .WithChannelId(WarningMessage.Replace(InsurerName, insurerName).Replace(RatingRequestId, ratingRequestId.ToString()).Replace(Message, message),
                    Priority.Critical, channelId);
        }

        public static LeadTransferFailed CreateFailureNoQuote(string insurerName, Guid ratingRequestId, Guid channelId)
        {
            return LeadTransferFailed
                .WithChannelId(FailNoQuote.Replace(InsurerName, insurerName).Replace(RatingRequestId, ratingRequestId.ToString()), channelId);
        }

        public static LeadTransferException CreateException(string exceptionMessage, string insurerName, Guid ratingRequestId, Exception exception,
            Guid channelId)
        {
            return LeadTransferException
                .WithChannelId(exception,
                    Exception.Replace(InsurerName, insurerName).Replace(RatingRequestId, ratingRequestId.ToString()).Replace(Message, exceptionMessage),
                    insurerName, channelId);
        }

        public static LeadTransferStarted CreateStart(string productCode, string insurerName, Guid ratingRequestId, string request, Guid channelId)
        {
            return
                LeadTransferStarted.WithChannelId(
                    Start.Replace(ProductCode, productCode)
                        .Replace(InsurerName, insurerName)
                        .Replace(RatingRequestId, ratingRequestId.ToString())
                        .Replace(Request, request ?? ""), channelId, string.Join(" ,", new {productCode, insurerName, ratingRequestId}));
        }

        public static LeadTransferFinished CreateFinished(string productCode, string insurerName, Guid ratingRequestId, string response,
            Guid channelId)
        {
            return LeadTransferFinished
                .WithChannelId(
                    Finish.Replace(ProductCode, productCode)
                        .Replace(InsurerName, insurerName)
                        .Replace(RatingRequestId, ratingRequestId.ToString())
                        .Replace(Response, response ?? ""), channelId);
        }

        public static LeadTransferInformation CreateCustom(string message, Guid ratingRequestId, string insurerName, Guid channelId)
        {
            return
                LeadTransferInformation.WithChannelId(
                    CustomMessage.Replace(Message, message).Replace(RatingRequestId, ratingRequestId.ToString()).Replace(InsurerName, insurerName),
                    Priority.Normal, channelId);
        }

        public static LeadTransferEmailMessage CreateErrorMailMessage(string message, string error, Guid ratingRequestId, string insurerName,
            Guid channelId, CommunicationMetadata email, string environment = "")
        {
            var errorEmail = new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = email.CommunicationMessage.ErrorAddress,
                Subject = string.IsNullOrEmpty(environment) ? email.CommunicationMessage.Subject : string.Format("({0}) {1}", environment, email.CommunicationMessage.Subject),
                TemplateName = email.CommunicationMessage.TemplateName,
                MessageData = new LeadTransferCommunicationData(
                    FailMessage.Replace(InsurerName, insurerName).Replace(RatingRequestId, ratingRequestId.ToString()).Replace(Message, message), error,
                    DescribeDate.Replace(Date, DateTime.UtcNow.ToString("G")), DescribeRating.Replace(RatingRequestId, ratingRequestId.ToString())),
                AppSettingsEmailSetting = email.CommunicationMessage.AppSettingsEmailSetting
            });
            return LeadTransferEmailMessage.ErrorWithChannelId(errorEmail, channelId);
        }
        public static QuoteAcceptanceNotificationMessage CreateNotificationMessage(PublishQuoteUploadMessageDto publishQuoteUploadMessageDto, QuoteAcceptanceNotificationMetaData.QuoteUploadResult quoteUploadResult, string notificationAddress)
        {
            return new QuoteAcceptanceNotificationMessage(new QuoteAcceptanceNotificationMetaData(publishQuoteUploadMessageDto.GetQuoteNotificationDto(), notificationAddress, quoteUploadResult));
        }

        public static QuoteAcceptanceNotificationMessage CreateNotificationMessage(PolicyBindingRequestDto publishQuoteUploadMessageDto, QuoteAcceptanceNotificationMetaData.QuoteUploadResult quoteUploadResult, string notificationAddress)
        {
            return new QuoteAcceptanceNotificationMessage(new QuoteAcceptanceNotificationMetaData(publishQuoteUploadMessageDto.GetNotificationDto(), notificationAddress, quoteUploadResult));
        }

        public static PolicyBindingNotificationMessage CreateNotificationPdfEmailMessage(int channelId, int quoteId, int leadId, int partyId, int agentId, AgentDetailDto loggedAgent, int agentPartyId)
        {
            return new PolicyBindingNotificationMessage(new PolicyBindingNotificationMetaData(channelId, quoteId, leadId, partyId, agentId, loggedAgent, agentPartyId));
        }
    }
}