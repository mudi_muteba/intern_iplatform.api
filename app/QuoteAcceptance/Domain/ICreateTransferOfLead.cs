﻿using System.Collections.Generic;

namespace Workflow.QuoteAcceptance.Domain
{
    public interface ICreateTransferOfLead
    {
        IEnumerable<ITransferLead> Create(object message, object workflow);
    }

    public interface ICreateTransferOfLead<in T1, in T2> : ICreateTransferOfLead
    {
        IEnumerable<ITransferLead> Create(T1 message, T2 workflow);
    }
}
