﻿using Common.Logging;
using ToolBox.Communication.Emailing;
using ToolBox.Communication.Interfaces;
using ToolBox.Templating.Interfaces;
using Workflow.Domain;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain.EmailNotification;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class QuoteAcceptanceMessageConsumer : AbstractMessageConsumer<QuoteAcceptanceNotificationMessage>
    {
        private readonly QuoteAcceptanceMailer quoteMailer;
        private static readonly ILog log = LogManager.GetLogger<QuoteAcceptanceMessageConsumer>();

        public QuoteAcceptanceMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, ITemplateEngine templateEngine) 
            : base(router, executor)
        {
            quoteMailer = new QuoteAcceptanceMailer(templateEngine);
        }

        public override void Consume(QuoteAcceptanceNotificationMessage message)
        {
            var metadata = message.Metadata as QuoteAcceptanceNotificationMetaData;
            if (metadata == null|| metadata.Quote == null || string.IsNullOrEmpty(metadata.NotificationAddress))
            {
                log.ErrorFormat("Failed to send quote acceptance notification. {0} cannot is empty", typeof(QuoteAcceptanceNotificationMetaData));
                return;
            }

            quoteMailer.Send(metadata.Quote, metadata.NotificationAddress, metadata.UploadResult);
        }
    }
}
