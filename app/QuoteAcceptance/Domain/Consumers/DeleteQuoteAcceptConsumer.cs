﻿using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Shared;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Enums;
using Infrastructure.Configuration;
using Workflow.Domain;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class DeleteQuoteAcceptConsumer : AbstractMessageConsumer<DeleteQuoteAcceptMessage>
    {
        private readonly IConnector _connector;

        public DeleteQuoteAcceptConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
        }
        
        public override void Consume(DeleteQuoteAcceptMessage message)
        {
            var metaData = message.Metadata as LeadQuoteAcceptDeleteMetaData;
            _connector.QuotesManagement.Quote.DeleteQuoteAccept(new DeleteQuoteAcceptDto(metaData.QuoteId));
        }
    }
}
