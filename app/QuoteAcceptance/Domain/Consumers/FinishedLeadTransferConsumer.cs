﻿using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Monitoring;
using Workflow.Domain;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class FinishedLeadTransferConsumer : AbstractMessageConsumer<LeadTransferFinished>
    {
        private static readonly ILog Log = LogManager.GetLogger<LeadTransferFinished>();
        private readonly IMonitorLeadTransfer _monitorLeadTransfer;
        private readonly IBuildMonitoringDto<IWorkflowExecutionMessage> _monitoringDto;

        public FinishedLeadTransferConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IMonitorLeadTransfer monitorLeadTransfer,
            IBuildMonitoringDto<IWorkflowExecutionMessage> monitoringDto)
            : base(router, executor)
        {
            _monitorLeadTransfer = monitorLeadTransfer;
            _monitoringDto = monitoringDto;
        }

        public override void Consume(LeadTransferFinished message)
        {
            Log.InfoFormat("Finish Transferring a lead");
            _monitorLeadTransfer.Dispatch(_monitoringDto.Finished(message));
        }
    }
}