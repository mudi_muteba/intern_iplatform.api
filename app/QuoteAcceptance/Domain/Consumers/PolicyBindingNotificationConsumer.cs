﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.DocumentManagement;
using iPlatform.Api.DTOs.PolicyBindings;
using iPlatform.Api.DTOs.QuoteAcceptance;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.Base.Criteria;
using iPlatform.Enums;
using Infrastructure.Configuration;
using MasterData;
using Workflow.Domain;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;


namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class PolicyBindingNotificationConsumer : AbstractMessageConsumer<PolicyBindingNotificationMessage>
    {
        private static readonly ILog log = LogManager.GetLogger<PolicyBindingNotificationConsumer>();
        private readonly IConnector m_Connector;

        public PolicyBindingNotificationConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            m_Connector = connector;
            var token = m_Connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            m_Connector.SetToken(token);
        }

        public override void Consume(PolicyBindingNotificationMessage message)
        {
            var metadata = message.Metadata as PolicyBindingNotificationMetaData;
            if (metadata == null)
            {
                log.ErrorFormat("Failed to send Policy Binding PDF notification. {0} cannot is empty", typeof(PolicyBindingNotificationMetaData));
                return;
            }

            var quoteId = metadata.QuoteId; 
            var partyId = metadata.PartyId; 

            var agentId = metadata.AgentId;

            var parameters = new Dictionary<string, object>
            {
                {"QuoteId", quoteId},
                {"ChannelId", metadata.ChannelId},
                {"PartyId", partyId},
                {"AgentId", agentId},
                {"CurrentChannelId", metadata.ChannelId}
            };

            var reportList = new List<ReportDto>();
            var reports = m_Connector.ReportManagement.Reports.GetReportsByCriteria(new ReportCriteriaDto { ChannelId = metadata.ChannelId }).Response;

            var report = reports.FirstOrDefault(x => x.Name == "Policy Binding Question Answers");

            if (report == null)
            {
                log.ErrorFormat("Failed to get Report Policy Binding Question Answers. for channel {0}", metadata.ChannelId);
                return;
            }

            reportList.Add(report);

            var generatorDto = new ReportGeneratorDto()
            {
                ChannelId = metadata.ChannelId,
                Format = new ExportTypes().FirstOrDefault(x => x.Key.ToLower() == "pdf"),
                Parameters = parameters,
                Reports = reportList,
            };

            // Generate PDF
            var response = m_Connector.ReportManagement.Reports.GenerateReport(generatorDto).Response;
            var uri = response.Reports.FirstOrDefault();

            // Send attachement to Agent Email address
            var byteFile = GetFile(uri);
            SendEmailCommunication(byteFile, uri, metadata.ChannelId, metadata.AgentId, metadata.LoggedAgent);

            // Attach to document tab
            if (uri == null) return;
            var responseDocument = m_Connector.DocumentManagement.Documents.StoreDocument(new CreateDocumentDto
            {
                PartyId = partyId,
                CreatedById = metadata.AgentPartyId,
                FileName = new Uri(uri).Segments.Last(),
                InputStream = byteFile
            }).Response;

            if (responseDocument > 0)
            {
                log.Info("Sucessfully attached PDF to Document tab");
            }
            else
            {
                log.ErrorFormat("Failed to attach PDF to Document tab");
            }
        }

        private byte[] GetFile(string uri)
        {
            var response = new WebClient().DownloadData(uri);

            if (response.Length > 0)
            {
                return response;
            }

            return response;
        }

        private void SendEmailCommunication(byte[] file, string uri, int channelId, int agentId, AgentDetailDto loggedAgent)
        {
            var notificationresponse = m_Connector.PolicyBindingsManagement.PolicyBindings.SendEmailNotification(new PolicyBindingNotificationDto
            {
                ChannelId = channelId,
                Uri = uri,
                Bytefile = file,
                AgentId = agentId,
                LoggedAgent = loggedAgent
            }).Response;
        }
    }
}