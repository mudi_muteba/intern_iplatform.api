﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using CustomerSatisfactionSurvey.Domain;
using CustomerSatisfactionSurvey.Domain.Data;
using CustomerSatisfactionSurvey.Domain.Data.EchoTCF;
using CustomerSatisfactionSurvey.Domain.Helpers;
using CustomerSatisfactionSurvey.EchoTCF;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Shared;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using Infrastructure.Configuration;
using Workflow.Domain;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class CustomerSatisfactionSurveyConsumer : AbstractMessageConsumer<CustomerSatisfactionSurveyMessage>
    {
        private static readonly ILog Log = LogManager.GetLogger<CustomerSatisfactionSurveyConsumer>();
        public CustomerSatisfactionSurveyConsumer(IWorkflowRouter router, IWorkflowExecutor executor)
            : base(router, executor)
        {

        }

        public override void Consume(CustomerSatisfactionSurveyMessage message)
        {
            var dto = ((CustomerSatisfactionSurveyMetadata)message.Metadata).CustomerSatisfactionSurveyRequest;
            var request = GetRequest(dto);
            EchoTCFService service = new EchoTCFService(dto.EchoTCF.Baseurl);

            if (dto.EchoTCF.ByPass.Contains("true"))
            {
                Log.WarnFormat("ECHOTCF: Bypassing Echo TCF Call");
                return;
            }

            service.Send(request);
        }

        public CustomerSatisfactionSurveyRequest GetRequest(CustomerSatisfactionSurveyRequestDto dto)
        {
            var index = new ConsumerSatisfactionIndex
            {
                ExportInfo = new ExportInfo
                {
                    GeneralNotes = dto.GeneralNotes,
                    SystemUser = dto.SystemUser,
                    VendorSystem = dto.VendorSystem
                }.SetDateTime(dto.ExportedDateTime),

                GeneralPolicyClaimInfo = new GeneralPolicyClaimInfo
                {
                    TriggerEvent = dto.TriggerEvent,
                    EntityType = dto.EntityType,
                    EntityCode = dto.EntityCode,
                    EntityName = dto.EntityName,
                    PolicyHolder =  dto.PolicyHolder,
                    ClientType = dto.ClientType,
                    IDCoRegNo = dto. IDCoRegNo,
                    PolicyClaimNumber = dto.PolicyClaimNumber,
                    IsPolicy = dto.IsPolicy,
                    IsClaim = dto.IsClaim, 
                    PolicyHolderCell = dto.PolicyHolderCell,
                    PolicyHolderEmail = dto.PolicyHolderEmail,
                    InsurerName = dto.InsurerName,
                    InsuranceType = dto.InsuranceType,
                    EffectiveDate = dto.EffectiveDate.ToString("yyyy-MM-dd")
                }
            };

            return new CustomerSatisfactionSurveyRequest
            {
                XmlRequest = XmlHelper.ObjectToXml(index),
                Token = dto.EchoTCF.Token
            };
        }
    }
}
