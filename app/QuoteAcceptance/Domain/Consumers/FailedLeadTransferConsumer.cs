﻿using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Monitoring;
using Workflow.Domain;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class FailedLeadTransferConsumer : AbstractMessageConsumer<LeadTransferFailed>
    {
        private static readonly ILog Log = LogManager.GetLogger<FailedLeadTransferConsumer>();
        private readonly IMonitorLeadTransfer _monitorLeadTransfer;
        private readonly IBuildMonitoringDto<IWorkflowExecutionMessage> _monitoringDto;

        public FailedLeadTransferConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IMonitorLeadTransfer monitorLeadTransfer,
            IBuildMonitoringDto<IWorkflowExecutionMessage> monitoringDto)
            : base(router, executor)
        {
            _monitorLeadTransfer = monitorLeadTransfer;
            _monitoringDto = monitoringDto;
        }

        public override void Consume(LeadTransferFailed message)
        {
            Log.InfoFormat("Failed occured transferring a lead");
            _monitorLeadTransfer.Dispatch(_monitoringDto.Faliure(message));
        }
    }
}