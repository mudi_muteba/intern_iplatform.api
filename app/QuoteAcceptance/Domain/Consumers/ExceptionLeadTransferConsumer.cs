﻿using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Monitoring;
using Workflow.Domain;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class ExceptionLeadTransferConsumer : AbstractMessageConsumer<LeadTransferException>
    {
        private static readonly ILog Log = LogManager.GetLogger<ExceptionLeadTransferConsumer>();
        private readonly IMonitorLeadTransfer _monitorLeadTransfer;
        private readonly IBuildMonitoringDto<IWorkflowExecutionMessage> _monitoringDto;

        public ExceptionLeadTransferConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IMonitorLeadTransfer monitorLeadTransfer, IBuildMonitoringDto<IWorkflowExecutionMessage> monitoringDto)
            : base(router, executor)
        {
            _monitorLeadTransfer = monitorLeadTransfer;
            _monitoringDto = monitoringDto;
        }

        public override void Consume(LeadTransferException message)
        {
            Log.InfoFormat("An exception occurred while transferring a lead");
            _monitorLeadTransfer.Dispatch(_monitoringDto.Exception(message));
        }
    }
}