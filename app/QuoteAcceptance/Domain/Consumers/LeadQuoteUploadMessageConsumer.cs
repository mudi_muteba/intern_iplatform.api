﻿using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Shared;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums;
using Infrastructure.Configuration;
using Shared.Extentions;
using Workflow.Domain;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class LeadQuoteUploadMessageConsumer : AbstractMessageConsumer<LeadQuoteUploadMessage>
    {
        private readonly IConnector _connector;

        public LeadQuoteUploadMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
        }

        public override void Consume(LeadQuoteUploadMessage message)
        {
            this.Info(() => "Pushing Successful Upload to iPlatform");

            var dto = ((LeadQuoteUploadMetadata)message.Metadata).CreateQuoteUploadLogDto;
            if (_connector != null && dto != null && dto.QuoteId > 0)
            {
                this.Info(() => "Saving Quote Upload Log");

                var result = _connector.QuotesManagement.Quote.LogQuoteUploadSuccess(dto);
                if(result.IsSuccess)
                    this.Info(() => "Successful Saving Quote Upload Log");
                else
                    this.Error(() => string.Format("Error Saving Quote Upload Log for QuoteId {0}", dto.QuoteId.ToString()));
            }   
        }
    }
}