﻿using System;
using System.Linq;
using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using Workflow.Domain;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Metadata;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class QuoteUploadMessageConsumer : AbstractMessageConsumer<QuoteUploadMessage>
    {
        private static readonly ILog Log = LogManager.GetLogger<QuoteUploadMessageConsumer>();
        private readonly ICreateTransferOfLead<QuoteUploadMessage, WorkflowEngine> _transferer;

        public QuoteUploadMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, ICreateTransferOfLead<QuoteUploadMessage, WorkflowEngine> transferer) 
            : base(router,executor)
        {
            _transferer = transferer;
        }

        public override void Consume(QuoteUploadMessage message)
        {
            try
            {
                Log.InfoFormat("Consuming Lead Transfer Messages from Insurer");
                var destinations = _transferer.Create(message, Workflow).ToList();

                if (!destinations.Any())
                    throw new Exception(string.Format("Cannot find any lead transfers to transport lead to for Product Code {0}", message.Quote.Policy.ProductCode));

                Log.InfoFormat("Found {0} lead destinations to transfer lead to", destinations.Count());
                foreach (var destination in destinations)
                    message.Continue(new MessageProcessingResult(destination.Transfer().Result.Success), Workflow);
            }
            catch(Exception ex)
            {
                //revert quote acceptance status.
                Workflow.Publish(DeleteQuoteAcceptMessage.Create(message.Quote.Id));

                //monitoring
                Workflow.Publish(LeadTransferMessage.CreateException(ex.Message, message.Quote.Policy.InsurerName, message.Quote.Request.Id, ex, message.Quote.ChannelInfo.SystemId));

                //email
                Workflow.Publish(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(), message.Quote.Request.Id, message.Quote.Policy.InsurerName, message.Quote.ChannelInfo.SystemId, message.GetMetadata<QuoteAcceptedMetadata>().Mail, message.Quote.QuoteAcceptanceEnvironment));
            }
        }
    }
}
