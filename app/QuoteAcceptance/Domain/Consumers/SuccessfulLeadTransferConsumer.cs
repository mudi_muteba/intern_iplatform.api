﻿using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Monitoring;
using Workflow.Domain;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class SuccessfulLeadTransferConsumer : AbstractMessageConsumer<LeadTransferSuccessful>
    {
        private static readonly ILog Log = LogManager.GetLogger<SuccessfulLeadTransferConsumer>();
        private readonly IMonitorLeadTransfer _monitorLeadTransfer;
        private readonly IBuildMonitoringDto<IWorkflowExecutionMessage> _monitoringDto;

        public SuccessfulLeadTransferConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IMonitorLeadTransfer monitorLeadTransfer,
            IBuildMonitoringDto<IWorkflowExecutionMessage> monitoringDto)
            : base(router, executor)
        {
            _monitorLeadTransfer = monitorLeadTransfer;
            _monitoringDto = monitoringDto;
        }

        public override void Consume(LeadTransferSuccessful message)
        {
            Log.InfoFormat("Successfully transferred a lead");

            _monitorLeadTransfer.Dispatch(_monitoringDto.Successful(message));

        }
    }
}