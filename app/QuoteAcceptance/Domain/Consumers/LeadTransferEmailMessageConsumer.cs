﻿using Common.Logging;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using ToolBox.Communication.Emailing;
using ToolBox.Communication.Emailing.Smtp;
using ToolBox.Templating.Interfaces;
using Workflow.Domain;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class LeadTransferEmailMessageConsumer : AbstractMessageConsumer<LeadTransferEmailMessage>
    {
        private static readonly ILog Log = LogManager.GetLogger<LeadTransferEmailMessageConsumer>();
        private readonly ITemplateEngine _templateEngine;

        public LeadTransferEmailMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, ITemplateEngine templateEngine)
            : base(router, executor)
        {
            _templateEngine = templateEngine;
        }

        public override void Consume(LeadTransferEmailMessage message)
        {
            var communicationMetadata = message.GetCommunicationMetadata();
            var sendCommunication = new EmailCommunicator(_templateEngine, new SmtpClientAdapter(communicationMetadata.AppSettingsEmailSetting), communicationMetadata.AppSettingsEmailSetting);

            Log.InfoFormat("Sending lead transfer mail to {0}", communicationMetadata.Address);
            sendCommunication.Send(communicationMetadata);
            Log.InfoFormat("Sent lead transfer mail to {0}", communicationMetadata.Address);
        }
    }
}
