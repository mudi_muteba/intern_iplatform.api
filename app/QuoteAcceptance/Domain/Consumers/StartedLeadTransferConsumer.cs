﻿using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Monitoring;
using Workflow.Domain;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain.Consumers
{
    public class StartedLeadTransferConsumer : AbstractMessageConsumer<LeadTransferStarted>
    {
        private static readonly ILog Log = LogManager.GetLogger<StartedLeadTransferConsumer>();
        private readonly IMonitorLeadTransfer _monitorLeadTransfer;
        private readonly IBuildMonitoringDto<IWorkflowExecutionMessage> _monitoringDto;
        public StartedLeadTransferConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IMonitorLeadTransfer monitorLeadTransfer, IBuildMonitoringDto<IWorkflowExecutionMessage> monitoringDto)
            : base(router, executor)
        {
            _monitorLeadTransfer = monitorLeadTransfer;
            _monitoringDto = monitoringDto;
        }

        public override void Consume(LeadTransferStarted message)
        {
            Log.InfoFormat("Started transferring lead");
            _monitorLeadTransfer.Dispatch(_monitoringDto.Started(message));
        }
    }
}