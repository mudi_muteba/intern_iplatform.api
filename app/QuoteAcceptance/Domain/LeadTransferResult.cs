﻿using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain
{
    public class LeadTransferResult
    {
        public LeadTransferResult(bool success)
        {
            Success = success;
        }

        public bool Success { get; private set; }
    }
}