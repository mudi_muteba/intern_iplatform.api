﻿using System.Collections.Generic;
namespace Workflow.QuoteAcceptance.Domain
{
    public abstract class AbstractTransferOfLeadFactory<T1, T2> : ICreateTransferOfLead<T1, T2>
    {
        public abstract IEnumerable<ITransferLead> Create(T1 message, T2 workflow);

        public IEnumerable<ITransferLead> Create(object message, object workflow)
        {
            return Create((T1)message, (T2)workflow);
        }
    }
}
