﻿using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain
{
    public interface ITransferLeadToInsurer
    {
        ExecutionPlan ExecutionPlan { get; }
        IRetryStrategy RetryStrategy { get; }
        LeadTransferResult Transfer(QuoteUploadMessage message);
    }
}