﻿using System;
using System.Collections.Generic;
using Domain.QuoteAcceptance.Leads;
using Workflow.Messages;

namespace Workflow.QuoteAcceptance.Domain
{
    public interface IConfigureTransferOfAcceptedQuote
    {
        IDictionary<IEnumerable<string>, Func<QuoteUploadMessage, WorkflowEngine, ITransferLead>> Insurers { get; }
    }
}