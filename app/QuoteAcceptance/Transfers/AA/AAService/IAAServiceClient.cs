﻿using System.ServiceModel;

namespace Workflow.QuoteAcceptance.AA.AAService
{
    public partial class CommandClient : IAAServiceClient
    {
    }

    public interface IAAServiceClient
    {
        string Submit(string inMessage);
    }

    public partial class CommandClient
    {
        public CommandClient(string endpointConfigurationName) 
        {
        }

        public CommandClient(string endpointConfigurationName, string remoteAddress) 
        {
        }

        public CommandClient(string endpointConfigurationName, EndpointAddress remoteAddress)
        {
        }

        public CommandClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) 
        {
        }

        public string Submit(string inMessage)
        {
            return "Success/True";
        }
    }
}
