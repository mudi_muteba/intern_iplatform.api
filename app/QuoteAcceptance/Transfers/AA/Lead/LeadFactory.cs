﻿using System;
using System.Linq;
using System.Text;
using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using AutoMapper;

namespace Workflow.QuoteAcceptance.AA.Lead
{
    public class LeadFactory
    {
        private static readonly ILog Log = LogManager.GetLogger<LeadFactory>();
        private readonly PublishQuoteUploadMessageDto _quote;
        private readonly StringBuilder _lead;
        public LeadFactory(QuoteUploadMessage message)
        {
            _lead = new StringBuilder();
            _quote = message.Quote;
        }

        public string CreateLead()
        {
            var validation = ValidateQuote();
            if (!string.IsNullOrEmpty(validation)) throw new Exception(string.Format("Validation for AA failed with message: {0}", validation));

            try
            {
                if (_quote.Request.Items.Count <= 0) return _lead.ToString();

                ValidateMonthlyAnnual();
                CreateHeader();
                CreatePolicy();
                CreateFooter();

            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating AA Lead Request exception: {0} {1}", ex.Message, _lead.ToString());
                throw;
            }
            return _lead.ToString();
        }

        private void ValidateMonthlyAnnual()
        {
            if (_quote.Policy.ProductCode == "AA" || _quote.Policy.ProductCode == "AAC" )
            {
                _quote.Request.Policy.PaymentPlan.Id = PaymentPlans.Annual.Id;
                _quote.Request.Policy.PaymentPlan.Name = PaymentPlans.Annual.Name;

                foreach (var item in _quote.Policy.Items)
                {
                    item.Premium = item.Premium * 12;
                    item.PremiumWithFees = item.PremiumWithFees * 12;
                }
            }
        }

        private void CreateFooter()
        {
            Log.Debug("Creating AA Lead Header.");
            try
            {
                _lead.AppendLine("</PayLoad>");
                _lead.AppendLine("</STPCommand>");

            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating AA Lead Footer exception: {0}", ex.Message);
                throw;
            }
        }

        private void CreateHeader()
        {
            Log.Debug("Creating AA Lead Header.");
            try
            {
                if (!string.IsNullOrEmpty(_lead.ToString()))
                    _lead.Clear();

                _lead.AppendLine("<STPCommand>");
                _lead.AppendLine("<Source>iPlatform</Source>");
                _lead.AppendLine("<Function>Decompose</Function>");
                _lead.AppendLine("<Parameters>");
                _lead.AppendLine("<InterfaceTypeID>iPlatform_Quote</InterfaceTypeID>");
                _lead.AppendLine("<RequestGuid>" + _quote.Request.PlatformId + "</RequestGuid>");
                _lead.AppendLine("</Parameters>");
                _lead.AppendLine("<PayLoad>");
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating AA Lead Header exception because {0}", ex, ex.Message);
                throw;
            }
        }

        private void CreatePolicy()
        {
            Log.Debug("Creating AA Lead Policy.");
            try
            {
                var quotedto = Mapper.Map<QuoteDto>(_quote);

                var quoteToUpload = quotedto.SerializeAsXml();
                //var quoteToUpload = _quote.SerializeAsXmlQuestionAnswer();
                _lead.AppendLine(quoteToUpload.Replace(@"<?xml version=""1.0""?>", ""));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating AA Lead Policy exception because {0}", ex, ex.Message);
                throw;
            }
        }

        private string ValidateQuote()
        {
            if (_quote == null)
                return "Quote Dto in null";
            if (_quote.InsuredInfo == null)
                return "No Insured Info in Quote Response Dto";
            if (_quote.Policy == null)
                return "No Policy Info in Quote Response Dto";
            if (_quote.Policy.Items == null)
                return "No Items in Quote Response Dto";
            if (_quote.Request.Policy == null)
                return "No Policy Info in Quote Request Dto";
            return _quote.Request.Items == null ? "No Items in Quote Request Dto" : "";
        }
    }
}
