﻿using Common.Logging;
using Workflow.QuoteAcceptance.Hollard.HollardService;

namespace Workflow.QuoteAcceptance.Hollard.Lead
{
    public static class LeadTransfer
    {
        private static readonly ILog Log = LogManager.GetLogger("LeadTransfer");

        public static Response TransferLead(IHollardServiceClient client, string authToken, string lead)
        {
            Log.Info("Uploading Quote to Hollard Service");
            var response = client.SendQuoteRequest(authToken, lead);
            return response;
        }
    }
}

