﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using iPlatform.Api.DTOs.Domain;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead
{
    public class LeadFactory
    {
        private static readonly ILog Log = LogManager.GetLogger<LeadFactory>();
        private readonly StringBuilder _lead = new StringBuilder();
        private readonly PublishQuoteUploadMessageDto _quote;
        private readonly LeadTransferHollardTaskMetadata _metadata;

        public LeadFactory(QuoteUploadMessage message)
        {
            _quote = message.Quote;
            _metadata = message.GetMetadata<QuoteAcceptedMetadata>().TaskMetadata.Get<LeadTransferHollardTaskMetadata>();
        }

        public string CreateLead()
        {
            Log.Info("Creating Hollard Lead Request.");
            var validation = ValidateQuote();

            if (validation != "")
            {
                Log.ErrorFormat("Validation failed for hollard lead with message: {0}", validation);
                return validation;
            }

            try
            {
                if (_quote.Request.Items.Count <= 0 ||
                    (_quote.Request.Items.Count(c => c.Cover.Id.Equals(Covers.Motor.Id)) <= 0 &&
                     _quote.Request.Items.Count(c => c.Cover.Id.Equals(Covers.Contents.Id)) <= 0 &&
                     _quote.Request.Items.Count(c => c.Cover.Id.Equals(Covers.Building.Id)) <= 0 &&
                     _quote.Request.Items.Count(c => c.Cover.Id.Equals(Covers.AllRisk.Id)) <= 0)) return _lead.ToString();
                CreateHeader();
                CreatePolicy();

                _lead.AppendLine("[EOF]");
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating Hollard Lead Request exception: {0} {1}", ex.Message, _lead.ToString());
                throw;
            }

            Log.InfoFormat("Lead: {0}",  _lead.ToString());
            return _lead.ToString();
        }

        private void CreateHeader()
        {
            Log.Info("Creating Hollard Lead Header.");
            try
            {
                if (!string.IsNullOrEmpty(_lead.ToString()))
                    _lead.Clear();

                _lead.AppendLine("[QTM HBF]");
                _lead.AppendLine("................");
                _lead.AppendLine("");
                _lead.AppendLine("[Transform]");
                _lead.AppendLine("Version 000;");
                _lead.AppendLine("");
                _lead.AppendLine("[QMASTER1]");
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating Hollard Lead Header exception: {0}", ex.Message);
                throw;
            }
        }

        private void CreatePolicy()
        {

            Log.Info("Creating Hollard Lead Policy.");

            _lead.AppendLineAnswer("Quote.QmClientNo", _quote.Request.Id.ToString());
            _lead.AppendLineAnswer("Quote.EffectiveDate", DateTime.Today.ToString("yyyyMMdd"));
            _lead.AppendLineAnswer("Quote.PolicyInstallmentDueDay", "1");
            _lead.AppendLineAnswer("Quote.QuoteDate", DateTime.Today.ToString("yyyyMMdd"));
            _lead.AppendLineAnswer("Quote.SchemeCode", _metadata.SchemeCode ?? "634"); //AMS
            _lead.AppendLineAnswer("Campaign.Campaign", _metadata.Campaign ?? "515"); //AMS
            _lead.AppendLineAnswer("Campaign.CampaignType", _metadata.CampaignType ?? "89"); //AMS
            _lead.AppendLineAnswer("Campaign.ExternalQuoteNo", _quote.Request.Id.ToString());
            _lead.AppendLineAnswer("Quote.ItcScore", _quote.Policy.ITCScore ?? "8");

            CreateInsuredPerson();
            _lead.AppendLine("");

            CreateContentDetail(); //QHH
            _lead.AppendLine("");

            CreateAllRiskDetail(); //QARITEMS
            _lead.AppendLine("");

            CreateBuildingDetail(); //QHO
            _lead.AppendLine("");

            CreateVehicleDetail(); //QPC
            _lead.AppendLine("");
        }

        private void CreateInsuredPerson()
        {
            Log.Info("Creating Hollard Lead Insured Person.");
            try
            {
                var idNumberInfo = new IdentityNumberInfo("");
                if (_quote.InsuredInfo != null && _quote.InsuredInfo.IDNumber != null)
                    idNumberInfo = new IdentityNumberInfo(_quote.InsuredInfo.IDNumber);

                var address = new RatingRequestAddressDto();
                if (_quote.Request != null && _quote.Request.Policy != null && _quote.Request.Policy.Persons != null)
                    address = _quote.Request.Policy.Persons[0].Addresses.FirstOrDefault();

                _lead.AppendLineAnswer("ClientDetails.IdentityNo", string.IsNullOrEmpty(idNumberInfo.IdentityNumber) ? _quote.Request.Id.ToString() : idNumberInfo.IdentityNumber);

                _lead.AppendLineAnswer("ClientDetails.TitleCode",
                    MappingFactory.Map("Title", _quote.InsuredInfo.Title.Code));

                _lead.AppendLineAnswer("ClientDetails.FirstName", string.IsNullOrEmpty(_quote.InsuredInfo.Firstname)
                    ? "Unknown"
                    : _quote.InsuredInfo.Firstname);

                _lead.AppendLineAnswer("ClientDetails.Initials",
                    string.IsNullOrEmpty(_quote.InsuredInfo.Firstname)
                        ? ""
                        : _quote.InsuredInfo.Firstname.Substring(0, 1));

                _lead.AppendLineAnswer("ClientDetails.Name", string.IsNullOrEmpty(_quote.InsuredInfo.Surname)
                    ? "Unknown"
                    : _quote.InsuredInfo.Surname);

                _lead.AppendLineAnswer("ClientDetails.Gender", MappingFactory.Map("Gender", idNumberInfo.Gender.Code));

                //lead.AppendLineAnswer("ClientDetails.DateOfBirth", idNumberInfo.BirthDate.ToString("yyyyMMdd"));
                _lead.AppendLineAnswer("ClientDetails.DateOfBirth", idNumberInfo.BirthDate.ToString("yyyyMMdd") ?? _quote.InsuredInfo.DateOfBirth.ToString("yyyyMMdd"));

                _lead.AppendLineAnswer("ClientDetails.EmailAddress",
                    string.IsNullOrEmpty(_quote.InsuredInfo.EmailAddress)
                        ? "Unknown"
                        : _quote.InsuredInfo.EmailAddress);

                _lead.AppendLineAnswer("ClientDetails.MaritalStatus",
                    MappingFactory.Map("MaritalStatus", _quote.InsuredInfo.MaritalStatus.Code));

                _lead.AppendLineAnswer("ClientTelNo.Cell", string.IsNullOrEmpty(_quote.InsuredInfo.ContactNumber)
                    ? "Unknown"
                    : _quote.InsuredInfo.ContactNumber);

                _lead.AppendLineAnswer("ClientTelNo.Home", "");

                _lead.AppendLineAnswer("ClientTelNo.Work", "");

                _lead.AppendLineAnswer("ClientDetails.LanguageCode",
                    MappingFactory.Map("Language", idNumberInfo.Gender.Code));

                if (_quote.Request != null && _quote.Request.Policy != null)
                    _lead.AppendLineAnswer("Quote.PaymentFrequency",
                        MappingFactory.Map("PaymentPlan", _quote.Request.Policy.PaymentPlan.Name));

                _lead.AppendLineAnswer("Quote.ConfirmationEmail", _metadata.AcceptancEmailAddress);

                if (address != null)
                {
                    Log.Debug("Creating Address node");
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine1", string.IsNullOrEmpty(address.Address1) ? string.Empty : address.Address1);
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine2", string.IsNullOrEmpty(address.Address2) ? string.Empty : address.Address2);
                    _lead.AppendLineAnswer("QuoteClientAddress.PostalCode", string.IsNullOrEmpty(address.PostalCode) ? string.Empty : address.PostalCode.PadLeft(4, '0'));
                    _lead.AppendLineAnswer("QuoteClientAddress.SuburbName", address.Suburb);

                    _lead.AppendLineAnswer("PostalAddress.AddressLine1", string.IsNullOrEmpty(address.Address1) ? string.Empty : address.Address1);
                    _lead.AppendLineAnswer("PostalAddress.AddressLine2", string.IsNullOrEmpty(address.Address2) ? string.Empty : address.Address2);
                    _lead.AppendLineAnswer("PostalAddress.PostalCode", string.IsNullOrEmpty(address.PostalCode) ? string.Empty : address.PostalCode.PadLeft(4, '0'));
                    _lead.AppendLineAnswer("PostalAddress.SuburbName", address.Suburb);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating Hollard Lead Insured Person exception: {0}", ex.Message);
                throw;
            }
        }

        private void CreateVehicleDetail()
        {
            Log.Info("Creating Hollard Lead Vehicle.");
            var i = 1;
            try
            {
                foreach (var vehicle in _quote.Request.Items)
                {
                    if (!Equals(vehicle.Cover.Id, Covers.Motor.Id)) continue;

                    var idNumberInfo =
                        new IdentityNumberInfo(
                            ConversionFactory<string>.ConvertAnswer(vehicle, Questions.MainDriverIDNumber) ??
                            DateTime.Today.Date.ToString(CultureInfo.InvariantCulture));

                    var person =
                        _quote.Request.Policy.Persons.FirstOrDefault(p => p.IdNumber == idNumberInfo.IdentityNumber) ??
                        _quote.Request.Policy.Persons.FirstOrDefault(p => p.IdNumber == _quote.InsuredInfo.IDNumber) ??
                        new RatingRequestPersonDto();

                    var premium =
                        _quote.Policy.Items.FirstOrDefault(
                            prem => prem.AssetNo == vehicle.AssetNo && Equals(prem.Cover, vehicle.Cover));

                    var postalCode = ConversionFactory<string>.ConvertAnswer(vehicle, Questions.PostalCode);

                    var address =
                        _quote.Request.Policy.Persons[0].Addresses.FirstOrDefault(
                            a =>
                                a.PostalCode == postalCode &&
                                a.Suburb == ConversionFactory<string>.ConvertAnswer(vehicle, Questions.Suburb)) ??
                        new RatingRequestAddressDto();

                    _lead.AppendLine("[QPC" + i + "]");
                    _lead.AppendLineAnswer("QuoteDriverDetails.IdentityNo", string.IsNullOrEmpty(idNumberInfo.IdentityNumber) ? _quote.Request.Id.ToString() : idNumberInfo.IdentityNumber);

                    _lead.AppendLineAnswer("QuoteDriverDetails.Title", person.Title.Code);
                    _lead.AppendLineAnswer("QuoteDriverDetails.FirstName", person.Initials);
                    _lead.AppendLineAnswer("QuoteVehicleDriver.Surname", person.Surname);
                    //lead.AppendLineAnswer("QuoteVehicleDriver.Initials", person.Initials);
                    
                    _lead.AppendLineAnswer("QuoteDriverDetails.DateOfBirth", idNumberInfo.BirthDate.ToString("yyyyMMdd"));
                    _lead.AppendLineAnswer("QuoteDriverDetails.Gender",
                        MappingFactory.Map("Gender", idNumberInfo.Gender.Code));
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine1", string.IsNullOrEmpty(address.Address1) ? string.Empty : address.Address1);
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine2", string.IsNullOrEmpty(address.Address2) ? string.Empty : address.Address2);

                    _lead.AppendLineAnswer("QuoteClientAddress.PostalCode", string.IsNullOrEmpty(postalCode) ? string.Empty : postalCode.PadLeft(4, '0'));

                    _lead.AppendLineAnswer("QuoteClientAddress.SuburbName",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.Suburb));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.InsurableEventCode", "C");
                    _lead.AppendLineAnswer("QuoteDriverDetails.MaritalStatus",
                        MappingFactory.Map("MaritalStatus", person.MaritalStatus.Code));
                    _lead.AppendLineAnswer("QuoteDriverDetails.Licencetype",
                        MappingFactory.Map(Questions.DriversLicenceType.Name,
                            ConversionFactory<string>.ConvertAnswer(vehicle, Questions.DriversLicenceType)));
                    _lead.AppendLineAnswer("QuoteDriverDetails.MonthDriverLicenceIssued",
                        ConversionFactory<DateTime>.ConvertAnswer(vehicle, Questions.DriversLicenceFirstIssuedDate)
                            .ToString("MM"));
                    _lead.AppendLineAnswer("QuoteDriverDetails.YearDriverLicenceIssued",
                        ConversionFactory<DateTime>.ConvertAnswer(vehicle, Questions.DriversLicenceFirstIssuedDate)
                            .ToString("yyyy"));

                    _lead.AppendLineAnswer("QuoteInsEventBenefit.ProductCode", _metadata.ProductCodeMotor ?? "70");
                    //lead.AppendLineAnswer("QuoteInsEventBenefit.ProductCode",
                    //    ConfigurationHelper.Read("Hollard/Upload/ProductCode/Motor", true, "70"));
                    
                    if (premium != null)
                    {
                        _lead.AppendLineAnswer("QuoteInsEventBenefit.Premium", premium.Premium.ToString("##.###"));
                        _lead.AppendLineAnswer("QuoteInsEventBenefit.RatePerK", "0.14");
                        _lead.AppendLineAnswer("QuoteInsEventBenefit.SasriaPremium", premium.Sasria.ToString("##.###"));
                    }
                    _lead.AppendLineAnswer("QuoteSOILoss.PeriodOfUninterruptedCover",
                        (ConversionFactory<int>.ConvertAnswer(vehicle, Questions.NoClaimBonus) > 6 //changed for hollard Questions.CurrentInsurancePeriod
                            ? 6
                            : ConversionFactory<int>.ConvertAnswer(vehicle, Questions.NoClaimBonus)).ToString(
                                CultureInfo.InvariantCulture));

                    _lead.AppendLineAnswer("QuoteSOILoss.NCB","0"); //changed for hollard  Questions.NoClaimBonus

                    //lead.AppendLineAnswer("QuoteSOILoss.NCB",
                    //    ConversionFactory<string>.ConvertAnswer(vehicle, Questions.NoClaimBonus)); //changed for hollard  Questions.NoClaimBonus
                    
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn12Months",
                        ConversionFactory<int>.ConvertAnswer(vehicle, Questions.ClaimsLast0to12Months)
                            .ToString(CultureInfo.InvariantCulture));
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn24Months",
                        ConversionFactory<int>.ConvertAnswer(vehicle, Questions.ClaimsLast12to24Months)
                            .ToString(CultureInfo.InvariantCulture));
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn36Months",
                        ConversionFactory<int>.ConvertAnswer(vehicle, Questions.ClaimsLast24to36Months)
                            .ToString(CultureInfo.InvariantCulture));

                    if (ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleTrackingDevice) != "")
                        _lead.AppendLineAnswer("QuoteDataCollectionDeviceSOI.DataCollectionDeviceCode",
                            MappingFactory.Map(Questions.VehicleTrackingDevice.Name + "Code",
                                ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleTrackingDevice)));

                    _lead.AppendLineAnswer("QuoteVehicleDetails.RegistrationNo",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleRegistrationNumber));

                    if (ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleImmobiliser) != "")
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode",
                            MappingFactory.Map(Questions.VehicleImmobiliser.Name,
                                ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleImmobiliser)));

                    if (ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleTrackingDevice) != "")
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode",
                            MappingFactory.Map(Questions.VehicleTrackingDevice.Name,
                                ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleTrackingDevice)));

                    if (ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleGaraging) != "")
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode",
                            MappingFactory.Map(Questions.VehicleGaraging.Name,
                                ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleGaraging)));

                    if (ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleGearlock) != "")
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode",
                            MappingFactory.Map(Questions.VehicleGearlock.Name,
                                ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleGearlock)));

                    bool modified;
                    Boolean.TryParse(ConversionFactory<string>.ConvertAnswer(vehicle, Questions.ModifiedImported),
                        out modified);
                    _lead.AppendLineAnswer("QuoteVehicleModification.ModificationCode",
                        modified.ToYesNoStringShort());

                    bool carHire;
                    bool.TryParse((MappingFactory.Map(Questions.CarHire.Name,
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.CarHire))), out carHire);
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.CarHire",
                        carHire.ToYesNoStringShort());

                    _lead.AppendLineAnswer("QuoteVehicleExtras.ExtraType", "1");
                    _lead.AppendLineAnswer("QuoteVehicleExtras.Value",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleExtrasValue));
                    _lead.AppendLineAnswer("QuoteVehicleExtras.RadioValue",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.RadioValue));
                    _lead.AppendLineAnswer("QuoteVehicleExtras.Description3",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.RadioMake) +
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.RadioModel) == ""
                            ? "Unknown"
                            : ConversionFactory<string>.ConvertAnswer(vehicle, Questions.RadioMake) + " " +
                              ConversionFactory<string>.ConvertAnswer(vehicle, Questions.RadioModel));
                    _lead.AppendLineAnswer("QuoteVehicleDetails.AverageMonthlyMileage", "0");
                    _lead.AppendLineAnswer("QuoteVehicleDetails.ManufactureYear",
                        ConversionFactory<int>.ConvertAnswer(vehicle, Questions.YearOfManufacture)
                            .ToString(CultureInfo.InvariantCulture));
                    _lead.AppendLineAnswer("QuoteVehicleDetails.VehicleType",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleMMCode));
                    _lead.AppendLineAnswer("VehicleType.VehicleMakeCode",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleMake));
                    _lead.AppendLineAnswer("VehicleType.VehicleModelCode",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VehicleModel));
                    _lead.AppendLineAnswer("QuoteVehicleDetails.VehicleUsedFor", "1");
                    _lead.AppendLineAnswer("VehicleModelMonthlyValues.TradeValue",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.SumInsured));
                    _lead.AppendLineAnswer("VehicleModelMonthlyValues.RetailsValue",
                        ConversionFactory<string>.ConvertAnswer(vehicle, Questions.SumInsured));

                    _lead.AppendLineAnswer("QuoteInsEventBenefit.ExcessType",
                        MappingFactory.Map("SelectedExcess", ConversionFactory<string>.ConvertAnswer(vehicle, Questions.VoluntaryExcess)));

                    i++;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating Hollard Lead Vehicle exception: {0}", ex.Message);
                throw;
            }


        }

        private void CreateContentDetail()
        {
            Log.Info("Creating Hollard Lead Content.");
            var i = 1;
            try
            {
                foreach (var content in _quote.Request.Items)
                {
                    if (!Equals(content.Cover.Id, Covers.Contents.Id)) continue;

                    var premium =
                        _quote.Policy.Items.FirstOrDefault(
                            prem => prem.AssetNo == content.AssetNo && Equals(prem.Cover, content.Cover));

                    _lead.AppendLine("[QHH" + i + "]");
                    
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine1",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.RiskAddress));
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine2", "");
                    _lead.AppendLineAnswer("QuoteClientAddress.PostalCode",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.PostalCode));
                    _lead.AppendLineAnswer("QuoteClientAddress.SuburbCode",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.Suburb));

                    _lead.AppendLineAnswer("QuoteInsEventBenefit.InsurableEventCode", "508");

                    //lead.AppendLineAnswer("QuoteSOILoss.PeriodOfUninterruptedCover", "0");
                        
                    _lead.AppendLineAnswer("QuoteSOILoss.PeriodOfUninterruptedCover",
                        string.IsNullOrEmpty(ConversionFactory<string>.ConvertAnswer(content, Questions.CurrentInsurancePeriod)) ? "0" :
                        ConversionFactory<string>.ConvertAnswer(content, Questions.CurrentInsurancePeriod));
                    
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn12Months",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.ClaimsLast0to12Months));
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn24Months",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.ClaimsLast12to24Months));
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn36Months",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.ClaimsLast24to36Months));
                    _lead.AppendLineAnswer("QuoteSOILoss.NCB",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.NoClaimBonus));
                    _lead.AppendLineAnswer("QuoteBuilding.DwellingType",
                        MappingFactory.Map(Questions.TypeOfResidence.Name,
                            ConversionFactory<string>.ConvertAnswer(content, Questions.TypeOfResidence)));
                    _lead.AppendLineAnswer("QuoteBuilding.RoofType",
                        MappingFactory.Map(Questions.RoofConstruction.Name,
                            ConversionFactory<string>.ConvertAnswer(content, Questions.RoofConstruction)));
                    _lead.AppendLineAnswer("QuoteBuilding.StructureType ",
                        MappingFactory.Map(Questions.WallConstruction.Name,
                            ConversionFactory<string>.ConvertAnswer(content, Questions.WallConstruction)));
                    _lead.AppendLineAnswer("QuoteBuilding.ThatchSafeInd",
                        ConversionFactory<bool>.ConvertAnswer(content, Questions.ThatchSafe).ToYesNoStringShort());
                    _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.ElectricFence",
                        ConversionFactory<bool>.ConvertAnswer(content, Questions.ElectricFencing).ToYesNoStringShort());
                    _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode",
                        MappingFactory.Map(Questions.RoofConstruction.Name + "Lightning",
                            ConversionFactory<string>.ConvertAnswer(content, Questions.RoofConstruction)));

                    if (ConversionFactory<bool>.ConvertAnswer(content, Questions.ElectricFencing))
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode", "104");

                    _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode",
                        ConversionFactory<bool>.ConvertAnswer(content, Questions.Plot) ? "141" : "142");

                    _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode",
                        MappingFactory.Map(Questions.AlarmType.Name,
                            ConversionFactory<string>.ConvertAnswer(content, Questions.AlarmType)));

                    if (ConversionFactory<bool>.ConvertAnswer(content, Questions.BurglarBarsonPassageSide))
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode", "116");

                    if (ConversionFactory<bool>.ConvertAnswer(content, Questions.BurglarBars))
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode", "114");

                    if (ConversionFactory<bool>.ConvertAnswer(content, Questions.SpanishBars))
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode", "115");

                    _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode",
                        ConversionFactory<bool>.ConvertAnswer(content, Questions.SecurityGate) ? "113" : "112");

                    if (ConversionFactory<bool>.ConvertAnswer(content, Questions.GolfCourse))
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode", "132");

                    if (MappingFactory.Map(Questions.Unoccupied.Name + "Daytime",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.Unoccupied)) == "true")
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode", "134");

                    if (ConversionFactory<bool>.ConvertAnswer(content, Questions.AccessControlledSuburb))
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode", "123");

                    if (ConversionFactory<bool>.ConvertAnswer(content, Questions.SecureComplex))
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode", "122");

                    if (ConversionFactory<bool>.ConvertAnswer(content, Questions.PerimeterWall))
                        _lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode", "121");

                    _lead.AppendLineAnswer("QuoteBuilding.DaysUnoccupied",
                        MappingFactory.Map(Questions.Unoccupied.Name,
                            ConversionFactory<string>.ConvertAnswer(content, Questions.Unoccupied)));
                    _lead.AppendLineAnswer("QuoteBuilding.HolidayHomeInd",
                        ConversionFactory<bool>.ConvertAnswer(content, Questions.HolidayHome).ToYesNoStringShort());
                    _lead.AppendLineAnswer("QuoteHouseContent.SumInsured",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.SumInsured));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.Premium",
                        premium == null ? "0" : premium.Premium.ToString("##.###"));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.SasriaPremium",
                        premium == null ? "0" : premium.Sasria.ToString("##.###"));

                    _lead.AppendLineAnswer("QuoteInsEventBenefit.ProductCode", _metadata.ProductCodeHh ?? "4");

                    //lead.AppendLineAnswer("QuoteInsEventBenefit.ProductCode",
                    //    ConfigurationHelper.Read("Hollard/Upload/ProductCode/HH", true, "4"));
                    i++;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating Hollard Lead Content exception: {0}", ex.Message);
                throw;
            }
        }

        private void CreateBuildingDetail()
        {
            Log.Info("Creating Hollard Lead Building.");
            var i = 1;
            try
            {
                foreach (var content in _quote.Request.Items)
                {
                    if (!Equals(content.Cover.Id, Covers.Building.Id)) continue;

                    var premium =
                        _quote.Policy.Items.FirstOrDefault(
                            prem => prem.AssetNo == content.AssetNo && Equals(prem.Cover, content.Cover));

                    _lead.AppendLine("[QHO" + i + "]");
                    
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine1",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.RiskAddress));
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine2", "");
                    _lead.AppendLineAnswer("QuoteClientAddress.PostalCode",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.PostalCode));
                    _lead.AppendLineAnswer("QuoteClientAddress.SuburbCode",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.Suburb));

                    _lead.AppendLineAnswer("QuoteInsEventBenefit.InsurableEventCode", "509");

                    _lead.AppendLineAnswer("QuoteSOILoss.PeriodOfUninterruptedCover",
                        string.IsNullOrEmpty(ConversionFactory<string>.ConvertAnswer(content, Questions.CurrentInsurancePeriod)) ? "0" :
                        ConversionFactory<string>.ConvertAnswer(content, Questions.CurrentInsurancePeriod));

                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn12Months",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.ClaimsLast0to12Months));
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn24Months",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.ClaimsLast12to24Months));
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn36Months",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.ClaimsLast24to36Months));
                    _lead.AppendLineAnswer("QuoteSOILoss.NCB",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.NoClaimBonus));
                    _lead.AppendLineAnswer("QuoteBuilding.DwellingType",
                        MappingFactory.Map(Questions.TypeOfResidence.Name,
                            ConversionFactory<string>.ConvertAnswer(content, Questions.TypeOfResidence)));
                    _lead.AppendLineAnswer("QuoteBuilding.RoofType",
                        MappingFactory.Map(Questions.RoofConstruction.Name,
                            ConversionFactory<string>.ConvertAnswer(content, Questions.RoofConstruction)));
                    _lead.AppendLineAnswer("QuoteBuilding.StructureType ",
                        MappingFactory.Map(Questions.WallConstruction.Name,
                            ConversionFactory<string>.ConvertAnswer(content, Questions.WallConstruction)));
                    _lead.AppendLineAnswer("QuoteBuilding.ThatchSafeInd",
                        ConversionFactory<bool>.ConvertAnswer(content, Questions.ThatchSafe).ToYesNoStringShort());
                   
                    //lead.AppendLineAnswer("QuoteSecurityPrecautionSOI.SecurityPrecautionCode",
                    //    MappingFactory.Map(Questions.RoofConstruction.Name + "Lightning",
                    //        ConversionFactory<string>.ConvertAnswer(content, Questions.RoofConstruction)));

                    _lead.AppendLineAnswer("QuoteBuilding.DaysUnoccupied",
                        MappingFactory.Map(Questions.Unoccupied.Name,
                            ConversionFactory<string>.ConvertAnswer(content, Questions.Unoccupied)));
                    _lead.AppendLineAnswer("QuoteBuilding.HolidayHomeInd",
                        ConversionFactory<bool>.ConvertAnswer(content, Questions.HolidayHome).ToYesNoStringShort());
                    _lead.AppendLineAnswer("QuoteBuilding.SumInsured",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.SumInsured));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.Premium",
                        premium == null ? "0" : premium.Premium.ToString("##.###"));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.SasriaPremium",
                        premium == null ? "0" : premium.Sasria.ToString("##.###"));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.ProductCode", _metadata.ProductCodeHo ?? "5");
                    //lead.AppendLineAnswer("QuoteInsEventBenefit.ProductCode",
                    //    ConfigurationHelper.Read("Hollard/Upload/ProductCode/HO", true, "5"));
                    i++;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating Hollard Create Building Detail exception: {0}", ex.Message);
                throw;
            }
        }

        private void CreateAllRiskDetail()
        {
            Log.Info("Creating Hollard Lead All Risks.");
            var i = 1;
            try
            {
                foreach (var content in _quote.Request.Items)
                {
                    if (!Equals(content.Cover.Id, Covers.AllRisk.Id)) continue;

                    var premium =
                        _quote.Policy.Items.FirstOrDefault(
                            prem => prem.AssetNo == content.AssetNo && Equals(prem.Cover, content.Cover));

                    _lead.AppendLine("[QARITEMS" + i + "]");
                    _lead.AppendLineAnswer("QuoteAllRiskItem.Description",
                       ConversionFactory<string>.ConvertAnswer(content, Questions.ItemDescription));
                    _lead.AppendLineAnswer("QuoteAllRiskItem.SOIGroup",
                       MappingFactory.Map(Questions.AllRiskCategory.Name,
                           ConversionFactory<string>.ConvertAnswer(content, Questions.AllRiskCategory)));
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine1",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.RiskAddress));
                    _lead.AppendLineAnswer("QuoteClientAddress.AddressLine2", "");
                    _lead.AppendLineAnswer("QuoteClientAddress.PostalCode",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.PostalCode));
                    _lead.AppendLineAnswer("QuoteClientAddress.SuburbCode",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.Suburb));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.InsurableEventCode", "C");
                    _lead.AppendLineAnswer("QuoteSOILoss.PeriodOfUninterruptedCover",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.CurrentInsurancePeriod));
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn12Months",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.ClaimsLast0to12Months));
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn24Months",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.ClaimsLast12to24Months));
                    _lead.AppendLineAnswer("QuoteSOILoss.LossesIn36Months",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.ClaimsLast24to36Months));
                    _lead.AppendLineAnswer("QuoteSOILoss.NCB",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.NoClaimBonus));
                    _lead.AppendLineAnswer("QuoteAllRiskItem.SumInsured",
                        ConversionFactory<string>.ConvertAnswer(content, Questions.SumInsured));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.Premium",
                        premium == null ? "0" : premium.Premium.ToString("##.###"));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.SasriaPremium",
                        premium == null ? "0" : premium.Sasria.ToString("##.###"));
                    _lead.AppendLineAnswer("QuoteInsEventBenefit.ProductCode", _metadata.ProductCodeAllRisks ?? "10");
                    //lead.AppendLineAnswer("QuoteInsEventBenefit.ProductCode",
                    //    ConfigurationHelper.Read("Hollard/Upload/ProductCode/AllRisks", true, "10"));
                    i++;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating Hollard Create All Risk Detail exception: {0}", ex.Message);
                throw;
            }
        }

        private string ValidateQuote()
        {
            Log.Info("Validating Quote Dto.");

            if (_quote == null)
                return "Quote Dto in null";
            if (_quote.InsuredInfo == null)
                return "No Insured Info in Quote Response Dto";
            if (_quote.Policy == null)
                return "No Policy Info in Quote Response Dto";
            if (_quote.Policy.Items == null)
                return "No Items in Quote Response Dto";
            if (_quote.Request.Policy == null)
                return "No Policy Info in Quote Request Dto";
            return _quote.Request.Items == null ? "No Items in Quote Request Dto" : "";
        }
    }
}