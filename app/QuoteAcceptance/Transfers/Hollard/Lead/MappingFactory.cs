﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Hollard.Lead.Mappings;

namespace Workflow.QuoteAcceptance.Hollard.Lead
{
    public static class MappingFactory
    {
        public static string Map(string question, string answer)
        {
            if (string.IsNullOrEmpty(answer)) return "";

            var typeTests = new Dictionary<string, Func<string>>();
            typeTests["Gender"] = () => MapGender.Map(answer);
            typeTests["Title"] = () => MapTitle.Map(answer);
            typeTests["MaritalStatus"] = () => MapMaritalStatus.Map(answer);
            typeTests["Language"] = () => MapLanguage.Map(answer);
            typeTests["PaymentPlan"] = () => MapPaymentPlan.Map(answer);

            //Motor
            typeTests[Questions.DriversLicenceType.Name] = () => MapDriversLicenceCode.Map(answer);
            typeTests[Questions.VehicleTrackingDevice.Name] = () => MapTrackingDevice.Map(answer);
            typeTests[Questions.VehicleTrackingDevice.Name + "Code"] = () => MapTrackingDevice.MapCode(answer);
            typeTests[Questions.VehicleImmobiliser.Name] = () => MapImmobilizer.Map(answer);
            typeTests[Questions.VehicleGaraging.Name] = () => MapGaraging.Map(answer);
            typeTests[Questions.VehicleGearlock.Name] = () => MapGearLock.Map(Boolean.Parse(answer == "Unknown" ? "false" : answer));
            typeTests["SelectedExcess"] = () => MapSelectedExcess.Map(answer);

            //Content and Buldings
            typeTests[Questions.RoofConstruction.Name] = () => MapRoofConstruction.Map(answer);
            typeTests[Questions.WallConstruction.Name] = () => MapWallConstruction.Map(answer);
            typeTests[Questions.RoofConstruction.Name + "Lightning"] = () => MapRoofConstruction.MapLightning(answer);
            typeTests[Questions.AlarmType.Name] = () => MapAlarmType.Map(answer);
            typeTests[Questions.TypeOfResidence.Name] = () => MapTypeOfResidence.Map(answer);
            typeTests[Questions.Unoccupied.Name] = () => MapDaysUnoccupied.Map(answer);
            typeTests[Questions.Unoccupied.Name + "Daytime"] = () => MapDaysUnoccupied.MapWorkingHours(answer);
            typeTests[Questions.PerimeterWall.Name] = () => MapPerimeterWall.Map(answer);
            typeTests[Questions.CarHire.Name] = () => MapCarHire.Map(answer);

            //All Risks
            typeTests[Questions.AllRiskCategory.Name] = () => MapAllRiskCategory.Map(answer);

            return typeTests.FirstOrDefault(x => x.Key == question).Value();
        }
    }
}
