﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapRoofConstruction
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.RoofConstructionNonStandard.Answer] = () => "4";
            typeTests[QuestionAnswers.RoofConstructionStandard.Answer] = () => "1";
            typeTests[QuestionAnswers.RoofConstructionThatchwithLightningconductor.Answer] = () => "2";
            typeTests[QuestionAnswers.RoofConstructionThatchwithoutLightningconductor.Answer] = () => "2";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value() == null || returnFunc.Value() == "")
                return "1";
            return returnFunc.Value();
        }

        public static string MapLightning(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.RoofConstructionNonStandard.Answer] = () => "150";
            typeTests[QuestionAnswers.RoofConstructionStandard.Answer] = () => "150";
            typeTests[QuestionAnswers.RoofConstructionThatchwithLightningconductor.Answer] = () => "151";
            typeTests[QuestionAnswers.RoofConstructionThatchwithoutLightningconductor.Answer] = () => "150";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "150";
            return returnFunc.Value();
        }
    }
}
