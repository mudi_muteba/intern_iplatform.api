﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapSelectedExcess
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.VoluntaryExcess3000.Answer] = () => "38";
            typeTests[QuestionAnswers.VoluntaryExcess4000.Answer] = () => "24";
            typeTests[QuestionAnswers.VoluntaryExcess5000.Answer] = () => "3";
            typeTests[QuestionAnswers.VoluntaryExcess6000.Answer] = () => "27";
            typeTests[QuestionAnswers.VoluntaryExcess7000.Answer] = () => "29";
            typeTests[QuestionAnswers.VoluntaryExcess7500.Answer] = () => "30";
            typeTests[QuestionAnswers.VoluntaryExcess8000.Answer] = () => "31";
            typeTests[QuestionAnswers.VoluntaryExcess9000.Answer] = () => "33";
            typeTests[QuestionAnswers.VoluntaryExcess10000.Answer] = () => "4";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "38";

            return returnFunc.Value();
        }
    }
}
