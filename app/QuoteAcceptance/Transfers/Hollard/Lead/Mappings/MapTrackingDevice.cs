﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapTrackingDevice 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.VehicleTrackingDeviceNone.Answer] = () => "3000";
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInterac.Answer] = () => "3008";
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInteracActive.Answer] = () => "3008";
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInteracHiAlert.Answer] = () => "3008";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackCT1.Answer] = () => "3018";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackQuickCT1.Answer] = () => "3018";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackQuickPlus.Answer] = () => "3018";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellSecureEagleFinditV5.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellSecureEagleTraceeV4.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellStopMk5.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDataTrakMK2D.Answer] = () => "3001";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDataTrakMK4.Answer] = () => "3001";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDigiCoreCTrack.Answer] = () => "3017";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDuoSolutionsDatalogger.Answer] = () => "3001";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMTrack.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixEscosec.Answer] = () => "3012";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX1.Answer] = () => "3002";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX2.Answer] = () => "3011";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX3.Answer] = () => "3002";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMobileTrackerFrankie.Answer] = () => "3015";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMobileTrackerFrankiePlus.Answer] = () => "3015";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarCyberSleuth.Answer] = () => "3021";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU450EarlyWarning.Answer] = () => "9005";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU470PhoneIn.Answer] = () => "9005";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU480Sleuth.Answer] = () => "3021";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVigilSupreme.Answer] = () => "9005";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddi3.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddi5.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddiTrack.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX120.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX170.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX200.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSkyTrax.Answer] = () => "3014";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSkyTraxF4.Answer] = () => "3014";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakClassic.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakElite.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakLifestyle.Answer] = () => "3999";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTracetec.Answer] = () => "3019";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerAlert.Answer] = () => "3025";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerLocateAlert.Answer] = () => "3005";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerLocateRetrieve.Answer] = () => "3005";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerRetrieve.Answer] = () => "3005";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "3000";
            return returnFunc.Value();
        }

        public static string MapCode(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.VehicleTrackingDeviceNone.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInterac.Answer] = () => "8";
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInteracActive.Answer] = () => "8";
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInteracHiAlert.Answer] = () => "8";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackCT1.Answer] = () => "14";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackQuickCT1.Answer] = () => "14";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackQuickPlus.Answer] = () => "14";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellSecureEagleFinditV5.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellSecureEagleTraceeV4.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellStopMk5.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDataTrakMK2D.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDataTrakMK4.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDigiCoreCTrack.Answer] = () => "13";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDuoSolutionsDatalogger.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMTrack.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixEscosec.Answer] = () => "9";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX1.Answer] = () => "7";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX2.Answer] = () => "7";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX3.Answer] = () => "7";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMobileTrackerFrankie.Answer] = () => "11";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMobileTrackerFrankiePlus.Answer] = () => "11";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarCyberSleuth.Answer] = () => "6";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU450EarlyWarning.Answer] = () => "6";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU470PhoneIn.Answer] = () => "6";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU480Sleuth.Answer] = () => "6";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVigilSupreme.Answer] = () => "6";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddi3.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddi5.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddiTrack.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX120.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX170.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX200.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSkyTrax.Answer] = () => "10";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSkyTraxF4.Answer] = () => "10";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakClassic.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakElite.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakLifestyle.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTracetec.Answer] = () => "18";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerAlert.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerLocateAlert.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerLocateRetrieve.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerRetrieve.Answer] = () => "1";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "1";
            return returnFunc.Value();
        }
    }
}
