﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapGaraging 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.VehicleGaragingBehindLockedGates.Answer] = () => "8005";
            typeTests[QuestionAnswers.VehicleGaragingCarport.Answer] = () => "8004";
            typeTests[QuestionAnswers.VehicleGaragingDriveway.Answer] = () => "8005";
            typeTests[QuestionAnswers.VehicleGaragingGaragedAtSchool.Answer] = () => "8002";
            typeTests[QuestionAnswers.VehicleGaragingInLockedGarage.Answer] = () => "8002";
            typeTests[QuestionAnswers.VehicleGaragingNone.Answer] = () => "8000";
            typeTests[QuestionAnswers.VehicleGaragingOffStreet.Answer] = () => "8007";
            typeTests[QuestionAnswers.VehicleGaragingOffStreetAtSchool.Answer] = () => "8007";
            typeTests[QuestionAnswers.VehicleGaragingOnStreet.Answer] = () => "8007";
            typeTests[QuestionAnswers.VehicleGaragingOnStreetAtSchool.Answer] = () => "8007";
            typeTests[QuestionAnswers.VehicleGaragingParkingLot.Answer] = () => "8999";
            typeTests[QuestionAnswers.VehicleGaragingSecurityComplex.Answer] = () => "8999";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "8002";
            return returnFunc.Value();
        }
    }
}
