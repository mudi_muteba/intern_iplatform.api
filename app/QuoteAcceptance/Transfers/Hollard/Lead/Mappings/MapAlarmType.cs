﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapAlarmType 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.AlarmTypeLinkedAlarm.Answer] = () => "9002";
            typeTests[QuestionAnswers.AlarmTypeNonLinkedAlarm.Answer] = () => "9000";
            typeTests[QuestionAnswers.AlarmTypeNone.Answer] = () => "9000";
            
            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "9000";

            return returnFunc.Value();
        }
    }
}
