﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapCarHire
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.CarHire10Days.Answer] = () => "true";
            typeTests[QuestionAnswers.CarHire20Days.Answer] = () => "true";
            typeTests[QuestionAnswers.CarHire30Days.Answer] = () => "true";
            typeTests[QuestionAnswers.CarHireNone.Answer] = () => "false";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "false";
            return returnFunc.Value();
        }
    }
}
