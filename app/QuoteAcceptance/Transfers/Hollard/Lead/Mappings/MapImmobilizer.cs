﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapImmobilizer
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.VehicleImmobiliserFactoryFitted.Answer] = () => "1003";
            typeTests[QuestionAnswers.VehicleImmobiliserNone.Answer] = () => "1000";
            typeTests[QuestionAnswers.VehicleImmobiliserVESA3SecureDevice.Answer] = () => "1001";
            typeTests[QuestionAnswers.VehicleImmobiliserVESA4SecureDevice.Answer] = () => "1002";
            typeTests[QuestionAnswers.VehicleImmobiliserVESA4withAntiHiJack.Answer] = () => "1002";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "1000";
            return returnFunc.Value();
        }
    }
}
