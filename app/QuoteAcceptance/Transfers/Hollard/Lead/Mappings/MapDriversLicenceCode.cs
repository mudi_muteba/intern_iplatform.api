﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapDriversLicenceCode 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.DriversLicenceTypeA.Answer] = () => "12";
            typeTests[QuestionAnswers.DriversLicenceTypeA1.Answer] = () => "12";
            typeTests[QuestionAnswers.DriversLicenceTypeB.Answer] = () => "12";
            typeTests[QuestionAnswers.DriversLicenceTypeC.Answer] = () => "4";
            typeTests[QuestionAnswers.DriversLicenceTypeC1.Answer] = () => "3";
            typeTests[QuestionAnswers.DriversLicenceTypeEB.Answer] = () => "5";
            typeTests[QuestionAnswers.DriversLicenceTypeEC.Answer] = () => "7";
            typeTests[QuestionAnswers.DriversLicenceTypeEC1.Answer] = () => "6";
            typeTests[QuestionAnswers.DriversLicenceTypeCommonWealth.Answer] = () => "11";
            typeTests[QuestionAnswers.DriversLicenceTypeLearnersLicence.Answer] = () => "8";
            typeTests[QuestionAnswers.DriversLicenceTypeInternationalDriversPermit.Answer] = () => "9";
            typeTests[QuestionAnswers.DriversLicenceTypeNeighbouringState.Answer] = () => "10";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "12";

            return returnFunc.Value();
        }
    }
}
