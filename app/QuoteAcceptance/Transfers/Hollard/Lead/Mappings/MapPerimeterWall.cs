﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapPerimeterWall
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.PerimeterWallBrickwalllessthan180cmtall.Answer] = () => "false";
            typeTests[QuestionAnswers.PerimeterWallBrickwallmorethan180cmtall.Answer] = () => "true";
            typeTests[QuestionAnswers.PerimeterWallBrickwallmorethan180cmtallwitheletricfencing.Answer] = () => "true";
            typeTests[QuestionAnswers.PerimeterWallNone.Answer] = () => "false";
            typeTests[QuestionAnswers.PerimeterWallPallisadewalllessthan180cmtall.Answer] = () => "false";
            typeTests[QuestionAnswers.PerimeterWallPallisadewallmorethan180cmtall.Answer] = () => "true";
            typeTests[QuestionAnswers.PerimeterWallPrecastwalllessthan180cmtall.Answer] = () => "false";
            typeTests[QuestionAnswers.PerimeterWallPrecastwallmorethan180cmtall.Answer] = () => "true";
            typeTests[QuestionAnswers.PerimeterWallWirefence.Answer] = () => "false";
            typeTests[QuestionAnswers.PerimeterWallWoodfencelessthan180cmtall.Answer] = () => "false";
            typeTests[QuestionAnswers.PerimeterWallWoodfencemorethan180cmtall.Answer] = () => "true";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "false";
            return returnFunc.Value();
        }
    }
}
