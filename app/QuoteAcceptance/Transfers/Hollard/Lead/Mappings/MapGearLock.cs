﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapGearLock 
    {
        public static string Map(bool answer)
        {
            var typeTests = new Dictionary<bool, Func<string>>();
            typeTests[false] = () => "2000";
            typeTests[true] = () => "2022";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "2000";
            return returnFunc.Value();
        }
    }
}
