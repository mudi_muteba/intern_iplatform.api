﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapMaritalStatus
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.MainDriverMaritalStatusCivilUnion.Answer] = () => "2";
            typeTests[QuestionAnswers.MainDriverMaritalStatusDivorced.Answer] = () => "3";
            typeTests[QuestionAnswers.MainDriverMaritalStatusDomesticPartner.Answer] = () => "2";
            typeTests[QuestionAnswers.MainDriverMaritalStatusMarried.Answer] = () => "2";
            typeTests[QuestionAnswers.MainDriverMaritalStatusOther.Answer] = () => "1";
            typeTests[QuestionAnswers.MainDriverMaritalStatusSeparated.Answer] = () => "4";
            typeTests[QuestionAnswers.MainDriverMaritalStatusSingle.Answer] = () => "6";
            typeTests[QuestionAnswers.MainDriverMaritalStatusUnincorporatedAssociation.Answer] = () => "1";
            typeTests[QuestionAnswers.MainDriverMaritalStatusUnknown.Answer] = () => "1";
            typeTests[QuestionAnswers.MainDriverMaritalStatusWidowed.Answer] = () => "5";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "1";
            return returnFunc.Value();
        }
    }
}