﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapDaysUnoccupied
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.Unoccupied120DaysInExcessOfLimit.Answer] = () => "120";
            typeTests[QuestionAnswers.Unoccupied150DaysInExcessOfLimit.Answer] = () => "150";
            typeTests[QuestionAnswers.Unoccupied180DaysInExcessOfLimit.Answer] = () => "180";
            typeTests[QuestionAnswers.Unoccupied210DaysInExcessOfLimit.Answer] = () => "210";
            typeTests[QuestionAnswers.Unoccupied240DaysInExcessOfLimit.Answer] = () => "240";
            typeTests[QuestionAnswers.Unoccupied270DaysInExcessOfLimit.Answer] = () => "270";
            typeTests[QuestionAnswers.Unoccupied300DaysInExcessOfLimit.Answer] = () => "300";
            typeTests[QuestionAnswers.Unoccupied90DaysInExcessOfLimit.Answer] = () => "90";
            typeTests[QuestionAnswers.UnoccupiedDuringWorkHours.Answer] = () => "0";
            typeTests[QuestionAnswers.UnoccupiedMoreThan30ConsecutiveDays.Answer] = () => "30";
            typeTests[QuestionAnswers.UnoccupiedMoreThan60DaysPerYear.Answer] = () => "60";
            typeTests[QuestionAnswers.UnoccupiedMoreThan7DaysInFirstMonth.Answer] = () => "7";
            typeTests[QuestionAnswers.UnoccupiedNo.Answer] = () => "0";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "0";
            return returnFunc.Value();
        }

        public static string MapWorkingHours(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.UnoccupiedNo.Answer] = () => "true";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "false";
            return returnFunc.Value();
        }
    }
}
