﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapPaymentPlan
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[PaymentPlans.Monthly.Name] = () => "M";
            typeTests[PaymentPlans.Annual.Name] = () => "A";
            typeTests[PaymentPlans.BiAnnual.Name] = () => "M";
            typeTests[PaymentPlans.OnceOff.Name] = () => "M";
            typeTests[PaymentPlans.Quarterly.Name] = () => "M";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "M";
            return returnFunc.Value();
        }
    }
}
