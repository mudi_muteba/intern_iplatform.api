﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapGender 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.MainDriverGenderUnknown.Answer] = () => "0";
            typeTests[QuestionAnswers.MainDriverGenderMale.Answer] = () => "1";
            typeTests[QuestionAnswers.MainDriverGenderFemale.Answer] = () => "2";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "0";
            return returnFunc.Value();
        }
    }
}
