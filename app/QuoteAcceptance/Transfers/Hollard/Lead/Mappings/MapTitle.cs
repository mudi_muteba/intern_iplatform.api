﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapTitle
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[Titles.Admiral.Code] = () => "";
            typeTests[Titles.Advocate.Code] = () => "Adv";
            typeTests[Titles.Archbishop.Code] = () => "";
            typeTests[Titles.Bishop.Code] = () => "";
            typeTests[Titles.Brigadier.Code] = () => "Brig";
            typeTests[Titles.Captain.Code] = () => "Capt";
            typeTests[Titles.Colonel.Code] = () => "Col";
            typeTests[Titles.Commissioner.Code] = () => "Com";
            typeTests[Titles.Director.Code] = () => "Dir";
            typeTests[Titles.Doctor.Code] = () => "Dr";
            typeTests[Titles.EstateLate.Code] = () => "";
            typeTests[Titles.General.Code] = () => "Gen";
            typeTests[Titles.Honourable.Code] = () => "Hon";
            typeTests[Titles.HonourableJudge.Code] = () => "Hon";
            typeTests[Titles.Inspector.Code] = () => "";
            typeTests[Titles.Judge.Code] = () => "";
            typeTests[Titles.Justice.Code] = () => "";
            typeTests[Titles.Lady.Code] = () => "";
            typeTests[Titles.Lieutenant.Code] = () => "";
            typeTests[Titles.LieutenantColonel.Code] = () => "";
            typeTests[Titles.LieutenantCommander.Code] = () => "";
            typeTests[Titles.Magistrate.Code] = () => "";
            typeTests[Titles.Major.Code] = () => "";
            typeTests[Titles.Minister.Code] = () => "";
            typeTests[Titles.Miss.Code] = () => "Miss";
            typeTests[Titles.Mr.Code] = () => "Mr";
            typeTests[Titles.MrJnr.Code] = () => "";
            typeTests[Titles.MrSnr.Code] = () => "";
            typeTests[Titles.Mrs.Code] = () => "Mrs";
            typeTests[Titles.Pastor.Code] = () => "";
            typeTests[Titles.Professor.Code] = () => "Prof";
            typeTests[Titles.Psychologist.Code] = () => "";
            typeTests[Titles.Rabbi.Code] = () => "";
            typeTests[Titles.Reverend.Code] = () => "Rev";
            typeTests[Titles.SeniorSuperintendent.Code] = () => "";
            typeTests[Titles.Sergeant.Code] = () => "";
            typeTests[Titles.Sir.Code] = () => "Sir";
            typeTests[Titles.Superintendent.Code] = () => "";
            typeTests[Titles.TheHonourable.Code] = () => "Hon";
            typeTests[Titles.Trust.Code] = () => "";
            typeTests[Titles.Trustee.Code] = () => "";
            typeTests[Titles.Unknown.Code] = () => "";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "Mr";

            return returnFunc.Value();
        }
    }
}