﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapWallConstruction
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.WallConstructionNonStandard.Answer] = () => "4";
            typeTests[QuestionAnswers.WallConstructionStandard.Answer] = () => "1";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "1";
            return returnFunc.Value();
        }
    }
}
