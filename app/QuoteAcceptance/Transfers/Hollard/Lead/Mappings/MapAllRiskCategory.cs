﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapAllRiskCategory 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.AllRiskCategoryAssetsOut.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryBikersRidingApparel.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryBinoculars.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryCalculators.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryCampingEquipment.Answer] = () => "62";
            typeTests[QuestionAnswers.AllRiskCategoryCarSoundEquipment.Answer] = () => "15";
            typeTests[QuestionAnswers.AllRiskCategoryCaravanCampingEquipment.Answer] = () => "62";
            typeTests[QuestionAnswers.AllRiskCategoryCaravanContents.Answer] = () => "64";
            typeTests[QuestionAnswers.AllRiskCategoryCellCarPhone.Answer] = () => "53";
            typeTests[QuestionAnswers.AllRiskCategoryCellPhones.Answer] = () => "53";
            typeTests[QuestionAnswers.AllRiskCategoryClothingPersonalEffectsSpecified.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryClothingPersonalEffectsUnspecified.Answer] = () => "41";
            typeTests[QuestionAnswers.AllRiskCategoryCompactDiscs.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryContactLenses.Answer] = () => "57";
            typeTests[QuestionAnswers.AllRiskCategoryCostumeJewellery.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryCrystal.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryDVDandPlaystationEquipment.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryDecoder.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryDivingEquipment.Answer] = () => "51";
            typeTests[QuestionAnswers.AllRiskCategoryDoctorBags.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryDocuments.Answer] = () => "63";
            typeTests[QuestionAnswers.AllRiskCategoryElectricalGoods.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryExtendedAssetsOut.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryFirearms.Answer] = () => "60";
            typeTests[QuestionAnswers.AllRiskCategoryFishfinder.Answer] = () => "62";
            typeTests[QuestionAnswers.AllRiskCategoryFishingEquipment.Answer] = () => "62";
            typeTests[QuestionAnswers.AllRiskCategoryFurs.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryGPS.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryGeneral.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryGenerators.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryGlasswareChina.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryGolfClubs.Answer] = () => "51";
            typeTests[QuestionAnswers.AllRiskCategoryGoodsInBankSafe.Answer] = () => "58";
            typeTests[QuestionAnswers.AllRiskCategoryHearingAid.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryHearingaidsandprosthesis.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryHouseholdGoods.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryIPod.Answer] = () => "54";
            typeTests[QuestionAnswers.AllRiskCategoryItemsTheftfromVehicles.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryItemskeptintheVault.Answer] = () => "58";
            typeTests[QuestionAnswers.AllRiskCategoryItemsremovedfrombankvault.Answer] = () => "58";
            typeTests[QuestionAnswers.AllRiskCategoryJewellery.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryJewelleryandwatchesspecified.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryKeysAndLocks.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryLaptop.Answer] = () => "66";
            typeTests[QuestionAnswers.AllRiskCategoryLeatherGarments.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryLeatherGoods.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryLossOfMoney.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMiscellaneousGoods.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMobilityscootersorshopriders.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMotorAccessories.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMotorcycleHelmets.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMotorisedEquipment.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMotorisedGolfCarts.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMotorisedandnonmotorisedwheelchairs.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMusicInstruments.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMusicalInstruments.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryMusicalInstrumentsProfessional.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryNavigationalEquipment.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryOffRoadMotorBikes.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryOnRoadMotorBikes.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryOpthalmicGlasses.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryOther.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryPaintingsBooksPictures.Answer] = () => "63";
            typeTests[QuestionAnswers.AllRiskCategoryParachutesparaglidersandhanggliders.Answer] = () => "51";
            typeTests[QuestionAnswers.AllRiskCategoryPedalCycles.Answer] = () => "59";
            typeTests[QuestionAnswers.AllRiskCategoryPersianRugs.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryPersonalComputer.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryPhotoEquipment.Answer] = () => "50";
            typeTests[QuestionAnswers.AllRiskCategoryPortableRadioCassetteCdPlayers.Answer] = () => "54";
            typeTests[QuestionAnswers.AllRiskCategoryPortableTv.Answer] = () => "55";
            typeTests[QuestionAnswers.AllRiskCategoryProstheticsMedicalAids.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryQuadBikesNonRoadLicensed.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryQuadBikesRestrictedCover.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryRadioCommunicationEquipment.Answer] = () => "54";
            typeTests[QuestionAnswers.AllRiskCategoryRadios.Answer] = () => "54";
            typeTests[QuestionAnswers.AllRiskCategorySatelliteDishes.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategorySkiEquipment.Answer] = () => "51";
            typeTests[QuestionAnswers.AllRiskCategorySolarPanel.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategorySpecified.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategorySpecifiedContents.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategorySpectaclesSunglasses.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategorySports.Answer] = () => "51";
            typeTests[QuestionAnswers.AllRiskCategorySportsEquipment.Answer] = () => "51";
            typeTests[QuestionAnswers.AllRiskCategoryStampCoinCollections.Answer] = () => "61";
            typeTests[QuestionAnswers.AllRiskCategorySwimmingPoolEquipment.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryToolsHandTools.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryTransportOfGroceriesAndHouseholdGoods.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryTwoWayRadios.Answer] = () => "54";
            typeTests[QuestionAnswers.AllRiskCategoryVideoCamera.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryVideoMachineVcr.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryWatches.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryWheelchairs.Answer] = () => "49";
            typeTests[QuestionAnswers.AllRiskCategoryZippyNippysKiddiesCycles.Answer] = () => "49";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "49";

            return returnFunc.Value();
        }
    }
}
