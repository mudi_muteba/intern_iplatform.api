﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Hollard.Lead.Mappings
{
    internal static class MapTypeOfResidence
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.TypeOfResidenceApartmentFlatAbove1stFloor.Answer] = () => "5";
            typeTests[QuestionAnswers.TypeOfResidenceApartmentFlatGroundor1stFloor.Answer] = () => "4";
            typeTests[QuestionAnswers.TypeOfResidenceDetachedHouseCottage.Answer] = () => "1";
            typeTests[QuestionAnswers.TypeOfResidenceDoubleStoreyHouse.Answer] = () => "1";
            typeTests[QuestionAnswers.TypeOfResidenceDoubleStoreyTownhouse.Answer] = () => "2";
            typeTests[QuestionAnswers.TypeOfResidenceGardenCottage.Answer] = () => "6";
            typeTests[QuestionAnswers.TypeOfResidenceParkHome.Answer] = () => "1";
            typeTests[QuestionAnswers.TypeOfResidenceSemiDetachedHouseCottage.Answer] = () => "1";
            typeTests[QuestionAnswers.TypeOfResidenceTownHouseClusterHomeNoLimitedAccess.Answer] = () => "3";
            typeTests[QuestionAnswers.TypeOfResidenceTownHouseClusterHouseLimitedAccess.Answer] = () => "2";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "1";
            return returnFunc.Value();
        }
    }
}
