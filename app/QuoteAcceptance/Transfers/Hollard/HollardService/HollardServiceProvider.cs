﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using Domain.Policies.Message;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Infrastructure;
using Workflow.QuoteAcceptance.Hollard.Lead;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.Hollard.HollardService
{
    public class HollardServiceProvider : ITransferLeadToInsurer
    {
        private readonly IHollardServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private readonly string _authenticationToken;
        private static readonly ILog Log = LogManager.GetLogger<HollardServiceProvider>();
        private static CommunicationMetadata _mail;
        private readonly Guid _channelId;
        public HollardServiceProvider(IHollardServiceClient client, LeadFactory leadFactory, string authenticationToken, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            _client = client;
            _leadFactory = leadFactory;
            _authenticationToken = authenticationToken;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var quote = message.Quote;
            var lead = _leadFactory.CreateLead();
            if (string.IsNullOrEmpty(lead))
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to Hollard. Validation failed.",quote.Request.Id.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("No details were found in the Quote", message.SerializeAsXml(), quote.Request.Id,InsurerName.Hollard.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureNoQuote(InsurerName.Hollard.Name(), quote.Request.Id, _channelId));
                return new LeadTransferResult(false);
            }

            var result = Execute(quote, lead);
            var process = _processStatus.FirstOrDefault(w => w.Key == result.ResponseStatus);
            if (process.Value != null)
                process.Value(result, quote.Request.Id, ExecutionPlan, _channelId, quote.QuoteAcceptanceEnvironment);
            
            return new LeadTransferResult(result.ResponseStatus == ResponseStatus.Success);
        }

        private Response Execute(PublishQuoteUploadMessageDto quote, string lead)
        {
            Response result = null;
            var responseSuccess = false;
            var errorMessage = string.Empty;
            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.Hollard.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, ReferenceNumber));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, ReferenceNumber));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.Hollard.Name(), quote.Request.Id, _channelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.Hollard.Name(), quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.Hollard.Name(), quote.Request.Id, response.SerializeAsXml(), _channelId)))
                .While(() => !responseSuccess)
                .Execute(() =>
                {
                    try
                    {
                        //if (quote.WorkflowEnvironment == "dev")
                        //{
                        //    Log.Warn("quoteacceptance/environment = dev. Quote was not uploaded");
                        //    result = new Response
                        //    {
                        //        Message = "success",
                        //        ResponseStatus = ResponseStatus.Success,
                        //        QuoteNo = Guid.NewGuid().ToString()
                        //    };
                        //}
                        //else
                            result = Upload(_client, _authenticationToken, lead);


                        responseSuccess = result.ResponseStatus == ResponseStatus.Success;
                        errorMessage = responseSuccess ? string.Empty : "Failure";
                        ReferenceNumber = string.IsNullOrEmpty(result.QuoteNo) ? Guid.NewGuid().ToString() : result.QuoteNo;                     
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.Hollard.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.Hollard.Name(), quote.Request.Id, ex, _channelId));
                    }

                    return ValidResponse(result);
                });

            return ValidResponse(result);
        }


        public static Response Upload(IHollardServiceClient client, string authtoken, string lead)
        {
            return LeadTransfer.TransferLead(client, authtoken, lead);
        }

        private static Response ValidResponse(Response response)
        {
            return response ?? new Response() {ResponseStatus = ResponseStatus.Failure};
        }

        private readonly IDictionary<ResponseStatus, Action<Response, Guid, ExecutionPlan, Guid, string>> _processStatus = new Dictionary
            <ResponseStatus, Action<Response, Guid, ExecutionPlan, Guid, string>>()
        {
            {
                ResponseStatus.Success,
                (result, ratingRequestId, executionPlan, channelId, enviroment) => Log.InfoFormat("Quote with Rating Request Id [{0}] successfully uploaded to Hollard", ratingRequestId)
            },
            {
                ResponseStatus.Failure, (result, ratingRequestId, executionPlan, channelId, enviroment) =>
                {
                    Log.ErrorFormat("Quote [{0}] failed to upload to Hollard with failure reason {1}",ratingRequestId, result.Message);
                    executionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}.", result.Message), result.SerializeAsXml(), ratingRequestId,InsurerName.Hollard.Name(), channelId, _mail,enviroment));
                    executionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.Hollard.Name(),ratingRequestId,string.Format("Failure Reason {0}", result.Message),channelId));
                }
            },
            {
                ResponseStatus.Error, (result, ratingRequestId, executionPlan, channelId, enviroment) =>
                {
                    Log.ErrorFormat("Quote [{0}] failed to upload to Hollard with failure reason {1}", ratingRequestId, result.Message);
                    executionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}.", result.Message),result.SerializeAsXml(), ratingRequestId, InsurerName.Hollard.Name(), channelId, _mail,enviroment));
                    executionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.Hollard.Name(), ratingRequestId,string.Format("Failure Reason {0}", result.Message), channelId));
                }
            }
        };

        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }

        private string NewLead()
        {
            return @"[QTM HBF]
................

[Transform]
Version 000;

[QMASTER1]
Quote.QmClientNo 265a0baf-8f9e-4e9c-a649-fb44468ef76a;
Quote.EffectiveDate 20140911;
Quote.PolicyInstallmentDueDay 1;
Quote.QuoteDate 20140911;
Quote.SchemeCode 634;
Campaign.Campaign 515;
Campaign.CampaignType 89;
Campaign.ExternalQuoteNo 265a0baf-8f9e-4e9c-a649-fb44468ef76a;
ClientDetails.IdentityNo 6603285990086;
ClientDetails.TitleCode Mr;
ClientDetails.FirstName Jannie;
ClientDetails.Initials J;
ClientDetails.Name Vermaak;
ClientDetails.Gender 1;
ClientDetails.DateOfBirth 19660328;
ClientDetails.EmailAddress support@cardinal.co.za;
ClientDetails.MaritalStatus 2;
ClientTelNo.Cell 0798934451;
ClientTelNo.Home ;
ClientTelNo.Work ;
ClientDetails.LanguageCode E;
Quote.PaymentFrequency M;
Quote.ConfirmationEmail Jannie@cardinal.co.za;
QuoteClientAddress.AddressLine1 1 Fritz Sonnenberg Road;
QuoteClientAddress.AddressLine2 Unknown;
QuoteClientAddress.Postalcode 1682;
QuoteClientAddress.SuburbCode CROWTHORNE;

[QPC1]
QuoteDriverDetails.IdentityNo 6603285990086;
QuoteDriverDetails.Title Mr;
QuoteDriverDetails.FirstName D;
QuoteVehicleDriver.Surname Daniels;
QuoteDriverDetails.DateOfBirth 19660328;
QuoteDriverDetails.Gender 1;
QuoteClientAddress.AddressLine1 1 Fritz Sonnenberg Road;
QuoteClientAddress.AddressLine2 Unknown;
QuoteClientAddress.Postalcode 1682;
QuoteClientAddress.SuburbCode CROWTHORNE;
QuoteInsEventBenefit.InsurableEventCode C;
QuoteDriverDetails.MaritalStatus 2;
QuoteDriverDetails.Licencetype 12;
QuoteDriverDetails.MonthDriverLicenceIssued 09;
QuoteDriverDetails.YearDriverLicenceIssued 2014;
QuoteInsEventBenefit.ProductCode 91;
QuoteInsEventBenefit.Premium 490.48;
QuoteInsEventBenefit.RatePerK 0.14;
QuoteInsEventBenefit.SasriaPremium 2;
QuoteSOILoss.PeriodOfUninterruptedCover 6;
QuoteSOILoss.NCB 7;
QuoteSOILoss.LossesIn12Months 0;
QuoteSOILoss.LossesIn24Months 0;
QuoteSOILoss.LossesIn36Months 0;
QuoteDataCollectionDeviceSOI.DataCollectionDeviceCode 14;
QuoteVehicleDetails.RegistrationNo ;
QuoteSecurityPrecautionSOI.SecurityPrecautionCode 1003;
QuoteSecurityPrecautionSOI.SecurityPrecautionCode 3018;
QuoteSecurityPrecautionSOI.SecurityPrecautionCode 8005;
QuoteVehicleModification.ModificationCode N;
QuoteInsEventBenefit.CarHire Y;
QuoteVehicleExtras.ExtraType 1;
QuoteVehicleExtras.Value ;
QuoteVehicleExtras.RadioValue ;
QuoteVehicleExtras.Description3 Unknown;
QuoteVehicleDetails.AverageMonthlyMileage 0;
QuoteVehicleDetails.ManufactureYear 0;
QuoteVehicleDetails.VehicleType 05034175;
VehicleType.VehicleMakeCode BMW;
VehicleType.VehicleModelCode 320 d EXCLUSIVE (E46);
QuoteVehicleDetails.VehicleUsedFor 1;
VehicleModelMonthlyValues.TradeValue 63700;
VehicleModelMonthlyValues.RetailsValue 63700;

[QHH1]
QuoteInsEventBenefit.InsurableEventCode C;
QuoteClientAddress.AddressLine1 Unit 13, Villa Toscana, 92 Puttick Ave;
QuoteClientAddress.AddressLine2 ;
QuoteClientAddress.PostalCode 8005;
QuoteClientAddress.SuburbCode GREEN POINT;
QuoteSOILoss.PeriodOfUninterruptedCover 6;
QuoteSOILoss.LossesIn12Months 0;
QuoteSOILoss.LossesIn24Months 0;
QuoteSOILoss.LossesIn36Months 0;
QuoteSOILoss.NCB 5;
QuoteBuilding.DwellingType 1;
QuoteBuilding.RoofType 1;
QuoteBuilding.StructureType  1;
QuoteBuilding.ThatchSafeInd N;
QuoteSecurityPrecautionSOI.ElectricFence N;
QuoteSecurityPrecautionSOI.SecurityPrecautionCode 150;
QuoteSecurityPrecautionSOI.SecurityPrecautionCode 142;
QuoteSecurityPrecautionSOI.SecurityPrecautionCode 9002;
QuoteSecurityPrecautionSOI.SecurityPrecautionCode 112;
QuoteBuilding.DaysUnoccupied 0;
QuoteBuilding.HolidayHomeInd N;
QuoteHouseContent.SumInsured 650000;
QuoteInsEventBenefit.Premium 55.48;
QuoteInsEventBenefit.SasriaPremium 1.8;
QuoteInsEventBenefit.ProductCode 91;

[QHO1]
QuoteInsEventBenefit.InsurableEventCode C;
QuoteClientAddress.AddressLine1 Unit 13, Villa Toscana, 92 Puttick Ave;
QuoteClientAddress.AddressLine2 ;
QuoteClientAddress.PostalCode 8005;
QuoteClientAddress.SuburbCode GREEN POINT;
QuoteSOILoss.PeriodOfUninterruptedCover 6;
QuoteSOILoss.LossesIn12Months 0;
QuoteSOILoss.LossesIn24Months 0;
QuoteSOILoss.LossesIn36Months 0;
QuoteSOILoss.NCB 7;
QuoteBuilding.DwellingType 1;
QuoteBuilding.RoofType 1;
QuoteBuilding.StructureType  1;
QuoteBuilding.ThatchSafeInd N;
QuoteSecurityPrecautionSOI.SecurityPrecautionCode 150;
QuoteBuilding.DaysUnoccupied 1;
QuoteBuilding.HolidayHomeInd N;
QuoteBuilding.SumInsured 650000;
QuoteInsEventBenefit.Premium 121.48;
QuoteInsEventBenefit.SasriaPremium 2.65;
QuoteInsEventBenefit.ProductCode 91;

[QARITEMS1]
QuoteAllRiskItem.Description All my clothing;
QuoteAllRiskItem.SOIGroup 41;
QuoteClientAddress.AddressLine1 Unit 13, Villa Toscana, 92 Puttick Ave;
QuoteClientAddress.AddressLine2 ;
QuoteClientAddress.PostalCode 8005;
QuoteClientAddress.SuburbCode GREEN POINT;
QuoteInsEventBenefit.InsurableEventCode C;
QuoteSOILoss.PeriodOfUninterruptedCover 6;
QuoteSOILoss.LossesIn12Months 0;
QuoteSOILoss.LossesIn24Months 0;
QuoteSOILoss.LossesIn36Months 0;
QuoteSOILoss.NCB 7;
QuoteAllRiskItem.SumInsured 10000;
QuoteInsEventBenefit.Premium 16.48;
QuoteInsEventBenefit.SasriaPremium .21;
QuoteInsEventBenefit.ProductCode 91;

[EOF]
";
        }
    }
}