﻿namespace Workflow.QuoteAcceptance.Hollard.HollardService
{
    public interface IHollardServiceClient
    {
        Response SendQuoteRequest(string token, string data);
    }

    public partial class HollardSwitchingClient : IHollardServiceClient
    {

    }
}