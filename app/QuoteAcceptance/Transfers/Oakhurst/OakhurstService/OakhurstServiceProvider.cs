﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using Domain.Policies.Message;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Oakhurst.Lead;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.Oakhurst.OakhurstService
{
    public class OakhurstServiceProvider : ITransferLeadToInsurer
    {
        private readonly IOakhurstServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private readonly AuthHeader _authHeader;
        private static CommunicationMetadata _mail;
        private readonly Guid _channelId;
        private static readonly ILog Log = LogManager.GetLogger<OakhurstServiceProvider>();
        public OakhurstServiceProvider(IOakhurstServiceClient client, LeadFactory leadFactory, AuthHeader authHeader, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            _client = client;
            _leadFactory = leadFactory;
            _authHeader = authHeader;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
        }

        private ServiceResults Execute(PublishQuoteUploadMessageDto quote, Lead lead)
        {
            ServiceResults result = null;
            var responseSuccess = false;
            var errorMessage = string.Empty;
            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.Oakhurst.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, ReferenceNumber));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, ReferenceNumber));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.Oakhurst.Name(), quote.Request.Id, _channelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.Oakhurst.Name(), quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.Oakhurst.Name(), quote.Request.Id, response.SerializeAsXml(), _channelId)))
                .While(() => !responseSuccess)
                .Execute(() =>
                {
                    try
                    {
                        result = Upload(_client, _authHeader, lead);

                        responseSuccess = result.Status == ResultsCode.Success;
                        errorMessage = responseSuccess ? string.Empty : result.Message;
                        ReferenceNumber = string.IsNullOrEmpty(result.ReferenceNo) ? Guid.NewGuid().ToString() : result.ReferenceNo;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.Oakhurst.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.Oakhurst.Name(), quote.Request.Id, ex, _channelId));
                    }
                    return ValidResponse(result);
                });

            return ValidResponse(result);
        }

        public static ServiceResults Upload(IOakhurstServiceClient client, AuthHeader authHeader, OakhurstService.Lead lead)
        {
            return LeadTransfer.TransferLead(client, authHeader, lead);
        }

        private static ServiceResults ValidResponse(ServiceResults response)
        {
            return response ?? new ServiceResults() { Status = ResultsCode.Failure };
        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var lead = _leadFactory.CreateLead();
            var quote = message.Quote;

            if (lead == null)
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to Oakhurst. No items were found in the Quote", message.Quote.Request.Id.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("No details were found in the Quote", message.SerializeAsXml(), message.Quote.Request.Id, InsurerName.Oakhurst.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureNoQuote(InsurerName.Oakhurst.Name(), message.Quote.Request.Id, _channelId));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                return new LeadTransferResult(false);
            }

            var result = Execute(message.Quote, lead);
            var process = _processStatus.FirstOrDefault(w => w.Key == result.Status);
            if (process.Value != null)
                process.Value(result, message.Quote.Request.Id, ExecutionPlan, _channelId, quote.QuoteAcceptanceEnvironment, message.Quote);

            return new LeadTransferResult(result.Status == ResultsCode.Success);
        }

        private readonly IDictionary<ResultsCode, Action<ServiceResults, Guid, ExecutionPlan, Guid, string, PublishQuoteUploadMessageDto>> _processStatus = new Dictionary
            <ResultsCode, Action<ServiceResults, Guid, ExecutionPlan, Guid, string, PublishQuoteUploadMessageDto>>()
        {
            {
                ResultsCode.Success,
                (result, ratingId, executionPlan, channelId, environment, quote) =>
                    Log.InfoFormat("Quote with Rating Request ID [{0}] successfully uploaded to Oakhurst", ratingId)
            },
            {
                ResultsCode.Failure, (result, ratingId, executionPlan, channelId, environment, quote) =>
                {
                    Log.ErrorFormat("Quote with Rating Request ID [{0}] failed to upload to Oakhurst with failure reason {1}", ratingId, result.Message);
                    executionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}", result.Message),result.SerializeAsXml(), ratingId,InsurerName.Oakhurst.Name(), channelId, _mail, environment));
                    executionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.Oakhurst.Name(),ratingId, string.Format("Failure Reason {0}", result.Message), channelId));
                    executionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                }
            },
            {
                ResultsCode.Warning, (result, ratingId, executionPlan, channelId, environment, quote) =>
                {
                    Log.WarnFormat("Quote with Rating Request ID [{0}] successfully uploaded to Oakhurst with Warning {1}", ratingId, result.Message);
                    executionPlan.AddMessage(LeadTransferMessage.CreateWarningWithMessage(InsurerName.Oakhurst.Name(),ratingId,string.Format("Quote with Rating Request ID [{0}] successfully uploaded to Oakhurst with Warning {1}", ratingId, result.Message),channelId));

                }
            }
        };
        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }
    }
}