﻿namespace Workflow.QuoteAcceptance.Oakhurst.OakhurstService
{
    public interface IOakhurstServiceClient
    {
        ServiceResults transferLead(AuthHeader authHeader, Lead quoteDetails, string extReference);
    }

    public partial class ServiceSoapClient : IOakhurstServiceClient
    {

    }
}