﻿using AutoMapper;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Mappings
{
    public class OakhurstQuoteMappers : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<OakhurstRatingService.Vehicle, VehicleDetail>();
            Mapper.CreateMap<OakhurstRatingService.Person, Person>();
            Mapper.CreateMap<OakhurstRatingService.NonMotor, NonMotorDetail>();
            Mapper.CreateMap<OakhurstRatingService.NonMotor, AllRiskDetail>();
            Mapper.CreateMap<OakhurstRatingService.Person, Insured>();
        }
    }
}