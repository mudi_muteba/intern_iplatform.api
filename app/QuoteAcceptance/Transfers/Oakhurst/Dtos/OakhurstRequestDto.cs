﻿using Workflow.QuoteAcceptance.Oakhurst.OakhurstRatingService;

namespace Workflow.QuoteAcceptance.Oakhurst.Dtos
{
    public class OakhurstRequestDto
    {
        public Quote Quote { get; set; }
    }
}