﻿using Workflow.QuoteAcceptance.Oakhurst.OakhurstRatingService;

namespace Workflow.QuoteAcceptance.Oakhurst.Dtos
{
    public class OakhurstResponseDto
    {
        public OakhurstResponseDto()
        {
         
        }
        public QuoteResults[][] Results { get; set; }
       
    }
}