﻿using System;
using System.Collections.Generic;
using System.Linq;
using Workflow.QuoteAcceptance.Oakhurst.Lead.Mappings;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead
{
    public static class MappingFactory<TReturn>
    {
        public static TReturn Map(string answer)
        {
            if (string.IsNullOrEmpty(answer)) return default(TReturn);

            var typeTests = new Dictionary<Type, Func<TReturn>>();
            typeTests[typeof(TrackingCode)] = () => (TReturn)(object)MapTrackingDevice.Map(answer);
            typeTests[typeof(ParkingCode)] = () => (TReturn)(object)MapGaraging.Map(answer);
            typeTests[typeof(GenderCode)] = () => (TReturn)(object)MapGender.Map(answer);
            typeTests[typeof(CustTypeCode)] = () => (TReturn)(object)MapCustomerType.Map(answer);
            typeTests[typeof(LicenseCode)] = () => (TReturn)(object)MapDriversLicenceCode.Map(answer);
            typeTests[typeof(IDCode)] = () => (TReturn)(object)MapIDType.Map(answer);
            typeTests[typeof(LanguageCode)] = () => (TReturn)(object)MapLanguage.Map(answer);
            typeTests[typeof(DrivingFrequencyCode)] = () => (TReturn)(object)MapDrivingFrequency.Map(answer);
            typeTests[typeof(MaritalCode)] = () => (TReturn)(object)MapMaritalStatus.Map(answer);
            typeTests[typeof(AlarmCode)] = () => (TReturn)(object)MapCarAlarm.Map(answer);
            typeTests[typeof(CoverCode)] = () => (TReturn)(object)MapTypeOfCoverMotor.Map(answer);
            typeTests[typeof(ValueCode)] = () => (TReturn)(object)MapValuationMethod.Map(answer);
            typeTests[typeof(UseCode)] = () => (TReturn)(object)MapClassOfUse.Map(answer);
            typeTests[typeof(RelationShipCode)] = () => (TReturn)(object)MapRelationship.Map(answer);
            typeTests[typeof(ColorCode)] = () => (TReturn)(object)MapColour.Map(answer);
            typeTests[typeof(PaintTypeCode)] = () => (TReturn)(object)MapPaintType.Map(answer);

            return typeTests.FirstOrDefault(x => x.Key == typeof(TReturn)).Value();
        }
    }
}
