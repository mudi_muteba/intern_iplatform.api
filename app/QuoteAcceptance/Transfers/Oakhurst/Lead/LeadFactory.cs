﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Oakhurst.Dtos;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;
using NonMotor = Workflow.QuoteAcceptance.Oakhurst.OakhurstRatingService.NonMotor;
using Person = Workflow.QuoteAcceptance.Oakhurst.OakhurstRatingService.Person;
using Vehicle = Workflow.QuoteAcceptance.Oakhurst.OakhurstRatingService.Vehicle;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead
{
    public class LeadFactory
    {
        private static readonly ILog Log = LogManager.GetLogger<LeadFactory>();
        private readonly PublishQuoteUploadMessageDto _quote;
        private readonly LeadTransferOakhurstTaskMetadata _metadata;
        public LeadFactory(QuoteUploadMessage message)
        {
            _quote = message.Quote;
            _metadata = message.GetMetadata<QuoteAcceptedMetadata>().TaskMetadata.Get<LeadTransferOakhurstTaskMetadata>();
        }


        public OakhurstService.Lead CreateLead()
        {
            Log.Info("Creating Oakhurst Lead Request.");


            if (_quote.Request.Items.Count == 0)
            {
                Log.Info("No Rating Request Items exist for Quote to be uploaded to Oakhurst");
                return null;
            }

            var oakhurstResult = SerialiseRequest();

            var lead = new OakhurstService.Lead
            {
                Comments = _quote.AgentDetail.Comments,
                DateOfQuote = DateTime.UtcNow,
                PremiumGiven = true,
                TotalPremium = Convert.ToDouble(_quote.Policy.Items.Sum(x => x.Premium)),
                CampaignID = string.IsNullOrEmpty(_metadata.CampaignId) ? 6 : int.Parse(_metadata.CampaignId),
                ExclusiveOption = false,
                FixedPremiumOption = false,
                NewBusiness = true,
                RaterVersion = 0,
                TestInd = false,
                AgentDetail = AgentDetail(),
                InsuredDetail = GetQuoteInsured(oakhurstResult),
                SchemeDetail = SchemeDetail(),
                Vehicles = GetQuoteVehicleDetails(oakhurstResult).Any() ? GetQuoteVehicleDetails(oakhurstResult).ToArray() : null,
                HouseContents = GetQuoteNonMotorDetails(oakhurstResult).Any() ?  GetQuoteNonMotorDetails(oakhurstResult).ToArray() : null
            };

            Log.DebugFormat("Lead: {0}", SerializerExtensions.Serialize(lead));
            return lead;
        }

        private Agent AgentDetail()
        {
            return new Agent
            {
                AgentCode = _metadata.AgentCode,
                AgentEmail = string.Format("{0};{1}", _metadata.AgentEmail, _quote.AgentDetail.EmailAddress),
                AgentName = _metadata.AgentName,
                AgentTelephone = _metadata.AgentTelephone,
                BrokerCode = _metadata.BrokerCode,
                SubPartnerCode = _metadata.SubPartnerCode,
                SubPartnerName = _metadata.SubPartnerName
            };
        }



        private Product SchemeDetail()
        {
            var schemeDetail = new Product
            {
                Scheme = _quote.Policy.ProductCode == "LITEAA" ? "LITE" : _quote.Policy.ProductCode //ConfigurationHelper.Read<string>("Oakhurst/SchemeName", true, null)
            };
            return schemeDetail;

        }

        private List<VehicleDetail> GetQuoteVehicleDetails(OakhurstRequestDto oakhurstResult)
        {
            
            var vehicleDetails = new List<VehicleDetail>();
            if (oakhurstResult.Quote.Vehicles == null) return vehicleDetails;
            foreach (var vehcile in oakhurstResult.Quote.Vehicles)
            {
                var mappedVehicle = Mapper.Map<Vehicle, VehicleDetail>(vehcile);

                var rateingRequestItem = _quote.Request.Items.FirstOrDefault(x => x.AssetNo == vehcile.ItemSeqNo);
                var vehicleResult = _quote.Policy.Items.FirstOrDefault(c => c.AssetNo == vehcile.ItemSeqNo);

                if (rateingRequestItem != null)
                {
                    mappedVehicle.RiskAddressLine1 = ConversionFactory<string>.ConvertAnswer(rateingRequestItem, Questions.RiskAddress);
                    mappedVehicle.PremiumDetail = ItemPremium(vehicleResult ?? new RatingResultItemDto());
                    mappedVehicle.ExcessOption = (int)(vehicleResult ?? new RatingResultItemDto()).Excess.Basic;
                    mappedVehicle.InceptionDate = DateTime.Now;
                }
                vehicleDetails.Add(mappedVehicle);
            }
            return vehicleDetails;
        }

        private List<NonMotorDetail> GetQuoteNonMotorDetails(OakhurstRequestDto oakhurstResult)
        {
            var contentDetails = new List<NonMotorDetail>();
            if (oakhurstResult.Quote.NonMotorItems == null) return contentDetails;
            foreach (var content in oakhurstResult.Quote.NonMotorItems)
            {
                var rescontent = Mapper.Map<NonMotor, NonMotorDetail>(content);
                var rateingRequestItem = _quote.Request.Items.FirstOrDefault(x => x.AssetNo == content.ItemSeqNo);
                var nonMotorResult = _quote.Policy.Items.FirstOrDefault(c => c.AssetNo == content.ItemSeqNo);
                if (rateingRequestItem != null)
                {
                    rescontent.RiskAddressLine1 = ConversionFactory<string>.ConvertAnswer(rateingRequestItem, Questions.RiskAddress);
                    rescontent.PremiumDetail = ItemPremium(nonMotorResult ?? new RatingResultItemDto());
                    rescontent.ExcessOption = (int)(nonMotorResult ?? new RatingResultItemDto()).Excess.Basic;
                    rescontent.InceptionDate = DateTime.Now;
                }
                contentDetails.Add(rescontent);
            }
            return contentDetails;
        }

        private Insured GetQuoteInsured(OakhurstRequestDto oakhurstResult)
        {
            return Mapper.Map<Person, Insured>(oakhurstResult.Quote.Insured);
        }

        private static ItemPremium ItemPremium(RatingResultItemDto result)
        {
            var premium = new ItemPremium
            {
                OtherFee = 0,
                RiskPremium = Convert.ToDouble(result.Premium),
                SasriaPremium = Convert.ToDouble(result.Sasria),
                SystemsFee = 0,
                TrackerPremium = 0
            };
            return premium;
        }

        private OakhurstRequestDto SerialiseRequest()
        {
            var result = new OakhurstRequestDto();
            if (!string.IsNullOrEmpty(_quote.Policy.ExternalRatingRequest))
            {
                result = _quote.Policy.ExternalRatingRequest.DeSerializeToObject<OakhurstRequestDto>();
            }
            return result;
        }
      
    }
}