﻿using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead
{
    public static class Authentication
    {
        public static AuthHeader AuthHeader(string logonId, string password)
        {
            return new AuthHeader
            {
                LogonID = logonId,
                Password = password
            };
        }
    }
}