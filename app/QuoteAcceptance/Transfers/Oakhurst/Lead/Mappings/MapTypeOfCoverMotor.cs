﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead.Mappings
{
    internal static class MapTypeOfCoverMotor 
    {
        public static CoverCode Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<CoverCode>>();
            typeTests[QuestionAnswers.TypeOfCoverMotorComprehensive.Answer] = () => CoverCode.Comprehensive;
            typeTests[QuestionAnswers.TypeOfCoverMotorTheftExcluded.Answer] = () => CoverCode.TLO;
            typeTests[QuestionAnswers.TypeOfCoverMotorThirdPartyFireandTheft.Answer] = () => CoverCode.TPFT;
            typeTests[QuestionAnswers.TypeOfCoverMotorThirdPartyOnly.Answer] = () => CoverCode.TPO;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(CoverCode) : returnFunc.Value();
        }
    }
}
