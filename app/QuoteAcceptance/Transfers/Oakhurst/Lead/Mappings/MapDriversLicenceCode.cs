﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead.Mappings
{
    internal static class MapDriversLicenceCode 
    {
        public static LicenseCode Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<LicenseCode>>();
            typeTests[QuestionAnswers.DriversLicenceTypeA.Answer] = () => LicenseCode.License_A;
            typeTests[QuestionAnswers.DriversLicenceTypeA1.Answer] = () => LicenseCode.License_A1;
            typeTests[QuestionAnswers.DriversLicenceTypeB.Answer] = () => LicenseCode.License_B;
            typeTests[QuestionAnswers.DriversLicenceTypeC.Answer] = () => LicenseCode.License_C;
            typeTests[QuestionAnswers.DriversLicenceTypeC1.Answer] = () => LicenseCode.License_C1;
            typeTests[QuestionAnswers.DriversLicenceTypeEB.Answer] = () => LicenseCode.License_EB;
            typeTests[QuestionAnswers.DriversLicenceTypeEC.Answer] = () => LicenseCode.License_EC;
            typeTests[QuestionAnswers.DriversLicenceTypeEC1.Answer] = () => LicenseCode.License_EC;
            typeTests[QuestionAnswers.DriversLicenceTypeCommonWealth.Answer] = () => LicenseCode.License_International;
            typeTests[QuestionAnswers.DriversLicenceTypeLearnersLicence.Answer] = () => LicenseCode.License_Learners;
            typeTests[QuestionAnswers.DriversLicenceTypeInternationalDriversPermit.Answer] = () => LicenseCode.License_International;
            typeTests[QuestionAnswers.DriversLicenceTypeNeighbouringState.Answer] = () => LicenseCode.License_International;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(LicenseCode) : returnFunc.Value();
        }
    }
}
