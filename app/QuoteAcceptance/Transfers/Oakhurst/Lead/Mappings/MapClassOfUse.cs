﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead.Mappings
{
    internal static class MapClassOfUse 
    {
        public static UseCode Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<UseCode>>();
            typeTests[QuestionAnswers.ClassOfUseBusiness.Answer] = () => UseCode.BusinessUse;
            typeTests[QuestionAnswers.ClassOfUseBusinessGoodsCarrying.Answer] = () => UseCode.CourierService;
            typeTests[QuestionAnswers.ClassOfUsePrivateIncludingWork.Answer] = () => UseCode.PrivateUse;
            typeTests[QuestionAnswers.ClassOfUseTaxi.Answer] = () => UseCode.TaxiUse;
            typeTests[QuestionAnswers.ClassOfUseCompanyMultipleDrivers.Answer] = () => UseCode.BusinessUse;
            typeTests[QuestionAnswers.ClassOfUseFarming.Answer] = () => UseCode.BusinessUse;
            typeTests[QuestionAnswers.ClassOfUseStrictlyPrivate.Answer] = () => UseCode.PrivateUse;
            typeTests[QuestionAnswers.ClassOfUseUnknown.Answer] = () => UseCode.PrivateUse;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(UseCode) : returnFunc.Value();
        }
    }
}
