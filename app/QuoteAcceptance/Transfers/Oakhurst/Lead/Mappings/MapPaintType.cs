﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead.Mappings
{
    internal static class MapPaintType
    {
        public static PaintTypeCode Map(string answer)
        {
            var data = new Dictionary<string, Func<PaintTypeCode>>();
            data[QuestionAnswers.VehiclePaintTypeMetallic.Answer] = () =>  PaintTypeCode.Metallic;
            data[QuestionAnswers.VehiclePaintTypePearl.Answer] = () =>  PaintTypeCode.Pearl;
            data[QuestionAnswers.VehiclePaintTypePlain.Answer] = () =>  PaintTypeCode.Plain;
            data[QuestionAnswers.VehiclePaintTypeWrappedMultiColour.Answer] = () =>  PaintTypeCode.Wrapped_Multi_Colour;
            data[QuestionAnswers.VehiclePaintTypeWrappedSingleColour.Answer] = () =>  PaintTypeCode.Wrapped_Single_Colour;
            data[QuestionAnswers.VehiclePaintTypeNotObtained.Answer] = () =>  PaintTypeCode.Plain;
            data[QuestionAnswers.VehiclePaintTypeUnknown.Answer] = () =>  PaintTypeCode.Plain;

            var returnFunc = data.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(PaintTypeCode) : returnFunc.Value();
        }
    }
}
