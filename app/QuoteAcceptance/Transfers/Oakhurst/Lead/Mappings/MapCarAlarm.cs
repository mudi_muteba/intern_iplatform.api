﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead.Mappings
{
    internal static class MapCarAlarm
    {
        public static AlarmCode Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<AlarmCode>>();
            typeTests[QuestionAnswers.VehicleImmobiliserFactoryFitted.Answer] = () => AlarmCode.Factory_Fitted;
            typeTests[QuestionAnswers.VehicleImmobiliserNone.Answer] = () => AlarmCode.No_Immoboliser;
            typeTests[QuestionAnswers.VehicleImmobiliserVESA3SecureDevice.Answer] = () => AlarmCode.VESA_Approved;
            typeTests[QuestionAnswers.VehicleImmobiliserVESA4SecureDevice.Answer] = () => AlarmCode.VESA_Approved;
            typeTests[QuestionAnswers.VehicleImmobiliserVESA4withAntiHiJack.Answer] = () => AlarmCode.VESA_Approved;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(AlarmCode) : returnFunc.Value();
        }
    }
}
