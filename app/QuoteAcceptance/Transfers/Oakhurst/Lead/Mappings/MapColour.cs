﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead.Mappings
{
    internal static class MapColour
    {
        public static ColorCode Map(string answer)
        {
            var data = new Dictionary<string, Func<ColorCode>>();
            data[QuestionAnswers.VehicleColourBeige.Answer] = () => ColorCode.Beige;
            data[QuestionAnswers.VehicleColourBlack.Answer] = () => ColorCode.Black;
            data[QuestionAnswers.VehicleColourBlue.Answer] = () => ColorCode.Blue;
            data[QuestionAnswers.VehicleColourBlueGreen.Answer] = () => ColorCode.Blue;
            data[QuestionAnswers.VehicleColourBronze.Answer] = () => ColorCode.Bronze;
            data[QuestionAnswers.VehicleColourBrown.Answer] = () => ColorCode.Brown;
            data[QuestionAnswers.VehicleColourBurgundy.Answer] = () => ColorCode.Burgundy;
            data[QuestionAnswers.VehicleColourChampagne.Answer] = () => ColorCode.Champaign;
            data[QuestionAnswers.VehicleColourCream.Answer] = () => ColorCode.Cream;
            data[QuestionAnswers.VehicleColourGold.Answer] = () => ColorCode.Gold;
            data[QuestionAnswers.VehicleColourGoldSilver.Answer] = () => ColorCode.Gold;
            data[QuestionAnswers.VehicleColourGreen.Answer] = () => ColorCode.Green;
            data[QuestionAnswers.VehicleColourGrey.Answer] = () => ColorCode.Grey;
            data[QuestionAnswers.VehicleColourGreyBlack.Answer] = () => ColorCode.Grey;
            data[QuestionAnswers.VehicleColourMaroon.Answer] = () => ColorCode.Maroon;
            data[QuestionAnswers.VehicleColourOrange.Answer] = () => ColorCode.Orange;
            data[QuestionAnswers.VehicleColourPink.Answer] = () => ColorCode.Pink;
            data[QuestionAnswers.VehicleColourPinkRed.Answer] = () => ColorCode.Pink;
            data[QuestionAnswers.VehicleColourPurple.Answer] = () => ColorCode.Purple;
            data[QuestionAnswers.VehicleColourRed.Answer] = () => ColorCode.Red;
            data[QuestionAnswers.VehicleColourSilver.Answer] = () => ColorCode.Silver;
            data[QuestionAnswers.VehicleColourTan.Answer] = () => ColorCode.Tan;
            data[QuestionAnswers.VehicleColourTanBrown.Answer] = () => ColorCode.Tan;
            data[QuestionAnswers.VehicleColourWhite.Answer] = () => ColorCode.White;
            data[QuestionAnswers.VehicleColourWhiteCream.Answer] = () => ColorCode.Cream;
            data[QuestionAnswers.VehicleColourYellow.Answer] = () => ColorCode.Yellow;
            data[QuestionAnswers.VehicleColourYellowOrange.Answer] = () => ColorCode.Yellow;
            data[QuestionAnswers.VehicleColourNotObtained.Answer] = () => ColorCode.Other;
            data[QuestionAnswers.VehicleColourUnknown.Answer] = () => ColorCode.Other;

            var returnFunc = data.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(ColorCode) : returnFunc.Value();
        }
    }
}
