﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead.Mappings
{
    internal static class MapGender 
    {
        public static GenderCode Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<GenderCode>>();
            typeTests[QuestionAnswers.MainDriverGenderMale.Answer] = () => GenderCode.Male;
            typeTests[QuestionAnswers.MainDriverGenderFemale.Answer] = () => GenderCode.Female;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(GenderCode) : returnFunc.Value();
        }
    }
}
