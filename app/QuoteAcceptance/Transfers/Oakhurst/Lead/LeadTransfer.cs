﻿using Common.Logging;
using Workflow.QuoteAcceptance.Oakhurst.OakhurstService;

namespace Workflow.QuoteAcceptance.Oakhurst.Lead
{
    public static  class LeadTransfer
    {
        private static readonly ILog Log = LogManager.GetLogger("LeadTransfer");

        public static ServiceResults TransferLead(IOakhurstServiceClient client, AuthHeader authHeader, OakhurstService.Lead lead)
        {
            Log.Info("Uploading Quote to Oakhurst Service");
            return client.transferLead(authHeader, lead, lead.Comments);
        }
    }
}
