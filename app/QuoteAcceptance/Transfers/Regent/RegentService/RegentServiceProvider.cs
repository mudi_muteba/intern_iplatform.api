﻿using System;
using Common.Logging;
using Domain.Policies.Message;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Infrastructure;
using Workflow.QuoteAcceptance.Regent.Lead;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.Regent.RegentService
{
    public class RegentServiceProvider : ITransferLeadToInsurer
    {
        private static readonly ILog Log = LogManager.GetLogger<RegentServiceProvider>();
        private readonly IRegentServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private readonly CommunicationMetadata _mail;
        private readonly Guid _channelId;
        public RegentServiceProvider(IRegentServiceClient client, LeadFactory leadFactory, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            _client = client;
            _leadFactory = leadFactory;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var quote = message.Quote;
            var lead = _leadFactory.CreateLead();

            if (string.IsNullOrEmpty(lead))
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to Regent. No items were found in the Quote", quote.Request.Id.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("No details were found in the Quote", message.SerializeAsXml(), quote.Request.Id,InsurerName.Regent.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureNoQuote(InsurerName.Regent.Name(), quote.Request.Id,_channelId));
                return new LeadTransferResult(false);
            }

            var result = Execute(quote, lead);

            if (!result)
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to Regent", quote.Request.Id.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Quote [{0}] failed to upload to Regent", quote.Request.Id), lead.SerializeAsXml(), quote.Request.Id,InsurerName.Regent.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.Regent.Name(),quote.Request.Id,_channelId));
                return new LeadTransferResult(false);
            }

            Log.InfoFormat("Quote [{0}] successfully uploaded to Regent", quote.Request.Id.ToString());
            return new LeadTransferResult(true);
        }

        private bool Execute(PublishQuoteUploadMessageDto quote, string lead)
        {
            var responseSuccess = false;
            var errorMessage = string.Empty;
            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.Regent.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, ReferenceNumber));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, ReferenceNumber));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.Regent.Name(), quote.Request.Id, _channelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.Regent.Name(),quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.Regent.Name(),quote.Request.Id, response.ToString(), _channelId)))
                .While(() => !responseSuccess)
                .Execute(() =>
                {
                    try
                    {
                        //if (quote.WorkflowEnvironment == "dev")
                        //{
                        //    Log.Warn("quoteacceptance/environment = dev. Quote was not uploaded");
                        //    responseSuccess = true;
                        //}  
                        //else
                            responseSuccess = Upload(_client, lead);

                        ReferenceNumber = Guid.NewGuid().ToString();
                        errorMessage = responseSuccess ? string.Empty : "Failure"; 
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.Regent.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.Regent.Name(),quote.Request.Id, ex, _channelId));
                    }
                    return responseSuccess;
                });

            return responseSuccess;
        }

        public static bool Upload(IRegentServiceClient client, string lead)
        {
            return LeadTransfer.TransferLead(client, lead);
        }

        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }
    }
}