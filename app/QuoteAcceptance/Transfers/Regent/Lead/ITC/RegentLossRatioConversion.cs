using System;
using System.Collections.Generic;

namespace Workflow.QuoteAcceptance.Regent.Lead.ITC
{
    internal class RegentLossRatioConversion : IProvideITCConversionData
    {
        private readonly Dictionary<Func<int?, bool>, Func<int?, string>> conversion = new Dictionary<Func<int?, bool>, Func<int?, string>>();

        public RegentLossRatioConversion()
        {
            // nothing from TU
            conversion.Add(score => !score.HasValue, _ => "0");

            // scores 1 - 7 maps directly to the same score
            conversion.Add(score => score.HasValue && (score.Value >= 1 && score.Value <= 7), score => score.Value.ToString());

            // deceased individual
            conversion.Add(score => score.HasValue && score.Value == -2, _ => "8");

            // administration order
            conversion.Add(score => score.HasValue && score.Value == -3, _ => "8");

            // does not consent. Should never happen as iRate should not invoke the ITC check, if the user 
            // did not consent to the check
            // default it to unknown
            conversion.Add(_ => true, _ => "13");
        }

        public Dictionary<Func<int?, bool>, Func<int?, string>> ConversionData
        {
            get { return conversion; }
        }
    }
}