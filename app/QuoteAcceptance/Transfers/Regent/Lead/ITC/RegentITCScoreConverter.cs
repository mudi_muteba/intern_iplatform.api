using System;
using System.Linq;
using Common.Logging;

namespace Workflow.QuoteAcceptance.Regent.Lead.ITC
{
    internal class RegentITCScoreConverter
    {
        private static readonly ILog log = LogManager.GetLogger<RegentITCScoreConverter>();

        public string Convert(string scoreToConvert, IProvideITCConversionData data)
        {
            int? score = null;

            if (!string.IsNullOrEmpty(scoreToConvert))
            {
                var tempScore = 0;
                if (int.TryParse(scoreToConvert, out tempScore))
                {
                    score = tempScore;
                }
            }

            var matching = data.ConversionData.FirstOrDefault(c => c.Key(score));

            if (matching.Key == null)
            {
                var message = string.Format("Could not convert TU ITC score for Regent. The ITC score was {0} and the data was of type {1}",
                    score.HasValue ? score.Value.ToString() : "NO VALUE", data.GetType());

                log.ErrorFormat(message);

                return "0";
            }

            return matching.Value(score);
        }
    }
}