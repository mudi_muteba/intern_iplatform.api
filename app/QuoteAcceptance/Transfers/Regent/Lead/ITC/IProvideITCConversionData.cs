using System;
using System.Collections.Generic;

namespace Workflow.QuoteAcceptance.Regent.Lead.ITC
{
    internal interface IProvideITCConversionData
    {
        Dictionary<Func<int?, bool>, Func<int?, string>> ConversionData { get; }
    }
}