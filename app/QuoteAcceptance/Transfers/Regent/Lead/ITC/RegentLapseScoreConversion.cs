using System;
using System.Collections.Generic;

namespace Workflow.QuoteAcceptance.Regent.Lead.ITC
{
    internal class RegentLapseScoreConversion : IProvideITCConversionData
    {
        private readonly Dictionary<Func<int?, bool>, Func<int?, string>> conversion = new Dictionary<Func<int?, bool>, Func<int?, string>>();

        public RegentLapseScoreConversion()
        {
            // nothing from TU
            conversion.Add(score => !score.HasValue, _ => "0");

            // scores 1 - 11 maps directly to the same score
            conversion.Add(score => score.HasValue && (score.Value >= 1 && score.Value <= 11), score => score.Value.ToString());

            // no credit information
            conversion.Add(score => score.HasValue && score.Value == -1, _ => "0");

            // immature credit record
            conversion.Add(score => score.HasValue && score.Value == -2, _ => "12");

            // negative credit performance
            conversion.Add(score => score.HasValue && score.Value == -3, _ => "11");

            // user does not consent. Should never happen as iRate should not invoke the ITC check, if the user 
            // did not consent to the check
            // default it to unknown
            conversion.Add(_ => true, _ => "0");
        }

        public Dictionary<Func<int?, bool>, Func<int?, string>> ConversionData
        {
            get { return conversion; }
        }
    }
}