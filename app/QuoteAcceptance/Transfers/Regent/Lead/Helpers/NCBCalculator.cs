﻿using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using Workflow.QuoteAcceptance.Regent.Lead.Mappings;

namespace Workflow.QuoteAcceptance.Regent.Lead.Helpers
{
    public class NcbCalculator
    {
        

        public static int GetClaimsLast12Months(RatingRequestItemDto item)
        {
            return MapClaimsLast12Months.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.ClaimsLast0to12Months));
        }

        public static int GetClaimsLast24Months(RatingRequestItemDto item)
        {
            return MapClaimsLast24Months.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.ClaimsLast12to24Months));
        }

        public static int GetClaimsLast36Months(RatingRequestItemDto item)
        {
            return MapClaimsLast36Months.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.ClaimsLast24to36Months));
        }

    }
}
