﻿using System;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Domain;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using Workflow.QuoteAcceptance.Regent.Lead.Helpers;
using Workflow.QuoteAcceptance.Regent.Lead.ITC;
using Workflow.QuoteAcceptance.Regent.Lead.Mappings;

namespace Workflow.QuoteAcceptance.Regent.Lead
{
    public class LeadFactory
    {
        private static readonly ILog log = LogManager.GetLogger<LeadFactory>();
        private readonly PublishQuoteUploadMessageDto _quote;
        public LeadFactory(QuoteUploadMessage message)
        {
            _quote = message.Quote;
        }

        public string CreateLead()
        {
            log.Info("Creating Regent Lead Request.");

            var xDoc = new XDocument();
            if (_quote.Request.Items.Count > 0)
            {
                var xLead = new XElement("lead");
                xLead.Add(Source());
                xLead.Add(InsuredDetail());
                xLead.Add(SectionDetail());
                xDoc.Add(xLead);
            }

            log.InfoFormat("Lead: {0}", xDoc.ToString());
            return xDoc.ToString();
        }

        private static XElement Source()
        {
            var xSource = new XElement("source");
            xSource.Add(new XElement("submitdate", DateTime.UtcNow.ToString("d")));
            xSource.Add(new XElement("name", "AllMyStuff"));
            xSource.Add(new XElement("campaign", "none"));
            xSource.Add(new XElement("language", ""));
            xSource.Add(new XElement("contactmethod", "Mobile"));
            return xSource;
        }

        private XElement InsuredDetail()
        {
            var xClient = new XElement("client");
            xClient.Add(new XElement("title", _quote.InsuredInfo.Title.Abbreviation ?? ""));
            xClient.Add(new XElement("name", _quote.InsuredInfo.Firstname ?? ""));
            xClient.Add(new XElement("surname", _quote.InsuredInfo.Surname ?? ""));
            xClient.Add(new XElement("idnumber", _quote.InsuredInfo.IDNumber ?? ""));
            xClient.Add(new XElement("telhome", ""));
            xClient.Add(new XElement("telwork", ""));
            xClient.Add(new XElement("mobile", _quote.InsuredInfo.ContactNumber ?? ""));
            xClient.Add(new XElement("email", string.Format("{0};{1}", "monitoring@iplatform.co.za", _quote.AgentDetail.EmailAddress)));
            xClient.Add(new XElement("physaddr1", _quote.Request.Policy.Persons[0].Addresses[0].Address1 ?? ""));
            xClient.Add(new XElement("physaddr2", _quote.Request.Policy.Persons[0].Addresses[0].Suburb ?? ""));
            xClient.Add(new XElement("physaddr3", ""));
            xClient.Add(new XElement("physaddr4", ""));

            var pcode = Int32.Parse(_quote.Request.Policy.Persons[0].Addresses[0].PostalCode);
            xClient.Add(new XElement("physcode", pcode.ToString("0000")));

            xClient.Add(new XElement("postaddr1", ""));
            xClient.Add(new XElement("postaddr2", ""));
            xClient.Add(new XElement("postaddr3", ""));
            xClient.Add(new XElement("postaddr4", ""));
            xClient.Add(new XElement("postcode", ""));

            return xClient;
        }

        private XElement SectionDetail()
        {
            var houseHoldersSI = string.Empty;
            foreach (var item in _quote.Request.Items)
            {
                if (item.Cover.Id.Equals(Covers.Contents.Id))
                    houseHoldersSI = ConversionFactory<decimal>.ConvertAnswer(item, Questions.SumInsured).ToString(CultureInfo.InvariantCulture);
            }
            var xSection = new XElement("products");

            foreach (var item in _quote.Request.Items)
            {
                var premium =
                      _quote.Policy.Items.FirstOrDefault(
                          prem => prem.AssetNo == item.AssetNo && Equals(prem.Cover, item.Cover)) ?? new RatingResultItemDto();

                var xItem = new XElement("coveritem");

                if (item.Cover.Id.Equals(Covers.AllRisk.Id))
                    xSection.Add(AllRiskDetails(item, premium, xItem, houseHoldersSI));

                if (item.Cover.Id.Equals(Covers.Motor.Id))
                    xSection.Add(VehicleDetails(_quote.Request.Id.ToString(), item, premium, xItem));

                if (item.Cover.Id.Equals(Covers.Contents.Id))
                    xSection.Add(ContentDetails(_quote.Request.Id.ToString(), item, premium, xItem));

                if (item.Cover.Id.Equals(Covers.Building.Id))
                    xSection.Add(BuildingDetails(_quote.Request.Id.ToString(), item, premium, xItem));
            }

            return xSection;
        }

        private static XElement AllRiskDetails(RatingRequestItemDto item, RatingResultItemDto premium, XElement xItem, string houseHoldersSI)
        {
            var allRiskSection = ConversionFactory<string>.ConvertAnswer(item, Questions.AllRiskCategory).Contains("Unspecified") ? 3 : 2;

            xItem.Add(new XElement("sectioncode", allRiskSection));
            xItem.Add(new XElement("desc", ConversionFactory<string>.ConvertAnswer(item, Questions.ItemDescription)));
            xItem.Add(new XElement("premium", premium.Premium));
            xItem.Add(new XElement("suminsured", ConversionFactory<string>.ConvertAnswer(item, Questions.SumInsured)));
            xItem.Add(allRiskSection == 2
                ? new XElement("param1",
                    MapAllRiskCategory.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.AllRiskCategory)))
                : new XElement("param1", houseHoldersSI));

            xItem.Add(new XElement("param2", ""));
            xItem.Add(new XElement("param3", ""));
            xItem.Add(new XElement("param4", ""));
            xItem.Add(new XElement("param5", ""));
            xItem.Add(new XElement("param6", ""));
            xItem.Add(new XElement("param7", ""));
            xItem.Add(new XElement("param8", ""));
            xItem.Add(new XElement("param9", ""));
            xItem.Add(new XElement("param10", ""));
            xItem.Add(new XElement("param11", ""));
            xItem.Add(new XElement("param12", ""));
            xItem.Add(new XElement("param13", ""));
            xItem.Add(new XElement("param14", ""));
            xItem.Add(new XElement("param15", ""));
            xItem.Add(new XElement("param16", ""));
            xItem.Add(new XElement("param17", ""));
            xItem.Add(new XElement("param18", ""));
            xItem.Add(new XElement("param19", ""));
            xItem.Add(new XElement("param20", ""));
            xItem.Add(new XElement("param21", ""));
            xItem.Add(new XElement("param22", ""));
            xItem.Add(new XElement("param23", ""));
            xItem.Add(new XElement("param24", ""));
            xItem.Add(new XElement("param25", ""));
            xItem.Add(new XElement("param26", ""));
            xItem.Add(new XElement("param27", ""));
            xItem.Add(new XElement("param28", ""));
            xItem.Add(new XElement("param29", ""));
            xItem.Add(new XElement("param30", ""));

            return xItem;
        }

        private XElement VehicleDetails(string referenceNumber, RatingRequestItemDto item, RatingResultItemDto premium, XElement xItem)
        {
            var idNumberInfo = new IdentityNumberInfo(DateTime.Today.Date.ToString(CultureInfo.InvariantCulture));

            if (ConversionFactory<string>.ConvertAnswer(item, Questions.MainDriverIDNumber) != "")
                idNumberInfo = new IdentityNumberInfo(ConversionFactory<string>.ConvertAnswer(item, Questions.MainDriverIDNumber));

            xItem.Add(new XElement("sectioncode", "129"));
            xItem.Add(new XElement("desc", "Car"));
            xItem.Add(new XElement("premium", premium.Premium));
            xItem.Add(new XElement("suminsured", ConversionFactory<string>.ConvertAnswer(item, Questions.SumInsured)));

            xItem.Add(new XElement("param1",
                MapGender.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.MainDriverGender)))); //MapGender.Map(idNumberInfo.Gender.Name)));

            var dob = ConversionFactory<DateTime>.ConvertAnswer(item, Questions.MainDriverDateofBirth);
            var age = dob.GetAge();
            xItem.Add(new XElement("param2", age.ToString())); //ConversionFactory<string>.ConvertAnswer(item, Questions.MainDriverAge)));

            xItem.Add(new XElement("param3",
                MapMaritalStatus.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.MainDriverMaritalStatus))));

            //todo do change as per rating side if needed
            xItem.Add(new XElement("param4",
                MapNCB.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.NoClaimBonus),item)));

            xItem.Add(new XElement("param5",
                MapBoolMotor.Map(ConversionFactory<bool>.ConvertAnswer(item, Questions.PreviouslyInsured))));

            xItem.Add(new XElement("param6",
                ConversionFactory<DateTime>.ConvertAnswer(item, Questions.DriversLicenceFirstIssuedDate)
                    .Year.ToString(CultureInfo.InvariantCulture)));

            xItem.Add(new XElement("param7",
                MapClassOfUse.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.ClassOfUse))));

            var vYear = ConversionFactory<int>.ConvertAnswer(item, Questions.YearOfManufacture);
            var vAge = 0;
            if (vYear == 0)
            {
                vAge = 26;
            }
            else
            {
                vAge = DateTime.UtcNow.Year - vYear;
            }
            xItem.Add(new XElement("param8", vAge)); //ConversionFactory<int>.ConvertAnswer(item, Questions.VehicleAge)

            xItem.Add(new XElement("param9", ConversionFactory<int>.ConvertAnswer(item, Questions.VehicleMMCode)));

            xItem.Add(new XElement("param10",
                MapTrackingDevice.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.VehicleTrackingDevice))));

            var pcode = ConversionFactory<int>.ConvertAnswer(item, Questions.PostalCode);
            xItem.Add(new XElement("param11", pcode.ToString("0000")));

            xItem.Add(new XElement("param12",
                MapTypeOfCoverMotor.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.TypeOfCoverMotor))));

            var pcodework = ConversionFactory<int>.ConvertAnswer(item, Questions.PostalCodeWork);
            xItem.Add(new XElement("param13", pcodework.ToString("0000")));

            xItem.Add(new XElement("param14",
                MapVoluntaryExcess.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.VoluntaryExcess))));

            xItem.Add(new XElement("param15",
                MapGaraging.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.VehicleOvernightParking))));

            xItem.Add(new XElement("param16", new RegentITCScoreConverter().Convert(_quote.Policy.ExtraITCScore,
                new RegentLossRatioConversion())));

            xItem.Add(new XElement("param17", new RegentITCScoreConverter().Convert(_quote.Policy.ITCScore,
                new RegentLapseScoreConversion())));

            xItem.Add(new XElement("param18",
                MapBoolMotor.Map(ConversionFactory<bool>.ConvertAnswer(item, Questions.ExcessWaiver))));
            
            xItem.Add(new XElement("param19", ""));

            xItem.Add(new XElement("param20", ConversionFactory<string>.ConvertAnswer(item, Questions.Suburb)));

            xItem.Add(new XElement("param21", ""));

            xItem.Add(new XElement("param22", ""));

            xItem.Add(new XElement("param23", ""));

            xItem.Add(new XElement("param24", ""));

            xItem.Add(new XElement("param25", ""));

            xItem.Add(new XElement("param26", ""));

            xItem.Add(new XElement("param27", ""));

            xItem.Add(new XElement("param28", ""));

            xItem.Add(new XElement("param29", ""));

            xItem.Add(new XElement("param30", referenceNumber));

            return xItem;
        }

        private static XElement ContentDetails(string referenceNumber, RatingRequestItemDto item, RatingResultItemDto premium, XElement xItem)
        {
            xItem.Add(new XElement("sectioncode", "93"));
            xItem.Add(new XElement("desc", ""));
            xItem.Add(new XElement("premium", premium.Premium));
            xItem.Add(new XElement("suminsured", ConversionFactory<string>.ConvertAnswer(item, Questions.SumInsured)));
            xItem.Add(new XElement("param1",  ConversionFactory<string>.ConvertAnswer(item, Questions.PostalCode)));
            xItem.Add(new XElement("param2", MapBoolContents.Map(ConversionFactory<bool>.ConvertAnswer(item, Questions.BurglarAlarmLinkedTo24HrArmedResponse))));
            xItem.Add(new XElement("param3", MapBoolContents.Map(ConversionFactory<bool>.ConvertAnswer(item, Questions.BurglarBars))));
            xItem.Add(new XElement("param4", MapBoolContents.Map(ConversionFactory<bool>.ConvertAnswer(item, Questions.SecureComplex))));
            xItem.Add(new XElement("param5", MapBoolContents.Map(ConversionFactory<bool>.ConvertAnswer(item, Questions.ElectricFencing))));
            xItem.Add(new XElement("param6", ConversionFactory<int>.ConvertAnswer(item, Questions.InsuredAge)));
            xItem.Add(new XElement("param7", MapNCB.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.NoClaimBonus),item)));
            xItem.Add(new XElement("param8", MapBoolMotor.Map(ConversionFactory<bool>.ConvertAnswer(item, Questions.ExcludeBurglary))));
            xItem.Add(new XElement("param9", MapVoluntaryExcess.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.VoluntaryExcess))));
            xItem.Add(new XElement("param10", MapBoolContents.Map(ConversionFactory<bool>.ConvertAnswer(item, Questions.SubsidenceandLandslip))));
            xItem.Add(new XElement("param11", MapWallConstruction.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.WallConstruction))));
            xItem.Add(new XElement("param12", MapRoofConstruction.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.RoofConstruction))));
            xItem.Add(new XElement("param13", MapUnoccupied.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.Unoccupied))));
            xItem.Add(new XElement("param14", MapAccidentalDamage.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.AccidentalDamage))));
            xItem.Add(new XElement("param15", MapTypeOfResidence.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.ResidenceIs))));
            xItem.Add(new XElement("param16", ""));
            xItem.Add(new XElement("param17", ""));
            xItem.Add(new XElement("param18", ""));
            xItem.Add(new XElement("param19", ""));
            xItem.Add(new XElement("param20", referenceNumber));
            xItem.Add(new XElement("param21", ""));
            xItem.Add(new XElement("param22", ""));
            xItem.Add(new XElement("param23", ""));
            xItem.Add(new XElement("param24", ""));
            xItem.Add(new XElement("param25", ""));
            xItem.Add(new XElement("param26", ""));
            xItem.Add(new XElement("param27", ""));
            xItem.Add(new XElement("param28", ""));
            xItem.Add(new XElement("param29", ""));
            xItem.Add(new XElement("param30", ""));

            return xItem;
        }
        private static XElement BuildingDetails(string referenceNumber, RatingRequestItemDto item, RatingResultItemDto premium, XElement xItem)
        {
            xItem.Add(new XElement("sectioncode", "94"));
            xItem.Add(new XElement("desc", ""));
            xItem.Add(new XElement("premium", premium.Premium));
            xItem.Add(new XElement("suminsured", ConversionFactory<string>.ConvertAnswer(item, Questions.SumInsured)));
            xItem.Add(new XElement("param1", ConversionFactory<string>.ConvertAnswer(item, Questions.PostalCode)));
            xItem.Add(new XElement("param2", MapBoolContents.Map(ConversionFactory<bool>.ConvertAnswer(item, Questions.SubsidenceandLandslip))));
            xItem.Add(new XElement("param3", MapWallConstruction.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.WallConstruction))));
            xItem.Add(new XElement("param4", MapRoofConstruction.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.RoofConstruction))));
            xItem.Add(new XElement("param5", MapUnoccupied.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.Unoccupied))));
            xItem.Add(new XElement("param6", MapTypeOfResidence.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.ResidenceIs))));
            xItem.Add(new XElement("param7", ConversionFactory<int>.ConvertAnswer(item, Questions.InsuredAge)));
            xItem.Add(new XElement("param8", MapVoluntaryExcess.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.VoluntaryExcess))));
            xItem.Add(new XElement("param9", ""));
            xItem.Add(new XElement("param10", ""));
            xItem.Add(new XElement("param11", ""));
            xItem.Add(new XElement("param12", ""));
            xItem.Add(new XElement("param13", ""));
            xItem.Add(new XElement("param14", ""));
            xItem.Add(new XElement("param15", ""));
            xItem.Add(new XElement("param16", ""));
            xItem.Add(new XElement("param17", ""));
            xItem.Add(new XElement("param18", ""));
            xItem.Add(new XElement("param19", ""));
            xItem.Add(new XElement("param20", referenceNumber));
            xItem.Add(new XElement("param21", ""));
            xItem.Add(new XElement("param22", ""));
            xItem.Add(new XElement("param23", ""));
            xItem.Add(new XElement("param24", ""));
            xItem.Add(new XElement("param25", ""));
            xItem.Add(new XElement("param26", ""));
            xItem.Add(new XElement("param27", ""));
            xItem.Add(new XElement("param28", ""));
            xItem.Add(new XElement("param29", ""));
            xItem.Add(new XElement("param30", ""));

            return xItem;
        }

        private int NCBCalculate(RatingRequestItemDto item)
        {

            var last12Months = NcbCalculator.GetClaimsLast12Months(item);
            var last24Months = NcbCalculator.GetClaimsLast24Months(item);
            var last36Months = NcbCalculator.GetClaimsLast36Months(item);

            //no claims
            if ((last12Months == 0) & (last24Months == 0) & (last36Months == 0))
            {
                return 3;
            }

            //1 or more claims in all 3 periods
            if ((last12Months > 0) & (last24Months > 0) & (last36Months > 0))
            {
                return 0;
            }

            //2 or more claims in any single period AND 1 or more claims in any 2 periods
            if (((last24Months == 0) & (last36Months == 0)) & ((last12Months == 1) | (last12Months >= 2)))
            {
                return 0;
            }

            if ((last12Months == 0) & (last36Months == 0))
            {
                if ((last24Months == 1) | (last24Months == 2))
                {
                    return 1;
                }
                else if (last24Months >= 2)
                {
                    return 0;
                }
            }

            if ((last12Months == 0) & (last24Months == 0))
            {
                if ((last36Months == 1) | (last36Months == 2))
                {
                    return 2;
                }
                else if (last36Months >= 2)
                {
                    return 1;
                }
            }

            //1 or more in any 2 periods
            if (((last12Months >= 1) & ((last24Months >= 1) | (last36Months >= 1))) |
                ((last24Months == 1) & (last36Months == 1)) |
                ((last24Months > 1) & (last36Months > 1)))
            {
                return 0;
            }

            return 0;
        }
    }
}