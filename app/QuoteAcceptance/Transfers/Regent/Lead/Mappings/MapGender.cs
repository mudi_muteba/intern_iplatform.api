﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapGender 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.MainDriverGenderMale.Answer] = () => "0";
            typeTests[QuestionAnswers.MainDriverGenderFemale.Answer] = () => "1";
            typeTests[QuestionAnswers.MainDriverGenderUnknown.Answer] = () => "3";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "0" : returnFunc.Value();
        }
    }
}
