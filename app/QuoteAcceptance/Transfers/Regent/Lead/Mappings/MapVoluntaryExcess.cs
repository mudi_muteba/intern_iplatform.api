﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapVoluntaryExcess
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.VoluntaryExcess0.Answer] = () => "0";
            typeTests[QuestionAnswers.VoluntaryExcess250.Answer] = () => "0";
            typeTests[QuestionAnswers.VoluntaryExcess500.Answer] = () => "0";
            typeTests[QuestionAnswers.VoluntaryExcess1000.Answer] = () => "0";
            typeTests[QuestionAnswers.VoluntaryExcess1750.Answer] = () => "1";
            typeTests[QuestionAnswers.VoluntaryExcess2000.Answer] = () => "1";
            typeTests[QuestionAnswers.VoluntaryExcess2500.Answer] = () => "1";
            typeTests[QuestionAnswers.VoluntaryExcess3000.Answer] = () => "1";
            typeTests[QuestionAnswers.VoluntaryExcess4000.Answer] = () => "2";
            typeTests[QuestionAnswers.VoluntaryExcess5000.Answer] = () => "2";
            typeTests[QuestionAnswers.VoluntaryExcess6000.Answer] = () => "2";
            typeTests[QuestionAnswers.VoluntaryExcess7000.Answer] = () => "3";
            typeTests[QuestionAnswers.VoluntaryExcess7500.Answer] = () => "3";
            typeTests[QuestionAnswers.VoluntaryExcess8000.Answer] = () => "3";
            typeTests[QuestionAnswers.VoluntaryExcess9000.Answer] = () => "3";
            typeTests[QuestionAnswers.VoluntaryExcess10000.Answer] = () => "3";
            typeTests[QuestionAnswers.VoluntaryExcess15000.Answer] = () => "3";
            typeTests[QuestionAnswers.VoluntaryExcess20000.Answer] = () => "3";
            typeTests[QuestionAnswers.VoluntaryExcess25000.Answer] = () => "3";
            typeTests[QuestionAnswers.VoluntaryExcess30000.Answer] = () => "3";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "0" : returnFunc.Value();
        }
    }
}
