﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapTypeOfCoverMotor 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.TypeOfCoverMotorComprehensive.Answer] = () => "1";
            typeTests[QuestionAnswers.TypeOfCoverMotorTheftExcluded.Answer] = () => "3";
            typeTests[QuestionAnswers.TypeOfCoverMotorThirdPartyFireandTheft.Answer] = () => "2";
            typeTests[QuestionAnswers.TypeOfCoverMotorThirdPartyOnly.Answer] = () => "3";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "1" : returnFunc.Value();
        }
    }
}
