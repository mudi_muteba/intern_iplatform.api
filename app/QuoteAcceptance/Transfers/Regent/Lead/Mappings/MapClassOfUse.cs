﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapClassOfUse 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.ClassOfUseBusiness.Answer] = () => "1";
            typeTests[QuestionAnswers.ClassOfUseBusinessGoodsCarrying.Answer] = () => "7";
            typeTests[QuestionAnswers.ClassOfUsePrivateIncludingWork.Answer] = () => "0";
            typeTests[QuestionAnswers.ClassOfUseTaxi.Answer] = () => "5";
            typeTests[QuestionAnswers.ClassOfUseCompanyMultipleDrivers.Answer] = () => "4";
            typeTests[QuestionAnswers.ClassOfUseFarming.Answer] = () => "3";
            typeTests[QuestionAnswers.ClassOfUseStrictlyPrivate.Answer] = () => "2";
            typeTests[QuestionAnswers.ClassOfUseUnknown.Answer] = () => "6";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "0" : returnFunc.Value();
        }
    }
}
