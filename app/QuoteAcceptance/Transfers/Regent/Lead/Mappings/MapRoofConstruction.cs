﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapRoofConstruction
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.RoofConstructionStandard.Answer] = () => "2";
            typeTests[QuestionAnswers.RoofConstructionNonStandard.Answer] = () => "1";
            typeTests[QuestionAnswers.RoofConstructionThatchwithLightningconductor.Answer] = () => "3";
            typeTests[QuestionAnswers.RoofConstructionThatchwithoutLightningconductor.Answer] = () => "4";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "1" : returnFunc.Value();
        }
    }
}
