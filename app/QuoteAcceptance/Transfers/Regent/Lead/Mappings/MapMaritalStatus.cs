﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapMaritalStatus 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.MainDriverMaritalStatusCivilUnion.Answer] = () => "0";
            typeTests[QuestionAnswers.MainDriverMaritalStatusDivorced.Answer] = () => "0";
            typeTests[QuestionAnswers.MainDriverMaritalStatusDomesticPartner.Answer] = () => "0";
            typeTests[QuestionAnswers.MainDriverMaritalStatusMarried.Answer] = () => "0";
            typeTests[QuestionAnswers.MainDriverMaritalStatusOther.Answer] = () => "2";
            typeTests[QuestionAnswers.MainDriverMaritalStatusSeparated.Answer] = () => "0";
            typeTests[QuestionAnswers.MainDriverMaritalStatusSingle.Answer] = () => "1";
            typeTests[QuestionAnswers.MainDriverMaritalStatusUnincorporatedAssociation.Answer] = () => "0";
            typeTests[QuestionAnswers.MainDriverMaritalStatusUnknown.Answer] = () => "2";
            typeTests[QuestionAnswers.MainDriverMaritalStatusWidowed.Answer] = () => "0";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "2" : returnFunc.Value();
        }
    }
}
