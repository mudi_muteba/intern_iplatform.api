﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapBoolMotor
    {
        public static string Map(bool answer)
        {
            var typeTests = new Dictionary<bool, Func<string>>();
            typeTests[true] = () => "1";
            typeTests[false] = () => "0";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "0" : returnFunc.Value();
        }
    }
}
