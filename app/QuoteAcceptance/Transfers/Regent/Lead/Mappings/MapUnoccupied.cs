﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapUnoccupied
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.UnoccupiedDuringWorkHours.Answer] = () => "2";
            typeTests[QuestionAnswers.UnoccupiedMoreThan7DaysInFirstMonth.Answer] = () => "2";
            typeTests[QuestionAnswers.UnoccupiedMoreThan30ConsecutiveDays.Answer] = () => "2";
            typeTests[QuestionAnswers.UnoccupiedMoreThan60DaysPerYear.Answer] = () => "1";
            typeTests[QuestionAnswers.Unoccupied90DaysInExcessOfLimit.Answer] = () => "1";
            typeTests[QuestionAnswers.Unoccupied120DaysInExcessOfLimit.Answer] = () =>"1";
            typeTests[QuestionAnswers.Unoccupied150DaysInExcessOfLimit.Answer] = () =>"1";
            typeTests[QuestionAnswers.Unoccupied180DaysInExcessOfLimit.Answer] = () =>"1";
            typeTests[QuestionAnswers.Unoccupied210DaysInExcessOfLimit.Answer] = () =>"1";
            typeTests[QuestionAnswers.Unoccupied240DaysInExcessOfLimit.Answer] = () =>"1";
            typeTests[QuestionAnswers.Unoccupied270DaysInExcessOfLimit.Answer] = () =>"1";
            typeTests[QuestionAnswers.Unoccupied300DaysInExcessOfLimit.Answer] = () =>"1";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "2" : returnFunc.Value();
        }
    }
}
