﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapAllRiskCategory
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.AllRiskCategoryAssetsOut.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryBikersRidingApparel.Answer] = () => "5";
            typeTests[QuestionAnswers.AllRiskCategoryBinoculars.Answer] = () => "7";
            typeTests[QuestionAnswers.AllRiskCategoryCalculators.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryCampingEquipment.Answer] = () => "12";
            typeTests[QuestionAnswers.AllRiskCategoryCarSoundEquipment.Answer] = () => "15";
            typeTests[QuestionAnswers.AllRiskCategoryCaravanCampingEquipment.Answer] = () => "12";
            typeTests[QuestionAnswers.AllRiskCategoryCaravanContents.Answer] = () => "12";
            typeTests[QuestionAnswers.AllRiskCategoryCellCarPhone.Answer] = () => "3";
            typeTests[QuestionAnswers.AllRiskCategoryCellPhones.Answer] = () => "3";
            typeTests[QuestionAnswers.AllRiskCategoryClothingPersonalEffectsSpecified.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryClothingPersonalEffectsUnspecified.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryCompactDiscs.Answer] = () => "14";
            typeTests[QuestionAnswers.AllRiskCategoryContactLenses.Answer] = () => "10";
            typeTests[QuestionAnswers.AllRiskCategoryCostumeJewellery.Answer] = () => "1";
            typeTests[QuestionAnswers.AllRiskCategoryCrystal.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryDVDandPlaystationEquipment.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryDecoder.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryDivingEquipment.Answer] = () => "5";
            typeTests[QuestionAnswers.AllRiskCategoryDoctorBags.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryDocuments.Answer] = () => "23";
            typeTests[QuestionAnswers.AllRiskCategoryElectricalGoods.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryExtendedAssetsOut.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryFirearms.Answer] = () => "8";
            typeTests[QuestionAnswers.AllRiskCategoryFishfinder.Answer] = () => "5";
            typeTests[QuestionAnswers.AllRiskCategoryFishingEquipment.Answer] = () => "5";
            typeTests[QuestionAnswers.AllRiskCategoryFurs.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryGPS.Answer] = () => "22";
            typeTests[QuestionAnswers.AllRiskCategoryGeneral.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryGenerators.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryGlasswareChina.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryGolfClubs.Answer] = () => "6";
            typeTests[QuestionAnswers.AllRiskCategoryGoodsInBankSafe.Answer] = () => "2";
            typeTests[QuestionAnswers.AllRiskCategoryHearingAid.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryHearingaidsandprosthesis.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryHouseholdGoods.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryIPod.Answer] = () => "17";
            typeTests[QuestionAnswers.AllRiskCategoryItemsTheftfromVehicles.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryItemskeptintheVault.Answer] = () => "2";
            typeTests[QuestionAnswers.AllRiskCategoryItemsremovedfrombankvault.Answer] = () => "2";
            typeTests[QuestionAnswers.AllRiskCategoryJewellery.Answer] = () => "1";
            typeTests[QuestionAnswers.AllRiskCategoryJewelleryandwatchesspecified.Answer] = () => "";
            typeTests[QuestionAnswers.AllRiskCategoryKeysAndLocks.Answer] = () => "19";
            typeTests[QuestionAnswers.AllRiskCategoryLaptop.Answer] = () => "4";
            typeTests[QuestionAnswers.AllRiskCategoryLeatherGarments.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryLeatherGoods.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryLossOfMoney.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMiscellaneousGoods.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMobilityscootersorshopriders.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMotorAccessories.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMotorcycleHelmets.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMotorisedEquipment.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMotorisedGolfCarts.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMotorisedandnonmotorisedwheelchairs.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMusicInstruments.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMusicalInstruments.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryMusicalInstrumentsProfessional.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryNavigationalEquipment.Answer] = () => "22";
            typeTests[QuestionAnswers.AllRiskCategoryOffRoadMotorBikes.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryOnRoadMotorBikes.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryOpthalmicGlasses.Answer] = () => "10";
            typeTests[QuestionAnswers.AllRiskCategoryOther.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryPaintingsBooksPictures.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryParachutesparaglidersandhanggliders.Answer] = () => "5";
            typeTests[QuestionAnswers.AllRiskCategoryPedalCycles.Answer] = () => "11";
            typeTests[QuestionAnswers.AllRiskCategoryPersianRugs.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryPersonalComputer.Answer] = () => "21";
            typeTests[QuestionAnswers.AllRiskCategoryPhotoEquipment.Answer] = () => "7";
            typeTests[QuestionAnswers.AllRiskCategoryPortableRadioCassetteCdPlayers.Answer] = () => "17";
            typeTests[QuestionAnswers.AllRiskCategoryPortableTv.Answer] = () => "17";
            typeTests[QuestionAnswers.AllRiskCategoryProstheticsMedicalAids.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryQuadBikesNonRoadLicensed.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryQuadBikesRestrictedCover.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryRadioCommunicationEquipment.Answer] = () => "17";
            typeTests[QuestionAnswers.AllRiskCategoryRadios.Answer] = () => "17";
            typeTests[QuestionAnswers.AllRiskCategorySatelliteDishes.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategorySkiEquipment.Answer] = () => "5";
            typeTests[QuestionAnswers.AllRiskCategorySolarPanel.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategorySpecified.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategorySpecifiedContents.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategorySpectaclesSunglasses.Answer] = () => "9";
            typeTests[QuestionAnswers.AllRiskCategorySports.Answer] = () => "5";
            typeTests[QuestionAnswers.AllRiskCategorySportsEquipment.Answer] = () => "5";
            typeTests[QuestionAnswers.AllRiskCategoryStampCoinCollections.Answer] = () => "16";
            typeTests[QuestionAnswers.AllRiskCategorySwimmingPoolEquipment.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryToolsHandTools.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryTransportOfGroceriesAndHouseholdGoods.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryTwoWayRadios.Answer] = () => "17";
            typeTests[QuestionAnswers.AllRiskCategoryVideoCamera.Answer] = () => "17";
            typeTests[QuestionAnswers.AllRiskCategoryVideoMachineVcr.Answer] = () => "17";
            typeTests[QuestionAnswers.AllRiskCategoryWatches.Answer] = () => "1";
            typeTests[QuestionAnswers.AllRiskCategoryWheelchairs.Answer] = () => "20";
            typeTests[QuestionAnswers.AllRiskCategoryZippyNippysKiddiesCycles.Answer] = () => "20";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "20" : returnFunc.Value();
        }
    }
}
