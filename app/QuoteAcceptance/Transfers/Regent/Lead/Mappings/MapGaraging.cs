﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapGaraging 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.VehicleOvernightParkingBehindLockedGates.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleOvernightParkingCarport.Answer] = () => "3";
            typeTests[QuestionAnswers.VehicleOvernightParkingDriveway.Answer] = () => "5";
            typeTests[QuestionAnswers.VehicleOvernightParkingGaragedAtSchool.Answer] = () => "0";
            typeTests[QuestionAnswers.VehicleOvernightParkingInLockedGarage.Answer] = () => "0";
            typeTests[QuestionAnswers.VehicleOvernightParkingNone.Answer] = () => "7";
            typeTests[QuestionAnswers.VehicleOvernightParkingOffStreet.Answer] = () => "5";
            typeTests[QuestionAnswers.VehicleOvernightParkingOffStreetAtSchool.Answer] = () => "5";
            typeTests[QuestionAnswers.VehicleOvernightParkingOnStreet.Answer] = () => "5";
            typeTests[QuestionAnswers.VehicleOvernightParkingOnStreetAtSchool.Answer] = () => "5";
            typeTests[QuestionAnswers.VehicleOvernightParkingParkingLot.Answer] = () => "6";
            typeTests[QuestionAnswers.VehicleOvernightParkingSecurityComplex.Answer] = () => "2";
            typeTests[QuestionAnswers.VehicleOvernightParkingBasement.Answer] = () => "4";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "7" : returnFunc.Value();
        }
    }
}
