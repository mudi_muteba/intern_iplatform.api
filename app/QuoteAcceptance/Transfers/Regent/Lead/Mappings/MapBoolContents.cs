﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapBoolContents
    {
        public static string Map(bool answer)
        {
            var typeTests = new Dictionary<bool, Func<string>>();
            typeTests[true] = () => "1";
            typeTests[false] = () => "2";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "2" : returnFunc.Value();
        }
    }
}
