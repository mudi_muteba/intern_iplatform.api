﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapWallConstruction
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.WallConstructionStandard.Answer] = () => "2";
            typeTests[QuestionAnswers.WallConstructionNonStandard.Answer] = () => "1";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "1" : returnFunc.Value();
        }
    }
}
