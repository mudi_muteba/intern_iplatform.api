﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapClaimsLast24Months 
    {
        public static int Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<int>>();
            typeTests[QuestionAnswers.ClaimsLast24Months0.Answer] = () => 0;
            typeTests[QuestionAnswers.ClaimsLast24Months1.Answer] = () => 1;
            typeTests[QuestionAnswers.ClaimsLast24Months2.Answer] = () => 2;
            typeTests[QuestionAnswers.ClaimsLast24Months3.Answer] = () => 3;
            typeTests[QuestionAnswers.ClaimsLast24Months4.Answer] = () => 4;
            typeTests[QuestionAnswers.ClaimsLast24Months5.Answer] = () => 5;
            typeTests[QuestionAnswers.ClaimsLast24Months6.Answer] = () => 6;
            typeTests[QuestionAnswers.ClaimsLast24Months7.Answer] = () => 7;
            typeTests[QuestionAnswers.ClaimsLast24Months8.Answer] = () => 8;
            typeTests[QuestionAnswers.ClaimsLast24Months9.Answer] = () => 9;
            typeTests[QuestionAnswers.ClaimsLast24Months10.Answer] = () => 10;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? 0 : returnFunc.Value();
        }
    }
}
