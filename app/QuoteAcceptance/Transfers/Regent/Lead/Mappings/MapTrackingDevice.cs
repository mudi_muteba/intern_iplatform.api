﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapTrackingDevice 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.VehicleTrackingDeviceNone.Answer] = () => "0";
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInterac.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInteracActive.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInteracHiAlert.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackCT1.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackQuickCT1.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackQuickPlus.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellSecureEagleFinditV5.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellSecureEagleTraceeV4.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellStopMk5.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDataTrakMK2D.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDataTrakMK4.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDigiCoreCTrack.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceDuoSolutionsDatalogger.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMTrack.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixEscosec.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX1.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX2.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX3.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMobileTrackerFrankie.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceMobileTrackerFrankiePlus.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarCyberSleuth.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU450EarlyWarning.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU470PhoneIn.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU480Sleuth.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVigilSupreme.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddi3.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddi5.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddiTrack.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX120.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX170.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX200.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSkyTrax.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSkyTraxF4.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakClassic.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakElite.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakLifestyle.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellSecureEagleFinditV5.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTracetec.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerAlert.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerLocateAlert.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerLocateRetrieve.Answer] = () => "1";
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerRetrieve.Answer] = () => "1";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "2" : returnFunc.Value();
        }
    }
}
