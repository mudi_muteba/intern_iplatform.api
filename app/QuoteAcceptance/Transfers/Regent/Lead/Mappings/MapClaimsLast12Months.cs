﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapClaimsLast12Months 
    {
        public static int Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<int>>();
            typeTests[QuestionAnswers.ClaimsLast12Months0.Answer] = () => 0;
            typeTests[QuestionAnswers.ClaimsLast12Months1.Answer] = () => 1;
            typeTests[QuestionAnswers.ClaimsLast12Months2.Answer] = () => 2;
            typeTests[QuestionAnswers.ClaimsLast12Months3.Answer] = () => 3;
            typeTests[QuestionAnswers.ClaimsLast12Months4.Answer] = () => 4;
            typeTests[QuestionAnswers.ClaimsLast12Months5.Answer] = () => 5;
            typeTests[QuestionAnswers.ClaimsLast12Months6.Answer] = () => 6;
            typeTests[QuestionAnswers.ClaimsLast12Months7.Answer] = () => 7;
            typeTests[QuestionAnswers.ClaimsLast12Months8.Answer] = () => 8;
            typeTests[QuestionAnswers.ClaimsLast12Months9.Answer] = () => 9;
            typeTests[QuestionAnswers.ClaimsLast12Months10.Answer] = () => 10;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? 0 : returnFunc.Value();
        }
    }
}
