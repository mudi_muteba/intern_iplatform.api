﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapAccidentalDamage
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.AccidentalDamage0.Answer] = () => "0";
            typeTests[QuestionAnswers.AccidentalDamage5000.Answer] = () => "1";
            typeTests[QuestionAnswers.AccidentalDamage10000.Answer] = () => "2";
            typeTests[QuestionAnswers.AccidentalDamage15000.Answer] = () => "3";
            typeTests[QuestionAnswers.AccidentalDamage20000.Answer] = () => "4";
            typeTests[QuestionAnswers.AccidentalDamage25000.Answer] = () => "5";
            typeTests[QuestionAnswers.AccidentalDamage30000.Answer] = () => "6";
            typeTests[QuestionAnswers.AccidentalDamage40000.Answer] = () => "7";
            typeTests[QuestionAnswers.AccidentalDamage50000.Answer] = () => "8";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "0" : returnFunc.Value();
        }
    }
}
