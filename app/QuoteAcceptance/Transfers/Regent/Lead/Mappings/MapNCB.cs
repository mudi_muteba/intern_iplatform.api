﻿using iPlatform.Api.DTOs.Ratings.Request;
using Workflow.QuoteAcceptance.Regent.Lead.Helpers;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapNCB
    {
        public static int Map(string answer, RatingRequestItemDto item)
        {
            //var typeTests = new Dictionary<string, Func<string>>();
            //typeTests[QuestionAnswers.NCB0.Answer] = () => "0";
            //typeTests[QuestionAnswers.NCB1.Answer] = () => "1";
            //typeTests[QuestionAnswers.NCB2.Answer] = () => "2";
            //typeTests[QuestionAnswers.NCB3.Answer] = () => "3";
            //typeTests[QuestionAnswers.NCB4.Answer] = () => "4";
            //typeTests[QuestionAnswers.NCB5.Answer] = () => "5";
            //typeTests[QuestionAnswers.NCB6.Answer] = () => "5";
            //typeTests[QuestionAnswers.NCB7.Answer] = () => "5";
            //typeTests[QuestionAnswers.NCB8.Answer] = () => "5";
            //typeTests[QuestionAnswers.NCB9.Answer] = () => "5";
            //typeTests[QuestionAnswers.NCB10.Answer] = () => "5";
            

            //var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            //return returnFunc.Value == null ? "6" : returnFunc.Value();

            var last12Months = NcbCalculator.GetClaimsLast12Months(item);
            var last24Months = NcbCalculator.GetClaimsLast24Months(item);
            var last36Months = NcbCalculator.GetClaimsLast36Months(item);

            //no claims
            if ((last12Months == 0) & (last24Months == 0) & (last36Months == 0))
            {
                return 3;
            }

            //1 or more claims in all 3 periods
            if ((last12Months > 0) & (last24Months > 0) & (last36Months > 0))
            {
                return 0;
            }

            //2 or more claims in any single period AND 1 or more claims in any 2 periods
            if (((last24Months == 0) & (last36Months == 0)) & ((last12Months == 1) | (last12Months >= 2)))
            {
                return 0;
            }

            if ((last12Months == 0) & (last36Months == 0))
            {
                if ((last24Months == 1) | (last24Months == 2))
                {
                    return 1;
                }
                else if (last24Months >= 2)
                {
                    return 0;
                }
            }

            if ((last12Months == 0) & (last24Months == 0))
            {
                if ((last36Months == 1) | (last36Months == 2))
                {
                    return 2;
                }
                else if (last36Months >= 2)
                {
                    return 1;
                }
            }

            //1 or more in any 2 periods
            if (((last12Months >= 1) & ((last24Months >= 1) | (last36Months >= 1))) |
                ((last24Months == 1) & (last36Months == 1)) |
                ((last24Months > 1) & (last36Months > 1)))
            {
                return 0;
            }

            return 0;
        }
    }
}
