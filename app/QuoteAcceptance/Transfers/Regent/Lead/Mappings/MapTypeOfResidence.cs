﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead.Mappings
{
    internal static class MapTypeOfResidence
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.ResidenceIsHolidayHome.Answer] = () => "1";
            typeTests[QuestionAnswers.ResidenceIsMainResidence.Answer] = () => "2";
            typeTests[QuestionAnswers.ResidenceIsOtherResidence.Answer] = () => "3";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "2" : returnFunc.Value();
        }
    }
}
