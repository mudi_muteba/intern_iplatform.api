﻿using Common.Logging;
using Workflow.QuoteAcceptance.Regent.RegentService;

namespace Workflow.QuoteAcceptance.Regent.Lead
{
    public static  class LeadTransfer
    {
        private static readonly ILog Log = LogManager.GetLogger("LeadTransfer");

        public static bool TransferLead(IRegentServiceClient client, string lead)
        {
            Log.Info("Uploading Quote to Regent Service");
            return client.SubmitLead(ref lead);
        }
    }
}
