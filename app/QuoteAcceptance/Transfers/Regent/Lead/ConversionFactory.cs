﻿using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace Workflow.QuoteAcceptance.Regent.Lead
{
    internal static class ConversionFactory<TReturn> 
    {
        internal static TReturn ConvertAnswer(RatingRequestItemDto item, Question question)
        {
            var typeTests = new Dictionary<Type, Func<TReturn>>();
            typeTests[typeof (int)] = () => (TReturn)(object)GetQuestionAnswerInt(item, question);
            typeTests[typeof(string)] = () => (TReturn)(object)GetQuestionAnswerString(item, question);
            typeTests[typeof(bool)] = () => (TReturn)(object)GetQuestionAnswerBool(item, question);
            typeTests[typeof(DateTime)] = () => (TReturn)(object)GetQuestionAnswerDate(item, question);
            typeTests[typeof (Decimal)] = () =>(TReturn)(object)GetQuestionAnswerDecimal(item, question);

            return typeTests.FirstOrDefault(x => x.Key == typeof(TReturn)).Value(); 
        }

        private static string GetQuestionAnswerString(RatingRequestItemDto item, iMasterData question)
        {
            var ratingRequestItemAnswerDto = item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
            if (ratingRequestItemAnswerDto != null)
            {
                if (ratingRequestItemAnswerDto.QuestionAnswer is string)
                    return ratingRequestItemAnswerDto.QuestionAnswer.ToString();

                var questionAnswer = ratingRequestItemAnswerDto.QuestionAnswer as QuestionAnswer;
                if (questionAnswer != null) return questionAnswer.Answer;
            }
            return "";
        }

        private static Boolean GetQuestionAnswerBool(RatingRequestItemDto item, iMasterData question)
        {
            var ratingRequestItemAnswerDto = item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
            if (ratingRequestItemAnswerDto != null)
            {
                bool returnValue;
                bool.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.ToString(), out returnValue);
                return returnValue;
            }
            return false;
        }

        private static int GetQuestionAnswerInt(RatingRequestItemDto item, iMasterData question)
        {
            var ratingRequestItemAnswerDto = item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
            if (ratingRequestItemAnswerDto != null)
            {
                int returnValue;
                try
                {
                    int.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.CastAsQuestionAnswer().Answer, out returnValue);
                }
                catch (Exception)
                {
                    int.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.ToString(), out returnValue);
                }
                return returnValue;
            }
            return 0;
        }

        private static DateTime GetQuestionAnswerDate(RatingRequestItemDto item, iMasterData question)
        {
            var ratingRequestItemAnswerDto = item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
            if (ratingRequestItemAnswerDto != null)
            {
                DateTime returnValue;
                DateTime.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.ToString(), out returnValue);
                return returnValue;
            }
            return DateTime.UtcNow;
        }

        private static decimal GetQuestionAnswerDecimal(RatingRequestItemDto item, iMasterData question)
        {
            var ratingRequestItemAnswerDto = item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
            if (ratingRequestItemAnswerDto != null)
            {
                decimal returnValue;
                decimal.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.ToString(), out returnValue);
                return returnValue;
            }
            return 0;
        }
    }
}