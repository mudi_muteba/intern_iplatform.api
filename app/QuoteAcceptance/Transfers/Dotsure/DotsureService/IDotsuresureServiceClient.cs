﻿namespace Workflow.QuoteAcceptance.Dotsure.DotsureService
{
    public interface IDotsuresureServiceClient
    {
        ServiceResults transferLead(AuthHeader authHeader, Lead quoteDetails, string extReference); 
    }

    public partial class ServiceSoapClient : IDotsuresureServiceClient
    {

    }
}
