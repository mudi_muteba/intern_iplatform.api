﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.QuoteAcceptance.Leads;
using Common.Logging;
using Domain.Policies.Message;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Infrastructure;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Dotsure.Lead;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.Dotsure.DotsureService
{
    public class DotsureServiceProvider : ITransferLeadToInsurer
    {
        private static readonly ILog Log = LogManager.GetLogger<DotsureServiceProvider>();
        private readonly AuthHeader _authHeader;
        private readonly IDotsuresureServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private static CommunicationMetadata _mail;
        private readonly Guid _channelId;
        public DotsureServiceProvider(IDotsuresureServiceClient client, LeadFactory leadFactory, AuthHeader authHeader, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            _client = client;
            _leadFactory = leadFactory;
            _authHeader = authHeader;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var lead = _leadFactory.CreateLead();
            var quote = message.Quote;

            if (lead == null)
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to Dotsure. No items were found in the Quote", quote.Request.Id.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("No details were found in the Quote", message.SerializeAsXml(), quote.Request.Id,InsurerName.Dotsure.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureNoQuote(InsurerName.Dotsure.Name(), quote.Request.Id, _channelId));
                return new LeadTransferResult(false);
            }
            
            var result = Execute(quote, lead);

            var process = _processStatus.FirstOrDefault(w => w.Key == result.Status);
            if (process.Value != null)
                process.Value(result, quote.Request.Id, ExecutionPlan, _channelId, quote.QuoteAcceptanceEnvironment);

            return new LeadTransferResult(result.Status == ResultsCode.Success);
        }

        private readonly IDictionary<ResultsCode, Action<ServiceResults, Guid, ExecutionPlan, Guid, string>> _processStatus = new Dictionary
            <ResultsCode, Action<ServiceResults, Guid, ExecutionPlan, Guid, string>>()
        {
            {
                ResultsCode.Success,
                (result, ratingId, executionPlan, channelId, environment) => Log.InfoFormat("Quote with Rating Request Id [{0}] successfully uploaded to Dotsure", ratingId)
            },
            {
                ResultsCode.Failure, (result, ratingId, executionPlan, channelId, environment) =>
                {
                    Log.ErrorFormat("Quote [{0}] failed to upload to Dotsure with failure reason {1}", ratingId, result.Message);
                    executionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}", result.Message), result.SerializeAsXml(), ratingId,InsurerName.Dotsure.Name(), channelId, _mail, environment));
                    executionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.Dotsure.Name(),ratingId,string.Format("Failure Reason {0}", result.Message),channelId));
                }
            },
            {
                ResultsCode.Warning, (result, ratingId, executionPlan, channelId, environment) =>
                {
                    Log.WarnFormat("Quote with Rating Request Id [{0}] successfully uploaded to Dotsure with Warning {1}", ratingId, result.Message);
                    executionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.Dotsure.Name(), ratingId, string.Format("Warning Reason {0}", result.Message), channelId));
                }
            }
        };

        private ServiceResults Execute(PublishQuoteUploadMessageDto quote, Lead lead)
        {
            ServiceResults result = null;
            var responseSuccess = false;
            var errorMessage = string.Empty;
            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode,InsurerName.Dotsure.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, ReferenceNumber));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, ReferenceNumber));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.Dotsure.Name(), quote.Request.Id, _channelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.Dotsure.Name(), quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.Dotsure.Name(), quote.Request.Id, response.SerializeAsXml(), _channelId)))
                .While(() => responseSuccess != true)
                .Execute(() =>
                {
                    try
                    {

                        //if (quote.WorkflowEnvironment == "dev")
                        //{
                        //    Log.Warn("quoteacceptance/environment = dev. Quote was not uploaded");
                        //    result = new ServiceResults
                        //    {
                        //        Message = "success",
                        //        Status = ResultsCode.Success,
                        //        ReferenceNo = Guid.NewGuid().ToString()
                        //    };
                        //}
                        //else
                            result = Upload(_client, _authHeader, lead);

                        responseSuccess = result.Status == ResultsCode.Success;
                        errorMessage = responseSuccess ? string.Empty : result.Message;
                        ReferenceNumber = string.IsNullOrEmpty(result.ReferenceNo) ? Guid.NewGuid().ToString() : result.ReferenceNo;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.Dotsure.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.Dotsure.Name(), quote.Request.Id, ex, _channelId));
                    }

                    return ValidResponse(result);
                });

            return ValidResponse(result);
        }

        private static ServiceResults ValidResponse(ServiceResults response)
        {
            return response ?? new ServiceResults() { Status = ResultsCode.Failure};
        }

        public static ServiceResults Upload(IDotsuresureServiceClient client, AuthHeader authHeader, DotsureService.Lead lead)
        {
            return LeadTransfer.TransferLead(client, authHeader, lead);
        }

        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }
    }
}