﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Dotsure.DotsureService;

namespace Workflow.QuoteAcceptance.Dotsure.Lead.Mappings
{
    internal static class MapMaritalStatus 
    {
        public static MaritalCode Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<MaritalCode>>();
            typeTests[QuestionAnswers.MainDriverMaritalStatusCivilUnion.Answer] = () => MaritalCode.Co_Habitating;
            typeTests[QuestionAnswers.MainDriverMaritalStatusDivorced.Answer] = () => MaritalCode.Divorced;
            typeTests[QuestionAnswers.MainDriverMaritalStatusDomesticPartner.Answer] = () => MaritalCode.Co_Habitating;
            typeTests[QuestionAnswers.MainDriverMaritalStatusMarried.Answer] = () => MaritalCode.Married;
            typeTests[QuestionAnswers.MainDriverMaritalStatusOther.Answer] = () => MaritalCode.Unknown;
            typeTests[QuestionAnswers.MainDriverMaritalStatusSeparated.Answer] = () => MaritalCode.Seperated;
            typeTests[QuestionAnswers.MainDriverMaritalStatusSingle.Answer] = () => MaritalCode.Single_NeverMarried;
            typeTests[QuestionAnswers.MainDriverMaritalStatusUnincorporatedAssociation.Answer] = () => MaritalCode.Unknown;
            typeTests[QuestionAnswers.MainDriverMaritalStatusUnknown.Answer] = () => MaritalCode.Unknown;
            typeTests[QuestionAnswers.MainDriverMaritalStatusWidowed.Answer] = () => MaritalCode.Widowed;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(MaritalCode) : returnFunc.Value();
        }
    }
}
