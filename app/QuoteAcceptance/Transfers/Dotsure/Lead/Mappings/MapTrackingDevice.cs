﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Dotsure.DotsureService;

namespace Workflow.QuoteAcceptance.Dotsure.Lead.Mappings
{
    internal static class MapTrackingDevice 
    {
        public static TrackingCode Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<TrackingCode>>();
            typeTests[QuestionAnswers.VehicleTrackingDeviceNone.Answer] = () => TrackingCode.NoTrackingDevice;
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInterac.Answer] = () => TrackingCode.Bandit;
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInteracActive.Answer] = () => TrackingCode.Bandit;
            typeTests[QuestionAnswers.VehicleTrackingDeviceBanditInteracHiAlert.Answer] = () => TrackingCode.Bandit;
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackCT1.Answer] = () => TrackingCode.CarTrack;
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackQuickCT1.Answer] = () => TrackingCode.CarTrack;
            typeTests[QuestionAnswers.VehicleTrackingDeviceCarTrackQuickPlus.Answer] = () => TrackingCode.CarTrack;
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellSecureEagleFinditV5.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellSecureEagleTraceeV4.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceCellStopMk5.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceDataTrakMK2D.Answer] = () => TrackingCode.DataTrack;
            typeTests[QuestionAnswers.VehicleTrackingDeviceDataTrakMK4.Answer] = () => TrackingCode.DataTrack;
            typeTests[QuestionAnswers.VehicleTrackingDeviceDigiCoreCTrack.Answer] = () => TrackingCode.Digicore;
            typeTests[QuestionAnswers.VehicleTrackingDeviceDuoSolutionsDatalogger.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceMTrack.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixEscosec.Answer] = () => TrackingCode.Matrix_ECO;
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX1.Answer] = () => TrackingCode.Matrix_MX1;
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX2.Answer] = () => TrackingCode.Matrix_MX2;
            typeTests[QuestionAnswers.VehicleTrackingDeviceMatrixMX3.Answer] = () => TrackingCode.Matrix_MX3;
            typeTests[QuestionAnswers.VehicleTrackingDeviceMobileTrackerFrankie.Answer] = () => TrackingCode.Frankie;
            typeTests[QuestionAnswers.VehicleTrackingDeviceMobileTrackerFrankiePlus.Answer] = () => TrackingCode.Frankie;
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarCyberSleuth.Answer] = () => TrackingCode.Netstar_CyberSleuthSupreme;
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU450EarlyWarning.Answer] = () => TrackingCode.Netstar_EarlyWarning;
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU470PhoneIn.Answer] = () => TrackingCode.Netstar_VBU480;
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVBU480Sleuth.Answer] = () => TrackingCode.Netstar_VBU480;
            typeTests[QuestionAnswers.VehicleTrackingDeviceNetStarVigilSupreme.Answer] = () => TrackingCode.Netstar_CyberSleuthSupreme;
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddi3.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddi5.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrbtechBuddiTrack.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX120.Answer] = () => TrackingCode.Orchid;
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX170.Answer] = () => TrackingCode.Orchid;
            typeTests[QuestionAnswers.VehicleTrackingDeviceOrchidDX200.Answer] = () => TrackingCode.Orchid;
            typeTests[QuestionAnswers.VehicleTrackingDeviceOther.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceSkyTrax.Answer] = () => TrackingCode.Skytrax;
            typeTests[QuestionAnswers.VehicleTrackingDeviceSkyTraxF4.Answer] = () => TrackingCode.Skytrax;
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakClassic.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakElite.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceSmartTrakLifestyle.Answer] = () => TrackingCode.Other_Device;
            typeTests[QuestionAnswers.VehicleTrackingDeviceTracetec.Answer] = () => TrackingCode.Tracetec;
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerAlert.Answer] = () => TrackingCode.Tracker_Alert;
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerLocateAlert.Answer] = () => TrackingCode.Tracker_Locate;
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerLocateRetrieve.Answer] = () => TrackingCode.Tracker_Retrieve;
            typeTests[QuestionAnswers.VehicleTrackingDeviceTrackerRetrieve.Answer] = () => TrackingCode.Tracker_Retrieve;


            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(TrackingCode) : returnFunc.Value();
        }
    }
}
