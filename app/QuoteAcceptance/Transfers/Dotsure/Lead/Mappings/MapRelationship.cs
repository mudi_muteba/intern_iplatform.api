﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Dotsure.DotsureService;

namespace Workflow.QuoteAcceptance.Dotsure.Lead.Mappings
{
    internal static class MapRelationship 
    {
        public static RelationShipCode Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<RelationShipCode>>();
            typeTests[QuestionAnswers.RelationshipToInsuredChild.Answer] = () => RelationShipCode.Child;
            typeTests[QuestionAnswers.RelationshipToInsuredDomesticWorker.Answer] = () => RelationShipCode.Other;
            typeTests[QuestionAnswers.RelationshipToInsuredInsured.Answer] = () => RelationShipCode.Insured;
            typeTests[QuestionAnswers.RelationshipToInsuredOther.Answer] = () => RelationShipCode.Other;
            typeTests[QuestionAnswers.RelationshipToInsuredResidingFamilyMember.Answer] = () => RelationShipCode.Other;
            typeTests[QuestionAnswers.RelationshipToInsuredSpouse.Answer] = () => RelationShipCode.Spouse;
            typeTests[QuestionAnswers.RegisteredOwnerPolicyHolder.Answer] = () => RelationShipCode.Insured;
            typeTests[QuestionAnswers.RegisteredOwnerSpouse.Answer] = () => RelationShipCode.Spouse;
            typeTests[QuestionAnswers.RegisteredOwnerFinanciallyDependentChild.Answer] = () => RelationShipCode.Child;
            typeTests[QuestionAnswers.RegisteredOwnerOther.Answer] = () => RelationShipCode.Other;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(RelationShipCode) : returnFunc.Value();
        }
    }
}
