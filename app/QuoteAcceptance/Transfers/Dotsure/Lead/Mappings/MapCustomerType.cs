﻿using Workflow.QuoteAcceptance.Dotsure.DotsureService;

namespace Workflow.QuoteAcceptance.Dotsure.Lead.Mappings
{
    internal static class MapCustomerType
    {
        public static CustTypeCode Map(string answer)
        {
            //var typeTests = new Dictionary<string, Func<CustTypeCode>>();
            //typeTests[QuestionAnswers.type.Answer] = () => CustTypeCode.Private_Individual;
            //typeTests[QuestionAnswers.MainDriverGenderFemale.Answer] = () => CustTypeCode.Company;
            //typeTests[QuestionAnswers.MainDriverGenderFemale.Answer] = () => CustTypeCode.Close_Corporation;
            //typeTests[QuestionAnswers.MainDriverGenderFemale.Answer] = () => CustTypeCode.Joint_Account;

            //var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            //return returnFunc.Value == null ? default(CustTypeCode) : returnFunc.Value();
            return default(CustTypeCode);
        }
    }
}
