﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using Workflow.QuoteAcceptance.Dotsure.DotsureService;

namespace Workflow.QuoteAcceptance.Dotsure.Lead.Mappings
{
    internal static class MapGaraging 
    {
        public static ParkingCode Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<ParkingCode>>();
            typeTests[QuestionAnswers.VehicleGaragingBehindLockedGates.Answer] = () => ParkingCode.Yard_Locked_Gates;
            typeTests[QuestionAnswers.VehicleGaragingCarport.Answer] = () => ParkingCode.Other;
            typeTests[QuestionAnswers.VehicleGaragingDriveway.Answer] = () => ParkingCode.Yard_Locked_Gates;
            typeTests[QuestionAnswers.VehicleGaragingGaragedAtSchool.Answer] = () => ParkingCode.Locked_Garage;
            typeTests[QuestionAnswers.VehicleGaragingInLockedGarage.Answer] = () => ParkingCode.Locked_Garage;
            typeTests[QuestionAnswers.VehicleGaragingNone.Answer] = () => ParkingCode.Other;
            typeTests[QuestionAnswers.VehicleGaragingOffStreet.Answer] = () => ParkingCode.Pavement_Street;
            typeTests[QuestionAnswers.VehicleGaragingOffStreetAtSchool.Answer] = () => ParkingCode.Pavement_Street;
            typeTests[QuestionAnswers.VehicleGaragingOnStreet.Answer] = () => ParkingCode.Pavement_Street;
            typeTests[QuestionAnswers.VehicleGaragingOnStreetAtSchool.Answer] = () => ParkingCode.Pavement_Street;
            typeTests[QuestionAnswers.VehicleGaragingParkingLot.Answer] = () => ParkingCode.Other;
            typeTests[QuestionAnswers.VehicleGaragingSecurityComplex.Answer] = () => ParkingCode.Security_Complex;

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? default(ParkingCode) : returnFunc.Value();
        }
    }
}
