﻿using Workflow.QuoteAcceptance.Dotsure.DotsureService;

namespace Workflow.QuoteAcceptance.Dotsure.Lead
{
    public static class Authentication
    {
        public static AuthHeader AuthHeader(string logonId, string password)
        {
            return new AuthHeader
            {
                LogonID = logonId,
                Password = password
            };
        }
    }
}