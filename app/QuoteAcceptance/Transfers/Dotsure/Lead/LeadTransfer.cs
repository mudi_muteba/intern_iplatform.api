﻿using Common.Logging;
using Workflow.QuoteAcceptance.Dotsure.DotsureService;

namespace Workflow.QuoteAcceptance.Dotsure.Lead
{
    public static class LeadTransfer
    {
        private static readonly ILog Log = LogManager.GetLogger("LeadTransfer");

        public static ServiceResults TransferLead(IDotsuresureServiceClient client, AuthHeader authHeader, DotsureService.Lead lead)
        {
            Log.Info("Uploading Quote to Dotsure Service");
            return client.transferLead(authHeader, lead, lead.Comments);
        }
    }
}
