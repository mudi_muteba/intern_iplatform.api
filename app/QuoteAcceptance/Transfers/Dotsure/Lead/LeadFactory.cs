﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using iPlatform.Api.DTOs.Domain;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using Workflow.QuoteAcceptance.Dotsure.DotsureService;

namespace Workflow.QuoteAcceptance.Dotsure.Lead
{
    public class LeadFactory
    {
        private static readonly ILog Log = LogManager.GetLogger<LeadFactory>();
        private readonly PublishQuoteUploadMessageDto _quote;
        private readonly LeadTransferDotsureTaskMetadata _metadata;

        public LeadFactory(QuoteUploadMessage message)
        {
            _quote = message.Quote;
            _metadata = message.GetMetadata<QuoteAcceptedMetadata>().TaskMetadata.Get<LeadTransferDotsureTaskMetadata>();
        }

        public DotsureService.Lead CreateLead()
        {
            Log.Info("Creating Dotsure Lead Request.");

            if ((_quote.Request.Items.Count == 0 || _quote.Request.Items.Count(c => c.Cover.Id.Equals(Covers.Motor.Id)) == 0))
            {
                Log.Info("No Rating Request Items exist for Quote to be uploaded to Dotsure");
                return null;
            }

            var lead = new DotsureService.Lead
            {
                Comments = _quote.Request.Id.ToString(),
                DateOfQuote = DateTime.UtcNow,
                PremiumGiven = true,
                TotalPremium = Convert.ToDouble(_quote.Policy.Items.Sum(x => x.Premium) + _quote.Policy.Items.Sum(x => x.Sasria)),
                CampaignID = 6,
                ExclusiveOption = false,
                FixedPremiumOption = false,
                NewBusiness = true,
                RaterVersion = 0,
                TestInd = false,
                AgentDetail = AgentDetail(),
                InsuredDetail = InsuredDetail(),
                SchemeDetail = SchemeDetail(),
                Vehicles = VehicleDetail().ToArray(),
            };
            Log.InfoFormat("Lead: {0}", SerializerExtensions.Serialize(lead));
            return lead;
        }

        private Agent AgentDetail()
        {
            return new Agent
            {
                AgentCode = _metadata.AgentCode,
                AgentEmail = string.Format("{0};{1}", _metadata.AgentEmail, _quote.AgentDetail.EmailAddress),
                AgentName = _metadata.AgentName,
                AgentTelephone = _metadata.AgentTelephone,
                BrokerCode = _metadata.BrokerCode,
                SubPartnerCode = _metadata.SubPartnerCode,
                SubPartnerName = _metadata.SubPartnerName
            };
        }

        private Insured InsuredDetail()
        {
            if (_quote.InsuredInfo == null)
                _quote.InsuredInfo = new InsuredInfoDto();

            var insuredDetail = new Insured
            {
                AddressLine1 = _quote.Request.Policy.Persons[0].Addresses[0].Address1 ?? "",
                AdvancedDriving = ConversionFactory<bool>.ConvertAnswer(_quote.Request.Items[0], Questions.AdvancedDriver),
                ContactNo = _quote.InsuredInfo.ContactNumber ?? "",
                CurrentInsurance = ConversionFactory<bool>.ConvertAnswer(_quote.Request.Items[0], Questions.PreviouslyInsured),
                CustomerType = MappingFactory<CustTypeCode>.Map(""),
                DateOfBirth = ConversionFactory<DateTime>.ConvertAnswer(_quote.Request.Items[0], Questions.MainDriverDateofBirth), // Need to test
                DateOfLicense = ConversionFactory<DateTime>.ConvertAnswer(_quote.Request.Items[0], Questions.DriversLicenceFirstIssuedDate),
                DrivingFrequencyCde = MappingFactory<DrivingFrequencyCode>.Map(""),
                FirstName = _quote.InsuredInfo.Firstname ?? "",
                GenderCde = MappingFactory<GenderCode>.Map(ConversionFactory<string>.ConvertAnswer(_quote.Request.Items[0], Questions.MainDriverGender)), // Need to test
                IDNumber = _quote.InsuredInfo.IDNumber ?? "",
                IDType = MappingFactory<IDCode>.Map(""),
                Initials = string.IsNullOrEmpty(_quote.InsuredInfo.Firstname) ? "" : _quote.InsuredInfo.Firstname.Substring(0, 1),
                ITCConsentGiven = _quote.InsuredInfo.ITCConsent,
                Language = MappingFactory<LanguageCode>.Map(""),
                LicenseTypeCde =
                    MappingFactory<LicenseCode>.Map(ConversionFactory<string>.ConvertAnswer(_quote.Request.Items[0], Questions.DriversLicenceType)),
                MaritalCde =
                    MappingFactory<MaritalCode>.Map(ConversionFactory<string>.ConvertAnswer(_quote.Request.Items[0],
                                                                                            Questions.MainDriverMaritalStatus)),
                NCB = ConversionFactory<int>.ConvertAnswer(_quote.Request.Items[0], Questions.NoClaimBonus),
                Postcode = _quote.Request.Policy.Persons[0].Addresses[0].PostalCode ?? "",
                Suburb = _quote.Request.Policy.Persons[0].Addresses[0].Suburb ?? "",
                Surname = _quote.InsuredInfo.Surname ?? "",
                InsuranceDeclined = false,
                LicenseEndorsed = ConversionFactory<bool>.ConvertAnswer(_quote.Request.Items[0], Questions.LicenseEndorsed),
                NoOfLosses = ConversionFactory<int>.ConvertAnswer(_quote.Request.Items[0], Questions.ClaimsLast24to36Months)
            };
            return insuredDetail;
        }

        private DotsureService.Product SchemeDetail()
        {
            var schemeDetail = new DotsureService.Product
            {
                Scheme = _quote.Policy.ProductCode == "DOTSURE" ? "DOTSURE DIRECT" : _quote.Policy.ProductCode //ConfigurationHelper.Read<string>("Dotsure/SchemeName", true, null)
            };
            return schemeDetail;
        }

        private List<VehicleDetail> VehicleDetail()
        {
            var vehicles = new List<VehicleDetail>();

            foreach (var vehicle in _quote.Request.Items.Select(item => new VehicleDetail
            {
                AlarmCde = MappingFactory<AlarmCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.AlarmType)),
                CoverCde = MappingFactory<CoverCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.TypeOfCoverMotor)),
                ExcessOption = (int)_quote.Policy.Items[0].Excess.Basic,
                InsuredValueCde = MappingFactory<ValueCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.ValuationMethod)),
                Make = ConversionFactory<string>.ConvertAnswer(item, Questions.VehicleMake),
                MMCode = ConversionFactory<string>.ConvertAnswer(item, Questions.VehicleMMCode),
                Model = ConversionFactory<string>.ConvertAnswer(item, Questions.VehicleModel),
                OpenDriverOption = false,
                Registration = ConversionFactory<string>.ConvertAnswer(item, Questions.VehicleRegistrationNumber),
                RiskAddressLine1 = ConversionFactory<string>.ConvertAnswer(item, Questions.RiskAddress), //quote.Request.Policy.Persons[0].Addresses[0].Address1 ?? "",
                RiskPostalCode = ConversionFactory<string>.ConvertAnswer(item, Questions.PostalCode), //quote.Request.Policy.Persons[0].Addresses[0].PostalCode ?? "",
                RiskSuburb = ConversionFactory<string>.ConvertAnswer(item, Questions.Suburb), //quote.Request.Policy.Persons[0].Addresses[0].Suburb ?? "",
                RiskTown = "", //quote.Request.Policy.Persons[0].Addresses[0].City ?? "",
                TrackingDeviceCde =
                    MappingFactory<TrackingCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.VehicleTrackingDevice)),
                VehicleParking = MappingFactory<ParkingCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.VehicleOvernightParking)),
                VehicleUse = MappingFactory<UseCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.ClassOfUse)),
                VehicleValue = ConversionFactory<int>.ConvertAnswer(item, Questions.SumInsured),
                YearModel = ConversionFactory<int>.ConvertAnswer(item, Questions.YearOfManufacture),
                CarryGoods = false,
                CodeIII = false,
                DataDots = ConversionFactory<bool>.ConvertAnswer(item, Questions.VehicleDataDot),
                EngineModified = ConversionFactory<bool>.ConvertAnswer(item, Questions.ModifiedImported),
                EngineNumber = "",
                ExtrasValue = ConversionFactory<int>.ConvertAnswer(item, Questions.VehicleExtrasValue),
                GearlockFitted = ConversionFactory<bool>.ConvertAnswer(item, Questions.VehicleGearlock),
                IsFinanced = ConversionFactory<string>.ConvertAnswer(item, Questions.FinanceHouse) != "",
                ItemSeqNo = item.AssetNo,
                MultipleRiskAddress = false,
                NewVehicle = (ConversionFactory<int>.ConvertAnswer(item, Questions.YearOfManufacture) == DateTime.UtcNow.Year),
                OwnerRelationCde = MappingFactory<RelationShipCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.RegisteredOwner)),
                PassengerLiability = false,
                RadioValue = ConversionFactory<int>.ConvertAnswer(item, Questions.RadioValue),
                RunOnFlat = false,
                TakenDelivery = true,
                VINNumber = "",
                RegularDriverRelation =
                    MappingFactory<RelationShipCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.RelationshipToInsured)),
                RegularDriver = RegularDriver(item)
            }))
            {
                var vehicleCopy = vehicle;
                var vehicleResult = _quote.Policy.Items.FirstOrDefault(c => c.AssetNo == vehicleCopy.ItemSeqNo);

                if (vehicleResult == null)
                    Log.ErrorFormat("Could add Item Premium Detail for Vehicle with Asset Number (Item Seq No {0})", vehicleCopy.ItemSeqNo);
                //throw new Exception(string.Format("Could add Item Premium Detail for Vehicle with Asset Number (Item Seq No {0})", vehicleCopy.ItemSeqNo));

                vehicle.PremiumDetail = ItemPremium(vehicleResult ?? new RatingResultItemDto());
                if (vehicleResult != null) vehicle.ExcessOption = (int)vehicleResult.Excess.Basic;
                vehicles.Add(vehicle);
            }
            return vehicles;
        }

        private Person RegularDriver(RatingRequestItemDto item)
        {
            return new Person
            {
                AddressLine1 = _quote.Request.Policy.Persons[0].Addresses[0].Address1 ?? "",
                AdvancedDriving = ConversionFactory<bool>.ConvertAnswer(item, Questions.AdvancedDriver),
                ContactNo = _quote.InsuredInfo.ContactNumber ?? "",
                CurrentInsurance = true,
                DateOfBirth = ConversionFactory<DateTime>.ConvertAnswer(item, Questions.MainDriverDateofBirth),
                DateOfLicense = ConversionFactory<DateTime>.ConvertAnswer(item, Questions.DriversLicenceFirstIssuedDate),
                DrivingFrequencyCde = MappingFactory<DrivingFrequencyCode>.Map(""),
                FirstName = _quote.Request.Policy.Persons[0].Initials ?? "",
                GenderCde =
                    MappingFactory<GenderCode>.Map(
                        new IdentityNumberInfo(ConversionFactory<string>.ConvertAnswer(item, Questions.MainDriverIDNumber)).Gender.Name),
                IDNumber = ConversionFactory<string>.ConvertAnswer(item, Questions.MainDriverIDNumber),
                IDType = MappingFactory<IDCode>.Map(""),
                Initials = _quote.Request.Policy.Persons[0].Initials ?? "",
                ITCConsentGiven = _quote.InsuredInfo.ITCConsent,
                LicenseTypeCde =
                    MappingFactory<LicenseCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.DriversLicenceType)),
                MaritalCde =
                    MappingFactory<MaritalCode>.Map(ConversionFactory<string>.ConvertAnswer(item, Questions.MainDriverMaritalStatus)),
                NCB = ConversionFactory<int>.ConvertAnswer(item, Questions.NoClaimBonus),
                Postcode = _quote.Request.Policy.Persons[0].Addresses[0].PostalCode ?? "",
                Suburb = _quote.Request.Policy.Persons[0].Addresses[0].Suburb ?? "",
                Surname = _quote.Request.Policy.Persons[0].Surname ?? "",
                InsuranceDeclined = false,
                LicenseEndorsed = ConversionFactory<bool>.ConvertAnswer(item, Questions.LicenseEndorsed),
                NoOfLosses = ConversionFactory<int>.ConvertAnswer(item, Questions.ClaimsLast24to36Months),
            };
        }

        private static ItemPremium ItemPremium(RatingResultItemDto result)
        {
            var premium = new ItemPremium
            {
                OtherFee = 0,
                RiskPremium = Convert.ToDouble(result.Premium),
                SasriaPremium = Convert.ToDouble(result.Sasria),
                SystemsFee = 0,
                TrackerPremium = 0
            };
            return premium;
        }
    }
}
