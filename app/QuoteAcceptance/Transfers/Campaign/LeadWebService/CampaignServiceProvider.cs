﻿using System;
using Common.Logging;
using Domain.Policies.Message;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Infrastructure;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Campaign.Helpers;
using Workflow.QuoteAcceptance.Campaign.Lead;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.Campaign.LeadWebService
{
    public class CampaignServiceProvider : ITransferLeadToInsurer
    {
        private static readonly ILog Log = LogManager.GetLogger<CampaignServiceProvider>();
        private readonly ICampaignServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private readonly CommunicationMetadata _mail;
        private readonly Guid _channelId;
        
        public CampaignServiceProvider(ICampaignServiceClient client, LeadFactory leadFactory, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            _client = client;
            _leadFactory = leadFactory;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;

        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var quote = message.Quote;
            var lead = _leadFactory.CreateLead().SerializeToXml();

            if (string.IsNullOrEmpty(lead))
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to Campaign Leads. No details were found in the Quote", quote.Request.Id.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("No details were found in the Quote", string.Empty, quote.Request.Id,InsurerName.Campaign.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureNoQuote(InsurerName.Campaign.Name(), quote.Request.Id,_channelId));
                return new LeadTransferResult(false);
            }

            var xmlResult = Execute(quote, lead);

            if (xmlResult.Message == null)
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to Campaing Leads. Null returned from the service", quote.Request.Id.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("Null returned from the service", string.Empty, quote.Request.Id,InsurerName.Campaign.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.Campaign.Name(), quote.Request.Id,"Null returned from the service", _channelId));
                return new LeadTransferResult(false);
            }
            if (xmlResult.Message.Contains("Error"))
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to Campaing Leads with failure reason {1}", quote.Request.Id.ToString(),xmlResult.Message);
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(xmlResult.Message, string.Empty, quote.Request.Id,InsurerName.Campaign.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.Campaign.Name(), quote.Request.Id,xmlResult.Message, _channelId));
                return new LeadTransferResult(false);
            }

            Log.InfoFormat("Quote [{0}] successfully uploaded to Campaing Leads", quote.Request.Id.ToString());

            return new LeadTransferResult(true);
        }

        private LeadImportResult Execute(PublishQuoteUploadMessageDto quote, string lead)
        {
            LeadImportResult result = null;
            var success = false;
            var errorMessage = string.Empty;
            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.Campaign.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, ReferenceNumber));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, ReferenceNumber));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.Campaign.Name(), quote.Request.Id, _channelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.Campaign.Name(), quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.Campaign.Name(), quote.Request.Id, response.ToString(), _channelId)))
                .While(() => success != true)
                .Execute(() =>
                {
                    try
                    {

                        //if (quote.WorkflowEnvironment == "dev")
                        //{
                        //    Log.Warn("quoteacceptance/environment = dev. Quote was not uploaded");
                        //    result = new LeadImportResult
                        //    {
                        //        ImportResult = "1",
                        //        Message = "Success"
                        //    };
                        //}   
                        //else
                            result = Upload(_client, lead);

                        success = result.ImportResult == "1";
                        errorMessage = success ? string.Empty : result.Message;
                        ReferenceNumber = Guid.NewGuid().ToString();
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.Campaign.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.Campaign.Name(), quote.Request.Id, ex, _channelId));
                    }

                    return ValidResponse(result);
                });

            return ValidResponse(result);
        }

        public static LeadImportResult Upload(ICampaignServiceClient client, string lead)
        {
            var response = client.Import(lead).ToString();
            return response.DeserializeFromXml<LeadImportResult>();
        }


        private static LeadImportResult ValidResponse(LeadImportResult response)
        {
            return response ?? new LeadImportResult() {Message = null};
        }
        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }
    }
}