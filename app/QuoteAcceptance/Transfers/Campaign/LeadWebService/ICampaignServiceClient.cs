namespace Workflow.QuoteAcceptance.Campaign.LeadWebService
{
    public interface ICampaignServiceClient
    {
        object Import(string strXML);
    }

    public partial class LeadWebServiceSoapClient : ICampaignServiceClient
    {
    }
}