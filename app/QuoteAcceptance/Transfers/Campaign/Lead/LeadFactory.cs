﻿using System;
using System.Globalization;
using System.Linq;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using iPlatform.Api.DTOs.Domain;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData;

namespace Workflow.QuoteAcceptance.Campaign.Lead
{
    public class LeadFactory
    {
        private static readonly ILog Log = LogManager.GetLogger<LeadFactory>();
        private readonly PublishQuoteUploadMessageDto _quote;
        private readonly LeadTransferCampaignTaskMetadata _metadata;

        public LeadFactory(QuoteUploadMessage message)
        {
            _quote = message.Quote;
            _metadata = message.GetMetadata<QuoteAcceptedMetadata>().TaskMetadata.Get<LeadTransferCampaignTaskMetadata>();
        }

        public Lead CreateLead()
        {
            if (_quote.Request.Items.Count == 0)
            {
                Log.Error("There are no Request Items for Campaign Quote");
                return null;
            }

            var idNumberInfo = new IdentityNumberInfo(DateTime.Today.Date.ToString(CultureInfo.InvariantCulture));
            if (_quote.Request.Policy.Persons.Any() && _quote.Request.Policy.Persons[0].IdNumber != null)
                idNumberInfo = new IdentityNumberInfo(_quote.Request.Policy.Persons[0].IdNumber);

            var deal = new Deal
            {
                SystemMode = "",
                CurrentDate = DateTime.Today,
                SystemReference = _metadata.SystemReference,
                CampaignSystemReference = _metadata.CampaignSystemReference,
                LeadSourceReference = _metadata.LeadSourceReference,
                BranchCode = _metadata.BranchCode,
                BranchName = _metadata.BranchName,
                FinanceCompanyCode = "",
                FinanceCompanyName = "",
                GroupCode = _metadata.GroupCode,
                GroupName = _metadata.GroupName,
                ContactNote = "",
                GeneralNote = ""
            };

            var user = new User
            {
                UserName = _metadata.UserName,
                UserWorkTelephoneCode = "",
                UserWorkTelephoneNumber = "",
                UserEmailAddress = _metadata.UserEmailAddress,
                UserFaxCode = "",
                UserFaxNumber = "",
                EmployeeNumber = "",
                FIFirstName = "",
                FILastName = "",
                FIIDNumber = ""
            };

            var client = new Client
            {
                Title = (_quote.InsuredInfo.Title == null) ? Titles.Unknown.Name : _quote.InsuredInfo.Title.Name,
                Initials = string.IsNullOrEmpty(_quote.InsuredInfo.Firstname) ? "" : _quote.InsuredInfo.Firstname.Substring(0, 1),
                FirstName = _quote.InsuredInfo.Firstname ?? "",
                Lastname = _quote.InsuredInfo.Surname ?? "",
                IDNumber = _quote.InsuredInfo.IDNumber ?? "",
                BirthDate = idNumberInfo.BirthDate.ToString(),
                Gender = (idNumberInfo.Gender == null) ? Genders.Unknown.Name : idNumberInfo.Gender.Name,
                HomeTelephoneCode = "",
                HomeTelephoneNumber = "",
                WorkTelephoneCode = "",
                WorkTelephoneNumber = "",
                FaxCode = "",
                FaxNumber = "",
                MobileNumber = _quote.InsuredInfo.ContactNumber ?? "",
                EmailAddress = _quote.InsuredInfo.EmailAddress ?? "",
                PhysicalAddress1 = _quote.Request.Policy.Persons[0].Addresses[0].Address1 ?? "",
                PhysicalAddress2 = _quote.Request.Policy.Persons[0].Addresses[0].Address2 ?? "",
                Suburb = _quote.Request.Policy.Persons[0].Addresses[0].Suburb ?? "",
                PostCode = _quote.Request.Policy.Persons[0].Addresses[0].PostalCode ?? "",
                PostalAddress1 = "",
                PostalAddress2 = "",
                Suburb1 = "",
                PostCode1 = "",
                clnCorrespondenceLanguage = "English",
                empEmploymentType = "Employed",
                empOccupation = "Unknown",
                clnMaritalStatus = "Unknown"
            };

            var lead = new Lead
            {
                Deal = deal,
                User = user,
                Client = client
            };

            return lead;

        }
    }
}