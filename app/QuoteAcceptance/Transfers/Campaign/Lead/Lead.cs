﻿using System;
using System.Xml.Serialization;

namespace Workflow.QuoteAcceptance.Campaign.Lead
{
    [Serializable]
    [XmlRoot("ApplicationData")]
    public class Lead
    {
        public Deal Deal { get; set; }
        public User User { get; set; }
        public Client Client { get; set; }
    }

    [Serializable]
    public class Deal
    {
        public string SystemMode { get; set; }
        public DateTime CurrentDate { get; set; }
        public string SystemReference { get; set; }
        public string CampaignSystemReference { get; set; }
        public string LeadSourceReference { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string FinanceCompanyCode { get; set; }
        public string FinanceCompanyName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string ContactNote { get; set; }
        public string GeneralNote { get; set; }
    }

    [Serializable]
    public class User
    {
        public string UserName { get; set; }
        public string UserWorkTelephoneCode { get; set; }
        public string UserWorkTelephoneNumber { get; set; }
        public string UserEmailAddress { get; set; }
        public string UserFaxCode { get; set; }
        public string UserFaxNumber { get; set; }
        public string EmployeeNumber { get; set; }
        public string FIFirstName { get; set; }
        public string FILastName { get; set; }
        public string FIIDNumber { get; set; }
    }

    [Serializable]
    public class Client
    {
        public string Title { get; set; }
        public string Initials { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string IDNumber { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string HomeTelephoneCode { get; set; }
        public string HomeTelephoneNumber { get; set; }
        public string WorkTelephoneCode { get; set; }
        public string WorkTelephoneNumber { get; set; }
        public string FaxCode { get; set; }
        public string FaxNumber { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public string PhysicalAddress1 { get; set; }
        public string PhysicalAddress2 { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public string PostalAddress1 { get; set; }
        public string PostalAddress2 { get; set; }
        public string Suburb1 { get; set; }
        public string PostCode1 { get; set; }
        public string clnCorrespondenceLanguage { get; set; }
        public string empEmploymentType { get; set; }
        public string empOccupation { get; set; }
        public string clnMaritalStatus { get; set; }
    }
}