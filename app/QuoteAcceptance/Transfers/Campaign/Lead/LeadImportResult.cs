﻿namespace Workflow.QuoteAcceptance.Campaign.Lead
{
    public class LeadImportResult
    {
        public string ImportResult { get; set; }
        public string Message { get; set; }
    }
}
