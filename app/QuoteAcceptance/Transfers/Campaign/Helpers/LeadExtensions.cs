﻿using System.IO;
using System.Xml.Serialization;

namespace Workflow.QuoteAcceptance.Campaign.Helpers
{
    public static class XmlExtensions
    {
        public static string SerializeToXml<T>(this T obj)
        {
            var xmlSerializer = new XmlSerializer(obj.GetType());
            var stringWriter = new StringWriter();
            xmlSerializer.Serialize(stringWriter, obj);
            return stringWriter.GetStringBuilder().ToString();
        }

        public static T DeserializeFromXml<T>(this string xml)
        {
            T result;
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (TextReader textReader = new StringReader(xml))
            {
                result = (T)xmlSerializer.Deserialize(textReader);
            }
            return result;
        }
    }
}
