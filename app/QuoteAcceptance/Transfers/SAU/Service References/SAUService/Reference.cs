﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Workflow.QuoteAcceptance.SAU.SAUService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SAUService.SAUBlackBoxServiceSoap")]
    public interface SAUBlackBoxServiceSoap {
        
        // CODEGEN: Generating message contract since message GetQuotationRequest has headers
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetQuotation", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationResponse GetQuotation(Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetQuotation", ReplyAction="*")]
        System.Threading.Tasks.Task<Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationResponse> GetQuotationAsync(Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationRequest request);
        
        // CODEGEN: Generating message contract since message SubmitLeadRequest has headers
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SubmitLead", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadResponse SubmitLead(Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SubmitLead", ReplyAction="*")]
        System.Threading.Tasks.Task<Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadResponse> SubmitLeadAsync(Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1064.2")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Authentication : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string userNameField;
        
        private string passwordField;
        
        private System.Xml.XmlAttribute[] anyAttrField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string UserName {
            get {
                return this.userNameField;
            }
            set {
                this.userNameField = value;
                this.RaisePropertyChanged("UserName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string Password {
            get {
                return this.passwordField;
            }
            set {
                this.passwordField = value;
                this.RaisePropertyChanged("Password");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAnyAttributeAttribute()]
        public System.Xml.XmlAttribute[] AnyAttr {
            get {
                return this.anyAttrField;
            }
            set {
                this.anyAttrField = value;
                this.RaisePropertyChanged("AnyAttr");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1064.2")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public enum Product {
        
        /// <remarks/>
        SAU,
        
        /// <remarks/>
        MULTIPLEX,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetQuotation", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetQuotationRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public Workflow.QuoteAcceptance.SAU.SAUService.Authentication Authentication;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string xmlSAUinput;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=1)]
        public Workflow.QuoteAcceptance.SAU.SAUService.Product product;
        
        public GetQuotationRequest() {
        }
        
        public GetQuotationRequest(Workflow.QuoteAcceptance.SAU.SAUService.Authentication Authentication, string xmlSAUinput, Workflow.QuoteAcceptance.SAU.SAUService.Product product) {
            this.Authentication = Authentication;
            this.xmlSAUinput = xmlSAUinput;
            this.product = product;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetQuotationResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetQuotationResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string GetQuotationResult;
        
        public GetQuotationResponse() {
        }
        
        public GetQuotationResponse(string GetQuotationResult) {
            this.GetQuotationResult = GetQuotationResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="SubmitLead", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class SubmitLeadRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public Workflow.QuoteAcceptance.SAU.SAUService.Authentication Authentication;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string xmlSAULead;
        
        public SubmitLeadRequest() {
        }
        
        public SubmitLeadRequest(Workflow.QuoteAcceptance.SAU.SAUService.Authentication Authentication, string xmlSAULead) {
            this.Authentication = Authentication;
            this.xmlSAULead = xmlSAULead;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="SubmitLeadResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class SubmitLeadResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string SubmitLeadResult;
        
        public SubmitLeadResponse() {
        }
        
        public SubmitLeadResponse(string SubmitLeadResult) {
            this.SubmitLeadResult = SubmitLeadResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface SAUBlackBoxServiceSoapChannel : Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SAUBlackBoxServiceSoapClient : System.ServiceModel.ClientBase<Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap>, Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap {
        
        public SAUBlackBoxServiceSoapClient() {
        }
        
        public SAUBlackBoxServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SAUBlackBoxServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SAUBlackBoxServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SAUBlackBoxServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationResponse Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap.GetQuotation(Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationRequest request) {
            return base.Channel.GetQuotation(request);
        }
        
        public string GetQuotation(Workflow.QuoteAcceptance.SAU.SAUService.Authentication Authentication, string xmlSAUinput, Workflow.QuoteAcceptance.SAU.SAUService.Product product) {
            Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationRequest inValue = new Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationRequest();
            inValue.Authentication = Authentication;
            inValue.xmlSAUinput = xmlSAUinput;
            inValue.product = product;
            Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationResponse retVal = ((Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap)(this)).GetQuotation(inValue);
            return retVal.GetQuotationResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationResponse> Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap.GetQuotationAsync(Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationRequest request) {
            return base.Channel.GetQuotationAsync(request);
        }
        
        public System.Threading.Tasks.Task<Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationResponse> GetQuotationAsync(Workflow.QuoteAcceptance.SAU.SAUService.Authentication Authentication, string xmlSAUinput, Workflow.QuoteAcceptance.SAU.SAUService.Product product) {
            Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationRequest inValue = new Workflow.QuoteAcceptance.SAU.SAUService.GetQuotationRequest();
            inValue.Authentication = Authentication;
            inValue.xmlSAUinput = xmlSAUinput;
            inValue.product = product;
            return ((Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap)(this)).GetQuotationAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadResponse Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap.SubmitLead(Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadRequest request) {
            return base.Channel.SubmitLead(request);
        }
        
        public string SubmitLead(Workflow.QuoteAcceptance.SAU.SAUService.Authentication Authentication, string xmlSAULead) {
            Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadRequest inValue = new Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadRequest();
            inValue.Authentication = Authentication;
            inValue.xmlSAULead = xmlSAULead;
            Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadResponse retVal = ((Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap)(this)).SubmitLead(inValue);
            return retVal.SubmitLeadResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadResponse> Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap.SubmitLeadAsync(Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadRequest request) {
            return base.Channel.SubmitLeadAsync(request);
        }
        
        public System.Threading.Tasks.Task<Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadResponse> SubmitLeadAsync(Workflow.QuoteAcceptance.SAU.SAUService.Authentication Authentication, string xmlSAULead) {
            Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadRequest inValue = new Workflow.QuoteAcceptance.SAU.SAUService.SubmitLeadRequest();
            inValue.Authentication = Authentication;
            inValue.xmlSAULead = xmlSAULead;
            return ((Workflow.QuoteAcceptance.SAU.SAUService.SAUBlackBoxServiceSoap)(this)).SubmitLeadAsync(inValue);
        }
    }
}
