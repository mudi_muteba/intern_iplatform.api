﻿using Common.Logging;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Ratings.Quoting;
using System;
using Domain.Policies.Message;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Domain.QuoteAcceptance.Extensions;
using iPlatform.Api.DTOs.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.Messages;
using System.Xml;
using Workflow.QuoteAcceptance.SAU.Lead;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.SAU.SAUService
{
    public class SAUServiceProvider : ITransferLeadToInsurer
    {
        private static readonly ILog log = LogManager.GetLogger<SAUServiceProvider>();
        private static CommunicationMetadata _mail;
        private readonly Guid _channelId;
        private readonly SAUBlackBoxServiceSoap _client;
        private readonly ApiMetadata _api;

        public SAUServiceProvider(SAUBlackBoxServiceSoap client, ApiMetadata api, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            _client = client;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
            _api = api;
        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var quote = message.Quote;

            var result = Execute(quote);
            if (result == null || !result.IsSuccess())
            {
                log.ErrorFormat("Quote [{0}] failed to upload to SAU with failure reason {1}", quote.Request.Id.ToString(), result.GetComment());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}", result.GetComment()), result.GetComment(), quote.Request.Id, InsurerName.SAU.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.SAU.Name(), quote.Request.Id, string.Format("Failure Reason {0}", result.GetComment()), _channelId));
                return new LeadTransferResult(false);
            }

            log.InfoFormat("Quote [{0}] successfully uploaded to SAU", quote.Request.Id.ToString());
            return new LeadTransferResult(result.IsSuccess());
        }

        private SubmitLeadResponse Execute(PublishQuoteUploadMessageDto quote )
        {
            SubmitLeadResponse result = null;
            var responseSuccess = false;
            var errorMessage = string.Empty;
            SubmitLeadRequest request = LeadFactory.CreateLead(quote, _api);

            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.SAU.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, ReferenceNumber));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, ReferenceNumber));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.SAU.Name(), quote.Request.Id, _channelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.SAU.Name(),
                            quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.SAU.Name(),
                            quote.Request.Id, response.ToString(), _channelId)))
                .While(() => !responseSuccess)
                .Execute(() =>
                {
                    try
                    {
                        //if (quote.WorkflowEnvironment == "dev")
                        //{
                        //    log.Warn("quoteacceptance/environment = dev. Quote was not uploaded");
                        //    result = new SubmitLeadResponse
                        //    {
                        //        SubmitLeadResult = "<SubmitResult><Status>Success</Status><Comments>Success!!</Comments></SubmitResult>"
                        //    };
                        //}
                        //else
                            result = Upload(_client, request);

                        responseSuccess = result.IsSuccess();
                        errorMessage = result.GetComment();
                        ReferenceNumber = string.IsNullOrEmpty(quote.Policy.InsurerReference) ? Guid.NewGuid().ToString() : quote.Policy.InsurerReference;
                    }
                    catch (Exception ex)
                    {
                        log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.SAU.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.SAU.Name(), quote.Request.Id, ex, _channelId));
                    }

                    return ValidResponse(result);
                });

            return ValidResponse(result);
        }

        private static SubmitLeadResponse ValidResponse(SubmitLeadResponse response)
        {
            return response ?? new SubmitLeadResponse { SubmitLeadResult = "<SubmitResult><Status>Failed</Status><Comments>Failure</Comments></SubmitResult>" };
        }
        public static SubmitLeadResponse Upload(SAUBlackBoxServiceSoap client, SubmitLeadRequest lead)
        {
            return client.SubmitLead(lead);
        }
        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }

      
    }


    public static class SAUHelperExtensions
    {
        public static bool IsSuccess(this SubmitLeadResponse response)
        {
            if (response == null)
                return false;

            var xmlReturn = new XmlDocument();
            xmlReturn.LoadXml(response.SubmitLeadResult);

            var xmlStatus = xmlReturn.SelectSingleNode("//SubmitResult/Status");
            if (xmlStatus == null)
                return false;

            var temp = xmlStatus.InnerText;

            return !string.IsNullOrWhiteSpace(temp) && temp.Trim() != "Failed";
        }

        public static string GetComment(this SubmitLeadResponse response)
        {
            var errorMessage = string.Empty;
            if (response == null)
                return errorMessage;

            var xmlReturn = new XmlDocument();
            xmlReturn.LoadXml(response.SubmitLeadResult);

            var xmlComment = xmlReturn.SelectSingleNode("//SubmitResult/Comments");

            if (xmlComment != null)
            {
                var temp = xmlComment.InnerText;
                if (!string.IsNullOrWhiteSpace(temp)) errorMessage = temp;
            }

            return errorMessage;
        }
    }
}
