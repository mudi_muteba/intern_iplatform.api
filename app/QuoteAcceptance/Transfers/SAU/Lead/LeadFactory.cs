﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using Workflow.QuoteAcceptance.SAU.SAUService;

namespace Workflow.QuoteAcceptance.SAU.Lead
{
    public static class LeadFactory
    {
        public static SubmitLeadRequest CreateLead(PublishQuoteUploadMessageDto quote, ApiMetadata Api)
        {
            if (quote.Request.Items.Count > 0 && quote.Request.Items.Any())
            {
                var lead = new SubmitLeadRequest
                {
                    Authentication = GetAuthentication(Api),
                    xmlSAULead = GetLead(quote, Api)
                };
                return lead;
            }
            return null;
        }

        private static Authentication GetAuthentication(ApiMetadata Api)
        {
            return new Authentication
            {
                UserName = Api.User,
                Password = Api.Password
            };
        }

        private static string GetLead(PublishQuoteUploadMessageDto quote, ApiMetadata Api)
        {
            var policyHolder = quote.Request.Policy.Persons.FirstOrDefault(x => x.PolicyHolder);
            if (policyHolder == null) return string.Empty;

            var xmlLead = string.Empty;
            xmlLead = "<SAULead>";
            xmlLead += "<CommonData>";
            xmlLead += "<leadId>" + quote.Policy.InsurerReference + "</leadId>";
            xmlLead += "<productType>" + GetProductCode(quote.Policy.ProductCode) + "</productType>";
            xmlLead += "<brokerCode>" + Api.BrokerCode + "</brokerCode>";
            xmlLead += "<fspCode>" + Api.FspCode + "</fspCode>";
            xmlLead += "<notes>" + quote.AgentDetail.Comments + "</notes>";
            xmlLead += "</CommonData>";

            xmlLead += "<InsuredDetails>";
            xmlLead += "<title>" + (policyHolder.Title != null ? policyHolder.Title.Name : Titles.Mr.Name) + "</title>";
            xmlLead += "<firstname>" + policyHolder.FirstName + "</firstname>";
            //xmlLead += "<ageOfInsured>" + CalcYears(policyHolder.DateOfBirth ?? DateTime.MinValue, DateTime.Now) + "</ageOfInsured>";
            xmlLead += "<initials>" + policyHolder.Initials + "</initials>";
            xmlLead += "<surname>" + policyHolder.Surname + "</surname>";
            xmlLead += "<cellNumber>" + policyHolder.PhoneCell + "</cellNumber>";
            xmlLead += "<phWork>" + policyHolder.PhoneWork + "</phWork>";
            xmlLead += "<phHome>" + policyHolder.PhoneHome + "</phHome>";
            xmlLead += "<eMail>" + policyHolder.Email + "</eMail>";
            xmlLead += "<idNumber>" + policyHolder.IdNumber + "</idNumber>";
            xmlLead += "</InsuredDetails>";

            var motorList = quote.Request.Items.Where(x => x.Cover.Equals(Covers.Motor)).Select(x => x).ToList();
            foreach (var motor in motorList)
                xmlLead += GetVehicles(motor, quote);

            xmlLead += "<SAUFee><BrokerFee>0</BrokerFee></SAUFee>";
            xmlLead += "</SAULead>";

            return xmlLead;
        }

        private static string GetProductCode(string productCode)
        {
            switch (productCode)
            {
                case "CENTND":
                    return "CentriqNamDriver";
                case "CENTRD":
                    return "CentriqOpenDriver";
                case "SNTMND":
                    return "SantamNamDriver";
                case "SNTMRD":
                    return "SantamOpenDriver";
            }
            return null;
        }

        private static string GetVehicles(RatingRequestItemDto item, PublishQuoteUploadMessageDto quote)
        {
            var xml = "<MotorVehicle>";
            xml += "<id>" + item.AssetNo + "</id>";


            xml += "<excesstype>flat</excesstype>";
            xml += "<excess>" + GetSelExcess(item) + "</excess>";
            //xml += "<suminsured>0</suminsured>";
            xml += "</MotorVehicle>";

            return xml;
        }

        private static string GetSelExcess(RatingRequestItemDto item)
        {
            var selectedExcessvalue = 0;

            var selexcess = item.Answers.Select(x => x)
                .FirstOrDefault(y => y.Question.Id == Questions.SelectedExcess.Id);

            if (selexcess != null && selexcess.QuestionAnswer != null)
            {
                var volexcessanswer = selexcess.QuestionAnswer.CastAsQuestionAnswer();
        
                 if (volexcessanswer.Id == QuestionAnswers.SelectedExcess0.Id) selectedExcessvalue = 0;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess500.Id) selectedExcessvalue = 2000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess1000.Id) selectedExcessvalue = 2000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess1500.Id) selectedExcessvalue = 2000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess2000.Id) selectedExcessvalue = 2000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess2500.Id) selectedExcessvalue = 3000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess3000.Id) selectedExcessvalue = 3000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess3500.Id) selectedExcessvalue = 4000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess4000.Id) selectedExcessvalue = 4000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess4500.Id) selectedExcessvalue = 5000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess5000.Id) selectedExcessvalue = 5000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess5500.Id) selectedExcessvalue = 6000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess6000.Id) selectedExcessvalue = 6000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess6500.Id) selectedExcessvalue = 7000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess7000.Id) selectedExcessvalue = 7000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess7500.Id) selectedExcessvalue = 8000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess8000.Id) selectedExcessvalue = 8000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess8500.Id) selectedExcessvalue = 9000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess9000.Id) selectedExcessvalue = 9000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess9500.Id) selectedExcessvalue = 10000;
                else if (volexcessanswer.Id == QuestionAnswers.SelectedExcess10000.Id) selectedExcessvalue = 10000;

            }

            return selectedExcessvalue.ToString();
        }

        private static int CalcYears(DateTime start, DateTime end)
        {
            return (end.Year - start.Year - 1) +
                (((end.Month > start.Month) ||
                ((end.Month == start.Month) && (end.Day >= start.Day))) ? 1 : 0);
        }
    }
}
