﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiWay
{
    public class Errors
    {
        public Errors(bool hasError, string error)
        {
            HasError = hasError;
            Error = error;
        }
        public Errors()
        {
            HasError = false;
            HasWarning = false;
        }

        public Errors(string error)
        {
            HasError = true;
            Error = error;
        }

        public bool HasError { get; set; }
        public bool HasWarning { get; set; }
        public string Error { get; set; }
        public string Warning { get; set; }
    }
}
