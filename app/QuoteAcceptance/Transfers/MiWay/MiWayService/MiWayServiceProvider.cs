﻿using Common.Logging;
using Domain.Base.Extentions;
using Domain.Policies.Message;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MiWay;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using iPlatform.Workflow.QuoteAcceptance.MiWay.MiWayService;

namespace Workflow.QuoteAcceptance.MiWay.MiWayService
{
    public class MiWayServiceProvider : Workflow.Infrastructure.AbstractConfigurationReader, ITransferLeadToInsurer
    {
        private static readonly ILog log = LogManager.GetLogger<MiWayServiceProvider>();
        private readonly WowWebWS _client;
        private static CommunicationMetadata _mail;
        private readonly Guid _channelId;
        private LeadTransferToMiWayMetadata _metadata;

        private UserInformationObjectUser userInfo;
        private string agentFullName;
        private string sourceSystemId;
        private string apiKey;

        private decimal customerId;
        private decimal policyNo;

        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }

        public MiWayServiceProvider(WowWebWS client, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            customerId = 0;
            policyNo = 0;
            _client = client;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
        }

        private void SetUserInfo()
        {
            if (_metadata != null)
            {
                agentFullName = _metadata.AgentFullName;
                sourceSystemId = _metadata.SourceSystemId;
                apiKey = _metadata.ApiKey;
                
                userInfo = new UserInformationObjectUser
                {
                    agentId = _metadata.AgentId,
                    userId = _metadata.UserId,
                    productId = _metadata.ProductId,
                    siteSequence = _metadata.SiteSequence
                };
            }
            else
            {
                agentFullName = "BIDVEST";
                sourceSystemId = "CARDINAL";
                
                userInfo = new UserInformationObjectUser
                {
                    agentId = 34011419,
                    userId = "BIDVEST",
                    productId = "RB",
                    siteSequence = 2
                };
            }
        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var quote = message.Quote;


            _metadata = message.GetMetadata<QuoteAcceptedMetadata>().TaskMetadata.Get<LeadTransferToMiWayMetadata>();
            SetUserInfo();

            var result = ExecuteActivateVersionAndSaveEvent(quote);

            if (IsFatalError(result))
            {
                log.ErrorFormat("Quote [{0}] failed to upload to MiWay with failure reason {1}", quote.Request.Id.ToString(), GetErrorMessage(result));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}", GetErrorMessage(result)),result.SerializeAsXml(), quote.Request.Id, InsurerName.MiWay.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.MiWay.Name(), quote.Request.Id, string.Format("Failure Reason {0}",  GetErrorMessage(result)), _channelId));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                return new LeadTransferResult(false);
            }

            log.InfoFormat("Lead [{0}] successfully uploaded to MiWay", quote.Request.Id.ToString());
            return new LeadTransferResult(!IsFatalError(result));
        }

        private BaseOutputTypeUser ExecuteActivateVersionAndSaveEvent(PublishQuoteUploadMessageDto quote)
        {
            BaseOutputTypeUser result = null; 
            var responseSuccess = false;
            var errorMessage = string.Empty;

            if (!String.IsNullOrEmpty(quote.Policy.ExternalRatingResult))
            {
                var allMiWayResponses = quote.Policy.ExternalRatingResult.DeSerializeToObject<MiWayResponseWrapperBase[]>();

                if (allMiWayResponses != null && allMiWayResponses.Length > 0)
                {
                    var createClientResponse = allMiWayResponses.GetResponseByType<CreateClientOutputTypeUser>();
                    var getPolicyResponse = allMiWayResponses.GetResponseByType<GetPolicyOutputTypeUser>();

                    if (createClientResponse != null && createClientResponse.clientId.HasValue)
                        customerId = createClientResponse.clientId.Value;

                    if (getPolicyResponse != null && getPolicyResponse.policy != null && getPolicyResponse.policy.policyNo.HasValue)
                        policyNo = getPolicyResponse.policy.policyNo.Value;
                }
            }

            var requestAcceptVersion = new acceptVersionRequest()
            {
                acceptVersionInput = new AcceptVersionInputTypeUser()
                {
                    effDate = quote.Policy.EffectiveDate,
                    policyNo = policyNo,
                    sourceSystemId = this.sourceSystemId,
                    userInformation = userInfo,
                    action = 4
                }
            };

            var requestSaveEvent = new saveEventStreamRequest()
            {
                saveEventStreamInput = new SaveEventStreamInputTypeUser()
                {
                    eventStreamDetails = new EventStreamUser()
                    {
                        eventGroup = "WRAPSTATUS",
                        eventType = "MNLSTATUS",
                        nameIdNo = customerId,
                        policyNo = policyNo
                    },
                    eventStreamDataDetails = CreateEventStreamData(),
                    sourceSystemId = this.sourceSystemId,
                    userInformation = userInfo,
                }
            };

            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.MiWay.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, policyNo.ToString()));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, policyNo.ToString()));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.MiWay.Name(), quote.Request.Id, _channelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.MiWay.Name(),
                            quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.MiWay.Name(),
                            quote.Request.Id, response.ToString(), _channelId)))
                .While(() => !responseSuccess)
                .Execute(() =>
                {
                    try
                    {
                        //result = Execute(_client.acceptVersion, requestAcceptVersion.acceptVersionInput);

                        using (OperationContextScope scope = new OperationContextScope((IContextChannel)_client))
                        {
                            OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = new HttpRequestMessageProperty
                            {
                                Headers =
                                {
                                    { "Authorization", "Basic " + apiKey }
                                }
                            };

                            result = _client.acceptVersion(requestAcceptVersion).result;

                            if (!IsFatalError(result))
                            {
                                result = _client.saveEventStream(requestSaveEvent).result;
                            }
                            else
                            {
                                errorMessage = GetErrorMessage(result);
                                responseSuccess = false;
                            }

                            responseSuccess = !IsFatalError(result);
                            errorMessage = GetErrorMessage(result);
                            ReferenceNumber = policyNo.ToString();
                        }

                    }
                    catch (Exception ex)
                    {
                        log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.MiWay.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.MiWay.Name(), quote.Request.Id, ex, _channelId));
                    }

                    return result;
                });

            return result;
        }

        private EventStreamDataCollection CreateEventStreamData()
        {
            var dataCollection = new EventStreamDataCollection();

            dataCollection.AddRange(new EventStreamDataUser[]
            {
                new EventStreamDataUser()
                {
                    dataNum = 76,
                    dataName = "RESP_TYPE"
                },
                new EventStreamDataUser()
                {
                    dataDat = DateTime.Now,
                    dataName = "RESCHTIME"
                },
                new EventStreamDataUser()
                {
                    dataChar = "CARDINAL",
                    dataName = "SOURCE"
                },
                new EventStreamDataUser()
                {
                    dataChar = agentFullName,
                    dataName = "AGENT_NAME"
                },
                new EventStreamDataUser()
                {
                    dataChar = "Y",
                    dataName = "SENDIPA"
                },
                //new EventStreamDataUser() //Please keep for future Reference
                //{
                //    dataChar = "This is how to add comments",
                //    dataName = "AGENT_COM"
                //}
            });

            return dataCollection;
        }

        public string GetErrorMessage(BaseOutputTypeUser response)
        {
            var errorMessage = new StringBuilder();
            
            if (response != null && response.errorCode != null &&
                 IsFatalError(response))
            {
                foreach (var error in response.errorCode.errorStack)
                {
                    if (error != null)
                    {
                        if (!String.IsNullOrEmpty(error.messageId))
                        {
                            errorMessage.Append(error.messageId);
                            errorMessage.Append(Environment.NewLine);
                        }

                        if (!String.IsNullOrEmpty(error.messageText))
                        {
                            errorMessage.Append(error.messageText);
                            errorMessage.Append(Environment.NewLine);
                        }
                    }
                    else
                    {
                        errorMessage.Append("Error message response is NULL");
                    }
                }
            }

            return errorMessage.ToString();
        }



        private TObj Execute<T, TObj>(Func<T, TObj> action, T input) where TObj : BaseOutputTypeUser
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)_client))
            {
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = new HttpRequestMessageProperty
                {
                    Headers =
                    {
                        { "Authorization", "Basic " + apiKey }
                    }
                };
                return ThrowExceptionOnError(action(input));
            }
        }

        private static bool IsFatalError(BaseOutputTypeUser errorCodesUser)
        {
            if (errorCodesUser == null || 
                errorCodesUser.errorCode == null || 
                String.IsNullOrEmpty(errorCodesUser.errorCode.errorCode))
            {
                return false;
            }

            var errorInteger = 0;
            Int32.TryParse(errorCodesUser.errorCode.errorCode, out errorInteger);

            return !(errorInteger == 0 ||
                     errorInteger == 3 ||
                    (errorInteger >= 3000 &&
                     errorInteger <= 4000));
        }

        private static T ThrowExceptionOnError<T>(T response) where T : BaseOutputTypeUser
        {
            if (response.errorCode.errorCode != "0")
            {
                var error = string.Format("Error calling MiWay service {0} {1} {2}", response.errorCode.errorCode,
                    response.errorCode.errorStack, response.errorCode.tiaError);

                if (IsFatalError(response))
                    throw new Exception(error);
            }

            return response;
        }
    }
}
