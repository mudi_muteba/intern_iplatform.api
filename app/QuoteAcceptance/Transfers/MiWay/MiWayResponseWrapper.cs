﻿using System.Collections.Generic;
using iPlatform.Workflow.QuoteAcceptance.MiWay.MiWayService;
using System.Linq;
using System.Xml.Serialization;

namespace MiWay
{
    [XmlInclude(typeof(CreateClientOutputTypeUser))]
    [XmlInclude(typeof(GetFieldValuesOutputTypeUser))]
    [XmlInclude(typeof(UpsertObjectOutputTypeUser))]
    [XmlInclude(typeof(GetPolicyOutputTypeUser))]
    [XmlInclude(typeof(CalcLinePriceOutputTypeUser))]
    [XmlInclude(typeof(CreateClientInputTypeUser))]
    [XmlInclude(typeof(UpsertPolicyLineInputTypeUser))]
    [XmlInclude(typeof(CalcLinePriceInputTypeUser))]
    [XmlInclude(typeof(GetQuoteInputTypeUser))]
    public class MiWayResponseWrapperBase
    {
        public MiWayResponseWrapperBase(BaseInputTypeUser request)
        {
            IsResponse = false;
            Request = request;
            Errors = new List<Errors>();
        }

        public MiWayResponseWrapperBase(BaseOutputTypeUser response)
        {
            IsResponse = true;
            Response = response;
            Errors = new List<Errors>();
        }

        public MiWayResponseWrapperBase()
        {
            IsResponse = true;
            Errors = new List<Errors>();
        }

        public MiWayResponseWrapperBase(List<Errors> errors)
        {
            Errors = new List<Errors>();
            Errors.AddRange(errors);
        }

        public MiWayResponseWrapperBase AddError(string error)
        {
            Errors.Add(new Errors(error));
            return this;
        }

        public bool IsResponse { get; set; }

        public BaseInputTypeUser Request { get; set; }

        public BaseOutputTypeUser Response { get; set; }
        public List<Errors> Errors { get; set; }
    }

    public static class MiWayMessageWrapperCollection
    {
        public static T GetResponseByType<T>(this ICollection<MiWayResponseWrapperBase> collection) where T : BaseOutputTypeUser
        {
            T result = default(T);

            if (collection != null)
            {
                var item = collection.FirstOrDefault(x => x.Response != null && x.Response.GetType() == typeof(T));

                if (item != null)
                    result = (T)(item.Response);
            }

            return result;
        }

        public static T GetRequestByType<T>(this ICollection<MiWayResponseWrapperBase> collection) where T : BaseInputTypeUser
        {
            T result = default(T);

            if (collection != null)
            {
                var item = collection.FirstOrDefault(x => x.Response != null && x.Response.GetType() == typeof(T));

                if (item != null)
                    result = (T)(item.Request);
            }

            return result;
        }
    }
}
