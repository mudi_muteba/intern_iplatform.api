﻿using Common.Logging;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Ratings.Quoting;
using System;
using System.Linq;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.Messages;
using Domain.QuoteAcceptance.Extensions;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Workflow.QuoteAcceptance.KingPriceV2.KingPriceServiceV2;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;
using MasterData;
using Domain.Policies.Message;

namespace Workflow.QuoteAcceptance.KingPriceV2.KingPriceService
{
    public class KingPriceServiceProviderV2 : ITransferLeadToInsurer
    {
        private static readonly ILog log = LogManager.GetLogger<KingPriceServiceProviderV2>();
        private readonly PartnerService _client;
        private static CommunicationMetadata _mail;
        private readonly Guid _channelId;
        private readonly SecurityContext _security;
        public KingPriceServiceProviderV2(PartnerService client, SecurityContext security, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            _client = client;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
            _security = security;
        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var quote = message.Quote;
            var result = Execute(quote);

            if (result == null || !result.Result.Success)
            {
                log.ErrorFormat("Quote [{0}] failed to upload to King Price with failure reason {1}", quote.Request.Id.ToString(), ValidResponse(result).Result.Error);
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}", ValidResponse(result).Result.Error), ValidResponse(result).Result.Error.SerializeAsXml(), quote.Request.Id, InsurerName.KingPrice.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.KingPrice.Name(), quote.Request.Id, string.Format("Failure Reason {0}", ValidResponse(result).Message), _channelId));
                return new LeadTransferResult(false);
            }

            log.InfoFormat("Quote [{0}] successfully uploaded to King Price", quote.Request.Id.ToString());
            return new LeadTransferResult(result.Result.Success);
        }

        private ActivateLeadResponseV2 Execute(PublishQuoteUploadMessageDto quote)
        {
            ActivateLeadResponseV2 result = null;
            var responseSuccess = false;
            var errorMessage = string.Empty;
            var email = string.Format("{0};{1}", "monitoring@iplatform.co.za", quote.AgentDetail.EmailAddress);
            var request = new ActivateLeadRequestV2 { BucketKey = int.Parse(quote.Policy.InsurerReference), EmailAddress = email};
            
            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.KingPrice.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, ReferenceNumber));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, ReferenceNumber));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.KingPrice.Name(),quote.Request.Id, _channelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.KingPrice.Name(),
                            quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.KingPrice.Name(),
                            quote.Request.Id, response.ToString(), _channelId)))
                .While(() => !responseSuccess)
                .Execute(() =>
                {
                    try
                    {
                        //if (quote.WorkflowEnvironment == "dev1")
                        //{
                        //    log.Warn("quoteacceptance/environment = dev. Quote was not uploaded");
                        //result = new ActivateLeadResponseV2
                        //{
                        //    Result = new ActivateLeadResultV2
                        //    {
                        //        Success = true,
                        //    },
                        //    Message = "Success",
                        //};
                        //}
                        //else
                        result = _client.ActivateContactV2(_security, request);

                        responseSuccess = result != null && result.Result != null && result.Result.Success;
                        errorMessage = responseSuccess ? string.Empty : result != null ? result.Result.Error.ToString() : string.Empty;
                        ReferenceNumber = string.IsNullOrEmpty(quote.Policy.InsurerReference) ? Guid.NewGuid().ToString() : quote.Policy.InsurerReference;

                    }
                    catch (Exception ex)
                    {
                        log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.KingPrice.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.KingPrice.Name(), quote.Request.Id, ex, _channelId));
                    }

                    return ValidResponse(result);
                });

            return ValidResponse(result);
        }

        private static ActivateLeadResponseV2 ValidResponse(ActivateLeadResponseV2 response)
        {
            return response ?? new ActivateLeadResponseV2 { Result = new ActivateLeadResultV2() };
        }

        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }
    }
}
