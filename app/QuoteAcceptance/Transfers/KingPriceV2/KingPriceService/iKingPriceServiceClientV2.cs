﻿using iPlatform.Workflow.QuoteAcceptance.KingPriceV2.KingPriceServiceV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workflow.QuoteAcceptance.KingPriceV2.KingPriceService
{
    public interface iKingPriceServiceClientV2
    {
        ActivateLeadResponse ActivateContact(SecurityContext securityContext, ActivateLeadRequest request);
    }

    public partial class PartnerServiceClient : iKingPriceServiceClientV2
    {
        public ActivateLeadResponse ActivateContact(SecurityContext securityContext, ActivateLeadRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
