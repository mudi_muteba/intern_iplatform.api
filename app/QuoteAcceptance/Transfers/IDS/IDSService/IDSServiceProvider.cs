﻿using System;
using Common.Logging;
using Domain.Policies.Message;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Infrastructure;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Ids.Lead;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.Ids.IDSService
{
    public class IdsServiceProvider : ITransferLeadToInsurer
    {
        private static readonly ILog Log = LogManager.GetLogger<IdsServiceProvider>();
        private readonly LeadFactory _leadFactory;
        private readonly IIdsServiceClient _client;
        private static CommunicationMetadata _mail;
        private readonly Guid _channelId;
        public IdsServiceProvider(IIdsServiceClient client, LeadFactory leadFactory, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            _client = client;
            _leadFactory = leadFactory;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan; 
        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var quote = message.Quote;
            var lead = _leadFactory.CreateLead();

            if (string.IsNullOrEmpty(lead))
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to IDS. Validation failed.", quote.Request.Id.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("No details were found in the Quote", message.SerializeAsXml(),quote.Request.Id, InsurerName.Ids.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureNoQuote(InsurerName.Ids.Name(), quote.Request.Id, _channelId));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                return new LeadTransferResult(false);
            }

            var result = Execute(quote, lead);
            if (IsSuccessfulResult(result))
            {
                Log.InfoFormat("Quote [{0}] successfully uploaded to IDS", quote.Request.Id.ToString());
                return new LeadTransferResult(true);
            }

            Log.ErrorFormat("Quote [{0}] failed to upload to IDS with failure reason {1}", quote.Request.Id.ToString(), result);
            ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Quote [{0}] failed to upload to IDS with reason {1}", quote.Request.Id, result), lead.SerializeAsXml(), quote.Request.Id,InsurerName.Ids.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
            ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.Ids.Name(), quote.Request.Id,_channelId));
            ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));

            return new LeadTransferResult(false);
        }

        private string Execute(PublishQuoteUploadMessageDto quote, string lead)
        {
            string result = null;
            var responseSuccess = false;
            var errorMessage = string.Empty;
            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.Ids.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, ReferenceNumber));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, ReferenceNumber));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.Ids.Name(), quote.Request.Id, _channelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.Ids.Name(), quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.Ids.Name(), quote.Request.Id, response.SerializeAsXml(), _channelId))
                        )
                .While(() => !responseSuccess)
                .Execute(() =>
                {
                    try
                    {
                        //if (quote.WorkflowEnvironment == "dev")
                        //{
                        //    Log.Warn("quoteacceptance/environment = dev. Quote was not uploaded");
                        //    result = "Success/True";
                        //}
                        //else
                            result = Upload(_client, lead);


                        responseSuccess = IsSuccessfulResult(result);
                        errorMessage = responseSuccess ? "Success" : "Failure";
                        ReferenceNumber = Guid.NewGuid().ToString();                       
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.Ids.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.Ids.Name(), quote.Request.Id, ex, _channelId));
                    }

                    return result;
                });

            return result;
        }

        public static string Upload(IIdsServiceClient client, string lead)
        {
            return LeadTransfer.TransferLead(client, lead);
        }
        public static bool IsSuccessfulResult(string result)
        {
            const string searchString = "Success/True";
            return !string.IsNullOrEmpty(result) && result.IndexOf(searchString, 0, StringComparison.InvariantCultureIgnoreCase) != -1;
        }

        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }
    }
}
