﻿using Workflow.QuoteAcceptance.Ids.IDSService;

namespace Workflow.QuoteAcceptance.Ids.Lead
{
    public static class LeadTransfer
    {
        public static string TransferLead(IIdsServiceClient client, string lead)
        {
            var response = client.Submit(lead);
            return response;
        }
    }
}
