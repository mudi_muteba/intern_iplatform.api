﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using AutoMapper;
using Castle.Core.Internal;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata;
namespace Workflow.QuoteAcceptance.Ids.Lead
{
    public class LeadFactory
    {
        private static readonly ILog Log = LogManager.GetLogger<LeadFactory>();
        private readonly PublishQuoteUploadMessageDto _quote;
        private readonly StringBuilder _lead;
        public LeadFactory(QuoteUploadMessage message)
        {
            _lead = new StringBuilder();
            _quote = message.Quote;

            var quoteAcceptedMetadata =  message.Metadata as QuoteAcceptedMetadata;
            if (quoteAcceptedMetadata == null) return;
            var leadTransferIdsTaskMetadata = quoteAcceptedMetadata.TaskMetadata as LeadTransferIdsTaskMetadata;
            //Used for Cims3 EntityGuid of Brokerage
            if (leadTransferIdsTaskMetadata == null) return;
            _quote.ChannelExternalReference = leadTransferIdsTaskMetadata.ExternalReference;
        }

        public string CreateLead()
        {
            var validation = ValidateQuote();
            if (!string.IsNullOrEmpty(validation)) throw new Exception(string.Format("Validation for IDS failed with message: {0}", validation));

            try
            {
                if (_quote.Request.Items.Count <= 0 ) return _lead.ToString();

                ValidateMonthlyAnnual();
                MapPaymentPlan();
                CreateHeader();
                FilterQuestions();
                CreatePolicy();
                CreateFooter();

            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating IDS Lead Request exception: {0} {1}", ex.Message, _lead.ToString());
                throw;
            }
            return _lead.ToString();
        }

        private void FilterQuestions()
        {
            if (new string[] {"ZURPERS", "DOMESTIC_ARMOUR"}.Contains(_quote.Policy.ProductCode))
            {
                _quote.Request.Items.Where(item => item.Cover.Id == Covers.Motor.Id).ForEach(motor =>
                {
                    if (_quote.Policy.ProductCode == "ZURPERS")
                    {
                        motor.Answers.Remove(
                            motor.Answers.FirstOrDefault(x => x.Question.Id == Questions.VoluntaryExcess.Id));
                    }
                    else if (_quote.Policy.ProductCode == "DOMESTIC_ARMOUR")
                    {
                        motor.Answers.Remove(
                            motor.Answers.FirstOrDefault(x => x.Question.Id == Questions.ZurichVoluntaryExcess.Id));
                    }
                });

                _quote.Request.Items.Where(item => item.Cover.Id == Covers.AllRisk.Id).ForEach(allRisk =>
                {
                    if (_quote.Policy.ProductCode == "ZURPERS")
                    {
                        allRisk.Answers.Remove(
                            allRisk.Answers.FirstOrDefault(x => x.Question.Id == Questions.AigDomesticArmourAllRiskCategories.Id));

                        allRisk.Answers.Remove(
                            allRisk.Answers.FirstOrDefault(x => x.Question.Id == Questions.AllRiskCategory.Id));
                    }
                    else if (_quote.Policy.ProductCode == "DOMESTIC_ARMOUR")
                    {
                        allRisk.Answers.Remove(
                            allRisk.Answers.FirstOrDefault(x => x.Question.Id == Questions.ZurichAllRiskCategories.Id));

                        allRisk.Answers.Remove(
                            allRisk.Answers.FirstOrDefault(x => x.Question.Id == Questions.AllRiskCategory.Id));
                    }
                });
            }
        }

        private void ValidateMonthlyAnnual()
        {
            if (_quote.Policy.ProductCode == "IWYZEMOTORC" ||
                _quote.Policy.ProductCode == "IWYZEMOTORTO" ||
                _quote.Policy.ProductCode == "IWYZEMOTORTPF")
            {
                _quote.Request.Policy.PaymentPlan.Id = PaymentPlans.Annual.Id;
                _quote.Request.Policy.PaymentPlan.Name = PaymentPlans.Annual.Name;

                foreach (var item in _quote.Policy.Items)
                {
                    item.Premium = item.Premium * 12;
                    item.PremiumWithFees = item.PremiumWithFees * 12;
                }
            }
        }

        private void CreateFooter()
        {
            Log.Debug("Creating IDS Lead Header.");
            try
            {
                _lead.AppendLine("</PayLoad>");
                _lead.AppendLine("</STPCommand>");

            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating IDS Lead Footer exception: {0}", ex.Message);
                throw;
            }
        }

        private void CreateHeader()
        {
            Log.Debug("Creating IDS Lead Header.");
            try
            {
                if (!string.IsNullOrEmpty(_lead.ToString()))
                    _lead.Clear();

                _lead.AppendLine("<STPCommand>");
                _lead.AppendLine("<Source>iPlatform</Source>");
                _lead.AppendLine("<Function>Decompose</Function>");
                _lead.AppendLine("<Parameters>");
                _lead.AppendLine("<InterfaceTypeID>iPlatform_Quote</InterfaceTypeID>");
                _lead.AppendLine("<RequestGuid>" + _quote.Request.PlatformId + "</RequestGuid>");
                _lead.AppendLine("</Parameters>");
                _lead.AppendLine("<PayLoad>");
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating IDS Lead Header exception because {0}",ex,ex.Message);
                throw;
            }
        }

        private void CreatePolicy()
        {
            Log.Debug("Creating IDS Lead Policy.");
            try
            {
                var quotedto = Mapper.Map<QuoteDto>(_quote);

                var quoteToUpload = quotedto.SerializeAsXml();
                //var quoteToUpload = _quote.SerializeAsXmlQuestionAnswer();
                _lead.AppendLine(quoteToUpload.Replace(@"<?xml version=""1.0""?>", ""));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Creating IDS Lead Policy exception because {0}", ex, ex.Message);
                throw;
            }
        }

        private string ValidateQuote()
        {
            if (_quote == null)
                return "Quote Dto in null";
            if (_quote.InsuredInfo == null)
                return "No Insured Info in Quote Response Dto";
            if (_quote.Policy == null)
                return "No Policy Info in Quote Response Dto";
            if (_quote.Policy.Items == null)
                return "No Items in Quote Response Dto";
            if (_quote.Request.Policy == null)
                return "No Policy Info in Quote Request Dto";
            return _quote.Request.Items == null ? "No Items in Quote Request Dto" : "";
        }

        private void MapPaymentPlan()
        {

            if (_quote.PaymentDetailsInfo == null) return;
            if (_quote.PaymentDetailsInfo.PaymentPlan == null) return;
            //This will map to what ever is selected in the SecondLevel UnderWriting Screens
            //Update for SmartUk
            if(_quote.PaymentDetailsInfo!= null)
            {
                _quote.Request.Policy.PaymentPlan = _quote.PaymentDetailsInfo.PaymentPlan;
            }
            
        }
    }
}
