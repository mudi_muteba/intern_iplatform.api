﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Extensions;

namespace Workflow.QuoteAcceptance.Telesure.Lead
{
    internal static class ConversionFactory<TReturn>
    {
        internal static TReturn ConvertAnswer(RatingRequestItemDto item, Question question)
        {
            var typeTests = new Dictionary<Type, Func<TReturn>>();
            typeTests[typeof(int)] = () => (TReturn)(object)GetQuestionAnswerInt(item, question);
            typeTests[typeof(string)] = () => (TReturn)(object)GetQuestionAnswerString(item, question);
            typeTests[typeof(bool)] = () => (TReturn)(object)GetQuestionAnswerBool(item, question);
            typeTests[typeof(DateTime)] = () => (TReturn)(object)GetQuestionAnswerDate(item, question);
            typeTests[typeof(Decimal)] = () => (TReturn)(object)GetQuestionAnswerDecimal(item, question);

            return typeTests.FirstOrDefault(x => x.Key == typeof(TReturn)).Value();
        }

        private static string GetQuestionAnswerString(RatingRequestItemDto item, iMasterData question)
        {
            try
            {
                RatingRequestItemAnswerDto ratingRequestItemAnswerDto =
                    item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
                if (ratingRequestItemAnswerDto != null)
                {
                    if (ratingRequestItemAnswerDto.QuestionAnswer is string)
                        return ratingRequestItemAnswerDto.QuestionAnswer.ToString();

                    var questionAnswer = ratingRequestItemAnswerDto.QuestionAnswer as QuestionAnswer;
                    if (questionAnswer != null) return questionAnswer.Answer;
                }
                return "";
            }
            catch (Exception)
            {
                return "";
            }
        }

        private static Boolean GetQuestionAnswerBool(RatingRequestItemDto item, iMasterData question)
        {
            try
            {
                RatingRequestItemAnswerDto ratingRequestItemAnswerDto =
                    item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
                if (ratingRequestItemAnswerDto != null)
                {
                    bool returnValue;
                    if (ratingRequestItemAnswerDto.QuestionAnswer is string)
                        bool.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.ToString(), out returnValue);
                    else
                        bool.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.CastAsQuestionAnswer().Answer,
                            out returnValue);

                    return returnValue;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static int GetQuestionAnswerInt(RatingRequestItemDto item, iMasterData question)
        {
            try
            {
                RatingRequestItemAnswerDto ratingRequestItemAnswerDto =
                    item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
                if (ratingRequestItemAnswerDto != null)
                {
                    int returnValue;
                    if (ratingRequestItemAnswerDto.QuestionAnswer is string)
                        int.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.ToString(), out returnValue);
                    else
                        int.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.CastAsQuestionAnswer().Answer,
                            out returnValue);
                    return returnValue;
                }
                return 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private static DateTime GetQuestionAnswerDate(RatingRequestItemDto item, Question question)
        {
            try
            {
                RatingRequestItemAnswerDto ratingRequestItemAnswerDto =
                    item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
                if (ratingRequestItemAnswerDto != null)
                {
                    DateTime returnValue;
                    if (ratingRequestItemAnswerDto.QuestionAnswer is string)
                        DateTime.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.ToString(), out returnValue);
                    else
                        DateTime.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.CastAsQuestionAnswer().Answer,
                            out returnValue);
                    return returnValue;
                }

                return DateTime.Now;
            }
            catch (Exception)
            {
                return DateTime.Now;
            }
        }

        private static decimal GetQuestionAnswerDecimal(RatingRequestItemDto item, iMasterData question)
        {
            try
            {
                RatingRequestItemAnswerDto ratingRequestItemAnswerDto =
                    item.Answers.FirstOrDefault(x => x.Question.Id == question.Id);
                if (ratingRequestItemAnswerDto != null)
                {
                    decimal returnValue;
                    if (ratingRequestItemAnswerDto.QuestionAnswer is string)
                        decimal.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.ToString(), out returnValue);
                    else
                        decimal.TryParse(ratingRequestItemAnswerDto.QuestionAnswer.CastAsQuestionAnswer().Answer,
                            out returnValue);
                    return returnValue;
                }
                return 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
