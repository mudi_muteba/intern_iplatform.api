﻿using Common.Logging;
using Workflow.QuoteAcceptance.Telesure.TelesureService;

namespace Workflow.QuoteAcceptance.Telesure.Lead
{

    public static class LeadTransfer
    {
        private static readonly ILog Log = LogManager.GetLogger("LeadTransfer");

        public static TransferCompleted TransferLead(ITelesureServiceClient client, AuthenticationHeader authenticationHeader,
            string pSessionId, TransferDetails pTransferObject)
        {
            Log.Info("Uploading Quote to Telesure Service");
            return client.TransferLeadToDataNetWithErrorDetails(authenticationHeader, pSessionId, pTransferObject);
        }
    }
}
