﻿using System;
using System.Collections.Generic;
using System.Linq;
using Workflow.QuoteAcceptance.Telesure.Lead.Mapping;

namespace Workflow.QuoteAcceptance.Telesure.Lead
{
    public static class MappingFactory
    {
        public static string Map(string question, string answer)
        {
            if (string.IsNullOrEmpty(answer)) return "";

            var typeTests = new Dictionary<string, Func<string>>();
            typeTests["Gender"] = () => MapGender.Map(answer);
            typeTests["Title"] = () => MapTitle.Map(answer);
            typeTests["MaritalStatus"] = () => MapMaritalStatus.Map(answer);
            
            return typeTests.FirstOrDefault(x => x.Key == question).Value();
        }
    }
}
