﻿using System;
using iPlatform.Api.DTOs.Ratings.Request;
using Workflow.QuoteAcceptance.Telesure.TelesureService;

namespace Workflow.QuoteAcceptance.Telesure.Lead
{
    [Serializable]
    public class TelesureResult
    {
        public MotorCalculations[] MotorCalculationInputs { get; set; }
        public NonMotorCalculations[] NonMotorCalculationInputs { get; set; }
        public RatingRequestDto PlatformRequest { get; set; }
    }
}
