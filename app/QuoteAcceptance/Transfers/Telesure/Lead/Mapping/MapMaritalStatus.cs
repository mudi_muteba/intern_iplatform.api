﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Telesure.Lead.Mapping
{
    internal static class MapMaritalStatus 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.MainDriverMaritalStatusCivilUnion.Answer] = () => "P";
            typeTests[QuestionAnswers.MainDriverMaritalStatusDivorced.Answer] = () => "D";
            typeTests[QuestionAnswers.MainDriverMaritalStatusDomesticPartner.Answer] = () => "P";
            typeTests[QuestionAnswers.MainDriverMaritalStatusMarried.Answer] = () => "M";
            typeTests[QuestionAnswers.MainDriverMaritalStatusOther.Answer] = () => "S";
            typeTests[QuestionAnswers.MainDriverMaritalStatusSeparated.Answer] = () => "A";
            typeTests[QuestionAnswers.MainDriverMaritalStatusSingle.Answer] = () => "S";
            typeTests[QuestionAnswers.MainDriverMaritalStatusUnincorporatedAssociation.Answer] = () => "P";
            typeTests[QuestionAnswers.MainDriverMaritalStatusUnknown.Answer] = () => "S";
            typeTests[QuestionAnswers.MainDriverMaritalStatusWidowed.Answer] = () => "W";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "S" : returnFunc.Value();
        }
    }
}
