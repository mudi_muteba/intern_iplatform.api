﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Telesure.Lead.Mapping
{
    internal static class MapTitle
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[Titles.Admiral.Code] = () => "ADM";
            typeTests[Titles.Advocate.Code] = () => "ADV";
            typeTests[Titles.Archbishop.Code] = () => "";
            typeTests[Titles.Bishop.Code] = () => "";
            typeTests[Titles.Brigadier.Code] = () => "BRIG";
            typeTests[Titles.Captain.Code] = () => "CAPT";
            typeTests[Titles.Colonel.Code] = () => "COL";
            typeTests[Titles.Commissioner.Code] = () => "COMM";
            typeTests[Titles.Director.Code] = () => "";
            typeTests[Titles.Doctor.Code] = () => "DR";
            typeTests[Titles.EstateLate.Code] = () => "";
            typeTests[Titles.General.Code] = () => "GNRL";
            typeTests[Titles.Honourable.Code] = () => "";
            typeTests[Titles.HonourableJudge.Code] = () => "";
            typeTests[Titles.Inspector.Code] = () => "INSP";
            typeTests[Titles.Judge.Code] = () => "JUDG";
            typeTests[Titles.Justice.Code] = () => "";
            typeTests[Titles.Lady.Code] = () => "LADY";
            typeTests[Titles.Lieutenant.Code] = () => "LT";
            typeTests[Titles.LieutenantColonel.Code] = () => "LTCO";
            typeTests[Titles.LieutenantCommander.Code] = () => "";
            typeTests[Titles.Magistrate.Code] = () => "";
            typeTests[Titles.Major.Code] = () => "MAJO";
            typeTests[Titles.Minister.Code] = () => "";
            typeTests[Titles.Miss.Code] = () => "MISS";
            typeTests[Titles.Mr.Code] = () => "MR";
            typeTests[Titles.MrJnr.Code] = () => "MR";
            typeTests[Titles.MrSnr.Code] = () => "MR";
            typeTests[Titles.Mrs.Code] = () => "MRS";
            typeTests[Titles.Pastor.Code] = () => "PSTR";
            typeTests[Titles.Professor.Code] = () => "PROF";
            typeTests[Titles.Psychologist.Code] = () => "";
            typeTests[Titles.Rabbi.Code] = () => "RABB";
            typeTests[Titles.Reverend.Code] = () => "REV";
            typeTests[Titles.SeniorSuperintendent.Code] = () => "";
            typeTests[Titles.Sergeant.Code] = () => "SGT";
            typeTests[Titles.Sir.Code] = () => "";
            typeTests[Titles.Superintendent.Code] = () => "SUPE";
            typeTests[Titles.TheHonourable.Code] = () => "";
            typeTests[Titles.Trust.Code] = () => "";
            typeTests[Titles.Trustee.Code] = () => "";
            typeTests[Titles.Unknown.Code] = () => "";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            if (returnFunc.Value == null || returnFunc.Value() == "")
                return "MR";

            return returnFunc.Value();
        }
    }
}