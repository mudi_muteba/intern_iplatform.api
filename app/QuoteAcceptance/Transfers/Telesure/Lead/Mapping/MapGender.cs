﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;

namespace Workflow.QuoteAcceptance.Telesure.Lead.Mapping
{
    internal static class MapGender 
    {
        public static string Map(string answer)
        {
            var typeTests = new Dictionary<string, Func<string>>();
            typeTests[QuestionAnswers.MainDriverGenderMale.Answer] = () => "M";
            typeTests[QuestionAnswers.MainDriverGenderFemale.Answer] = () => "F";
            typeTests[QuestionAnswers.MainDriverGenderUnknown.Answer] = () => "M";

            var returnFunc = typeTests.FirstOrDefault(x => x.Key == answer);
            return returnFunc.Value == null ? "M" : returnFunc.Value();
        }
    }
}
