﻿using Workflow.QuoteAcceptance.Telesure.TelesureService;

namespace Workflow.QuoteAcceptance.Telesure.Lead
{
    public class ExternalRatingResultCombined
    {
        public CalculateMultipleNonMotorMultipleCompaniesPremiumsResponse CalculateMultipleNonMotorMultipleCompaniesPremiumsResponse { get; set; }
        public CalculateMultipleMotorMultipleCompaniesPremiumsResponse CalculateMultipleMotorMultipleCompaniesPremiumsResponse { get; set; }
    }
}