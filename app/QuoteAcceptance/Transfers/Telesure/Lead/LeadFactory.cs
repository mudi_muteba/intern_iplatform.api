﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Common.Logging;
using Domain.QuoteAcceptance.Leads;
using iPlatform.Api.DTOs.Domain;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Query;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using RestSharp;
using Workflow.Infrastructure;
using Workflow.QuoteAcceptance.Telesure.TelesureService;

namespace Workflow.QuoteAcceptance.Telesure.Lead
{
    public class LeadFactory
    {
        private static readonly ILog Log = LogManager.GetLogger<LeadFactory>();
        private static List<MotorCalculations> _companyResult;
        private static List<NonMotorCalculations> _companyNmResult;
        private static RatingRequestDto _platformRequest;
        private readonly PublishQuoteUploadMessageDto _quote;
        public LeadFactory(QuoteUploadMessage message)
        {
            _quote = message.Quote;
        }

        public TransferDetails CreateLead(IEnumerable<TelesureInsurerProduct> mapping, string iRateUrl)
        {
            Log.Info("Creating Telesure Lead Request.");

            if (_quote.Request.Items.Count == 0)
            {
                Log.Info("No Rating Request Items exist for Quote to be uploaded to Telesure");
                return null;
            }

            var irateResult = GetTelesureResult();

            //TODO SELECT THE SELECTED COMPANY
            if (irateResult == null || irateResult.MotorCalculationInputs == null || !irateResult.MotorCalculationInputs.Any())
                Log.InfoFormat("No Motor Calculation Inputs were retrieved from iRate. Url {0} Request Id {1}", iRateUrl, _quote.Request.Id);

            _companyResult =
                GetCompanyResult(
                    irateResult != null ? irateResult.MotorCalculationInputs ?? new MotorCalculations[] { } : new MotorCalculations[] { }, mapping);

            _companyNmResult =
                GetNmCompanyResult(
                    irateResult != null ? irateResult.NonMotorCalculationInputs ?? new NonMotorCalculations[] { } : new NonMotorCalculations[] { }, mapping);

            _platformRequest = irateResult != null ? irateResult.PlatformRequest ?? new RatingRequestDto() : new RatingRequestDto();


            if (!_companyResult.Any() & !_companyNmResult.Any())
                throw new Exception("There are no Calculations for Lead to be uploaded to Telesure");

            var email = string.Format("{0};{1}", "monitoring@iplatform.co.za", _quote.AgentDetail.EmailAddress); //TODO: need to get AcceptanceEmailAddress from SettingsQuoteUpload
            var newCompany = new CalculationCompany { ConsultantDetails = new TransferConsultantDetails { Email = _quote.AgentDetail.EmailAddress } };
            var motorCompanyDetails = _companyResult.Any() ? _companyResult[0].CalculationInput.CompanyDetails : newCompany;
            var nonMotorCompanyDetails = _companyNmResult.Any() ? _companyNmResult[0].CalculationInput.CompanyDetails : newCompany;

            motorCompanyDetails.ConsultantDetails.Email = email;
            nonMotorCompanyDetails.ConsultantDetails.Email = email;

            var companyDetails = _companyResult.Any()
                ? motorCompanyDetails
                : nonMotorCompanyDetails;

            var lead = new TransferDetails
            {
                QuoteDate = DateTime.UtcNow,
                PolicyHolderDetails = GetPolicyHolderDetails(irateResult, _companyResult, _companyNmResult),
                AdditionalMembers = GetAdditionalPersonDetails(irateResult, _companyResult, _companyNmResult),
                CompanyDetails = companyDetails,
                MotorDetails = _companyResult.Any() ? _companyResult.ToArray() : new MotorCalculations[] { },
                NonMotorDetails = _companyNmResult.Any() ? _companyNmResult.ToArray() : new NonMotorCalculations[] { }
            };
            Log.InfoFormat("Lead: {0}", SerializerExtensions.Serialize(lead));
            return lead;
        }

        private static List<MotorCalculations> GetCompanyResult(IEnumerable<MotorCalculations> irateMotorResult, IEnumerable<TelesureInsurerProduct> telesureInsurerProducts)
        {
            foreach (var telesureInsurerProduct in telesureInsurerProducts)
            {
                var product = telesureInsurerProduct;
                var motorresults =
                        irateMotorResult
                        .Select(a => a)
                        .Where(x => x.CalculationInput.CompanyDetails.CompanyCode == product.CompanyCode &&
                                x.CalculationInput.CompanyDetails.UnderwritingCompanyCode == product.UwCompanyCode &&
                                x.CalculationInput.CompanyDetails.UnderwritingProductCode == product.UwProductCode &&
                                x.CalculationInput.CompanyDetails.BrandCode == product.SchemeCode)
                        .ToList();
                if (motorresults.Any())
                    return motorresults;
            }
            return new List<MotorCalculations>();

        }

        private static List<NonMotorCalculations> GetNmCompanyResult(IEnumerable<NonMotorCalculations> irateNMotorResult, IEnumerable<TelesureInsurerProduct> telesureInsurerProducts)
        {
            foreach (var telesureInsurerProduct in telesureInsurerProducts)
            {
                var product = telesureInsurerProduct;
                var motorresults =
                        irateNMotorResult
                        .Select(a => a)
                        .Where(x => x.CalculationInput.CompanyDetails.CompanyCode == product.CompanyCode &&
                                x.CalculationInput.CompanyDetails.UnderwritingCompanyCode == product.UwCompanyCode &&
                                (x.CalculationInput.CompanyDetails.UnderwritingProductCode == product.UwProductCode ||
                                    x.CalculationInput.CompanyDetails.UnderwritingProductCode == product.Token ||
                                    x.CalculationInput.CompanyDetails.UnderwritingProductCode == product.AuthCode) &&
                                x.CalculationInput.CompanyDetails.BrandCode == product.SchemeCode)
                        .ToList();
                if (motorresults.Any())
                    return motorresults;
            }
            return new List<NonMotorCalculations>();

        }

        private PersonDetails[] GetAdditionalPersonDetails(TelesureResult irateResult, List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var contactList = _quote.Request.Policy.Persons.ToList();
            contactList.RemoveAt(0);
            return contactList.Select(contact => (GetPersonDetails(contact, irateResult, result, nMResult))).ToArray();
        }

        private PersonDetails GetPersonDetails(RatingRequestPersonDto contact, TelesureResult irateResult,
            List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var insured = new IdentityNumberInfo(contact.IdNumber);

            var assetNo = GetItemReference(_quote, contact);

            MotorCalculations itemToUse = GetItemToUseAdditionalPerson(assetNo, result);

            return new PersonDetails
            {
                Title = contact.Title != null ? contact.Title.Name : Titles.Mr.Name,
                IDNumber = contact.IdNumber ?? "",
                IDType = "",
                Firstname = contact.Initials ?? "",
                Surname = contact.Surname ?? "",
                TelephoneAreaCode = "",
                TelephoneNumber = "",
                WorkTelAreaCode = "",
                WorkTelNumber = "",
                Cellphone = "",
                EmailAddress = "",
                MaritalStatus = contact.MaritalStatus != null ? contact.MaritalStatus.Name : MaritalStatuses.Single.Name,
                Gender = MappingFactory.Map("Gender", insured.Gender.Code),
                DateOfBirth = insured.BirthDate ?? DateTime.MinValue,
                VDNNumber = GetVdnNumber(result, nMResult),
                GreenCampaignID = "",
                ReferenceID = _quote.Request.Id.ToString(),
                Comments = "",
                ResidentialAddressLine1 = contact.Addresses[0].Address1 ?? "",
                ResidentialAddressLine2 = contact.Addresses[0].Address2 ?? "",
                ResidentialAddressLine3 = contact.Addresses[0].Address3 ?? "",
                ResidentialSuburbName = contact.Addresses[0].Suburb ?? "",
                ResidentialSuburbCode = contact.Addresses[0].PostalCode ?? "",
                ResidentialSuburbSeqNo = GetSuburbSequenceNo(result, nMResult, contact.Addresses[0].ExternalAddressID),
                ResidentialAreaType = GetResidentialAreaType(result, nMResult),
                ResidentialAccessControl = GetResidentialAccessControl(result, nMResult),
                PostalAddressLine1 = "",
                PostalAddressLine2 = "",
                PostalAddressLine3 = "",
                PostalSuburbName = "",
                PostalSuburbCode = "",
                PostalSuburbSeqNo = "",
                PostalAreaType = "",
                PostalAccessControl = "",
                ITCScore = GetITCScore(result, nMResult),
                ITCStatus = GetITCStatus(result, nMResult),
                ITCDate = DateTime.UtcNow,
                BrokerCode = GetBrokerCode(result, nMResult),
                BrandCode = GetBrandCode(result, nMResult),
                BrokerContactSeqNo = GetBrokerContactSeq(result, nMResult),
                TotalPremium = GetTotalPremium(result, nMResult),
                FeePremium = GetFeePremium(result, nMResult),
                Relationship = "",
                SequenceNo = itemToUse.CalculationInput.DriverDetails.DriversSequenceNo,
                LicenseDate = itemToUse.CalculationInput.DriverDetails.LicenseDate,
                LicenseType = itemToUse.CalculationInput.DriverDetails.LicenseType ?? "",
                EmploymentStatus = itemToUse.CalculationInput.DriverDetails.EmploymentStatus ?? "",
                MembershipNumber = ""
            };
        }

        private string GetSuburbSequenceNo(List<MotorCalculations> result, List<NonMotorCalculations> nMResult, string ExternalAddressId)
        {

            //check for address used in item in motor first
            var motorReference = GetReferenceForAddressMotor(Int32.Parse(ExternalAddressId));

            foreach (MotorCalculations motor in result)
            {
                if (motor.CalculationInput.ReferenceNo == motorReference)
                {
                    return motor.CalculationInput.DriverDetails.SuburbSeqNo;
                }
            }

            //check for address used in item in rest
            var nonmotorReference = GetReferenceForAddressNonMotor(Int32.Parse(ExternalAddressId));

            foreach (NonMotorCalculations nonmotor in nMResult)
            {
                if (nonmotor.CalculationInput.ReferenceNo == nonmotorReference)
                {
                    return nonmotor.CalculationInput.HomeOwnerDetails.SuburbSeqNo;
                }
            }

            return string.Empty;
        }

        private string GetReferenceForAddressMotor(int externalAddress)
        {
            foreach (var quoteItem in _platformRequest.Items)
            {
                var overnightAddress = quoteItem.Answers.FirstOrDefault(a => a.Question.Name == "Overnight Address");
                if (overnightAddress != null && overnightAddress.QuestionAnswer != null)
                {
                    if (Int32.Parse(overnightAddress.QuestionAnswer.ToString()) == externalAddress) return quoteItem.AssetNo.ToString();
                }
            }
            return null;
        }

        private string GetReferenceForAddressNonMotor(int externalAddress)
        {
            foreach (var quoteItem in _platformRequest.Items)
            {
                var riskAddress = quoteItem.Answers.FirstOrDefault(a => a.Question.Name == "Risk Address");
                if (riskAddress != null && riskAddress.QuestionAnswer != null)
                {
                    if (Int32.Parse(riskAddress.QuestionAnswer.ToString()) == externalAddress) return quoteItem.AssetNo.ToString();
                }
            }
            return null;
        }

        private string GetEmploymentStatus(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var local = result.Any() ? result[0].CalculationInput.DriverDetails.EmploymentStatus ?? "" : "";
            if (local == "") local = nMResult.Any() ? nMResult[0].CalculationInput.HomeOwnerDetails.EmploymentStatus ?? "" : "";

            return local;
        }

        private decimal GetFeePremium(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var fee = result.Any() ? result[0].CalculationOutput.FeePremium : 0;
            fee += nMResult.Any() ? nMResult[0].CalculationOutput.FeePremium : 0;

            return fee;
        }

        private decimal GetTotalPremium(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var premium = result.Any() ? result[0].CalculationOutput.TotalPremium : 0;
            premium += nMResult.Any() ? nMResult[0].CalculationOutput.TotalPremium : 0;

            return premium;
        }

        private string GetBrokerContactSeq(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var local = result.Any() ? result[0].CalculationInput.CompanyDetails.ConsultantDetails.BrokerContactSeqno.ToString(CultureInfo.InvariantCulture) ?? "" : "";
            if (local == "") local = nMResult.Any() ? nMResult[0].CalculationInput.CompanyDetails.ConsultantDetails.BrokerContactSeqno.ToString(CultureInfo.InvariantCulture) ?? "" : "";

            return local;
        }

        private string GetBrandCode(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var local = result.Any() ? result[0].CalculationInput.CompanyDetails.BrandCode ?? "" : "";
            if (local == "") local = nMResult.Any() ? nMResult[0].CalculationInput.CompanyDetails.BrandCode ?? "" : "";

            return local;
        }

        private string GetBrokerCode(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var local = result.Any() ? result[0].CalculationInput.CompanyDetails.BrokerCode ?? "" : "";
            if (local == "") local = nMResult.Any() ? nMResult[0].CalculationInput.CompanyDetails.BrokerCode ?? "" : "";

            return local;
        }

        private string GetITCStatus(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var local = result.Any() ? result[0].CalculationInput.ITCStatus ?? "" : "";
            if (local == "") local = nMResult.Any() ? nMResult[0].CalculationInput.ITCStatus ?? "" : "";

            return local;
        }

        private int GetITCScore(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var itcScore = result.Any() ? result[0].CalculationInput.ITCScore : nMResult.Any() ? nMResult[0].CalculationInput.ITCScore : 9545;
            return itcScore;
        }

        private string GetResidentialAccessControl(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var residentialAccessControl = result.Any() ? result[0].CalculationInput.DriverDetails.AccessControl ?? "" : "";
            if (residentialAccessControl == "") residentialAccessControl = nMResult.Any() ? nMResult[0].CalculationInput.HomeOwnerDetails.AccessControl ?? "" : "";

            return residentialAccessControl;
        }

        private string GetResidentialAreaType(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var residentialAreaType = result.Any() ? result[0].CalculationInput.DriverDetails.AreaType ?? "" : "";
            if (residentialAreaType == "") residentialAreaType = nMResult.Any() ? nMResult[0].CalculationInput.HomeOwnerDetails.AreaType ?? "" : "";

            return residentialAreaType;
        }

        private string GetVdnNumber(List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var vdnNumber = result.Any() ? result[0].CalculationInput.CompanyDetails.VDN ?? "" : "";
            if (vdnNumber == "") vdnNumber = nMResult.Any() ? nMResult[0].CalculationInput.CompanyDetails.VDN ?? "" : "";

            return vdnNumber;
        }

        private static MotorCalculations GetItemToUseAdditionalPerson(string assetNo, List<MotorCalculations> result)
        {
            var localItem = result.Select(a => a).Where(x => x.CalculationInput.ReferenceNo == assetNo).ToList();

            return localItem.Any() ? localItem[0] : new MotorCalculations();
        }

        private static string GetItemReference(PublishQuoteUploadMessageDto quote, RatingRequestPersonDto contact)
        {
            List<RatingRequestItemDto> items =
                quote.Request.Items.Select(a => a).Where(x => x.Cover.Code == Covers.Motor.Code).ToList();

            foreach (var item in items)
            {
                var idNumber = ConversionFactory<string>.ConvertAnswer(item, Questions.MainDriverIDNumber);

                if (contact.IdNumber == idNumber)
                {
                    return item.AssetNo.ToString();
                }
            }
            return string.Empty;
        }

        private PersonDetails GetPolicyHolderDetails(TelesureResult irateResult, List<MotorCalculations> result, List<NonMotorCalculations> nMResult)
        {
            var person = new PersonDetails();
            var insured = new IdentityNumberInfo(_quote.InsuredInfo.IDNumber);

            person.Title = MappingFactory.Map("Title", _quote.InsuredInfo.Title != null ? _quote.InsuredInfo.Title.Code : Titles.Mr.Code);
            person.IDNumber = _quote.InsuredInfo.IDNumber ?? "";
            person.IDType = "1";
            person.Firstname = _quote.InsuredInfo.Firstname ?? "";
            person.Surname = _quote.InsuredInfo.Surname ?? "";
            person.TelephoneAreaCode = "";
            person.TelephoneNumber = "";
            person.WorkTelAreaCode = "";
            person.WorkTelNumber = "";
            person.Cellphone = _quote.InsuredInfo.ContactNumber ?? "";
            person.EmailAddress = _quote.InsuredInfo.EmailAddress ?? "";

            if (!string.IsNullOrEmpty(_quote.InsuredInfo.IDNumber))
            {
                person.MaritalStatus = GetMaritialStatus(_quote);
            }
            else
            {
                person.MaritalStatus = MappingFactory.Map("MaritalStatus", (_quote.InsuredInfo.MaritalStatus ?? MaritalStatuses.Unknown).Code);
            }
            person.DateOfBirth = _quote.InsuredInfo.DateOfBirth.Value;
            person.Gender = MappingFactory.Map("Gender", insured.Gender.Code);
            if (_quote.InsuredInfo.IDNumber != null)
                person.DateOfBirth = DateTime.ParseExact(_quote.InsuredInfo.IDNumber.Substring(0, 6), "yyMMdd", null);
            person.VDNNumber = GetVdnNumber(result, nMResult);
            person.GreenCampaignID = "";
            person.ReferenceID = _quote.Request.Id.ToString();
            person.Comments = _quote.AgentDetail.Comments;
            person.ResidentialAddressLine1 = _quote.Request.Policy.Persons[0].Addresses[0].Address1 ?? "";
            person.ResidentialAddressLine2 = _quote.Request.Policy.Persons[0].Addresses[0].Address2 ?? "";
            person.ResidentialAddressLine3 = _quote.Request.Policy.Persons[0].Addresses[0].Address3 ?? "";
            person.ResidentialSuburbName = _quote.Request.Policy.Persons[0].Addresses[0].Suburb ?? "";
            person.ResidentialSuburbCode = _quote.Request.Policy.Persons[0].Addresses[0].PostalCode ?? "";
            person.ResidentialSuburbSeqNo = GetSuburbSequenceNo(result, nMResult, _quote.Request.Policy.Persons[0].Addresses[0].ExternalAddressID);
            person.ResidentialAreaType = GetResidentialAreaType(result, nMResult);
            person.ResidentialAccessControl = GetResidentialAccessControl(result, nMResult);
            person.PostalAddressLine1 = "";
            person.PostalAddressLine2 = "";
            person.PostalAddressLine3 = "";
            person.PostalSuburbName = "";
            person.PostalSuburbCode = "";
            person.PostalSuburbSeqNo = "";
            person.PostalAreaType = "";
            person.PostalAccessControl = "";
            person.ITCScore = GetITCScore(result, nMResult);
            person.ITCStatus = GetITCStatus(result, nMResult);
            person.ITCDate = DateTime.UtcNow;
            person.ITCVersion = "D0";
            person.BrokerCode = GetBrokerCode(result, nMResult);
            person.BrandCode = GetBrandCode(result, nMResult);
            person.BrokerContactSeqNo = GetBrokerContactSeq(result, nMResult);
            person.TotalPremium = GetTotalPremium(result, nMResult);
            person.FeePremium = GetFeePremium(result, nMResult);
            person.Relationship = "";
            person.SequenceNo = 1;
            person.LicenseDate = result.Any() ? result[0].CalculationInput.DriverDetails.LicenseDate : DateTime.MinValue;
            person.LicenseType = result.Any() ? result[0].CalculationInput.DriverDetails.LicenseType ?? "" : "";
            person.EmploymentStatus = GetEmploymentStatus(result, nMResult);
            person.MembershipNumber = "";
            return person;
        }

        private string GetMaritialStatus(PublishQuoteUploadMessageDto _quote)
        {
            var maritalStatus = "";
            bool matchId = false;
            foreach (var proposalItems in _quote.Request.Items)
            {
                foreach (var questions in proposalItems.Answers.Where(questions => questions.Question.Id == 104))
                {
                    if (_quote.InsuredInfo.IDNumber == questions.QuestionAnswer.ToString())
                    {
                        matchId = true;
                        foreach (var marital in proposalItems.Answers.Where(marital => marital.Question.Id == 72))
                        {
                            maritalStatus = MappingFactory.Map("MaritalStatus", marital.QuestionAnswer.GetAnswer() ?? MaritalStatuses.Unknown.ToString());
                        }
                    }
                }
            }
            if (matchId == false)
            {
                maritalStatus = MappingFactory.Map("MaritalStatus", (_quote.InsuredInfo.MaritalStatus ?? MaritalStatuses.Unknown).Code);
            }
            return maritalStatus;
        }


        private TelesureResult GetTelesureResult()
        {
            bool useTelesureCombineResultToggle = false;
            bool.TryParse(ConfigurationManager.AppSettings["UseTelesureCombineResultToggle"], out useTelesureCombineResultToggle);

            var telesureResult = new TelesureResult();
            if (string.IsNullOrEmpty(_quote.Policy.ExternalRatingResult))
                return telesureResult;

            if (!useTelesureCombineResultToggle)
            {
                if (_quote.Policy.ExternalRatingResult.Contains("CalculateMultipleNonMotorMultipleCompaniesPremiumsResponse"))
                    SetNonMotorCalculationInputs(telesureResult);
                else
                    SetMotorCalculationInputs(telesureResult);
            }
            else
                SetCalculationInputs(telesureResult);

            telesureResult.PlatformRequest = _quote.Request;

            return telesureResult;
        }

        private void SetMotorCalculationInputs(TelesureResult telesureResult)
        {
            var serializer = new XmlSerializer(typeof(CalculateMultipleMotorMultipleCompaniesPremiumsResponse));
            var xml = serializer.Deserialize(new StringReader(_quote.Policy.ExternalRatingResult)) as CalculateMultipleMotorMultipleCompaniesPremiumsResponse;

            IEnumerable<MotorCalculations> motorresults = new List<MotorCalculations>();
            if (xml.CalculateMultipleMotorMultipleCompaniesPremiumsResult != null)
                motorresults = xml.CalculateMultipleMotorMultipleCompaniesPremiumsResult.SelectMany(a => a).ToList();

            telesureResult.MotorCalculationInputs = motorresults.ToArray();
        }

        private void SetNonMotorCalculationInputs(TelesureResult telesureResult)
        {
            var serializer = new XmlSerializer(typeof(CalculateMultipleNonMotorMultipleCompaniesPremiumsResponse));
            var xml = serializer.Deserialize(new StringReader(_quote.Policy.ExternalRatingResult)) as CalculateMultipleNonMotorMultipleCompaniesPremiumsResponse;

            IEnumerable<NonMotorCalculations> motorresults = new List<NonMotorCalculations>();
            if (xml.CalculateMultipleNonMotorMultipleCompaniesPremiumsResult != null)
                motorresults = xml.CalculateMultipleNonMotorMultipleCompaniesPremiumsResult.SelectMany(a => a).ToList();

            telesureResult.NonMotorCalculationInputs = motorresults.ToArray();
        }


        private void SetCalculationInputs(TelesureResult telesureResult)
        {
            var serializer = new XmlSerializer(typeof(ExternalRatingResultCombined));
            var xml = serializer.Deserialize(new StringReader(_quote.Policy.ExternalRatingResult)) as ExternalRatingResultCombined;

            IEnumerable<NonMotorCalculations> nonMotorresults = new List<NonMotorCalculations>();
            if (xml.CalculateMultipleNonMotorMultipleCompaniesPremiumsResponse.CalculateMultipleNonMotorMultipleCompaniesPremiumsResult != null)
                nonMotorresults = xml.CalculateMultipleNonMotorMultipleCompaniesPremiumsResponse.CalculateMultipleNonMotorMultipleCompaniesPremiumsResult.SelectMany(a => a).ToList();

            telesureResult.NonMotorCalculationInputs = nonMotorresults.ToArray();


            IEnumerable<MotorCalculations> motorresults = new List<MotorCalculations>();
            if (xml.CalculateMultipleMotorMultipleCompaniesPremiumsResponse.CalculateMultipleMotorMultipleCompaniesPremiumsResult != null)
                motorresults = xml.CalculateMultipleMotorMultipleCompaniesPremiumsResponse.CalculateMultipleMotorMultipleCompaniesPremiumsResult.SelectMany(a => a).ToList();

            telesureResult.MotorCalculationInputs = motorresults.ToArray();
        }
    }
}
