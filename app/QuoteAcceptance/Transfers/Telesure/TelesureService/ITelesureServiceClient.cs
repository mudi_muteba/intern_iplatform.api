﻿namespace Workflow.QuoteAcceptance.Telesure.TelesureService
{

    public partial class APIServiceSoapClient : ITelesureServiceClient
    {   
    }

    public interface ITelesureServiceClient
    {
        TransferCompleted TransferLeadToDataNetWithErrorDetails(AuthenticationHeader authenticationHeader,
            string pSessionId, TransferDetails pTransferObject);
    }
}
