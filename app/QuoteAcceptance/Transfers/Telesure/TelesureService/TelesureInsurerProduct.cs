﻿using System;
using System.Runtime.Serialization;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using System.Collections.Generic;

namespace Workflow.QuoteAcceptance.Telesure.TelesureService
{
    [DataContract]
    public class TelesureInsurerProduct
    {
        [DataMember]
        public string InsurerCode { get; private set; }
        [DataMember]
        public string ProductCode { get; private set; }
        [DataMember]
        public string Password { get; private set; }
        [DataMember]
        public string UserName { get; private set; }
        [DataMember]
        public string AuthCode { get; private set; }
        [DataMember]
        public string BrokerCode { get; private set; }
        [DataMember]
        public string AgentCode { get; private set; }
        [DataMember]
        public string SchemeCode { get; private set; }
        [DataMember]
        public string Token { get; private set; }
        [DataMember]
        public string SubscriberCode { get; private set; }
        [DataMember]
        public string CompanyCode { get; set; }
        [DataMember]
        public string UwCompanyCode { get; set; }
        [DataMember]
        public string UwProductCode { get; set; }

        public TelesureInsurerProduct(string insurerCode, string productCode, string password, string userName, string authCode,
            string brokerCode, string agentCode, string schemeCode, string token, string subscriberCode,
            string companyCode, string uwCompanyCode, string uwProductCode)
        {
            InsurerCode = insurerCode;
            ProductCode = productCode;
            Password = password;
            UserName = userName;
            AuthCode = authCode;
            BrokerCode = brokerCode;
            AgentCode = agentCode;
            SchemeCode = schemeCode;
            Token = token;
            SubscriberCode = subscriberCode;
            CompanyCode = companyCode;
            UwCompanyCode = uwCompanyCode;
            UwProductCode = uwProductCode;
        }



        public bool IsMatch(string insurerCode, string productCode)
        {
            return InsurerCode.Equals(insurerCode, StringComparison.InvariantCultureIgnoreCase) &&
                   ProductCode.Equals(productCode, StringComparison.InvariantCultureIgnoreCase);
        }

        public static List<TelesureInsurerProduct> CreateRatingSetting(LeadTransferTelesureTaskMetadata metadata, ApiMetadata webServiceMetadata)
        {
            var list = new List<TelesureInsurerProduct>();

            foreach(var info in metadata.TeleSureInfos)
            {
                list.Add(new TelesureInsurerProduct(info.InsurerCode, info.ProductCode, webServiceMetadata.Password, webServiceMetadata.User,
                info.AuthCode,
                info.BrokerCode, info.AgentCode, info.SchemeCode, info.Token, info.SubscriberCode, info.CompanyCode,
                info.UwCompanyCode, info.UwProductCode));
            }

            return list;
        }
    }
}