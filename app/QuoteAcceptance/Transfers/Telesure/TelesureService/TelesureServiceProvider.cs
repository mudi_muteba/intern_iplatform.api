﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.Policies.Message;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Telesure.Lead;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;

namespace Workflow.QuoteAcceptance.Telesure.TelesureService
{
    public class TelesureServiceProvider : ITransferLeadToInsurer
    {
        private readonly ITelesureServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private readonly AuthenticationHeader _authenticationHeader;
        private static readonly ILog Log = LogManager.GetLogger<TelesureServiceProvider>();
        private static IEnumerable<TelesureInsurerProduct> _productMapping;
        private static CommunicationMetadata _mail;
        private readonly Guid _channelId;

        public TelesureServiceProvider(ITelesureServiceClient client, LeadFactory leadFactory, AuthenticationHeader authenticationHeader, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            _client = client;
            _leadFactory = leadFactory;
            _authenticationHeader = authenticationHeader;
            _mail = mail;
            _channelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
        }
        public static IEnumerable<TelesureInsurerProduct> Mapping(LeadTransferTelesureTaskMetadata metadata, ApiMetadata webServiceMetadata)
        {
            var result = new List<TelesureInsurerProduct>();
            result.AddRange(TelesureInsurerProduct.CreateRatingSetting(metadata, webServiceMetadata));
            return result;
        }

        private TransferCompleted Execute(PublishQuoteUploadMessageDto quote, TransferDetails lead)
        {
            TransferCompleted result = null;
            var responseSuccess = false;
            var errorMessage = string.Empty;
            RetryStrategy
                .OnSuccess(() =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.Telesure.Name(), quote.Request.Id, _channelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, ReferenceNumber, SerializerExtensions.Serialize(lead, typeof(TransferDetails))));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, ReferenceNumber));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, _mail.CommunicationMessage.Address));
                    })
                .OnFailure(() =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.Telesure.Name(), quote.Request.Id, _channelId));
                    })
                .OnStart(() =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.Telesure.Name(),quote.Request.Id, quote.SerializeAsXml(), _channelId)))
                .OnFinish((response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.Telesure.Name(),quote.Request.Id, response.SerializeAsXml(), _channelId)))
                .While(() => !responseSuccess)
                .Execute(() =>
                {
                    try
                    {
                        //if (quote.WorkflowEnvironment == "dev")
                        //{
                        //    Log.Warn("quoteacceptance/environment = dev. Quote was not uploaded");
                        //    result = new TransferCompleted
                        //    {
                        //        ContactID = Guid.NewGuid().ToString(),
                        //        ErrorStatus = "Success",
                        //    };
                        //}
                        //else
                        //{
                            var sessionId = GetSessionId(_client, _authenticationHeader);
                            if (sessionId == Guid.Empty.ToString())
                                ExecutionPlan.AddMessage(LeadTransferMessage.CreateCustom(GetNoSessionMessage(_authenticationHeader), Guid.Empty, InsurerName.Telesure.Name(), _channelId));
                            else
                                result = Upload(_client, lead, _authenticationHeader, sessionId);
                        //}
                            
                        responseSuccess = result.ErrorStatus != "E";
                        errorMessage = responseSuccess ? string.Empty : string.Join("|", result.ErrorDescription);
                        ReferenceNumber = string.IsNullOrEmpty(result.ContactID) ? Guid.NewGuid().ToString() : result.ContactID;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.Telesure.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.Telesure.Name(),quote.Request.Id, ex, _channelId));
                    }

                    return ValidResponse(result);
                });

            return ValidResponse(result);
        }
        public static TransferCompleted Upload(ITelesureServiceClient client, TransferDetails lead, AuthenticationHeader authenticationHeader,string sessionId)
        {
            return LeadTransfer.TransferLead(client, authenticationHeader, sessionId, lead);
        }
        private static TransferCompleted ValidResponse(TransferCompleted response)
        {
            return response ?? new TransferCompleted(){ ErrorStatus = "E"};
        }

        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var quote = message.Quote;

            if (quote == null)
            {
                Log.Error("Insurer Quote Accepted Message for Telesure has no Quote.");
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("Quote has not been added", message.SerializeAsXml(), Guid.Empty,InsurerName.Telesure.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureNoQuote(InsurerName.Telesure.Name(), Guid.Empty, _channelId));
                return new LeadTransferResult(false);
            }

            var metadata = message.GetMetadata<QuoteAcceptedMetadata>().TaskMetadata.Get<LeadTransferTelesureTaskMetadata>();
            _productMapping = GetCompanyDetails(quote, metadata, message.GetMetadata<QuoteAcceptedMetadata>().ApiMetadata);

            var lead = _leadFactory.CreateLead(_productMapping, metadata.IRateUrl);
            if (lead == null)
            {
                Log.ErrorFormat("Quote [{0}] failed to upload to Telesure. No items were found in the Quote", quote.Request.Id.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("No details were found in the Quote", message.SerializeAsXml(), quote.Request.Id,InsurerName.Telesure.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureNoQuote(InsurerName.Telesure.Name(), quote.Request.Id, _channelId));
                return new LeadTransferResult(false);
            }

            var result = Execute(quote, lead);
            if (result.ErrorStatus.Equals("E", StringComparison.CurrentCultureIgnoreCase))
            {
                var errorMessageList = result.ErrorDescription != null
                    ? result.ErrorDescription.ToList()
                    : new List<string>() {"No Error description available"};
                var errorMessage = errorMessageList.Aggregate(string.Empty, (current, errMess) => current + errMess);

                Log.ErrorFormat("Quote with Rating Request ID [{0}] failed to upload to Telesue with failure reason {1}", quote.Request.Id.ToString(),errorMessage);
                Log.DebugFormat("Lead: {0}", lead);
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}", errorMessage), result.SerializeAsXml(), quote.Request.Id,InsurerName.Telesure.Name(), _channelId, _mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, _mail.CommunicationMessage.Address));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.Hollard.Name(), quote.Request.Id, string.Format("Failure Reason {0}", errorMessage), _channelId));
                return new LeadTransferResult(false);
            }

            Log.InfoFormat("Quote with Rating Request ID [{0}] successfully uploaded to Telesure", quote.Request.Id.ToString());
            Log.DebugFormat("Lead: {0}", lead);
            return new LeadTransferResult(true);
        }

        public static IEnumerable<TelesureInsurerProduct> GetCompanyDetails(PublishQuoteUploadMessageDto quote, LeadTransferTelesureTaskMetadata metadata, ApiMetadata apimetata)
        {
            IEnumerable<TelesureInsurerProduct> telesureInsurerProducts = Mapping(metadata, apimetata);

            var localmap = telesureInsurerProducts
                .Where(x =>
                    x.InsurerCode == quote.Policy.InsurerCode.Trim() &&
                    x.ProductCode == quote.Policy.ProductCode.Trim())
                .Select(
                    x =>
                        new TelesureInsurerProduct(x.InsurerCode, x.ProductCode, x.Password, x.UserName, x.AuthCode,
                            x.BrokerCode, x.AgentCode, x.SchemeCode, x.Token, x.SubscriberCode, x.CompanyCode,
                            x.UwCompanyCode, x.UwProductCode));
            return localmap;
        }

        public static string GetSessionId(ITelesureServiceClient client, AuthenticationHeader authHeader)
        {
            Log.InfoFormat("Getting Session ID in Telesure Service using authentication header values username {0} and password {1}",
                authHeader.Username, authHeader.Password);
            var sessionId = ((APIServiceSoapClient)client).GetNewSessionID(authHeader);

            if (sessionId != Guid.Empty)
                return sessionId.ToString();

            Log.Error(GetNoSessionMessage(authHeader));
            
            return Guid.Empty.ToString();
        }

        private static string GetNoSessionMessage(AuthenticationHeader authHeader)
        {
            return string.Format("Could NOT get Session ID in Telesure Service using authentication header values username {0} and password {1}",
                    authHeader.Username, authHeader.Password);
        }
        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }
    }
}