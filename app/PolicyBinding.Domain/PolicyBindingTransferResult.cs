﻿using Workflow.Messages;

namespace Workflow.PolicyBinding.Domain
{
    public class PolicyBindingTransferResult
    {
        public PolicyBindingTransferResult(bool success)
        {
            Success = success;
        }

        public bool Success { get; private set; }
    }
}