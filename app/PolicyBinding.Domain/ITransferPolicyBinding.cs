﻿namespace Workflow.PolicyBinding.Domain
{
    public interface ITransferPolicyBinding
    {
        ITransferPolicyBinding Transfer();
        PolicyBindingTransferResult Result { get; }
    }
}