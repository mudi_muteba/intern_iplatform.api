﻿using System.Collections.Generic;
namespace Workflow.PolicyBinding.Domain
{
    public abstract class AbstractTransferOfPolicyBindingFactory<T1, T2> : ICreateTransferOfPolicyBinding<T1, T2>
    {
        public abstract IEnumerable<ITransferPolicyBinding> Create(T1 message, T2 workflow);

        public IEnumerable<ITransferPolicyBinding> Create(object message, object workflow)
        {
            return Create((T1)message, (T2)workflow);
        }
    }
}
