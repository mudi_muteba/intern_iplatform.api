﻿using Domain.PolicyBindings.Messages;
using Domain.QuoteAcceptance.Infrastructure;
using Workflow.Messages;

namespace Workflow.PolicyBinding.Domain
{
    public interface ITransferPolicyBindingToInsurer
    {
        ExecutionPlan ExecutionPlan { get; }
        IRetryStrategy RetryStrategy { get; }
        PolicyBindingTransferResult Transfer(SubmitPolicyBindingMessage message);
    }
}