﻿using System;
using System.Linq;
using Common.Logging;
using Workflow.Domain;
using Workflow.Messages;
using Domain.PolicyBindings.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Domain.PolicyBindings.Metadata;
using Domain.Base.Extentions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;


namespace Workflow.PolicyBinding.Domain.Consumers
{
    public class SubmitPolicyBindingMessageConsumer : AbstractMessageConsumer<SubmitPolicyBindingMessage>
    {
        private static readonly ILog m_Log = LogManager.GetLogger<SubmitPolicyBindingMessageConsumer>();
        private readonly ICreateTransferOfPolicyBinding<SubmitPolicyBindingMessage, WorkflowEngine> m_Transferer;

        public SubmitPolicyBindingMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, ICreateTransferOfPolicyBinding<SubmitPolicyBindingMessage, WorkflowEngine> transferer) 
            : base(router,executor)
        {
            m_Transferer = transferer;
        }

        public override void Consume(SubmitPolicyBindingMessage message)
        {
            m_Log.InfoFormat("Consuming Lead Transfer Messages from Insurer");
            try
            {
                m_Log.InfoFormat("Consuming Lead Transfer Messages from Insurer");
                var destinations = m_Transferer.Create(message, Workflow).ToList();

                if (!destinations.Any())
                {
                    throw new Exception(string.Format("Cannot find any lead transfers to transport lead to for Product Code {0}", message.Dto.RequestInfo.ProductCode));
                }

                m_Log.InfoFormat("Found {0} lead destinations to transfer lead to", destinations.Count());
                foreach (var destination in destinations)
                    message.Continue(new MessageProcessingResult(destination.Transfer().Result.Success), Workflow);
            }
            catch (Exception ex)
            {
                //revert Policy Binding status.
                Workflow.Publish(DeletePolicyBindingCompletedMessage.Create(message.Dto.QuoteId, message.Dto.RequestId));

                //monitoring
                Workflow.Publish(LeadTransferMessage.CreateException(ex.Message, message.Dto.RequestInfo.InsurerName,
                    message.Dto.RequestId, ex, message.Dto.ChannelInfo.SystemId));

                //email
                Workflow.Publish(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(),
                    message.Dto.RequestId, message.Dto.RequestInfo.InsurerName, message.Dto.ChannelInfo.SystemId,
                    message.GetMetadata<PolicyBindingMetadata>().Mail, message.Dto.Environment));
            }
        }
    }
}
