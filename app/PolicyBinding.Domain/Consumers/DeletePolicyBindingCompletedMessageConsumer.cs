﻿using Domain.QuoteAcceptance.Metadata.Shared;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Enums;
using Infrastructure.Configuration;
using Workflow.Domain;
using Workflow.Messages;
using Domain.PolicyBindings.Messages;

namespace Workflow.PolicyBinding.Domain.Consumers
{
    public class DeletePolicyBindingCompletedMessageConsumer : AbstractMessageConsumer<DeletePolicyBindingCompletedMessage>
    {
        private readonly IConnector _connector;

        public DeletePolicyBindingCompletedMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
            var token = _connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            _connector.SetToken(token);
        }
        
        public override void Consume(DeletePolicyBindingCompletedMessage message)
        {
            var metaData = message.Metadata as DeletePolicyBindingCompletedMetaData;
            _connector.QuotesManagement.Quote.DeletePolicyBindingCompleted(new DeletePolicyBindingCompletedDto(metaData.QuoteId, metaData.RequestId));
        }
    }
}
