﻿using System.Collections.Generic;

namespace Workflow.PolicyBinding.Domain
{
    public interface ICreateTransferOfPolicyBinding
    {
        IEnumerable<ITransferPolicyBinding> Create(object message, object workflow);
    }

    public interface ICreateTransferOfPolicyBinding<in T1, in T2> : ICreateTransferOfPolicyBinding
    {
        IEnumerable<ITransferPolicyBinding> Create(T1 message, T2 workflow);
    }
}
