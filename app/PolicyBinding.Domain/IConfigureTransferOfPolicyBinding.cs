﻿using Domain.PolicyBindings.Messages;
using System;
using System.Collections.Generic;
using Workflow.Messages;

namespace Workflow.PolicyBinding.Domain
{
    public interface IConfigureTransferOfPolicyBinding
    {
        IDictionary<IEnumerable<string>, Func<SubmitPolicyBindingMessage, WorkflowEngine, ITransferPolicyBinding>> Insurers { get; }
    }
}