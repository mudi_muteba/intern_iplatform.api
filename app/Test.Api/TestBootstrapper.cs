﻿using Api;
using Castle.Windsor;
using TestHelper.Installers;

namespace Test.Api
{
    public class TestBootstrapper : Bootstrapper
    {
        protected override void InstallDataAccessLayer(IWindsorContainer existingContainer)
        {
            existingContainer.Install(new TestDataAccessInstaller(false, null));
        }
    }
}