﻿using System;
using iPlatform.Api.DTOs.Policy;
using MasterData;

namespace TestObjects.Mothers.Policy
{
    public class NewPolicyDtoObjectMother
    {
        public static CreatePolicyHeaderDto ValidPolicyDto()
        {
            var dto = new CreatePolicyHeaderDto
            {
                PolicyNo = "Pending",
                ProductId = 1,
                PartyId = 1,
                DateEffective = new DateTime(2015, 5, 5),
                DateEnd = new DateTime(2016, 5, 5),
                DateRenewal = new DateTime(2016, 5, 5),
                Frequency = 1,
                Renewable = false,
                NewBusiness = false,
                VAP = false
            };

            dto.PolicyStatus = new PolicyStatus()
            {
                Id = 2,
                Name = "Potential",
                VisibleIndex = 2
            };

            dto.PolicyItems.Add(new CreatePolicyItemDto
            {
                AssetNumber = 1,
                ItemDescription = "VW 2.5 tdi",
                DateEffective = new DateTime(2015, 5, 5),
                DateEnd = new DateTime(2016, 5, 5),
            });

            dto.PolicyItems.Add(new CreatePolicyItemDto
            {
                AssetNumber = 2,
                ItemDescription = "Cruze 1.6 LTS",
                DateEffective = new DateTime(2015, 5, 5),
                DateEnd = new DateTime(2016, 5, 5),
            });

            return dto;
        }
    }
}