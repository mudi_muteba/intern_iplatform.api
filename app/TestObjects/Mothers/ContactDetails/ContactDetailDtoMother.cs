﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.ContactDetail;

namespace TestObjects.Mothers.ContactDetails
{
    public class ContactDetailDtoMother
    {
        public static ContactDetailDto ContactDetailDto
        {
            get
            {
                var name = typeof(ContactDetailDto).Name;
                return new ContactDetailDto
                {
                    Id = typeof(ContactDetailDto).GetHashCode() + 1,
                    Email = string.Format("email@{0}.co.za", name),
                    Cell = "0821234567",
                    Direct = "0111234567",
                    Fax = "0111234568",
                    Work = "0111234569",
                    Home = "0111234566",
                    Website = string.Format("website.{0}.co.za", name),
                    Links = new List<ResourceLink>()
                };
            }
        }

        public static CreateContactDetailDto CreateContactDetailDto
        {
            get
            {
                var name = typeof(CreateContactDetailDto).Name;
                return new CreateContactDetailDto
                {
                    Id = typeof(ContactDetailDto).GetHashCode() + 1,
                    Email = string.Format("email@{0}.co.za", name),
                    Cell = "0821234567",
                    Direct = "0111234567",
                    Fax = "0111234568",
                    Work = "0111234569",
                    Home = "0111234566",
                    Website = string.Format("website.{0}.co.za", name),
                    Links = new List<ResourceLink>()
                };
            }
        }
    }
}