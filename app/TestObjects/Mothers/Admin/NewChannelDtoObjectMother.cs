﻿using System;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;
using iPlatform.Api.DTOs.Admin;
using MasterData;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;

namespace TestObjects.Mothers.Admin
{
    public class NewChannelDtoObjectMother
    {
        public static CreateChannelDto ValidChannelDto()
        {
            return new CreateChannelDto
            {
                Id = 999,
                IsActive = false,
                IsDefault = false,
                Currency =  Currencies.SouthAfricanRand,
                DateFormat = "dd/MM/yyyy",
                Language = Languages.English,
                Name = "This is a test channel " + new Random().Next(100,9999).ToString(),
                Code = new Random().Next(100,9999).ToString(),
                Country = Countries.SouthAfrica,
                ExternalReference = "Reference" + new Random().Next(100, 9999).ToString(),
            };
        }

        public static EditChannelDto ValidEditChannelDto(int channelId)
        {
            return new EditChannelDto
            {
                Id = channelId,
                IsActive = true,
                IsDefault = true,
                Currency = Currencies.SouthAfricanRand,
                DateFormat = "dd/MM/yyyy",
                Language = Languages.English,
                Name = "This is a test channel " + DateTime.UtcNow.ToString(),
                Code = "TestChannelCodeUpdated",
                OrganizationId = 1,
                ExternalReference = "Reference1" + new Random().Next(100, 9999).ToString(),
            };
        }
        public static EditChannelDto ValidEditChannelDto(CreateChannelDto dto)
        {
            return new EditChannelDto
            {
                Id = dto.Id,
                IsActive = true,
                IsDefault = true,
                Currency = Currencies.SouthAfricanRand,
                DateFormat = "dd/MM/yyyy",
                Language = Languages.Zulu,
                Name = dto.Name,
                Code = dto.Code,
                ExternalReference = dto.ExternalReference,
            };
        }

        public static EditChannelDto ValidEditChannelUniqueCodeDto(int channelId)
        {
            return new EditChannelDto
            {
                Id = channelId,
                IsActive = true,
                IsDefault = true,
                Currency = Currencies.SouthAfricanRand,
                DateFormat = "dd/MM/yyyy",
                Language = Languages.English,
                Name = "This is a test channel " + DateTime.UtcNow.ToString(),
                Code = "CodeA",
                ExternalReference = "Referencex1" + new Random().Next(100, 9999).ToString(),
            };
        }

        public static CreateChannelPermissionDto ValidChannelPermissionDto()
        {
            return new CreateChannelPermissionDto
            {
                ChannelId = Guid.Parse("16F67549-B713-4558-A38D-A399BEE346F0"),
                ProductCode = "BUD",
                InsurerCode =  "BIB", 
                AllowProposalCreation = false,
                AllowQuoting = false,
                AllowSecondLevelUnderwriting = false,
            };
        }

        public static EditChannelPermissionDto ValidChannelPermissionDto(int id)
        {
            return new EditChannelPermissionDto
            {
                Id = id,
                ChannelId = Guid.Parse("16F67549-B713-4558-A38D-A399BEE346F0"),
                ProductCode = "BUD",
                InsurerCode = "BIB",
                AllowProposalCreation = true,
                AllowQuoting = true,
                AllowSecondLevelUnderwriting = true,
            };
        }

        public static CreateChannelPermissionDto ValidChannelPermissionDto1()
        {
            return new CreateChannelPermissionDto
            {
                ChannelId = Guid.Parse("16F67549-B713-4558-A38D-A399BEE346F0"),
                ProductCode = "ALLIN",
                InsurerCode = "HOL",
                AllowProposalCreation = false,
                AllowQuoting = false,
                AllowSecondLevelUnderwriting = false,
            };
        }

        public static CreateChannelPermissionDto ValidChannelPermissionDto2()
        {
            return new CreateChannelPermissionDto
            {
                ChannelId = Guid.Parse("16F67549-B713-4558-A38D-A399BEE346F0"),
                ProductCode = "HOLHMH",
                InsurerCode = "HOL",
                AllowProposalCreation = false,
                AllowQuoting = false,
                AllowSecondLevelUnderwriting = false,
            };
        }

        public static CreateChannelPermissionDto ValidChannelPermissionDto3()
        {
            return new CreateChannelPermissionDto
            {
                ChannelId = Guid.Parse("16F67549-B713-4558-A38D-A399BEE346F0"),
                ProductCode = "REGPRD",
                InsurerCode = "REG",
                AllowProposalCreation = false,
                AllowQuoting = false,
                AllowSecondLevelUnderwriting = false,
            };
        }

        public static CreateChannelPermissionDto ValidChannelPermissionDto4()
        {
            return new CreateChannelPermissionDto
            {
                ChannelId = Guid.Parse("16F67549-B713-4558-A38D-A399BEE346F0"),
                ProductCode = "AUGPRD",
                InsurerCode = "AUG",
                AllowProposalCreation = false,
                AllowQuoting = false,
                AllowSecondLevelUnderwriting = false,
            };
        }
    }
}