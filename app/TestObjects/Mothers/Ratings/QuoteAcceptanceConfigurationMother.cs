using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using Domain.Admin.SettingsQuoteUploads;
using System.Collections.Generic;
using Domain.Admin;
using Domain.Products;

namespace TestObjects.Mothers.Ratings
{
    public class QuoteAcceptanceConfigurationMother
    {
        public static List<SettingsQuoteUploadDto> OakhurstConfig()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/ServiceURL", "http://196.212.18.130/OaksureUATWS/service.asmx"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/User", "PSG00001"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/Password", "C0v3rM3"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/AgentCode", "PSGD"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/AgentName", "PSGD"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/AgentTelephone", "0861988888"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/BrokerCode", "PSGD"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/SubBrokerCode", "PSGD"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/SubPartnerCode", "PSGD"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/SubPartnerName", "PSGD"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/SchemeName", "LITE"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/subject", "UPLOADING LEAD TO OAKHURST"));
            config.Add(new SettingsQuoteUploadDto("Oakhurst/Upload/template", "LeadUploadTemplate"));

            return config;
        }

        public static List<SettingsQuoteUploadDto> CampaignConfig()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/LeadServiceURL", "http://196.29.140.181/LeadWebService/LeadWebservice.asmx"));
            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/CampaignSystemReference", "RAM825MEN"));
            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/SystemReference", "OQV454SUP"));
            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/LeadSourceReference", ""));
            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/subject", "UPLOADING LEAD TO CAMPAIGN"));
            config.Add(new SettingsQuoteUploadDto("Campaign/Upload/template", "LeadUploadTemplate"));

            return config;
        }

        public static List<SettingsQuoteUploadDto> DotsureConfig()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/ServiceURL", "http://196.212.18.130/DotsureUATWS/service.asmx"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/User", "DOTSURE"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/Password", "D0tSur3"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/AgentCode", "D0tSur3"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/AgentName", "DOTAMS"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/AgentTelephone", "0861988888"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/BrokerCode", "DOTAMS"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/SubBrokerCode", "DOTAMS"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/SubPartnerCode", "DOTAMS"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/SubPartnerName", "DOTAMS"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/SchemeName", "DOTSURE DIRECT"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/subject", "UPLOADING LEAD TO DOSTSURE"));
            config.Add(new SettingsQuoteUploadDto("Dotsure/Upload/template", "LeadUploadTemplate"));

            return config;
        }

        public static List<SettingsQuoteUploadDto> HollardConfig()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/ServiceURL", "https://hollardswitchingmoduletest.hollard.co.za/service.svc"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/AuthToken", "RUoget350EhkMxauURxmRIiLfQA3rv4g"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/SchemeCode", "634"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/Campaign", "515"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/CampaignType", "89"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/ProductCode", "91"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/ProductCode/Motor", "70"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/ProductCode/AllRisks", "10"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/ProductCode/HO", "5"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/ProductCode/HH", "4"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/subject", "UPLOADING LEAD TO HOLLARD"));
            config.Add(new SettingsQuoteUploadDto("Hollard/Upload/template", "LeadUploadTemplate"));

            return config;
        }

        public static List<SettingsQuoteUploadDto> IDSConfig()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("IDS/Upload/ServiceURL", "http://195.202.66.179/STPWS/Command.svc"));
            config.Add(new SettingsQuoteUploadDto("IDS/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("IDS/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("IDS/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("IDS/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("IDS/Upload/subject", "UPLOADING LEAD TO IDS"));
            config.Add(new SettingsQuoteUploadDto("IDS/Upload/template", "LeadUploadTemplate"));
            return config;
        }

        public static List<SettingsQuoteUploadDto> RegentConfig()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("Regent/Upload/ServiceURL", "http://test.regent.co.za/webservices/clientinterface.asmx"));
            config.Add(new SettingsQuoteUploadDto("Regent/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Regent/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Regent/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("Regent/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("Regent/Upload/subject", "UPLOADING LEAD TO REGENT"));
            config.Add(new SettingsQuoteUploadDto("Regent/Upload/template", "LeadUploadTemplate"));
            return config;
        }
        public static List<SettingsQuoteUploadDto> SAUConfig()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("SAU/Upload/ServiceURL", "http://services.sauonline.co.za/blackbox/quote.asmx"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/UserName", "CIMS"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/Password", "C!m5@SAU"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/BrokerCode", "MC12"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/fspCode", "728"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/AppName", "IPLATFORM"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/CompanyCode", "Cardinal"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/subject", "UPLOADING LEAD TO SAU"));
            config.Add(new SettingsQuoteUploadDto("SAU/Upload/template", "LeadUploadTemplate"));

            return config;
        }
        public static List<SettingsQuoteUploadDto> TelesureUnityConfig()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/ServiceURL", "https://preview.telesure.co.za/api/quickquotes/apiservice.asmx"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/InsurerCode", "UNITY"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/ProductCode", "UNI"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/BrokerCode", "UPSGDIR"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/CompanyCode", "13"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/UwCompanyCode", "02"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/UwProductCode", "02"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/AuthCode", "15"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/Token", "16"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/SchemeCode", "12"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/AgentCode", "MUPSG"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/SubscriberCode", "PSU"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/subject", "UPLOADING LEAD TO SAU"));
            config.Add(new SettingsQuoteUploadDto("Telesureunity/Upload/template", "LeadUploadTemplate"));

            return config;
        }

        public static List<SettingsQuoteUploadDto> KingPriceV2Config()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/ServiceURL", "https://secure.kingprice.co.za/ServicesUAT/PartnerService.svc"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/ChannelEndpoint", "https://secure.kingprice.co.za/ServicesUAT/PartnerService.svc"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/SecurityEndpoint", "https://secure.kingprice.co.za/IdentityServer/issue/wstrust/mixed/username"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/SecurityDestination", "https://secure.kingprice.co.za/ServicesUAT/"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/InsurerCode", "KPI"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/ProductCode", "KPIPERS2"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/UserName", "iPlatform_APIPartner"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/Password", "h8kPZp3RMU4f"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/Token", "API-9717E8459DCE4149A77ED02DA6CC245D"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/AgentCode", "IPLATFORM"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/BrokerCode", "FC34F169-84AF-4A34-9321-A60B00C164CC"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/CompanyCode", "Psg Wealth Financial Planning (Pty) Ltd  [Iplat]"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/subject", "UPLOADING LEAD TO King Price"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceobv2/Upload/template", "LeadUploadTemplate"));

            return config;
        }

        public static List<SettingsQuoteUploadDto> KingPriceConfig()
        {
            var config = new List<SettingsQuoteUploadDto>();

            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/ServiceURL", "https://secure.kingprice.co.za/ServicesUAT/PartnerService.svc"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/ChannelEndpoint", "https://secure.kingprice.co.za/ServicesUAT/PartnerService.svc"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/SecurityEndpoint", "https://secure.kingprice.co.za/IdentityServer/issue/wstrust/mixed/username"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/SecurityDestination", "https://secure.kingprice.co.za/ServicesUAT/"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/InsurerCode", "KPI"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/ProductCode", "KPIPERS2"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/UserName", "iPlatform_APIPartner"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/Password", "h8kPZp3RMU4f"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/Token", "API-9717E8459DCE4149A77ED02DA6CC245D"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/AgentCode", "IPLATFORM"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/BrokerCode", "FC34F169-84AF-4A34-9321-A60B00C164CC"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/CompanyCode", "Psg Wealth Financial Planning (Pty) Ltd  [Iplat]"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/AcceptanceEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/ErrorEmailAddress", "monitoring@iplatform.co.za"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/RetryDelayIncrement", "5"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/MaxRetries", "2"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/subject", "UPLOADING LEAD TO King Price"));
            config.Add(new SettingsQuoteUploadDto("Kingpriceob/Upload/template", "LeadUploadTemplate"));

            return config;
        }

        public static List<SettingsQuoteUpload> DotsureEntityConfig()
        {
            var config = new List<SettingsQuoteUpload>();

            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/ServiceURL", SettingValue = "http://196.212.18.130/DotsureUATWS/service.asmx", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" }});
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/User", SettingValue = "DOTSURE", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/Password", SettingValue = "D0tSur3", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/AgentCode", SettingValue = "D0tSur3", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/AgentName", SettingValue = "DOTAMS", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/AgentTelephone", SettingValue = "0861988888", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } }); ;
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/BrokerCode", SettingValue = "DOTAMS", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/SubBrokerCode", SettingValue = "DOTAMS", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/SubPartnerCode", SettingValue = "DOTAMS", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/SubPartnerName", SettingValue = "DOTAMS", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/SchemeName", SettingValue = "DOTSURE DIRECT", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/AcceptanceEmailAddress", SettingValue = "monitoring@iplatform.co.za", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/ErrorEmailAddress", SettingValue = "monitoring@iplatform.co.za", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/RetryDelayIncrement", SettingValue = "5", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/MaxRetries", SettingValue = "2", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/subject", SettingValue = "UPLOADING LEAD TO DOSTSURE", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });
            config.Add(new SettingsQuoteUpload { SettingName = "Dotsure/Upload/template", SettingValue = "LeadUploadTemplate", Environment = "Dev", Channel = new Channel(1), Product = new Product { ProductCode = "DOTSURE" } });

            return config;
        }
    }
}