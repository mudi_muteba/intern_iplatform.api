﻿using iPlatform.Api.DTOs.Ratings.Response;

namespace TestObjects.Mothers.Ratings.Result
{
    public class RatingResultItemObjectMother
    {
        public static RatingResultItemBuilder Builder
        {
            get { return new RatingResultItemBuilder(); }
        }

        public static IRatingResultItemData SantamRatingResultItemData
        {
            get { return new SantamRatingResultItemData(); }
        }

        public static RatingResultItemDto SantamRatingResultItem
        {
            get { return Builder.Build(SantamRatingResultItemData); }
        }

        public static IRatingResultItemData RenasaMotorRatingResultItemData
        {
            get { return new RenasaRatingResultMotorItemData(); }
        }

        public static RatingResultItemDto RenasaMotorRatingResultItem
        {
            get { return Builder.Build(RenasaMotorRatingResultItemData); }
        }

        public static IRatingResultItemData HollardMotorRatingResultItemData
        {
            get { return new HollardRatingResultMotorItemData(); }
        }

        public static RatingResultItemDto HollardMotorRatingResultItem
        {
            get { return Builder.Build(HollardMotorRatingResultItemData); }
        }

        public static IRatingResultItemData RenasaContentRatingResultItemData
        {
            get { return new RenasaRatingResultContentItemData(); }
        }

        public static RatingResultItemDto RenasaContentRatingResultItem
        {
            get { return Builder.Build(RenasaContentRatingResultItemData); }
        }

        public static IRatingResultItemData RenasaBuildingRatingResultItemData
        {
            get { return new RenasaRatingResultBuildingItemData(); }
        }

        public static RatingResultItemDto RenasaBuildingRatingResultItem
        {
            get { return Builder.Build(RenasaBuildingRatingResultItemData); }
        }

        public static IRatingResultItemData RenasaAllRiskRatingResultItemData
        {
            get { return new RenasaRatingResultAllRiskItemData(); }
        }

        public static RatingResultItemDto RenasaAllRiskRatingResultItem
        {
            get { return Builder.Build(RenasaAllRiskRatingResultItemData); }
        }

    }
}
