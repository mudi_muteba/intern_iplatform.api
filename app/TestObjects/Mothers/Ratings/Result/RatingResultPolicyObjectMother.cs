﻿using iPlatform.Api.DTOs.Ratings.Response;

namespace TestObjects.Mothers.Ratings.Result
{
    public class RatingResultPolicyObjectMother
    {
        public static RatingResultPolicyBuilder Builder
        {
            get { return new RatingResultPolicyBuilder(); }
        }

        public static IRatingResultPolicyData Kingprice2RatingResultPolicyData
        {
            get { return new KP2RatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto Kingprice2RatingResultPolicy
        {
            get { return Builder.Build(Kingprice2RatingResultPolicyData); }
        }

        public static IRatingResultPolicyData SauRatingResultPolicyData
        {
            get { return new SauRatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto SauRatingResultPolicy
        {
            get { return Builder.Build(SauRatingResultPolicyData); }
        }

        public static IRatingResultPolicyData SantamRatingResultPolicyData
        {
            get { return new SantamRatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto SantamRatingResultPolicy
        {
            get { return Builder.Build(SantamRatingResultPolicyData); }
        }

        public static IRatingResultPolicyData RenasaRatingResultPolicyData
        {
            get { return new RenasaRatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto RenasaRatingResultPolicy
        {
            get { return Builder.Build(RenasaRatingResultPolicyData); }
        }

        public static IRatingResultPolicyData OakhurstRatingResultPolicyData
        {
            get { return new OakhurstRatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto OakhurstRatingResultPolicy
        {
            get { return Builder.Build(OakhurstRatingResultPolicyData); }
        }

        public static IRatingResultPolicyData DotsureRatingResultPolicyData
        {
            get { return new DotsureRatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto DotsureRatingResultPolicy
        {
            get { return Builder.Build(DotsureRatingResultPolicyData); }
        }

        public static IRatingResultPolicyData TelesureRatingResultPolicyData
        {
            get { return new TelesureRatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto TelesureRatingResultPolicy
        {
            get { return Builder.Build(TelesureRatingResultPolicyData); }
        }

        public static IRatingResultPolicyData FakeRatingResultPolicyData
        {
            get { return new FakeRatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto FakeRatingResultPolicy
        {
            get { return Builder.Build(FakeRatingResultPolicyData); }
        }

        public static IRatingResultPolicyData HollardRatingResultPolicyData
        {
            get { return new HollardRatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto HollardRatingResultPolicy
        {
            get { return Builder.Build(HollardRatingResultPolicyData); }
        }
        

        public static IRatingResultPolicyData RegentMotorRatingResultItemData
        {
            get { return new RegentRatingResultPolicyData(); }
        }

        public static RatingResultPolicyDto RegentRatingResultPolicy
        {
            get { return Builder.Build(RegentMotorRatingResultItemData); }
        }
    }
}
