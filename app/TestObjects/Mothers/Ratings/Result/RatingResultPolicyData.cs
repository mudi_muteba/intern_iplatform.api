﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Response;

namespace TestObjects.Mothers.Ratings.Result
{
    public interface IRatingResultPolicyData
    {
        RatingResultState State { get; }
        string InsurerCode { get; }
        string ProductCode { get; }
        string InsurerProductCode { get; }
        decimal Fees { get; }
        List<RatingResultItemDto> Items { get; }
        string ITCScore { get; }
        string ExtraITCScore { get; }
    }
    public class KP2RatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "KPI"; }
        }

        public string ProductCode
        {
            get { return "KPIPERS2"; }
        }

        public string InsurerProductCode
        {
            get { return ""; }
        }

        public decimal Fees
        {
            get { return 100m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>
                {
                    RatingResultItemObjectMother.SantamRatingResultItem
                };
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }
    }
    public class SauRatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "SAU"; }
        }

        public string ProductCode
        {
            get { return "CENTND"; }
        }

        public string InsurerProductCode
        {
            get { return ""; }
        }

        public decimal Fees
        {
            get { return 100m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>
                {
                    RatingResultItemObjectMother.SantamRatingResultItem
                };
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }
    }

    public class SantamRatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "STM"; }
        }

        public string ProductCode
        {
            get { return "MULMOT"; }
        }

        public string InsurerProductCode
        {
            get { return ""; }
        }

        public decimal Fees
        {
            get { return 100m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>
                {
                    RatingResultItemObjectMother.SantamRatingResultItem
                };
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }
    }

    public class RenasaRatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "REN"; }
        }

        public string ProductCode
        {
            get { return "RENPRD"; }
        }

        public string InsurerProductCode
        {
            get { return ""; }
        }

        public decimal Fees
        {
            get { return 139.05m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>
                {
                    RatingResultItemObjectMother.RenasaMotorRatingResultItem,
                    RatingResultItemObjectMother.RenasaContentRatingResultItem,
                    RatingResultItemObjectMother.RenasaBuildingRatingResultItem,
                    RatingResultItemObjectMother.RenasaAllRiskRatingResultItem
                };
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }

    }

    public class TelesureRatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "UNITY"; }
        }

        public string ProductCode
        {
            get { return "UNI"; }
        }

        public string InsurerProductCode
        {
            get { return ""; }
        }

        public decimal Fees
        {
            get { return 139.05m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>
                {
                    RatingResultItemObjectMother.RenasaMotorRatingResultItem,
                    RatingResultItemObjectMother.RenasaContentRatingResultItem,
                    RatingResultItemObjectMother.RenasaBuildingRatingResultItem,
                    RatingResultItemObjectMother.RenasaAllRiskRatingResultItem
                };
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }

    }
    public class RegentRatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "REGENT"; }
        }

        public string ProductCode
        {
            get { return "REGPRD"; }
        }

        public string InsurerProductCode
        {
            get { return ""; }
        }

        public decimal Fees
        {
            get { return 139.05m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>();
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }

    }

    public class HollardRatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "HOL"; }
        }

        public string ProductCode
        {
            get { return "HOLHMH"; }
        }

        public string InsurerProductCode
        {
            get { return ""; }
        }

        public decimal Fees
        {
            get { return 139.05m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>
                {
                    RatingResultItemObjectMother.HollardMotorRatingResultItem,
                    RatingResultItemObjectMother.RenasaContentRatingResultItem,
                    RatingResultItemObjectMother.RenasaBuildingRatingResultItem,
                    RatingResultItemObjectMother.RenasaAllRiskRatingResultItem
                };
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }

    }

    public class FakeRatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "FAKE"; }
        }

        public string ProductCode
        {
            get { return "FAKE"; }
        }

        public string InsurerProductCode
        {
            get { return ""; }
        }

        public decimal Fees
        {
            get { return 139.05m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>
                {
                    RatingResultItemObjectMother.HollardMotorRatingResultItem,
                    RatingResultItemObjectMother.RenasaContentRatingResultItem,
                    RatingResultItemObjectMother.RenasaBuildingRatingResultItem,
                    RatingResultItemObjectMother.RenasaAllRiskRatingResultItem
                };
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }

    }

    public class OakhurstRatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "AA"; }
        }

        public string ProductCode
        {
            get { return "LITE"; }
        }

        public string InsurerProductCode
        {
            get { return "LITEAA"; }
        }

        public decimal Fees
        {
            get { return 139.05m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>
                {
                    RatingResultItemObjectMother.SantamRatingResultItem
                };
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }
    }

    public class DotsureRatingResultPolicyData : IRatingResultPolicyData
    {
        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }

        public string InsurerCode
        {
            get { return "LLOYDS"; }
        }

        public string ProductCode
        {
            get { return "DOTSURE"; }
        }

        public string InsurerProductCode
        {
            get { return "DOTSURE"; }
        }

        public decimal Fees
        {
            get { return 139.05m; }
        }

        public List<RatingResultItemDto> Items
        {
            get
            {
                return new List<RatingResultItemDto>
                {
                    RatingResultItemObjectMother.SantamRatingResultItem
                };
            }
        }

        public string ITCScore
        {
            get { return "8"; }
        }

        public string ExtraITCScore
        {
            get { return "6"; }
        }
    }
}
