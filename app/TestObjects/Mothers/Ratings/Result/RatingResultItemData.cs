﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;

namespace TestObjects.Mothers.Ratings.Result
{
    public interface IRatingResultItemData
    {
        decimal Premium { get; }
        decimal Sasria { get; }
        int AssetNo { get; }
        Cover Cover { get; }
        RatingResultItemExcessDto Excess { get; }
        RatingResultState State { get; }
    }

    public class SantamRatingResultItemData : IRatingResultItemData
    {
        public decimal Premium
        {
            get { return 8021.76m; }
        }

        public decimal Sasria
        {
            get { return 2m; }
        }

        public int AssetNo
        {
            get { return 1; }
        }

        public Cover Cover
        {
            get { return Covers.Motor; }
        }

        public RatingResultItemExcessDto Excess
        {
            get
            {
                return new RatingResultItemExcessDto
                {
                    Calculated = false,
                    Basic = 0,
                    Description = string.Empty
                };
            }
        }

        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }
    }

    public class HollardRatingResultMotorItemData : IRatingResultItemData
    {
        public decimal Premium
        {
            get { return 490.48m; }
        }

        public decimal Sasria
        {
            get { return 2m; }
        }


        public int AssetNo
        {
            get { return 1; }
        }

        public Cover Cover
        {
            get { return Covers.Motor; }
        }

        public RatingResultItemExcessDto Excess
        {
            get
            {
                return new RatingResultItemExcessDto
                {
                    Calculated = true,
                    Basic = 2800m,
                    Description = string.Empty
                };
            }
        }

        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }
    }

    public class RenasaRatingResultMotorItemData : IRatingResultItemData
    {
        public decimal Premium
        {
            get { return 490.48m; }
        }

        public decimal Sasria
        {
            get { return 2m; }
        }


        public int AssetNo
        {
            get { return 1; }
        }

        public Cover Cover
        {
            get { return Covers.Motor; }
        }

        public RatingResultItemExcessDto Excess
        {
            get
            {
                return new RatingResultItemExcessDto
                {
                    Calculated = true,
                    Basic = 2800m,
                    Description = string.Empty
                };
            }
        }

        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }
    }

    public class RenasaRatingResultContentItemData : IRatingResultItemData
    {
        public decimal Premium
        {
            get { return 55.48m; }
        }

        public decimal Sasria
        {
            get { return 1.80m; }
        }


        public int AssetNo
        {
            get { return 2; }
        }

        public Cover Cover
        {
            get { return Covers.Contents; }
        }

        public RatingResultItemExcessDto Excess
        {
            get
            {
                return new RatingResultItemExcessDto
                {
                    Calculated = true,
                    Basic = 2000m,
                    Description = string.Empty
                };
            }
        }

        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }
    }

    public class RenasaRatingResultBuildingItemData : IRatingResultItemData
    {
        public decimal Premium
        {
            get { return 121.48m; }
        }

        public decimal Sasria
        {
            get { return 2.65m; }
        }


        public int AssetNo
        {
            get { return 3; }
        }

        public Cover Cover
        {
            get { return Covers.Building; }
        }

        public RatingResultItemExcessDto Excess
        {
            get
            {
                return new RatingResultItemExcessDto
                {
                    Calculated = true,
                    Basic = 200m,
                    Description = string.Empty
                };
            }
        }

        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }
    }

    public class RenasaRatingResultAllRiskItemData : IRatingResultItemData
    {
        public decimal Premium
        {
            get { return 16.48m; }
        }

        public decimal Sasria
        {
            get { return 0.21m; }
        }


        public int AssetNo
        {
            get { return 4; }
        }

        public Cover Cover
        {
            get { return Covers.AllRisk; }
        }

        public RatingResultItemExcessDto Excess
        {
            get
            {
                return new RatingResultItemExcessDto
                {
                    Calculated = true,
                    Basic = 100m,
                    Description = string.Empty
                };
            }
        }

        public RatingResultState State
        {
            get { return new RatingResultState(); }
        }
    }
}
