﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Ratings.Response;

namespace TestObjects.Mothers.Ratings.Result
{
    public interface IRatingResultPolicyBuilder
    {
        RatingResultPolicyBuilder WithState(RatingResultState state);
        RatingResultPolicyBuilder WithInsurerCode(string code);
        RatingResultPolicyBuilder WithProductCode(string code);
        RatingResultPolicyBuilder WithInsurerProductCode(string code);
        RatingResultPolicyBuilder WithFees(decimal fees);
        RatingResultPolicyBuilder WithItem(Action<IRatingResultItemBuilder> action);
        RatingResultPolicyBuilder WithItems(List<RatingResultItemDto> items);
    }

    public class RatingResultPolicyBuilder : IRatingResultPolicyBuilder
    {
        private RatingResultState state;
        private string insurerCode;
        private string productCode;
        private string insurerProductCode;
        private decimal fees;
        private readonly List<RatingResultItemDto> items = new List<RatingResultItemDto>();
        private string itcScore;
        private string extraITCScore;

        public RatingResultPolicyDto Build()
        {
            return new RatingResultPolicyDto
            {
                State = state,
                InsurerCode = insurerCode,
                ProductCode = productCode,
                InsurerProductCode = insurerProductCode,
                Items = items,
                Fees = fees,
                ITCScore = itcScore,
                ExtraITCScore = extraITCScore
            };
        }

        public RatingResultPolicyDto Build(IRatingResultPolicyData data)
        {
            return WithInsurerCode(data.InsurerCode)
                .WithProductCode(data.ProductCode)
                .WithInsurerProductCode(data.InsurerProductCode)
                .WithItems(data.Items)
                .WithFees(data.Fees)
                .WithState(data.State)
                .WithITCScore(data.ITCScore)
                .WithExtraITCScore(data.ExtraITCScore)
                .Build();
        }

        private RatingResultPolicyBuilder WithExtraITCScore(string extraITCScore)
        {
            this.extraITCScore = extraITCScore;
            return this;
        }

        private RatingResultPolicyBuilder WithITCScore(string itcScore)
        {
            this.itcScore = itcScore;
            return this;
        }

        public RatingResultPolicyBuilder WithState(RatingResultState state)
        {
            this.state = state;
            return this;
        }

        public RatingResultPolicyBuilder WithInsurerCode(string code)
        {
            insurerCode = code;
            return this;
        }

        public RatingResultPolicyBuilder WithProductCode(string code)
        {
            productCode = code;
            return this;
        }

        public RatingResultPolicyBuilder WithInsurerProductCode(string code)
        {
            insurerProductCode = code;
            return this;
        }

        public RatingResultPolicyBuilder WithFees(decimal fees)
        {
            this.fees = fees;
            return this;
        }

        public RatingResultPolicyBuilder WithItem(Action<IRatingResultItemBuilder> action)
        {
            var builder = new RatingResultItemBuilder();

            action(builder);

            items.Add(builder.Build());

            return this;
        }

        public RatingResultPolicyBuilder WithItems(List<RatingResultItemDto> items)
        {
            this.items.AddRange(items);
            return this;
        }
    }
}
