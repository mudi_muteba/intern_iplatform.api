﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;

namespace TestObjects.Mothers.Ratings.Result
{
    public interface IRatingResultItemBuilder
    {
        RatingResultItemBuilder WithPremium(decimal premium);
        RatingResultItemBuilder WithSasria(decimal sasria);
        RatingResultItemBuilder WithAssetNo(int assetNo);
        RatingResultItemBuilder WithCover(Cover cover);
        RatingResultItemBuilder WithExcess(RatingResultItemExcessDto excess);
        RatingResultItemBuilder WithState(RatingResultState state);
    }

    public class RatingResultItemBuilder : IRatingResultItemBuilder
    {
        private decimal premium;
        private decimal sasria;
        private int assetNo;
        private Cover cover;
        private RatingResultItemExcessDto excess;
        private RatingResultState state;

        public RatingResultItemDto Build()
        {
            return new RatingResultItemDto
            {
                Premium = premium,
                Sasria = sasria,
                Cover = cover,
                State = state,
                AssetNo = assetNo,
                Excess = excess
            };
        }

        public RatingResultItemDto Build(IRatingResultItemData data)
        {
            return WithPremium(data.Premium)
                .WithSasria(data.Sasria)
                .WithAssetNo(data.AssetNo)
                .WithCover(data.Cover)
                .WithExcess(data.Excess)
                .WithState(data.State)
                .Build();
        }

        public RatingResultItemBuilder WithPremium(decimal premium)
        {
            this.premium = premium;
            return this;
        }

        public RatingResultItemBuilder WithSasria(decimal sasria)
        {
            this.sasria = sasria;
            return this;
        }


        public RatingResultItemBuilder WithAssetNo(int assetNo)
        {
            this.assetNo = assetNo;
            return this;
        }

        public RatingResultItemBuilder WithCover(Cover cover)
        {
            this.cover = cover;
            return this;
        }

        public RatingResultItemBuilder WithExcess(RatingResultItemExcessDto excess)
        {
            this.excess = excess;
            return this;
        }

        public RatingResultItemBuilder WithState(RatingResultState state)
        {
            this.state = state;
            return this;
        }
    }
}
