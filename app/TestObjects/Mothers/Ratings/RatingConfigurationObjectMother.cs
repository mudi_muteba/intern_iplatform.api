using System;
using System.Collections.Generic;
using Domain.Ratings;

namespace TestObjects.Mothers.Ratings
{
    public static class RatingConfigurationObjectMother
    {
        public static List<RatingConfiguration> ValidRatingSettings()
        {
            return new List<RatingConfiguration>()
            {
                new RatingConfiguration(1, new Guid("f05f2384-2524-4531-80ba-8a198fc14ba0"), 1,  string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, true, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "Product1", "Insurer1"),

                new RatingConfiguration(1, new Guid("f05f2384-2524-4531-80ba-8a198fc14ba0"), 2, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, true, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "Product2", "Insurer2"),

                new RatingConfiguration(1, new Guid("f05f2384-2524-4531-80ba-8a198fc14ba0"), 3, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, true, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "Product3", "Insurer3"),

                new RatingConfiguration(2, Guid.NewGuid(), 5, string.Empty,string.Empty, string.Empty, string.Empty,
                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, true, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "Product3", "Insurer3"),
            };
        }
    }
}