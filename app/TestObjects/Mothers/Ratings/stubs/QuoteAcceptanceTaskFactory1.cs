﻿using Domain.Base.Workflow;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace TestObjects.Mothers.Ratings.stubs
{
    public class QuoteAcceptanceTaskFactory1 : AbstractWorkflowTaskFactory<QuoteAcceptanceKey1>
    {
        public override IWorkflowRoutingMessage Create(QuoteAcceptanceKey1 command)
        {
            return new WorkflowRoutingMessage().AddMessage(new QuoteAcceptanceTask1(null));
        }
    }
}