﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace TestObjects.Mothers.Ratings.stubs
{
    public class QuoteAcceptanceTask1 : WorkflowExecutionMessage
    {
        public QuoteAcceptanceTask1(NullTaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default)
            : base(metadata, messageType)
        {
            
        }
        public QuoteAcceptanceTask1()
            
        {
            
        }
    }
}