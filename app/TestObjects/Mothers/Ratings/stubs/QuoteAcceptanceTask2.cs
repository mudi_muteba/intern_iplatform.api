﻿using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace TestObjects.Mothers.Ratings.stubs
{
    public class QuoteAcceptanceTask2 : WorkflowExecutionMessage
    {
       public QuoteAcceptanceTask2()
        {
            
        }
        public QuoteAcceptanceTask2(WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
        }

        public QuoteAcceptanceTask2(ITaskMetadata metadata, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default)
            : base(metadata, messageType)
        {
        }

        //public void Execute(WorkflowEngine workflowEngine)
        //{
        //    throw new NotImplementedException();
        //}

        //public void Completed()
        //{
        //    throw new NotImplementedException();
        //}

        //public void Started()
        //{
        //    throw new NotImplementedException();
        //}

        //public void Print(int level, IPrintExecutionPlan printer)
        //{
        //    throw new NotImplementedException();
        //}

        //public void WithAudit()
        //{
        //    throw new NotImplementedException();
        //}

        //public IWorkflowMessageTaskStatus Status { get; set; }
        //public IEnumerable<IWorkflowMessageTask> SubTasks { get; set; }
        //public IEnumerable<IWorkflowMessageTask> FailureTasks { get; set; }
        //public IEnumerable<IWorkflowMessageTask> SuccessTasks { get; set; }
        //public ITaskMetadata TaskMetadata { get; set; }
        //public Guid ParentCorrelationId { get; set; }
        //public Guid CorrelationId { get; set; }
    }
}