using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using System;
using TestObjects.Mothers.Ratings.Result;
using TestObjects.Mothers.Router;
using iPlatform.Api.DTOs.Admin;

namespace TestObjects.Mothers.Ratings
{
    public class QuoteAcceptanceDtoObjectMother
    {
        public static PublishQuoteUploadMessageDto ValidQuoteAcceptance()
        {
            return new PublishQuoteUploadMessageDto
            {
                Request = RatingRequestObjectMother.FullRequest(),
                Policy = new RatingResultPolicyDto
                {
                    ProductCode = RouteAcceptedQuoteTaskMother.ForDotsure().ProductCode,
                    InsurerCode = RouteAcceptedQuoteTaskMother.ForDotsure().InsurerCode
                },
                InsuredInfo = new InsuredInfoDto
                {
                    IDNumber = "7908190139086",
                },
                ChannelInfo = new ChannelDto { Id = 1 },
                Environment = "Dev"
            };
        }

        private static InsuredInfoDto GetInsuredInfo(string IDnumber = "")
        {
            return new InsuredInfoDto
            {
                ContactNumber = "0844485820",
                EmailAddress = "donotcall@iplatform.co.za",
                Firstname = "test",
                IDNumber = IDnumber,
                ITCConsent = false,
                MaritalStatus = MaritalStatuses.Married,
                Surname = "donotcall",
                Title = Titles.Magistrate,
                DateOfBirth = new DateTime(1966, 03, 28),
            };
        }
        public static PublishQuoteUploadMessageDto CampaignQuoteAcceptance()
        {
            return new PublishQuoteUploadMessageDto
            {
                InsuredInfo = GetInsuredInfo("6603285990086"),
                Request = RatingRequestObjectMother.FullRequest(),
                Policy = RatingResultPolicyObjectMother.OakhurstRatingResultPolicy
            };
        }
        public static PublishQuoteUploadMessageDto DotsureQuoteAcceptance()
        {
            return new PublishQuoteUploadMessageDto
            {
                InsuredInfo = GetInsuredInfo("6603285990086"),
                Request = RatingRequestObjectMother.SimpleMotorOnlyRequest,
                Policy = RatingResultPolicyObjectMother.OakhurstRatingResultPolicy
            };
        }
        public static PublishQuoteUploadMessageDto HollardQuoteAcceptance()
        {
            return new PublishQuoteUploadMessageDto
            {
                InsuredInfo = GetInsuredInfo(),
                Request = RatingRequestObjectMother.SimpleMultiCoverRequest,
                Policy = RatingResultPolicyObjectMother.HollardRatingResultPolicy
            };
        }
        public static PublishQuoteUploadMessageDto IDSQuoteAcceptance()
        {
            return new PublishQuoteUploadMessageDto
            {
                InsuredInfo = GetInsuredInfo(),
                Request = RatingRequestObjectMother.SimpleMotorOnlyRequest,
                Policy = RatingResultPolicyObjectMother.TelesureRatingResultPolicy
            };
        }
        public static PublishQuoteUploadMessageDto OakhurstQuoteAcceptance()
        {
            return new PublishQuoteUploadMessageDto
            {
                InsuredInfo = GetInsuredInfo(),
                Request = RatingRequestObjectMother.SimpleMotorOnlyRequest,
                Policy = RatingResultPolicyObjectMother.OakhurstRatingResultPolicy
            };
        }
        public static PublishQuoteUploadMessageDto RegentQuoteAcceptance()
        {
            return new PublishQuoteUploadMessageDto
            {
                InsuredInfo = GetInsuredInfo(),
                Request = RatingRequestObjectMother.SimpleMultiCoverRequest,
                Policy = RatingResultPolicyObjectMother.RenasaRatingResultPolicy
            };
        }
        public static PublishQuoteUploadMessageDto TelesureQuoteAcceptance()
        {
            var dto = new PublishQuoteUploadMessageDto
            {
                InsuredInfo = GetInsuredInfo("8009280774087"),
                Request = RatingRequestObjectMother.SimpleMultiCoverRequest,
                Policy = RatingResultPolicyObjectMother.TelesureRatingResultPolicy
            };
            dto.Request.Id = new Guid("d6d12095-6894-470b-913d-b8598c207634");
            return dto;
        }
        public static PublishQuoteUploadMessageDto SauQuoteAcceptance()
        {
            var dto = new PublishQuoteUploadMessageDto
            {
                InsuredInfo = GetInsuredInfo("8009280774087"),
                Request = RatingRequestObjectMother.SimpleMotorOnlyRequest,
                Policy = RatingResultPolicyObjectMother.SauRatingResultPolicy
            };
            dto.Policy.InsurerReference = "2586820";
            return dto;
        }
        public static PublishQuoteUploadMessageDto KingPrice2QuoteAcceptance()
        {
            var dto = new PublishQuoteUploadMessageDto
            {
                InsuredInfo = GetInsuredInfo("8009280774087"),
                Request = RatingRequestObjectMother.SimpleMotorOnlyRequest,
                Policy = RatingResultPolicyObjectMother.Kingprice2RatingResultPolicy
            };
            dto.Policy.InsurerReference = "2586820";
            return dto;
        }
    }
}