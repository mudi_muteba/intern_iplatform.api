using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using TestObjects.Mothers.Ratings.Request.Builders;
using TestObjects.Mothers.Ratings.Request;

namespace TestObjects.Mothers.Ratings
{
    public class ContentsAndBuildingsRatingRequestObjectMother
    {
        public static RatingRequestDto RatingRequestTestData()
        {
            var ratingRequest = new RatingRequestDto
            {
                RatingContext = { ChannelId = new Guid("2A2339E6-D556-428E-BD0B-9E2ECF1715A5") }
            };
            ratingRequest.Id = Guid.NewGuid();

            ratingRequest.Policy.Persons.Add(new RatingRequestPersonDto()
            {
                Email = "noone@nowhere.com",
                Surname = "Smith",
                FirstName = "John",
                PhoneCell = "0825342233",
                IdNumber = "7908190139086",
                Addresses = new List<RatingRequestAddressDto>()
                {
                    new RatingRequestAddressDto()
                    {
                        PostalCode = "1682"
                    }
                }
            });

            ratingRequest.Items.Add(CoverItem(1));

            return ratingRequest;
        }

        private static RatingRequestItemDto CoverItem(int assetNo)
        {
            return new RatingRequestItemDto()
            {
                AssetNo = assetNo,
                Cover = Covers.Motor,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.RiskAddress,
                                QuestionAnswer = "Unit 13, Villa Toscana, 92 Puttick Ave"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.IDNumber,
                                QuestionAnswer = "0825342233"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SumInsured,
                                QuestionAnswer = "650000"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Suburb,
                                QuestionAnswer = "GREEN POINT"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.PostalCode,
                                QuestionAnswer = ContentsAndBuildingsConstants.PostalCode
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGUsage,
                                QuestionAnswer = QuestionAnswers.AIGUsageMainResidence
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGOwnership,
                                QuestionAnswer = QuestionAnswers.AIGOwnershipYesMortgaged
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGHomeType,
                                QuestionAnswer = QuestionAnswers.AIGHomeTypeMultiStoreyFreeStandingHouse
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGRoofConstruction,
                                QuestionAnswer = QuestionAnswers.AIGRoofConstructionStandard
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGWallConstruction,
                                QuestionAnswer = QuestionAnswers.AIGWallConstructionStandard
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGThatchLapa,
                                QuestionAnswer = QuestionAnswers.AIGThatchLapaNone
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGThatchLapaSize,
                                QuestionAnswer = false
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGLinkedAlarmwitharmedresponse,
                                QuestionAnswer = true
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGPropertyBorder,
                                QuestionAnswer = QuestionAnswers.AIGPropertyBorderVacantland
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGConstructionYear,
                                QuestionAnswer = ContentsAndBuildingsConstants.ConstructionYear
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGSquareMetresOfProperty,
                                QuestionAnswer = ContentsAndBuildingsConstants.SizeOfProperty
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGNumberOfBathrooms,
                                QuestionAnswer = QuestionAnswers.AIGNumberOfBathrooms2
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGPeriodPropertyIsUnoccupied,
                                QuestionAnswer = QuestionAnswers.AIGPeriodPropertyIsUnoccupied0days30days
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGBusiness,
                                QuestionAnswer = QuestionAnswers.AIGBusinessNone
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGAccidentalDamage,
                                QuestionAnswer = QuestionAnswers.AIGAccidentalDamageNone
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGWaterPumpingMachinery,
                                QuestionAnswer = QuestionAnswers.AIGWaterPumpingMachineryNone
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AIGBuildingAdditionalExcess,
                                QuestionAnswer = QuestionAnswers.AIGBuildingAdditionalExcessNone
                            }
                }
            };
        }
       
    }
}