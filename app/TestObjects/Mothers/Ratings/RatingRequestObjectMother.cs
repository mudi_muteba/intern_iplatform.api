using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using TestObjects.Mothers.Ratings.Request.Builders;
using TestObjects.Mothers.Ratings.Request;

namespace TestObjects.Mothers.Ratings
{
    public class RatingRequestObjectMother
    {
        public static RatingRequestDto WithItemsNoAnswers()
        {
            var ratingRequest = new RatingRequestDto();
            ratingRequest.Id = Guid.NewGuid();
            ratingRequest.RatingContext = new RatingContextDto()
            {
                ChannelId = Guid.NewGuid()
            };
            ratingRequest.Items.Add(new RatingRequestItemDto() {});
            ratingRequest.Items.Add(new RatingRequestItemDto() {});

            return ratingRequest;
        }

        public static RatingRequestDto WithItemsWithNoAnswers()
        {
            var ratingRequest = new RatingRequestDto();
            ratingRequest.Id = Guid.NewGuid();
            ratingRequest.RatingContext = new RatingContextDto()
            {
                ChannelId = Guid.NewGuid()
            };

            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 1,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    }
                }
            });
            ratingRequest.Items.Add(new RatingRequestItemDto() {});

            return ratingRequest;
        }

        public static RatingRequestDto WithItemsWithAnswersNoPersons()
        {
            var ratingRequest = new RatingRequestDto();
            ratingRequest.Id = Guid.NewGuid();
            ratingRequest.RatingContext = new RatingContextDto()
            {
                ChannelId = Guid.NewGuid()
            };

            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 1,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    }
                }
            });
            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 2,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    }
                }
            });

            return ratingRequest;
        }

        public static RatingRequestDto WithItemsWithAnswersAndValidPersons()
        {
            var ratingRequest = new RatingRequestDto();
            ratingRequest.Id = Guid.NewGuid();
            ratingRequest.RatingContext = new RatingContextDto()
            {
                ChannelId = new Guid("f05f2384-2524-4531-80ba-8a198fc14ba0")
            };

            ratingRequest.Policy.Persons.Add(new RatingRequestPersonDto()
            {
                Email = "noone@nowhere.com",
                Surname = "Smith",
                FirstName = "John",
                PhoneCell = "0825342233",
                IdNumber = "7908190139086",
                Addresses = new List<RatingRequestAddressDto>()
                {
                    new RatingRequestAddressDto()
                    {
                        PostalCode = "2092"
                    }
                }
            });

            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 1,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleImmobiliser,
                        QuestionAnswer = QuestionAnswers.VehicleImmobiliserFactoryFitted
                    }
                }
            });
            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 2,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.ClassOfUse,
                        QuestionAnswer = QuestionAnswers.ClassOfUseStrictlyPrivate
                    },
                }
            });

            return ratingRequest;
        }

        public static RatingRequestDto WithItemsWithInvalidAnswersAndValidPersons()
        {
            var ratingRequest = new RatingRequestDto();
            ratingRequest.Id = Guid.NewGuid();
            ratingRequest.RatingContext = new RatingContextDto()
            {
                ChannelId = Guid.NewGuid()
            };

            ratingRequest.Policy.Persons.Add(new RatingRequestPersonDto()
            {
                Email = "noone@nowhere.com",
                Surname = "Smith",
                FirstName = "John",
                PhoneCell = "0825342233",
                IdNumber = "7908190139086",
                Addresses = new List<RatingRequestAddressDto>()
                {
                    new RatingRequestAddressDto()
                    {
                        PostalCode = "2092"
                    }
                }
            });

            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 1,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleImmobiliser,
                        QuestionAnswer = "waffle"
                    },
                }
            });
            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 2,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.ClassOfUse,
                        QuestionAnswer = "this is the not the correct answer"
                    },
                }
            });

            return ratingRequest;
        }

        public static RatingRequestDto WithItemsWithAnswersAndEmptyPersons()
        {
            var ratingRequest = new RatingRequestDto();
            ratingRequest.Id = Guid.NewGuid();
            ratingRequest.RatingContext = new RatingContextDto()
            {
                ChannelId = Guid.NewGuid()
            };

            ratingRequest.Policy.Persons.Add(new RatingRequestPersonDto()
            {
            });

            ratingRequest.Policy.Persons.Add(new RatingRequestPersonDto()
            {
            });

            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 1,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    }
                }
            });
            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 2,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    }
                }
            });

            return ratingRequest;
        }

        public static RatingRequestDto WithItemsWithAnswersAndInvalidPersons()
        {
            var ratingRequest = new RatingRequestDto();
            ratingRequest.Id = Guid.NewGuid();
            ratingRequest.RatingContext = new RatingContextDto()
            {
                ChannelId = Guid.NewGuid()
            };

            ratingRequest.Policy.Persons.Add(new RatingRequestPersonDto()
            {
                FirstName = "john"
            });

            ratingRequest.Policy.Persons.Add(new RatingRequestPersonDto()
            {
                Surname = "Smith"
            });

            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 1,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    }
                }
            });
            ratingRequest.Items.Add(new RatingRequestItemDto()
            {
                AssetNo = 2,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2012"
                    }
                }
            });

            return ratingRequest;
        }

        // screw this method up, and I will find you
        // Always code as if the person who ends up maintaining your code is a violent psychopath who knows where you live
        public static RatingRequestDto FullRequest()
        {
            var ratingRequest = new RatingRequestDto
            {
                RatingContext = {ChannelId = new Guid("16F67549-B713-4558-A38D-A399BEE346F0")}
            };
            ratingRequest.Id = Guid.NewGuid();

            ratingRequest.Policy.Persons.Add(new RatingRequestPersonDto()
            {
                Email = "noone@nowhere.com",
                Surname = "Smith",
                FirstName = "John",
                PhoneCell = "0825342233",
                IdNumber = "7908190139086",
                Addresses = new List<RatingRequestAddressDto>()
                {
                    new RatingRequestAddressDto()
                    {
                        PostalCode = "1682"
                    }
                }
            });

            ratingRequest.Items.Add(MotorItem(1));

            return ratingRequest;
        }

        private static RatingRequestItemDto MotorItem(int assetNo)
        {
            return new RatingRequestItemDto()
            {
                AssetNo = assetNo,
                Cover = Covers.Motor,
                Answers = new List<RatingRequestItemAnswerDto>()
                {
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.ClassOfUse,
                        QuestionAnswer = QuestionAnswers.ClassOfUsePrivateIncludingWork
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.TypeOfCoverMotor,
                        QuestionAnswer = QuestionAnswers.TypeOfCoverMotorComprehensive
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.PostalCode,
                        QuestionAnswer = "1682"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.PostalCodeWork,
                        QuestionAnswer = "1682"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.Suburb,
                        QuestionAnswer = "CROWTHORNE"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.Province,
                        QuestionAnswer = QuestionAnswers.ProvinceGauteng
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleMake,
                        QuestionAnswer = "BMW"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleModel,
                        QuestionAnswer = "320 d EXCLUSIVE (E46)"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.YearOfManufacture,
                        QuestionAnswer = "2001"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleMMCode,
                        QuestionAnswer = "05034175"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleType,
                        QuestionAnswer = QuestionAnswers.VehicleTypeSedan
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.RegisteredOwnerIDNumber,
                        QuestionAnswer = "6603285990086"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleImmobiliser,
                        QuestionAnswer = QuestionAnswers.VehicleImmobiliserFactoryFitted
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleTrackingDevice,
                        QuestionAnswer = QuestionAnswers.VehicleTrackingDeviceCarTrackCT1
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleGaraging,
                        QuestionAnswer = QuestionAnswers.VehicleGaragingBehindLockedGates
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleOvernightParking,
                        QuestionAnswer = QuestionAnswers.VehicleOvernightParkingBehindLockedGates
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleDaytimeParking,
                        QuestionAnswer = QuestionAnswers.VehicleDaytimeParkingBehindLockedGates
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.CompulsoryExcess,
                        QuestionAnswer = QuestionAnswers.CompulsoryExcess2500
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VoluntaryExcess,
                        QuestionAnswer = QuestionAnswers.VoluntaryExcess2500
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.DriversLicenceType,
                        QuestionAnswer = QuestionAnswers.DriversLicenceTypeB
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.DriversLicenceFirstIssuedDate,
                        QuestionAnswer = "1985-01-22T00:00:00"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.MainDriverIDNumber,
                        QuestionAnswer = "6603285990086"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.MainDriverDateofBirth,
                        QuestionAnswer = "1966-03-28"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.MainDriverMaritalStatus,
                        QuestionAnswer = QuestionAnswers.MainDriverMaritalStatusMarried
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.MainDriverGender,
                        QuestionAnswer = QuestionAnswers.MainDriverGenderMale
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.NoClaimBonus,
                        QuestionAnswer = QuestionAnswers.NCB7
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.PreviouslyInsured,
                        QuestionAnswer = true
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.ClaimsLast0to12Months,
                        QuestionAnswer = QuestionAnswers.ClaimsLast12Months0
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.ClaimsLast12to24Months,
                        QuestionAnswer = QuestionAnswers.ClaimsLast36Months0
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.ClaimsLast24to36Months,
                        QuestionAnswer = QuestionAnswers.ClaimsLast24Months0
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.CarHire,
                        QuestionAnswer = QuestionAnswers.CarHire10Days
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.SumInsured,
                        QuestionAnswer = "63700"
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.RegisteredOwner,
                        QuestionAnswer = QuestionAnswers.RegisteredOwnerPolicyHolder
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.RelationshipToInsured,
                        QuestionAnswer = QuestionAnswers.RelationshipToInsuredInsured
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleExtrasValue,
                        QuestionAnswer = 0
                    },
                    new RatingRequestItemAnswerDto()
                    {
                        Question = Questions.VehicleExtrasValue,
                        QuestionAnswer = 0
                    },
                }
            };
        }

        public static RatingRequestDto SimpleMotorOnlyRequest
        {
            get { return new RatingRequestBuilder().Build(new SimpleMotorOnlyRequestData()); }
        }

        public static RatingRequestDto SimpleMultiCoverRequest
        {
            get { return new RatingRequestBuilder().Build(new SimpleMuliCoverRequestData()); }
        }
    }
}