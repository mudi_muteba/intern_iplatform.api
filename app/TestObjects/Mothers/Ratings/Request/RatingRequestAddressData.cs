﻿
using MasterData;

namespace TestObjects.Mothers.Ratings.Request
{
    public interface IRatingRequestAddressData
    {
        AddressType AddressType { get; }
        string AddressLine1 { get; }
        string AddressLine2 { get; }
        string AddressLine3 { get; }
        string AddressLine4 { get; }
        string Suburb { get; }
        string City { get; }
        string PostalCode { get; }
        bool DefaultAddress { get; }
    }

    public class Address1 : IRatingRequestAddressData
    {
        public AddressType AddressType
        {
            get { return AddressTypes.PhysicalAddress; }
        }

        public string AddressLine1
        {
            get { return "1 Fritz Sonnenberg Road"; }
        }

        public string AddressLine2
        {
            get { return string.Empty; }
        }

        public string AddressLine3
        {
            get { return string.Empty; }
        }

        public string AddressLine4
        {
            get { return string.Empty; }
        }

        public string Suburb
        {
            get { return "CROWTHORNE"; }
        }

        public string City
        {
            get { return "CROWTHORNE"; }
        }

        public string PostalCode
        {
            get { return "1682"; }
        }

        public bool DefaultAddress
        {
            get { return true; }
        }
    }
}
