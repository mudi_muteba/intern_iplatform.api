using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;

namespace TestObjects.Mothers.Ratings.Request
{
    public interface IRatingRequestData
    {
        Guid Id { get; }
        IList<RatingRequestItemDto> Items { get; }
        RatingRequestPolicyDto Policy { get; }
    }

    public class SimpleMotorOnlyRequestData : IRatingRequestData
    {
        public Guid Id
        {
            get { return new Guid("265A0BAF-8F9E-4E9C-A649-FB44468EF76A"); }
        }

        public IList<RatingRequestItemDto> Items
        {
            get
            {
                return new List<RatingRequestItemDto>
                    {
                        RatingRequestItemObjectMother.MotorItem
                    };
            }
        }

        public RatingRequestPolicyDto Policy
        {
            get { return RatingRequestPolicyObjectMother.Policy1; }
        }
    }

    public class SimpleMuliCoverRequestData : IRatingRequestData
    {
        public Guid Id
        {
            get { return new Guid("265A0BAF-8F9E-4E9C-A649-FB44468EF76A"); }
        }

        public IList<RatingRequestItemDto> Items
        {
            get
            {
                return new List<RatingRequestItemDto>
                    {
                        RatingRequestItemObjectMother.MotorItem,
                        RatingRequestItemObjectMother.MotorItem2,
                        RatingRequestItemObjectMother.ContentItem,
                        RatingRequestItemObjectMother.BuildingItem,
                        RatingRequestItemObjectMother.AllRiskItem
                    };
            }
        }

        public RatingRequestPolicyDto Policy
        {
            get { return RatingRequestPolicyObjectMother.Policy1; }
        }
    }
}