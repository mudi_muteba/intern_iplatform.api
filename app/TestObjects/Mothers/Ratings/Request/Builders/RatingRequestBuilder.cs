using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;

namespace TestObjects.Mothers.Ratings.Request.Builders
{
    public class RatingRequestBuilder
    {
        private RatingRequestPolicyDto policy;
        private Guid id;
        private readonly List<RatingRequestItemDto> items = new List<RatingRequestItemDto>();

        public RatingRequestDto Build()
        {
            return new RatingRequestDto
            {
                Policy = policy,
                Items = items,
                Id = id
            };
        }

        public RatingRequestDto Build(IRatingRequestData data)
        {
            return WithId(data.Id)
                .WithPolicy(data.Policy)
                .WithItems(data.Items)
                .Build();
        }

        public RatingRequestBuilder WithId(Guid id)
        {
            this.id = id;
            return this;
        }

        public RatingRequestBuilder WithPolicy(Action<IRatingRequestPolicyBuilder> action)
        {
            var builder = new RatingRequestPolicyBuilder();
            action(builder);

            policy = builder.Build();

            return this;
        }

        public RatingRequestBuilder WithPolicy(RatingRequestPolicyDto policy)
        {
            this.policy = policy;
            return this;
        }

        public RatingRequestBuilder WithItem(Action<IRatingRequestItemBuilder> action)
        {
            var builder = new RatingRequestItemBuilder();

            action(builder);

            items.Add(builder.Build());

            return this;
        }

        public RatingRequestBuilder WithItems(IList<RatingRequestItemDto> items)
        {
            this.items.AddRange(items);
            return this;
        }
    }
}