using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace TestObjects.Mothers.Ratings.Request.Builders
{
    public interface IRatingRequestPolicyBuilder
    {
        RatingRequestPolicyDto Build();
        RatingRequestPolicyDto Build(IRatingRequestPolicyData data);
        IRatingRequestPolicyBuilder WithPaymentPlan(PaymentPlan frequency);
        IRatingRequestPolicyBuilder WithPerson(Action<IRatingRequestPersonBuilder> action);
        IRatingRequestPolicyBuilder WithPersons(IList<RatingRequestPersonDto> persons);
    }

    public class RatingRequestPolicyBuilder : IRatingRequestPolicyBuilder
    {
        private PaymentPlan paymentPlan = PaymentPlans.Monthly;
        private readonly List<RatingRequestPersonDto> persons = new List<RatingRequestPersonDto>();

        public RatingRequestPolicyDto Build()
        {
            return new RatingRequestPolicyDto
            {
                PaymentPlan = paymentPlan,
                Persons = persons,
            };
        }

        public RatingRequestPolicyDto Build(IRatingRequestPolicyData data)
        {
            return WithPaymentPlan(data.PaymentPlan)
                .WithPersons(data.Persons)
                .Build();
        }

        public IRatingRequestPolicyBuilder WithPaymentPlan(PaymentPlan paymentPlan)
        {
            this.paymentPlan = paymentPlan;
            return this;
        }

        public IRatingRequestPolicyBuilder WithPerson(Action<IRatingRequestPersonBuilder> action)
        {
            var builder = new RatingRequestPersonBuilder();

            action(builder);

            persons.Add(builder.Build());

            return this;
        }

        public IRatingRequestPolicyBuilder WithPersons(IList<RatingRequestPersonDto> persons)
        {
            this.persons.AddRange(persons);
            return this;
        }
    }
}
