using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace TestObjects.Mothers.Ratings.Request.Builders
{
    public interface IRatingRequestAddressBuilder
    {
        RatingRequestAddressDto Build();
        RatingRequestAddressDto Build(IRatingRequestAddressData data);
        IRatingRequestAddressBuilder WithAddressType(AddressType addressType);
        IRatingRequestAddressBuilder WithAddress1(string address1);
        IRatingRequestAddressBuilder WithAddress2(string address2);
        IRatingRequestAddressBuilder WithAddress3(string address3);
        IRatingRequestAddressBuilder WithAddress4(string address4);
        IRatingRequestAddressBuilder WithCity(string city);
        IRatingRequestAddressBuilder WithSuburb(string suburb);
        IRatingRequestAddressBuilder WithPostalCode(string postalCode);
        IRatingRequestAddressBuilder WithDefaultAddress(bool defaultAddress);
    }

    public class RatingRequestAddressBuilder : IRatingRequestAddressBuilder
    {
        private AddressType addressType = AddressTypes.PhysicalAddress; // Default to Physical
        private string address1;
        private string address2;
        private string address3;
        private string address4;
        private string city;
        private string suburb;
        private string postalCode;
        private bool defaultAddress;

        public RatingRequestAddressDto Build()
        {
            return new RatingRequestAddressDto
            {
                AddressType = addressType,
                Address1 = address1,
                Address2 = address2,
                Address3 = address3,
                Address4 = address4,
                City = city,
                Suburb = suburb,
                PostalCode = postalCode,
                DefaultAddress = defaultAddress
            };
        }

        public RatingRequestAddressDto Build(IRatingRequestAddressData data)
        {
            return WithAddressType(data.AddressType)
                .WithAddress1(data.AddressLine1)
                .WithAddress2(data.AddressLine2)
                .WithAddress3(data.AddressLine3)
                .WithAddress4(data.AddressLine4)
                .WithCity(data.City)
                .WithSuburb(data.Suburb)
                .WithPostalCode(data.PostalCode)
                .WithDefaultAddress(data.DefaultAddress)
                .Build();
        }

        public IRatingRequestAddressBuilder WithAddressType(AddressType addressType)
        {
            this.addressType = addressType;
            return this;
        }

        public IRatingRequestAddressBuilder WithAddress1(string address1)
        {
            this.address1 = address1;
            return this;
        }

        public IRatingRequestAddressBuilder WithAddress2(string address2)
        {
            this.address2 = address2;
            return this;
        }

        public IRatingRequestAddressBuilder WithAddress3(string address3)
        {
            this.address3 = address3;
            return this;
        }

        public IRatingRequestAddressBuilder WithAddress4(string address4)
        {
            this.address4 = address4;
            return this;
        }

        public IRatingRequestAddressBuilder WithCity(string city)
        {
            this.city = city;
            return this;
        }

        public IRatingRequestAddressBuilder WithSuburb(string suburb)
        {
            this.suburb = suburb;
            return this;
        }

        public IRatingRequestAddressBuilder WithPostalCode(string postalCode)
        {
            this.postalCode = postalCode;
            return this;
        }

        public IRatingRequestAddressBuilder WithDefaultAddress(bool defaultAddress)
        {
            this.defaultAddress = defaultAddress;
            return this;
        }
    }
}