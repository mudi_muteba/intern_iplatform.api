using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace TestObjects.Mothers.Ratings.Request.Builders
{
    public interface IRatingRequestPersonBuilder
    {
        RatingRequestPersonDto Build();
        RatingRequestPersonDto Build(IRatingRequestPersonData data);
        IRatingRequestPersonBuilder WithSurname(string surname);
        IRatingRequestPersonBuilder WithInitials(string initials);
        IRatingRequestPersonBuilder WithMaritalStatus(MaritalStatus maritalStatus);
        IRatingRequestPersonBuilder WithIdNumber(string idNumber);
        IRatingRequestPersonBuilder WithTitle(Title title);
        IRatingRequestPersonBuilder WithLanguage(Language language);
        IRatingRequestPersonBuilder WithPhoneCell(string phonecell);
        IRatingRequestPersonBuilder WithEmail(string email);
        IRatingRequestPersonBuilder WithPolicyHolder(bool policyHolder);
        IRatingRequestPersonBuilder WithAddress(Action<IRatingRequestAddressBuilder> action);
        IRatingRequestPersonBuilder WithAddresses(IList<RatingRequestAddressDto> addresses);
    }

    public class RatingRequestPersonBuilder : IRatingRequestPersonBuilder
    {
        private string surname;
        private string initials;
        private MaritalStatus maritalStatus;
        private string idNumber;
        private Title title;
        private Language language;
        private bool policyHolder;
        private string phonecell;
        private string email;
        private readonly List<RatingRequestAddressDto> addresses = new List<RatingRequestAddressDto>();

        public RatingRequestPersonDto Build()
        {
            return new RatingRequestPersonDto
            {
                Surname = surname,
                Initials = initials,
                MaritalStatus = maritalStatus,
                IdNumber = idNumber,
                Title = title,
                Language = language,
                PolicyHolder = policyHolder,
                Addresses = addresses,
                PhoneCell = phonecell,
                Email = email,
            };
        }

        public RatingRequestPersonDto Build(IRatingRequestPersonData data)
        {
            return WithSurname(data.Surname)
                    .WithInitials(data.Initials)
                    .WithMaritalStatus(data.MaritalStatus)
                    .WithIdNumber(data.IdNumber)
                    .WithTitle(data.Title)
                    .WithLanguage(data.Language)
                    .WithPolicyHolder(data.PolicyHolder)
                    .WithEmail(data.Email)
                    .WithPhoneCell(data.PhoneCell)
                    .WithAddresses(data.Addresses)
                    .Build();
        }

        public IRatingRequestPersonBuilder WithSurname(string surname)
        {
            this.surname = surname;
            return this;
        }

        public IRatingRequestPersonBuilder WithInitials(string initials)
        {
            this.initials = initials;
            return this;
        }

        public IRatingRequestPersonBuilder WithMaritalStatus(MaritalStatus maritalStatus)
        {
            this.maritalStatus = maritalStatus;
            return this;
        }

        public IRatingRequestPersonBuilder WithIdNumber(string idNumber)
        {
            this.idNumber = idNumber;
            return this;
        }

        public IRatingRequestPersonBuilder WithTitle(Title title)
        {
            this.title = title;
            return this;
        }

        public IRatingRequestPersonBuilder WithLanguage(Language language)
        {
            this.language = language;
            return this;
        }

        public IRatingRequestPersonBuilder WithPolicyHolder(bool policyHolder)
        {
            this.policyHolder = policyHolder;
            return this;
        }
        public IRatingRequestPersonBuilder WithPhoneCell(string phonecell)
        {
            this.phonecell = phonecell;
            return this;
        }
        public IRatingRequestPersonBuilder WithEmail(string email)
        {
            this.email = email;
            return this;
        }

        public IRatingRequestPersonBuilder WithAddress(Action<IRatingRequestAddressBuilder> action)
        {
            var builder = new RatingRequestAddressBuilder();

            action(builder);

            addresses.Add(builder.Build());

            return this;
        }

        public IRatingRequestPersonBuilder WithAddresses(IList<RatingRequestAddressDto> addresses)
        {
            this.addresses.AddRange(addresses);
            return this;
        }
    }
}
