using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace TestObjects.Mothers.Ratings.Request.Builders
{
    public interface IRatingRequestAnswerBuilder
    {
        RatingRequestItemAnswerDto Build();
        IRatingRequestAnswerBuilder WithQuestion(Question question);
        IRatingRequestAnswerBuilder WithAnswer(object answer);
    }

    internal class RatingRequestAnswerBuilder : IRatingRequestAnswerBuilder
    {
        private Question question;
        private object answer;

        public RatingRequestItemAnswerDto Build()
        {
            return new RatingRequestItemAnswerDto
            {
                Question = question,
                QuestionAnswer = answer
            };
        }

        public IRatingRequestAnswerBuilder WithQuestion(Question question)
        {
            this.question = question;
            return this;
        }

        public IRatingRequestAnswerBuilder WithAnswer(object answer)
        {
            this.answer = answer;
            return this;
        }
    }
}
