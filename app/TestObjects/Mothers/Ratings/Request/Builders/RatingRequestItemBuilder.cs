using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace TestObjects.Mothers.Ratings.Request.Builders
{
    public interface IRatingRequestItemBuilder
    {
        RatingRequestItemDto Build();
        RatingRequestItemDto Build(IRatingRequestItemData data);
        IRatingRequestItemBuilder WithCover(Cover cover);
        IRatingRequestItemBuilder WithAssetNo(int assetNo);
        IRatingRequestItemBuilder WithParentAssetNo(int parentAssetNo);
        IRatingRequestItemBuilder WithPremiumIncluded(bool premiumIncluded);
        IRatingRequestItemBuilder WithRatingDate(DateTime ratingDate);
        IRatingRequestItemBuilder WithAnswer(Action<IRatingRequestAnswerBuilder> action);
        IRatingRequestItemBuilder WithAnswers(IList<RatingRequestItemAnswerDto> answers);
    }

    public class RatingRequestItemBuilder : IRatingRequestItemBuilder
    {
        private Cover cover;
        private int assetNo;
        private int parentAssetNo;
        private bool premiumIncluded;
        private DateTime ratingDate = new DateTime(1900, 1, 1);
        private readonly List<RatingRequestItemAnswerDto> answers = new List<RatingRequestItemAnswerDto>();

        public RatingRequestItemDto Build()
        {
            return new RatingRequestItemDto
            {
                Cover = cover,
                AssetNo = assetNo,
                ParentAssetNo = parentAssetNo,
                PremiumIncluded = premiumIncluded,
                RatingDate = ratingDate,
                Answers = answers
            };
        }

        public RatingRequestItemDto Build(IRatingRequestItemData data)
        {
            return WithCover(data.Cover)
                .WithAssetNo(data.AssetNo)
                .WithParentAssetNo(data.ParentAssetNo)
                .WithPremiumIncluded(data.PremiumIncluded)
                .WithRatingDate(data.RatingDate)
                .WithAnswers(data.Answers)
                .Build();
        }

        public IRatingRequestItemBuilder WithCover(Cover coverCode)
        {
            cover = coverCode;
            return this;
        }

        public IRatingRequestItemBuilder WithAssetNo(int assetNo)
        {
            this.assetNo = assetNo;
            return this;
        }

        public IRatingRequestItemBuilder WithParentAssetNo(int parentAssetNo)
        {
            this.parentAssetNo = parentAssetNo;
            return this;
        }

        public IRatingRequestItemBuilder WithPremiumIncluded(bool premiumIncluded)
        {
            this.premiumIncluded = premiumIncluded;
            return this;
        }

        public IRatingRequestItemBuilder WithRatingDate(DateTime ratingDate)
        {
            this.ratingDate = ratingDate;
            return this;
        }

        public IRatingRequestItemBuilder WithAnswer(Action<IRatingRequestAnswerBuilder> action)
        {
            var builder = new RatingRequestAnswerBuilder();

            action(builder);

            answers.Add(builder.Build());

            return this;
        }

        public IRatingRequestItemBuilder WithAnswers(IList<RatingRequestItemAnswerDto> answers)
        {
            this.answers.AddRange(answers);
            return this;
        }
    }
}
