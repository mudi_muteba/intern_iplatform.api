using iPlatform.Api.DTOs.Ratings.Request;
using TestObjects.Mothers.Ratings.Request.Builders;

namespace TestObjects.Mothers.Ratings.Request
{
    public class RatingRequestItemObjectMother
    {
        public static RatingRequestItemBuilder Builder
        {
            get { return new RatingRequestItemBuilder(); }
        }

        public static IRatingRequestItemData MotorItemData
        {
            get { return new MotorItemData(); }
        }

        public static RatingRequestItemDto MotorItem
        {
            get { return Builder.Build(MotorItemData); }
        }
        public static RatingRequestItemDto MotorItem2
        {
            get { return Builder.Build(new MotorItem2Data()); }
        }

        public static IRatingRequestItemData ContentItemData
        {
            get { return new ContentItemData(); }
        }

        public static RatingRequestItemDto ContentItem
        {
            get { return Builder.Build(ContentItemData); }
        }

        public static IRatingRequestItemData BuildingItemData
        {
            get { return new BuildingItemData(); }
        }

        public static RatingRequestItemDto BuildingItem
        {
            get { return Builder.Build(BuildingItemData); }
        }

        public static IRatingRequestItemData AllRiskItemData
        {
            get { return new AllRiskItemData(); }
        }

        public static RatingRequestItemDto AllRiskItem
        {
            get { return Builder.Build(AllRiskItemData); }
        }
    }
}