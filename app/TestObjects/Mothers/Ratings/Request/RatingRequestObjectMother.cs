using iPlatform.Api.DTOs.Ratings.Request;
using TestObjects.Mothers.Ratings.Request.Builders;

namespace TestObjects.Mothers.Ratings.Request
{
    public class RatingRequestObjectMother
    {
        public static RatingRequestBuilder Builder
        {
            get { return new RatingRequestBuilder(); }
        }

        public static IRatingRequestData SimpleMotorOnlyRequestData
        {
            get { return new SimpleMotorOnlyRequestData(); }
        }

        public static RatingRequestDto SimpleMotorOnlyRequest
        {
            get { return Builder.Build(SimpleMotorOnlyRequestData); }
        } 
        
        public static IRatingRequestData SimpleMultiCoverRequestData
        {
            get { return new SimpleMuliCoverRequestData(); }
        }

        public static RatingRequestDto SimpleMultiCoverRequest
        {
            get { return Builder.Build(SimpleMultiCoverRequestData); }
        }
    }
}