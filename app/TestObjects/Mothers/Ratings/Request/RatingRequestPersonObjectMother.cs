﻿using iPlatform.Api.DTOs.Ratings.Request;
using TestObjects.Mothers.Ratings.Request.Builders;

namespace TestObjects.Mothers.Ratings.Request
{
    public class RatingRequestPersonObjectMother
    {
        public static RatingRequestPersonBuilder Builder
        {
            get { return new RatingRequestPersonBuilder(); }
        }

        public static IRatingRequestPersonData Person1Data
        {
            get { return new Person1(); }
        }

        public static RatingRequestPersonDto Person1
        {
            get { return Builder.Build(Person1Data); }
        }
    }
}
