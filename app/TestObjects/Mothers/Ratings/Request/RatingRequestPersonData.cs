﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace TestObjects.Mothers.Ratings.Request
{
    public interface IRatingRequestPersonData
    {
        string Surname { get; }
        string Initials { get; }
        MaritalStatus MaritalStatus { get; }
        string IdNumber { get; }
        Title Title { get; }
        Language Language { get; }
        bool PolicyHolder { get; }
        List<RatingRequestAddressDto> Addresses { get; }
        string PhoneCell { get; }
        string Email { get; }
    }

    public class Person1 : IRatingRequestPersonData
    {
        public string Initials
        {
            get { return "D"; }
        }

        public string Surname
        {
            get { return "DONOTCALL"; }
        }

        public Title Title
        {
            get { return Titles.Mr; }
        }

        public Language Language
        {
            get { return Languages.English; }
        }

        public bool PolicyHolder
        {
            get { return true; }
        }

        public List<RatingRequestAddressDto> Addresses
        {
            get { return new List<RatingRequestAddressDto> { RatingRequestAddressObjectMother.Address1 }; }
        }

        public MaritalStatus MaritalStatus
        {
            get { return MaritalStatuses.Married; }
        }

        public string IdNumber
        {
            get { return "6603285990086"; }
        }

        public string Occupation
        {
            get { return "Banker"; }
        }

        public string PhoneCell
        {
            get { return "0844485820"; }
        }

        public string Email
        {
            get { return "donotcall@iplatform.co.za"; }
        }
    }
}
