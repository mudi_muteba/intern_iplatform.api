using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;

namespace TestObjects.Mothers.Ratings.Request
{
    public class RatingRequestAnswerObjectMother
    {
        public static IRatingRequestAnswerData MotorAnswerData
        {
            get { return new MotorAnswerData(); }
        }

        public static IList<RatingRequestItemAnswerDto> MotorAnswers
        {
            get { return MotorAnswerData.Answers; }
        }

        public static IRatingRequestAnswerData ContentAnswerData
        {
            get { return new ContentAnswerData(); }
        }

        public static IList<RatingRequestItemAnswerDto> ContentAnswers
        {
            get { return ContentAnswerData.Answers; }
        }

        public static IRatingRequestAnswerData BuildingAnswerData
        {
            get { return new BuildingAnswerData(); }
        }

        public static IList<RatingRequestItemAnswerDto> BuildingAnswers
        {
            get { return BuildingAnswerData.Answers; }
        }

        public static IRatingRequestAnswerData AllRiskAnswerData
        {
            get { return new AllRiskAnswerData(); }
        }

        public static IList<RatingRequestItemAnswerDto> AllRiskAnswers
        {
            get { return AllRiskAnswerData.Answers; }
        }
    }
}