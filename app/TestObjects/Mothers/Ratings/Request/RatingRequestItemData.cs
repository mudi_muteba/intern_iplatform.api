using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace TestObjects.Mothers.Ratings.Request
{
    public interface IRatingRequestItemData
    {
        Cover Cover { get; }
        int AssetNo { get; }
        int ParentAssetNo { get; }
        IList<RatingRequestItemAnswerDto> Answers { get; }
        bool PremiumIncluded { get; }
        DateTime RatingDate { get; }
    }

    public class MotorItemData : IRatingRequestItemData
    {
        public Cover Cover
        {
            get { return Covers.Motor; }
        }

        public int AssetNo
        {
            get { return 2185; }
        }

        public int ParentAssetNo
        {
            get { return 2185; }
        }

        public IList<RatingRequestItemAnswerDto> Answers
        {
            get { return RatingRequestAnswerObjectMother.MotorAnswers; }
        }

        public bool PremiumIncluded
        {
            get { return false; }
        }

        public DateTime RatingDate
        {
            get { return DateTime.UtcNow; }
        }
    }
    public class MotorItem2Data : IRatingRequestItemData
    {
        public Cover Cover
        {
            get { return Covers.Motor; }
        }

        public int AssetNo
        {
            get { return 7; }
        }

        public int ParentAssetNo
        {
            get { return 7; }
        }

        public IList<RatingRequestItemAnswerDto> Answers
        {
            get { return RatingRequestAnswerObjectMother.MotorAnswers; }
        }

        public bool PremiumIncluded
        {
            get { return false; }
        }

        public DateTime RatingDate
        {
            get { return DateTime.UtcNow; }
        }
    }

    public class ContentItemData : IRatingRequestItemData
    {
        public Cover Cover
        {
            get { return Covers.Contents; }
        }

        public int AssetNo
        {
            get { return 2; }
        }

        public int ParentAssetNo
        {
            get { return 2; }
        }

        public IList<RatingRequestItemAnswerDto> Answers
        {
            get { return RatingRequestAnswerObjectMother.ContentAnswers; }
        }

        public bool PremiumIncluded
        {
            get { return false; }
        }

        public DateTime RatingDate
        {
            get { return DateTime.UtcNow; }
        }
    }

    public class BuildingItemData : IRatingRequestItemData
    {
        public Cover Cover
        {
            get { return Covers.Building; }
        }

        public int AssetNo
        {
            get { return 3; }
        }

        public int ParentAssetNo
        {
            get { return 3; }
        }

        public IList<RatingRequestItemAnswerDto> Answers
        {
            get { return RatingRequestAnswerObjectMother.BuildingAnswers; }
        }

        public bool PremiumIncluded
        {
            get { return false; }
        }

        public DateTime RatingDate
        {
            get { return DateTime.UtcNow; }
        }
    }

    public class AllRiskItemData : IRatingRequestItemData
    {
        public Cover Cover
        {
            get { return Covers.AllRisk; }
        }

        public int AssetNo
        {
            get { return 4; }
        }

        public int ParentAssetNo
        {
            get { return 4; }
        }

        public IList<RatingRequestItemAnswerDto> Answers
        {
            get { return RatingRequestAnswerObjectMother.AllRiskAnswers; }
        }

        public bool PremiumIncluded
        {
            get { return false; }
        }

        public DateTime RatingDate
        {
            get { return DateTime.UtcNow; }
        }
    }
}