﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace TestObjects.Mothers.Ratings.Request
{
    public interface IRatingRequestPolicyData
    {
        PaymentPlan PaymentPlan { get; }
        List<RatingRequestPersonDto> Persons { get; }
    }

    public class Policy1 : IRatingRequestPolicyData
    {
        public PaymentPlan PaymentPlan
        {
            get { return PaymentPlans.Monthly; }
        }

        public List<RatingRequestPersonDto> Persons
        {
            get { return new List<RatingRequestPersonDto>{ RatingRequestPersonObjectMother.Person1 }; }
        }
    }
}
