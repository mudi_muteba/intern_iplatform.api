﻿using iPlatform.Api.DTOs.Ratings.Request;
using TestObjects.Mothers.Ratings.Request.Builders;

namespace TestObjects.Mothers.Ratings.Request
{
    public class RatingRequestAddressObjectMother
    {
        public static RatingRequestAddressBuilder Builder
        {
            get { return new RatingRequestAddressBuilder(); }
        }

        public static IRatingRequestAddressData Address1Data
        {
            get { return new Address1(); }
        }

        public static RatingRequestAddressDto Address1
        {
            get { return Builder.Build(Address1Data); }
        }
    }
}
