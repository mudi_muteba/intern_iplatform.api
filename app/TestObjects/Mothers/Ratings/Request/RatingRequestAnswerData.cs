using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace TestObjects.Mothers.Ratings.Request
{
    public interface IRatingRequestAnswerData
    {
        IList<RatingRequestItemAnswerDto> Answers { get; }
    }

    public class MotorAnswerData : IRatingRequestAnswerData
    {
        public IList<RatingRequestItemAnswerDto> Answers
        {
            get
            {
                return new List<RatingRequestItemAnswerDto>
                    {
                        // General Info
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClassOfUse,
                                QuestionAnswer = QuestionAnswers.ClassOfUsePrivateIncludingWork
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.TypeOfCoverMotor,
                                QuestionAnswer = QuestionAnswers.TypeOfCoverMotorComprehensive
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.PostalCode,
                                QuestionAnswer = "1682"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.PostalCodeWork,
                                QuestionAnswer = "1682"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Suburb, // Can cause Hollard Lookup Error: Invalid Suburb ID 
                                QuestionAnswer = "CROWTHORNE"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Province,
                                QuestionAnswer = QuestionAnswers.ProvinceGauteng
                            },

                        // Risk Info
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleMake,
                                QuestionAnswer = "BMW"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleModel,
                                QuestionAnswer = "320 d EXCLUSIVE (E46)"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.YearOfManufacture,
                                QuestionAnswer = "2001",
                                //uestion.QuestionType = Domain.Models.QuestionTypes.Textbox
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleMMCode,
                                QuestionAnswer = "05034175"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleType,
                                QuestionAnswer = QuestionAnswers.VehicleTypeSedan
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.RegisteredOwnerIDNumber,
                                QuestionAnswer = "6603285990086"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleRegistrationNumber,
                                QuestionAnswer = "CXS050MP"
                            },

                        // Security Info
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleImmobiliser,
                                QuestionAnswer = QuestionAnswers.VehicleImmobiliserFactoryFitted
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleTrackingDevice,
                                QuestionAnswer = QuestionAnswers.VehicleTrackingDeviceCarTrackQuickCT1
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleGaraging,
                                QuestionAnswer = QuestionAnswers.VehicleGaragingBehindLockedGates
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleOvernightParking,
                                QuestionAnswer = QuestionAnswers.VehicleOvernightParkingBehindLockedGates
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleDaytimeParking,
                                QuestionAnswer = QuestionAnswers.VehicleDaytimeParkingBehindLockedGates
                            },
                        
                        // Additional Options
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.CompulsoryExcess,
                                QuestionAnswer = QuestionAnswers.CompulsoryExcess2500
                            },

                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VoluntaryExcess,
                                QuestionAnswer = QuestionAnswers.VoluntaryExcess3000
                            },

                        // Driver Info
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.DriversLicenceType,
                                QuestionAnswer = QuestionAnswers.DriversLicenceTypeB
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.DriversLicenceFirstIssuedDate,
                                QuestionAnswer = new DateTime(1978, 01, 22)
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.MainDriverIDNumber, // Currently required for derived questions (DOB, Age, Gender)
                                QuestionAnswer = "6603285990086"
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.MainDriverDateofBirth, 
                                QuestionAnswer = "1977-11-28"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.MainDriverMaritalStatus,
                                QuestionAnswer = QuestionAnswers.MainDriverMaritalStatusMarried
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.MainDriverGender,
                                QuestionAnswer = QuestionAnswers.MainDriverGenderMale
                            },
                        // Insurance History
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.NoClaimBonus,
                                QuestionAnswer = QuestionAnswers.NCB7
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.PreviouslyInsured,
                                QuestionAnswer = true
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast0to12Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast12Months0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast12to24Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast24Months0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast24to36Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast36Months0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.CarHire,
                                QuestionAnswer = QuestionAnswers.CarHire10Days
                            },
                        // Finance Info
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SumInsured,
                                QuestionAnswer = "63700"
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.RegisteredOwner,
                                QuestionAnswer = QuestionAnswers.RegisteredOwnerPolicyHolder
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.RelationshipToInsured,
                                QuestionAnswer = QuestionAnswers.RelationshipToInsuredInsured
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VehicleExtrasValue,
                                QuestionAnswer = 0
                            },
                    };
            }
        }
    }

    public class ContentAnswerData : IRatingRequestAnswerData
    {
        public IList<RatingRequestItemAnswerDto> Answers
        {
            get
            {
                return new List<RatingRequestItemAnswerDto>
                    {
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.RiskAddress,
                                QuestionAnswer = "Unit 13, Villa Toscana, 92 Puttick Ave"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.IDNumber,
                                QuestionAnswer = ""
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.MainDriverDateofBirth,
                                QuestionAnswer = "1977-11-28"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SumInsured,
                                QuestionAnswer = "650000"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Suburb,
                                QuestionAnswer = "GREEN POINT"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.PostalCode,
                                QuestionAnswer = "8005"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AlarmType,
                                QuestionAnswer = QuestionAnswers.AlarmTypeLinkedAlarm
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.BurglarAlarmLinkedTo24HrArmedResponse,
                                QuestionAnswer = true
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ResidenceIs,
                                QuestionAnswer = QuestionAnswers.ResidenceIsMainResidence
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.TypeOfResidence,
                                QuestionAnswer = QuestionAnswers.TypeOfResidenceDetachedHouseCottage
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.BurglarBars,
                                QuestionAnswer = true
                            }, 
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SecurityGate,
                                QuestionAnswer = true
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SecureComplex,
                                QuestionAnswer = true
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ElectricFencing,
                                QuestionAnswer = true
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.InsuredAge,
                                QuestionAnswer = 35
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ExcludeBurglary,
                                QuestionAnswer = false
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SubsidenceandLandslip,
                                QuestionAnswer = false
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.WallConstruction,
                                QuestionAnswer = QuestionAnswers.WallConstructionStandard
                            },
                        new RatingRequestItemAnswerDto
                            {   
                                Question = Questions.RoofConstruction,
                                QuestionAnswer = QuestionAnswers.RoofConstructionStandard
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Unoccupied,
                                QuestionAnswer = QuestionAnswers.UnoccupiedDuringWorkHours
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.NoClaimBonus,
                                QuestionAnswer = QuestionAnswers.NCB5
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.CompulsoryExcess,
                                QuestionAnswer = QuestionAnswers.CompulsoryExcess0
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VoluntaryExcess,
                                QuestionAnswer = QuestionAnswers.VoluntaryExcess0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AccidentalDamage,
                                QuestionAnswer = QuestionAnswers.AccidentalDamage5000
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ExcludeFlood,
                                QuestionAnswer = false
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.HolidayHome,
                                QuestionAnswer = false
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.BurglarBarsonPassageSide,
                                QuestionAnswer = false
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SecurityGateonPassageSide,
                                QuestionAnswer = false
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ThatchSafe,
                                QuestionAnswer = false
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SituationOfResidence,
                                QuestionAnswer = QuestionAnswers.SituationOfResidenceSecurityVillage
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ResidenceUse,
                                QuestionAnswer = QuestionAnswers.ResidenceUseStandard
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.HomeIndustry,
                                QuestionAnswer = QuestionAnswers.HomeIndustryHealthcare
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.HomeIndustrySumInsured,
                                QuestionAnswer = 1000
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.LimitedBedandBreakfast,
                                QuestionAnswer = false
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Province,
                                QuestionAnswer = QuestionAnswers.ProvinceGauteng
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast0to12Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast12Months0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast12to24Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast24Months0
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast24to36Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast36Months0
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.OccupationDate,
                                QuestionAnswer = "2011-01-01"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.RoofGradient,
                                QuestionAnswer = QuestionAnswers.RoofGradientGradient
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.CashBack,
                                QuestionAnswer = false
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Retired,
                                QuestionAnswer = false
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.GuestHouse,
                                QuestionAnswer = false
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.TypeOfCoverHome,
                                QuestionAnswer = QuestionAnswers.TypeOfCoverHomeComprehensive
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.MainDriverMaritalStatus,
                                QuestionAnswer = QuestionAnswers.MainDriverMaritalStatusMarried
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.TypeOfCoverHome,
                                QuestionAnswer = QuestionAnswers.TypeOfCoverHomeComprehensive
                            }
                    };
            }
        }
    }

    public class BuildingAnswerData : IRatingRequestAnswerData
    {
        public IList<RatingRequestItemAnswerDto> Answers
        {
            get
            {
                return new List<RatingRequestItemAnswerDto>
                    {
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.RiskAddress,
                                QuestionAnswer = "Unit 13, Villa Toscana, 92 Puttick Ave"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.IDNumber,
                                QuestionAnswer = "6603285990086"
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.MainDriverDateofBirth,
                                QuestionAnswer = "1977-11-28"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SumInsured,
                                QuestionAnswer = "650000"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Suburb,
                                QuestionAnswer = "GREEN POINT"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.PostalCode,
                                QuestionAnswer = "8005"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ResidenceIs,
                                QuestionAnswer = QuestionAnswers.ResidenceIsMainResidence
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.TypeOfResidence,
                                QuestionAnswer = QuestionAnswers.TypeOfResidenceDetachedHouseCottage
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SubsidenceandLandslip,
                                QuestionAnswer = false
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.WallConstruction,
                                QuestionAnswer = QuestionAnswers.WallConstructionStandard
                            },
                        new RatingRequestItemAnswerDto
                            {   
                                Question = Questions.RoofConstruction,
                                QuestionAnswer = QuestionAnswers.RoofConstructionStandard
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Unoccupied,
                                QuestionAnswer = QuestionAnswers.UnoccupiedDuringWorkHours
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.NoClaimBonus,
                                QuestionAnswer = QuestionAnswers.NCB7
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.PreviouslyInsured,
                                QuestionAnswer = true
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast0to12Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast12Months0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast12to24Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast24Months0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast24to36Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast36Months0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.CompulsoryExcess,
                                QuestionAnswer = QuestionAnswers.CompulsoryExcess0
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.VoluntaryExcess,
                                QuestionAnswer = QuestionAnswers.VoluntaryExcess0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SituationOfResidence,
                                QuestionAnswer = QuestionAnswers.SituationOfResidenceSecurityVillage
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Retired,
                                QuestionAnswer = false
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Province,
                                QuestionAnswer = QuestionAnswers.ProvinceGauteng
                            },
                    };
            }
        }
    }

    public class AllRiskAnswerData : IRatingRequestAnswerData
    {
        public IList<RatingRequestItemAnswerDto> Answers
        {
            get
            {
                return new List<RatingRequestItemAnswerDto>
                    {
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.AllRiskCategory,
                                QuestionAnswer = QuestionAnswers.AllRiskCategoryClothingPersonalEffectsUnspecified
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.RiskAddress,
                                QuestionAnswer = "Unit 13, Villa Toscana, 92 Puttick Ave"
                            },
                         new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Suburb,
                                QuestionAnswer = "GREEN POINT"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.PostalCode,
                                QuestionAnswer = "8005"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ItemDescription,
                                QuestionAnswer = "All my clothing"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SumInsured,
                                QuestionAnswer = "10000"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.SerialIMEINumber,
                                QuestionAnswer = "None"
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.Retired,
                                QuestionAnswer = "False"
                            },
                             new RatingRequestItemAnswerDto
                            {
                                Question = Questions.NoClaimBonus,
                                QuestionAnswer = QuestionAnswers.NCB7
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.PreviouslyInsured,
                                QuestionAnswer = true
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast0to12Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast12Months0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast12to24Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast24Months0
                            },
                        new RatingRequestItemAnswerDto
                            {
                                Question = Questions.ClaimsLast24to36Months,
                                QuestionAnswer = QuestionAnswers.ClaimsLast36Months0
                            },
                    };
            }
        }
    }
}