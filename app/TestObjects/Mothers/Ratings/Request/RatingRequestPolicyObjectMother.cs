﻿using iPlatform.Api.DTOs.Ratings.Request;
using TestObjects.Mothers.Ratings.Request.Builders;

namespace TestObjects.Mothers.Ratings.Request
{
    public class RatingRequestPolicyObjectMother
    {
        public static RatingRequestPolicyBuilder Builder
        {
            get { return new RatingRequestPolicyBuilder(); }
        }

        public static IRatingRequestPolicyData Policy1Data
        {
            get { return new Policy1(); }
        }

        public static RatingRequestPolicyDto Policy1
        {
            get { return Builder.Build(Policy1Data); }
        }
    }
}
