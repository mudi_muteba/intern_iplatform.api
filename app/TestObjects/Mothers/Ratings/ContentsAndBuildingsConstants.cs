﻿namespace TestObjects.Mothers.Ratings
{
    public static class ContentsAndBuildingsConstants
    {
        public static string PostalCode { get { return "2192"; } }

        public static string Province { get { return "Gauteng"; } }

        public static string Usage { get { return "Main Residence"; } }

        public static string FinancedOrPaidoff { get { return "Yes - Mortgaged"; } }

        public static string HomeType { get { return "Free Standing House"; } }

        public static string RoofConstruction { get { return "Standard"; } }

        public static string WallConstruction { get { return "Standard"; } }

        public static string ThatchLapa { get { return "Attached"; } }

        public static string HomeBoarder { get { return "Vacant land"; } }

        public static string ConstructionYear { get { return "2011"; } }

        public static string SizeOfProperty { get { return "12000"; } }

        public static string NumberOfBathroom { get { return "2"; } }

        public static string PropertyUnoccupied { get { return "0 days -30 days"; } }

        public static string BussinessConducted { get { return "None"; } }

        public static string MortgageBank { get { return "ABSA"; } }

        public static string AccidentalDamage { get { return "None"; } }

        public static string WaterPumpingMachinery { get { return "None"; } }

        public static string AdditionalExcess { get { return "None"; } }

    }
}
