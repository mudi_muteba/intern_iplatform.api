﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.DisplaySetting;

namespace TestObjects.Mothers.DisplaySetting
{
    public class DisplaySettingObjectMother
    {
        public static CreateDisplaySettingDto ValidCreateDisplaySettingDto()
        {
            return new CreateDisplaySettingDto()
            {
                ChannelId = 1,
                ProductId = 1,
                ClaimEndPointUrl = "http://ClaimEndPointUrl.co.za",
                FinanceEndPointUrl = "http://FinanceEndPointUrl.co.za",
                PolicyEndPointUrl = "http://PolicyEndPointUrl.co.za",
                PolicyScheduleEndPointUrl = "http://PolicyScheduleEndPointUrl.co.za",
                ThirdPartyDisplay = true
            };
        }

        public static EditDisplaySettingDto ValidEditDisplaySettingDto(int id)
        {
            CreateDisplaySettingDto createDisplaySettingDto = ValidCreateDisplaySettingDto();

            return new EditDisplaySettingDto()
            {
                Id=id,
                ChannelId = createDisplaySettingDto.ChannelId,
                ProductId = createDisplaySettingDto.ProductId,
                ClaimEndPointUrl = createDisplaySettingDto.ClaimEndPointUrl,
                FinanceEndPointUrl = createDisplaySettingDto.FinanceEndPointUrl,
                PolicyEndPointUrl = createDisplaySettingDto.PolicyEndPointUrl,
                PolicyScheduleEndPointUrl = createDisplaySettingDto.PolicyScheduleEndPointUrl,
                ThirdPartyDisplay = createDisplaySettingDto.ThirdPartyDisplay
            };
        }
    }
}
