﻿using System;
using MasterData;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;

namespace TestObjects.Mothers.SettingsQuoteUploads
{
    public class SettingsQuoteUploadObjectMother
    {
        public static CreateSettingsQuoteUploadDto ValidDto()
        {
            return new CreateSettingsQuoteUploadDto
            {
                ChannelId = 1,
                Environment = "dev",
                ProductId = 1,
                SettingName = "ServiceURL9",
                SettingValue = "http://dev.iplatform.api"
            };
        }


        public static SaveSettingsQuoteUploadsDto ValidMultipleDto()
        {
            var dto = new SaveSettingsQuoteUploadsDto
            {
                ChannelId = 2,
                Environment = "dev",
            };
            dto.Settings.Add(new SaveSettingsQuoteUploadDetailDto { SettingName = "ServiceURL1", SettingValue = "http://dev.iplatform.api", ProductId = 1 });
            dto.Settings.Add(new SaveSettingsQuoteUploadDetailDto { SettingName = "ServiceURL2", SettingValue = "http://dev.iplatform.api", ProductId = 1 });
            dto.Settings.Add(new SaveSettingsQuoteUploadDetailDto { SettingName = "ServiceURL3", SettingValue = "http://dev.iplatform.api", ProductId = 1 });
            return dto;
        }
    }
}