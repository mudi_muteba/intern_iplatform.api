﻿using System;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;

namespace TestObjects.Mothers.Campaigns
{
    public class NewCampaignDtoObjectMother
    {
        public static CreateCampaignDto ValidCampaignDto()
        {
            return new CreateCampaignDto
            {
                ChannelId = 1,
                EndDate = DateTime.Now.AddDays(30),
                StartDate = DateTime.Now,
                Name = new Random().Next(100,999) + "This is a test campaign " + new Random().Next(100,999),
                Reference = "REF#" + new Random().Next(100,999),
                CampaignHoursFrom = "08:00",
                CampaignHoursTo = "17:00",
                MaximumLeadCallbackLimit = 6,
                AutoRescheduleCallbacks = true,
                AutoRescheduleCallbackHours = 8,
                AllowSimilarCampaignLeadCallbacks = true,
                AllowLeadCallbacksOutsideCampaignHours = true,
                UseLastInFirstOutCallScheme = false
            };
        }

        public static CreateCampaignDto InValidProductIdCampaignDto()
        {
            var campaign = ValidCampaignDto();

            campaign.Products.Add(new ProductInfoDto()
            {
                Id = 99998,
                Name = "Product99998"
            });

            campaign.Products.Add(new ProductInfoDto()
            {
                Id = 99999,
                Name = "Product99999"
            });

            return campaign;
        }

        public static CreateCampaignDto ValidCampaignSimilarCampaignDto()
        {
            var campaign = ValidCampaignDto();

            campaign.SimilarCampaigns.Add(new CampaignDto()
            {
                Id = 1
            });

            return campaign;
        }

        public static CreateCampaignDto ValidProductCampaignDto()
        {
            var campaign = ValidCampaignDto();

            campaign.Products.Add(new ProductInfoDto()
            {
                Id = 47,
                Name = "Virgin Funeral Plan"
            });


            return campaign;
        }

        public static CreateCampaignDto ValidTagCampaignDto()
        {
            var campaign = ValidProductCampaignDto();

            campaign.Tags.Add(new TagDto()
            {
                Id = 1,
                Name = "Personal"
            });
            return campaign;
        }

        public static CreateCampaignDto InvalidTagCampaignDto()
        {
            var campaign = ValidProductCampaignDto();

            campaign.Tags.Add(new TagDto()
            {
                Id = 999999,
                Name = "INVALID"
            });
            return campaign;
        }

        public static CreateCampaignDto InvalidAgentCampaignDto()
        {
            var campaign = ValidTagCampaignDto();

            campaign.Agents.Add(new PartyDto()
            {
                Id = 999999999
            });
            return campaign;
        }

        public static CreateCampaignDto ValidReferenceCampaignDto()
        {
            var campaign = ValidTagCampaignDto();

            campaign.References.Add(new CampaignReferenceDto()
            {
                Name = "Personal"
            });
            return campaign;
        }

        public static CreateCampaignDto InvalidReferenceCampaignDto()
        {
            var campaign = ValidTagCampaignDto();

            campaign.References.Add(new CampaignReferenceDto()
            {
                Name = ""
            });
            return campaign;
        }
    }
}