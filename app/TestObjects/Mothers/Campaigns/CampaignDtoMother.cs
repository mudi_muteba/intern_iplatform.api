﻿using iPlatform.Api.DTOs.Campaigns;

namespace TestObjects.Mothers.Campaigns
{
    public class CampaignDtoMother
    {
        public static CampaignDto CampaignDto
        {
            get
            {
                var name = typeof(CampaignDto).Name;
                return new CampaignDto
                {
                    Id = typeof(CampaignDto).GetHashCode() + 1,
                    ChannelId = typeof(CampaignDto).GetHashCode() + 2,
                    Name = name,
                };
            }
        }
    }
}