﻿using System;
using MasterData;
using iPlatform.Api.DTOs.Organisations;
using TestObjects.Mothers.ContactDetails;

namespace TestObjects.Mothers.Organizations
{
    public class OrganizationObjectMother
    {
        public static CreateOrganizationDto ValidDto()
        {
            return new CreateOrganizationDto
            {
                Code = "TEST",
                RegisteredName = "TEST",
                TradingName = "TEST",
                TradingSince = null,
                Description = "",
                RegNo = "",
                FspNo = "",
                VatNo = "",
                ShortTermInsurers = false,
                LongTermInsurers = false,
                TemporaryFsp = false,
                ProfessionalIndemnityInsurer = "",
                PiCoverPolicyNo = "",
                TypeOfAgency = "",
                FgPolicyNo = "",
                FidelityGuaranteeInsurer = "",
                ContactDetail = ContactDetailDtoMother.CreateContactDetailDto,
                ChannelId = 1

            };
        }
        public static EditOrganizationDto ValidEditDto(int id)
        {
            return new EditOrganizationDto
            {
                Id = id,
                Code = "TEST",
                RegisteredName = "TEST",
                TradingName = "TEST",
                TradingSince = null,
                Description = "",
                RegNo = "",
                FspNo = "",
                VatNo = "",
                ShortTermInsurers = false,
                LongTermInsurers = false,
                TemporaryFsp = false,
                ProfessionalIndemnityInsurer = "",
                PiCoverPolicyNo = "",
                TypeOfAgency = "",
                FgPolicyNo = "",
                FidelityGuaranteeInsurer = "",
                ContactDetail = ContactDetailDtoMother.CreateContactDetailDto,
            };
        }

    }
}