﻿using iPlatform.Api.DTOs.RatingRuleHeaders;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using MasterData;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Products;

namespace TestObjects.Mothers.RatingRuleHeaderCalculations
{
    public class RatingRuleHeaderCalculationDtoMother
    {
        public static CreateRatingRuleHeaderCalculationDto ValidCreateDto(RatingRuleHeaderDto ratingRuleHeaderDto)
        {
            return new CreateRatingRuleHeaderCalculationDto()
            {
                RatingRuleHeader = ratingRuleHeaderDto,
                Question = Questions.SumInsured,
                QuestionAnswer = "10000",
                Channel = new ChannelInfoDto()
                {
                    Id = 1,
                    Name = "IP"
                },
                Product = new ProductInfoDto()
                {
                    Id = 1,
                    ChannelId = 1
                },
                Cover = Covers.Motor,
                Rank = 0,
                RateCalculatorType = RateCalculatorType.Fixed,
                FailureMessage = "Sum Insured Required",
                WarningMessage = "Sum Insured Required",
                From = "",
                To ="",
                IsPercentage = false,
                MaxPremium = 500.00m,
                MinPremium = 250.00m,
                Percentage = 0.00m,
                Premium = 500.00m
            };
        }

        public static EditRatingRuleHeaderCalculationDto ValidEditDto(int id)
        {
            return new EditRatingRuleHeaderCalculationDto()
            {
                Id = id,
                Question = Questions.SumInsured,
                QuestionAnswer = "20000",
                Channel = new ChannelInfoDto()
                {
                    Id = 1,
                    Name = "IP"
                },
                Product = new ProductInfoDto()
                {
                    Id = 1,
                    ChannelId = 1
                },
                Cover = Covers.Motor,
                Rank = 0,
                RateCalculatorType = RateCalculatorType.Fixed,
                FailureMessage = "Sum Insured Required",
                WarningMessage = "Sum Insured Required",
                From = "",
                To = "",
                IsPercentage = false,
                MaxPremium = 500.00m,
                MinPremium = 250.00m,
                Percentage = 0.00m,
                Premium = 500.00m
            };
        }
    }
}
