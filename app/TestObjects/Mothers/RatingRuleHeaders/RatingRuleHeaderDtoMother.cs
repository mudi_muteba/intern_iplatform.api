﻿using System;
using iPlatform.Api.DTOs.RatingRuleHeaders;

namespace TestObjects.Mothers.RatingRuleHeaders
{
    public class RatingRuleHeaderDtoMother
    {
        public static CreateRatingRuleHeaderDto ValidCreateDto()
        {
            return new CreateRatingRuleHeaderDto()
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(1)
            };
        }

        public static EditRatingRuleHeaderDto ValidEditDto(int id) {
            return new EditRatingRuleHeaderDto()
            {
                Id = id,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(1)
            };
        }

    }
}
