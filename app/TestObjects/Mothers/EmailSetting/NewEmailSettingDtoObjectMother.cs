﻿
using Domain.Emailing.Factory;

namespace TestObjects.Mothers.EmailSetting
{
    public class NewEmailSettingDtoObjectMother
    {
        public static AppSettingsEmailSetting ValidEmailSetting()
        {
            return new AppSettingsEmailSetting
            {
                Host = "smtp.gmail.com",
                Port = 587,
                DefaultContactNumber = "+27 11 300 1100",
                UserName = "iplatform@iplatform.co.za",
                Password = "p8nynyocRLhoCeN2wdoN8A",
                DefaultFrom = "iPlatform<iplatform@platform.co.za>",
                SubAccountID = "QuoteAcceptance",
                UseDefaultCredentials = false,
                UseSSL = true,
                DefaultBCC = "",
                CustomFrom = ""
            };
        }
    }
}