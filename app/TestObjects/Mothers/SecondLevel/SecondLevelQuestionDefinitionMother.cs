﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Campaigns;
using Domain.Party;
using Domain.Party.ProposalHeaders;
using Domain.Party.Quotes;
using Domain.Products;
using Domain.SecondLevel.Questions;
using Domain.Users;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.SecondLevel.Answers;
using iPlatform.Api.DTOs.SecondLevel.Individual;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using MasterData;

namespace TestObjects.Mothers.SecondLevel
{
    public class SecondLevelQuestionDefinitionMother
    {
        public static SecondLevelQuestionDefinition ForSecondLevelQuestionDefinition()
        {
            return new SecondLevelQuestionDefinition()
            {
                SecondLevelQuestion = new SecondLevelQuestion()
                {
                    QuestionType = new QuestionType()
                    {
                        Id = 1000,
                        Name = "Definition Question Type Name"
                    },
                    SecondLevelQuestionGroup = new SecondLevelQuestionGroup()
                    {
                        Id = 2000,
                        Name = "Second Level Question Group Name"
                    },
                    Id = 3000,
                    Name = "Second Level Question Name"
                },
                CoverDefinition = new CoverDefinition(),
                DisplayName = "Definition Display Name",
                ToolTip = "Second Level Question Definition Tool Tip"
            };
        }

        public static Quote ForSecondLevelQuote()
        {
            var proposal = new ProposalHeader();
            proposal.Created(new User("You mother", "Is not your father", new List<int>()),new Party(),new CampaignReference(new Campaign(), string.Empty ));

            return new Quote(
                new QuoteHeader(Guid.NewGuid().ToString(),proposal), new Product(1000, "Second Level Mother Product"), 500M, "Mother Reference", "0000", "2222",
                Enumerable.Empty<string>(), Enumerable.Empty<string>());
        }

         public static SecondLevelQuestionsAnswersSaveDto ForSecondLevelQuestionsSaveDto()
         {
             return new SecondLevelQuestionsAnswersSaveDto()
             {
                 Id = 1,
                 Items = new List<SecondLevelQuestionAnswerSaveDto>()
                 {
                     new SecondLevelQuestionAnswerSaveDto()
                     {
                         Id = 1,
                         Answer = "11",
                         QuoteItemId = 2221,
                         SecondLevelQuestionId = 1
                     },
                     new SecondLevelQuestionAnswerSaveDto()
                     {
                         Id = 2,
                         Answer = "",
                         QuoteItemId = 2221,
                         SecondLevelQuestionId = 2
                     },
                     new SecondLevelQuestionAnswerSaveDto()
                     {
                         Id = 3,
                         Answer = "8",
                         QuoteItemId = 2221,
                         SecondLevelQuestionId = 3
                     },
                     new SecondLevelQuestionAnswerSaveDto()
                     {
                         Id = 4,
                         Answer = "14",
                         QuoteItemId = 2221,
                         SecondLevelQuestionId = 4
                     }
                 },
                 QuoteId = 2006
             };
         }

        public static SecondLevelQuoteDto ForSecondLevelQuoteDto()
        {
            return new SecondLevelQuoteDto()
            {
                QuoteId = 2006,
                Items = new List<QuoteItemDto>()
                {
                    new QuoteItemDto()
                    {
                        Id = 2221,
                        CoverDefinition = new QuoteCoverDefinitionDto()
                        {
                            Id =11,
                            DisplayName = "Motor",
                            Benefits = ListProductBenefitDto(),
                            Cover = new Cover()
                            {
                                Name = "Motor",
                                Id = 218,
                                Code = "MOTOR"
                            },
                            VisibleIndex = 0,
                            CoverDefinitionType = new CoverDefinitionType()
                            {
                                Id = 1,
                                Name = "Main Cover"
                            }
                        },
                        Asset = new QuoteItemAssetDto()
                        {
                            AssetNumber = 222,
                            SumInsured = new MoneyDto(0.0000M)
                        },
                        Description = string.Empty,
                        Excess = new QuoteItemExcessDto()
                        {
                            Description = "Note that the total sum insured for all products combined cannot exceed R500,000..1. R250 for each and every claim excluding cellular phones and pedal cycles.2. R500 for each and every claim in respect of cellular phones. 3. 10% of claim minimum R250 for each and every claim in respect of pedal cycles.",
                            Basic = new MoneyDto(3000.0000M),
                            IsCalculated = true,
                            VoluntaryExcess = new MoneyDto(0.00000M)
                        },
                        Fees = new QuoteItemFeesDto()
                        {
                            Premium = new MoneyDto(168.1818M),
                            Sasria = new MoneyDto(0.0360M)
                        },
                        QuoteItemState = new QuoteItemStateDto()
                        {
                            
                        }
                    }
                },
                ProductId = 5,
                //IndvidualId = 96,
                PartyId = 95,
                Individual = new SecondLevelIndividualDto()
                {
                    IndvidualId = 95,
                    AnyJudgementsReason = "",
                    BeenSequestratedOrLiquidatedReason = "",
                    BeenSequestratedOrLiquidated = false,
                    AnyJudgements = false,
                    UnderAdministrationOrDebtReview = false,
                    UnderAdministrationOrDebtReviewReason = ""
                }
            };
        }

        public static List<ProductBenefitDto> ListProductBenefitDto()
        {
            return new List<ProductBenefitDto>()
            {
                new ProductBenefitDto()
                {
                    Name = "Included",
                    Value = "Yes",
                    ShowToClient = true,
                    VisibleIndex = 0
                },
                 new ProductBenefitDto()
                {
                    Name = "Riot and Strike outside RSA",
                    Value = "No",
                    ShowToClient = true,
                    VisibleIndex = 1
                },
                 new ProductBenefitDto()
                {
                    Name = "Unspecified Items as defined",
                    Value = "Yes",
                    ShowToClient = true,
                    VisibleIndex = 2
                },
                 new ProductBenefitDto()
                {
                    Name = "If yes ~ Cover for Jewellery, Clothing and Personal items",
                    Value = "Yes",
                    ShowToClient = true,
                    VisibleIndex = 3
                },
                 new ProductBenefitDto()
                {
                    Name = "If yes ~ Limit any one item",
                    Value = "25% of sum insured",
                    ShowToClient = true,
                    VisibleIndex = 4
                },
                 new ProductBenefitDto()
                {
                    Name = "If yes ~ Cover for money and negotiable instruments",
                    Value = "Must be specified",
                    ShowToClient = true,
                    VisibleIndex = 5
                },
                 new ProductBenefitDto()
                {
                    Name = "Specified Items [If yes refer to list of items]",
                    Value = "Yes",
                    ShowToClient = true,
                    VisibleIndex = 6
                }
            };
        }

       
    }
}
