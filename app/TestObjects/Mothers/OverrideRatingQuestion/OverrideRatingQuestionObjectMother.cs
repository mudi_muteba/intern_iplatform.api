﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition;

namespace TestObjects.Mothers.OverrideRatingQuestion
{
    public class OverrideRatingQuestionObjectMother
    {
        public static CreateOverrideRatingQuestionDto ValidCreateOverrideRatingQuestionDto()
        {
            List<CreateOverrideRatingQuestionChannelDto> channels = new List<CreateOverrideRatingQuestionChannelDto>();
            for (int i = 1; i < 4; i++)
            {
                channels.Add(new CreateOverrideRatingQuestionChannelDto()
                {
                    ChannelId = i
                });
            }

            List<CreateOverrideRatingQuestionMapVapQuestionDefinitionDto> questions = new List<CreateOverrideRatingQuestionMapVapQuestionDefinitionDto>();
            for (int i = 1; i < 4; i++)
            {
                questions.Add(new CreateOverrideRatingQuestionMapVapQuestionDefinitionDto()
                {
                    MapVapQuestionDefinitionId = i
                });
            }

            return new CreateOverrideRatingQuestionDto()
            {
                ProductId = 2,
                CoverId = 78,
                QuestionId = 99,
                OverrideValue = "",
                OverrideRatingQuestionChannels = channels,
                OverrideRatingQuestionMapVapQuestionDefinitions = questions 
            };
        }

        public static EditOverrideRatingQuestionDto ValidEditOverrideRatingQuestionDto(int id)
        {
            CreateOverrideRatingQuestionDto createOverrideRatingQuestionDto = ValidCreateOverrideRatingQuestionDto();
            List<EditOverrideRatingQuestionChannelDto> channels = new List<EditOverrideRatingQuestionChannelDto>();
            for (int i = 2; i < 4; i++)
            {
                channels.Add(new EditOverrideRatingQuestionChannelDto()
                {
                    ChannelId = i
                });
            }
            List<EditOverrideRatingQuestionMapVapQuestionDefinitionDto> questions = new List<EditOverrideRatingQuestionMapVapQuestionDefinitionDto>();
            for (int i = 1; i < 4; i++)
            {
                questions.Add(new EditOverrideRatingQuestionMapVapQuestionDefinitionDto()
                {
                    MapVapQuestionDefinitionId = i
                });
            }

            return new EditOverrideRatingQuestionDto()
            {
                Id = id,
                ProductId = createOverrideRatingQuestionDto.ProductId,
                CoverId = createOverrideRatingQuestionDto.CoverId,
                QuestionId = createOverrideRatingQuestionDto.QuestionId,
                OverrideValue = "test",
                OverrideRatingQuestionChannels = channels,
                OverrideRatingQuestionMapVapQuestionDefinitions = questions
            };
        }
    }
}
