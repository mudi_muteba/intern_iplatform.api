﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Individual;
using MasterData;
using TestObjects.Mothers.ContactDetails;
using TestObjects.Mothers.Occupations;

namespace TestObjects.Mothers.Individual
{
    public class IndividualDtoMother
    {
        public static IndividualDto IndividualDto
        {
            get
            {
                var name = typeof(IndividualDto).Name;
                return new IndividualDto
                {
                    Id = typeof(IndividualDto).GetHashCode() + 1,
                    PartyId = typeof(IndividualDto).GetHashCode() + 2,
                    Title = Titles.Mr,
                    FirstName = name + "FirstName",
                    MiddleName = name + "MiddleName",
                    Surname = name + "Surname",
                    Gender = Genders.Male,
                    DateOfBirth = new DateTimeDto(new DateTime(1984, 10, 15)),
                    IdentityNo = "8310155284080",
                    MaritalStatus = MaritalStatuses.Married,
                    Language = Languages.English,
                    ContactDetail = ContactDetailDtoMother.ContactDetailDto,
                    DisplayName = name + "DisplayName",
                    DateCreated = new DateTimeDto(DateTime.UtcNow),
                    DateUpdated = new DateTimeDto(DateTime.UtcNow),
                    Links = new List<ResourceLink>(),
                    Occupation = OccupationDtoMother.OccupationDto,
                    
                };
            }
        } 
    }
}