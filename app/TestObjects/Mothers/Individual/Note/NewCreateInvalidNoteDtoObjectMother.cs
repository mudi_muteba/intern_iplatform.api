﻿using iPlatform.Api.DTOs.Party.Note;

namespace TestObjects.Mothers.Individual.Note
{
    public static class NewCreateInvalidNoteDtoObjectMother
    {
        public static CreateNoteDto InvalidCreateNoteDto()
        {
            return new CreateNoteDto
            {
              Confidential = true,
              Notes = string.Empty
            };
        }
    }
}
