﻿using iPlatform.Api.DTOs.Party.Note;

namespace TestObjects.Mothers.Individual.Note
{
    public static class NewCreateNoteDtoObjectMother
    {
        public static CreateNoteDto ValidCreateNoteDto()
        {
            return new CreateNoteDto
            {
                Confidential = true,
                Notes = "This is a note",
            };
        }
    }
}