﻿using System;
using iPlatform.Api.DTOs.Party.Relationships;
using MasterData;

namespace TestObjects.Mothers.Individual
{
    public class NewRelationshipDtoObjectMother
    {
        public static CreateRelationshipDto ValidRelationshipDto(int partyId, int childPartyId)
        {
            return new CreateRelationshipDto
            {
                ChannelId = 1,
                Claims = "claims " + DateTime.UtcNow.ToString(),
                Functions = "functions " + DateTime.UtcNow.ToString(),
                Other = "other " + DateTime.UtcNow.ToString(),
                RelationshipTypeId = RelationshipTypes.Dependant.Id,
                PartyId= partyId,
                ChildPartyId = childPartyId
            };
        }

        public static EditRelationshipDto ValidEditRelationshipDto(int id, CreateRelationshipDto dto)
        {
            return new EditRelationshipDto {
                Id = id,
                ChildPartyId = dto.ChildPartyId,
                PartyId = dto.PartyId,
                RelationshipTypeId = dto.RelationshipTypeId,
                Claims = dto.Claims,
                Functions = dto.Functions,
                Other = dto.Other
            };
        }

        public static EditRelationshipDto ValidEditRelationshipWithEmptyPartiesDto(int id, CreateRelationshipDto dto)
        {
            return new EditRelationshipDto
            {
                Id = id,
                ChildPartyId = 0,
                PartyId = 0,
                RelationshipTypeId = dto.RelationshipTypeId,
                Claims = dto.Claims,
                Functions = dto.Functions,
                Other = dto.Other
            };
        }

        public static EditRelationshipDto ValidEditRelationshipWithInvalidPartiesDto(int id, CreateRelationshipDto dto)
        {
            return new EditRelationshipDto
            {
                Id = id,
                ChildPartyId = 9999,
                PartyId = 10000,
                RelationshipTypeId = dto.RelationshipTypeId,
                Claims = dto.Claims,
                Functions = dto.Functions,
                Other = dto.Other
            };
        }
    }
}