﻿using iPlatform.Api.DTOs.Party.Bank;

namespace TestObjects.Mothers.Individual.Bank
{
    public static class NewCreateBankDetailsDtoObjectMother
    {
        public static CreateBankDetailsDto ValidCreateBankDetailsDto()
        {
            return new CreateBankDetailsDto
            {
                AccountNo = "1234567890",
                Bank = "FNB",
                BankAccHolder = "Gota catch em all",
                BankBranch = "This is a branch",
                BankBranchCode = "2",
                TypeAccount = "This is a type"

            };
        }
        public static CreateBankDetailsDto InvalidCreateBankDetailsDto()
        {
            return new CreateBankDetailsDto
            {
                AccountNo = "",
                Bank = "",
                BankAccHolder = "Gota catch em all",
                BankBranch = "",
                BankBranchCode = ""

            };
        }
    }
}
