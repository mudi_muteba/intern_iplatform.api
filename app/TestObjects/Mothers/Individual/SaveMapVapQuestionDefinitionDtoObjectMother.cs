﻿using iPlatform.Api.DTOs.MapVapQuestionDefinitions;

namespace TestObjects.Mothers.Individual
{
    public class MapVapQuestionDefinitionDtoObjectMother
    {

        public static CreateMapVapQuestionDefinitionDto ValidCreateDto()
        {
            return new CreateMapVapQuestionDefinitionDto
            {
                QuestionDefinitionId = 1,
                ChannelId = 1,
                ProductId = 1,
                Premium = (decimal) 123.12,
                Enabled = true
            };
        }

        public static EditMapVapQuestionDefinitionDto ValidEditDto(int id)
        {
            return new EditMapVapQuestionDefinitionDto
            {
                Id = id,
                QuestionDefinitionId = 1,
                ChannelId = 1,
                ProductId = 1,
                Premium = (decimal)1000,
                Enabled = false
            };
        }
    }
}