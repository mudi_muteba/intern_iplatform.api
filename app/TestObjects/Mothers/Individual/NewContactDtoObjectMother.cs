﻿using System;
using iPlatform.Api.DTOs.Party.Contacts;
using MasterData;

namespace TestObjects.Mothers.Individual
{
    public class NewContactDtoObjectMother
    {
        public static CreateContactDto ValidContactDto()
        {
            return new CreateContactDto
            {
                Title = Titles.Mrs,
                Gender = Genders.Female,
                FirstName = "Florence",
                Surname = "Khambule",
                DateOfBirth = new DateTime(1987, 08, 08),
                IdentityNo = "8708070697082",
                MaritalStatus = MaritalStatuses.Married,
                RelationshipType = RelationshipTypes.Branch,
                Language = Languages.Swazi
            };
        }
        public static CreateContactDto InvalidContactDto()
        {
            return new CreateContactDto
            {
                Title = Titles.Mrs,
                Gender = Genders.Female,
                FirstName = "Florence",
                Surname = "Khambule",
                DateOfBirth = new DateTime(1987, 08, 08),
                IdentityNo = string.Empty,
                MaritalStatus = MaritalStatuses.Married,
                RelationshipType = null,
                Language = Languages.Swazi
            };
        }
    }
}