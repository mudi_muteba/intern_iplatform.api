﻿using System;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Occupation;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData;

namespace TestObjects.Mothers.Individual.Import
{
    public static class NewImportIndividualDtoObjectMother
    {
        public static ImportIndividualDto ValidImportIndividualDto()
        {
            return new ImportIndividualDto
            {
                Title = Titles.Mr,
                Gender = Genders.Male,
                FirstName = "Ootam",
                Surname = "Seewoogoolam",
                MaritalStatus = MaritalStatuses.Married,
                IdentityNo = "83051498746498461",
                DateOfBirth = new DateTime(1983,5,14),
                ChannelId = 1,
                Occupation = new OccupationDto{Id = 1},
                ExternalReference = "ER-0005",
                ContactDetail = new ContactDetailDto(){
                    Work = "454658765465",
                    Direct = "6966126",
                    Cell = "57649500",
                    Home = "6966126",
                    Fax = null,
                    Email = "ootam@allmystuff.co.za",
                    Website = null,
                    Facebook = null,
                    Twitter = null,
                    LinkedIn = null,
                    Skype = null,
                },
            };

        }
        
    }
}
