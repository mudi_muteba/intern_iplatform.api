﻿using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using MasterData;

namespace TestObjects.Mothers.Individual.AssetVehicles
{
    public static class NewAssetVehicleDtoObjectMother
    {
        public static CreateAssetVehicleDto ValidCreateAssetVehicleDto(int partyId)
        {
            return new CreateAssetVehicleDto
            {
                PartyId = partyId,
                VehicleMMCode = "MMCODE10",
                VehicleMake = "Honda",
                VehicleModel = "SVR",
                VehicleRegistrationNumber = "CXS050MP",
                VehicleType = VehicleTypes.Sedan,
                YearOfManufacture = 2010

            };
        }
        public static CreateAssetVehicleDto InvalidCreateAssetVehicleDto(int partyId)
        {
            return new CreateAssetVehicleDto
            {
                PartyId = partyId,
                VehicleMMCode = "",
                VehicleMake = "",
                VehicleModel = "",
                VehicleRegistrationNumber = "",
                VehicleType = VehicleTypes.Sedan,
                YearOfManufacture = 2010

            };
        }
    }
}
