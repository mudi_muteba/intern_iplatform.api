﻿using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using MasterData;
using System.Collections.Generic;

namespace TestObjects.Mothers.Individual.CorrespondencePreference
{
    public static class NewCreateCorrespondencePrefDtoObjectMother
    {
        public static EditCorrespondencePreferenceDto ValidCreateCorrespondencePrefDto()
        {
            var dto = new EditCorrespondencePreferenceDto();
            dto.CorrespondencePreferences.Add(new PartyCorrespondencePreferenceDto
            {
                CorrespondenceType = CorrespondenceTypes.MarketingInformation,
                Email = true,
                Telephone = true,
                SMS = true,
                Post = true,
            });
            dto.CorrespondencePreferences.Add(new PartyCorrespondencePreferenceDto
            {
                CorrespondenceType = CorrespondenceTypes.GeneralDocuments,
                Email = true,
                Telephone = true,
                SMS = true,
                Post = true,
            });
            dto.CorrespondencePreferences.Add(new PartyCorrespondencePreferenceDto
            {
                CorrespondenceType = CorrespondenceTypes.PolicyDocuments,
                Email = true,
                Telephone = true,
                SMS = true,
                Post = true,
            });
            return dto;
        }

        public static EditCorrespondencePreferenceDto ValidEditCorrespondencePrefDto(int id)
        {
            var dto = new EditCorrespondencePreferenceDto();
            dto.PartyId = id;
            dto.CorrespondencePreferences.Add(new PartyCorrespondencePreferenceDto
            {
                CorrespondenceType = CorrespondenceTypes.MarketingInformation,
                Email = false,
                Telephone = false,
                SMS = false,
                Post = false,
            });
            dto.CorrespondencePreferences.Add(new PartyCorrespondencePreferenceDto
            {
                CorrespondenceType = CorrespondenceTypes.GeneralDocuments,
                Email = false,
                Telephone = false,
                SMS = false,
                Post = false,
            });
            dto.CorrespondencePreferences.Add(new PartyCorrespondencePreferenceDto
            {
                CorrespondenceType = CorrespondenceTypes.PolicyDocuments,
                Email = false,
                Telephone = false,
                SMS = false,
                Post = false,
            });
            return dto;
        }
    }
}
