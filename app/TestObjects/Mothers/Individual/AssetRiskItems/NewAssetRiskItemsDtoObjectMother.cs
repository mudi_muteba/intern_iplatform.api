﻿using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;

namespace TestObjects.Mothers.Individual.AssetRiskItems
{
    public static class NewAssetRiskItemDtoObjectMother
    {
        public static CreateAssetRiskItemDto ValidCreateAssetRiskItemDto(int partyId)
        {
            return new CreateAssetRiskItemDto
            {
                PartyId = partyId,
                AllRiskCategory = 1,
                SerialIMEINumber = "10023568548652486"
            };
        }
        public static CreateAssetRiskItemDto InvalidCreateAssetRiskItemDto(int partyId)
        {
            return new CreateAssetRiskItemDto
            {
                PartyId = partyId,
               AllRiskCategory = 0,
                SerialIMEINumber = "10023568548652486"
            };
        }
    }
}
