﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Individual;
using MasterData;
using TestObjects.Mothers.Occupations;

namespace TestObjects.Mothers.Individual
{
    public class BasicIndividualDtoMother
    {
        public static BasicIndividualDto BasicIndividualDto
        {
            get
            {
                var name = typeof(BasicIndividualDto).Name;
                return new BasicIndividualDto
                {
                    Id = typeof(BasicIndividualDto).GetHashCode() + 1,
                    Title = Titles.Mr,
                    Gender = Genders.Female,
                    FirstName = name + "FirstName",
                    MiddleName = name + "MiddleName",
                    Surname = name + "Surname",
                    DateOfBirth = new DateTimeDto(DateTime.UtcNow),
                    MaritalStatus = MaritalStatuses.Single,
                    IdentityNo = "8310155284080",
                    Language = Languages.Sotho,
                    DisplayName = name,
                    PartyId = typeof(BasicIndividualDto).GetHashCode(),
                    DateCreated = new DateTimeDto(DateTime.UtcNow),
                    DateUpdated = new DateTimeDto(DateTime.UtcNow),
                    ExternalReference = name + "ExternalReference",
                    Links = new List<ResourceLink>(),
                    Occupation = OccupationDtoMother.OccupationDto,
                    PassportNo = ""
                };
            }
        }
    }
}