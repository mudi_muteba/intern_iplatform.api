﻿using iPlatform.Api.DTOs.Party.ProposalDefinition;

namespace TestObjects.Mothers.Individual.ProposalDefinition
{
    public static class NewProposalDefinitionDtoObjectMother
    {
        public static CreateProposalDefinitionDto ValidCreateProposalDefinitionDto_CONTENTS(int proposalHeaderId)
        {
            return new CreateProposalDefinitionDto
            {
                CoverId = 78,
                ProductId = 27, // Multi Quote,
                ProposalHeaderId = proposalHeaderId
            };
        }

        public static CreateProposalDefinitionDto ValidCreateProposalDefinitionDto_MOTOR(int proposalHeaderId)
        {
            return new CreateProposalDefinitionDto
            {
                Description =  "Motor test",
                CoverId = 218,
                ProductId = 27, // Multi Quote,
            ProposalHeaderId = proposalHeaderId
            };
        }
        public static CreateProposalDefinitionDto ValidCreateProposalDefinitionDto_RISK_ITEM(int proposalHeaderId)
        {
            return new CreateProposalDefinitionDto
            {
                Description = "RISK_ITEM test",
                CoverId = 17,
                ProductId = 27, // Multi Quote,
                ProposalHeaderId = proposalHeaderId
            };
        }

        public static EditProposalDefinitionDto ValidEditProposalDefinitionDto(int id, int proposalHeaderId)
        {
            return new EditProposalDefinitionDto
            {
                Description = "EDIT test",
                ProposalHeaderId = proposalHeaderId,
                Id = id,
            };
        }


        public static CreateProposalDefinitionDto ValidCreateProposalDefinitionDto_BUILDINGS(int proposalHeaderId)
        {
            return new CreateProposalDefinitionDto
            {
                CoverId = 44,
                ProductId = 46, //AIG BUILDINGS,
                ProposalHeaderId = proposalHeaderId
            };
        }
    }
}
