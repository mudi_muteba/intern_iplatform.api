﻿using iPlatform.Api.DTOs.Party.Address;
using MasterData;

namespace TestObjects.Mothers.Individual.Address
{
    public static class NewCreateAddressDtoObjectMother
    {
        public static CreateAddressDto ValidCreateAddressDto()
        {
            return new CreateAddressDto
            {
                StateProvince = StateProvinces.Gauteng,
                IsComplex = false,
                Line1 = "TBC",
                Line2 = "Line 2",
                Line3 = "Line 3",
                Line4 = "FERNDALE EXT 1",
                Code = "1234",
                Complex = "This is a complex",
                AddressType = AddressTypes.PhysicalAddress,
            };
        }

        public static CreateAddressDto InvalidCreateAddressDto()
        {
            return new CreateAddressDto
            {
                StateProvince = StateProvinces.Gauteng,
                IsComplex = false,
                Line1 = string.Empty,
                Line2 = "Line 2",
                Line3 = "Line 3",
                Line4 = string.Empty,
                Code = string.Empty,
                Complex = "This is a complex",
                AddressType = AddressTypes.PhysicalAddress,
            };
        }
    }
}
