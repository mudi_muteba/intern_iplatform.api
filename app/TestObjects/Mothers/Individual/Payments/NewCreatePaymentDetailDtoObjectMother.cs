﻿using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.Payments;
using MasterData;
using System;

namespace TestObjects.Mothers.Individual.Payments
{
    public static class NewCreatePaymentDetailDtoObjectMother
    {
        public static CreatePaymentDetailsDto ValidCreatePaymentDetailDto(int bankDetailId, int partyId)
        {
            return new CreatePaymentDetailsDto
            {
                BankDetailsId = bankDetailId,
                InceptionDate =  DateTime.UtcNow,
                DebitOrderDate = DateTime.UtcNow.AddDays(10),
                MethodOfPayment = MethodOfPayments.DebitOrder,
                PaymentPlan = PaymentPlans.Annual,
                QuoteId = 2006,
                PartyId = partyId
            };
        }


        public static EditPaymentDetailsDto ValidEditPaymentDetailDto(int bankDetailId, int partyId)
        {
            return new EditPaymentDetailsDto
            {
                BankDetailsId = bankDetailId,
                QuoteId = 2006,
                PartyId = partyId,
                InceptionDate = DateTime.UtcNow,
                DebitOrderDate = DateTime.UtcNow.AddDays(3),
                MethodOfPayment = MethodOfPayments.EFTCash,
                PaymentPlan = PaymentPlans.BiAnnual,
                
            };
        }
    }
}
