﻿using iPlatform.Api.DTOs.Party.ProposalHeaders;

namespace TestObjects.Mothers.Individual.ProposalHeader
{
    public static class NewCreateInvalidProposalHeaderDtoObjectMother
    {
        public static CreateProposalHeaderDto InvalidCreateProposalHeaderDto()
        {
            return new CreateProposalHeaderDto();
        }
    }
}