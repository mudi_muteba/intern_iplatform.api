﻿using iPlatform.Api.DTOs.Party.ProposalHeaders;

namespace TestObjects.Mothers.Individual.ProposalHeader
{
    public static class NewCreateProposalHeaderDtoObjectMother
    {
        public static CreateProposalHeaderDto ValidCreateProposalHeaderDto()
        {
            return new CreateProposalHeaderDto
            {
                Description = "This is a new proposal header",
            };
        }
    }
}
