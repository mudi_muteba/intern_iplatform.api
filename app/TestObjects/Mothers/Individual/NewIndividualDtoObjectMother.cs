﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Occupation;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData;

namespace TestObjects.Mothers.Individual
{
    public class NewIndividualDtoObjectMother
    {

        public static CreateIndividualDto ValidBasicIndividualDto()
        {
            return new CreateIndividualDto
            {
                ChannelId = 1,
                FirstName = "Name",
                Surname = "Surname",
                ContactDetail = new CreateContactDetailDto { Cell = "Cell", Email = "Email" }
            };
        }

        public static CreateIndividualDto ValidIndividualDto()
        {
            var dto = new CreateIndividualDto
            {
                ChannelId = 1,
                Title = Titles.Mrs,
                Gender = Genders.Female,
                FirstName = "Florence",
                Surname = "Khambule",
                DateOfBirth = new DateTime(1987, 08, 08),
                IdentityNo = "8708070" + new Random().Next(999999),
                MaritalStatus = MaritalStatuses.Married,
                ContactDetail = new CreateContactDetailDto {Work = "0114921474", Cell = "0731451425", Email = "Email"},
                Language = Languages.Tswana,
               // Occupation = new OccupationDto{Id = 1}, //MISSING TESTING DATA
                ExternalReference = "ER-0001",
                LeadExternalReference =  "LEAD01",
                Campaigns = new List<CampaignInfoDto>() 
            };

            //dto.Campaigns.Add(new CampaignInfoDto{Id = 1}); //MISSING TESTING DATA
            return dto;
        }

        public static CreateIndividualDto ValidIndividualOne()
        {
            return new CreateIndividualDto
            {
                ChannelId = 1,
                Title = Titles.Mrs,
                Gender = Genders.Female,
                FirstName = "Party",
                Surname = "One",
                DateOfBirth = new DateTime(1983, 08, 08),
                IdentityNo = "8708070697083",
                MaritalStatus = MaritalStatuses.Married,
                ContactDetail = new CreateContactDetailDto { Work = "0114921474", Cell = "0731451425" },
                Language = Languages.Tswana,
                Occupation = new OccupationDto { Id = 1 },
                ExternalReference = "ER-0002"
            };
        }

        public static CreateIndividualDto ValidIndividualTwo()
        {
            return new CreateIndividualDto
            {
                ChannelId = 1,
                Title = Titles.Mr,
                Gender = Genders.Male,
                FirstName = "Party",
                Surname = "Two",
                DateOfBirth = new DateTime(1980, 08, 08),
                IdentityNo = "8708070697080",
                MaritalStatus = MaritalStatuses.Married,
                ContactDetail = new CreateContactDetailDto { Work = "0114921471", Cell = "0731451421" },
                Language = Languages.Tswana,
                Occupation = new OccupationDto { Id = 1 },
                ExternalReference = "ER-0003"
            };
        }

        public static CreateAddressDto ValidAddressDto(int indivdualId)
        {
            return new CreateAddressDto
            {
                Line1 = "Line 1",
                Line2 = "Line 2",
                Line3 = "Line 3",
                Line4 = "Line 4",
                Code = "1234",
                DefaultAddress = true,
                PartyId = indivdualId,
                Country = Countries.SouthAfrica,
                AddressType = AddressTypes.PhysicalAddress,
                StateProvince =
                    StateProvinces.Gauteng,
            };
        }


        public static CreateIndividualDto ValidIndividualWithCampaignDto()
        {
            return new CreateIndividualDto
            {
                Title = Titles.Mr,
                Gender = Genders.Male,
                FirstName = "Ootam",
                Surname = "Seewoogoolam",
                MaritalStatus = MaritalStatuses.Married,
                IdentityNo = GetRandom16Digit(),
                DateOfBirth = new DateTime(1983, 5, 14),
                ChannelId = 1,
                Occupation = new OccupationDto { Id = 1 },
                ExternalReference = "ER-0004",
                ContactDetail = new CreateContactDetailDto()
                {
                    Work = "454658765465",
                    Direct = "6966126",
                    Cell = "57649500",
                    Home = "6966126",
                    Fax = null,
                    Email = "ootam@allmystuff.co.za",
                    Website = null,
                    Facebook = null,
                    Twitter = null,
                    LinkedIn = null,
                    Skype = null,
                },
            };

        }


        public static string GetRandom16Digit()
        {
            Random r = new Random();

            return r.Next(10000000, 100000000).ToString() + r.Next(10000000, 100000000).ToString();
        }



        public static ImportIndividualDto NewImportIndividual()
        {
            return new ImportIndividualDto()
            {
                Title = Titles.Mr,
                Gender = Genders.Male,
                FirstName = "Florence",
                Surname = "Khambule",
                MaritalStatus = MaritalStatuses.Married,
                IdentityNo = GetRandom16Digit(),
                DateOfBirth = new DateTime(1987, 8, 8),
                ChannelId = 1,
                Occupation = new OccupationDto { Id = 1 },
                ExternalReference = "ER-0004",
                Source = "API",
                ContactDetail = new ContactDetailDto()
                {
                    Cell = "07826546461",
                    Email = "ootam@allmystuff.co.za",
                    Work = "453453453454",
                    Direct = "6966126",
                    Home = "6966126",
                    Fax = null,
                    Website = null,
                    Facebook = null,
                    Twitter = null,
                    LinkedIn = null,
                    Skype = null,
                }
            };
        }
    }
}