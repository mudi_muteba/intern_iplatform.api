﻿using System;
using Domain.Base.Workflow;
using TestObjects.Mothers.Quotes;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using TestObjects.Mothers.Ratings;

namespace TestObjects.Mothers.Router
{
    public class RouteAcceptedQuoteTaskMother
    {
        public static RouteAcceptedQuoteTask ForCampaign()
        {
            var dto = QuoteAcceptanceDtoMother.CreateForCampaignSantam();
            return new RouteAcceptedQuoteTask(dto, "MULPLX", "STM", Guid.Empty, QuoteAcceptanceConfigurationMother.CampaignConfig());
        }

        public static RouteAcceptedQuoteTask ForDotsure()
        {
            var dto = QuoteAcceptanceDtoMother.CreateForDotsure();
            return new RouteAcceptedQuoteTask(dto, "DOTSURE", "LLOYDS", Guid.Empty, QuoteAcceptanceConfigurationMother.DotsureConfig());
        }

        public static RouteAcceptedQuoteTask ForHollard()
        {
            var dto = QuoteAcceptanceDtoMother.CreateForHollard();
            return new RouteAcceptedQuoteTask(dto, "HOLHMH", "HOL", Guid.Empty, QuoteAcceptanceConfigurationMother.HollardConfig());
        }

        public static RouteAcceptedQuoteTask ForKingPrice()
        {
            var dto = QuoteAcceptanceDtoMother.CreateForKingPrice();
            return new RouteAcceptedQuoteTask(dto, "KPIPERS", "KPI", Guid.Empty, QuoteAcceptanceConfigurationMother.KingPriceConfig());
        }

        public static RouteAcceptedQuoteTask ForOakhurst()
        {
            var dto = QuoteAcceptanceDtoMother.CreateForOakhurst();
            return new RouteAcceptedQuoteTask(dto, "LITE", "AA", Guid.Empty, QuoteAcceptanceConfigurationMother.OakhurstConfig());
        }

        public static RouteAcceptedQuoteTask ForRegent()
        {
            var dto = QuoteAcceptanceDtoMother.CreateForRegent();
            return new RouteAcceptedQuoteTask(dto, "REGPRD", "REGENT", Guid.Empty, QuoteAcceptanceConfigurationMother.RegentConfig());
        }

        public static RouteAcceptedQuoteTask ForTelesure()
        {
            var dto = QuoteAcceptanceDtoMother.CreateForTelesure();
            return new RouteAcceptedQuoteTask(dto, "UNI", "UNITY", Guid.Empty, QuoteAcceptanceConfigurationMother.TelesureUnityConfig());
        }
    }
}
