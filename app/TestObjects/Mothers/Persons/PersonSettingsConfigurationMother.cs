﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Admin;
using Domain.Lookups.Person;
using MasterData;

namespace TestObjects.Mothers.Persons
{
    public class PersonSettingsConfigurationMother
    {
        public static List<PersonLookupSetting> ValidPersonLookupConfiguration()
        {
            var setting = new PersonLookupSetting(
                Countries.SouthAfrica
                , "1234567890asdfghjklZXCVBNM"
                , "valid@iplatform.co.za"
                , true
                , "valid@iplatform.co.za"
                , string.Empty
                , false
                , string.Empty
                , string.Empty
                , false
                , string.Empty
                , false
                , string.Empty
                , string.Empty
                , string.Empty
                );
            setting.Channel = new Channel(1);


            return new List<PersonLookupSetting>
            {
               setting
            };
        }
    }
}
