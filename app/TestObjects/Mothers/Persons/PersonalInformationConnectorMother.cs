﻿using System;
using System.Collections.Generic;
using iPerson.Api.DTOs.Connector;
using iPerson.Api.DTOs.PersonInformation;
using iPerson.Api.DTOs.Responses;
using RestSharp;

namespace TestObjects.Mothers.Persons
{
    public class PersonalInformationConnectorMother : PersonalInformationConnector
    {
        public PersonalInformationConnectorMother(IRestClient client) : base(client)
        {
        }

        public PersonalInformationConnectorMother()
        {
        }

    

        public new GETResponseDto<PersonInformationResponsesDto> Get(PersonInformationContext context, string idNumber = "",
            string phoneNumber = "", string email = "")
        {

            return new GETResponseDto<PersonInformationResponsesDto>(new PersonInformationResponsesDto()
            {
                Results = new List<PersonInformationCollectionDto>()
                {
                   new PersonInformationCollectionDto()
                   {
                       People = new List<PersonInformationResponseDto>()
                   ,Source = new PersonInformationSourceDto()
                   {
                       Id = 1,
                       Name = "PCUBED"
                   }
                   }
                }


            });

        }


    }
}