﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using MasterData.Authorisation;

namespace TestObjects.Mothers.Context
{
    public class ContextObjectMother
    {
        private readonly List<int> _channels = new List<int>();
        private readonly SystemAuthorisation _systemAuthorisation = new SystemAuthorisation();
        private readonly int _userId = 42;
        private readonly string _userName = "contextObjectMother@testing.com";

        public ExecutionContext Build()
        {
            return new ExecutionContext(string.Empty, _userId, _userName, _channels, 1, _systemAuthorisation);
        }

        public ContextObjectMother WithChannel(int channelId)
        {
            if (_channels.Any(c => c == channelId))
                return this;

            _channels.Add(channelId);
            return this;
        }

        public ContextObjectMother WithAuthorisation(ChannelAuthorisation channelAuthorisation)
        {
            WithChannel(channelAuthorisation.ChannelId);
            _systemAuthorisation.Channels.Add(channelAuthorisation);
            return this;
        }
    }
}