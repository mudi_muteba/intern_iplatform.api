﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Ratings.Events;
using iPlatform.Enums.Workflows;
using TestObjects.Mothers.Router;
using Domain.Admin.ChannelEvents;

namespace TestObjects.Mothers.Channels
{
    public class ChannelEventObjectMother
    {
        public static List<ChannelEvent> ValidChannelProductEvents(int channel = 1)
        {
            var channelEvent = new ChannelEvent(new Channel(channel),
                typeof(ExternalQuoteAcceptedWithUnderwritingEnabledEvent).Name, RouteAcceptedQuoteTaskMother.ForDotsure().ProductCode);

            channelEvent.SetTask(WorkflowMessageType.LeadTransferal);

            return new List<ChannelEvent>
            {
                channelEvent
            };
        }
    }
}