﻿using System;
using System.Collections.Generic;
using Domain.Admin;
using Domain.Lookups.VehicleGuide;
using MasterData;

namespace TestObjects.Mothers.Channels
{
    public class VehicleGuideConfigurationObjectMother
    {
        public static List<VehicleGuideSetting> ValidVehicleGuideConfiguration()
        {
            return new List<VehicleGuideSetting>
            {
                new VehicleGuideSetting(
                    new Channel(1)
                    , Countries.SouthAfrica
                    , "1234567890asdfghjklZXCVBNM"
                    , "valid@iplatform.co.za"
                    , true
                    , true
                    , string.Empty
                    , string.Empty
                    , null
                    , null
                    , null
                    , null
                    , String.Empty
                    , String.Empty
                    , String.Empty
                    , String.Empty
                    , false
                    , false
                    , false
                    , String.Empty
                    , String.Empty
                    , String.Empty
                    , String.Empty
                    , String.Empty
                    )
            };
        }
    }
}