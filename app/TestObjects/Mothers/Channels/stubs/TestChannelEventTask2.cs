﻿using Domain.Admin.ChannelEvents;

namespace TestObjects.Mothers.Channels.stubs
{
    public class TestChannelEventTask2 : ChannelEventTask
    {
        public bool WasCalled { get; private set; }
    }
}