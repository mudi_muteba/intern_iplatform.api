﻿using Domain.Admin.ChannelEvents;

namespace TestObjects.Mothers.Channels.stubs
{
    public class TestChannelEventTask1 : ChannelEventTask
    {
        public bool WasCalled { get; private set; }
    }
}