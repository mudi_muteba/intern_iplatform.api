﻿using System;
using Domain.Imports.Leads.Workflow.Metadata;

namespace TestObjects.Mothers.Lead
{
    public class ExcelImportMetadataMother
    {
        public static ExcelImportMetadata ValidFileMetadata
        {
            get
            {
                return new ExcelImportMetadata(3, 2, 1, "ValidLeads.xlsx", Convert.ToBase64String(LeadExcelFileMother.ValidFile));
            }
        }

        public static ExcelImportMetadata EmptyFileMetadata
        {
            get
            {
                return new ExcelImportMetadata(3, 2, 1, "EmptyLeads.xlsx", Convert.ToBase64String(LeadExcelFileMother.EmptyFile));
            }
        }
    }
}