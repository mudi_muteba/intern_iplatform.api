﻿using iPlatform.Api.DTOs.Leads;
using MasterData;
using TestObjects.Mothers.Individual;

namespace TestObjects.Mothers.Lead
{
    public class LeadBasicDtoMother
    {
        public static LeadBasicDto LeadBasicDto
        {
            get
            {
                var name = typeof(LeadBasicDto).Name;
                return new LeadBasicDto
                {
                    Id = typeof(LeadBasicDto).GetHashCode() + 1,
                    Name = name,
                    Individual = IndividualDtoMother.IndividualDto,
                    LeadStatus = ActivityTypes.Created
                };
            }
        }
    }
}