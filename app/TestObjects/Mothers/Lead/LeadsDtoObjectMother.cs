﻿using System;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Excel;
using iPlatform.Api.DTOs.Leads.Quality;
using MasterData;

namespace TestObjects.Mothers.Lead
{
    public class LeadsDtoObjectMother
    {
        public static LeadImportXMLDto ValidLeadImportXMLDtoWithCampaign(string actualXml)
        {
            return new LeadImportXMLDto
            {
                ChannelId = 1,
                XMLString = actualXml
            };
        }

        public static LeadImportExcelDto ValidLeadImportExcelDtoWithCampaign(int campaignId, string filename, Byte[] file)
        {
            return new LeadImportExcelDto(Guid.NewGuid(), filename, "Description", campaignId, 2, 1, file);
        }

        public static CreateLeadDto ValidLeadDto(int partyid, int campaignId)
        {
            return new CreateLeadDto
            {
                PartyId = partyid,
                ChannelId = 1,
                CampaignId = campaignId,
                LeadStatus = LeadStatuses.Created
            };
        }


        public static CreateLeadQualityDto ValidLeadQualityDto()
        {
            return new CreateLeadQualityDto
            {
                WealthIndex = string.Empty,
                CreditGradeNonCPA = string.Empty,
                CreditActiveNonCPA = false,
                MosaicCPAGroupMerged = string.Empty,
                DemLSM = string.Empty,
                FASNonCPAGroupDescriptionShort = string.Empty,
                IdentityNumber = string.Empty,
                Id = 0
            };
        }


        public static EditLeadQualityDto UpdatedvalidLeadQualityDto(int leadid)
        {
            return new EditLeadQualityDto
            {
                WealthIndex = "blah1",
                CreditGradeNonCPA = "blah2",
                CreditActiveNonCPA = false,
                MosaicCPAGroupMerged = "blah3",
                DemLSM = "blah4",
                FASNonCPAGroupDescriptionShort = "blah5",
                IdentityNumber = "blah6",
                Id = leadid
            };
        }
    }
}