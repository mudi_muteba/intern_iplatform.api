﻿namespace TestObjects.Mothers.Lead
{
    public class LeadExcelFileMother
    {
        public static byte[] ValidFile
        {
            get
            {
                return Properties.Resources.ValidLeads;
            }
        }
        public static byte[] EmptyFile
        {
            get
            {
                return Properties.Resources.EmptyLeads;
            }
        }
    }
}