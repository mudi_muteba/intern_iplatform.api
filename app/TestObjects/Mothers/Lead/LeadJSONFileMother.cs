﻿namespace TestObjects.Mothers.Lead
{
    public class LeadJSONFileMother
    {
        public static byte[] ValidLeadSubmitJson
        {
            get
            {
                return Properties.Resources.working_lead_submit;
            }
        }
    }
}