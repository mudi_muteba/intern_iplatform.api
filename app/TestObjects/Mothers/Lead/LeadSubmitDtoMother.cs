﻿using iPlatform.Api.DTOs.Leads;
using Newtonsoft.Json;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace TestObjects.Mothers.Lead
{
    public class LeadSubmitDtoMother
    {
        public static SubmitLeadDto SubmitLeadJSONDto
        {
            get
            {
                return DeserializeJson<SubmitLeadDto>(LeadJSONFileMother.ValidLeadSubmitJson);
            }
        }

        public static SubmitLeadDto SubmitLeadXMLDto
        {
            get
            {
                return DeserializeXML<SubmitLeadDto>(LeadXMLFileMother.ValidLeadSubmitJson);
            }
        }



        private static T DeserializeJson<T>(byte[] data) where T : class
        {
            using (var stream = new MemoryStream(data))
            using (var reader = new StreamReader(stream))
                return JsonSerializer.Create().Deserialize(reader, typeof(T)) as T;
        }

        private static T DeserializeXML<T>(string data) where T : class
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlReader reader = XmlReader.Create(new StringReader(data));
            return (T)serializer.Deserialize(reader);
        }




    }
}