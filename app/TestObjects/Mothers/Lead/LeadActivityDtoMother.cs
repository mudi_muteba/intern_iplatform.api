﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using TestObjects.Mothers.Activities;
using TestObjects.Mothers.Campaigns;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Users;

namespace TestObjects.Mothers.Lead
{
    public class LeadActivityDtoMother
    {
        public static LeadActivityDto LeadActivityDto
        {
            get
            {
                var name = typeof(LeadActivityDto).Name;
                return new LeadActivityDto
                {
                    Id = typeof(LeadActivityDto).GetHashCode() + 1,
                    DateCreated = new DateTimeDto(DateTime.UtcNow),
                    DateUpdated = new DateTimeDto(DateTime.UtcNow),
                    ActivityType = ActivityTypeDtoMother.ActivityTypeDto,
                    Description = name + "Description",
                    Links = new List<ResourceLink>(),
                    Lead = LeadBasicDtoMother.LeadBasicDto,
                    Party = BasicIndividualDtoMother.BasicIndividualDto,
                    User = ListUserDtoMother.ListUserDto,
                    Campaign = CampaignDtoMother.CampaignDto
                };
            }
        }
    }
}