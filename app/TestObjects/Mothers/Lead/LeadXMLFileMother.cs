﻿namespace TestObjects.Mothers.Lead
{
    public class LeadXMLFileMother
    {
        public static string ValidLeadSubmitJson
        {
            get
            {
                return Properties.Resources.working_lead_submit1;
            }
        }
    }
}