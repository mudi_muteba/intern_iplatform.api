﻿using System;

namespace TestObjects.Mothers.ItcQuestionDefinitions
{
    public class DefaultItcQuestionDefinition: IDefineItcQuestionDefinitionData
    {
        private int? _id;
        private int? _channelId;
        private string _name;
        private string _questionText;
        private DateTime _created;
        private DateTime? _updated;
        private bool _deleted;

        public int? Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name {
            get
            {
                _name = "Default";
                return _name;
            }

            set { _name = value; }
        }

        public int? ChannelId
        {
            get { return _channelId; }
            set { _channelId = value; }
        }

        public string QuestionText
        {
            get
            {
                _questionText = "bla bla bla ##PLACEHOLDER## asdasdasdasd";
                return _questionText;
            }
            set { _questionText = value; }
        }

        public DateTime DateCreated
        {
            get
            {
                _created = DateTime.UtcNow;
                return _created;
            }

            set { _created = value; }
        }

        public DateTime? DateUpdated
        {
            get
            {
                _updated = null;
                return _updated;
            }
            set { _updated = value; }
        }

        public bool IsDeleted
        {
            get
            {
                _deleted = false;
                return _deleted;
            }
            set { _deleted = value; }
        }
    }
}