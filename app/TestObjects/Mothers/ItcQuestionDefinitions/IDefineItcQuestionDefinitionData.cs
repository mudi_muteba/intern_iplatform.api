﻿using System;

namespace TestObjects.Mothers.ItcQuestionDefinitions
{
    public interface IDefineItcQuestionDefinitionData
    {
        int? Id { get; set; }
        string Name { get; set; }
        int? ChannelId { get; set; }
        string QuestionText { get; set; }
        DateTime DateCreated { get; set; }
        DateTime? DateUpdated { get; set; }
        bool IsDeleted { get; set; }
    }
}