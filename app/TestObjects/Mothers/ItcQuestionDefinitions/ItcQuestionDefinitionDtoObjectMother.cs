﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.ItcQuestionDefinition;
using iPlatform.Api.DTOs.ItcQuestion;

namespace TestObjects.Mothers.ItcQuestionDefinitions
{
    public class ItcQuestionDefinitionDtoObjectMother
    {
        private static IEnumerable<string> m_Channels = new[] {"Default", "AIG", "PSG", "HOL", "IP"};

        public static CreateItcQuestionDto NewITCQuestion()
        {
            var data = new DefaultItcQuestionDefinition();
            return new CreateItcQuestionDto
            {
                Name = data.Name,
                QuestionText = data.QuestionText
            };
        }
        
        public static CreateItcQuestionDto NewITCQuestionWithChannel()
        {
            var data = new DefaultItcQuestionDefinition();
            return new CreateItcQuestionDto
            {
                Id = 0,
                Name = data.Name,
                ChannelId = new Random().Next(1, m_Channels.Count()),
                QuestionText = data.QuestionText
            };
        }

        public static GetItcQuestionDto GetITCQuestionWithId()
        {
            return new GetItcQuestionDto
            {
                Id = new Random().Next(1, m_Channels.Count())
            };
        }

        public static GetItcQuestionDto GetITCQuestionWithChannelCode()
        {
            return new GetItcQuestionDto
            {
                Id = new Random().Next(1, m_Channels.Count()),
                ChannelId = new Random().Next(1, m_Channels.Count())
            };
        }

        public static GetItcQuestionDto GetITCQuestionWithNonMappedChannelCode()
        {
            return new GetItcQuestionDto
            {
                Id = new Random().Next(1, m_Channels.Count()),
                ChannelId = new Random().Next(1, m_Channels.Count()),
                ChannelName = "XYZ"
            };
        }

        public static GetItcQuestionDto GetITCQuestionWithChannelId()
        {
            return new GetItcQuestionDto
            {
                Id = new Random().Next(1, m_Channels.Count()),
                ChannelId = new Random().Next(1, m_Channels.Count())
            };
        }

        public static GetItcQuestionDto GetITCQuestionWithNonMappedChannelId()
        {
            return new GetItcQuestionDto
            {
                Id = new Random().Next(m_Channels.Count() + 1, m_Channels.Count() * 2),
                ChannelId = new Random().Next(1, m_Channels.Count()),
                ChannelName = "XYZ"
            };
        }

        public static UpdateItcQuestionDto GetExistingItcQuestionDtoForUpdate()
        {
            return new UpdateItcQuestionDto
            {
                Id = new Random().Next(1, m_Channels.Count()),
                ChannelId = new Random().Next(1, m_Channels.Count()),
                QuestionText = String.Join(" ", m_Channels),
                Name = m_Channels.ToArray()[new Random().Next(1, m_Channels.Count())]
            };
        }

        public static UpdateItcQuestionDto GetNonExistingItcQuestionDtoForUpdate()
        {
            return new UpdateItcQuestionDto
            {
                Id = new Random().Next(m_Channels.Count() + 1, m_Channels.Count() * 2),
                ChannelId = new Random().Next(1, m_Channels.Count()),
                QuestionText = String.Join(" ", m_Channels),
                Name = m_Channels.ToArray()[new Random().Next(1, m_Channels.Count())]
            };
        }

        public static RemoveItcQuestionDto ExistingITCQuestionToBeRemoved()
        {
            return new RemoveItcQuestionDto
            {
                Id = new Random().Next(1, m_Channels.Count())
            };
        }

        public static RemoveItcQuestionDto NonExistingITCQuestionToBeRemoved()
        {
            return new RemoveItcQuestionDto
            {
                Id = new Random().Next(m_Channels.Count()+1, m_Channels.Count()*2)
            };
        }

        public static IEnumerable<Channel> GetExistingChannels()
        {
            var results = new List<Channel>();
            for (int index = 0; index < m_Channels.Count(); index++)
            {
             results.Add(new Channel(index + 1, true, Guid.NewGuid(), DateTime.UtcNow, null, true)
             {
                 ExternalReference = new Random().Next(1, m_Channels.Count()).ToString()
             });   
            }

            return results;
        }

        public static IEnumerable<ItcQuestionDefinition> GetExistingItcQuestionDefinitions()
        {
            var results = new List<ItcQuestionDefinition>();
            var channels = m_Channels.ToList();
            for (int i = 0; i < m_Channels.Count(); i++)
            {
                var channelId = channels[i] == "Default" ? (int?)null : (i + 1);
                results.Add(new ItcQuestionDefinition(i + 1, channels[i], Guid.NewGuid().ToString(), DateTime.UtcNow, channelId, null));    
            }
            
            return results;
        }
    }
}