﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using iPlatform.Api.DTOs.Policy;

namespace TestObjects.Mothers.Claims
{
    public class NewClaimsItemDtoObjectMother
    {
        public static CreateClaimsItemsDto ValidCreateClaimsItemsDto(int claimHeaderId, int partyId,List<PolicyItemDto> policyItems, int ChannelId)
        {
            var dto = new CreateClaimsItemsDto
            {
                ClaimsHeaderId = claimHeaderId,
                PartyId = partyId,
                claimDto = new ClaimsItemDtos(),
                ChannelId = ChannelId
            };

            foreach (var policy in policyItems)
            {
                dto.claimDto.PolicyItems.Add(new ClaimsItemDto
                {
                    PolicyItemId = policy.Id,
                    ClaimTypeId = 1,
                    Description = "blah blah" + DateTime.UtcNow
                });
            }

            return dto;
        }

        public static SaveClaimsItemAnswersDto ValidSaveAnswers(int claimHeaderId, int claimItemId, ListClaimsItemDto claimItems)
        {
            var dto = new SaveClaimsItemAnswersDto();
            dto.ClaimsHeaderId = claimHeaderId;
            dto.Id = claimItemId;

            if (claimItems.Answers == null)
                return dto;

            foreach (var item in claimItems.Answers)
            {
                dto.Answers.Add(new SaveClaimsItemAnswerDto
                {
                    Id = item.Id,
                    Answer = "blah blah" + DateTime.UtcNow,
                });
            }
            return dto;
        }
    }
}