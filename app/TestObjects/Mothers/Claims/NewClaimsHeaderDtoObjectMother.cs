﻿using iPlatform.Api.DTOs.Claims.ClaimsHeaders;

namespace TestObjects.Mothers.Claims
{
    public class NewClaimsHeaderDtoObjectMother
    {
        public static CreateClaimsHeaderDto ValidCreateClaimsHeaderDto()
        {
            var dto = new CreateClaimsHeaderDto();
            dto.ChannelId = 1;
            return dto;
        }

        public static ClaimSearchDto ValidClaimSearchDto()
        {
            return new ClaimSearchDto { PolicyHeaderId = 1 };
        }
    }
}