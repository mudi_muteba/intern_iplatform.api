﻿using System;
using iPlatform.Api.DTOs.FuneralMembers;

namespace TestObjects.Mothers.FuneralMembers
{
    public class NewFuneralMemberDtoObjectMother
    {
        public static CreateFuneralMemberDto ValidFuneralMemberDto()
        {
            return new CreateFuneralMemberDto
            {
                MemberRelationshipId = 1,
                Initials = "OS",
                Surname = "Seewoogoolam",
                IdNumber = "S14051983465445646",
                IsStudent = false,
                GenderId = 1,
                DateOnCover = DateTime.UtcNow,
                SumInsured = "10500",
                ProposalDefinitionId = 86
            };
        }

        public static EditFuneralMemberDto ValidEditFuneralMemberDto()
        {
            return new EditFuneralMemberDto
            {
                MemberRelationshipId = 1,
                Initials = "BL",
                Surname = "blah blah",
                IdNumber = "S14051983465445646",
                IsStudent = false,
                GenderId = 1,
                DateOnCover = DateTime.UtcNow,
                SumInsured = "250",
                ProposalDefinitionId = 86
            };
        }

    }
}