﻿using iPlatform.Api.DTOs.Activities;

namespace TestObjects.Mothers.Activities
{
    public class ActivityTypeDtoMother
    {
        public static ActivityTypeDto ActivityTypeDto
        {
            get
            {
                var name = typeof(ActivityTypeDto).Name;
                return new ActivityTypeDto
                {
                    Id = typeof(ActivityTypeDto).GetHashCode() + 1,
                    Name = name
                };
            }
        }
    }
}