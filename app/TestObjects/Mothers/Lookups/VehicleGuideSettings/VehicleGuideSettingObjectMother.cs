﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;

namespace TestObjects.Mothers.Lookups.VehicleGuideSettings
{
    public class VehicleGuideSettingObjectMother
    {
        public static CreateVehicleGuideSettingDto ValidCreateVehicleGuideSettingDto()
        {
            return new CreateVehicleGuideSettingDto()
            {
                ChannelId = 1,
                ApiKey = "TestApiKey",
                CountryId = 1,
                Email = "test@test.test",
                Evalue8Enabled = false,
                Evalue8Password = "Evalue8Password",
                Evalue8Username = "Evalue8Username",
                KeAutoKey = "TestAutoKey",
                KeAutoSecret = "TestAutoSecret",
                LSA_ContractId = Guid.NewGuid(),
                LSA_CustomerId = Guid.NewGuid(),
                LSA_PackageId = Guid.NewGuid(),
                LSA_Password = "LSA_Password",
                LSA_UserId = Guid.NewGuid(),
                LSA_UserName = "LSA_UserName",
                LightstoneEnabled = false,
                MMBookEnabled = false,
                TransUnionAutoEnabled = false
            };
        }

        public static EditVehicleGuideSettingDto ValidEditVehicleGuideSettingDto(int id)
        {
            CreateVehicleGuideSettingDto dto = ValidCreateVehicleGuideSettingDto();
            return new EditVehicleGuideSettingDto()
            {
                Id = id,
                ChannelId = dto.ChannelId,
                ApiKey = dto.ApiKey,
                CountryId = dto.CountryId,
                Email = dto.Email,
                Evalue8Enabled = dto.Evalue8Enabled,
                Evalue8Password = dto.Evalue8Password,
                Evalue8Username = dto.Evalue8Username,
                KeAutoKey = dto.KeAutoKey,
                KeAutoSecret = dto.KeAutoSecret,
                LSA_ContractId = dto.LSA_ContractId,
                LSA_CustomerId = dto.LSA_CustomerId,
                LSA_PackageId = dto.LSA_PackageId,
                LSA_Password = dto.LSA_Password,
                LSA_UserId = dto.LSA_UserId,
                LSA_UserName = dto.LSA_UserName,
                LightstoneEnabled = dto.LightstoneEnabled,
                MMBookEnabled = dto.MMBookEnabled,
                TransUnionAutoEnabled = dto.TransUnionAutoEnabled
            };
        }
    }
}
