﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.JsonDataStores;

namespace TestObjects.Mothers.JsonDataStores
{
    public static class JsonDataStoreDtoMother
    {
        public static CreateJsonDataStoreDto CreateJsonDataStoreDto
        {
            get
            {
                return new CreateJsonDataStoreDto
                {
                    SessionId = "32wwxictd54z3gnrz33pzeo3",
                    ChannelId = 1,
                    PartyId = 999999999,
                    Section = "TestSection",
                    SavePoint = 1,
                    TransactionDate = DateTime.UtcNow
                };
            }
        }

        public static JsonDataStoreSearchDto JsonDataStoreSearchDto
        {
            get
            {
                return new JsonDataStoreSearchDto
                {
                    SessionId = "32wwxictd54z3gnrz33pzeo3"
                };
            }
        }

        public static JsonDataStoreSearchDto JsonDataStoreSearchDtoWithLatest
        {
            get
            {
                return new JsonDataStoreSearchDto
                {
                    SessionId = "32wwxictd54z3gnrz33pzeo3",
                    PartyId = 999999999,
                    GetLatestByPartyId = true
                };
            }
        }

        public static List<CreateJsonDataStoreDto> ListJsonCreateDataStoreDtos
        {
            get
            {
                return new List<CreateJsonDataStoreDto>
                {
                    { new CreateJsonDataStoreDto
                        {
                            SessionId = "32wwxictd54z3gnrz33pzeo3",
                            ChannelId = 1,
                            PartyId = 999999999,
                            Section = "TestSection2",
                            SavePoint = 1,
                            TransactionDate = DateTime.UtcNow
                        }
                    },
                    { new CreateJsonDataStoreDto
                        {
                            SessionId = "32wwxictd54z3gnrz33pzeo3",
                            ChannelId = 1,
                            PartyId = 999999999,
                            Section = "TestSection3",
                            SavePoint = 2,
                            TransactionDate = DateTime.UtcNow
                        }
                    },
                    { new CreateJsonDataStoreDto
                        {
                            SessionId = "32wwxictd54z3gnrz33pzeo3",
                            ChannelId = 1,
                            PartyId = 999999999,
                            Section = "TestSection4",
                            SavePoint = 3,
                            TransactionDate = DateTime.UtcNow
                        }
                    },
                    { new CreateJsonDataStoreDto
                        {
                            SessionId = "32wwxictd54z3gnrz33pzeo3",
                            ChannelId = 1,
                            PartyId = 999999999,
                            Section = "TestSection5",
                            SavePoint = 1,
                            TransactionDate = DateTime.UtcNow
                        }
                    }
                };
            }
        }
    }
}
