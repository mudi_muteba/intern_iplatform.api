﻿using System;
using iPlatform.Api.DTOs.AdditionalMembers;

namespace TestObjects.Mothers.AdditionalMembers
{
    public class NewAdditionalMemberDtoObjectMother
    {
        public static CreateAdditionalMemberDto ValidAdditionalMemberDto()
        {
            return new CreateAdditionalMemberDto
            {
                MemberRelationshipId = 1,
                Initials = "OS",
                Surname = "Seewoogoolam",
                IdNumber = "8708070" + new Random().Next(999999),
                IsStudent = false,
                GenderId = 1,
                DateOnCover = DateTime.UtcNow,
                SumInsured = "10500",
                ProposalDefinitionId = 228,
                DateOfBirth = new DateTime(1988, 01, 01).ToUniversalTime()
            };
        }

        public static EditAdditionalMemberDto ValidEditAdditionalMemberDto()
        {
            return new EditAdditionalMemberDto
            {
                MemberRelationshipId = 1,
                Initials = "BL",
                Surname = "blah blah",
                IdNumber = "8708070" + new Random().Next(999999),
                IsStudent = false,
                GenderId = 1,
                DateOnCover = DateTime.UtcNow,
                SumInsured = "250",
                ProposalDefinitionId = 228,
                DateOfBirth = new DateTime(1988, 01, 01).ToUniversalTime()
            };
        }

    }
}