﻿using iPlatform.Api.DTOs.HistoryLoss.LossHistory;

namespace TestObjects.Mothers.HistoryLoss
{
    public class NewLossHistoryDtoObjectMother
    {
        public CreateLossHistoryDto CreateLossHistoryDto
        {
            get
            {
                return new CreateLossHistoryDto
                {
                    PartyId = 33,
                    CurrentlyInsuredId = 5,
                    InsurerCancel = true,
                    InterruptedPolicyClaim = true,
                    PreviousComprehensiveInsurance = true,
                    UninterruptedPolicyId = 5,
                    UninterruptedPolicyNoClaimId = 5,
                    ContactId = 144,
                    MissedPremiumLast6Months = true
                };
            }
        }

        public EditLossHistoryDto EditLossHistoryDto
        {
            get
            {
                return new EditLossHistoryDto
                {
                    PartyId = 33,
                    CurrentlyInsuredId = 1,
                    InsurerCancel = false,
                    InterruptedPolicyClaim = false,
                    PreviousComprehensiveInsurance = false,
                    UninterruptedPolicyId = 1,
                    UninterruptedPolicyNoClaimId = 1,
                    MissedPremiumLast6Months = false
                };
            }
        }
    }
}
