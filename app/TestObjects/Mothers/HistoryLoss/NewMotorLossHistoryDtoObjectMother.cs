﻿using iPlatform.Api.DTOs.HistoryLoss.Motor;

namespace TestObjects.Mothers.HistoryLoss
{
    public class NewMotorLossHistoryDtoObjectMother
    {
        public static CreateMotorLossHistoryDto ValidMotorLossHistoryDto()
        {
            return new CreateMotorLossHistoryDto
            {
                MotorTypeOfLossId = 1,
                DateOfLoss = "2015-01-01",
                MotorClaimAmountId = 1,
                CurrentlyInsuredId = 1,
                UninterruptedPolicyId = 1,
                InsurerCancel = true,
                MotorCurrentTypeOfCoverId = 1,
                PreviousComprehensiveInsurance = true,
                MotorUninterruptedPolicyNoClaimId = 1,
                Why = "because i wanted"
            };
        }

        public static EditMotorLossHistoryDto ValidEditMotorLossHistoryDto()
        {
            return new EditMotorLossHistoryDto
            {
                MotorTypeOfLossId = 2,
                DateOfLoss = "2014-12-12",
                MotorClaimAmountId = 2,
                CurrentlyInsuredId = 2,
                UninterruptedPolicyId = 2,
                InsurerCancel = false,
                MotorCurrentTypeOfCoverId = 2,
                PreviousComprehensiveInsurance = false,
                MotorUninterruptedPolicyNoClaimId = 2,
                Why = "blah blah"
            };
        }

    }
}