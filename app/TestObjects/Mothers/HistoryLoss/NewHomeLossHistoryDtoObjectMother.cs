﻿using iPlatform.Api.DTOs.HistoryLoss.Home;

namespace TestObjects.Mothers.HistoryLoss
{
    public class NewHomeLossHistoryDtoObjectMother
    {
        public static CreateHomeLossHistoryDto ValidHomeLossHistoryDto()
        {
            return new CreateHomeLossHistoryDto
            {
                HomeTypeOfLossId = 1,
                DateOfLoss = "2015-01-01",
                HomeClaimAmountId = 1,
                CurrentlyInsuredId = 1,
                UninterruptedPolicyId = 1,
                InsurerCancel = true,
                HomeClaimLocationId = 1,
                Why = "because i wanted"
            };
        }

        public static EditHomeLossHistoryDto ValidEditHomeLossHistoryDto()
        {
            return new EditHomeLossHistoryDto
            {
                HomeTypeOfLossId = 2,
                DateOfLoss = "2014-12-12",
                HomeClaimAmountId = 2,
                CurrentlyInsuredId = 2,
                UninterruptedPolicyId = 2,
                InsurerCancel = false,
                HomeClaimLocationId = 2,
                Why = "blah blah"
            };
        }

    }
}