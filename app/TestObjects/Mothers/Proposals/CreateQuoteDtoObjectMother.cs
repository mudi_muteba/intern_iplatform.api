﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups;
using iPlatform.Api.DTOs.Ratings.Quoting;

namespace TestObjects.Mothers.Proposals
{
    public class CreateQuoteDtoObjectMother
    {
        public static CreateQuoteDto ForNewQuote()
        {
            return new CreateQuoteDto()
            {
                ChannelId = 2,
                IndividualId = 95,
                ProposalHeaderId = 86
            };
        }


        public static CreateQuoteUploadLogDto CreateUploadLog()
        {
            return new CreateQuoteUploadLogDto
            {
                ProductId =  27,
                QuoteId = 1,
                Fees = 100,
                Premium = 2000,
                Sasria = 2,
                Message = "Success",
                RequestId = Guid.NewGuid().ToString()
            };
        }
    }
}
