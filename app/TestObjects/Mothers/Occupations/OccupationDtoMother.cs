﻿using iPlatform.Api.DTOs.Occupation;

namespace TestObjects.Mothers.Occupations
{
    public class OccupationDtoMother
    {
        public static OccupationDto OccupationDto
        {
            get
            {
                var name = typeof(OccupationDto).Name;
                return new OccupationDto
                {
                    Id = typeof(OccupationDto).GetHashCode() + 1,
                    Name = name + "name",
                    Code = name + "code"
                };
            }
        }
    }
}