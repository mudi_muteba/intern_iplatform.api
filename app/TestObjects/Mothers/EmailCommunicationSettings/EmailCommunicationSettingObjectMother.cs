﻿using System;
using MasterData;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;

namespace TestObjects.Mothers.EmailCommunicationSettings
{
    public class EmailCommunicationSettingObjectMother
    {
        public static CreateEmailCommunicationSettingDto ValidDto()
        {
            return new CreateEmailCommunicationSettingDto
            {
                ChannelId = 1,
                Host = "smtp.gmail.com",
                Port = 587,
                DefaultContactNumber = "+27 11 300 1100",
                Username = "iplatform@iplatform.co.za",
                Password = "p8nynyocRLhoCeN2wdoN8A",
                DefaultFrom = "iPlatform<iplatform@platform.co.za>",
                SubAccountID = "QuoteAcceptance",
                UseDefaultCredentials = false,
                UseSSL = true,
                DefaultBCC = ""
            };
        }
    }
}