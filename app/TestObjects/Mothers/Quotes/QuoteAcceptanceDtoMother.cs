﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.QuoteAcceptance;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using TestObjects.Mothers.Ratings.Request;
using TestObjects.Mothers.Ratings.Result;

namespace TestObjects.Mothers.Quotes
{
    public class QuoteAcceptanceDtoMother
    {
        public static PublishQuoteUploadMessageDto CreateForOakhurst()
        {
            return new PublishQuoteUploadMessageDto()
            {
                InsuredInfo = new InsuredInfoDto
                {
                    ContactNumber = "0798934451",
                    EmailAddress = "support@cardinal.co.za",
                    Firstname = "Jannie",
                    IDNumber = "6603285990086",
                    ITCConsent = false,
                    MaritalStatus = MaritalStatuses.Married,
                    Surname = "Vermaak",
                    Title = Titles.Magistrate
                },
                Request = RatingRequestObjectMother.SimpleMultiCoverRequest,
                Policy = RatingResultPolicyObjectMother.OakhurstRatingResultPolicy,
                ChannelInfo = GetChannelDto(),
                AgentDetail = GetAgentDetail()

            };
        }

        public static PublishQuoteUploadMessageDto CreateForTelesure()
        {
            return new PublishQuoteUploadMessageDto()
            {
                InsuredInfo = new InsuredInfoDto
                {
                    ContactNumber = "0798934451",
                    EmailAddress = "support@cardinal.co.za",
                    Firstname = "Jannie",
                    IDNumber = "6603285990086",
                    ITCConsent = false,
                    MaritalStatus = MaritalStatuses.Married,
                    Surname = "Vermaak",
                    Title = Titles.Magistrate
                },
                Request = RatingRequestObjectMother.SimpleMultiCoverRequest,
                Policy = RatingResultPolicyObjectMother.TelesureRatingResultPolicy,
                ChannelInfo = GetChannelDto(),
                AgentDetail = GetAgentDetail()
            };
        }

        public static PublishQuoteUploadMessageDto CreateForHollard()
        {
            return new PublishQuoteUploadMessageDto()
            {
                InsuredInfo = new InsuredInfoDto
                {
                    ContactNumber = "0798934451",
                    EmailAddress = "support@cardinal.co.za",
                    Firstname = "Jannie",
                    IDNumber = "6603285990086",
                    ITCConsent = false,
                    MaritalStatus = MaritalStatuses.Married,
                    Surname = "Vermaak",
                    Title = Titles.Magistrate
                },
                Request = RatingRequestObjectMother.SimpleMultiCoverRequest,
                Policy = RatingResultPolicyObjectMother.HollardRatingResultPolicy,
                ChannelInfo = GetChannelDto(),
                AgentDetail = GetAgentDetail()
            };
        }

        public static PublishQuoteUploadMessageDto CreateForKingPrice()
        {
            return new PublishQuoteUploadMessageDto()
            {
                InsuredInfo = new InsuredInfoDto
                {
                    ContactNumber = "0798934451",
                    EmailAddress = "support@cardinal.co.za",
                    Firstname = "Jannie",
                    IDNumber = "6603285990086",
                    ITCConsent = false,
                    MaritalStatus = MaritalStatuses.Married,
                    Surname = "Vermaak",
                    Title = Titles.Magistrate
                },
                Request = RatingRequestObjectMother.SimpleMultiCoverRequest,
                Policy = new RatingResultPolicyDto()
                {
                    InsurerCode = "KPI",
                    ProductCode = "KPIPERS"
                },
                ChannelInfo = GetChannelDto(),
                AgentDetail = GetAgentDetail()
            };
        }

        public static PublishQuoteUploadMessageDto CreateForDotsure()
        {
            return new PublishQuoteUploadMessageDto()
            {
                InsuredInfo = new InsuredInfoDto
                {
                    ContactNumber = "0798934451",
                    EmailAddress = "support@cardinal.co.za",
                    Firstname = "Jannie",
                    IDNumber = "6603285990086",
                    ITCConsent = false,
                    MaritalStatus = MaritalStatuses.Married,
                    Surname = "Vermaak",
                    Title = Titles.Magistrate
                },
                Request = RatingRequestObjectMother.SimpleMultiCoverRequest,
                Policy = RatingResultPolicyObjectMother.DotsureRatingResultPolicy,
                ChannelInfo = GetChannelDto(),
                AgentDetail = GetAgentDetail()
            };
        }

        public static PublishQuoteUploadMessageDto CreateForRegent()
        {
            return new PublishQuoteUploadMessageDto()
            {
                InsuredInfo = new InsuredInfoDto
                {
                    ContactNumber = "0798934451",
                    EmailAddress = "support@cardinal.co.za",
                    Firstname = "Jannie",
                    IDNumber = "6603285990086",
                    ITCConsent = false,
                    MaritalStatus = MaritalStatuses.Married,
                    Surname = "Vermaak",
                    Title = Titles.Magistrate
                },
                Request = RatingRequestObjectMother.SimpleMultiCoverRequest,
                Policy = RatingResultPolicyObjectMother.RegentRatingResultPolicy,
                ChannelInfo = GetChannelDto(),
                AgentDetail = GetAgentDetail()
            };
        }

        public static PublishQuoteUploadMessageDto CreateForCampaignSantam()
        {
            var quote = new PublishQuoteUploadMessageDto()
            {
                InsuredInfo = new InsuredInfoDto
                {
                    ContactNumber = "0798934451",
                    EmailAddress = "support@cardinal.co.za",
                    Firstname = "Jannie",
                    IDNumber = "6603285990086",
                    ITCConsent = false,
                    MaritalStatus = MaritalStatuses.Married,
                    Surname = "Vermaak",
                    Title = Titles.Magistrate
                },
                Request = RatingRequestObjectMother.SimpleMotorOnlyRequest,
                Policy = RatingResultPolicyObjectMother.SantamRatingResultPolicy,
                ChannelInfo = GetChannelDto(),
                AgentDetail = GetAgentDetail()

            };

            quote.Policy.ProductCode = "MULPLX";
            return quote;
        }

        private static ChannelDto GetChannelDto()
        {
            return new ChannelDto
            {
                EmailCommunicationSetting = new EmailCommunicationSettingDto
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    DefaultContactNumber = "+27 11 300 1100",
                    Username = "iplatform@iplatform.co.za",
                    Password = "p8nynyocRLhoCeN2wdoN8A",
                    DefaultFrom = "iPlatform <iplatform@platform.co.za>",
                    SubAccountID = "QuoteAcceptance",
                    UseDefaultCredentials = false,
                    UseSSL = true,
                    DefaultBCC = ""
                }
            };
        }


        private static AgentDetailDto GetAgentDetail()
        {
            return new AgentDetailDto()
            {
                EmailAddress = "root@iplatform.co.za",
                Name = "Administrator Root",
                UserId = 1,
                Comments = "Blah blah comment"

            };
        }
    }
}