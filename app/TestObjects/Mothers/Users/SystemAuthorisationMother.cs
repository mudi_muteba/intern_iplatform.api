﻿using MasterData.Authorisation;

namespace TestObjects.Mothers.Users
{
    public class SystemAuthorisationMother
    {
        public static SystemAuthorisation AdminAuthorisation()
        {
            return new SystemAuthorisation(
                new ChannelAuthorisation(1, UserAuthorisation.Full),
                new ChannelAuthorisation(2, UserAuthorisation.Full)
                );
        }
    }
}