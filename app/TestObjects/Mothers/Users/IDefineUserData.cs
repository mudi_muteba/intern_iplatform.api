﻿namespace TestObjects.Mothers.Users
{
    public interface IDefineUserData
    {
        string UserName { get; }
        string Password { get; }
    }
}