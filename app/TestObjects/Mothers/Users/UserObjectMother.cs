﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Users;
using iPlatform.Api.DTOs.Users;
using MasterData;

namespace TestObjects.Mothers.Users
{
    public class UserObjectMother
    {
        public static User AdminUser()
        {
            var channels = new List<Channel>();
            channels.Add(new Channel(1, 1, "dd MMM yyyy", 2));
            channels.Add(new Channel(2, 5, "dd/MM/yyyy", 1));
            var adminUser = new User("admin@iplatform.co.za", "Password1", channels);

            foreach (var channel in channels)
            {
                adminUser.AllocateToAuthorisationGroups(new List<UserAuthorisationGroupDto>()
                {
                    new UserAuthorisationGroupDto(new List<int> {channel.Id}, new List<AuthorisationGroup>()
                    {
                        AuthorisationGroups.Admin
                    })
                });
            }

            return adminUser;
        }

        public static User UserWithNoGroups()
        {
            return new User("no_groups@nowhere.com", "This is the password", new List<int>() {1, 2});
        }

        public static User UserWithOneGroupInOneChannel()
        {
            var user = new User("one_group_one_channel@nowhere.com", "This is the password", new List<int>() {1});
            var authorisation = new List<UserAuthorisationGroupDto>()
            {
                new UserAuthorisationGroupDto(new List<int> {1}, new List<AuthorisationGroup>()
                {
                    AuthorisationGroups.CallCentreAgent
                })
            };

            user.AllocateToAuthorisationGroups(authorisation);

            return user;
        }

        public static User UserWithOneGroupInMultipleChannels()
        {
            var user = new User("one_group_multiple_channels@nowhere.com", "This is the password",
                new List<int>() {1, 2});
            var authorisation = new List<UserAuthorisationGroupDto>()
            {
                new UserAuthorisationGroupDto(new List<int> {1}, new List<AuthorisationGroup>()
                {
                    AuthorisationGroups.CallCentreAgent
                }),
                new UserAuthorisationGroupDto(new List<int> {2}, new List<AuthorisationGroup>()
                {
                    AuthorisationGroups.CallCentreManager
                }),
            };

            user.AllocateToAuthorisationGroups(authorisation);

            return user;
        }

        public static User UserWithMultipleGroupsInMultipleChannels()
        {
            var user = new User("mulitple_group_multiple_channels@nowhere.com", "This is the password",
                new List<int>() {1, 2});

            var authorisation = new List<UserAuthorisationGroupDto>()
            {
                new UserAuthorisationGroupDto(new List<int> { 1}, new List<AuthorisationGroup>()
                {
                    AuthorisationGroups.CallCentreAgent,
                    AuthorisationGroups.CallCentreManager,
                }),
                new UserAuthorisationGroupDto(new List<int> {2}, new List<AuthorisationGroup>()
                {
                    AuthorisationGroups.CallCentreAgent,
                    AuthorisationGroups.CallCentreManager
                }),
            };

            user.AllocateToAuthorisationGroups(authorisation);

            return user;
        }

        public static User UserWithMultipleGroupsInOneChannel()
        {
            var user = new User("mulitple_group_one_channel@nowhere.com", "This is the password", new List<int>() {1});
            var authorisation = new List<UserAuthorisationGroupDto>()
            {
                new UserAuthorisationGroupDto(new List<int> {1}, new List<AuthorisationGroup>()
                {
                    AuthorisationGroups.CallCentreAgent,
                    AuthorisationGroups.CallCentreManager
                })
            };

            user.AllocateToAuthorisationGroups(authorisation);

            return user;
        }

        public static User ExistingUser()
        {
            var data = new DefaultUserData();

            return new User(data.UserName, data.Password, new List<int>() {1, 2});
        }

        public static User ExistingFixUser()
        {
            var data = new DefaultFixUserData();

            return new User(data.UserName, data.Password, new List<int>() { 1, 2 });
        }
    }
}