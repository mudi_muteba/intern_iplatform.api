﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Users;
using MasterData;
using TestObjects.Mothers.Individual;
using System;

namespace TestObjects.Mothers.Users
{
    public class UserDtoObjectMother
    {
        public static RegisterUserDto NewUserRegistration()
        {
            var data = new DefaultUserData();

            return new RegisterUserDto("Name", "Surname", data.UserName, "8410155284089", "0821234567");
        }

        public static RegisterUserDto NewUserRegistration2()
        {
            var data = new DefaultUserData();
            return new RegisterUserDto(data.UserName, "testsurname", data.UserName, "8410155284087", "0821234569");
        }

        public static CreateUserDto ValidApprovedNewUser()
        {
            var data = new DefaultUserData();
            var createIndividualDto = NewIndividualDtoObjectMother.ValidBasicIndividualDto();
            createIndividualDto.SetContext(new DtoContext(1, data.UserName, string.Empty));
            return new CreateUserDto(data.UserName, data.Password, true)
            {
                Channels = new List<int>
                {
                    1,
                    2
                },
                Groups = new List<UserAuthorisationGroupDto>
                {
                    new UserAuthorisationGroupDto(new List<int> {1},
                        new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent}),
                    new UserAuthorisationGroupDto(new List<int> {2},
                        new List<AuthorisationGroup>
                        {
                            AuthorisationGroups.CallCentreAgent,
                            AuthorisationGroups.CallCentreManager
                        }),
                },
                CreateIndividualDto = createIndividualDto,
                ExternalReference = "Referencev" + new Random().Next(100, 9999).ToString()
            };
        }

        public static IEnumerable<CreateUserDto> GenerateValidApprovedNewUsers(int numberToGenerate, string baseUserName, List<int> channels = null)
        {
            var data = new DefaultUserData();
            channels = channels ?? new List<int> {1, 2};

            for (var n = 0; n < numberToGenerate; n++)
            {
                var createIndividualDto = NewIndividualDtoObjectMother.ValidBasicIndividualDto();
                createIndividualDto.FirstName = createIndividualDto.FirstName + n;
                createIndividualDto.Surname = createIndividualDto.Surname + n;
                var userName = string.Format("{0}_{1}@iplatform.co.za", baseUserName, n);
                //var userName = String.Format("{0}{1}{2}@nowhere.com", new Random().Next(100, 999), baseUserName, new Random().Next(1000, 9999));

                createIndividualDto.SetContext(new DtoContext(1, userName, string.Empty));
                yield return new CreateUserDto(userName, data.Password, true)
                {
                    Channels = channels,
                    CreateIndividualDto = createIndividualDto,
                    Groups = new List<UserAuthorisationGroupDto>
                    {
                        new UserAuthorisationGroupDto(new List<int> {1},
                            new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent}),
                        new UserAuthorisationGroupDto(new List<int> {2},
                            new List<AuthorisationGroup>
                            {
                                AuthorisationGroups.CallCentreAgent,
                                AuthorisationGroups.CallCentreManager
                            }),
                    },
                    ExternalReference = "Reference" + n.ToString() + new Random().Next(100, 9999).ToString()
                };
            }
        }

        public static CreateUserDto UnapprovedUser()
        {
            var data = new DefaultUserData();
            var createIndividualDto = NewIndividualDtoObjectMother.ValidBasicIndividualDto();
            createIndividualDto.SetContext(new DtoContext(1, data.UserName, string.Empty));
            return new CreateUserDto(data.UserName, data.Password, false)
            {
                Channels = new List<int>
                {
                    1,
                    2
                },
                CreateIndividualDto = createIndividualDto,
                Groups = new List<UserAuthorisationGroupDto>
                {
                    new UserAuthorisationGroupDto(new List<int> {1},
                        new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent}),
                    new UserAuthorisationGroupDto(new List<int> {2},
                        new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent}),
                },
                ExternalReference = "Reference" + new Random().Next(100, 9999).ToString()
            };
        }

        public static CreateUserDto DuplicateUser()
        {
            var data = new DefaultFixUserData();
            var createIndividualDto = NewIndividualDtoObjectMother.ValidBasicIndividualDto();
            createIndividualDto.SetContext(new DtoContext(1, data.UserName, string.Empty));
            return new CreateUserDto(data.UserName, data.Password, false)
            {
                Channels = new List<int>
                {
                    1,
                    2
                },
                CreateIndividualDto = createIndividualDto,
                Groups = new List<UserAuthorisationGroupDto>
                {
                    new UserAuthorisationGroupDto(new List<int> {1},
                        new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent}),
                    new UserAuthorisationGroupDto(new List<int> {2},
                        new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent}),
                },
                ExternalReference = "Referencex" + new Random().Next(100, 9999).ToString()
            };
        }

        public static CreateUserDto InvalidStaticDataForNewUser()
        {
            return new CreateUserDto();
        }

        public static EditUserDto InvalidStaticDataForEditUser()
        {
            return new EditUserDto();
        }

        public static EditUserDto MissingUserToEdit(int? id)
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidBasicIndividualDto();
            createIndividualDto.SetContext(new DtoContext(id ?? 1, "", string.Empty));
            return new EditUserDto(id ?? 1)
            {
                Channels = new List<int>
                {
                    1,
                    2
                },
                CreateIndividualDto = createIndividualDto,
                Groups = new List<UserAuthorisationGroupDto>
                {
                    new UserAuthorisationGroupDto(new List<int> {1},
                        new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent}),
                    new UserAuthorisationGroupDto(new List<int> {2},
                        new List<AuthorisationGroup>
                        {
                            AuthorisationGroups.CallCentreAgent,
                            AuthorisationGroups.CallCentreManager
                        }),
                }
            };
        }

        public static EditUserDto EditExistingUser(int id)
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidBasicIndividualDto();
            createIndividualDto.SetContext(new DtoContext(id, "", string.Empty));
            return new EditUserDto(id)
            {
                Channels = new List<int>
                {
                    1,
                    3
                },
                CreateIndividualDto = createIndividualDto,
                Groups = new List<UserAuthorisationGroupDto>
                {
                    new UserAuthorisationGroupDto(new List<int> {1},
                        new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent, AuthorisationGroups.Broker}),
                    new UserAuthorisationGroupDto(new List<int> {3},
                        new List<AuthorisationGroup>
                        {
                            AuthorisationGroups.CallCentreAgent,
                        }),
                },
                ExternalReference = "Reference11" + new Random().Next(100, 9999).ToString()
            };
        }

        public static EditUserDto EditExistingUserWithMismatchedChannelAuthorisation(int id)
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidBasicIndividualDto();
            createIndividualDto.SetContext(new DtoContext(id, "", string.Empty));
            return new EditUserDto(id)
            {
                Channels = new List<int>
                {
                    1,
                    3
                },
                CreateIndividualDto = createIndividualDto,
                Groups = new List<UserAuthorisationGroupDto>
                {
                    new UserAuthorisationGroupDto(new List<int> {1},
                        new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent, AuthorisationGroups.Broker}),
                    new UserAuthorisationGroupDto(new List<int> {2},
                        new List<AuthorisationGroup> {AuthorisationGroups.CallCentreAgent, AuthorisationGroups.Broker}),
                    new UserAuthorisationGroupDto(new List<int> {3},
                        new List<AuthorisationGroup>
                        {
                            AuthorisationGroups.CallCentreAgent,
                        }),
                },
                ExternalReference = "Reference2" + new Random().Next(100, 9999).ToString()
            };
        }
    }
}