﻿namespace TestObjects.Mothers.Users
{
    public interface IDefineAuthenticationData
    {
        string UserName { get; }
        string Password { get; }
        string System { get; }
        string IPAddress { get; }
    }
}