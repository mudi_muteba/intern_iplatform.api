﻿using System;

namespace TestObjects.Mothers.Users
{
    public class DefaultUserData : IDefineUserData
    {
        public string Password
        {
            get { return "This is a valid passw0rd?"; }
        }

        public string UserName
        {
            get { return String.Format("{0}valid_user{1}@nowhere.com",new Random().Next(100,999),new Random().Next(1000,9999)); }
        }
    }

    public class DefaultFixUserData : IDefineUserData
    {
        public string Password
        {
            get { return "This is a valid passw0rd?"; }
        }

        public string UserName
        {
            get { return String.Format("{0}valid_user{1}@nowhere.com", 100, 100); }
        }
    }
}