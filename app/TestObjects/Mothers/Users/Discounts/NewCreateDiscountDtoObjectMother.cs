using Domain.Products;
using iPlatform.Api.DTOs.Users.Discount;

namespace TestObjects.Mothers.Users.Discounts
{
    public class NewCreateDiscountDtoObjectMother
    {
        public static CreateDiscountDto ValidCreateDiscountDto(int userId, int coverDefinitionId)
        {
            return new CreateDiscountDto
            {
                UserId = userId,
                Discount = (decimal) 33.3,
                CoverDefinitionId = coverDefinitionId
            };
        }

        public static CreateDiscountDto InvalidCreateDiscountDto(int userId)
        {
            return new CreateDiscountDto
            {
                UserId = userId,
                Discount = (decimal)33.3,
                CoverDefinitionId = 0
            };

        }
    }
}