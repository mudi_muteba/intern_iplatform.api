﻿namespace TestObjects.Mothers.Users
{
    public class Channel2UserData : IDefineAuthenticationData
    {
        public string IPAddress
        {
            get { return "192.168.2.2"; }
        }

        public string UserName
        {
            get { return "z_channel_2@iplatform.co.za"; }
        }

        public string Password
        {
            get { return "q1w2e3r4t5 ?_"; }
        }

        public string System
        {
            get { return "Testing System"; }
        }
    }
}