﻿using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;

namespace TestObjects.Mothers.Users
{
    public class AuthenticationObjectMother
    {
        public static AuthenticationRequestDto Channel2User()
        {
            return Build(new Channel2UserData());
        }

        public static AuthenticationRequestDto RootUser()
        {
            return Build(new rootUserData());
        }

        public static AuthenticationRequestDto ValidAuthenticationRequest(CreateUserDto validUser)
        {
            return Build(new ValidAuthenticationData(validUser));
        }

        public static AuthenticationRequestDto Build(IDefineAuthenticationData data)
        {
            return new AuthenticationRequestDto
            {
                Email = data.UserName,
                Password = data.Password,
                IPAddress = data.IPAddress,
                System = data.System
            };
        }
    }
}