﻿namespace TestObjects.Mothers.Users
{
    public class rootUserData : IDefineAuthenticationData
    {
        public string IPAddress
        {
            get { return "192.168.2.2"; }
        }

        public string UserName
        {
            get { return "root@iplatform.co.za"; }
        }

        public string Password
        {
            get { return "q1w2e3r4t5 ?_"; }
        }

        public string System
        {
            get { return "Testing System"; }
        }
    }
}