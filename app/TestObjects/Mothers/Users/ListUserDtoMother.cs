﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Users;

namespace TestObjects.Mothers.Users
{
    public class ListUserDtoMother
    {
        public static ListUserDto ListUserDto
        {
            get
            {
                var name = typeof(ListUserDto).Name;
                return new ListUserDto
                {
                    Id = typeof(ListUserDto).GetHashCode() + 1,
                    UserName = name + "UserName",
                    IsApproved = true,
                    IsLoggedIn = true,
                    LastLoggedInDate = new DateTimeDto(DateTime.UtcNow),
                    Links = new List<ResourceLink>()
                };
            }
        }
    }
}