﻿using iPlatform.Api.DTOs.Users;

namespace TestObjects.Mothers.Users
{
    public class ValidAuthenticationData : IDefineAuthenticationData
    {
        private readonly CreateUserDto _validUser;

        public ValidAuthenticationData(CreateUserDto validUser)
        {
            _validUser = validUser;
        }

        public string IPAddress
        {
            get { return "192.168.2.123"; }
        }

        public string UserName
        {
            get { return _validUser.UserName; }
        }

        public string Password
        {
            get { return _validUser.Password; }
        }

        public string System
        {
            get { return "Testing System"; }
        }
    }
}