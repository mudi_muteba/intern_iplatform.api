﻿using System;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using MasterData;

namespace TestObjects.Mothers.SettingsiPerson
{
    public class SettingsiPersonObjectMother
    {
        public static CreateSettingsiPersonDto ValidSettingsiPersonDto()
        {
            return new CreateSettingsiPersonDto
            {
                ProductId = 1,
                ChannelId = 1,
                BatchNumber = "BatchNumber",
                BranchNumber = "BranchNumber",
                CheckRequired = true,
                EnquirerContactName = "EnquirerContactName",
                EnquirerContactPhoneNo = "EnquirerContactPhoneNo",
                EnvironmentType = EnvironmentTypes.Development,
                SecurityCode = "SecurityCode",
                SubscriberCode = "SubscriberCode"
            };
        }

        public static CreateSettingsiPersonDto InvalidChannelSystemIdSettingsiPersonDto()
        {
            var settingsiPerson = ValidSettingsiPersonDto();

            settingsiPerson.ChannelId = 23000;

            return settingsiPerson;
        }

        public static CreateSettingsiPersonDto InvalidProductIdSettingsiPersonDto()
        {
            var settingsiPerson = ValidSettingsiPersonDto();

            settingsiPerson.ProductId = 999999;

            return settingsiPerson;
        }

        public static EditSettingsiPersonDto ValidEditSettingsiPersonDto(int id)
        {
            var dto = ValidSettingsiPersonDto();

            return new EditSettingsiPersonDto
            {
                Id = id,
                BatchNumber = dto.BatchNumber,
                BranchNumber = dto.BranchNumber,
                ChannelId = dto.ChannelId,
                CheckRequired = dto.CheckRequired,
                EnquirerContactName = dto.EnquirerContactName,
                EnquirerContactPhoneNo = dto.EnquirerContactPhoneNo,
                EnvironmentType = dto.EnvironmentType,
                ProductId = dto.ProductId,
                SecurityCode = dto.SecurityCode,
                SubscriberCode = dto.SubscriberCode,
            };
        }
    }
}
