﻿using System;
using iPlatform.Api.DTOs.Teams;
using iPlatform.Api.DTOs.Users;

namespace TestObjects.Mothers.Team
{
    public class NewTeamDtoObjectMother
    {
        public static CreateTeamDto ValidTeamDto()
        {
            return new CreateTeamDto
            {
                Name = "This is a test team " + DateTime.UtcNow.ToString(),
            };
        }

        public static CreateTeamDto ValidTeamWithValidUserDto()
        {
            var dto = ValidTeamDto();
            dto.Users.Add(new UserInfoDto() { Id = 1 });
            return dto;
        }
    }
}