﻿using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace TestObjects.Mothers.ChannelEvents
{
    public class ChannelEventObjectMother
    {
        public static CreateChannelEventDto ValidDto()
        {
            var dto = new CreateChannelEventDto
            {
                ChannelId = 1,
                EventName = "this is a test",
                ProductCode = "AUGPRD"
            };

            dto.Tasks.Add(new ChannelEventTaskDto {
                TaskName = "2"
            });

            return dto;
        }
    }
}