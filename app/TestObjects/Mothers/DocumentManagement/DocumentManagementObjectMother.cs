﻿using System;
using System.IO;
using System.Text;
using iPlatform.Api.DTOs.DocumentManagement;

namespace TestObjects.Mothers.DocumentManagement
{
    public static class DocumentManagementObjectMother
    {
        public static CreateDocumentDto ValidDocumentManagementDto()
        {
            string test = "Testing 1-2-3";

            // convert string to stream
            byte[] byteArray = Encoding.UTF8.GetBytes(test);

            return new CreateDocumentDto
            {
                CreatedById = 1,
                PartyId = 1,
                ExpiryDate = DateTime.Now.AddMonths(12),
                FileName = "Test",
                ContentType = "app/test",
                ContentLength = 100,
                InputStream = byteArray,
                FileAssociation = "Test",
                IsDocumentTrue = true,
                Tags = "Test Tag",
                ValidFrom = DateTime.Now.AddYears(-1),
                NoOriginalDocument = false
            };
        }

        public static DeleteDocumentDto ValidDeleteDocumentDto(int userDocumentId)
        {
            return new DeleteDocumentDto
            {
                UserDocumentId = userDocumentId
            };
        }
    }
}