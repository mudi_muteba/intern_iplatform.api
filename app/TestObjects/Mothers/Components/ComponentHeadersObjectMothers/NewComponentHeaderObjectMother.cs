﻿using iPlatform.Api.DTOs.Components.ComponentHeaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestObjects.Mothers.Components.ComponentHeadersObjectMothers
{
    public class NewComponentHeaderObjectMother
    {

        public static CreateComponentHeaderDto ValidComponentHeaderDto(int _indivdualId)
        {
            return new CreateComponentHeaderDto()
            {
                PartyId = _indivdualId
            };
        }
    }
}
