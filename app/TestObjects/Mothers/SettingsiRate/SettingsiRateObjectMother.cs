﻿using System;
using iPlatform.Api.DTOs.Admin.SettingsiRates;

namespace TestObjects.Mothers.SettingsiRate
{
    public class SettingsiRateObjectMother
    {
        public static CreateSettingsiRateDto ValidSettingsiRateDto()
        {
            return new CreateSettingsiRateDto
            {
                ProductId = 1,
                Password = "I2P0L1A5T2F0O1R5M-New",
                AgentCode = "IPLATFORM-new-B",
                BrokerCode = null,
                AuthCode = null,
                SchemeCode = null,
                Token = null,
                ChannelId = 1,
                Environment = "UAT-New-B",
                UserId = "VMSA_WEB-B",
                SubscriberCode = "",
                UwCompanyCode = null,
                UwProductCode = null,
                ProductCode = "VMSAFuneral",
                CompanyCode = ""
            };
        }

        public static EditSettingsiRateDto ValidEditSettingsiRateDto(int id)
        {
            CreateSettingsiRateDto dto = ValidSettingsiRateDto();

            return new EditSettingsiRateDto
            {
                Id = id,
                ProductId = dto.ProductId,
                Password = dto.Password,
                AgentCode = dto.AgentCode,
                BrokerCode = dto.BrokerCode,
                AuthCode = dto.AuthCode,
                SchemeCode = dto.SchemeCode,
                Token = dto.Token,
                ChannelId = dto.ChannelId,
                Environment = dto.Environment,
                UserId = dto.UserId,
                SubscriberCode = dto.SubscriberCode,
                UwCompanyCode = dto.UwCompanyCode,
                UwProductCode = dto.UwProductCode,
                ProductCode = dto.ProductCode,
                CompanyCode = dto.CompanyCode
            };
        }

        public static CreateSettingsiRateDto InvalidChannelSystemIdSettingsiRateDto()
        {
            var settingsiRate = ValidSettingsiRateDto();

            settingsiRate.ChannelId = 0;

            return settingsiRate;
        }

        public static CreateSettingsiRateDto InvalidProductIdSettingsiRateDto()
        {
            var settingsiRate = ValidSettingsiRateDto();

            settingsiRate.ProductId = 999999;

            return settingsiRate;
        }
    }
}
