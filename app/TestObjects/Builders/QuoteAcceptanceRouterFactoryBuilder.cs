﻿using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Infrastructure.Factories;

namespace TestObjects.Builders
{
    public class QuoteAcceptanceRouterFactoryBuilder
    {
        public static ICreateWorkflowRoutingMessage<RouteAcceptedQuoteTask> CreateWorkflowRRouteAcceptedQuoteTask()
        {
            //ICreateWorkflowTasks<RouteAcceptedQuoteTask> workflowTaskFactory;
            ICreateTaskMetdata<RouteAcceptedQuoteTask> taskMetadataFactory;

            taskMetadataFactory = new CreateLeadTransferTaskMetadataFactory();
            //workflowTaskFactory = new CreateRouteAcceptedQuoteWorkflowTaskFactory(taskMetadataFactory);
            //return new CreateRouteAcceptedQuoteMessageFactory(workflowTaskFactory);
            return new CreateRouteAcceptedQuoteMessageFactory(taskMetadataFactory);
        }
    }
}
