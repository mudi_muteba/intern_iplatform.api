﻿using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Infrastructure.Factories;
using Workflow.Messages;

namespace TestObjects.Builders
{
    public class WorkflowExecutionMessageBuilder
    {
        public static IWorkflowExecutionMessage ForLeadTransferralMessage(RouteAcceptedQuoteTask command)
        {
            var metadata = new CreateLeadTransferTaskMetadataFactory().Create(command);
            return new CreateLeadTransferralMessageFactory().Create(metadata);
        }
    }
}
