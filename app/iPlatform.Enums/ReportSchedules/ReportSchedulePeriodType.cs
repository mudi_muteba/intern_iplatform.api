﻿namespace iPlatform.Enums.ReportSchedules
{
    public enum ReportSchedulePeriodType
    {
        CurrentDay,
        CurrentWeek,
        CurrentMonth,
        CurrentYear,
        PreviousMonth,
        Previous3Months
    }
}