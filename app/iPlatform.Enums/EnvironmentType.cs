﻿namespace iPlatform.Enums
{
    public enum EnvironmentType
    {
        Dev,
        Staging,
        UAT,
        Live
    }
}
