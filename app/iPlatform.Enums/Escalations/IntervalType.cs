﻿namespace iPlatform.Enums.Escalations
{
    public enum IntervalType
    {
        Minutes,
        Hours,
        Days
    }
}