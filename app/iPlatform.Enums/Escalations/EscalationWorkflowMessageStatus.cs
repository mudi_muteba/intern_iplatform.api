﻿namespace iPlatform.Enums.Escalations
{
    public enum EscalationWorkflowMessageStatus
    {
        Unscheduled,
        Scheduled,
        InProgress,
        Complete,
        Failed
    }
}