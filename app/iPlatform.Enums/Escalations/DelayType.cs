﻿namespace iPlatform.Enums.Escalations
{
    public enum DelayType
    {
        Interval,
        Time,
        DateTime
    }
}