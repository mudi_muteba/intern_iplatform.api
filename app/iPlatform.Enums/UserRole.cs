﻿using System.ComponentModel;

namespace iPlatform.Enums
{
    public enum UserRole
    {
        [Description("Admin User")]
        Admin,
        [Description("Call Centre Agent")]
        CallCentreAgent,
        [Description("Call Centre Manager")]
        CallCentreManager,
        [Description("Broker")]
        Broker,
        [Description("Client")]
        Client,
    }
}
