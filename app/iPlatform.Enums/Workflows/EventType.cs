﻿namespace iPlatform.Enums.Workflows
{
    /// <summary>
    /// Used as an integration layer between code and database whereby the index of 
    /// the enum is stored in the database which is used to locate the event class 
    /// decorated by the enum.
    /// </summary>
    public enum EventType
    {
        LeadCallBack,
        UserAuthenticated
    }
}