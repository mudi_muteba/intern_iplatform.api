﻿namespace iPlatform.Enums.Workflows
{
    /// <summary>
    /// Used to decorate WorkflowExecutionMessage implementations.
    /// These enums are stored in the database ChannelEventTask many to many table.
    /// Each event will have 1 or many WorkflowTaskTypes. 
    /// Each enum will therefore reslove to one WorkflowExecutionMessage sub class.
    /// Each Event will produce one WorkflowRoutingMessage which contains one or many 
    /// WorkflowExecutionMessages.
    /// The event data can then be mapped onto the resolved WorkflowExecutionMessage
    /// that will added to a single WorkflowRoutingMessage published where each added 
    /// WorkflowExecutionMessage it contains will ultimately be consumed in workflow.
    /// </summary>
    public enum WorkflowMessageType
    {
        /// <summary>
        /// Test workflow message type (Used for end to end acceptance testing)
        /// </summary>
        Sms = 0,
        CreateProposalFromExternalRatingRequest = 1,
        LeadTransferal = 2,
        Email = 3,
        PolicyBinding = 4,
    }
}