﻿using iPlatform.PolicyEngine.Data;
using iPlatform.PolicyEngine.Data.Requests;
using iPlatform.PolicyEngine.Data.Responses;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Services
{
    public class DefaultPolicyService : IPolicyDataService
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(DefaultPolicyService));

        private IPolicyService GetServiceClient(PolicyEngineRequest request)
        {
            string url = request.Setting.PolicyServiceSetting.PolicyServiceUrl;
            bool useHttps = url.ToLower().StartsWith("https");
            IPolicyService client;

            if (useHttps)
            {
                var binding = new BasicHttpsBinding
                {
                    MaxBufferSize = 50000000,
                    MaxBufferPoolSize = 50000000,
                    MaxReceivedMessageSize = 50000000,
                    ReaderQuotas =
                    {
                        MaxDepth = int.MaxValue,
                        MaxStringContentLength = int.MaxValue,
                        MaxArrayLength = int.MaxValue,
                        MaxBytesPerRead = int.MaxValue,
                        MaxNameTableCharCount = int.MaxValue
                    }
                };
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                var endpoint = new EndpointAddress(url);
                var channelFactory = new ChannelFactory<IPolicyService>(binding, endpoint);
                if (!string.IsNullOrEmpty(request.Setting.PolicyServiceSetting.PolicyServiceUsername) &&
                    !string.IsNullOrEmpty(request.Setting.PolicyServiceSetting.PolicyServicePassword))
                {
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                    var credentialBehaviour = channelFactory.Endpoint.Behaviors.Find<ClientCredentials>();
                    credentialBehaviour.UserName.UserName = request.Setting.PolicyServiceSetting.PolicyServiceUsername;
                    credentialBehaviour.UserName.Password = request.Setting.PolicyServiceSetting.PolicyServicePassword;
                }
                client = channelFactory.CreateChannel();
            }
            else
            {
                var binding = new BasicHttpBinding
                {
                    MaxBufferSize = 50000000,
                    MaxBufferPoolSize = 50000000,
                    MaxReceivedMessageSize = 50000000,
                    ReaderQuotas =
                    {
                        MaxDepth = int.MaxValue,
                        MaxStringContentLength = int.MaxValue,
                        MaxArrayLength = int.MaxValue,
                        MaxBytesPerRead = int.MaxValue,
                        MaxNameTableCharCount = int.MaxValue
                    }
                };
                var endpoint = new EndpointAddress(url);
                var channelFactory = new ChannelFactory<IPolicyService>(binding, endpoint);
                if (!string.IsNullOrEmpty(request.Setting.PolicyServiceSetting.PolicyServiceUsername) &&
                    !string.IsNullOrEmpty(request.Setting.PolicyServiceSetting.PolicyServicePassword))
                {
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                    var credentialBehaviour = channelFactory.Endpoint.Behaviors.Find<ClientCredentials>();
                    credentialBehaviour.UserName.UserName = request.Setting.PolicyServiceSetting.PolicyServiceUsername;
                    credentialBehaviour.UserName.Password = request.Setting.PolicyServiceSetting.PolicyServicePassword;
                }
                client = channelFactory.CreateChannel();
            }

            return client;
        }

        public string GetPolicies(PolicyEngineRequest request)
        {
            IPolicyService client = GetServiceClient(request);
            try
            {
                int mobile;
                string resp = client.GetPolicies(request.IDNumber,
                    int.TryParse(request.MobileNumber, out mobile) ? mobile : 0);
                ((ICommunicationObject)client).Close();

                return resp;
            }
            catch (Exception ex)
            {
                if (client != null)
                    ((ICommunicationObject)client).Close();

                log.ErrorFormat("Executing {0} (Message: {1}, InnerExeption: {2}, StackTrace: {3})",
                    this.GetType().ToString(), ex.Message, ex.InnerException.ToString(), ex.StackTrace.ToString());
                throw;
            }
        }

        public string GetPolicySchedule(PolicyEngineRequest request)
        {
            IPolicyService client = GetServiceClient(request);
            try
            {
                int mobile;
                string resp = client.GetPolicySchedule(request.IDNumber,
                    int.TryParse(request.MobileNumber, out mobile) ? mobile : 0).ToString();
                ((ICommunicationObject)client).Close();

                return resp;
            }
            catch (Exception ex)
            {
                if (client != null)
                    ((ICommunicationObject)client).Close();

                log.ErrorFormat("Executing {0} (Message: {1}, InnerExeption: {2}, StackTrace: {3})",
                    this.GetType().ToString(), ex.Message, ex.InnerException.ToString(), ex.StackTrace.ToString());
                throw;
            }
        }

        public string GetFinancials(PolicyEngineRequest request)
        { 
            IPolicyService client = GetServiceClient(request);
            string resp = String.Empty;

            try
            {
                if (String.IsNullOrWhiteSpace(request.FromDate) || String.IsNullOrWhiteSpace(request.ToDate))
                {
                    request.FromDate = "Jan 1 1900 12:00AM";
                    request.ToDate = "Jan 1 1900 12:00AM";
                }

                int mobile;
                resp = client.GetFinancials(request.IDNumber,
                    (int.TryParse(request.MobileNumber, out mobile) ? mobile : 0), request.FromDate, request.ToDate).ToString();
                ((ICommunicationObject)client).Close();
            }
            catch (Exception ex)
            {
                log.Error(String.Format("An error occurred whilst trying to get logger. Message: {0}", ex.Message), ex);
                throw;
            }
            finally
            {
                if (client != null)
                    ((ICommunicationObject)client).Close();
            }

            return resp;
        }

        public string GetClaims(PolicyEngineRequest request)
        {
            IPolicyService client = GetServiceClient(request);
            string resp = String.Empty;

            try
            {
                resp = string.Empty;
                resp = client.GetClaims(request.IDNumber);
                ((ICommunicationObject)client).Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine("-----------------" + exception.Message + "-----------------");
                if (client != null)
                    ((ICommunicationObject)client).Abort();
            }
            return resp;
        }

        public bool IsPolicyHolder(PolicyEngineRequest request)
        {
            IPolicyService client = GetServiceClient(request);
            bool isClient = false;

            try
            {
                isClient = client.IsClient(request.IDNumber, Int32.Parse(request.MobileNumber));
                ((ICommunicationObject)client).Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine("-----------------" + exception.Message + "-----------------");
                if (client != null)
                {
                    ((ICommunicationObject)client).Abort();
                }
            }
            return isClient;
        }
    }
}
