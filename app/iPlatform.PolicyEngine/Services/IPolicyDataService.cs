﻿using iPlatform.PolicyEngine.Data.Requests;
using iPlatform.PolicyEngine.Data.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Services
{
    public interface IPolicyDataService
    {
        string GetPolicies(PolicyEngineRequest request);

        string GetClaims(PolicyEngineRequest request);

        bool IsPolicyHolder(PolicyEngineRequest request);

        string GetPolicySchedule(PolicyEngineRequest request);

        string GetFinancials(PolicyEngineRequest request);
    }
}
