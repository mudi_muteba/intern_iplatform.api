﻿using Common.Logging;
using iPlatform.PolicyEngine.Data;
using iPlatform.PolicyEngine.Data.Requests;
using iPlatform.PolicyEngine.Data.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Services
{
    public class AibPolicyService : IPolicyDataService
    {
        protected static readonly ILog log = LogManager.GetLogger<AibPolicyService>();
        public string GetPolicies(PolicyEngineRequest request)
        {
            var binding = new BasicHttpsBinding
            {
                MaxBufferSize = 50000000,
                MaxBufferPoolSize = 50000000,
                MaxReceivedMessageSize = 50000000,
                ReaderQuotas =
                {
                    MaxDepth = int.MaxValue,
                    MaxStringContentLength = int.MaxValue,
                    MaxArrayLength = int.MaxValue,
                    MaxBytesPerRead = int.MaxValue,
                    MaxNameTableCharCount = int.MaxValue
                }
            };
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            var endpoint = new EndpointAddress("https://ws.insurance-aib.co.za/gcaib-ws/");
            var channelFactory = new ChannelFactory<IPolicyService>(binding, endpoint);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            var credentialBehaviour = channelFactory.Endpoint.Behaviors.Find<ClientCredentials>();
            credentialBehaviour.UserName.UserName = "svcGCAIBPrd";
            credentialBehaviour.UserName.Password = "Glacier2013";
            IPolicyService client = channelFactory.CreateChannel();

            try
            {
                string xml = client.GetPolicies(request.IDNumber, 0);

                ((ICommunicationObject)client).Close();

                return xml;
            }
            catch (Exception ex)
            {
                if (client != null)
                {
                    ((ICommunicationObject)client).Abort();
                }

                log.ErrorFormat("Executing {0} (Message: {1}, InnerExeption: {2}, StackTrace: {3})",
                    this.GetType().ToString(), ex.Message, ex.InnerException.ToString(), ex.StackTrace.ToString());
                throw;
            }
        }


        public string GetPolicySchedule(PolicyEngineRequest request)
        {
            throw new NotImplementedException();
        }

        public string GetClaims(PolicyEngineRequest request)
        {
            throw new NotImplementedException();
        }

        public string GetFinancials(PolicyEngineRequest request)
        {
            throw new NotImplementedException();
        }

        public bool IsPolicyHolder(PolicyEngineRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
