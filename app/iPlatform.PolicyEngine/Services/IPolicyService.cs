﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Services
{
    [ServiceContract]
    public interface IPolicyService
    {

        [OperationContract]
        string GetPolicies(string IdNumber, int CellNumber);

        [OperationContract]
        string GetClaims(string IdNumber);

        [OperationContract]
        bool IsClient(string IdNumber, int CellNumber);

        [OperationContract]
        object GetPolicySchedule(string IdNumber, int CellNumber);

        [OperationContract]
        string GetFinancials(string IdNumber, int CellNumber, string DateFrom, string DateTo);

    }
}
