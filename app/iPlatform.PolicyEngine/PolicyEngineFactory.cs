﻿using Common.Logging;
using iPlatform.PolicyEngine.Data;
using iPlatform.PolicyEngine.Data.Requests;
using iPlatform.PolicyEngine.Data.Responses;
using iPlatform.PolicyEngine.Services;

namespace iPlatform.PolicyEngine
{
    public class PolicyEngineFactory
    {
        private static readonly ILog log = LogManager.GetLogger<PolicyEngineFactory>();
        public static ClaimData GetClaimData(PolicyEngineRequest request)
        {
            string xml = GetPolicyService(request).GetClaims(request);

            if (!string.IsNullOrEmpty(xml))
            {
                return XmlHelper.XmlToObject<ClaimData>(xml);
            }

            return null;
        }

        public static PolicyData GetPolicyData(PolicyEngineRequest request)
        {
            var validity = ValidateRequest(request);
            if(validity.IsValid)
            {
                string xml = GetPolicyService(request).GetPolicies(request);

                if (!string.IsNullOrEmpty(xml))
                    return XmlHelper.XmlToObject<PolicyData>(xml);
            }

            return new PolicyData().AddError(validity);
        }

        public static bool IsValidPolicyHolder(PolicyEngineRequest request)
        {
            ValidateRequest(request);

            return GetPolicyService(request).IsPolicyHolder(request);
        }

        public static ScheduleData GetPolicySchedule(PolicyEngineRequest request)
        {
            var validity = ValidateRequest(request);
            if (validity.IsValid)
            {
                string xml = GetPolicyService(request).GetPolicySchedule(request);

                if (!string.IsNullOrEmpty(xml))
                    return XmlHelper.XmlToObject<ScheduleData>(xml);
            }
            return new ScheduleData().AddError(validity);
        }

        public static FinancialData GetFinancials(PolicyEngineRequest request)
        {
            ValidateRequest(request);

            string xml = GetPolicyService(request).GetFinancials(request);

            if (!string.IsNullOrEmpty(xml))
            {
                return XmlHelper.XmlToObject<FinancialData>(xml);
            }

            return null;
        }

        private static RequestValidationResponse ValidateRequest(PolicyEngineRequest request)
        {
            RequestValidationResponse valid = new RequestValidationResponse();
            if (request.Setting == null)
            {
                string msg = "Policy service setting is empty.";
                valid.AddError(msg);
                log.ErrorFormat(msg);
            }
                
            if (string.IsNullOrEmpty(request.IDNumber))
            {
                string msg = "Invalid ID Number specified.";
                valid.AddError(msg);
                log.ErrorFormat(msg);
            }

            return valid;
        }

        private static IPolicyDataService GetPolicyService(PolicyEngineRequest request)
        {
            IPolicyDataService service;
            try
            {
                log.InfoFormat("Retrieving Service for Insurer {0}", request.Setting.Organization.DisplayName);
                service = request.Container.Resolve<IPolicyDataService>(request.Setting.Organization.Code.ToUpper());
                request.Container.Release(service);
            }
            catch
            {
                log.InfoFormat("unable to get service for Insurer {0}, loading default service... ", request.Setting.Organization.DisplayName);
                service = request.Container.Resolve<IPolicyDataService>("DEFAULT");
                request.Container.Release(service);
            }
            return service;
        }
    }
}
