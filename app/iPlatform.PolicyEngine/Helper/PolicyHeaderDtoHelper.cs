﻿using iPlatform.Api.DTOs.Policy;
using iPlatform.PolicyEngine.Data.Models;
using iPlatform.PolicyEngine.Data.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using System.Globalization;
using log4net;

namespace iPlatform.PolicyEngine.Data.Helper
{
    public static class PolicyHeaderDtoHelper
    {
        private static ILog _Log = LogManager.GetLogger(typeof(PolicyHeaderDtoHelper));

        public static PolicyHeaderDto MapFromEntity(this PolicyHeaderDto item, Insurer insurer, PolicySearchDto dto)
        {
            try
            {
                if (item == null)
                    return item;

                var product = insurer.Products.FirstOrDefault();
                var policy = product.Policies.FirstOrDefault();

                item.PolicyMasterGuid = policy.PolicyMasterGuid;
                item.PolicyNo = policy.PolicyNumber;
                item.ProductId = 0;
                item.ProductName = product.Name;
                item.InsurerId = 0;
                item.InsurerName = insurer.Name;
                item.PolicyStatus = policy.PolicyStatus;
                item.PaymentPlan = policy.PolicyType;
                item.InceptionDate = new DateTimeDto(DateTime.Parse(policy.OriginalInceptionDate));
                item.RenewalDate = new DateTimeDto(DateTime.Parse(policy.AnnualRenewalDate));

                List<PolicyItemDto> items = new List<PolicyItemDto>();
                decimal totalForItems = 0;

                var numberFormatInfo = new NumberFormatInfo();
                numberFormatInfo.NumberDecimalSeparator = ".";

                //get sum of all riskitems riskpayment
                totalForItems = policy.CoverTypes.SelectMany(p => p.RiskItems)
                    .Sum(r => Decimal.Parse(r.RiskPayment, numberFormatInfo));

                item.TotalPayment = new MoneyDto(totalForItems);

                if(dto.IncludeItems)
                {
                    policy.CoverTypes
                        .ForEach(p => p.RiskItems
                        .ForEach(r => items.Add(new PolicyItemDto().MapFromEntity(r, p))));
                }
                    
                item.PolicyItems = items;                
            }
            catch (Exception ex)
            {
                _Log.Error(String.Format("An error occurred whilst trying to map PolicyHeaderDto. Message: {1}", ex.Message), ex);
                throw;
            }

            return item;
        }

        public static PolicyItemDto MapFromEntity(this PolicyItemDto item, RiskItem model, CoverType coverType)
        {
            try
            {
                if (item == null)
                    return null;

                string description = "";

                if (!String.IsNullOrWhiteSpace(model.Description1))
                    description += model.Description1.Trim();

                if (!String.IsNullOrWhiteSpace(model.Description2))
                    description += ", " + model.Description2.Trim();

                if (!String.IsNullOrWhiteSpace(model.Description3))
                    description += ", " + model.Description3.Trim();

                if (!String.IsNullOrWhiteSpace(model.Description4))
                    description += ", " + model.Description4.Trim();

                if (!String.IsNullOrWhiteSpace(model.Description5))
                    description += ", " + model.Description5.Trim();


                var numberFormatInfo = new NumberFormatInfo();
                numberFormatInfo.NumberDecimalSeparator = ".";

                item.ItemMasterGuid = model.ItemMasterGuid;
                item.PolicyHeaderClaimableItemId = 0;
                item.AssetNumber = 0;
                item.DateCreated = null;
                item.DateEffective = null;
                item.DateEnd = null;
                item.ItemDescription = description.Trim();
                item.Cover = coverType.Name;
                item.TotalPayment = new MoneyDto(Decimal.Parse(model.RiskPayment, numberFormatInfo));
                item.VoluntaryExcess = new MoneyDto(0);
                item.ExcessBasic = new MoneyDto(0);
                item.SumInsured = new MoneyDto(Decimal.Parse(model.SumInsured, numberFormatInfo));

                return item;
            }
            catch (Exception ex)
            {
                _Log.Error(String.Format("An error occurred whilst trying to map PolicyItemDto. Message: {1}", ex.Message), ex);
                throw;
            }
        }
    }
}
