﻿using iPlatform.PolicyEngine.Data.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data
{
    public static class PolicyHelper
    {
        public static PolicyData AddError(this PolicyData policy, string error)
        {
            policy.Errors.Add(error);
            return policy;
        }

        public static PolicyData AddError(this PolicyData policy, RequestValidationResponse validResponse)
        {
            foreach (string error in validResponse.Errors)
            {
                policy.Errors.Add(error);
            }
            
            return policy;
        }

        public static void AddError(this RequestValidationResponse response, string error)
        {
            response.Errors.Add(error);
            response.NotValid();
        }

        public static ScheduleData AddError(this ScheduleData schedule, RequestValidationResponse validResponse)
        {
            foreach (string error in validResponse.Errors)
            {
                schedule.Errors.Add(error);
            }

            return schedule;
        }
    }
}
