﻿using iPlatform.PolicyEngine.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace iPlatform.PolicyEngine.Data
{
    public class ScheduleData
    {
        public ScheduleData()
        {
            Schedules = new List<Schedule>();
            Errors = new List<string>();
        }

        public List<Schedule> Schedules { get; set; }

        [XmlIgnoreAttribute]
        public List<string> Errors { get; set; }
    }
}
