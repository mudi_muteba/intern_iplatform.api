﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Responses
{
    public class PolicyEngineReponse
    {
        public string Xml { get; set; }

        public PolicyEngineReponse() { }
        public PolicyEngineReponse(string xml)
        {
            this.Xml = xml;
        }
    }
}