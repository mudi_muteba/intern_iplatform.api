﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Responses
{
    public class RequestValidationResponse
    {
        public RequestValidationResponse()
        {
            IsValid = true;
            Errors = new List<string>();
        }
        public bool IsValid { get; set; }
        public List<string> Errors { get; set; }

        public void NotValid()
        {
            IsValid = false;
        }
    }
}