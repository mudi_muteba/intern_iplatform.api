﻿using iPlatform.PolicyEngine.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace iPlatform.PolicyEngine.Data
{
    public class PolicyData
    {
        public PolicyData()
        {
            Insurers = new List<Insurer>();
            Errors = new List<string>();
        }

        public List<Insurer> Insurers { get; set; }

        [XmlIgnoreAttribute]
        public List<string> Errors {get; set;}

    }
}