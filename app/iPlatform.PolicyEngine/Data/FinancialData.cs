﻿using iPlatform.PolicyEngine.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data
{
    public class FinancialData
    {
        public FinancialData()
        {
            Financials = new List<Financial>();
        }

        public List<Financial> Financials { get; set; }
    }
}
