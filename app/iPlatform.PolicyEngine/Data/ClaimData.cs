﻿using iPlatform.PolicyEngine.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data
{
    public class ClaimData
    {
        public ClaimData()
        {
            Claims = new List<Claim>();
        }

        public List<Claim> Claims { get; set; }
    }
}
