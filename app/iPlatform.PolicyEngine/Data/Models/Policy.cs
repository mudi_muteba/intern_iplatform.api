﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Models
{
    public class Policy
    {
        public Policy()
        {
            CoverTypes = new List<CoverType>();
        }

        public string PolicyNumber { get; set; }
        public string OriginalInceptionDate { get; set; }
        public string AnnualRenewalDate { get; set; }
        public string PolicyType { get; set; }
        public string PolicyStatus { get; set; }
        public string TotalPayment { get; set; }
        public List<CoverType> CoverTypes { get; set; }
        public string PolicyMasterGuid { get; set; }


    }
}
