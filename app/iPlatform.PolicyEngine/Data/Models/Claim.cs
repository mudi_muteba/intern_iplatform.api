﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Models
{
    public class Claim
    {
        public string ClaimNumber { get; set; }
        public string DateOfLoss { get; set; }
        public string Circumstances { get; set; }
        public string ClaimAmount { get; set; }
        public string Status { get; set; }
        public string ClaimType { get; set; }
    }
}
