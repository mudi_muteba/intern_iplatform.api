﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Models
{
    public class Product
    {
        public Product()
        {
            Policies = new List<Policy>();
        }

        public string Name { get; set; }
        public List<Policy> Policies { get; set; }
    }
}
