﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Models
{
    public class Insurer
    {
        public Insurer()
        {
            Products = new List<Product>();
        }

        public string Name { get; set; }
        public List<Product> Products { get; set; }
    }
}
