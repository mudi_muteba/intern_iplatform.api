﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Models
{
    public class Schedule
    {
        public string eDate { get; set; }
        public string AttachtmentGUID { get; set; }
        public string FileData { get; set; }
        public string Name { get; set; }
    }
}
