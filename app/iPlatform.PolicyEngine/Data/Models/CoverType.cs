﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Models
{
    public class CoverType
    {
        public CoverType()
        {
            RiskItems = new List<RiskItem>();
        }

        public string Name { get; set; }
        public List<RiskItem> RiskItems { get; set; }
    }
}
