﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data
{
    public class ErrorMessages : ReadOnlyCollection<ErrorMessage>
    {
        public static ErrorMessage TimeOut = new ErrorMessage("Service Timeout",
            "Please review service, no response received", "SERVICE_TIMEOUT");

        public static ErrorMessage InvalidRequest = new ErrorMessage("Invalid Request",
            "Please review service, no response received", "SERVICE_TIMEOUT");

        public ErrorMessages()
            : base(
                new[]
                {
                    TimeOut,
                })
        {
        }
    }   
}
