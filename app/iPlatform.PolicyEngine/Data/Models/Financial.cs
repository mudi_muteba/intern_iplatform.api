﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Models
{
    public class Financial
    {
        public string TransactionCode { get; set; }
        public string Reference { get; set; }
        public string AuditNo { get; set; }
        public string TransactionDate { get; set; }
        public string Description { get; set; }
        public string PolicyEffectiveDate { get; set; }
        public string DebitAmt { get; set; }
        public string CreditAmt { get; set; }
        public string TStamp { get; set; }
        public string TBalance { get; set; }
    }
}
