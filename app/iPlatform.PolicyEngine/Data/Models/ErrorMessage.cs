﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data
{
    public class ErrorMessage
    {
        public string Message { get; set; }
        public string InnerException { get; set; }
        public string StackTrace { get; set; }

        public ErrorMessage(string message)
        {
            this.Message = message;
            this.InnerException = message;
            this.StackTrace = string.Empty;
        }

        public ErrorMessage(string message, string innerException, string stacktrace)
        {
            this.Message = message;
            this.InnerException = innerException;
            this.StackTrace = stacktrace;
        }
    }
}
