﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Models
{
    public class RiskItem
    {
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string Description3 { get; set; }
        public string Description4 { get; set; }
        public string Description5 { get; set; }
        public string SumInsured { get; set; }
        public string RiskPayment { get; set; }
        public bool IsMotor { get; set; }
        public bool IsProperty { get; set; }
        public string ItemMasterGuid { get; set; }
    }
}
