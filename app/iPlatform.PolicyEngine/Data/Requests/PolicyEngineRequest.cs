﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Requests
{
    public class PolicyEngineRequest
    {
        public PolicyServiceSettingInfo Setting { get; set; }
        public string MobileNumber { get; set; }
        public string IDNumber { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public IWindsorContainer Container { get; set; }

    }
}
