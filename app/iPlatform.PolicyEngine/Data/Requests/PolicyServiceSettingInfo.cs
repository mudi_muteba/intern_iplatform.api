﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Requests
{
    public class PolicyServiceSettingInfo
    {
        public PolicyServiceSettingInfo()
        {

        }


        public virtual int Id { get; set; }
        public virtual int ChannelId { get; set; }
        public virtual OrganizationData Organization { get; set; }
        public virtual PolicyServiceSettingData PolicyServiceSetting { get; set; }
    }
}
