﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Requests
{
    public class OrganizationData
    {
        private string _tradingName;

        public OrganizationData()
        {
        }

        public virtual string Code { get; set; }

        public virtual string TradingName
        {
            get { return _tradingName; }
            set
            {
                _tradingName = value;
                DisplayName = _tradingName;
            }
        }

        public virtual int PartyId { get; set; }
        public virtual DateTime? TradingSince { get; set; }
        public virtual string RegisteredName { get; set; }
        public virtual string Description { get; set; }
        public virtual string RegNo { get; set; }
        public virtual string FspNo { get; set; }
        public virtual string VatNo { get; set; }
        public virtual bool TemporaryFsp { get; set; }
        public virtual string ProfessionalIndemnityInsurer { get; set; }
        public virtual string PiCoverPolicyNo { get; set; }
        public virtual string TypeOfAgency { get; set; }
        public virtual string FgPolicyNo { get; set; }
        public virtual string FidelityGuaranteeInsurer { get; set; }
        public virtual OrganizationTypeData OrganizationType { get; set; }
        public virtual bool ShortTermInsurers { get; set; }
        public virtual bool LongTermInsurers { get; set; }

        public virtual string DisplayName { get; set; }
    }
}
