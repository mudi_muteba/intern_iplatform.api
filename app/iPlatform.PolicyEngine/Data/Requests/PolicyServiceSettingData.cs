﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Requests
{
    public class PolicyServiceSettingData
    {
        public PolicyServiceSettingData()
        {

        }


        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string PolicyServiceUrl { get; set; }
        public virtual string PolicyServiceUsername { get; set; }
        public virtual string PolicyServicePassword { get; set; }
        public virtual bool PolicyServiceNeedAuthentication { get; set; }
    }
}
