﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.PolicyEngine.Data.Requests
{
    public class OrganizationTypeData
    {
        public OrganizationTypeData() { }
        internal OrganizationTypeData(int id) { Id = id; }
		
		
		public int Id { get; set; }
		
		public string Name { get; set; }
    }
}
