﻿using System;

namespace MasterData
{
        [Serializable]

    public class CoverDefinitionType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public CoverDefinitionType() { }
        internal CoverDefinitionType(int id) { Id = id; }
    }
}