﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class CoverDefinitionTypes : ReadOnlyCollection<CoverDefinitionType>, iMasterDataCollection
    {
        public static CoverDefinitionType AddOn = new CoverDefinitionType(4) { Name = "Add On" };
        public static CoverDefinitionType CoverPerItem = new CoverDefinitionType(5) { Name = "Cover Per Item" };
        public static CoverDefinitionType CoverPerPolicy = new CoverDefinitionType(6) { Name = "Cover Per Policy" };
        public static CoverDefinitionType Extension = new CoverDefinitionType(3) { Name = "Extension" };
        public static CoverDefinitionType MainCover = new CoverDefinitionType(1) { Name = "Main Cover" };
        public static CoverDefinitionType SubCover = new CoverDefinitionType(2) { Name = "Sub Cover" };

        public CoverDefinitionTypes() : base(new[] { AddOn, CoverPerItem, CoverPerPolicy, Extension, MainCover, SubCover }) { }
    }
}