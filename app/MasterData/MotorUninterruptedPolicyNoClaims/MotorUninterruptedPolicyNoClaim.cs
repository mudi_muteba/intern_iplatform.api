namespace MasterData
{
    public class MotorUninterruptedPolicyNoClaim : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public MotorUninterruptedPolicyNoClaim() { }

        internal MotorUninterruptedPolicyNoClaim(int id) { Id = id; }
    }
}