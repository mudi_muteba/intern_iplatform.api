﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ImportStatuses : ReadOnlyCollection<ImportStatus>, iMasterDataCollection
    {
        public static ImportStatus Created = new ImportStatus(1) { Name = "Created", Code = "Created" };
        public static ImportStatus NoValidEnpoint = new ImportStatus(2) { Name = "NoValidEnpoint", Code = "NoValidEnpoint" };
        public static ImportStatus Sent = new ImportStatus(3) { Name = "Sent", Code = "Sent" };
        public static ImportStatus Failed = new ImportStatus(4) { Name = "Failed", Code = "Failed" };
        public static ImportStatus Success = new ImportStatus(5) { Name = "Success", Code = "Success" };
        public static ImportStatus Resent = new ImportStatus(6) { Name = "Resent", Code = "Resent" };

        public static ImportStatus Imported = new ImportStatus(7) { Name = "Imported", Code = "Imported" };
        public static ImportStatus Sentforunderwriting = new ImportStatus(8) { Name = "Sent for underwriting", Code = "Sent for underwriting" };
        public static ImportStatus Quoted = new ImportStatus(9) { Name = "Quoted", Code = "Quoted" };
        public static ImportStatus Rejectedatunderwriting = new ImportStatus(10) { Name = "Rejected at underwriting", Code = "Rejected at underwriting" };
        public static ImportStatus Sold = new ImportStatus(11) { Name = "Sold", Code = "Sold" };
        public static ImportStatus Proposal = new ImportStatus(12) { Name = "Proposal", Code = "Proposal" };
        public static ImportStatus QuoteAccepted = new ImportStatus(13) { Name = "QuoteAccepted", Code = "QuoteAccepted" };
        public static ImportStatus Loss = new ImportStatus(14) { Name = "Loss", Code = "Loss" };
        public static ImportStatus Dead = new ImportStatus(15) { Name = "Dead", Code = "Dead" };
        public static ImportStatus Failure = new ImportStatus(16) { Name = "Failure", Code = "Failure" };
        public static ImportStatus Delay = new ImportStatus(17) { Name = "Delay", Code = "Delay" };
        public ImportStatuses() : base(new[] { Created, NoValidEnpoint, Sent, Failed, Success, Resent,
            Imported,Sentforunderwriting,Quoted,Rejectedatunderwriting,Sold,Proposal,QuoteAccepted,
            Loss,Dead,Failure,Delay}) { }
    }
}