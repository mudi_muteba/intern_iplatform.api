﻿using System;

namespace MasterData
{
    [Serializable]
    public class ImportStatus : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }

        public ImportStatus() { }

        internal ImportStatus(int id) { Id = id; }
    }
}