﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ImportTypes : ReadOnlyCollection<ImportType>, iMasterDataCollection
    {
        public static ImportType Seriti = new ImportType(1) { Name = "Seriti" };
        public static ImportType Excel = new ImportType(2) { Name = "Excel" };

        public ImportTypes() : base(new[] { Seriti, Excel }) { }
    }
}