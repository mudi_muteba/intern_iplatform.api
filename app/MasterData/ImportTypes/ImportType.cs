﻿using System;

namespace MasterData
{
    [Serializable]
    public class ImportType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ImportType() { }

        internal ImportType(int id) { Id = id; }
    }
}