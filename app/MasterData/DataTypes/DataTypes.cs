﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class DataTypes : ReadOnlyCollection<DataType>, iMasterDataCollection
    {
        public static DataType Currency = new DataType(1) { Name = "Currency" };
        public static DataType Text = new DataType(2) { Name = "Free Text" };
        public static DataType Date = new DataType(3) { Name = "Date" };
        public static DataType DateTime = new DataType(4) { Name = "Date & Time" };
        public static DataType Number = new DataType(5) { Name = "Number" };
        public static DataType Lookup = new DataType(6) { Name = "Lookup" };
        public static DataType Time = new DataType(7) { Name = "Time" };
        public static DataType Toggle = new DataType(8) { Name = "Toggle" };

        public DataTypes() : base(new[] { Currency, Text, Date, DateTime, Number, Lookup, Time, Toggle }) { }
    }
}