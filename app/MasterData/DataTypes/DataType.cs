﻿using System;

namespace MasterData
{
    [Serializable]
    public class DataType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public DataType() { }

        internal DataType(int id) { Id = id; }
    }
}