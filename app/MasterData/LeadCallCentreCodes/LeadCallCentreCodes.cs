﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class LeadCallCentreCodes : ReadOnlyCollection<LeadCallCentreCode>, iMasterDataCollection
    {
        #region Primary Lead Call Centre Codes
        public static LeadCallCentreCode LeadNotAllocated = new LeadCallCentreCode(1, 0)
        {
            Name = "Lead Not Allocated",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = false,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = false,
            IsVisibleToCallCentreAgent = false,
            RequiresCallCentreAgentComments = false,
            Icon = "fa fa-asterisk",
            Description = "The lead is not yet allocated to a call centre agent.",
            EventTemplate = "{LEAD} is not yet allocated to a call centre agent.",
            VisibleIndex = 0,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode LeadAllocated = new LeadCallCentreCode(2, 0)
        {
            Name = "Lead Allocated",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = false,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = false,
            IsVisibleToCallCentreAgent = false,
            RequiresCallCentreAgentComments = false,
            Icon = "fa fa-arrow-circle-o-right",
            Description = "The lead is allocated to a call centre agent.",
            EventTemplate = "{LEAD} was allocated to {AGENT} by {SOURCE} for campaign: {CAMPAIGN}.",
            VisibleIndex = 1,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode LeadContacted = new LeadCallCentreCode(3, 0) {
            Name = "Lead Contacted",
            HasChildren = true,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = false,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = false,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = false,
            Icon = "fa fa-microphone",
            Description = "The lead is contacted by a call centre agent.",
            EventTemplate = "{LEAD} was contacted by {AGENT}.",
            VisibleIndex = 2,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode LeadNotContacted = new LeadCallCentreCode(4, 0)
        {
            Name = "Lead Not Contacted",
            HasChildren = true,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = false,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = false,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = false,
            Icon = "fa fa-microphone-slash",
            Description = "The lead could not be contacted by the call centre agent.",
            EventTemplate = "{LEAD} could not be contacted by {AGENT}.",
            VisibleIndex = 3,
            PerformanceWeight = 0
        };
        #endregion

        #region Secondary Lead Call Centre Codes
            #region Lead Could Be Contacted
            public static LeadCallCentreCode Sold = new LeadCallCentreCode(5, 3)
                {
                    Name = "Sold",
                    HasChildren = false,
                    ShouldCreateDeadLeadActivity = false,
                    ShouldCreateLossLeadActivity = false,
                    ShouldCreateDelayLeadActivity = false,
                    ShouldBlockLeadFromBeingContacted = false,
                    ShouldAllowCallRescheduling = false,
                    IsFinalStatus = true,
                    IsVisibleToCallCentreAgent = false,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-money",
                    Description = "The lead accepts a policy.",
                    EventTemplate = "{LEAD} accepted a policy sold by {AGENT} for {INSURER}.",
                    VisibleIndex = 0,
                    PerformanceWeight = 0
                };

                public static LeadCallCentreCode NotInterested = new LeadCallCentreCode(6, 3)
                {
                    Name = "Not Interested",
                    HasChildren = true,
                    ShouldCreateDeadLeadActivity = false,
                    ShouldCreateLossLeadActivity = true,
                    ShouldCreateDelayLeadActivity = false,
                    ShouldBlockLeadFromBeingContacted = false,
                    ShouldAllowCallRescheduling = false,
                    IsFinalStatus = false,
                    IsVisibleToCallCentreAgent = true,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-deaf",
                    Description = "The lead is not interested.",
                    EventTemplate = "{LEAD} was not interested.",
                    VisibleIndex = 1,
                    PerformanceWeight = 0
                };

                public static LeadCallCentreCode CallBackLater = new LeadCallCentreCode(7, 3)
                {
                    Name = "Call Back Later",
                    HasChildren = false,
                    ShouldCreateDeadLeadActivity = false,
                    ShouldCreateLossLeadActivity = false,
                    ShouldCreateDelayLeadActivity = true,
                    ShouldBlockLeadFromBeingContacted = false,
                    ShouldAllowCallRescheduling = true,
                    IsFinalStatus = false,
                    IsVisibleToCallCentreAgent = true,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-calendar",
                    Description = "The lead wants to be called back later.",
                    EventTemplate = "{LEAD} wants to be called back later and was rescheduled to {DATE} by {SOURCE}.",
                    VisibleIndex = 2,
                    PerformanceWeight = 0
                };

                public static LeadCallCentreCode WrongNumber = new LeadCallCentreCode(8, 3)
                {
                    Name = "Wrong Number",
                    HasChildren = false,
                    ShouldCreateDeadLeadActivity = true,
                    ShouldCreateLossLeadActivity = false,
                    ShouldCreateDelayLeadActivity = false,
                    ShouldBlockLeadFromBeingContacted = false,
                    ShouldAllowCallRescheduling = false,
                    IsFinalStatus = true,
                    IsVisibleToCallCentreAgent = true,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-minus-circle",
                    Description = "The lead's contact number is invalid because it belongs to someone else.",
                    EventTemplate = "{LEAD} contact number ({CONTACTNUMBER}) is invalid because it belongs to someone else.",
                    VisibleIndex = 3,
                    PerformanceWeight = 0
                };

                public static LeadCallCentreCode LanguageBarrier = new LeadCallCentreCode(9, 3)
                {
                    Name = "Language Barrier",
                    HasChildren = false,
                    ShouldCreateDeadLeadActivity = false,
                    ShouldCreateLossLeadActivity = false,
                    ShouldCreateDelayLeadActivity = true,
                    ShouldBlockLeadFromBeingContacted = false,
                    ShouldAllowCallRescheduling = true,
                    IsFinalStatus = false,
                    IsVisibleToCallCentreAgent = true,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-deaf",
                    Description = "The lead wants to be assisted in his/her own language.",
                    EventTemplate = "{LEAD} desired assistance in his/her own language and was rescheduled for {DATE} and re-assigned to {AGENT} by {SOURCE}.",
                    VisibleIndex = 4,
                    PerformanceWeight = 0
                };

                public static LeadCallCentreCode RequestedProductInformation = new LeadCallCentreCode(10, 3)
                {
                    Name = "Requested Product Information",
                    HasChildren = false,
                    ShouldCreateDeadLeadActivity = false,
                    ShouldCreateLossLeadActivity = false,
                    ShouldCreateDelayLeadActivity = true,
                    ShouldBlockLeadFromBeingContacted = false,
                    ShouldAllowCallRescheduling = true,
                    IsFinalStatus = false,
                    IsVisibleToCallCentreAgent = true,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-paper-plane-o",
                    Description = "The lead is interested but requires product information to be e-mailed first.",
                    EventTemplate = "{LEAD} was interested but required product information to be e-mailed first.",
                    VisibleIndex = 5,
                    PerformanceWeight = 0
                };

                public static LeadCallCentreCode Unavailable = new LeadCallCentreCode(11, 3)
                {
                    Name = "Unavailable",
                    HasChildren = false,
                    ShouldCreateDeadLeadActivity = false,
                    ShouldCreateLossLeadActivity = false,
                    ShouldCreateDelayLeadActivity = true,
                    ShouldBlockLeadFromBeingContacted = false,
                    ShouldAllowCallRescheduling = true,
                    IsFinalStatus = false,
                    IsVisibleToCallCentreAgent = true,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-user-times",
                    Description = "The lead was unavailable.",
                    EventTemplate = "{LEAD} was unavailable.",
                    VisibleIndex = 6,
                    PerformanceWeight = 0
                };

                public static LeadCallCentreCode Uninsurable = new LeadCallCentreCode(12, 3)
                {
                    Name = "Uninsurable",
                    HasChildren = false,
                    ShouldCreateDeadLeadActivity = false,
                    ShouldCreateLossLeadActivity = false,
                    ShouldCreateDelayLeadActivity = false,
                    ShouldBlockLeadFromBeingContacted = true,
                    ShouldAllowCallRescheduling = false,
                    IsFinalStatus = true,
                    IsVisibleToCallCentreAgent = true,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-user-times",
                    Description = "The lead is uninsurable.",
                    EventTemplate = "{LEAD} is uninsurable.",
                    VisibleIndex = 7,
                    PerformanceWeight = 0
                };

                public static LeadCallCentreCode NotMarketable = new LeadCallCentreCode(13, 3)
                {
                    Name = "Not Marketable",
                    HasChildren = false,
                    ShouldCreateDeadLeadActivity = false,
                    ShouldCreateLossLeadActivity = true,
                    ShouldCreateDelayLeadActivity = false,
                    ShouldBlockLeadFromBeingContacted = true,
                    ShouldAllowCallRescheduling = false,
                    IsFinalStatus = true,
                    IsVisibleToCallCentreAgent = true,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-exclamation-triangle",
                    Description = "The lead does not want toe be contacted again.",
                    EventTemplate = "{LEAD} does not want to be contacted again and has been completely removed from the call list.",
                    VisibleIndex = 8,
                    PerformanceWeight = 0
                };

                public static LeadCallCentreCode DuplicateLead = new LeadCallCentreCode(14, 3)
                {
                    Name = "Duplicate Lead",
                    HasChildren = false,
                    ShouldCreateDeadLeadActivity = false,
                    ShouldCreateLossLeadActivity = false,
                    ShouldCreateDelayLeadActivity = false,
                    ShouldBlockLeadFromBeingContacted = true,
                    ShouldAllowCallRescheduling = false,
                    IsFinalStatus = true,
                    IsVisibleToCallCentreAgent = true,
                    RequiresCallCentreAgentComments = false,
                    Icon = "fa fa-files-o",
                    Description = "The lead is a duplicate.",
                    EventTemplate = "{LEAD} was marked as a duplicate lead by {SOURCE}.",
                    VisibleIndex = 9,
                    PerformanceWeight = 0
                };
            #endregion

            #region Lead Could Not Be Contacted
            public static LeadCallCentreCode Deceased = new LeadCallCentreCode(15, 4)
            {
                Name = "Deceased",
                HasChildren = false,
                ShouldCreateDeadLeadActivity = true,
                ShouldCreateLossLeadActivity = false,
                ShouldCreateDelayLeadActivity = false,
                ShouldBlockLeadFromBeingContacted = true,
                ShouldAllowCallRescheduling = false,
                IsFinalStatus = true,
                IsVisibleToCallCentreAgent = true,
                RequiresCallCentreAgentComments = false,
                Icon = "fa fa-user-times",
                Description = "The lead has passed away.",
                EventTemplate = "{LEAD} has passed away.",
                VisibleIndex = 0,
                PerformanceWeight = 0
            };

            public static LeadCallCentreCode ContactNumberInvalid = new LeadCallCentreCode(16, 4)
            {
                Name = "Contact Number Invalid",
                HasChildren = false,
                ShouldCreateDeadLeadActivity = true,
                ShouldCreateLossLeadActivity = false,
                ShouldCreateDelayLeadActivity = false,
                ShouldBlockLeadFromBeingContacted = false,
                ShouldAllowCallRescheduling = false,
                IsFinalStatus = true,
                IsVisibleToCallCentreAgent = true,
                RequiresCallCentreAgentComments = false,
                Icon = "fa fa-times",
                Description = "The lead's contact number is invalid or does not exist.",
                EventTemplate = "{LEAD} contact number ({CONTACTNUMBER}) is invalid or does not exist.",
                VisibleIndex = 1,
                PerformanceWeight = 0
            };

            public static LeadCallCentreCode MaximumContactAttemptsReached = new LeadCallCentreCode(17, 4)
            {
                Name = "Maximum Contact Attempts Reached",
                HasChildren = false,
                ShouldCreateDeadLeadActivity = true,
                ShouldCreateLossLeadActivity = false,
                ShouldCreateDelayLeadActivity = false,
                ShouldBlockLeadFromBeingContacted = false,
                ShouldAllowCallRescheduling = false,
                IsFinalStatus = true,
                IsVisibleToCallCentreAgent = false,
                RequiresCallCentreAgentComments = false,
                Icon = "fa fa-battery-quarter",
                Description = "The lead has been contacted the maximum allowable times for assigned campaign.",
                EventTemplate = "{LEAD} has been contacted the maximum allowable times ({MAXATTEMPTS}) for campaign: ({CAMPAIGN}).",
                VisibleIndex = 2,
                PerformanceWeight = 0
            };

            public static LeadCallCentreCode FaxMachineContactNumber = new LeadCallCentreCode(18, 4)
            {
                Name = "Fax Machine Contact Number",
                HasChildren = false,
                ShouldCreateDeadLeadActivity = true,
                ShouldCreateLossLeadActivity = false,
                ShouldCreateDelayLeadActivity = false,
                ShouldBlockLeadFromBeingContacted = false,
                ShouldAllowCallRescheduling = false,
                IsFinalStatus = true,
                IsVisibleToCallCentreAgent = true,
                RequiresCallCentreAgentComments = false,
                Icon = "fa fa-fax",
                Description = "The lead's contact number is a fax machine number.",
                EventTemplate = "{LEAD} contact number ({CONTACTNUMBER}) is a fax machine number.",
                VisibleIndex = 3,
                PerformanceWeight = 0
            };

            public static LeadCallCentreCode SimilarCampaign = new LeadCallCentreCode(19, 4)
            {
                Name = "Similar Campaign",
                HasChildren = false,
                ShouldCreateDeadLeadActivity = false,
                ShouldCreateLossLeadActivity = false,
                ShouldCreateDelayLeadActivity = false,
                ShouldBlockLeadFromBeingContacted = false,
                ShouldAllowCallRescheduling = false,
                IsFinalStatus = true,
                IsVisibleToCallCentreAgent = true,
                RequiresCallCentreAgentComments = false,
                Icon = "fa fa-files-o",
                Description = "The lead was already part of similiar campaigns.",
                EventTemplate = "{LEAD} was already part of similiar campaigns: {CAMPAIGN}.",
                VisibleIndex = 4,
                PerformanceWeight = 0
            };

            public static LeadCallCentreCode AnsweringMachine = new LeadCallCentreCode(20, 4)
            {
                Name = "Answering Machine",
                HasChildren = false,
                ShouldCreateDeadLeadActivity = false,
                ShouldCreateLossLeadActivity = false,
                ShouldCreateDelayLeadActivity = true,
                ShouldBlockLeadFromBeingContacted = false,
                ShouldAllowCallRescheduling = true,
                IsFinalStatus = true,
                IsVisibleToCallCentreAgent = true,
                RequiresCallCentreAgentComments = false,
                Icon = "fa fa-volume-control-phone",
                Description = "The lead's contact number forwards to an answering machine or voicemail inbox.",
                EventTemplate = "{LEAD} contact number ({CONTACTNUMBER}) fowarded to an answering machine or voicemail inbox.",
                VisibleIndex = 5,
                PerformanceWeight = 0
            };

            public static LeadCallCentreCode LineEngaged = new LeadCallCentreCode(21, 4)
            {
                Name = "Line Engaged",
                HasChildren = false,
                ShouldCreateDeadLeadActivity = false,
                ShouldCreateLossLeadActivity = false,
                ShouldCreateDelayLeadActivity = true,
                ShouldBlockLeadFromBeingContacted = false,
                ShouldAllowCallRescheduling = true,
                IsFinalStatus = false,
                IsVisibleToCallCentreAgent = true,
                RequiresCallCentreAgentComments = false,
                Icon = "fa fa-microphone-slash",
                Description = "The lead's line is engaged/busy.",
                EventTemplate = "{LEAD} contact number ({CONTACTNUMBER}) was engaged/busy.",
                VisibleIndex = 6,
                PerformanceWeight = 0
            };

            public static LeadCallCentreCode NumberDoesNotExist = new LeadCallCentreCode(22, 4)
            {
                Name = "Number Does Not Exist",
                HasChildren = false,
                ShouldCreateDeadLeadActivity = true,
                ShouldCreateLossLeadActivity = false,
                ShouldCreateDelayLeadActivity = false,
                ShouldBlockLeadFromBeingContacted = false,
                ShouldAllowCallRescheduling = false,
                IsFinalStatus = true,
                IsVisibleToCallCentreAgent = true,
                RequiresCallCentreAgentComments = false,
                Icon = "fa fa-question-circle-o",
                Description = "The lead's contact number does not exist anymore.",
                EventTemplate = "{LEAD} contact number ({CONTACTNUMBER}) does not exist anymore.",
                VisibleIndex = 7,
                PerformanceWeight = 0
            };
            #endregion
        #endregion

        #region Tertiary Lead Call Centre Codes
        public static LeadCallCentreCode OutstandingServiceQuery = new LeadCallCentreCode(23, 6)
        {
            Name = "Outstanding Service Query",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = true,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = true,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = true,
            Icon = "fa fa-exclamation-triangle",
            Description = "The lead is not interested due to an outstanding service query.",
            EventTemplate = "{LEAD} was not interested due to an outstanding service query.",
            VisibleIndex = 0,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode OutstandingClaim = new LeadCallCentreCode(24, 6)
        {
            Name = "Outstanding Claim",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = true,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = true,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = true,
            Icon = "fa fa-exclamation-triangle",
            Description = "The lead is not interested due to an oustanding or current claim.",
            EventTemplate = "{LEAD} was not interested due to an outstanding or current claim: {CLAIM}.",
            VisibleIndex = 1,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode ClaimRejected = new LeadCallCentreCode(25, 6)
        {
            Name = "Claim Rejected",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = true,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = true,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = true,
            Icon = "fa fa-exclamation-triangle",
            Description = "The lead is not interested due to a claim that was previously rejected.",
            EventTemplate = "{LEAD} was not interested due to a claim that was previously rejected.",
            VisibleIndex = 2,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode ExistingCoverage = new LeadCallCentreCode(26, 6)
        {
            Name = "Existing Coverage",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = true,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = true,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = false,
            Icon = "fa fa-exclamation-triangle",
            Description = "The lead is not intersted due to already being covered.",
            EventTemplate = "{LEAD} was not interested due to already being covered.",
            VisibleIndex = 3,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode Presentation = new LeadCallCentreCode(27, 6)
        {
            Name = "Presentation",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = true,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = true,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = true,
            Icon = "fa fa-exclamation-triangle",
            Description = "The lead is presented with the product offerings but is not interested.",
            EventTemplate = "{LEAD} was presented with the product offerings but was not interested.",
            VisibleIndex = 4,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode NoPresentation = new LeadCallCentreCode(28, 6)
        {
            Name = "No Presentation",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = true,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = true,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = true,
            Icon = "fa fa-exclamation-triangle",
            Description = "The lead does not allow the call centre agent to present the product offering.",
            EventTemplate = "{LEAD} did not allow {AGENT} to present the product offering.",
            VisibleIndex = 5,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode Unemployed = new LeadCallCentreCode(29, 6)
        {
            Name = "Unemployed",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = true,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = true,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = false,
            Icon = "fa fa-exclamation-triangle",
            Description = "The lead is not intrested due to being currently unemployed.",
            EventTemplate = "{LEAD} was not interested due to being currently unemployed.",
            VisibleIndex = 6,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode HungUp = new LeadCallCentreCode(30, 6)
        {
            Name = "Hung Up",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = true,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = true,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = false,
            Icon = "fa fa-exclamation-triangle",
            Description = "The lead hangs up during the call.",
            EventTemplate = "{LEAD} hung up during the call.",
            VisibleIndex = 7,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode TooExpensive = new LeadCallCentreCode(31, 6)
        {
            Name = "Too Expensive",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = true,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = true,
            IsVisibleToCallCentreAgent = true,
            RequiresCallCentreAgentComments = false,
            Icon = "fa fa-exclamation-triangle",
            Description = "The lead is not interested due to the premium being more expensive than existing coverage or is not affordable.",
            EventTemplate = "{LEAD} is not interested due to the premium being more expensive than existing coverage or was not affordable.",
            VisibleIndex = 8,
            PerformanceWeight = 0
        };

        public static LeadCallCentreCode LeadReallocated = new LeadCallCentreCode(32, 0)
        {
            Name = "Lead Reallocated",
            HasChildren = false,
            ShouldCreateDeadLeadActivity = false,
            ShouldCreateLossLeadActivity = false,
            ShouldCreateDelayLeadActivity = false,
            ShouldBlockLeadFromBeingContacted = false,
            ShouldAllowCallRescheduling = false,
            IsFinalStatus = false,
            IsVisibleToCallCentreAgent = false,
            RequiresCallCentreAgentComments = false,
            Icon = "fa fa-arrow-circle-o-right",
            Description = "The lead has been  reallocated to a call centre agent.",
            EventTemplate = "{LEAD} was reallocated to {AGENT} by {SOURCE} for campaign: {CAMPAIGN}.",
            VisibleIndex = 1,
            PerformanceWeight = 0
        };
        #endregion

        public LeadCallCentreCodes() : base(new[] {
            LeadNotAllocated,
            LeadAllocated,
            LeadContacted,
            LeadNotContacted,
            Sold,
            Deceased,
            ContactNumberInvalid,
            MaximumContactAttemptsReached,
            NotMarketable,
            FaxMachineContactNumber,
            NotInterested,
            SimilarCampaign,
            DuplicateLead,
            WrongNumber,
            CallBackLater,
            AnsweringMachine,
            LanguageBarrier,
            LineEngaged,
            Unavailable,
            Uninsurable,
            NumberDoesNotExist,
            RequestedProductInformation,
            OutstandingServiceQuery,
            OutstandingClaim,
            ClaimRejected,
            ExistingCoverage,
            Presentation,
            NoPresentation,
            Unemployed,
            HungUp,
            TooExpensive,
            LeadReallocated
        }) { }
    }
}