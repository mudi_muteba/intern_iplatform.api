using System.Collections.ObjectModel;

namespace MasterData
{
    public class Titles : ReadOnlyCollection<Title>, iMasterDataCollection
    {
        public static Title Admiral = new Title(1) { Name = "Admiral", Abbreviation = "Adm.", Code = "Admiral", VisibleIndex = 5 };
        public static Title Advocate = new Title(2) { Name = "Advocate", Abbreviation = "Adv.", Code = "Advocate", VisibleIndex = 6 };
        public static Title Archbishop = new Title(3) { Name = "Archbishop", Abbreviation = "Abp.", Code = "Archbishop", VisibleIndex = 7 };
        public static Title Bishop = new Title(4) { Name = "Bishop", Abbreviation = "Bp.", Code = "Bishop", VisibleIndex = 8 };
        public static Title Brigadier = new Title(5) { Name = "Brigadier", Abbreviation = "Brig.", Code = "Brigadier", VisibleIndex = 9 };
        public static Title Captain = new Title(6) { Name = "Captain", Abbreviation = "Cpt.", Code = "Captain", VisibleIndex = 10 };
        public static Title Colonel = new Title(7) { Name = "Colonel", Abbreviation = "Col.", Code = "Colonel", VisibleIndex = 11 };
        public static Title Commissioner = new Title(8) { Name = "Commissioner", Abbreviation = "Comm.", Code = "Commissioner", VisibleIndex = 12 };
        public static Title Director = new Title(9) { Name = "Director", Abbreviation = "Dir.", Code = "Director", VisibleIndex = 13 };
        public static Title Doctor = new Title(10) { Name = "Doctor", Abbreviation = "Dr.", Code = "Doctor", VisibleIndex = 14 };
        public static Title EstateLate = new Title(11) { Name = "Estate Late", Abbreviation = "Estate Late", Code = "EstateLate", VisibleIndex = 15 };
        public static Title General = new Title(12) { Name = "General", Abbreviation = "Gen.", Code = "General", VisibleIndex = 16 };
        public static Title Honourable = new Title(13) { Name = "Honourable", Abbreviation = "Hon.", Code = "Honourable", VisibleIndex = 17 };
        public static Title HonourableJudge = new Title(14) { Name = "Honourable Judge", Abbreviation = "Hon. J.", Code = "HonourableJudge", VisibleIndex = 18 };
        public static Title Inspector = new Title(15) { Name = "Inspector", Abbreviation = "Insp.", Code = "Inspector", VisibleIndex = 19 };
        public static Title Judge = new Title(16) { Name = "Judge", Abbreviation = "Judge", Code = "Judge", VisibleIndex = 20 };
        public static Title Justice = new Title(17) { Name = "Justice", Abbreviation = "Justice", Code = "Justice", VisibleIndex = 21 };
        public static Title Lady = new Title(18) { Name = "Lady", Abbreviation = "Lady", Code = "Lady", VisibleIndex = 22 };
        public static Title Lieutenant = new Title(19) { Name = "Lieutenant", Abbreviation = "Lt.", Code = "Lieutenant", VisibleIndex = 23 };
        public static Title LieutenantColonel = new Title(20) { Name = "Lieutenant Colonel", Abbreviation = "Lt. Col.", Code = "LieutenantColonel", VisibleIndex = 24 };
        public static Title LieutenantCommander = new Title(21) { Name = "Lieutenant Commander", Abbreviation = "Lt. Cmdr.", Code = "LieutenantCommander", VisibleIndex = 25 };
        public static Title Magistrate = new Title(22) { Name = "Magistrate", Abbreviation = "Magistrate", Code = "Magistrate", VisibleIndex = 26 };
        public static Title Major = new Title(23) { Name = "Major", Abbreviation = "Maj.", Code = "Major", VisibleIndex = 27 };
        public static Title Minister = new Title(24) { Name = "Minister", Abbreviation = "Mnst.", Code = "Minister", VisibleIndex = 28 };
        public static Title Miss = new Title(25) { Name = "Miss", Abbreviation = "Ms.", Code = "Miss", VisibleIndex = 4 };
        public static Title Mr = new Title(26) { Name = "Mr", Abbreviation = "Mr.", Code = "Mr", VisibleIndex = 0 };
        public static Title MrJnr = new Title(27) { Name = "Mr (Jnr)", Abbreviation = "Mr. Jnr.", Code = "MrJnr", VisibleIndex = 2 };
        public static Title MrSnr = new Title(28) { Name = "Mr (Snr)", Abbreviation = "Mr. Snr.", Code = "MrSnr", VisibleIndex = 3 };
        public static Title Mrs = new Title(29) { Name = "Mrs", Abbreviation = "Mrs.", Code = "Mrs", VisibleIndex = 1 };
        public static Title Pastor = new Title(30) { Name = "Pastor", Abbreviation = "Ps.", Code = "Pastor", VisibleIndex = 29 };
        public static Title Professor = new Title(31) { Name = "Professor", Abbreviation = "Prof.", Code = "Professor", VisibleIndex = 30 };
        public static Title Psychologist = new Title(32) { Name = "Psychologist", Abbreviation = "Psych.", Code = "Psychologist", VisibleIndex = 31 };
        public static Title Rabbi = new Title(33) { Name = "Rabbi", Abbreviation = "Rabbi", Code = "Rabbi", VisibleIndex = 32 };
        public static Title Reverend = new Title(34) { Name = "Reverend", Abbreviation = "Rev.", Code = "RevOrReverend", VisibleIndex = 33 };
        public static Title SeniorSuperintendent = new Title(35) { Name = "Senior Superintendent", Abbreviation = "Snr. Supt.", Code = "SnrSup", VisibleIndex = 34 };
        public static Title Sergeant = new Title(36) { Name = "Sergeant", Abbreviation = "Sgt.", Code = "Sergeant", VisibleIndex = 35 };
        public static Title Sir = new Title(37) { Name = "Sir", Abbreviation = "Sir", Code = "Sir", VisibleIndex = 36 };
        public static Title Superintendent = new Title(38) { Name = "Superintendent", Abbreviation = "Supt.", Code = "Sup", VisibleIndex = 37 };
        public static Title TheHonourable = new Title(39) { Name = "The Honourable", Abbreviation = "The Hon.", Code = "TheHonourable", VisibleIndex = 38 };
        public static Title Trust = new Title(40) { Name = "Trust", Abbreviation = "Trust", Code = "Trust", VisibleIndex = 39 };
        public static Title Trustee = new Title(41) { Name = "Trustee", Abbreviation = "Trustee", Code = "Trustee", VisibleIndex = 40 };
        public static Title Unknown = new Title(42) { Name = "Unknown", Abbreviation = "Unknown", Code = "Unknown", VisibleIndex = 41 };

        public Titles() : base(new[] { Admiral, Advocate, Archbishop, Bishop, Brigadier, Captain, Colonel, Commissioner, Director, Doctor, EstateLate, General, Honourable, HonourableJudge, Inspector, Judge, Justice, Lady, Lieutenant, LieutenantColonel, LieutenantCommander, Magistrate, Major, Minister, Miss, Mr, MrJnr, MrSnr, Mrs, Pastor, Professor, Psychologist, Rabbi, Reverend, SeniorSuperintendent, Sergeant, Sir, Superintendent, TheHonourable, Trust, Trustee, Unknown }) { }
    }
}