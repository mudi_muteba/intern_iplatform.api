using System;

namespace MasterData
{
    [Serializable]
    public class Title : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Abbreviation { get; set; }
        public virtual string Code { get; set; }
        public virtual int VisibleIndex { get; set; }

        public Title() { }

        internal Title(int id) { Id = id; }
    }
}