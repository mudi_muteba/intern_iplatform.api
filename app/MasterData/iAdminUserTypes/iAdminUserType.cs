﻿using System;

namespace MasterData
{
    [Serializable]
    public class iAdminUserType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public iAdminUserType() { }

        internal iAdminUserType(int id) { Id = id; }
    }
}