﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class iAdminUserTypes : ReadOnlyCollection<iAdminUserType>, iMasterDataCollection
    {
        public static iAdminUserType Both = new iAdminUserType(1) { Name = "Both" };
        public static iAdminUserType Client = new iAdminUserType(2) { Name = "Client" };
        public static iAdminUserType iAdmin = new iAdminUserType(3) { Name = "iAdmin" };

        public iAdminUserTypes() : base(new[] { Both, Client, iAdmin }) { }
    }
}