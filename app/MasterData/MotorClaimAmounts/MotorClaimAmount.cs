﻿using System;

namespace MasterData
{
    [Serializable]
    public class MotorClaimAmount : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public MotorClaimAmount() { }

        internal MotorClaimAmount(int id) { Id = id; }
    }
}