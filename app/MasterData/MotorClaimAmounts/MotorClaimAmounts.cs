using System.Collections.ObjectModel;

namespace MasterData
{
    public class MotorClaimAmounts : ReadOnlyCollection<MotorClaimAmount>, iMasterDataCollection
    {
        public static MotorClaimAmount MotorClaimAmountNone = new MotorClaimAmount(1) { Name = "MotorClaimAmount None", Answer = "None", VisibleIndex = 0 };
        public static MotorClaimAmount MotorClaimAmount12500 = new MotorClaimAmount(2) { Name = "MotorClaimAmount 1 - 2500", Answer = "1 - 2500", VisibleIndex = 1 };
        public static MotorClaimAmount MotorClaimAmount25015000 = new MotorClaimAmount(3) { Name = "MotorClaimAmount 2501 - 5000", Answer = "2501 - 5000", VisibleIndex = 2 };
        public static MotorClaimAmount MotorClaimAmount50017500 = new MotorClaimAmount(4) { Name = "MotorClaimAmount 5001 - 7500", Answer = "5001 - 7500", VisibleIndex = 3 };
        public static MotorClaimAmount MotorClaimAmount750110000 = new MotorClaimAmount(5) { Name = "MotorClaimAmount 7501 - 10000", Answer = "7501 - 10000", VisibleIndex = 4 };
        public static MotorClaimAmount MotorClaimAmount1000115000 = new MotorClaimAmount(6) { Name = "MotorClaimAmount 10001 - 15000", Answer = "10001 - 15000", VisibleIndex = 5 };
        public static MotorClaimAmount MotorClaimAmount1500120000 = new MotorClaimAmount(7) { Name = "MotorClaimAmount 15001 - 20000", Answer = "15001 - 20000", VisibleIndex = 6 };
        public static MotorClaimAmount MotorClaimAmount2000125000 = new MotorClaimAmount(8) { Name = "MotorClaimAmount 20001 - 25000", Answer = "20001 - 25000", VisibleIndex = 7 };
        public static MotorClaimAmount MotorClaimAmount2500130000 = new MotorClaimAmount(9) { Name = "MotorClaimAmount 25001 - 30000", Answer = "25001 - 30000", VisibleIndex = 8 };
        public static MotorClaimAmount MotorClaimAmount3000135000 = new MotorClaimAmount(10) { Name = "MotorClaimAmount 30001 - 35000", Answer = "30001 - 35000", VisibleIndex = 9 };
        public static MotorClaimAmount MotorClaimAmount3500140000 = new MotorClaimAmount(11) { Name = "MotorClaimAmount 35001 - 40000", Answer = "35001 - 40000", VisibleIndex = 10 };
        public static MotorClaimAmount MotorClaimAmount4000150000 = new MotorClaimAmount(12) { Name = "MotorClaimAmount 40001 - 50000", Answer = "40001 - 50000", VisibleIndex = 11 };
        public static MotorClaimAmount MotorClaimAmount50001100000 = new MotorClaimAmount(13) { Name = "MotorClaimAmount 50001 - 100000", Answer = "50001 - 100000", VisibleIndex = 12 };
        public static MotorClaimAmount MotorClaimAmount100001200000 = new MotorClaimAmount(14) { Name = "MotorClaimAmount 100001 - 200000", Answer = "100001 - 200000", VisibleIndex = 13 };
        public static MotorClaimAmount MotorClaimAmount200001ormore = new MotorClaimAmount(15) { Name = "MotorClaimAmount 200001 or more", Answer = "200001 or more", VisibleIndex = 14 };

        public MotorClaimAmounts() : base(new[] { MotorClaimAmountNone, MotorClaimAmount12500, MotorClaimAmount25015000, MotorClaimAmount50017500, MotorClaimAmount750110000, MotorClaimAmount1000115000, MotorClaimAmount1500120000, MotorClaimAmount2000125000, MotorClaimAmount2500130000, MotorClaimAmount3000135000, MotorClaimAmount3500140000, MotorClaimAmount4000150000, MotorClaimAmount50001100000, MotorClaimAmount100001200000, MotorClaimAmount200001ormore }) { }
    }
}