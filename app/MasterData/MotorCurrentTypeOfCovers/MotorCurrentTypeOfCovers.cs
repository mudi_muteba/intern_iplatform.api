using System.Collections.ObjectModel;

namespace MasterData
{
    public class MotorCurrentTypeOfCovers : ReadOnlyCollection<MotorCurrentTypeOfCover>, iMasterDataCollection
    {
        public static MotorCurrentTypeOfCover MotorCurrentTypeOfCoverComprehensive = new MotorCurrentTypeOfCover(1) { Name = "MotorCurrentTypeOfCover Comprehensive", Answer = "Comprehensive", VisibleIndex = 0 };
        public static MotorCurrentTypeOfCover MotorCurrentTypeOfCoverTPOnly = new MotorCurrentTypeOfCover(2) { Name = "MotorCurrentTypeOfCover TP Only", Answer = "TP Only", VisibleIndex = 0 };
        public static MotorCurrentTypeOfCover MotorCurrentTypeOfCoverTotalloss = new MotorCurrentTypeOfCover(3) { Name = "MotorCurrentTypeOfCover Total  loss", Answer = "Total  loss", VisibleIndex = 0 };

        public MotorCurrentTypeOfCovers() : base(new[] { MotorCurrentTypeOfCoverComprehensive, MotorCurrentTypeOfCoverTPOnly, MotorCurrentTypeOfCoverTotalloss }) { }
    }
}