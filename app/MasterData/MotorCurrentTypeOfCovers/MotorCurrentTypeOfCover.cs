﻿namespace MasterData
{
    public class MotorCurrentTypeOfCover : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public MotorCurrentTypeOfCover() { }

        internal MotorCurrentTypeOfCover(int id) { Id = id; }
    }
}