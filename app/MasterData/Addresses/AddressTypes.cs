using System.Collections.ObjectModel;

namespace MasterData
{
    public class AddressTypes : ReadOnlyCollection<AddressType>, iMasterDataCollection
    {
        public static AddressType PhysicalAddress = new AddressType(1) { Name = "Physical Address" };
        public static AddressType PostalAddress = new AddressType(2) { Name = "Postal Address" };

        public AddressTypes() : base(new[] { PhysicalAddress, PostalAddress }) { }
    }
}