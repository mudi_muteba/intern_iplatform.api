﻿using System;

namespace MasterData
{
    [Serializable]
    public class AddressType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public AddressType() { }

        internal AddressType(int id) { Id = id; }

        protected bool Equals(AddressType other)
        {
            return Id == other.Id && string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((AddressType)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }
    }
}