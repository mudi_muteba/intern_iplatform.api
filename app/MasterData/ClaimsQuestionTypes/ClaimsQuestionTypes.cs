﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ClaimsQuestionTypes : ReadOnlyCollection<ClaimsQuestionType>, iMasterDataCollection
    {
        public static ClaimsQuestionType Checkbox = new ClaimsQuestionType(1) { Name = "Checkbox" };
        public static ClaimsQuestionType Date = new ClaimsQuestionType(2) { Name = "Date" };
        public static ClaimsQuestionType Dropdown = new ClaimsQuestionType(3) { Name = "Dropdown" };
        public static ClaimsQuestionType Textbox = new ClaimsQuestionType(4) { Name = "Textbox" };
        public static ClaimsQuestionType LicenceScan = new ClaimsQuestionType(5) { Name = "LicenceScan" };
        public static ClaimsQuestionType VehicleLicenceScan = new ClaimsQuestionType(6) { Name = "VehicleLicenceScan" };
        public static ClaimsQuestionType Draw = new ClaimsQuestionType(7) { Name = "Draw" };
        public static ClaimsQuestionType Signature = new ClaimsQuestionType(8) { Name = "Signature" };
        public static ClaimsQuestionType SelectFromGallery = new ClaimsQuestionType(9) { Name = "SelectFromGallery" };
        public static ClaimsQuestionType TextboxNo = new ClaimsQuestionType(10) { Name = "TextboxNo" };
        public static ClaimsQuestionType Textarea = new ClaimsQuestionType(11) { Name = "Textarea" };

        public ClaimsQuestionTypes() : base(new[] { Checkbox, Date, Dropdown, Textbox, LicenceScan, VehicleLicenceScan, Draw, Signature, SelectFromGallery, TextboxNo, Textarea }) { }
    }
}