﻿using System;

namespace MasterData
{
    [Serializable]
    public class ClaimsQuestionType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ClaimsQuestionType() { }

        internal ClaimsQuestionType(int id) { Id = id; }
    }
}