using System.Collections.ObjectModel;

namespace MasterData
{
    public class OrganizationTypes : ReadOnlyCollection<OrganizationType>, iMasterDataCollection
    {
        public static OrganizationType BusinessTrust = new OrganizationType(6) { Name = "BusinessTrust" };
        public static OrganizationType CloseCorporation = new OrganizationType(3) { Name = "CloseCorporation" };
        public static OrganizationType NonProfitOrganisations = new OrganizationType(7) { Name = "NonProfitOrganisations" };
        public static OrganizationType Partnership = new OrganizationType(2) { Name = "Partnership" };
        public static OrganizationType PrivateCompanyPtyLtd = new OrganizationType(4) { Name = "PrivateCompany_Pty_Ltd" };
        public static OrganizationType PublicCompany = new OrganizationType(5) { Name = "PublicCompany" };
        public static OrganizationType SoleProprietor = new OrganizationType(1) { Name = "SoleProprietor" };

        public OrganizationTypes() : base(new[] { BusinessTrust, CloseCorporation, NonProfitOrganisations, Partnership, PrivateCompanyPtyLtd, PublicCompany, SoleProprietor }) { }
    }
}