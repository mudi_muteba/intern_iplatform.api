﻿using System;

namespace MasterData
{
    [Serializable]
    public class OrganizationType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public OrganizationType() { }

        internal OrganizationType(int id) { Id = id; }
    }
}