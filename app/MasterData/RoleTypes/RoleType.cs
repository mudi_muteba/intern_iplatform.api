﻿using System;

namespace MasterData
{
    [Serializable]
    public class RoleType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public RoleType() { }

        internal RoleType(int id) { Id = id; }
    }
}