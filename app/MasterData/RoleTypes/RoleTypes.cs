using System.Collections.ObjectModel;

namespace MasterData
{
    public class RoleTypes : ReadOnlyCollection<RoleType>, iMasterDataCollection
    {
        public static RoleType Client = new RoleType(4) { Name = "Client" };
        public static RoleType ClientSite = new RoleType(2) { Name = "Client Site" };
        public static RoleType Insurer = new RoleType(1) { Name = "Insurer" };
        public static RoleType User = new RoleType(3) { Name = "User" };

        public RoleTypes() : base(new[] { Client, ClientSite, Insurer, User }) { }
    }
}