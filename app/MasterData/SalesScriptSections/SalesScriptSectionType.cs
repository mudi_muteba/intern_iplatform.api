﻿using System;

namespace MasterData
{
    [Serializable]
    public class SalesScriptSectionType : BaseEntity, iMasterData
    {
        public virtual int Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        public virtual string Name { get; set; }

        public SalesScriptSectionType() { }

        internal SalesScriptSectionType(int id) { Id = id; }
    }
}
