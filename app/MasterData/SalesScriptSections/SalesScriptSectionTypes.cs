﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class SalesScriptSectionTypes : ReadOnlyCollection<SalesScriptSectionType>, iMasterDataCollection
    {
        public static SalesScriptSectionType SalesScript = new SalesScriptSectionType(1) { Name = "Sales scripts" };
        public static SalesScriptSectionType QuoteGeneration = new SalesScriptSectionType(2) { Name = "Quote generation" };
        public static SalesScriptSectionType Quote = new SalesScriptSectionType(3) { Name = "Quote" };
        public static SalesScriptSectionType Bind = new SalesScriptSectionType(4) { Name = "Bind" };

        public SalesScriptSectionTypes() : base(new[] {SalesScript, QuoteGeneration, Quote, Bind }) { }
    }
}
