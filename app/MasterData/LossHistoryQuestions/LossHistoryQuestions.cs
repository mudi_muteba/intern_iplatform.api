﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace MasterData
{
    public class LossHistoryQuestions : ReadOnlyCollection<LossHistoryQuestion>, iMasterDataCollection
    {
        #region Previous Insurance Questions
        public static LossHistoryQuestion CurrentlyInsured = new LossHistoryQuestion(1)
        {
            Name = "Currently Insured",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 1),
            VisibleIndex = 1
        };
        public static LossHistoryQuestion UninterruptedPolicy = new LossHistoryQuestion(2)
        {
            Name = "Uninterrupted Insurance Duration",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 1),
            VisibleIndex = 2
        };
        public static LossHistoryQuestion InsurerCancel = new LossHistoryQuestion(3)
        {
            Name = "Previous Insurance Cancelled",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 1),
            VisibleIndex = 3
        };
        public static LossHistoryQuestion PreviousComprehensiveInsurance = new LossHistoryQuestion(4)
        {
            Name = "Had Comprehensive Insurance",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 1),
            VisibleIndex = 4
        };
        public static LossHistoryQuestion UninterruptedPolicyNoClaim = new LossHistoryQuestion(5)
        {
            Name = "Uninterrupted Comprehensive Insurance Duration with no clams",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 1),
            VisibleIndex = 5
        };
        public static LossHistoryQuestion InterruptedPolicyClaim = new LossHistoryQuestion(6)
        {
            Name = "Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 1),
            VisibleIndex = 6
        };

        public static LossHistoryQuestion MissedPremiumLast6Months = new LossHistoryQuestion(16)
        {
            Name = "Missed Premium Last 6 Months",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 1),
            VisibleIndex = 7
        };

        #endregion

        #region Building Claims
        public static LossHistoryQuestion BuildingTypeOfLoss = new LossHistoryQuestion(7)
        {
            Name = "Building Type of loss",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 2),
            VisibleIndex = 7
        };
        public static LossHistoryQuestion BuildingDateOfLoss = new LossHistoryQuestion(8)
        {
            Name = "Building Date of Loss",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 2),
            VisibleIndex = 8
        };
        public static LossHistoryQuestion BuildingClaimAmount = new LossHistoryQuestion(9)
        {
            Name = "Building Claim Amount",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 2),
            VisibleIndex = 9
        };
        public static LossHistoryQuestion BuildingClaimLocation = new LossHistoryQuestion(10)
        {
            Name = "Building Claim Location",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 2),
            VisibleIndex = 10
        };
        #endregion

        #region Motor Claims
        public static LossHistoryQuestion MotorTypeOfLoss = new LossHistoryQuestion(11)
        {
            Name = "Motor Type of Loss",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 3),
            VisibleIndex = 11
        };
        public static LossHistoryQuestion MotorDateOfLoss = new LossHistoryQuestion(12)
        {
            Name = "Motor Date of Loss",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 3),
            VisibleIndex = 12
        };
        public static LossHistoryQuestion MotorClaimAmount = new LossHistoryQuestion(13)
        {
            Name = "Motor Claim Amount",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 3),
            VisibleIndex = 13
        };
        public static LossHistoryQuestion MotorAnyoneInjured = new LossHistoryQuestion(14)
        {
            Name = "Motor Anyone Injured",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 3),
            VisibleIndex = 14
        };
        public static LossHistoryQuestion MotorInjuriesSerious = new LossHistoryQuestion(15)
        {
            Name = "Motor Injury Severity",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            LossHistoryQuestionGroup = new LossHistoryQuestionGroups().FirstOrDefault(x => x.Id == 3),
            VisibleIndex = 14
        };
        #endregion

        public LossHistoryQuestions() : base(new[]
        {
            CurrentlyInsured
            ,InsurerCancel
            ,InterruptedPolicyClaim
            ,PreviousComprehensiveInsurance
            ,UninterruptedPolicy
            ,UninterruptedPolicyNoClaim

            ,BuildingTypeOfLoss
            ,BuildingDateOfLoss
            ,BuildingClaimAmount
            ,BuildingClaimLocation

            ,MotorTypeOfLoss
            ,MotorDateOfLoss
            ,MotorClaimAmount

            ,MotorAnyoneInjured
            ,MotorInjuriesSerious
            ,MissedPremiumLast6Months
        })
        { }
    }
}
