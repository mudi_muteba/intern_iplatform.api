﻿using System;

namespace MasterData
{
    [Serializable]
    public class LossHistoryQuestion : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual LossHistoryQuestionGroup LossHistoryQuestionGroup { get; set; }
        public virtual QuestionType QuestionType { get; set; }
        public virtual int VisibleIndex { get; set; }
        //public virtual string ClientCode { get; set; }

        public LossHistoryQuestion() { }

        internal LossHistoryQuestion(int id) { Id = id; }
    }
}
