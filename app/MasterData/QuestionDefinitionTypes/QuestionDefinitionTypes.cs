using System.Collections.ObjectModel;

namespace MasterData
{
    public class QuestionDefinitionTypes : ReadOnlyCollection<QuestionDefinitionType>, iMasterDataCollection
    {
        public static QuestionDefinitionType Standard = new QuestionDefinitionType(1) { Name = "Standard" };
        public static QuestionDefinitionType SubQuestion = new QuestionDefinitionType(2) { Name = "Sub Question" };

        public QuestionDefinitionTypes() : base(new[] { Standard, SubQuestion }) { }
    }
}