﻿using System;

namespace MasterData
{
    [Serializable]
    public class QuestionDefinitionType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public QuestionDefinitionType() { }

        internal QuestionDefinitionType(int id) { Id = id; }
    }
}