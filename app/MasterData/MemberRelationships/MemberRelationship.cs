﻿namespace MasterData
{
    public class MemberRelationship : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual int? VisibleIndex { get; set; }

        public MemberRelationship() { }

        internal MemberRelationship(int id) { Id = id; }
    }
}