using System.Collections.ObjectModel;

namespace MasterData
{
    public class MemberRelationships : ReadOnlyCollection<MemberRelationship>, iMasterDataCollection
    {
        public static MemberRelationship Spouse = new MemberRelationship(1) { Name = "Spouse", VisibleIndex = 16 };
        public static MemberRelationship Mother = new MemberRelationship(2) { Name = "Mother", VisibleIndex = 10 };
        public static MemberRelationship Motherinlaw = new MemberRelationship(3) { Name = "Mother-in-law", VisibleIndex = 11 };
        public static MemberRelationship Fatherinlaw = new MemberRelationship(4) { Name = "Father-in-law", VisibleIndex = 8 };
        public static MemberRelationship Brotherinlaw = new MemberRelationship(5) { Name = "Brother-in-law", VisibleIndex = 4 };
        public static MemberRelationship Sisterinlaw = new MemberRelationship(6) { Name = "Sister-in-law", VisibleIndex = 15 };
        public static MemberRelationship Brother = new MemberRelationship(7) { Name = "Brother", VisibleIndex = 3 };
        public static MemberRelationship Sister = new MemberRelationship(8) { Name = "Sister", VisibleIndex = 14 };
        public static MemberRelationship Aunt = new MemberRelationship(9) { Name = "Aunt", VisibleIndex = 2 };
        public static MemberRelationship Uncle = new MemberRelationship(10) { Name = "Uncle", VisibleIndex = 17 };
        public static MemberRelationship Domesticworker = new MemberRelationship(11) { Name = "Domestic worker", VisibleIndex = 6 };
        public static MemberRelationship Child = new MemberRelationship(12) { Name = "Child", VisibleIndex = 5 };
        public static MemberRelationship Additionalspouses = new MemberRelationship(13) { Name = "Additional spouse(s)", VisibleIndex = 1 };
        public static MemberRelationship Niece = new MemberRelationship(17) { Name = "Niece", VisibleIndex = 13 };
        public static MemberRelationship Nephew = new MemberRelationship(18) { Name = "Nephew", VisibleIndex = 12 };
        public static MemberRelationship Father = new MemberRelationship(19) { Name = "Father", VisibleIndex = 7 };
        public static MemberRelationship Grandchild = new MemberRelationship(20) { Name = "Grandchild", VisibleIndex = 9 };
        public static MemberRelationship AdditionalChilds = new MemberRelationship(21) { Name = "Additional Child(s)", VisibleIndex = 0 };

        public static MemberRelationship Soninlaw = new MemberRelationship(22) { Name = "Son-in-law", VisibleIndex = 18 };
        public static MemberRelationship Daughterinlaw = new MemberRelationship(23) { Name = "Daughter-in-law", VisibleIndex = 19 };
        public static MemberRelationship AdultSon = new MemberRelationship(24) { Name = "Adult Son", VisibleIndex = 20 };
        public static MemberRelationship AdultDaughter = new MemberRelationship(25) { Name = "Adult Daughter", VisibleIndex = 21 };
        public static MemberRelationship DisabledDaughter = new MemberRelationship(26) { Name = "Disabled Daughter", VisibleIndex = 22 };
        public static MemberRelationship DisabledSon = new MemberRelationship(27) { Name = "Disabled Son", VisibleIndex = 23 };
        public static MemberRelationship Grandmother = new MemberRelationship(28) { Name = "Grandmother", VisibleIndex = 24 };
        public static MemberRelationship Grandfather = new MemberRelationship(29) { Name = "Grandfather", VisibleIndex = 25 };

        public MemberRelationships() : base(new[] { Spouse, Mother, Motherinlaw, Fatherinlaw, Brotherinlaw, Sisterinlaw, Brother, Sister, Aunt, Uncle, Domesticworker, Child, Additionalspouses, Grandchild, AdditionalChilds, Niece, Nephew, Father, Soninlaw, Daughterinlaw, AdultSon, AdultDaughter, DisabledSon, DisabledDaughter, Grandmother, Grandfather }) { }
    }
}