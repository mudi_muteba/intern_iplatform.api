﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class EchoTCFEventTypes : ReadOnlyCollection<EchoTCFEventType>, iMasterDataCollection
    {
        /*
                1 New Business
                2 Claim
                3 Renewal
                4 Endorsement
                5 Cancellation
                6 Assistance
         */
        public static EchoTCFEventType NewBusiness = new EchoTCFEventType(1) { Name = "New Business" };
        public static EchoTCFEventType Claim = new EchoTCFEventType(2) { Name = "Claim" };
        public static EchoTCFEventType Renewal = new EchoTCFEventType(3) { Name = "Renewal" };
        public static EchoTCFEventType Endorsement = new EchoTCFEventType(4) { Name = "Endorsement" };
        public static EchoTCFEventType Cancellation = new EchoTCFEventType(5) { Name = "Cancellation" };
        public static EchoTCFEventType Assistance = new EchoTCFEventType(6) { Name = "Assistance" };

        public EchoTCFEventTypes() : base(new[] { NewBusiness, Claim, Renewal, Endorsement, Cancellation, Assistance}) { }
    }
}