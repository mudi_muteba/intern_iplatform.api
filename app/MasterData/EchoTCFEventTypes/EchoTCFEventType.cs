﻿using System;

namespace MasterData
{
    [Serializable]
    public class EchoTCFEventType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public EchoTCFEventType() { }

        internal EchoTCFEventType(int id) { Id = id; }
    }
}