﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterData
{
    [Serializable]
    public class FormStatus : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }

        public FormStatus() { }

        internal FormStatus(int id) { Id = id; }
    }
}