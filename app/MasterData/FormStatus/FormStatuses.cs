﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class FormStatuses : ReadOnlyCollection<FormStatus>, iMasterDataCollection
    {
        public static FormStatus Live = new FormStatus(1) { Name = "Live", Code = "Live" };
        public static FormStatus Draft = new FormStatus(2) { Name = "Draft", Code = "Draft" };

        public FormStatuses() : base(new[] { Live, Draft }) { }
    }
}