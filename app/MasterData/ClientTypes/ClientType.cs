﻿using System;

namespace MasterData
{
    [Serializable]
    public class ClientType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ClientType() { }

        internal ClientType(int id) { Id = id; }
    }
}