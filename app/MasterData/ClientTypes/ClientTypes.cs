﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ClientTypes : ReadOnlyCollection<ClientType>, iMasterDataCollection
    {
        /* Echo TCF Client Types
            1 Policy
            2 Claim
         */
        public static ClientType Policy = new ClientType(1) { Name = "Policy" };
        public static ClientType Claim = new ClientType(2) { Name = "Claim" };



        public ClientTypes() : base(new[] { Policy, Claim }) { }
    }
}