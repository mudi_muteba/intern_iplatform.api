﻿namespace MasterData
{
    public class CustomerSurveyStatus : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public CustomerSurveyStatus() { }

        internal CustomerSurveyStatus(int id) { Id = id; }
    }
}