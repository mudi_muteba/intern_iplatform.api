﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class CustomerSurveyStatuses : ReadOnlyCollection<CustomerSurveyStatus>, iMasterDataCollection
    {
        public static CustomerSurveyStatus Received = new CustomerSurveyStatus(1) { Name = "Received" };
        public static CustomerSurveyStatus Published = new CustomerSurveyStatus(2) { Name = "Published" };
        public static CustomerSurveyStatus Failed = new CustomerSurveyStatus(3) { Name = "Failed" };
        public CustomerSurveyStatuses() : base(new[] { Received, Published, Failed }) { }
    }
}
