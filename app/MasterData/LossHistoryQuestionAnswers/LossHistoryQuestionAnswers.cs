﻿using System.Collections.ObjectModel;
using System.Linq;

namespace MasterData
{
    public class LossHistoryQuestionAnswers : ReadOnlyCollection<LossHistoryQuestionAnswer>, iMasterDataCollection
    {
        public static LossHistoryQuestionAnswer CurrentlyInsuredNo = new LossHistoryQuestionAnswer(1) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 1).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 1).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 1) };
        public static LossHistoryQuestionAnswer CurrentlyInsuredAutoYes = new LossHistoryQuestionAnswer(2) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 2).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 2).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 1) };
        public static LossHistoryQuestionAnswer CurrentlyInsuredHomeYes = new LossHistoryQuestionAnswer(3) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 3).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 3).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 1) };
        public static LossHistoryQuestionAnswer CurrentlyInsuredAutoAndHome = new LossHistoryQuestionAnswer(4) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 4).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 4).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 1) };

        public static LossHistoryQuestionAnswer UninterruptedPolicy0 = new LossHistoryQuestionAnswer(5) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 1).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 1).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy05A = new LossHistoryQuestionAnswer(6) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 2).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 2).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy10A = new LossHistoryQuestionAnswer(7) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 3).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 3).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy15A = new LossHistoryQuestionAnswer(8) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 4).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 4).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy20A = new LossHistoryQuestionAnswer(9) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 5).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 5).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy25A = new LossHistoryQuestionAnswer(10) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 6).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 6).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy3 = new LossHistoryQuestionAnswer(11) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 7).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 7).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy4 = new LossHistoryQuestionAnswer(12) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 8).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy5 = new LossHistoryQuestionAnswer(13) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 9).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 9).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy6 = new LossHistoryQuestionAnswer(14) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 10).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 10).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy7 = new LossHistoryQuestionAnswer(15) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 11).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 11).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy8 = new LossHistoryQuestionAnswer(16) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 12).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 12).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy9 = new LossHistoryQuestionAnswer(17) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 13).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 13).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy10B = new LossHistoryQuestionAnswer(18) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 14).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 14).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy11 = new LossHistoryQuestionAnswer(19) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 15).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 15).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy12 = new LossHistoryQuestionAnswer(20) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 16).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 16).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy13 = new LossHistoryQuestionAnswer(21) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 17).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 17).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy14 = new LossHistoryQuestionAnswer(22) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 18).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 18).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy15B = new LossHistoryQuestionAnswer(23) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 19).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 19).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy16 = new LossHistoryQuestionAnswer(24) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 20).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 20).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy17 = new LossHistoryQuestionAnswer(25) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 21).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 21).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy18 = new LossHistoryQuestionAnswer(26) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 22).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 22).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy19 = new LossHistoryQuestionAnswer(27) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 23).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 23).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy20B = new LossHistoryQuestionAnswer(28) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 24).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 24).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy21 = new LossHistoryQuestionAnswer(29) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 25).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 25).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy22 = new LossHistoryQuestionAnswer(30) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 26).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 26).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy23 = new LossHistoryQuestionAnswer(31) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 27).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 27).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy24 = new LossHistoryQuestionAnswer(32) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 28).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 28).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy25B = new LossHistoryQuestionAnswer(33) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 29).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 29).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy26 = new LossHistoryQuestionAnswer(34) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 30).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 30).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy27 = new LossHistoryQuestionAnswer(35) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 31).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 31).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy28 = new LossHistoryQuestionAnswer(36) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 32).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 32).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy29 = new LossHistoryQuestionAnswer(37) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 33).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 33).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy30 = new LossHistoryQuestionAnswer(38) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 34).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 34).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy31 = new LossHistoryQuestionAnswer(39) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 35).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 35).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy32 = new LossHistoryQuestionAnswer(40) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 36).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 36).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy33 = new LossHistoryQuestionAnswer(41) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 37).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 37).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy34 = new LossHistoryQuestionAnswer(42) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 38).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 38).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy35 = new LossHistoryQuestionAnswer(43) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 39).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 39).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy36 = new LossHistoryQuestionAnswer(44) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 40).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 40).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy37 = new LossHistoryQuestionAnswer(45) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 41).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 41).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy38 = new LossHistoryQuestionAnswer(46) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 42).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 42).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy39 = new LossHistoryQuestionAnswer(47) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 43).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 43).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };
        public static LossHistoryQuestionAnswer UninterruptedPolicy40C = new LossHistoryQuestionAnswer(48) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 44).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 44).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 2) };

        public static LossHistoryQuestionAnswer InsurerCancelTrue = new LossHistoryQuestionAnswer(49) { Name = "InsurerCancelTrue", Answer = "true", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 3) };
        public static LossHistoryQuestionAnswer InsurerCancelFalse = new LossHistoryQuestionAnswer(50) { Name = "InsurerCancelFalse", Answer = "false", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 3) };

        public static LossHistoryQuestionAnswer PreviousComprehensiveInsuranceTrue = new LossHistoryQuestionAnswer(51) { Name = "PreviousComprehensiveInsuranceTrue", Answer = "true", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 4) };
        public static LossHistoryQuestionAnswer PreviousComprehensiveInsuranceFalse = new LossHistoryQuestionAnswer(52) { Name = "PreviousComprehensiveInsuranceFalse", Answer = "false", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 4) };

        public static LossHistoryQuestionAnswer UninterruptedPolicyNoClaimTrue = new LossHistoryQuestionAnswer(53) { Name = "UninterruptedPolicyNoClaimTrue", Answer = "true", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 5) };
        public static LossHistoryQuestionAnswer UninterruptedPolicyNoClaimFalse = new LossHistoryQuestionAnswer(54) { Name = "UninterruptedPolicyNoClaimFalse", Answer = "false", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 5) };

        public static LossHistoryQuestionAnswer InterruptedPolicyClaimTrue = new LossHistoryQuestionAnswer(55) { Name = "InterruptedPolicyClaimTrue", Answer = "true", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 6) };
        public static LossHistoryQuestionAnswer InterruptedPolicyClaimFalse = new LossHistoryQuestionAnswer(56) { Name = "InterruptedPolicyClaimFalse", Answer = "false", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 6) };

        public static LossHistoryQuestionAnswer BuildingTypeOfLossTheft = new LossHistoryQuestionAnswer(57) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 1).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 1).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 7) };
        public static LossHistoryQuestionAnswer BuildingTypeOfLossPipeleakage = new LossHistoryQuestionAnswer(58) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 2).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 2).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 7) };
        public static LossHistoryQuestionAnswer BuildingTypeOfLossFire = new LossHistoryQuestionAnswer(59) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 3).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 3).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 7) };
        public static LossHistoryQuestionAnswer BuildingTypeOfLossStorm = new LossHistoryQuestionAnswer(60) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 4).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 4).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 7) };
        public static LossHistoryQuestionAnswer BuildingTypeOfLossGeysers = new LossHistoryQuestionAnswer(61) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 5).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 5).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 7) };
        public static LossHistoryQuestionAnswer BuildingTypeOfLossAccidentaldamage = new LossHistoryQuestionAnswer(62) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 6).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 6).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 7) };
        public static LossHistoryQuestionAnswer BuildingTypeOfLosssubsidenceandlandslip = new LossHistoryQuestionAnswer(63) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 7).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 7).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 7) };
        public static LossHistoryQuestionAnswer BuildingTypeOfOtherifotherspecify = new LossHistoryQuestionAnswer(64) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 7) };

        public static LossHistoryQuestionAnswer BuildingClaimAmountNone = new LossHistoryQuestionAnswer(65) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 1).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 1).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount12500 = new LossHistoryQuestionAnswer(66) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 2).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 2).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount25015000 = new LossHistoryQuestionAnswer(67) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 3).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 3).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount50017500 = new LossHistoryQuestionAnswer(68) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 4).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 4).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount750110000 = new LossHistoryQuestionAnswer(69) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 5).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 5).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount1000115000 = new LossHistoryQuestionAnswer(70) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 6).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 6).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount1500120000 = new LossHistoryQuestionAnswer(71) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 7).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 7).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount2000125000 = new LossHistoryQuestionAnswer(72) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount2500130000 = new LossHistoryQuestionAnswer(73) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount3000135000 = new LossHistoryQuestionAnswer(74) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount3500140000 = new LossHistoryQuestionAnswer(75) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount4000150000 = new LossHistoryQuestionAnswer(76) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount50001100000 = new LossHistoryQuestionAnswer(77) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount100001200000 = new LossHistoryQuestionAnswer(78) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };
        public static LossHistoryQuestionAnswer BuildingClaimAmount200001ormore = new LossHistoryQuestionAnswer(79) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 9) };

        public static LossHistoryQuestionAnswer BuildingClaimLocationMainresidence = new LossHistoryQuestionAnswer(80) { Name = new HomeClaimLocations().FirstOrDefault(x => x.Id == 1).Name, Answer = new HomeClaimLocations().FirstOrDefault(x => x.Id == 1).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 10) };
        public static LossHistoryQuestionAnswer BuildingClaimLocationSecondaryresidence = new LossHistoryQuestionAnswer(81) { Name = new HomeClaimLocations().FirstOrDefault(x => x.Id == 2).Name, Answer = new HomeClaimLocations().FirstOrDefault(x => x.Id == 2).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 10) };
        public static LossHistoryQuestionAnswer BuildingClaimLocationPreviousresidence = new LossHistoryQuestionAnswer(82) { Name = new HomeClaimLocations().FirstOrDefault(x => x.Id == 3).Name, Answer = new HomeClaimLocations().FirstOrDefault(x => x.Id == 3).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 10) };
        public static LossHistoryQuestionAnswer BuildingClaimLocationHolidayhome = new LossHistoryQuestionAnswer(83) { Name = new HomeClaimLocations().FirstOrDefault(x => x.Id == 4).Name, Answer = new HomeClaimLocations().FirstOrDefault(x => x.Id == 4).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 10) };

        public static LossHistoryQuestionAnswer MotorTypeOfLossTheft = new LossHistoryQuestionAnswer(84) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 1).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 1).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 11) };
        public static LossHistoryQuestionAnswer MotorTypeOfLossPipeleakage = new LossHistoryQuestionAnswer(85) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 2).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 2).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 11) };
        public static LossHistoryQuestionAnswer MotorTypeOfLossFire = new LossHistoryQuestionAnswer(86) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 3).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 3).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 11) };
        public static LossHistoryQuestionAnswer MotorTypeOfLossStorm = new LossHistoryQuestionAnswer(87) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 4).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 4).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 11) };
        public static LossHistoryQuestionAnswer MotorTypeOfLossGeysers = new LossHistoryQuestionAnswer(88) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 5).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 5).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 11) };
        public static LossHistoryQuestionAnswer MotorTypeOfLossAccidentaldamage = new LossHistoryQuestionAnswer(89) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 6).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 6).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 11) };
        public static LossHistoryQuestionAnswer MotorTypeOfLosssubsidenceandlandslip = new LossHistoryQuestionAnswer(90) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 7).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 7).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 11) };
        public static LossHistoryQuestionAnswer MotorTypeOfOtherifotherspecify = new LossHistoryQuestionAnswer(91) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 8).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 11) };

        public static LossHistoryQuestionAnswer MotorClaimAmountNone = new LossHistoryQuestionAnswer(92) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 1).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 1).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount12500 = new LossHistoryQuestionAnswer(93) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 2).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 2).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount25015000 = new LossHistoryQuestionAnswer(94) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 3).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 3).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount50017500 = new LossHistoryQuestionAnswer(95) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 4).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 4).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount750110000 = new LossHistoryQuestionAnswer(96) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 5).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 5).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount1000115000 = new LossHistoryQuestionAnswer(97) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 6).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 6).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount1500120000 = new LossHistoryQuestionAnswer(98) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 7).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 7).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount2000125000 = new LossHistoryQuestionAnswer(99) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount2500130000 = new LossHistoryQuestionAnswer(100) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 9).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 9).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount3000135000 = new LossHistoryQuestionAnswer(101) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 10).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 10).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount3500140000 = new LossHistoryQuestionAnswer(102) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 11).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 11).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount4000150000 = new LossHistoryQuestionAnswer(103) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 12).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 12).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount50001100000 = new LossHistoryQuestionAnswer(104) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 13).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 13).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount100001200000 = new LossHistoryQuestionAnswer(105) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 14).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 14).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };
        public static LossHistoryQuestionAnswer MotorClaimAmount200001ormore = new LossHistoryQuestionAnswer(106) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 15).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 15).Answer, Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 13) };

        public static LossHistoryQuestionAnswer MotorAnyoneInjuredTrue = new LossHistoryQuestionAnswer(107) { Name = "MotorAnyoneInjuredTrue", Answer = "true", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 14) };
        public static LossHistoryQuestionAnswer MotorAnyoneInjuredFalse = new LossHistoryQuestionAnswer(108) { Name = "MotorAnyoneInjuredFalse", Answer = "false", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 14) };

        public static LossHistoryQuestionAnswer MotorInjuriesSeriousTrue = new LossHistoryQuestionAnswer(109) { Name = "MotorInsurerCancelTrue", Answer = "true", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 15) };
        public static LossHistoryQuestionAnswer MotorInjuriesSeriousFalse = new LossHistoryQuestionAnswer(110) { Name = "MotorInsurerCancelFalse", Answer = "false", Question = new LossHistoryQuestions().FirstOrDefault(x => x.Id == 15) };

        public LossHistoryQuestionAnswers() : base(new[]
        {
            CurrentlyInsuredNo
            ,CurrentlyInsuredAutoYes
            ,CurrentlyInsuredHomeYes
            ,CurrentlyInsuredAutoAndHome
            ,UninterruptedPolicy0
            ,UninterruptedPolicy05A
            ,UninterruptedPolicy10A
            ,UninterruptedPolicy15A
            ,UninterruptedPolicy20A
            ,UninterruptedPolicy25A
            ,UninterruptedPolicy3
            ,UninterruptedPolicy4
            ,UninterruptedPolicy5
            ,UninterruptedPolicy6
            ,UninterruptedPolicy7
            ,UninterruptedPolicy8
            ,UninterruptedPolicy9
            ,UninterruptedPolicy10B
            ,UninterruptedPolicy11
            ,UninterruptedPolicy12
            ,UninterruptedPolicy13
            ,UninterruptedPolicy14
            ,UninterruptedPolicy15B
            ,UninterruptedPolicy16
            ,UninterruptedPolicy17
            ,UninterruptedPolicy18
            ,UninterruptedPolicy19
            ,UninterruptedPolicy20B
            ,UninterruptedPolicy21
            ,UninterruptedPolicy22
            ,UninterruptedPolicy23
            ,UninterruptedPolicy24
            ,UninterruptedPolicy25B
            ,UninterruptedPolicy26
            ,UninterruptedPolicy27
            ,UninterruptedPolicy28
            ,UninterruptedPolicy29
            ,UninterruptedPolicy30
            ,UninterruptedPolicy31
            ,UninterruptedPolicy32
            ,UninterruptedPolicy33
            ,UninterruptedPolicy34
            ,UninterruptedPolicy35
            ,UninterruptedPolicy36
            ,UninterruptedPolicy37
            ,UninterruptedPolicy38
            ,UninterruptedPolicy39
            ,UninterruptedPolicy40C
            ,InsurerCancelTrue
            ,InsurerCancelFalse
            ,PreviousComprehensiveInsuranceTrue
            ,PreviousComprehensiveInsuranceFalse
            ,UninterruptedPolicyNoClaimTrue
            ,UninterruptedPolicyNoClaimFalse
            ,InterruptedPolicyClaimTrue
            ,InterruptedPolicyClaimFalse
            ,BuildingTypeOfLossTheft
            ,BuildingTypeOfLossPipeleakage
            ,BuildingTypeOfLossFire
            ,BuildingTypeOfLossStorm
            ,BuildingTypeOfLossGeysers
            ,BuildingTypeOfLossAccidentaldamage
            ,BuildingTypeOfLosssubsidenceandlandslip
            ,BuildingTypeOfOtherifotherspecify
            ,BuildingClaimAmountNone
            ,BuildingClaimAmount12500
            ,BuildingClaimAmount25015000
            ,BuildingClaimAmount50017500
            ,BuildingClaimAmount750110000
            ,BuildingClaimAmount1000115000
            ,BuildingClaimAmount1500120000
            ,BuildingClaimAmount2000125000
            ,BuildingClaimAmount2500130000
            ,BuildingClaimAmount3000135000
            ,BuildingClaimAmount3500140000
            ,BuildingClaimAmount4000150000
            ,BuildingClaimAmount50001100000
            ,BuildingClaimAmount100001200000
            ,BuildingClaimAmount200001ormore
            ,BuildingClaimLocationMainresidence
            ,BuildingClaimLocationSecondaryresidence
            ,BuildingClaimLocationPreviousresidence
            ,BuildingClaimLocationHolidayhome
            ,MotorTypeOfLossTheft
            ,MotorTypeOfLossPipeleakage
            ,MotorTypeOfLossFire
            ,MotorTypeOfLossStorm
            ,MotorTypeOfLossGeysers
            ,MotorTypeOfLossAccidentaldamage
            ,MotorTypeOfLosssubsidenceandlandslip
            ,MotorTypeOfOtherifotherspecify
            ,MotorClaimAmountNone
            ,MotorClaimAmount12500
            ,MotorClaimAmount25015000
            ,MotorClaimAmount50017500
            ,MotorClaimAmount750110000
            ,MotorClaimAmount1000115000
            ,MotorClaimAmount1500120000
            ,MotorClaimAmount2000125000
            ,MotorClaimAmount2500130000
            ,MotorClaimAmount3000135000
            ,MotorClaimAmount3500140000
            ,MotorClaimAmount4000150000
            ,MotorClaimAmount50001100000
            ,MotorClaimAmount100001200000
            ,MotorClaimAmount200001ormore
            ,MotorAnyoneInjuredTrue
            ,MotorAnyoneInjuredFalse
            ,MotorInjuriesSeriousTrue
            ,MotorInjuriesSeriousFalse
        })
        { }
    }
}
