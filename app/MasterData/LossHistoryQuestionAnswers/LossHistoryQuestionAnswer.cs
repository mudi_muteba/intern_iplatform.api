﻿using System;

namespace MasterData
{
    [Serializable]
    public class LossHistoryQuestionAnswer : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual LossHistoryQuestion Question { get; set; }

        public LossHistoryQuestionAnswer() { }

        internal LossHistoryQuestionAnswer(int id) { Id = id; }
    }
}
