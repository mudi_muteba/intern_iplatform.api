﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ChannelSettingOwners : ReadOnlyCollection<ChannelSettingOwner>, iMasterDataCollection
    {
        public static ChannelSettingOwner Api = new ChannelSettingOwner(1) { Name = "iPlatform.Api" };
        public static ChannelSettingOwner Web = new ChannelSettingOwner(2) { Name = "iPlatform.Web"};

        public ChannelSettingOwners() : base(new[] { Api, Web }) { }
    }
}
