﻿namespace MasterData
{
    public class ChannelSettingOwner : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ChannelSettingOwner() { }

        internal ChannelSettingOwner(int id) { Id = id; }
    }
}

