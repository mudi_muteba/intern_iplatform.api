﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class LeadStatuses : ReadOnlyCollection<LeadStatus>, iMasterDataCollection
    {
        public static LeadStatus Created = new LeadStatus(1) { Name = "Created", VisibleIndex = 0 };
        public static LeadStatus Proposal = new LeadStatus(2) { Name = "Proposal", VisibleIndex = 1 };
        public static LeadStatus Delayed = new LeadStatus(3) { Name = "Delayed", VisibleIndex = 2 };
        public static LeadStatus Loss = new LeadStatus(4) { Name = "Loss", VisibleIndex = 3 };
        public static LeadStatus Dead = new LeadStatus(5) { Name = "Dead", VisibleIndex = 4 };
        public static LeadStatus Quoted = new LeadStatus(6) { Name = "Quoted", VisibleIndex = 5 };
        public static LeadStatus QuoteAccepted = new LeadStatus(7) { Name = "QuoteAccepted", VisibleIndex = 6 };
        public static LeadStatus Imported = new LeadStatus(8) { Name = "Imported", VisibleIndex = 7 };
        public static LeadStatus Policy = new LeadStatus(9) { Name = "Policy", VisibleIndex = 8 };
        public static LeadStatus QuoteIntentToBuy = new LeadStatus(10) { Name = "Quote Intent To Buy", VisibleIndex = 9 };
        public static LeadStatus SurveyResponded = new LeadStatus(11) { Name = "Customer satisfaction survey responded", VisibleIndex = 10 };
        public static LeadStatus SentForUnderwriting = new LeadStatus(12) { Name = "Sent for underwriting", VisibleIndex = 11 };
        public static LeadStatus RejectedAtUnderwriting = new LeadStatus(13) { Name = "Rejected at underwriting", VisibleIndex = 12 };
        public static LeadStatus Sold = new LeadStatus(14) { Name = "Sold", VisibleIndex = 13 };
        public static LeadStatus PolicyBindingCreated = new LeadStatus(15) { Name = "Policy binding created", VisibleIndex = 14 };
        public static LeadStatus PolicyBindingCompleted = new LeadStatus(16) { Name = "Policy binding completed", VisibleIndex = 15 };
        public LeadStatuses() : base(new[] { Created, Proposal, Delayed, Loss, Dead, Quoted, QuoteAccepted, Imported, Policy, QuoteIntentToBuy, SurveyResponded, SentForUnderwriting, RejectedAtUnderwriting, Sold, PolicyBindingCreated, PolicyBindingCompleted }) { }
    }
}