﻿namespace MasterData
{
    public class LeadStatus : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual int VisibleIndex { get; set; }

        public LeadStatus() { }

        internal LeadStatus(int id) { Id = id; }
    }
}