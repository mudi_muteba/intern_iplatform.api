﻿using System;

namespace MasterData
{
    [Serializable]
    public class ProductFeeType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ProductFeeType() { }

        internal ProductFeeType(int id) { Id = id; }
    }
}