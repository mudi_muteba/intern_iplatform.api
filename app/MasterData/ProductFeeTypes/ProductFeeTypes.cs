using System.Collections.ObjectModel;

namespace MasterData
{
    public class ProductFeeTypes : ReadOnlyCollection<ProductFeeType>, iMasterDataCollection
    {
        public static ProductFeeType Administrator = new ProductFeeType(1) { Name = "Administrator" };
        public static ProductFeeType Broker = new ProductFeeType(2) { Name = "Broker" };
        public static ProductFeeType Underwriter = new ProductFeeType(3) { Name = "Underwriter" };

        public ProductFeeTypes() : base(new[] { Administrator, Broker, Underwriter }) { }
    }
}