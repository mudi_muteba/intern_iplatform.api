﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class DocumentTypes : ReadOnlyCollection<DocumentType>, iMasterDataCollection
    {
        public static DocumentType BrokerAppointment = new DocumentType(1) { Name = "Broker appointment" };
        public static DocumentType DebitOrder = new DocumentType(2) { Name = "Debit order" };
        public static DocumentType PolicyDocument = new DocumentType(3) { Name = "Policy document" };
        public static DocumentType AdviceRecordDocument = new DocumentType(4) { Name = "Advice record document" };
        public static DocumentType ServiceLevelAgreementDocument = new DocumentType(5) { Name = "Service level agreement document" };
        public static DocumentType ProtectionOfPersonalInfo = new DocumentType(6) { Name = "Protection Of Personal Information" };

        public DocumentTypes() : base(new[] { BrokerAppointment, DebitOrder, PolicyDocument, AdviceRecordDocument, ServiceLevelAgreementDocument, ProtectionOfPersonalInfo }) { }
    }
}
