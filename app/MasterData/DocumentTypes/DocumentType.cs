﻿using System;

namespace MasterData
{
    [Serializable]
    public class DocumentType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public DocumentType() { }

        internal DocumentType(int id) { Id = id; }
    }
}
