using System;

namespace MasterData
{
    [Serializable]
    public class ActivityType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ActivityType() { }

        internal ActivityType(int id) { Id = id; }
    }
}