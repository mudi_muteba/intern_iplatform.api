using System.Collections.ObjectModel;

namespace MasterData
{
    public class ActivityTypes : ReadOnlyCollection<ActivityType>, iMasterDataCollection
    {
        public static ActivityType Created = new ActivityType(1) { Name = "Created" };
        public static ActivityType Imported = new ActivityType(2) { Name = "Imported" };
        public static ActivityType ProposalCreated = new ActivityType(3) { Name = "Proposal Created" };
        public static ActivityType QuoteRequested = new ActivityType(4) { Name = "Quote Requested" };
        public static ActivityType QuoteAccepted = new ActivityType(5) { Name = "Quote Accepted" };
        public static ActivityType ClaimsCreated = new ActivityType(6) { Name = "Claims Created" };
        public static ActivityType Delayed = new ActivityType(7) { Name = "Delayed" };
        public static ActivityType Dead = new ActivityType(8) { Name = "Dead" };
        public static ActivityType Loss = new ActivityType(9) { Name = "Loss" };
        public static ActivityType PolicyCreated = new ActivityType(10) { Name = "Policy Created" };
        public static ActivityType QuoteSent = new ActivityType(11) { Name = "Quote Sent" };
        public static ActivityType QuoteIntentToBuy = new ActivityType(12) { Name = "Intent To Buy Quote" };
        public static ActivityType SurveyResponded = new ActivityType(13) { Name = "Customer satisfaction survey responded" };
        public static ActivityType SentForUnderwriting = new ActivityType(14) { Name = "Quote sent for underwriting" };
        public static ActivityType RejectedAtUnderwriting = new ActivityType(15) { Name = "Rejected at underwriting" };
        public static ActivityType Sold = new ActivityType(16) { Name = "Insurance Sold" };
        public static ActivityType PolicyBindingCreated = new ActivityType(17) { Name = "Policy Binding Created" };
        public static ActivityType PolicyBindingCompleted = new ActivityType(18) { Name = "Policy Binding Completed" };
        public ActivityTypes() : base(new[] { Created, Imported, ProposalCreated, QuoteRequested, QuoteAccepted, ClaimsCreated, Delayed, Dead, Loss, PolicyCreated, QuoteSent, QuoteIntentToBuy, SentForUnderwriting, RejectedAtUnderwriting, Sold, SurveyResponded,  PolicyBindingCreated, PolicyBindingCompleted }) { }
    }
}