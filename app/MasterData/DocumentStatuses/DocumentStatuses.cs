﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class DocumentStatuses : ReadOnlyCollection<DocumentStatus>, iMasterDataCollection
    {
        public static DocumentStatus Draft = new DocumentStatus(1) { Name = "Draft" };
        public static DocumentStatus Pending = new DocumentStatus(2) { Name = "Pending" };
        public static DocumentStatus Circulating = new DocumentStatus(3) { Name = "Circulating" };
        public static DocumentStatus Cancelled = new DocumentStatus(4) { Name = "Cancelled" };
        public static DocumentStatus Rejected = new DocumentStatus(5) { Name = "Rejected" };
        public static DocumentStatus Completed = new DocumentStatus(6) { Name = "Completed" };
        public static DocumentStatus Shared = new DocumentStatus(7) { Name = "Shared" };
        public static DocumentStatus InProgress = new DocumentStatus(8) { Name = "In Progress" };

        public DocumentStatuses() : base(new[] { Draft, Pending, Circulating, Cancelled, Rejected, Completed, Shared, InProgress }) { }
    }
}

