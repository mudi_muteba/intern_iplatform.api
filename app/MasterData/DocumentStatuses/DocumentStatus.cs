﻿using System;

namespace MasterData
{
    [Serializable]
    public class DocumentStatus : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public DocumentStatus() { }

        internal DocumentStatus(int id) { Id = id; }
    }
}
