using System.Collections.ObjectModel;

namespace MasterData
{
    public class QuestionComparisons : ReadOnlyCollection<QuestionComparison>, iMasterDataCollection
    {
        public static QuestionComparison Greater = new QuestionComparison(1) { Name = "Greater", Code = ">" };
        public static QuestionComparison Less = new QuestionComparison(2) { Name = "Less", Code = "<" };
        public static QuestionComparison Equal = new QuestionComparison(3) { Name = "Equal", Code = "==" };
        public static QuestionComparison Include = new QuestionComparison(4) { Name = "Include", Code = "INCLUDE" };
        public static QuestionComparison Exclude = new QuestionComparison(5) { Name = "Exclude", Code = "EXCLUDE" };
        public static QuestionComparison Between = new QuestionComparison(6) { Name = "Between", Code = "BETWEEN" };
        public QuestionComparisons() : base(new[] { Greater, Less, Equal, Include, Exclude, Between }) { }
    }
}