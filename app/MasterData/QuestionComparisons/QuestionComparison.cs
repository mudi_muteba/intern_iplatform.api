﻿using System;

namespace MasterData
{
    [Serializable]
    public class QuestionComparison : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public QuestionComparison() { }

        internal QuestionComparison(int id) { Id = id; }
    }
}