﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class CustomerSurveySources : ReadOnlyCollection<CustomerSurveySource>, iMasterDataCollection
    {
        public static CustomerSurveySource Unknown = new CustomerSurveySource(1) { Name = "Unknown"};
        public static CustomerSurveySource EchoTCF = new CustomerSurveySource(2) { Name = "EchoTCF" };
        public CustomerSurveySources() : base(new[] { Unknown, EchoTCF }) { }
    }
}
