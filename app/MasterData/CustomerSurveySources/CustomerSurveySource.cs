﻿namespace MasterData
{
    public class CustomerSurveySource : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public CustomerSurveySource() { }

        internal CustomerSurveySource(int id) { Id = id; }
    }
}