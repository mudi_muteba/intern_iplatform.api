using System.Collections.ObjectModel;

namespace MasterData
{
    public class QuestionAlerts : ReadOnlyCollection<QuestionAlert>, iMasterDataCollection
    {
        public static QuestionAlert Warning = new QuestionAlert(1) { Name = "Warning" };
        public static QuestionAlert Decline = new QuestionAlert(2) { Name = "Decline" };

        public QuestionAlerts() : base(new[] { Warning, Decline }) { }
    }
}