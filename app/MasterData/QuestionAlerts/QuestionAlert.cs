﻿using System;

namespace MasterData
{
    [Serializable]
    public class QuestionAlert : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public QuestionAlert() { }

        internal QuestionAlert(int id) { Id = id; }
    }
}