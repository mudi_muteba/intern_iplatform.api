using System;

namespace MasterData
{
  [Serializable]
    public class QuestionGroup : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual int VisibleIndex { get; set; }

        public QuestionGroup() { }

        internal QuestionGroup(int id) { Id = id; }

        protected bool Equals(QuestionGroup other)
        {
            return Id == other.Id && string.Equals((string)Name, (string)other.Name) && VisibleIndex == other.VisibleIndex;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((QuestionGroup)obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Id;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ VisibleIndex;
                return hashCode;
            }
        }
    }
}