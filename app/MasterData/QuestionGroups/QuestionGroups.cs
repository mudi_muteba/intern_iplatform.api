using System.Collections.ObjectModel;

namespace MasterData
{
    public class QuestionGroups : ReadOnlyCollection<QuestionGroup>, iMasterDataCollection
    {
        public static QuestionGroup GeneralInformation = new QuestionGroup(1) { Name = "General Information", VisibleIndex = 0 };
        public static QuestionGroup RiskInformation = new QuestionGroup(2) { Name = "Risk Information", VisibleIndex = 100 };
        public static QuestionGroup SecurityInformation = new QuestionGroup(3) { Name = "Security Information", VisibleIndex = 200 };
        public static QuestionGroup AdditionalOptions = new QuestionGroup(4) { Name = "Additional Options", VisibleIndex = 300 };
        public static QuestionGroup DriverInformation = new QuestionGroup(5) { Name = "Driver Information", VisibleIndex = 400 };
        public static QuestionGroup InsuranceHistory = new QuestionGroup(6) { Name = "Insurance History", VisibleIndex = 500 };
        public static QuestionGroup FinanceInformation = new QuestionGroup(7) { Name = "Finance Information", VisibleIndex = 600 };
        public static QuestionGroup Lossesinlast5years = new QuestionGroup(9) { Name = "Losses in last 5 years", VisibleIndex = 600 };
        public static QuestionGroup DiscountLoading = new QuestionGroup(10) { Name = "Discount / Loading", VisibleIndex = 800 };
        public static QuestionGroup ImposedAdditionalExcess = new QuestionGroup(11) { Name = "Imposed / Additional Excess", VisibleIndex = 700 };
        public static QuestionGroup OccupancyDetails = new QuestionGroup(12) { Name = "Occupancy Details", VisibleIndex = 400 };
        public static QuestionGroup BasicSelectableexcess = new QuestionGroup(13) { Name = "Basic / Selectable excess", VisibleIndex = 500 };
        public static QuestionGroup AdditionalCoverage = new QuestionGroup(14) { Name = "Additional Coverage", VisibleIndex = 450 };
        public static QuestionGroup NamedDrivers = new QuestionGroup(15) { Name = "Named Drivers", VisibleIndex = 450 };
        public static QuestionGroup VapTyreRim = new QuestionGroup(16) { Name = "VAP Tyre & Rim", VisibleIndex = 460 };
        public static QuestionGroup Valueaddedproducts = new QuestionGroup(8) { Name = "Value added products", VisibleIndex = 900 };

        public static QuestionGroup VehicleDetails = new QuestionGroup(20) { Name = "Vehicle", VisibleIndex = 100 };
        public static QuestionGroup VehicleInformation = new QuestionGroup(18) { Name = "Vehicle Information", VisibleIndex = 200 };
       

        public QuestionGroups() : base(new[] { GeneralInformation, RiskInformation, SecurityInformation, AdditionalOptions, DriverInformation, InsuranceHistory, FinanceInformation, Valueaddedproducts, Lossesinlast5years, DiscountLoading, ImposedAdditionalExcess, OccupancyDetails, BasicSelectableexcess, AdditionalCoverage, NamedDrivers, VapTyreRim, VehicleDetails, VehicleInformation }) { }
    }
}