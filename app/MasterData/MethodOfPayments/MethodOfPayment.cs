﻿namespace MasterData
{
    public class MethodOfPayment : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public MethodOfPayment() { }

        internal MethodOfPayment(int id) { Id = id; }
    }
}