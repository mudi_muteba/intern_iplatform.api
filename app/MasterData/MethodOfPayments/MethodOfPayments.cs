using System.Collections.ObjectModel;

namespace MasterData
{
    public class MethodOfPayments : ReadOnlyCollection<MethodOfPayment>, iMasterDataCollection
    {
        public static MethodOfPayment DebitOrder = new MethodOfPayment(1) { Name = "Debit Order" };
        public static MethodOfPayment EFTCash = new MethodOfPayment(2) { Name = "EFT/Cash" };
        public static MethodOfPayment MPesa = new MethodOfPayment(3) { Name = "MPesa" };
        public static MethodOfPayment CreditCard = new MethodOfPayment(4) { Name = "Credit Card" };

        public MethodOfPayments() : base(new[] { DebitOrder, EFTCash, MPesa, CreditCard }) { }
    }
}