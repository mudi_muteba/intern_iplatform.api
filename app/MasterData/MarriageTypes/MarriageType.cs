namespace MasterData
{
    public class MarriageType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual int VisibleIndex { get; set; }

        public MarriageType() { }

        internal MarriageType(int id) { Id = id; }
    }
}