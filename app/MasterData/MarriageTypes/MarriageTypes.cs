using System.Collections.ObjectModel;

namespace MasterData
{
    public class MarriageTypes : ReadOnlyCollection<MarriageType>, iMasterDataCollection
    {
        public static MarriageType Unspecified = new MarriageType(4) { Name = "Unspecified", Code = "Unspecified", VisibleIndex = 0 };
        public static MarriageType InCommunityOfProperty = new MarriageType(1) { Name = "In Community Of Property", Code = "InCommunityOfProperty", VisibleIndex = 1 };
        public static MarriageType OutOfCommunityOfPropertyWithAccrual = new MarriageType(2) { Name = "Out of Community of Property (With Accrual)", Code = "OutOfCommunityOfPropertyWithAccrual", VisibleIndex = 2 };
        public static MarriageType OutOfCommunityOfPropertyWithoutAccrual = new MarriageType(3) { Name = "Out of Community of Property Without Accrual", Code = "OutOfCommunityOfPropertyWithoutAccrual", VisibleIndex = 3 };

        public MarriageTypes() : base(new[] { Unspecified, InCommunityOfProperty, OutOfCommunityOfPropertyWithAccrual, OutOfCommunityOfPropertyWithoutAccrual }) { }
    }
}