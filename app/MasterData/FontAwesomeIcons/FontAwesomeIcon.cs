﻿using System;

namespace MasterData
{
    [Serializable]
    public class FontAwesomeIcon : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public FontAwesomeIcon() { }

        internal FontAwesomeIcon(int id)
        {
            Id = id;
        }
    }
}