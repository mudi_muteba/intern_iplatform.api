﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class FontAwesomeIcons : ReadOnlyCollection<FontAwesomeIcon>, iMasterDataCollection
    {
        public static FontAwesomeIcon fa500px = new FontAwesomeIcon(1) { Name = "fa-500px" };
        public static FontAwesomeIcon faaddressbook = new FontAwesomeIcon(2) { Name = "fa-address-book" };
        public static FontAwesomeIcon faaddressbooko = new FontAwesomeIcon(3) { Name = "fa-address-book-o" };
        public static FontAwesomeIcon faaddresscard = new FontAwesomeIcon(4) { Name = "fa-address-card" };
        public static FontAwesomeIcon faaddresscardo = new FontAwesomeIcon(5) { Name = "fa-address-card-o" };
        public static FontAwesomeIcon faadjust = new FontAwesomeIcon(6) { Name = "fa-adjust" };
        public static FontAwesomeIcon faadn = new FontAwesomeIcon(7) { Name = "fa-adn" };
        public static FontAwesomeIcon faaligncenter = new FontAwesomeIcon(8) { Name = "fa-align-center" };
        public static FontAwesomeIcon faalignjustify = new FontAwesomeIcon(9) { Name = "fa-align-justify" };
        public static FontAwesomeIcon faalignleft = new FontAwesomeIcon(10) { Name = "fa-align-left" };
        public static FontAwesomeIcon faalignright = new FontAwesomeIcon(11) { Name = "fa-align-right" };
        public static FontAwesomeIcon faamazon = new FontAwesomeIcon(12) { Name = "fa-amazon" };
        public static FontAwesomeIcon faambulance = new FontAwesomeIcon(13) { Name = "fa-ambulance" };
        public static FontAwesomeIcon faamericansignlanguageinterpreting = new FontAwesomeIcon(14) { Name = "fa-american-sign-language-interpreting" };
        public static FontAwesomeIcon faanchor = new FontAwesomeIcon(15) { Name = "fa-anchor" };
        public static FontAwesomeIcon faandroid = new FontAwesomeIcon(16) { Name = "fa-android" };
        public static FontAwesomeIcon faangellist = new FontAwesomeIcon(17) { Name = "fa-angellist" };
        public static FontAwesomeIcon faangledoubledown = new FontAwesomeIcon(18) { Name = "fa-angle-double-down" };
        public static FontAwesomeIcon faangledoubleleft = new FontAwesomeIcon(19) { Name = "fa-angle-double-left" };
        public static FontAwesomeIcon faangledoubleright = new FontAwesomeIcon(20) { Name = "fa-angle-double-right" };
        public static FontAwesomeIcon faangledoubleup = new FontAwesomeIcon(21) { Name = "fa-angle-double-up" };
        public static FontAwesomeIcon faangledown = new FontAwesomeIcon(22) { Name = "fa-angle-down" };
        public static FontAwesomeIcon faangleleft = new FontAwesomeIcon(23) { Name = "fa-angle-left" };
        public static FontAwesomeIcon faangleright = new FontAwesomeIcon(24) { Name = "fa-angle-right" };
        public static FontAwesomeIcon faangleup = new FontAwesomeIcon(25) { Name = "fa-angle-up" };
        public static FontAwesomeIcon faapple = new FontAwesomeIcon(26) { Name = "fa-apple" };
        public static FontAwesomeIcon faarchive = new FontAwesomeIcon(27) { Name = "fa-archive" };
        public static FontAwesomeIcon faareachart = new FontAwesomeIcon(28) { Name = "fa-area-chart" };
        public static FontAwesomeIcon faarrowcircledown = new FontAwesomeIcon(29) { Name = "fa-arrow-circle-down" };
        public static FontAwesomeIcon faarrowcircleleft = new FontAwesomeIcon(30) { Name = "fa-arrow-circle-left" };
        public static FontAwesomeIcon faarrowcircleodown = new FontAwesomeIcon(31) { Name = "fa-arrow-circle-o-down" };
        public static FontAwesomeIcon faarrowcircleoleft = new FontAwesomeIcon(32) { Name = "fa-arrow-circle-o-left" };
        public static FontAwesomeIcon faarrowcircleoright = new FontAwesomeIcon(33) { Name = "fa-arrow-circle-o-right" };
        public static FontAwesomeIcon faarrowcircleoup = new FontAwesomeIcon(34) { Name = "fa-arrow-circle-o-up" };
        public static FontAwesomeIcon faarrowcircleright = new FontAwesomeIcon(35) { Name = "fa-arrow-circle-right" };
        public static FontAwesomeIcon faarrowcircleup = new FontAwesomeIcon(36) { Name = "fa-arrow-circle-up" };
        public static FontAwesomeIcon faarrowdown = new FontAwesomeIcon(37) { Name = "fa-arrow-down" };
        public static FontAwesomeIcon faarrowleft = new FontAwesomeIcon(38) { Name = "fa-arrow-left" };
        public static FontAwesomeIcon faarrowright = new FontAwesomeIcon(39) { Name = "fa-arrow-right" };
        public static FontAwesomeIcon faarrowup = new FontAwesomeIcon(40) { Name = "fa-arrow-up" };
        public static FontAwesomeIcon faarrows = new FontAwesomeIcon(41) { Name = "fa-arrows" };
        public static FontAwesomeIcon faarrowsalt = new FontAwesomeIcon(42) { Name = "fa-arrows-alt" };
        public static FontAwesomeIcon faarrowsh = new FontAwesomeIcon(43) { Name = "fa-arrows-h" };
        public static FontAwesomeIcon faarrowsv = new FontAwesomeIcon(44) { Name = "fa-arrows-v" };
        public static FontAwesomeIcon faassistivelisteningsystems = new FontAwesomeIcon(45) { Name = "fa-assistive-listening-systems" };
        public static FontAwesomeIcon faasterisk = new FontAwesomeIcon(46) { Name = "fa-asterisk" };
        public static FontAwesomeIcon faat = new FontAwesomeIcon(47) { Name = "fa-at" };
        public static FontAwesomeIcon faaudiodescription = new FontAwesomeIcon(48) { Name = "fa-audio-description" };
        public static FontAwesomeIcon fabackward = new FontAwesomeIcon(49) { Name = "fa-backward" };
        public static FontAwesomeIcon fabalancescale = new FontAwesomeIcon(50) { Name = "fa-balance-scale" };
        public static FontAwesomeIcon faban = new FontAwesomeIcon(51) { Name = "fa-ban" };
        public static FontAwesomeIcon fabandcamp = new FontAwesomeIcon(52) { Name = "fa-bandcamp" };
        public static FontAwesomeIcon fabarchart = new FontAwesomeIcon(53) { Name = "fa-bar-chart" };
        public static FontAwesomeIcon fabarcode = new FontAwesomeIcon(54) { Name = "fa-barcode" };
        public static FontAwesomeIcon fabars = new FontAwesomeIcon(55) { Name = "fa-bars" };
        public static FontAwesomeIcon fabath = new FontAwesomeIcon(56) { Name = "fa-bath" };
        public static FontAwesomeIcon fabatteryempty = new FontAwesomeIcon(57) { Name = "fa-battery-empty" };
        public static FontAwesomeIcon fabatteryfull = new FontAwesomeIcon(58) { Name = "fa-battery-full" };
        public static FontAwesomeIcon fabatteryhalf = new FontAwesomeIcon(59) { Name = "fa-battery-half" };
        public static FontAwesomeIcon fabatteryquarter = new FontAwesomeIcon(60) { Name = "fa-battery-quarter" };
        public static FontAwesomeIcon fabatterythreequarters = new FontAwesomeIcon(61) { Name = "fa-battery-three-quarters" };
        public static FontAwesomeIcon fabed = new FontAwesomeIcon(62) { Name = "fa-bed" };
        public static FontAwesomeIcon fabeer = new FontAwesomeIcon(63) { Name = "fa-beer" };
        public static FontAwesomeIcon fabehance = new FontAwesomeIcon(64) { Name = "fa-behance" };
        public static FontAwesomeIcon fabehancesquare = new FontAwesomeIcon(65) { Name = "fa-behance-square" };
        public static FontAwesomeIcon fabell = new FontAwesomeIcon(66) { Name = "fa-bell" };
        public static FontAwesomeIcon fabello = new FontAwesomeIcon(67) { Name = "fa-bell-o" };
        public static FontAwesomeIcon fabellslash = new FontAwesomeIcon(68) { Name = "fa-bell-slash" };
        public static FontAwesomeIcon fabellslasho = new FontAwesomeIcon(69) { Name = "fa-bell-slash-o" };
        public static FontAwesomeIcon fabicycle = new FontAwesomeIcon(70) { Name = "fa-bicycle" };
        public static FontAwesomeIcon fabinoculars = new FontAwesomeIcon(71) { Name = "fa-binoculars" };
        public static FontAwesomeIcon fabirthdaycake = new FontAwesomeIcon(72) { Name = "fa-birthday-cake" };
        public static FontAwesomeIcon fabitbucket = new FontAwesomeIcon(73) { Name = "fa-bitbucket" };
        public static FontAwesomeIcon fabitbucketsquare = new FontAwesomeIcon(74) { Name = "fa-bitbucket-square" };
        public static FontAwesomeIcon fablacktie = new FontAwesomeIcon(75) { Name = "fa-black-tie" };
        public static FontAwesomeIcon fablind = new FontAwesomeIcon(76) { Name = "fa-blind" };
        public static FontAwesomeIcon fabluetooth = new FontAwesomeIcon(77) { Name = "fa-bluetooth" };
        public static FontAwesomeIcon fabluetoothb = new FontAwesomeIcon(78) { Name = "fa-bluetooth-b" };
        public static FontAwesomeIcon fabold = new FontAwesomeIcon(79) { Name = "fa-bold" };
        public static FontAwesomeIcon fabolt = new FontAwesomeIcon(80) { Name = "fa-bolt" };
        public static FontAwesomeIcon fabomb = new FontAwesomeIcon(81) { Name = "fa-bomb" };
        public static FontAwesomeIcon fabook = new FontAwesomeIcon(82) { Name = "fa-book" };
        public static FontAwesomeIcon fabookmark = new FontAwesomeIcon(83) { Name = "fa-bookmark" };
        public static FontAwesomeIcon fabookmarko = new FontAwesomeIcon(84) { Name = "fa-bookmark-o" };
        public static FontAwesomeIcon fabraille = new FontAwesomeIcon(85) { Name = "fa-braille" };
        public static FontAwesomeIcon fabriefcase = new FontAwesomeIcon(86) { Name = "fa-briefcase" };
        public static FontAwesomeIcon fabtc = new FontAwesomeIcon(87) { Name = "fa-btc" };
        public static FontAwesomeIcon fabug = new FontAwesomeIcon(88) { Name = "fa-bug" };
        public static FontAwesomeIcon fabuilding = new FontAwesomeIcon(89) { Name = "fa-building" };
        public static FontAwesomeIcon fabuildingo = new FontAwesomeIcon(90) { Name = "fa-building-o" };
        public static FontAwesomeIcon fabullhorn = new FontAwesomeIcon(91) { Name = "fa-bullhorn" };
        public static FontAwesomeIcon fabullseye = new FontAwesomeIcon(92) { Name = "fa-bullseye" };
        public static FontAwesomeIcon fabus = new FontAwesomeIcon(93) { Name = "fa-bus" };
        public static FontAwesomeIcon fabuysellads = new FontAwesomeIcon(94) { Name = "fa-buysellads" };
        public static FontAwesomeIcon facalculator = new FontAwesomeIcon(95) { Name = "fa-calculator" };
        public static FontAwesomeIcon facalendar = new FontAwesomeIcon(96) { Name = "fa-calendar" };
        public static FontAwesomeIcon facalendarchecko = new FontAwesomeIcon(97) { Name = "fa-calendar-check-o" };
        public static FontAwesomeIcon facalendarminuso = new FontAwesomeIcon(98) { Name = "fa-calendar-minus-o" };
        public static FontAwesomeIcon facalendaro = new FontAwesomeIcon(99) { Name = "fa-calendar-o" };
        public static FontAwesomeIcon facalendarpluso = new FontAwesomeIcon(100) { Name = "fa-calendar-plus-o" };
        public static FontAwesomeIcon facalendartimeso = new FontAwesomeIcon(101) { Name = "fa-calendar-times-o" };
        public static FontAwesomeIcon facamera = new FontAwesomeIcon(102) { Name = "fa-camera" };
        public static FontAwesomeIcon facameraretro = new FontAwesomeIcon(103) { Name = "fa-camera-retro" };
        public static FontAwesomeIcon facar = new FontAwesomeIcon(104) { Name = "fa-car" };
        public static FontAwesomeIcon facaretdown = new FontAwesomeIcon(105) { Name = "fa-caret-down" };
        public static FontAwesomeIcon facaretleft = new FontAwesomeIcon(106) { Name = "fa-caret-left" };
        public static FontAwesomeIcon facaretright = new FontAwesomeIcon(107) { Name = "fa-caret-right" };
        public static FontAwesomeIcon facaretsquareodown = new FontAwesomeIcon(108) { Name = "fa-caret-square-o-down" };
        public static FontAwesomeIcon facaretsquareoleft = new FontAwesomeIcon(109) { Name = "fa-caret-square-o-left" };
        public static FontAwesomeIcon facaretsquareoright = new FontAwesomeIcon(110) { Name = "fa-caret-square-o-right" };
        public static FontAwesomeIcon facaretsquareoup = new FontAwesomeIcon(111) { Name = "fa-caret-square-o-up" };
        public static FontAwesomeIcon facaretup = new FontAwesomeIcon(112) { Name = "fa-caret-up" };
        public static FontAwesomeIcon facartarrowdown = new FontAwesomeIcon(113) { Name = "fa-cart-arrow-down" };
        public static FontAwesomeIcon facartplus = new FontAwesomeIcon(114) { Name = "fa-cart-plus" };
        public static FontAwesomeIcon facc = new FontAwesomeIcon(115) { Name = "fa-cc" };
        public static FontAwesomeIcon faccamex = new FontAwesomeIcon(116) { Name = "fa-cc-amex" };
        public static FontAwesomeIcon faccdinersclub = new FontAwesomeIcon(117) { Name = "fa-cc-diners-club" };
        public static FontAwesomeIcon faccdiscover = new FontAwesomeIcon(118) { Name = "fa-cc-discover" };
        public static FontAwesomeIcon faccjcb = new FontAwesomeIcon(119) { Name = "fa-cc-jcb" };
        public static FontAwesomeIcon faccmastercard = new FontAwesomeIcon(120) { Name = "fa-cc-mastercard" };
        public static FontAwesomeIcon faccpaypal = new FontAwesomeIcon(121) { Name = "fa-cc-paypal" };
        public static FontAwesomeIcon faccstripe = new FontAwesomeIcon(122) { Name = "fa-cc-stripe" };
        public static FontAwesomeIcon faccvisa = new FontAwesomeIcon(123) { Name = "fa-cc-visa" };
        public static FontAwesomeIcon facertificate = new FontAwesomeIcon(124) { Name = "fa-certificate" };
        public static FontAwesomeIcon fachainbroken = new FontAwesomeIcon(125) { Name = "fa-chain-broken" };
        public static FontAwesomeIcon facheck = new FontAwesomeIcon(126) { Name = "fa-check" };
        public static FontAwesomeIcon facheckcircle = new FontAwesomeIcon(127) { Name = "fa-check-circle" };
        public static FontAwesomeIcon facheckcircleo = new FontAwesomeIcon(128) { Name = "fa-check-circle-o" };
        public static FontAwesomeIcon fachecksquare = new FontAwesomeIcon(129) { Name = "fa-check-square" };
        public static FontAwesomeIcon fachecksquareo = new FontAwesomeIcon(130) { Name = "fa-check-square-o" };
        public static FontAwesomeIcon fachevroncircledown = new FontAwesomeIcon(131) { Name = "fa-chevron-circle-down" };
        public static FontAwesomeIcon fachevroncircleleft = new FontAwesomeIcon(132) { Name = "fa-chevron-circle-left" };
        public static FontAwesomeIcon fachevroncircleright = new FontAwesomeIcon(133) { Name = "fa-chevron-circle-right" };
        public static FontAwesomeIcon fachevroncircleup = new FontAwesomeIcon(134) { Name = "fa-chevron-circle-up" };
        public static FontAwesomeIcon fachevrondown = new FontAwesomeIcon(135) { Name = "fa-chevron-down" };
        public static FontAwesomeIcon fachevronleft = new FontAwesomeIcon(136) { Name = "fa-chevron-left" };
        public static FontAwesomeIcon fachevronright = new FontAwesomeIcon(137) { Name = "fa-chevron-right" };
        public static FontAwesomeIcon fachevronup = new FontAwesomeIcon(138) { Name = "fa-chevron-up" };
        public static FontAwesomeIcon fachild = new FontAwesomeIcon(139) { Name = "fa-child" };
        public static FontAwesomeIcon fachrome = new FontAwesomeIcon(140) { Name = "fa-chrome" };
        public static FontAwesomeIcon facircle = new FontAwesomeIcon(141) { Name = "fa-circle" };
        public static FontAwesomeIcon facircleo = new FontAwesomeIcon(142) { Name = "fa-circle-o" };
        public static FontAwesomeIcon facircleonotch = new FontAwesomeIcon(143) { Name = "fa-circle-o-notch" };
        public static FontAwesomeIcon facirclethin = new FontAwesomeIcon(144) { Name = "fa-circle-thin" };
        public static FontAwesomeIcon faclipboard = new FontAwesomeIcon(145) { Name = "fa-clipboard" };
        public static FontAwesomeIcon faclocko = new FontAwesomeIcon(146) { Name = "fa-clock-o" };
        public static FontAwesomeIcon faclone = new FontAwesomeIcon(147) { Name = "fa-clone" };
        public static FontAwesomeIcon facloud = new FontAwesomeIcon(148) { Name = "fa-cloud" };
        public static FontAwesomeIcon faclouddownload = new FontAwesomeIcon(149) { Name = "fa-cloud-download" };
        public static FontAwesomeIcon facloudupload = new FontAwesomeIcon(150) { Name = "fa-cloud-upload" };
        public static FontAwesomeIcon facode = new FontAwesomeIcon(151) { Name = "fa-code" };
        public static FontAwesomeIcon facodefork = new FontAwesomeIcon(152) { Name = "fa-code-fork" };
        public static FontAwesomeIcon facodepen = new FontAwesomeIcon(153) { Name = "fa-codepen" };
        public static FontAwesomeIcon facodiepie = new FontAwesomeIcon(154) { Name = "fa-codiepie" };
        public static FontAwesomeIcon facoffee = new FontAwesomeIcon(155) { Name = "fa-coffee" };
        public static FontAwesomeIcon facog = new FontAwesomeIcon(156) { Name = "fa-cog" };
        public static FontAwesomeIcon facogs = new FontAwesomeIcon(157) { Name = "fa-cogs" };
        public static FontAwesomeIcon facolumns = new FontAwesomeIcon(158) { Name = "fa-columns" };
        public static FontAwesomeIcon facomment = new FontAwesomeIcon(159) { Name = "fa-comment" };
        public static FontAwesomeIcon facommento = new FontAwesomeIcon(160) { Name = "fa-comment-o" };
        public static FontAwesomeIcon facommenting = new FontAwesomeIcon(161) { Name = "fa-commenting" };
        public static FontAwesomeIcon facommentingo = new FontAwesomeIcon(162) { Name = "fa-commenting-o" };
        public static FontAwesomeIcon facomments = new FontAwesomeIcon(163) { Name = "fa-comments" };
        public static FontAwesomeIcon facommentso = new FontAwesomeIcon(164) { Name = "fa-comments-o" };
        public static FontAwesomeIcon facompass = new FontAwesomeIcon(165) { Name = "fa-compass" };
        public static FontAwesomeIcon facompress = new FontAwesomeIcon(166) { Name = "fa-compress" };
        public static FontAwesomeIcon faconnectdevelop = new FontAwesomeIcon(167) { Name = "fa-connectdevelop" };
        public static FontAwesomeIcon facontao = new FontAwesomeIcon(168) { Name = "fa-contao" };
        public static FontAwesomeIcon facopyright = new FontAwesomeIcon(169) { Name = "fa-copyright" };
        public static FontAwesomeIcon facreativecommons = new FontAwesomeIcon(170) { Name = "fa-creative-commons" };
        public static FontAwesomeIcon facreditcard = new FontAwesomeIcon(171) { Name = "fa-credit-card" };
        public static FontAwesomeIcon facreditcardalt = new FontAwesomeIcon(172) { Name = "fa-credit-card-alt" };
        public static FontAwesomeIcon facrop = new FontAwesomeIcon(173) { Name = "fa-crop" };
        public static FontAwesomeIcon facrosshairs = new FontAwesomeIcon(174) { Name = "fa-crosshairs" };
        public static FontAwesomeIcon facss3 = new FontAwesomeIcon(175) { Name = "fa-css3" };
        public static FontAwesomeIcon facube = new FontAwesomeIcon(176) { Name = "fa-cube" };
        public static FontAwesomeIcon facubes = new FontAwesomeIcon(177) { Name = "fa-cubes" };
        public static FontAwesomeIcon facutlery = new FontAwesomeIcon(178) { Name = "fa-cutlery" };
        public static FontAwesomeIcon fadashcube = new FontAwesomeIcon(179) { Name = "fa-dashcube" };
        public static FontAwesomeIcon fadatabase = new FontAwesomeIcon(180) { Name = "fa-database" };
        public static FontAwesomeIcon fadeaf = new FontAwesomeIcon(181) { Name = "fa-deaf" };
        public static FontAwesomeIcon fadelicious = new FontAwesomeIcon(182) { Name = "fa-delicious" };
        public static FontAwesomeIcon fadesktop = new FontAwesomeIcon(183) { Name = "fa-desktop" };
        public static FontAwesomeIcon fadeviantart = new FontAwesomeIcon(184) { Name = "fa-deviantart" };
        public static FontAwesomeIcon fadiamond = new FontAwesomeIcon(185) { Name = "fa-diamond" };
        public static FontAwesomeIcon fadigg = new FontAwesomeIcon(186) { Name = "fa-digg" };
        public static FontAwesomeIcon fadotcircleo = new FontAwesomeIcon(187) { Name = "fa-dot-circle-o" };
        public static FontAwesomeIcon fadownload = new FontAwesomeIcon(188) { Name = "fa-download" };
        public static FontAwesomeIcon fadribbble = new FontAwesomeIcon(189) { Name = "fa-dribbble" };
        public static FontAwesomeIcon fadropbox = new FontAwesomeIcon(190) { Name = "fa-dropbox" };
        public static FontAwesomeIcon fadrupal = new FontAwesomeIcon(191) { Name = "fa-drupal" };
        public static FontAwesomeIcon faedge = new FontAwesomeIcon(192) { Name = "fa-edge" };
        public static FontAwesomeIcon faeercast = new FontAwesomeIcon(193) { Name = "fa-eercast" };
        public static FontAwesomeIcon faeject = new FontAwesomeIcon(194) { Name = "fa-eject" };
        public static FontAwesomeIcon faellipsish = new FontAwesomeIcon(195) { Name = "fa-ellipsis-h" };
        public static FontAwesomeIcon faellipsisv = new FontAwesomeIcon(196) { Name = "fa-ellipsis-v" };
        public static FontAwesomeIcon faempire = new FontAwesomeIcon(197) { Name = "fa-empire" };
        public static FontAwesomeIcon faenvelope = new FontAwesomeIcon(198) { Name = "fa-envelope" };
        public static FontAwesomeIcon faenvelopeo = new FontAwesomeIcon(199) { Name = "fa-envelope-o" };
        public static FontAwesomeIcon faenvelopeopen = new FontAwesomeIcon(200) { Name = "fa-envelope-open" };
        public static FontAwesomeIcon faenvelopeopeno = new FontAwesomeIcon(201) { Name = "fa-envelope-open-o" };
        public static FontAwesomeIcon faenvelopesquare = new FontAwesomeIcon(202) { Name = "fa-envelope-square" };
        public static FontAwesomeIcon faenvira = new FontAwesomeIcon(203) { Name = "fa-envira" };
        public static FontAwesomeIcon faeraser = new FontAwesomeIcon(204) { Name = "fa-eraser" };
        public static FontAwesomeIcon faetsy = new FontAwesomeIcon(205) { Name = "fa-etsy" };
        public static FontAwesomeIcon faeur = new FontAwesomeIcon(206) { Name = "fa-eur" };
        public static FontAwesomeIcon faexchange = new FontAwesomeIcon(207) { Name = "fa-exchange" };
        public static FontAwesomeIcon faexclamation = new FontAwesomeIcon(208) { Name = "fa-exclamation" };
        public static FontAwesomeIcon faexclamationcircle = new FontAwesomeIcon(209) { Name = "fa-exclamation-circle" };
        public static FontAwesomeIcon faexclamationtriangle = new FontAwesomeIcon(210) { Name = "fa-exclamation-triangle" };
        public static FontAwesomeIcon faexpand = new FontAwesomeIcon(211) { Name = "fa-expand" };
        public static FontAwesomeIcon faexpeditedssl = new FontAwesomeIcon(212) { Name = "fa-expeditedssl" };
        public static FontAwesomeIcon faexternallink = new FontAwesomeIcon(213) { Name = "fa-external-link" };
        public static FontAwesomeIcon faexternallinksquare = new FontAwesomeIcon(214) { Name = "fa-external-link-square" };
        public static FontAwesomeIcon faeye = new FontAwesomeIcon(215) { Name = "fa-eye" };
        public static FontAwesomeIcon faeyeslash = new FontAwesomeIcon(216) { Name = "fa-eye-slash" };
        public static FontAwesomeIcon faeyedropper = new FontAwesomeIcon(217) { Name = "fa-eyedropper" };
        public static FontAwesomeIcon fafacebook = new FontAwesomeIcon(218) { Name = "fa-facebook" };
        public static FontAwesomeIcon fafacebookofficial = new FontAwesomeIcon(219) { Name = "fa-facebook-official" };
        public static FontAwesomeIcon fafacebooksquare = new FontAwesomeIcon(220) { Name = "fa-facebook-square" };
        public static FontAwesomeIcon fafastbackward = new FontAwesomeIcon(221) { Name = "fa-fast-backward" };
        public static FontAwesomeIcon fafastforward = new FontAwesomeIcon(222) { Name = "fa-fast-forward" };
        public static FontAwesomeIcon fafax = new FontAwesomeIcon(223) { Name = "fa-fax" };
        public static FontAwesomeIcon fafemale = new FontAwesomeIcon(224) { Name = "fa-female" };
        public static FontAwesomeIcon fafighterjet = new FontAwesomeIcon(225) { Name = "fa-fighter-jet" };
        public static FontAwesomeIcon fafile = new FontAwesomeIcon(226) { Name = "fa-file" };
        public static FontAwesomeIcon fafilearchiveo = new FontAwesomeIcon(227) { Name = "fa-file-archive-o" };
        public static FontAwesomeIcon fafileaudioo = new FontAwesomeIcon(228) { Name = "fa-file-audio-o" };
        public static FontAwesomeIcon fafilecodeo = new FontAwesomeIcon(229) { Name = "fa-file-code-o" };
        public static FontAwesomeIcon fafileexcelo = new FontAwesomeIcon(230) { Name = "fa-file-excel-o" };
        public static FontAwesomeIcon fafileimageo = new FontAwesomeIcon(231) { Name = "fa-file-image-o" };
        public static FontAwesomeIcon fafileo = new FontAwesomeIcon(232) { Name = "fa-file-o" };
        public static FontAwesomeIcon fafilepdfo = new FontAwesomeIcon(233) { Name = "fa-file-pdf-o" };
        public static FontAwesomeIcon fafilepowerpointo = new FontAwesomeIcon(234) { Name = "fa-file-powerpoint-o" };
        public static FontAwesomeIcon fafiletext = new FontAwesomeIcon(235) { Name = "fa-file-text" };
        public static FontAwesomeIcon fafiletexto = new FontAwesomeIcon(236) { Name = "fa-file-text-o" };
        public static FontAwesomeIcon fafilevideoo = new FontAwesomeIcon(237) { Name = "fa-file-video-o" };
        public static FontAwesomeIcon fafilewordo = new FontAwesomeIcon(238) { Name = "fa-file-word-o" };
        public static FontAwesomeIcon fafileso = new FontAwesomeIcon(239) { Name = "fa-files-o" };
        public static FontAwesomeIcon fafilm = new FontAwesomeIcon(240) { Name = "fa-film" };
        public static FontAwesomeIcon fafilter = new FontAwesomeIcon(241) { Name = "fa-filter" };
        public static FontAwesomeIcon fafire = new FontAwesomeIcon(242) { Name = "fa-fire" };
        public static FontAwesomeIcon fafireextinguisher = new FontAwesomeIcon(243) { Name = "fa-fire-extinguisher" };
        public static FontAwesomeIcon fafirefox = new FontAwesomeIcon(244) { Name = "fa-firefox" };
        public static FontAwesomeIcon fafirstorder = new FontAwesomeIcon(245) { Name = "fa-first-order" };
        public static FontAwesomeIcon faflag = new FontAwesomeIcon(246) { Name = "fa-flag" };
        public static FontAwesomeIcon faflagcheckered = new FontAwesomeIcon(247) { Name = "fa-flag-checkered" };
        public static FontAwesomeIcon faflago = new FontAwesomeIcon(248) { Name = "fa-flag-o" };
        public static FontAwesomeIcon faflask = new FontAwesomeIcon(249) { Name = "fa-flask" };
        public static FontAwesomeIcon faflickr = new FontAwesomeIcon(250) { Name = "fa-flickr" };
        public static FontAwesomeIcon fafloppyo = new FontAwesomeIcon(251) { Name = "fa-floppy-o" };
        public static FontAwesomeIcon fafolder = new FontAwesomeIcon(252) { Name = "fa-folder" };
        public static FontAwesomeIcon fafoldero = new FontAwesomeIcon(253) { Name = "fa-folder-o" };
        public static FontAwesomeIcon fafolderopen = new FontAwesomeIcon(254) { Name = "fa-folder-open" };
        public static FontAwesomeIcon fafolderopeno = new FontAwesomeIcon(255) { Name = "fa-folder-open-o" };
        public static FontAwesomeIcon fafont = new FontAwesomeIcon(256) { Name = "fa-font" };
        public static FontAwesomeIcon fafontawesome = new FontAwesomeIcon(257) { Name = "fa-font-awesome" };
        public static FontAwesomeIcon fafonticons = new FontAwesomeIcon(258) { Name = "fa-fonticons" };
        public static FontAwesomeIcon fafortawesome = new FontAwesomeIcon(259) { Name = "fa-fort-awesome" };
        public static FontAwesomeIcon faforumbee = new FontAwesomeIcon(260) { Name = "fa-forumbee" };
        public static FontAwesomeIcon faforward = new FontAwesomeIcon(261) { Name = "fa-forward" };
        public static FontAwesomeIcon fafoursquare = new FontAwesomeIcon(262) { Name = "fa-foursquare" };
        public static FontAwesomeIcon fafreecodecamp = new FontAwesomeIcon(263) { Name = "fa-free-code-camp" };
        public static FontAwesomeIcon fafrowno = new FontAwesomeIcon(264) { Name = "fa-frown-o" };
        public static FontAwesomeIcon fafutbolo = new FontAwesomeIcon(265) { Name = "fa-futbol-o" };
        public static FontAwesomeIcon fagamepad = new FontAwesomeIcon(266) { Name = "fa-gamepad" };
        public static FontAwesomeIcon fagavel = new FontAwesomeIcon(267) { Name = "fa-gavel" };
        public static FontAwesomeIcon fagbp = new FontAwesomeIcon(268) { Name = "fa-gbp" };
        public static FontAwesomeIcon fagenderless = new FontAwesomeIcon(269) { Name = "fa-genderless" };
        public static FontAwesomeIcon fagetpocket = new FontAwesomeIcon(270) { Name = "fa-get-pocket" };
        public static FontAwesomeIcon fagg = new FontAwesomeIcon(271) { Name = "fa-gg" };
        public static FontAwesomeIcon faggcircle = new FontAwesomeIcon(272) { Name = "fa-gg-circle" };
        public static FontAwesomeIcon fagift = new FontAwesomeIcon(273) { Name = "fa-gift" };
        public static FontAwesomeIcon fagit = new FontAwesomeIcon(274) { Name = "fa-git" };
        public static FontAwesomeIcon fagitsquare = new FontAwesomeIcon(275) { Name = "fa-git-square" };
        public static FontAwesomeIcon fagithub = new FontAwesomeIcon(276) { Name = "fa-github" };
        public static FontAwesomeIcon fagithubalt = new FontAwesomeIcon(277) { Name = "fa-github-alt" };
        public static FontAwesomeIcon fagithubsquare = new FontAwesomeIcon(278) { Name = "fa-github-square" };
        public static FontAwesomeIcon fagitlab = new FontAwesomeIcon(279) { Name = "fa-gitlab" };
        public static FontAwesomeIcon faglass = new FontAwesomeIcon(280) { Name = "fa-glass" };
        public static FontAwesomeIcon faglide = new FontAwesomeIcon(281) { Name = "fa-glide" };
        public static FontAwesomeIcon faglideg = new FontAwesomeIcon(282) { Name = "fa-glide-g" };
        public static FontAwesomeIcon faglobe = new FontAwesomeIcon(283) { Name = "fa-globe" };
        public static FontAwesomeIcon fagoogle = new FontAwesomeIcon(284) { Name = "fa-google" };
        public static FontAwesomeIcon fagoogleplus = new FontAwesomeIcon(285) { Name = "fa-google-plus" };
        public static FontAwesomeIcon fagoogleplusofficial = new FontAwesomeIcon(286) { Name = "fa-google-plus-official" };
        public static FontAwesomeIcon fagoogleplussquare = new FontAwesomeIcon(287) { Name = "fa-google-plus-square" };
        public static FontAwesomeIcon fagooglewallet = new FontAwesomeIcon(288) { Name = "fa-google-wallet" };
        public static FontAwesomeIcon fagraduationcap = new FontAwesomeIcon(289) { Name = "fa-graduation-cap" };
        public static FontAwesomeIcon fagratipay = new FontAwesomeIcon(290) { Name = "fa-gratipay" };
        public static FontAwesomeIcon fagrav = new FontAwesomeIcon(291) { Name = "fa-grav" };
        public static FontAwesomeIcon fahsquare = new FontAwesomeIcon(292) { Name = "fa-h-square" };
        public static FontAwesomeIcon fahackernews = new FontAwesomeIcon(293) { Name = "fa-hacker-news" };
        public static FontAwesomeIcon fahandlizardo = new FontAwesomeIcon(294) { Name = "fa-hand-lizard-o" };
        public static FontAwesomeIcon fahandodown = new FontAwesomeIcon(295) { Name = "fa-hand-o-down" };
        public static FontAwesomeIcon fahandoleft = new FontAwesomeIcon(296) { Name = "fa-hand-o-left" };
        public static FontAwesomeIcon fahandoright = new FontAwesomeIcon(297) { Name = "fa-hand-o-right" };
        public static FontAwesomeIcon fahandoup = new FontAwesomeIcon(298) { Name = "fa-hand-o-up" };
        public static FontAwesomeIcon fahandpapero = new FontAwesomeIcon(299) { Name = "fa-hand-paper-o" };
        public static FontAwesomeIcon fahandpeaceo = new FontAwesomeIcon(300) { Name = "fa-hand-peace-o" };
        public static FontAwesomeIcon fahandpointero = new FontAwesomeIcon(301) { Name = "fa-hand-pointer-o" };
        public static FontAwesomeIcon fahandrocko = new FontAwesomeIcon(302) { Name = "fa-hand-rock-o" };
        public static FontAwesomeIcon fahandscissorso = new FontAwesomeIcon(303) { Name = "fa-hand-scissors-o" };
        public static FontAwesomeIcon fahandspocko = new FontAwesomeIcon(304) { Name = "fa-hand-spock-o" };
        public static FontAwesomeIcon fahandshakeo = new FontAwesomeIcon(305) { Name = "fa-handshake-o" };
        public static FontAwesomeIcon fahashtag = new FontAwesomeIcon(306) { Name = "fa-hashtag" };
        public static FontAwesomeIcon fahddo = new FontAwesomeIcon(307) { Name = "fa-hdd-o" };
        public static FontAwesomeIcon faheader = new FontAwesomeIcon(308) { Name = "fa-header" };
        public static FontAwesomeIcon faheadphones = new FontAwesomeIcon(309) { Name = "fa-headphones" };
        public static FontAwesomeIcon faheart = new FontAwesomeIcon(310) { Name = "fa-heart" };
        public static FontAwesomeIcon fahearto = new FontAwesomeIcon(311) { Name = "fa-heart-o" };
        public static FontAwesomeIcon faheartbeat = new FontAwesomeIcon(312) { Name = "fa-heartbeat" };
        public static FontAwesomeIcon fahistory = new FontAwesomeIcon(313) { Name = "fa-history" };
        public static FontAwesomeIcon fahome = new FontAwesomeIcon(314) { Name = "fa-home" };
        public static FontAwesomeIcon fahospitalo = new FontAwesomeIcon(315) { Name = "fa-hospital-o" };
        public static FontAwesomeIcon fahourglass = new FontAwesomeIcon(316) { Name = "fa-hourglass" };
        public static FontAwesomeIcon fahourglassend = new FontAwesomeIcon(317) { Name = "fa-hourglass-end" };
        public static FontAwesomeIcon fahourglasshalf = new FontAwesomeIcon(318) { Name = "fa-hourglass-half" };
        public static FontAwesomeIcon fahourglasso = new FontAwesomeIcon(319) { Name = "fa-hourglass-o" };
        public static FontAwesomeIcon fahourglassstart = new FontAwesomeIcon(320) { Name = "fa-hourglass-start" };
        public static FontAwesomeIcon fahouzz = new FontAwesomeIcon(321) { Name = "fa-houzz" };
        public static FontAwesomeIcon fahtml5 = new FontAwesomeIcon(322) { Name = "fa-html5" };
        public static FontAwesomeIcon faicursor = new FontAwesomeIcon(323) { Name = "fa-i-cursor" };
        public static FontAwesomeIcon faidbadge = new FontAwesomeIcon(324) { Name = "fa-id-badge" };
        public static FontAwesomeIcon faidcard = new FontAwesomeIcon(325) { Name = "fa-id-card" };
        public static FontAwesomeIcon faidcardo = new FontAwesomeIcon(326) { Name = "fa-id-card-o" };
        public static FontAwesomeIcon fails = new FontAwesomeIcon(327) { Name = "fa-ils" };
        public static FontAwesomeIcon faimdb = new FontAwesomeIcon(328) { Name = "fa-imdb" };
        public static FontAwesomeIcon fainbox = new FontAwesomeIcon(329) { Name = "fa-inbox" };
        public static FontAwesomeIcon faindent = new FontAwesomeIcon(330) { Name = "fa-indent" };
        public static FontAwesomeIcon faindustry = new FontAwesomeIcon(331) { Name = "fa-industry" };
        public static FontAwesomeIcon fainfo = new FontAwesomeIcon(332) { Name = "fa-info" };
        public static FontAwesomeIcon fainfocircle = new FontAwesomeIcon(333) { Name = "fa-info-circle" };
        public static FontAwesomeIcon fainr = new FontAwesomeIcon(334) { Name = "fa-inr" };
        public static FontAwesomeIcon fainstagram = new FontAwesomeIcon(335) { Name = "fa-instagram" };
        public static FontAwesomeIcon fainternetexplorer = new FontAwesomeIcon(336) { Name = "fa-internet-explorer" };
        public static FontAwesomeIcon faioxhost = new FontAwesomeIcon(337) { Name = "fa-ioxhost" };
        public static FontAwesomeIcon faitalic = new FontAwesomeIcon(338) { Name = "fa-italic" };
        public static FontAwesomeIcon fajoomla = new FontAwesomeIcon(339) { Name = "fa-joomla" };
        public static FontAwesomeIcon fajpy = new FontAwesomeIcon(340) { Name = "fa-jpy" };
        public static FontAwesomeIcon fajsfiddle = new FontAwesomeIcon(341) { Name = "fa-jsfiddle" };
        public static FontAwesomeIcon fakey = new FontAwesomeIcon(342) { Name = "fa-key" };
        public static FontAwesomeIcon fakeyboardo = new FontAwesomeIcon(343) { Name = "fa-keyboard-o" };
        public static FontAwesomeIcon fakrw = new FontAwesomeIcon(344) { Name = "fa-krw" };
        public static FontAwesomeIcon falanguage = new FontAwesomeIcon(345) { Name = "fa-language" };
        public static FontAwesomeIcon falaptop = new FontAwesomeIcon(346) { Name = "fa-laptop" };
        public static FontAwesomeIcon falastfm = new FontAwesomeIcon(347) { Name = "fa-lastfm" };
        public static FontAwesomeIcon falastfmsquare = new FontAwesomeIcon(348) { Name = "fa-lastfm-square" };
        public static FontAwesomeIcon faleaf = new FontAwesomeIcon(349) { Name = "fa-leaf" };
        public static FontAwesomeIcon faleanpub = new FontAwesomeIcon(350) { Name = "fa-leanpub" };
        public static FontAwesomeIcon falemono = new FontAwesomeIcon(351) { Name = "fa-lemon-o" };
        public static FontAwesomeIcon faleveldown = new FontAwesomeIcon(352) { Name = "fa-level-down" };
        public static FontAwesomeIcon falevelup = new FontAwesomeIcon(353) { Name = "fa-level-up" };
        public static FontAwesomeIcon falifering = new FontAwesomeIcon(354) { Name = "fa-life-ring" };
        public static FontAwesomeIcon falightbulbo = new FontAwesomeIcon(355) { Name = "fa-lightbulb-o" };
        public static FontAwesomeIcon falinechart = new FontAwesomeIcon(356) { Name = "fa-line-chart" };
        public static FontAwesomeIcon falink = new FontAwesomeIcon(357) { Name = "fa-link" };
        public static FontAwesomeIcon falinkedin = new FontAwesomeIcon(358) { Name = "fa-linkedin" };
        public static FontAwesomeIcon falinkedinsquare = new FontAwesomeIcon(359) { Name = "fa-linkedin-square" };
        public static FontAwesomeIcon falinode = new FontAwesomeIcon(360) { Name = "fa-linode" };
        public static FontAwesomeIcon falinux = new FontAwesomeIcon(361) { Name = "fa-linux" };
        public static FontAwesomeIcon falist = new FontAwesomeIcon(362) { Name = "fa-list" };
        public static FontAwesomeIcon falistalt = new FontAwesomeIcon(363) { Name = "fa-list-alt" };
        public static FontAwesomeIcon falistol = new FontAwesomeIcon(364) { Name = "fa-list-ol" };
        public static FontAwesomeIcon falistul = new FontAwesomeIcon(365) { Name = "fa-list-ul" };
        public static FontAwesomeIcon falocationarrow = new FontAwesomeIcon(366) { Name = "fa-location-arrow" };
        public static FontAwesomeIcon falock = new FontAwesomeIcon(367) { Name = "fa-lock" };
        public static FontAwesomeIcon falongarrowdown = new FontAwesomeIcon(368) { Name = "fa-long-arrow-down" };
        public static FontAwesomeIcon falongarrowleft = new FontAwesomeIcon(369) { Name = "fa-long-arrow-left" };
        public static FontAwesomeIcon falongarrowright = new FontAwesomeIcon(370) { Name = "fa-long-arrow-right" };
        public static FontAwesomeIcon falongarrowup = new FontAwesomeIcon(371) { Name = "fa-long-arrow-up" };
        public static FontAwesomeIcon falowvision = new FontAwesomeIcon(372) { Name = "fa-low-vision" };
        public static FontAwesomeIcon famagic = new FontAwesomeIcon(373) { Name = "fa-magic" };
        public static FontAwesomeIcon famagnet = new FontAwesomeIcon(374) { Name = "fa-magnet" };
        public static FontAwesomeIcon famale = new FontAwesomeIcon(375) { Name = "fa-male" };
        public static FontAwesomeIcon famap = new FontAwesomeIcon(376) { Name = "fa-map" };
        public static FontAwesomeIcon famapmarker = new FontAwesomeIcon(377) { Name = "fa-map-marker" };
        public static FontAwesomeIcon famapo = new FontAwesomeIcon(378) { Name = "fa-map-o" };
        public static FontAwesomeIcon famappin = new FontAwesomeIcon(379) { Name = "fa-map-pin" };
        public static FontAwesomeIcon famapsigns = new FontAwesomeIcon(380) { Name = "fa-map-signs" };
        public static FontAwesomeIcon famars = new FontAwesomeIcon(381) { Name = "fa-mars" };
        public static FontAwesomeIcon famarsdouble = new FontAwesomeIcon(382) { Name = "fa-mars-double" };
        public static FontAwesomeIcon famarsstroke = new FontAwesomeIcon(383) { Name = "fa-mars-stroke" };
        public static FontAwesomeIcon famarsstrokeh = new FontAwesomeIcon(384) { Name = "fa-mars-stroke-h" };
        public static FontAwesomeIcon famarsstrokev = new FontAwesomeIcon(385) { Name = "fa-mars-stroke-v" };
        public static FontAwesomeIcon famaxcdn = new FontAwesomeIcon(386) { Name = "fa-maxcdn" };
        public static FontAwesomeIcon fameanpath = new FontAwesomeIcon(387) { Name = "fa-meanpath" };
        public static FontAwesomeIcon famedium = new FontAwesomeIcon(388) { Name = "fa-medium" };
        public static FontAwesomeIcon famedkit = new FontAwesomeIcon(389) { Name = "fa-medkit" };
        public static FontAwesomeIcon fameetup = new FontAwesomeIcon(390) { Name = "fa-meetup" };
        public static FontAwesomeIcon fameho = new FontAwesomeIcon(391) { Name = "fa-meh-o" };
        public static FontAwesomeIcon famercury = new FontAwesomeIcon(392) { Name = "fa-mercury" };
        public static FontAwesomeIcon famicrochip = new FontAwesomeIcon(393) { Name = "fa-microchip" };
        public static FontAwesomeIcon famicrophone = new FontAwesomeIcon(394) { Name = "fa-microphone" };
        public static FontAwesomeIcon famicrophoneslash = new FontAwesomeIcon(395) { Name = "fa-microphone-slash" };
        public static FontAwesomeIcon faminus = new FontAwesomeIcon(396) { Name = "fa-minus" };
        public static FontAwesomeIcon faminuscircle = new FontAwesomeIcon(397) { Name = "fa-minus-circle" };
        public static FontAwesomeIcon faminussquare = new FontAwesomeIcon(398) { Name = "fa-minus-square" };
        public static FontAwesomeIcon faminussquareo = new FontAwesomeIcon(399) { Name = "fa-minus-square-o" };
        public static FontAwesomeIcon famixcloud = new FontAwesomeIcon(400) { Name = "fa-mixcloud" };
        public static FontAwesomeIcon famobile = new FontAwesomeIcon(401) { Name = "fa-mobile" };
        public static FontAwesomeIcon famodx = new FontAwesomeIcon(402) { Name = "fa-modx" };
        public static FontAwesomeIcon famoney = new FontAwesomeIcon(403) { Name = "fa-money" };
        public static FontAwesomeIcon famoono = new FontAwesomeIcon(404) { Name = "fa-moon-o" };
        public static FontAwesomeIcon famotorcycle = new FontAwesomeIcon(405) { Name = "fa-motorcycle" };
        public static FontAwesomeIcon famousepointer = new FontAwesomeIcon(406) { Name = "fa-mouse-pointer" };
        public static FontAwesomeIcon famusic = new FontAwesomeIcon(407) { Name = "fa-music" };
        public static FontAwesomeIcon faneuter = new FontAwesomeIcon(408) { Name = "fa-neuter" };
        public static FontAwesomeIcon fanewspapero = new FontAwesomeIcon(409) { Name = "fa-newspaper-o" };
        public static FontAwesomeIcon faobjectgroup = new FontAwesomeIcon(410) { Name = "fa-object-group" };
        public static FontAwesomeIcon faobjectungroup = new FontAwesomeIcon(411) { Name = "fa-object-ungroup" };
        public static FontAwesomeIcon faodnoklassniki = new FontAwesomeIcon(412) { Name = "fa-odnoklassniki" };
        public static FontAwesomeIcon faodnoklassnikisquare = new FontAwesomeIcon(413) { Name = "fa-odnoklassniki-square" };
        public static FontAwesomeIcon faopencart = new FontAwesomeIcon(414) { Name = "fa-opencart" };
        public static FontAwesomeIcon faopenid = new FontAwesomeIcon(415) { Name = "fa-openid" };
        public static FontAwesomeIcon faopera = new FontAwesomeIcon(416) { Name = "fa-opera" };
        public static FontAwesomeIcon faoptinmonster = new FontAwesomeIcon(417) { Name = "fa-optin-monster" };
        public static FontAwesomeIcon faoutdent = new FontAwesomeIcon(418) { Name = "fa-outdent" };
        public static FontAwesomeIcon fapagelines = new FontAwesomeIcon(419) { Name = "fa-pagelines" };
        public static FontAwesomeIcon fapaintbrush = new FontAwesomeIcon(420) { Name = "fa-paint-brush" };
        public static FontAwesomeIcon fapaperplane = new FontAwesomeIcon(421) { Name = "fa-paper-plane" };
        public static FontAwesomeIcon fapaperplaneo = new FontAwesomeIcon(422) { Name = "fa-paper-plane-o" };
        public static FontAwesomeIcon fapaperclip = new FontAwesomeIcon(423) { Name = "fa-paperclip" };
        public static FontAwesomeIcon faparagraph = new FontAwesomeIcon(424) { Name = "fa-paragraph" };
        public static FontAwesomeIcon fapause = new FontAwesomeIcon(425) { Name = "fa-pause" };
        public static FontAwesomeIcon fapausecircle = new FontAwesomeIcon(426) { Name = "fa-pause-circle" };
        public static FontAwesomeIcon fapausecircleo = new FontAwesomeIcon(427) { Name = "fa-pause-circle-o" };
        public static FontAwesomeIcon fapaw = new FontAwesomeIcon(428) { Name = "fa-paw" };
        public static FontAwesomeIcon fapaypal = new FontAwesomeIcon(429) { Name = "fa-paypal" };
        public static FontAwesomeIcon fapencil = new FontAwesomeIcon(430) { Name = "fa-pencil" };
        public static FontAwesomeIcon fapencilsquare = new FontAwesomeIcon(431) { Name = "fa-pencil-square" };
        public static FontAwesomeIcon fapencilsquareo = new FontAwesomeIcon(432) { Name = "fa-pencil-square-o" };
        public static FontAwesomeIcon fapercent = new FontAwesomeIcon(433) { Name = "fa-percent" };
        public static FontAwesomeIcon faphone = new FontAwesomeIcon(434) { Name = "fa-phone" };
        public static FontAwesomeIcon faphonesquare = new FontAwesomeIcon(435) { Name = "fa-phone-square" };
        public static FontAwesomeIcon fapictureo = new FontAwesomeIcon(436) { Name = "fa-picture-o" };
        public static FontAwesomeIcon fapiechart = new FontAwesomeIcon(437) { Name = "fa-pie-chart" };
        public static FontAwesomeIcon fapiedpiper = new FontAwesomeIcon(438) { Name = "fa-pied-piper" };
        public static FontAwesomeIcon fapiedpiperalt = new FontAwesomeIcon(439) { Name = "fa-pied-piper-alt" };
        public static FontAwesomeIcon fapiedpiperpp = new FontAwesomeIcon(440) { Name = "fa-pied-piper-pp" };
        public static FontAwesomeIcon fapinterest = new FontAwesomeIcon(441) { Name = "fa-pinterest" };
        public static FontAwesomeIcon fapinterestp = new FontAwesomeIcon(442) { Name = "fa-pinterest-p" };
        public static FontAwesomeIcon fapinterestsquare = new FontAwesomeIcon(443) { Name = "fa-pinterest-square" };
        public static FontAwesomeIcon faplane = new FontAwesomeIcon(444) { Name = "fa-plane" };
        public static FontAwesomeIcon faplay = new FontAwesomeIcon(445) { Name = "fa-play" };
        public static FontAwesomeIcon faplaycircle = new FontAwesomeIcon(446) { Name = "fa-play-circle" };
        public static FontAwesomeIcon faplaycircleo = new FontAwesomeIcon(447) { Name = "fa-play-circle-o" };
        public static FontAwesomeIcon faplug = new FontAwesomeIcon(448) { Name = "fa-plug" };
        public static FontAwesomeIcon faplus = new FontAwesomeIcon(449) { Name = "fa-plus" };
        public static FontAwesomeIcon fapluscircle = new FontAwesomeIcon(450) { Name = "fa-plus-circle" };
        public static FontAwesomeIcon faplussquare = new FontAwesomeIcon(451) { Name = "fa-plus-square" };
        public static FontAwesomeIcon faplussquareo = new FontAwesomeIcon(452) { Name = "fa-plus-square-o" };
        public static FontAwesomeIcon fapodcast = new FontAwesomeIcon(453) { Name = "fa-podcast" };
        public static FontAwesomeIcon fapoweroff = new FontAwesomeIcon(454) { Name = "fa-power-off" };
        public static FontAwesomeIcon faprint = new FontAwesomeIcon(455) { Name = "fa-print" };
        public static FontAwesomeIcon faproducthunt = new FontAwesomeIcon(456) { Name = "fa-product-hunt" };
        public static FontAwesomeIcon fapuzzlepiece = new FontAwesomeIcon(457) { Name = "fa-puzzle-piece" };
        public static FontAwesomeIcon faqq = new FontAwesomeIcon(458) { Name = "fa-qq" };
        public static FontAwesomeIcon faqrcode = new FontAwesomeIcon(459) { Name = "fa-qrcode" };
        public static FontAwesomeIcon faquestion = new FontAwesomeIcon(460) { Name = "fa-question" };
        public static FontAwesomeIcon faquestioncircle = new FontAwesomeIcon(461) { Name = "fa-question-circle" };
        public static FontAwesomeIcon faquestioncircleo = new FontAwesomeIcon(462) { Name = "fa-question-circle-o" };
        public static FontAwesomeIcon faquora = new FontAwesomeIcon(463) { Name = "fa-quora" };
        public static FontAwesomeIcon faquoteleft = new FontAwesomeIcon(464) { Name = "fa-quote-left" };
        public static FontAwesomeIcon faquoteright = new FontAwesomeIcon(465) { Name = "fa-quote-right" };
        public static FontAwesomeIcon farandom = new FontAwesomeIcon(466) { Name = "fa-random" };
        public static FontAwesomeIcon faravelry = new FontAwesomeIcon(467) { Name = "fa-ravelry" };
        public static FontAwesomeIcon farebel = new FontAwesomeIcon(468) { Name = "fa-rebel" };
        public static FontAwesomeIcon farecycle = new FontAwesomeIcon(469) { Name = "fa-recycle" };
        public static FontAwesomeIcon fareddit = new FontAwesomeIcon(470) { Name = "fa-reddit" };
        public static FontAwesomeIcon faredditalien = new FontAwesomeIcon(471) { Name = "fa-reddit-alien" };
        public static FontAwesomeIcon faredditsquare = new FontAwesomeIcon(472) { Name = "fa-reddit-square" };
        public static FontAwesomeIcon farefresh = new FontAwesomeIcon(473) { Name = "fa-refresh" };
        public static FontAwesomeIcon faregistered = new FontAwesomeIcon(474) { Name = "fa-registered" };
        public static FontAwesomeIcon farenren = new FontAwesomeIcon(475) { Name = "fa-renren" };
        public static FontAwesomeIcon farepeat = new FontAwesomeIcon(476) { Name = "fa-repeat" };
        public static FontAwesomeIcon fareply = new FontAwesomeIcon(477) { Name = "fa-reply" };
        public static FontAwesomeIcon fareplyall = new FontAwesomeIcon(478) { Name = "fa-reply-all" };
        public static FontAwesomeIcon faretweet = new FontAwesomeIcon(479) { Name = "fa-retweet" };
        public static FontAwesomeIcon faroad = new FontAwesomeIcon(480) { Name = "fa-road" };
        public static FontAwesomeIcon farocket = new FontAwesomeIcon(481) { Name = "fa-rocket" };
        public static FontAwesomeIcon farss = new FontAwesomeIcon(482) { Name = "fa-rss" };
        public static FontAwesomeIcon farsssquare = new FontAwesomeIcon(483) { Name = "fa-rss-square" };
        public static FontAwesomeIcon farub = new FontAwesomeIcon(484) { Name = "fa-rub" };
        public static FontAwesomeIcon fasafari = new FontAwesomeIcon(485) { Name = "fa-safari" };
        public static FontAwesomeIcon fascissors = new FontAwesomeIcon(486) { Name = "fa-scissors" };
        public static FontAwesomeIcon fascribd = new FontAwesomeIcon(487) { Name = "fa-scribd" };
        public static FontAwesomeIcon fasearch = new FontAwesomeIcon(488) { Name = "fa-search" };
        public static FontAwesomeIcon fasearchminus = new FontAwesomeIcon(489) { Name = "fa-search-minus" };
        public static FontAwesomeIcon fasearchplus = new FontAwesomeIcon(490) { Name = "fa-search-plus" };
        public static FontAwesomeIcon fasellsy = new FontAwesomeIcon(491) { Name = "fa-sellsy" };
        public static FontAwesomeIcon faserver = new FontAwesomeIcon(492) { Name = "fa-server" };
        public static FontAwesomeIcon fashare = new FontAwesomeIcon(493) { Name = "fa-share" };
        public static FontAwesomeIcon fasharealt = new FontAwesomeIcon(494) { Name = "fa-share-alt" };
        public static FontAwesomeIcon fasharealtsquare = new FontAwesomeIcon(495) { Name = "fa-share-alt-square" };
        public static FontAwesomeIcon fasharesquare = new FontAwesomeIcon(496) { Name = "fa-share-square" };
        public static FontAwesomeIcon fasharesquareo = new FontAwesomeIcon(497) { Name = "fa-share-square-o" };
        public static FontAwesomeIcon fashield = new FontAwesomeIcon(498) { Name = "fa-shield" };
        public static FontAwesomeIcon faship = new FontAwesomeIcon(499) { Name = "fa-ship" };
        public static FontAwesomeIcon fashirtsinbulk = new FontAwesomeIcon(500) { Name = "fa-shirtsinbulk" };
        public static FontAwesomeIcon fashoppingbag = new FontAwesomeIcon(501) { Name = "fa-shopping-bag" };
        public static FontAwesomeIcon fashoppingbasket = new FontAwesomeIcon(502) { Name = "fa-shopping-basket" };
        public static FontAwesomeIcon fashoppingcart = new FontAwesomeIcon(503) { Name = "fa-shopping-cart" };
        public static FontAwesomeIcon fashower = new FontAwesomeIcon(504) { Name = "fa-shower" };
        public static FontAwesomeIcon fasignin = new FontAwesomeIcon(505) { Name = "fa-sign-in" };
        public static FontAwesomeIcon fasignlanguage = new FontAwesomeIcon(506) { Name = "fa-sign-language" };
        public static FontAwesomeIcon fasignout = new FontAwesomeIcon(507) { Name = "fa-sign-out" };
        public static FontAwesomeIcon fasignal = new FontAwesomeIcon(508) { Name = "fa-signal" };
        public static FontAwesomeIcon fasimplybuilt = new FontAwesomeIcon(509) { Name = "fa-simplybuilt" };
        public static FontAwesomeIcon fasitemap = new FontAwesomeIcon(510) { Name = "fa-sitemap" };
        public static FontAwesomeIcon faskyatlas = new FontAwesomeIcon(511) { Name = "fa-skyatlas" };
        public static FontAwesomeIcon faskype = new FontAwesomeIcon(512) { Name = "fa-skype" };
        public static FontAwesomeIcon faslack = new FontAwesomeIcon(513) { Name = "fa-slack" };
        public static FontAwesomeIcon fasliders = new FontAwesomeIcon(514) { Name = "fa-sliders" };
        public static FontAwesomeIcon faslideshare = new FontAwesomeIcon(515) { Name = "fa-slideshare" };
        public static FontAwesomeIcon fasmileo = new FontAwesomeIcon(516) { Name = "fa-smile-o" };
        public static FontAwesomeIcon fasnapchat = new FontAwesomeIcon(517) { Name = "fa-snapchat" };
        public static FontAwesomeIcon fasnapchatghost = new FontAwesomeIcon(518) { Name = "fa-snapchat-ghost" };
        public static FontAwesomeIcon fasnapchatsquare = new FontAwesomeIcon(519) { Name = "fa-snapchat-square" };
        public static FontAwesomeIcon fasnowflakeo = new FontAwesomeIcon(520) { Name = "fa-snowflake-o" };
        public static FontAwesomeIcon fasort = new FontAwesomeIcon(521) { Name = "fa-sort" };
        public static FontAwesomeIcon fasortalphaasc = new FontAwesomeIcon(522) { Name = "fa-sort-alpha-asc" };
        public static FontAwesomeIcon fasortalphadesc = new FontAwesomeIcon(523) { Name = "fa-sort-alpha-desc" };
        public static FontAwesomeIcon fasortamountasc = new FontAwesomeIcon(524) { Name = "fa-sort-amount-asc" };
        public static FontAwesomeIcon fasortamountdesc = new FontAwesomeIcon(525) { Name = "fa-sort-amount-desc" };
        public static FontAwesomeIcon fasortasc = new FontAwesomeIcon(526) { Name = "fa-sort-asc" };
        public static FontAwesomeIcon fasortdesc = new FontAwesomeIcon(527) { Name = "fa-sort-desc" };
        public static FontAwesomeIcon fasortnumericasc = new FontAwesomeIcon(528) { Name = "fa-sort-numeric-asc" };
        public static FontAwesomeIcon fasortnumericdesc = new FontAwesomeIcon(529) { Name = "fa-sort-numeric-desc" };
        public static FontAwesomeIcon fasoundcloud = new FontAwesomeIcon(530) { Name = "fa-soundcloud" };
        public static FontAwesomeIcon faspaceshuttle = new FontAwesomeIcon(531) { Name = "fa-space-shuttle" };
        public static FontAwesomeIcon faspinner = new FontAwesomeIcon(532) { Name = "fa-spinner" };
        public static FontAwesomeIcon faspoon = new FontAwesomeIcon(533) { Name = "fa-spoon" };
        public static FontAwesomeIcon faspotify = new FontAwesomeIcon(534) { Name = "fa-spotify" };
        public static FontAwesomeIcon fasquare = new FontAwesomeIcon(535) { Name = "fa-square" };
        public static FontAwesomeIcon fasquareo = new FontAwesomeIcon(536) { Name = "fa-square-o" };
        public static FontAwesomeIcon fastackexchange = new FontAwesomeIcon(537) { Name = "fa-stack-exchange" };
        public static FontAwesomeIcon fastackoverflow = new FontAwesomeIcon(538) { Name = "fa-stack-overflow" };
        public static FontAwesomeIcon fastar = new FontAwesomeIcon(539) { Name = "fa-star" };
        public static FontAwesomeIcon fastarhalf = new FontAwesomeIcon(540) { Name = "fa-star-half" };
        public static FontAwesomeIcon fastarhalfo = new FontAwesomeIcon(541) { Name = "fa-star-half-o" };
        public static FontAwesomeIcon fastaro = new FontAwesomeIcon(542) { Name = "fa-star-o" };
        public static FontAwesomeIcon fasteam = new FontAwesomeIcon(543) { Name = "fa-steam" };
        public static FontAwesomeIcon fasteamsquare = new FontAwesomeIcon(544) { Name = "fa-steam-square" };
        public static FontAwesomeIcon fastepbackward = new FontAwesomeIcon(545) { Name = "fa-step-backward" };
        public static FontAwesomeIcon fastepforward = new FontAwesomeIcon(546) { Name = "fa-step-forward" };
        public static FontAwesomeIcon fastethoscope = new FontAwesomeIcon(547) { Name = "fa-stethoscope" };
        public static FontAwesomeIcon fastickynote = new FontAwesomeIcon(548) { Name = "fa-sticky-note" };
        public static FontAwesomeIcon fastickynoteo = new FontAwesomeIcon(549) { Name = "fa-sticky-note-o" };
        public static FontAwesomeIcon fastop = new FontAwesomeIcon(550) { Name = "fa-stop" };
        public static FontAwesomeIcon fastopcircle = new FontAwesomeIcon(551) { Name = "fa-stop-circle" };
        public static FontAwesomeIcon fastopcircleo = new FontAwesomeIcon(552) { Name = "fa-stop-circle-o" };
        public static FontAwesomeIcon fastreetview = new FontAwesomeIcon(553) { Name = "fa-street-view" };
        public static FontAwesomeIcon fastrikethrough = new FontAwesomeIcon(554) { Name = "fa-strikethrough" };
        public static FontAwesomeIcon fastumbleupon = new FontAwesomeIcon(555) { Name = "fa-stumbleupon" };
        public static FontAwesomeIcon fastumbleuponcircle = new FontAwesomeIcon(556) { Name = "fa-stumbleupon-circle" };
        public static FontAwesomeIcon fasubscript = new FontAwesomeIcon(557) { Name = "fa-subscript" };
        public static FontAwesomeIcon fasubway = new FontAwesomeIcon(558) { Name = "fa-subway" };
        public static FontAwesomeIcon fasuitcase = new FontAwesomeIcon(559) { Name = "fa-suitcase" };
        public static FontAwesomeIcon fasuno = new FontAwesomeIcon(560) { Name = "fa-sun-o" };
        public static FontAwesomeIcon fasuperpowers = new FontAwesomeIcon(561) { Name = "fa-superpowers" };
        public static FontAwesomeIcon fasuperscript = new FontAwesomeIcon(562) { Name = "fa-superscript" };
        public static FontAwesomeIcon fatable = new FontAwesomeIcon(563) { Name = "fa-table" };
        public static FontAwesomeIcon fatablet = new FontAwesomeIcon(564) { Name = "fa-tablet" };
        public static FontAwesomeIcon fatachometer = new FontAwesomeIcon(565) { Name = "fa-tachometer" };
        public static FontAwesomeIcon fatag = new FontAwesomeIcon(566) { Name = "fa-tag" };
        public static FontAwesomeIcon fatags = new FontAwesomeIcon(567) { Name = "fa-tags" };
        public static FontAwesomeIcon fatasks = new FontAwesomeIcon(568) { Name = "fa-tasks" };
        public static FontAwesomeIcon fataxi = new FontAwesomeIcon(569) { Name = "fa-taxi" };
        public static FontAwesomeIcon fatelegram = new FontAwesomeIcon(570) { Name = "fa-telegram" };
        public static FontAwesomeIcon fatelevision = new FontAwesomeIcon(571) { Name = "fa-television" };
        public static FontAwesomeIcon fatencentweibo = new FontAwesomeIcon(572) { Name = "fa-tencent-weibo" };
        public static FontAwesomeIcon faterminal = new FontAwesomeIcon(573) { Name = "fa-terminal" };
        public static FontAwesomeIcon fatextheight = new FontAwesomeIcon(574) { Name = "fa-text-height" };
        public static FontAwesomeIcon fatextwidth = new FontAwesomeIcon(575) { Name = "fa-text-width" };
        public static FontAwesomeIcon fath = new FontAwesomeIcon(576) { Name = "fa-th" };
        public static FontAwesomeIcon fathlarge = new FontAwesomeIcon(577) { Name = "fa-th-large" };
        public static FontAwesomeIcon fathlist = new FontAwesomeIcon(578) { Name = "fa-th-list" };
        public static FontAwesomeIcon fathemeisle = new FontAwesomeIcon(579) { Name = "fa-themeisle" };
        public static FontAwesomeIcon fathermometerempty = new FontAwesomeIcon(580) { Name = "fa-thermometer-empty" };
        public static FontAwesomeIcon fathermometerfull = new FontAwesomeIcon(581) { Name = "fa-thermometer-full" };
        public static FontAwesomeIcon fathermometerhalf = new FontAwesomeIcon(582) { Name = "fa-thermometer-half" };
        public static FontAwesomeIcon fathermometerquarter = new FontAwesomeIcon(583) { Name = "fa-thermometer-quarter" };
        public static FontAwesomeIcon fathermometerthreequarters = new FontAwesomeIcon(584) { Name = "fa-thermometer-three-quarters" };
        public static FontAwesomeIcon fathumbtack = new FontAwesomeIcon(585) { Name = "fa-thumb-tack" };
        public static FontAwesomeIcon fathumbsdown = new FontAwesomeIcon(586) { Name = "fa-thumbs-down" };
        public static FontAwesomeIcon fathumbsodown = new FontAwesomeIcon(587) { Name = "fa-thumbs-o-down" };
        public static FontAwesomeIcon fathumbsoup = new FontAwesomeIcon(588) { Name = "fa-thumbs-o-up" };
        public static FontAwesomeIcon fathumbsup = new FontAwesomeIcon(589) { Name = "fa-thumbs-up" };
        public static FontAwesomeIcon faticket = new FontAwesomeIcon(590) { Name = "fa-ticket" };
        public static FontAwesomeIcon fatimes = new FontAwesomeIcon(591) { Name = "fa-times" };
        public static FontAwesomeIcon fatimescircle = new FontAwesomeIcon(592) { Name = "fa-times-circle" };
        public static FontAwesomeIcon fatimescircleo = new FontAwesomeIcon(593) { Name = "fa-times-circle-o" };
        public static FontAwesomeIcon fatint = new FontAwesomeIcon(594) { Name = "fa-tint" };
        public static FontAwesomeIcon fatoggleoff = new FontAwesomeIcon(595) { Name = "fa-toggle-off" };
        public static FontAwesomeIcon fatoggleon = new FontAwesomeIcon(596) { Name = "fa-toggle-on" };
        public static FontAwesomeIcon fatrademark = new FontAwesomeIcon(597) { Name = "fa-trademark" };
        public static FontAwesomeIcon fatrain = new FontAwesomeIcon(598) { Name = "fa-train" };
        public static FontAwesomeIcon fatransgender = new FontAwesomeIcon(599) { Name = "fa-transgender" };
        public static FontAwesomeIcon fatransgenderalt = new FontAwesomeIcon(600) { Name = "fa-transgender-alt" };
        public static FontAwesomeIcon fatrash = new FontAwesomeIcon(601) { Name = "fa-trash" };
        public static FontAwesomeIcon fatrasho = new FontAwesomeIcon(602) { Name = "fa-trash-o" };
        public static FontAwesomeIcon fatree = new FontAwesomeIcon(603) { Name = "fa-tree" };
        public static FontAwesomeIcon fatrello = new FontAwesomeIcon(604) { Name = "fa-trello" };
        public static FontAwesomeIcon fatripadvisor = new FontAwesomeIcon(605) { Name = "fa-tripadvisor" };
        public static FontAwesomeIcon fatrophy = new FontAwesomeIcon(606) { Name = "fa-trophy" };
        public static FontAwesomeIcon fatruck = new FontAwesomeIcon(607) { Name = "fa-truck" };
        public static FontAwesomeIcon fatry = new FontAwesomeIcon(608) { Name = "fa-try" };
        public static FontAwesomeIcon fatty = new FontAwesomeIcon(609) { Name = "fa-tty" };
        public static FontAwesomeIcon fatumblr = new FontAwesomeIcon(610) { Name = "fa-tumblr" };
        public static FontAwesomeIcon fatumblrsquare = new FontAwesomeIcon(611) { Name = "fa-tumblr-square" };
        public static FontAwesomeIcon fatwitch = new FontAwesomeIcon(612) { Name = "fa-twitch" };
        public static FontAwesomeIcon fatwitter = new FontAwesomeIcon(613) { Name = "fa-twitter" };
        public static FontAwesomeIcon fatwittersquare = new FontAwesomeIcon(614) { Name = "fa-twitter-square" };
        public static FontAwesomeIcon faumbrella = new FontAwesomeIcon(615) { Name = "fa-umbrella" };
        public static FontAwesomeIcon faunderline = new FontAwesomeIcon(616) { Name = "fa-underline" };
        public static FontAwesomeIcon faundo = new FontAwesomeIcon(617) { Name = "fa-undo" };
        public static FontAwesomeIcon fauniversalaccess = new FontAwesomeIcon(618) { Name = "fa-universal-access" };
        public static FontAwesomeIcon fauniversity = new FontAwesomeIcon(619) { Name = "fa-university" };
        public static FontAwesomeIcon faunlock = new FontAwesomeIcon(620) { Name = "fa-unlock" };
        public static FontAwesomeIcon faunlockalt = new FontAwesomeIcon(621) { Name = "fa-unlock-alt" };
        public static FontAwesomeIcon faupload = new FontAwesomeIcon(622) { Name = "fa-upload" };
        public static FontAwesomeIcon fausb = new FontAwesomeIcon(623) { Name = "fa-usb" };
        public static FontAwesomeIcon fausd = new FontAwesomeIcon(624) { Name = "fa-usd" };
        public static FontAwesomeIcon fauser = new FontAwesomeIcon(625) { Name = "fa-user" };
        public static FontAwesomeIcon fausercircle = new FontAwesomeIcon(626) { Name = "fa-user-circle" };
        public static FontAwesomeIcon fausercircleo = new FontAwesomeIcon(627) { Name = "fa-user-circle-o" };
        public static FontAwesomeIcon fausermd = new FontAwesomeIcon(628) { Name = "fa-user-md" };
        public static FontAwesomeIcon fausero = new FontAwesomeIcon(629) { Name = "fa-user-o" };
        public static FontAwesomeIcon fauserplus = new FontAwesomeIcon(630) { Name = "fa-user-plus" };
        public static FontAwesomeIcon fausersecret = new FontAwesomeIcon(631) { Name = "fa-user-secret" };
        public static FontAwesomeIcon fausertimes = new FontAwesomeIcon(632) { Name = "fa-user-times" };
        public static FontAwesomeIcon fausers = new FontAwesomeIcon(633) { Name = "fa-users" };
        public static FontAwesomeIcon favenus = new FontAwesomeIcon(634) { Name = "fa-venus" };
        public static FontAwesomeIcon favenusdouble = new FontAwesomeIcon(635) { Name = "fa-venus-double" };
        public static FontAwesomeIcon favenusmars = new FontAwesomeIcon(636) { Name = "fa-venus-mars" };
        public static FontAwesomeIcon faviacoin = new FontAwesomeIcon(637) { Name = "fa-viacoin" };
        public static FontAwesomeIcon faviadeo = new FontAwesomeIcon(638) { Name = "fa-viadeo" };
        public static FontAwesomeIcon faviadeosquare = new FontAwesomeIcon(639) { Name = "fa-viadeo-square" };
        public static FontAwesomeIcon favideocamera = new FontAwesomeIcon(640) { Name = "fa-video-camera" };
        public static FontAwesomeIcon favimeo = new FontAwesomeIcon(641) { Name = "fa-vimeo" };
        public static FontAwesomeIcon favimeosquare = new FontAwesomeIcon(642) { Name = "fa-vimeo-square" };
        public static FontAwesomeIcon favine = new FontAwesomeIcon(643) { Name = "fa-vine" };
        public static FontAwesomeIcon favk = new FontAwesomeIcon(644) { Name = "fa-vk" };
        public static FontAwesomeIcon favolumecontrolphone = new FontAwesomeIcon(645) { Name = "fa-volume-control-phone" };
        public static FontAwesomeIcon favolumedown = new FontAwesomeIcon(646) { Name = "fa-volume-down" };
        public static FontAwesomeIcon favolumeoff = new FontAwesomeIcon(647) { Name = "fa-volume-off" };
        public static FontAwesomeIcon favolumeup = new FontAwesomeIcon(648) { Name = "fa-volume-up" };
        public static FontAwesomeIcon faweibo = new FontAwesomeIcon(649) { Name = "fa-weibo" };
        public static FontAwesomeIcon faweixin = new FontAwesomeIcon(650) { Name = "fa-weixin" };
        public static FontAwesomeIcon fawhatsapp = new FontAwesomeIcon(651) { Name = "fa-whatsapp" };
        public static FontAwesomeIcon fawheelchair = new FontAwesomeIcon(652) { Name = "fa-wheelchair" };
        public static FontAwesomeIcon fawheelchairalt = new FontAwesomeIcon(653) { Name = "fa-wheelchair-alt" };
        public static FontAwesomeIcon fawifi = new FontAwesomeIcon(654) { Name = "fa-wifi" };
        public static FontAwesomeIcon fawikipediaw = new FontAwesomeIcon(655) { Name = "fa-wikipedia-w" };
        public static FontAwesomeIcon fawindowclose = new FontAwesomeIcon(656) { Name = "fa-window-close" };
        public static FontAwesomeIcon fawindowcloseo = new FontAwesomeIcon(657) { Name = "fa-window-close-o" };
        public static FontAwesomeIcon fawindowmaximize = new FontAwesomeIcon(658) { Name = "fa-window-maximize" };
        public static FontAwesomeIcon fawindowminimize = new FontAwesomeIcon(659) { Name = "fa-window-minimize" };
        public static FontAwesomeIcon fawindowrestore = new FontAwesomeIcon(660) { Name = "fa-window-restore" };
        public static FontAwesomeIcon fawindows = new FontAwesomeIcon(661) { Name = "fa-windows" };
        public static FontAwesomeIcon fawordpress = new FontAwesomeIcon(662) { Name = "fa-wordpress" };
        public static FontAwesomeIcon fawpbeginner = new FontAwesomeIcon(663) { Name = "fa-wpbeginner" };
        public static FontAwesomeIcon fawpexplorer = new FontAwesomeIcon(664) { Name = "fa-wpexplorer" };
        public static FontAwesomeIcon fawpforms = new FontAwesomeIcon(665) { Name = "fa-wpforms" };
        public static FontAwesomeIcon fawrench = new FontAwesomeIcon(666) { Name = "fa-wrench" };
        public static FontAwesomeIcon faxing = new FontAwesomeIcon(667) { Name = "fa-xing" };
        public static FontAwesomeIcon faxingsquare = new FontAwesomeIcon(668) { Name = "fa-xing-square" };
        public static FontAwesomeIcon faycombinator = new FontAwesomeIcon(669) { Name = "fa-y-combinator" };
        public static FontAwesomeIcon fayahoo = new FontAwesomeIcon(670) { Name = "fa-yahoo" };
        public static FontAwesomeIcon fayelp = new FontAwesomeIcon(671) { Name = "fa-yelp" };
        public static FontAwesomeIcon fayoast = new FontAwesomeIcon(672) { Name = "fa-yoast" };
        public static FontAwesomeIcon fayoutube = new FontAwesomeIcon(673) { Name = "fa-youtube" };
        public static FontAwesomeIcon fayoutubeplay = new FontAwesomeIcon(674) { Name = "fa-youtube-play" };
        public static FontAwesomeIcon fayoutubesquare = new FontAwesomeIcon(675) { Name = "fa-youtube-square" };

        public FontAwesomeIcons() : base(new[] {
            fa500px,
            faaddressbook,
            faaddressbooko,
            faaddresscard,
            faaddresscardo,
            faadjust,
            faadn,
            faaligncenter,
            faalignjustify,
            faalignleft,
            faalignright,
            faamazon,
            faambulance,
            faamericansignlanguageinterpreting,
            faanchor,
            faandroid,
            faangellist,
            faangledoubledown,
            faangledoubleleft,
            faangledoubleright,
            faangledoubleup,
            faangledown,
            faangleleft,
            faangleright,
            faangleup,
            faapple,
            faarchive,
            faareachart,
            faarrowcircledown,
            faarrowcircleleft,
            faarrowcircleodown,
            faarrowcircleoleft,
            faarrowcircleoright,
            faarrowcircleoup,
            faarrowcircleright,
            faarrowcircleup,
            faarrowdown,
            faarrowleft,
            faarrowright,
            faarrowup,
            faarrows,
            faarrowsalt,
            faarrowsh,
            faarrowsv,
            faassistivelisteningsystems,
            faasterisk,
            faat,
            faaudiodescription,
            fabackward,
            fabalancescale,
            faban,
            fabandcamp,
            fabarchart,
            fabarcode,
            fabars,
            fabath,
            fabatteryempty,
            fabatteryfull,
            fabatteryhalf,
            fabatteryquarter,
            fabatterythreequarters,
            fabed,
            fabeer,
            fabehance,
            fabehancesquare,
            fabell,
            fabello,
            fabellslash,
            fabellslasho,
            fabicycle,
            fabinoculars,
            fabirthdaycake,
            fabitbucket,
            fabitbucketsquare,
            fablacktie,
            fablind,
            fabluetooth,
            fabluetoothb,
            fabold,
            fabolt,
            fabomb,
            fabook,
            fabookmark,
            fabookmarko,
            fabraille,
            fabriefcase,
            fabtc,
            fabug,
            fabuilding,
            fabuildingo,
            fabullhorn,
            fabullseye,
            fabus,
            fabuysellads,
            facalculator,
            facalendar,
            facalendarchecko,
            facalendarminuso,
            facalendaro,
            facalendarpluso,
            facalendartimeso,
            facamera,
            facameraretro,
            facar,
            facaretdown,
            facaretleft,
            facaretright,
            facaretsquareodown,
            facaretsquareoleft,
            facaretsquareoright,
            facaretsquareoup,
            facaretup,
            facartarrowdown,
            facartplus,
            facc,
            faccamex,
            faccdinersclub,
            faccdiscover,
            faccjcb,
            faccmastercard,
            faccpaypal,
            faccstripe,
            faccvisa,
            facertificate,
            fachainbroken,
            facheck,
            facheckcircle,
            facheckcircleo,
            fachecksquare,
            fachecksquareo,
            fachevroncircledown,
            fachevroncircleleft,
            fachevroncircleright,
            fachevroncircleup,
            fachevrondown,
            fachevronleft,
            fachevronright,
            fachevronup,
            fachild,
            fachrome,
            facircle,
            facircleo,
            facircleonotch,
            facirclethin,
            faclipboard,
            faclocko,
            faclone,
            facloud,
            faclouddownload,
            facloudupload,
            facode,
            facodefork,
            facodepen,
            facodiepie,
            facoffee,
            facog,
            facogs,
            facolumns,
            facomment,
            facommento,
            facommenting,
            facommentingo,
            facomments,
            facommentso,
            facompass,
            facompress,
            faconnectdevelop,
            facontao,
            facopyright,
            facreativecommons,
            facreditcard,
            facreditcardalt,
            facrop,
            facrosshairs,
            facss3,
            facube,
            facubes,
            facutlery,
            fadashcube,
            fadatabase,
            fadeaf,
            fadelicious,
            fadesktop,
            fadeviantart,
            fadiamond,
            fadigg,
            fadotcircleo,
            fadownload,
            fadribbble,
            fadropbox,
            fadrupal,
            faedge,
            faeercast,
            faeject,
            faellipsish,
            faellipsisv,
            faempire,
            faenvelope,
            faenvelopeo,
            faenvelopeopen,
            faenvelopeopeno,
            faenvelopesquare,
            faenvira,
            faeraser,
            faetsy,
            faeur,
            faexchange,
            faexclamation,
            faexclamationcircle,
            faexclamationtriangle,
            faexpand,
            faexpeditedssl,
            faexternallink,
            faexternallinksquare,
            faeye,
            faeyeslash,
            faeyedropper,
            fafacebook,
            fafacebookofficial,
            fafacebooksquare,
            fafastbackward,
            fafastforward,
            fafax,
            fafemale,
            fafighterjet,
            fafile,
            fafilearchiveo,
            fafileaudioo,
            fafilecodeo,
            fafileexcelo,
            fafileimageo,
            fafileo,
            fafilepdfo,
            fafilepowerpointo,
            fafiletext,
            fafiletexto,
            fafilevideoo,
            fafilewordo,
            fafileso,
            fafilm,
            fafilter,
            fafire,
            fafireextinguisher,
            fafirefox,
            fafirstorder,
            faflag,
            faflagcheckered,
            faflago,
            faflask,
            faflickr,
            fafloppyo,
            fafolder,
            fafoldero,
            fafolderopen,
            fafolderopeno,
            fafont,
            fafontawesome,
            fafonticons,
            fafortawesome,
            faforumbee,
            faforward,
            fafoursquare,
            fafreecodecamp,
            fafrowno,
            fafutbolo,
            fagamepad,
            fagavel,
            fagbp,
            fagenderless,
            fagetpocket,
            fagg,
            faggcircle,
            fagift,
            fagit,
            fagitsquare,
            fagithub,
            fagithubalt,
            fagithubsquare,
            fagitlab,
            faglass,
            faglide,
            faglideg,
            faglobe,
            fagoogle,
            fagoogleplus,
            fagoogleplusofficial,
            fagoogleplussquare,
            fagooglewallet,
            fagraduationcap,
            fagratipay,
            fagrav,
            fahsquare,
            fahackernews,
            fahandlizardo,
            fahandodown,
            fahandoleft,
            fahandoright,
            fahandoup,
            fahandpapero,
            fahandpeaceo,
            fahandpointero,
            fahandrocko,
            fahandscissorso,
            fahandspocko,
            fahandshakeo,
            fahashtag,
            fahddo,
            faheader,
            faheadphones,
            faheart,
            fahearto,
            faheartbeat,
            fahistory,
            fahome,
            fahospitalo,
            fahourglass,
            fahourglassend,
            fahourglasshalf,
            fahourglasso,
            fahourglassstart,
            fahouzz,
            fahtml5,
            faicursor,
            faidbadge,
            faidcard,
            faidcardo,
            fails,
            faimdb,
            fainbox,
            faindent,
            faindustry,
            fainfo,
            fainfocircle,
            fainr,
            fainstagram,
            fainternetexplorer,
            faioxhost,
            faitalic,
            fajoomla,
            fajpy,
            fajsfiddle,
            fakey,
            fakeyboardo,
            fakrw,
            falanguage,
            falaptop,
            falastfm,
            falastfmsquare,
            faleaf,
            faleanpub,
            falemono,
            faleveldown,
            falevelup,
            falifering,
            falightbulbo,
            falinechart,
            falink,
            falinkedin,
            falinkedinsquare,
            falinode,
            falinux,
            falist,
            falistalt,
            falistol,
            falistul,
            falocationarrow,
            falock,
            falongarrowdown,
            falongarrowleft,
            falongarrowright,
            falongarrowup,
            falowvision,
            famagic,
            famagnet,
            famale,
            famap,
            famapmarker,
            famapo,
            famappin,
            famapsigns,
            famars,
            famarsdouble,
            famarsstroke,
            famarsstrokeh,
            famarsstrokev,
            famaxcdn,
            fameanpath,
            famedium,
            famedkit,
            fameetup,
            fameho,
            famercury,
            famicrochip,
            famicrophone,
            famicrophoneslash,
            faminus,
            faminuscircle,
            faminussquare,
            faminussquareo,
            famixcloud,
            famobile,
            famodx,
            famoney,
            famoono,
            famotorcycle,
            famousepointer,
            famusic,
            faneuter,
            fanewspapero,
            faobjectgroup,
            faobjectungroup,
            faodnoklassniki,
            faodnoklassnikisquare,
            faopencart,
            faopenid,
            faopera,
            faoptinmonster,
            faoutdent,
            fapagelines,
            fapaintbrush,
            fapaperplane,
            fapaperplaneo,
            fapaperclip,
            faparagraph,
            fapause,
            fapausecircle,
            fapausecircleo,
            fapaw,
            fapaypal,
            fapencil,
            fapencilsquare,
            fapencilsquareo,
            fapercent,
            faphone,
            faphonesquare,
            fapictureo,
            fapiechart,
            fapiedpiper,
            fapiedpiperalt,
            fapiedpiperpp,
            fapinterest,
            fapinterestp,
            fapinterestsquare,
            faplane,
            faplay,
            faplaycircle,
            faplaycircleo,
            faplug,
            faplus,
            fapluscircle,
            faplussquare,
            faplussquareo,
            fapodcast,
            fapoweroff,
            faprint,
            faproducthunt,
            fapuzzlepiece,
            faqq,
            faqrcode,
            faquestion,
            faquestioncircle,
            faquestioncircleo,
            faquora,
            faquoteleft,
            faquoteright,
            farandom,
            faravelry,
            farebel,
            farecycle,
            fareddit,
            faredditalien,
            faredditsquare,
            farefresh,
            faregistered,
            farenren,
            farepeat,
            fareply,
            fareplyall,
            faretweet,
            faroad,
            farocket,
            farss,
            farsssquare,
            farub,
            fasafari,
            fascissors,
            fascribd,
            fasearch,
            fasearchminus,
            fasearchplus,
            fasellsy,
            faserver,
            fashare,
            fasharealt,
            fasharealtsquare,
            fasharesquare,
            fasharesquareo,
            fashield,
            faship,
            fashirtsinbulk,
            fashoppingbag,
            fashoppingbasket,
            fashoppingcart,
            fashower,
            fasignin,
            fasignlanguage,
            fasignout,
            fasignal,
            fasimplybuilt,
            fasitemap,
            faskyatlas,
            faskype,
            faslack,
            fasliders,
            faslideshare,
            fasmileo,
            fasnapchat,
            fasnapchatghost,
            fasnapchatsquare,
            fasnowflakeo,
            fasort,
            fasortalphaasc,
            fasortalphadesc,
            fasortamountasc,
            fasortamountdesc,
            fasortasc,
            fasortdesc,
            fasortnumericasc,
            fasortnumericdesc,
            fasoundcloud,
            faspaceshuttle,
            faspinner,
            faspoon,
            faspotify,
            fasquare,
            fasquareo,
            fastackexchange,
            fastackoverflow,
            fastar,
            fastarhalf,
            fastarhalfo,
            fastaro,
            fasteam,
            fasteamsquare,
            fastepbackward,
            fastepforward,
            fastethoscope,
            fastickynote,
            fastickynoteo,
            fastop,
            fastopcircle,
            fastopcircleo,
            fastreetview,
            fastrikethrough,
            fastumbleupon,
            fastumbleuponcircle,
            fasubscript,
            fasubway,
            fasuitcase,
            fasuno,
            fasuperpowers,
            fasuperscript,
            fatable,
            fatablet,
            fatachometer,
            fatag,
            fatags,
            fatasks,
            fataxi,
            fatelegram,
            fatelevision,
            fatencentweibo,
            faterminal,
            fatextheight,
            fatextwidth,
            fath,
            fathlarge,
            fathlist,
            fathemeisle,
            fathermometerempty,
            fathermometerfull,
            fathermometerhalf,
            fathermometerquarter,
            fathermometerthreequarters,
            fathumbtack,
            fathumbsdown,
            fathumbsodown,
            fathumbsoup,
            fathumbsup,
            faticket,
            fatimes,
            fatimescircle,
            fatimescircleo,
            fatint,
            fatoggleoff,
            fatoggleon,
            fatrademark,
            fatrain,
            fatransgender,
            fatransgenderalt,
            fatrash,
            fatrasho,
            fatree,
            fatrello,
            fatripadvisor,
            fatrophy,
            fatruck,
            fatry,
            fatty,
            fatumblr,
            fatumblrsquare,
            fatwitch,
            fatwitter,
            fatwittersquare,
            faumbrella,
            faunderline,
            faundo,
            fauniversalaccess,
            fauniversity,
            faunlock,
            faunlockalt,
            faupload,
            fausb,
            fausd,
            fauser,
            fausercircle,
            fausercircleo,
            fausermd,
            fausero,
            fauserplus,
            fausersecret,
            fausertimes,
            fausers,
            favenus,
            favenusdouble,
            favenusmars,
            faviacoin,
            faviadeo,
            faviadeosquare,
            favideocamera,
            favimeo,
            favimeosquare,
            favine,
            favk,
            favolumecontrolphone,
            favolumedown,
            favolumeoff,
            favolumeup,
            faweibo,
            faweixin,
            fawhatsapp,
            fawheelchair,
            fawheelchairalt,
            fawifi,
            fawikipediaw,
            fawindowclose,
            fawindowcloseo,
            fawindowmaximize,
            fawindowminimize,
            fawindowrestore,
            fawindows,
            fawordpress,
            fawpbeginner,
            fawpexplorer,
            fawpforms,
            fawrench,
            faxing,
            faxingsquare,
            faycombinator,
            fayahoo,
            fayelp,
            fayoast,
            fayoutube,
            fayoutubeplay,
            fayoutubesquare
        }) { }
    }
}