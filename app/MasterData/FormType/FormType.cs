﻿using System;

namespace MasterData
{
    [Serializable]
    public class FormType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }

        public FormType() { }

        internal FormType(int id) { Id = id; }
    }
}