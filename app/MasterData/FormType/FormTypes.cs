﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class FormTypes : ReadOnlyCollection<FormType>, iMasterDataCollection
    {
        public static FormType Login = new FormType(1) { Name = "Login", Code = "Login" };
        public static FormType ProposalMotor = new FormType(2) { Name = "ProposalMotor", Code = "ProposalMotor" };
        public static FormType ForgetPassword = new FormType(3) { Name = "ForgetPassword", Code = "ForgetPassword" };
        public static FormType ProposalHouseholdContents = new FormType(4) { Name = "ProposalHouseholdContents", Code = "ProposalHouseholdContents" };
        public static FormType ProposalHouseOwners = new FormType(5) { Name = "ProposalHouseOwners", Code = "ProposalHouseOwners" };
        public static FormType ProposalAllRisks = new FormType(6) { Name = "ProposalAllRisks", Code = "ProposalAllRisks" };
        public static FormType ProposalTyreRim = new FormType(7) { Name = "ProposalTyreRim", Code = "ProposalTyreRim" };
        public static FormType ProposalFuneral = new FormType(8) { Name = "ProposalFuneral", Code = "ProposalFuneral" };
        public static FormType ProposalExtendedWarranty = new FormType(9) { Name = "ProposalExtendedWarranty", Code = "ProposalExtendedWarranty" };
        public static FormType ProposalPersonalAccident = new FormType(10) { Name = "ProposalPersonalAccident", Code = "ProposalPersonalAccident" };
        public static FormType ProposalPreOwnedWarranty = new FormType(11) { Name = "ProposalPreOwnedWarranty", Code = "ProposalPreOwnedWarranty" };
        public static FormType ProposalPersonalLegalLiability = new FormType(12) { Name = "ProposalPersonalLegalLiability", Code = "ProposalPersonalLegalLiability" };
        public static FormType PolicyBinding = new FormType(13) { Name = "PolicyBinding", Code = "PolicyBinding" };

        public FormTypes() : base(new[] {
            Login,
            ProposalMotor,
            ForgetPassword,
            ProposalHouseholdContents,
            ProposalHouseOwners,
            ProposalAllRisks,
            ProposalTyreRim,
            ProposalFuneral,
            ProposalExtendedWarranty,
            ProposalPersonalAccident,
            ProposalPreOwnedWarranty,
            ProposalPersonalLegalLiability,
            PolicyBinding
        }) { }
    }
}