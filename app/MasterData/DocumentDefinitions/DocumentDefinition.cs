﻿using System;

namespace MasterData
{
    [Serializable]
    public class DocumentDefinition : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public virtual DocumentType DocumentTypeId { get; set; }

        public virtual int XCoordinate { get; set; }

        public virtual int YCoordinate { get; set; }

        public virtual int FieldWidth { get; set; }

        public virtual int FieldHeight { get; set; }

        public virtual int PageNumber { get; set; }

        public DocumentDefinition() { }

        public virtual DocumentFieldType DocumentFieldType { get; set; }

        internal DocumentDefinition(int id) { Id = id; }
    }
}

