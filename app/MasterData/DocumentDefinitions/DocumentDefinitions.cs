﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class DocumentDefinitions : ReadOnlyCollection<DocumentDefinition>, iMasterDataCollection
    {
        // TO set coordinates for new documents 
        // 1 - Generate PDF
        // 2 - Use GIMP or PS & set measurement to pt (points) to get x,y coordinates
        public static DocumentDefinition BrokerAppointment = new DocumentDefinition(1)
        {
            Name = "Broker appointment details",
            DocumentTypeId = DocumentTypes.BrokerAppointment,
            FieldHeight = 50,
            FieldWidth = 150,
            XCoordinate = 87,
            YCoordinate = 479,
            PageNumber = 1,
            DocumentFieldType = DocumentFieldTypes.Signature
        };

        public static DocumentDefinition DebitOrder = new DocumentDefinition(2)
        {
            Name = "Debit order details",
            DocumentTypeId = DocumentTypes.DebitOrder,
            FieldHeight = 50,
            FieldWidth = 150,
            XCoordinate = 96,
            YCoordinate = 140,
            PageNumber = 2,
            DocumentFieldType = DocumentFieldTypes.Signature
        };

        public static DocumentDefinition DebitOrderSignnatureExtra1 = new DocumentDefinition(7)
        {
            Name = "Debit order details signature extra 1",
            DocumentTypeId = DocumentTypes.DebitOrder,
            FieldHeight = 50,
            FieldWidth = 150,
            XCoordinate = 314,
            YCoordinate = 158,
            PageNumber = 1,
            DocumentFieldType = DocumentFieldTypes.Signature
        };

        public static DocumentDefinition DebitOrderSignedAt = new DocumentDefinition(10)
        {
            Name = "Debit order signed at",
            DocumentTypeId = DocumentTypes.DebitOrder,
            FieldHeight = 50,
            FieldWidth = 150,
            XCoordinate = 160,
            YCoordinate = 96,
            PageNumber = 2,
            DocumentFieldType = DocumentFieldTypes.PlainText
        };

        public static DocumentDefinition DebitOrderPlainText1 = new DocumentDefinition(8)
        {
            Name = "Debit order details plain text 1",
            DocumentTypeId = DocumentTypes.DebitOrder,
            FieldHeight = 50,
            FieldWidth = 150,
            XCoordinate = 204,
            YCoordinate = 466,
            PageNumber = 1,
            DocumentFieldType = DocumentFieldTypes.PlainText
        };

        public static DocumentDefinition PolicyDocument = new DocumentDefinition(3)
        {
            Name = "Policy document details",
            DocumentTypeId = DocumentTypes.PolicyDocument,
            FieldHeight = 50,
            FieldWidth = 100,
            XCoordinate = 129,
            YCoordinate = 503,
            PageNumber = 1,
            DocumentFieldType = DocumentFieldTypes.Signature
        };

        public static DocumentDefinition AdviceRecordDocument = new DocumentDefinition(4)
        {
            Name = "Advice record document details",
            DocumentTypeId = DocumentTypes.AdviceRecordDocument,
            FieldHeight = 50,
            FieldWidth = 100,
            XCoordinate = 48,
            YCoordinate = 747,
            PageNumber = 3,
            DocumentFieldType = DocumentFieldTypes.Signature
        };

        public static DocumentDefinition ServiceLevelAgreementDocument = new DocumentDefinition(5)
        {
            Name = "Service level agreement document",
            DocumentTypeId = DocumentTypes.ServiceLevelAgreementDocument,
            FieldHeight = 50,
            FieldWidth = 150,
            XCoordinate = 89,
            YCoordinate = 575,
            PageNumber = 4,
            DocumentFieldType = DocumentFieldTypes.Signature
        };

        public static DocumentDefinition ServiceLevelAgreementDocumentExtra1 = new DocumentDefinition(9)
        {
            Name = "Service level agreement document extra 1",
            DocumentTypeId = DocumentTypes.ServiceLevelAgreementDocument,
            FieldHeight = 50,
            FieldWidth = 150,
            XCoordinate = 341,
            YCoordinate = 576,
            PageNumber = 4,
            DocumentFieldType = DocumentFieldTypes.Signature
        };

        public static DocumentDefinition ProtectionOfPersonalInfo = new DocumentDefinition(6)
        {
            Name = "Protection Of Personal Information document",
            DocumentTypeId = DocumentTypes.ProtectionOfPersonalInfo,
            FieldHeight = 50,
            FieldWidth = 150,
            XCoordinate = 204,
            YCoordinate = 164,
            PageNumber = 2,
            DocumentFieldType = DocumentFieldTypes.Signature
        };

        public static DocumentDefinition ProtectionOfPersonalInfoSignedAt = new DocumentDefinition(11)
        {
            Name = "Protection Of Personal Information document signed at",
            DocumentTypeId = DocumentTypes.ProtectionOfPersonalInfo,
            FieldHeight = 50,
            FieldWidth = 150,
            XCoordinate = 180,
            YCoordinate = 124,
            PageNumber = 2,
            DocumentFieldType = DocumentFieldTypes.PlainText
        };

        public DocumentDefinitions() : base(new[] { BrokerAppointment,
                                                    DebitOrder, DebitOrderSignnatureExtra1, DebitOrderPlainText1, DebitOrderSignedAt,
                                                    PolicyDocument,
                                                    AdviceRecordDocument,
                                                    ServiceLevelAgreementDocument, ServiceLevelAgreementDocumentExtra1,
                                                    ProtectionOfPersonalInfo, ProtectionOfPersonalInfoSignedAt  }) { }
    }
}
