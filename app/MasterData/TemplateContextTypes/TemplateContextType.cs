﻿using System;

namespace MasterData
{
    [Serializable]
    public class TemplateContextType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public TemplateContextType() { }

        internal TemplateContextType(int id)
        {
            Id = id;
        }
    }
}