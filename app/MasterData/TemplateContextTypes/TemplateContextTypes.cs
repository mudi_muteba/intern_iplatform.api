﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class TemplateContextTypes : ReadOnlyCollection<TemplateContextType>, iMasterDataCollection
    {
        public static TemplateContextType ComparativeQuoteTemplate = new TemplateContextType(1) { Name = "Comparative Quote Template" };
        public static TemplateContextType ForgotPasswordTemplate = new TemplateContextType(2) { Name = "Forgot Password Template" };
        public static TemplateContextType ImportTemplate = new TemplateContextType(3) { Name = "Import Template" };
        public static TemplateContextType LogoTemplate = new TemplateContextType(4) { Name = "Logo Template" };
        public static TemplateContextType NewsletterTemplate = new TemplateContextType(5) { Name = "Newsletter Template" };
        public static TemplateContextType NotificationTemplate = new TemplateContextType(6) { Name = "Notification Template" };
        public static TemplateContextType OneTimePinTemplate = new TemplateContextType(7) { Name = "One-Time PIN Template" };
        public static TemplateContextType ReportSchedulerTemplate = new TemplateContextType(8) { Name = "Scheduler Template" };
        public static TemplateContextType StyleTemplate = new TemplateContextType(9) { Name = "Style Template" };
        public static TemplateContextType SystemInformationTemplate = new TemplateContextType(10) { Name = "System Information Template" };
        public static TemplateContextType WelcomeTemplate = new TemplateContextType(11) { Name = "Welcome Template" };

        public TemplateContextTypes() : base(new[] {
            ComparativeQuoteTemplate,
            ForgotPasswordTemplate,
            ImportTemplate,
            LogoTemplate,
            NewsletterTemplate,
            NotificationTemplate,
            OneTimePinTemplate,
            ReportSchedulerTemplate,
            StyleTemplate,
            SystemInformationTemplate,
            WelcomeTemplate
        }) { }
    }
}