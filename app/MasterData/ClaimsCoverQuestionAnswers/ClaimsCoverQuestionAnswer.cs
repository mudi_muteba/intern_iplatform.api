using System;

namespace MasterData
{
    [Serializable]
    public class ClaimsCoverQuestionAnswer : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual Cover Cover { get; set; }
        public virtual ClaimsQuestionAnswer ClaimsQuestionAnswer { get; set; }
        public ClaimsCoverQuestionAnswer() { }

        internal ClaimsCoverQuestionAnswer(int id) { Id = id; }
    }
}