using System;

namespace MasterData
{
    [Serializable]
    public class AuthorisationGroup : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public AuthorisationGroup() { }

        internal AuthorisationGroup(int id) { Id = id; }
    }
}