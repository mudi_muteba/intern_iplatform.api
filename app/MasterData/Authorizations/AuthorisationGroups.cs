using System.Collections.ObjectModel;

namespace MasterData
{
    public class AuthorisationGroups : ReadOnlyCollection<AuthorisationGroup>, iMasterDataCollection
    {
        public static AuthorisationGroup CallCentreAgent = new AuthorisationGroup(1) { Name = "Call Centre Agent" };
        public static AuthorisationGroup CallCentreManager = new AuthorisationGroup(2) { Name = "Call Centre Manager" };
        public static AuthorisationGroup Broker = new AuthorisationGroup(3) { Name = "Broker" };
        public static AuthorisationGroup Client = new AuthorisationGroup(4) { Name = "Client" };
        public static AuthorisationGroup Admin = new AuthorisationGroup(5) { Name = "Admin" };

        public AuthorisationGroups() : base(new[] { CallCentreAgent, CallCentreManager, Broker, Client, Admin }) { }
    }
}