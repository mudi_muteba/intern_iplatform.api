using System;

namespace MasterData
{
    [Serializable]
    public class MaritalStatus : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual int VisibleIndex { get; set; }

        public MaritalStatus() { }

        internal MaritalStatus(int id) { Id = id; }
    }
}