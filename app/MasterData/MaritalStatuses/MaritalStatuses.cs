using System.Collections.ObjectModel;

namespace MasterData
{
    public class MaritalStatuses : ReadOnlyCollection<MaritalStatus>, iMasterDataCollection
    {
        public static MaritalStatus CivilUnion = new MaritalStatus(1) { Name = "Civil Union", Code = "CivilUnion", VisibleIndex = 5 };
        public static MaritalStatus Divorced = new MaritalStatus(2) { Name = "Divorced", Code = "Divorced", VisibleIndex = 2 };
        public static MaritalStatus DomesticPartner = new MaritalStatus(3) { Name = "Domestic Partner", Code = "DomesticPartner", VisibleIndex = 4 };
        public static MaritalStatus Married = new MaritalStatus(4) { Name = "Married", Code = "Married", VisibleIndex = 0 };
        public static MaritalStatus Other = new MaritalStatus(5) { Name = "Other", Code = "Other", VisibleIndex = 8 };
        public static MaritalStatus Separated = new MaritalStatus(6) { Name = "Separated", Code = "Separated", VisibleIndex = 3 };
        public static MaritalStatus Single = new MaritalStatus(7) { Name = "Single", Code = "Single", VisibleIndex = 1 };
        public static MaritalStatus UnincorporatedAssociation = new MaritalStatus(8) { Name = "Unincorporated Association", Code = "UnincorporatedAssociation", VisibleIndex = 6 };
        public static MaritalStatus Unknown = new MaritalStatus(9) { Name = "Unknown", Code = "Unknown", VisibleIndex = 9 };
        public static MaritalStatus Widowed = new MaritalStatus(10) { Name = "Widowed", Code = "Widowed", VisibleIndex = 7 };

        public MaritalStatuses() : base(new[] { CivilUnion, Divorced, DomesticPartner, Married, Other, Separated, Single, UnincorporatedAssociation, Unknown, Widowed }) { }
    }
}