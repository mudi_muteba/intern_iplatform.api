﻿using System;

namespace MasterData
{
    [Serializable]
    public abstract class BaseEntity
    {
        public virtual int Id { get; set; }

        protected BaseEntity() { }
    }
}