﻿using System;

namespace MasterData
{
    [Serializable]
    public class ReportCategory : BaseEntity, iMasterData
    {
        public virtual int ParentId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Icon { get; set; }
        public virtual bool IsDefault { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsVisibleInReportViewer { get; set; }

        public ReportCategory() { }

        internal ReportCategory(int id, int parentId)
        {
            Id = id;
            ParentId = parentId;
        }
    }
}