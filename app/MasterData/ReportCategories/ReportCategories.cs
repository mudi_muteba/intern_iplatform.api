﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ReportCategories : ReadOnlyCollection<ReportCategory>, iMasterDataCollection
    {
        public static ReportCategory Quotes = new ReportCategory(1, 0) { Name = "Quotes", Icon = "fa fa-money", IsDefault = true, IsActive = true, IsVisibleInReportViewer = true };
        public static ReportCategory Leads = new ReportCategory(2, 0) { Name = "Leads", Icon = "fa fa-users", IsDefault = false, IsActive = true, IsVisibleInReportViewer = true };
        public static ReportCategory DigitalSignatures = new ReportCategory(3, 0) { Name = "Digitally Signable Documents", Icon = "fa fa-pencil-square-o", IsDefault = false, IsActive = true, IsVisibleInReportViewer = false };
        public static ReportCategory System = new ReportCategory(4, 0) { Name = "System", Icon = "fa fa-gear", IsDefault = false, IsActive = true, IsVisibleInReportViewer = true };

        public ReportCategories() : base(new[] { Quotes, Leads, DigitalSignatures, System }) { }
    }
}