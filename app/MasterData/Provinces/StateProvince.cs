﻿using System;

namespace MasterData
{
    [Serializable]
    public class StateProvince : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual Country Country { get; set; }

        public StateProvince() { }

        internal StateProvince(int id) { Id = id; }
    }
}