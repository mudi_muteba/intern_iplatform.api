using System.Collections.ObjectModel;
using System.Linq;

namespace MasterData
{
    public class StateProvinces : ReadOnlyCollection<StateProvince>, iMasterDataCollection
    {
        public static StateProvince NotSpecified = new StateProvince(1) { Name = "[Not Specified]", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince EasternCape = new StateProvince(2) { Name = "Eastern Cape", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince FreeState = new StateProvince(3) { Name = "Free State", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince Gauteng = new StateProvince(4) { Name = "Gauteng", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince KwaZuluNatal = new StateProvince(5) { Name = "KwaZulu-Natal", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince Limpopo = new StateProvince(6) { Name = "Limpopo", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince Mpumalanga = new StateProvince(7) { Name = "Mpumalanga", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince NorthernCape = new StateProvince(8) { Name = "Northern Cape", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince NorthWest = new StateProvince(9) { Name = "North West", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince WesternCape = new StateProvince(10) { Name = "Western Cape", Country = new Countries().FirstOrDefault(x => x.Id == 197) };
        public static StateProvince Lamu = new StateProvince(11) { Name = "Lamu", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Embu = new StateProvince(12) { Name = "Embu", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Marsabit = new StateProvince(13) { Name = "Marsabit", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kisii = new StateProvince(14) { Name = "Kisii", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kericho = new StateProvince(15) { Name = "Kericho", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Nakuru = new StateProvince(16) { Name = "Nakuru", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Samburu = new StateProvince(17) { Name = "Samburu", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Baringo = new StateProvince(18) { Name = "Baringo", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Garissa = new StateProvince(19) { Name = "Garissa", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince WestPokot = new StateProvince(20) { Name = "West Pokot", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Machakos = new StateProvince(21) { Name = "Machakos", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Muranga = new StateProvince(22) { Name = "Murang'a", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Bungoma = new StateProvince(23) { Name = "Bungoma", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Wajir = new StateProvince(24) { Name = "Wajir", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kirinyaga = new StateProvince(25) { Name = "Kirinyaga", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Turkana = new StateProvince(26) { Name = "Turkana", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Makueni = new StateProvince(27) { Name = "Makueni", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince UasinGishu = new StateProvince(28) { Name = "Uasin Gishu", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kajiado = new StateProvince(29) { Name = "Kajiado", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Isiolo = new StateProvince(30) { Name = "Isiolo", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Vihiga = new StateProvince(31) { Name = "Vihiga", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Elgeyomarakwet = new StateProvince(32) { Name = "Elgeyo/marakwet", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kisumu = new StateProvince(33) { Name = "Kisumu", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince HomaBay = new StateProvince(34) { Name = "Homa Bay", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kiambu = new StateProvince(35) { Name = "Kiambu", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Nyamira = new StateProvince(36) { Name = "Nyamira", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kitui = new StateProvince(37) { Name = "Kitui", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Nyeri = new StateProvince(38) { Name = "Nyeri", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kakamega = new StateProvince(39) { Name = "Kakamega", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Nairobi = new StateProvince(40) { Name = "Nairobi", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Nyandarua = new StateProvince(41) { Name = "Nyandarua", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kwale = new StateProvince(42) { Name = "Kwale", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Migori = new StateProvince(43) { Name = "Migori", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince TransNzoia = new StateProvince(44) { Name = "Trans Nzoia", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Busia = new StateProvince(45) { Name = "Busia", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince TaitaTaveta = new StateProvince(46) { Name = "Taita Taveta", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Siaya = new StateProvince(47) { Name = "Siaya", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Narok = new StateProvince(48) { Name = "Narok", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Meru = new StateProvince(49) { Name = "Meru", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Mandera = new StateProvince(50) { Name = "Mandera", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Kilifi = new StateProvince(51) { Name = "Kilifi", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince TharakaNithi = new StateProvince(52) { Name = "Tharaka - Nithi", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Nandi = new StateProvince(53) { Name = "Nandi", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Mombasa = new StateProvince(54) { Name = "Mombasa", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince TanaRiver = new StateProvince(55) { Name = "Tana River", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Laikipia = new StateProvince(56) { Name = "Laikipia", Country = new Countries().FirstOrDefault(x => x.Id == 110) };
        public static StateProvince Bomet = new StateProvince(57) { Name = "Bomet", Country = new Countries().FirstOrDefault(x => x.Id == 110) };

        public StateProvinces() : base(new[] { NotSpecified, EasternCape, FreeState, Gauteng, KwaZuluNatal, Limpopo, Mpumalanga, NorthernCape, NorthWest, WesternCape, Lamu, Embu, Marsabit, Kisii, Kericho, Nakuru, Samburu, Baringo, Garissa, WestPokot, Machakos, Muranga, Bungoma, Wajir, Kirinyaga, Turkana, Makueni, UasinGishu, Kajiado, Isiolo, Vihiga, Elgeyomarakwet, Kisumu, HomaBay, Kiambu, Nyamira, Kitui, Nyeri, Kakamega, Nairobi, Nyandarua, Kwale, Migori, TransNzoia, Busia, TaitaTaveta, Siaya, Narok, Meru, Mandera, Kilifi, TharakaNithi, Nandi, Mombasa, TanaRiver, Laikipia, Bomet }) { }
    }
}