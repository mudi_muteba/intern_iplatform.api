﻿using System;

namespace MasterData
{
    [Serializable]
    public class SecondLevelQuestionGroup : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual int VisibleIndex { get; set; }

        public SecondLevelQuestionGroup() { }

        internal SecondLevelQuestionGroup(int id) { Id = id; }
    }
}