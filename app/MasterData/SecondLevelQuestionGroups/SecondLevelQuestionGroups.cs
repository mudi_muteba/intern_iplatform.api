using System.Collections.ObjectModel;

namespace MasterData
{
    public class SecondLevelQuestionGroups : ReadOnlyCollection<SecondLevelQuestionGroup>, iMasterDataCollection
    {
        public static SecondLevelQuestionGroup AdministrationandJudgements = new SecondLevelQuestionGroup(1) { Name = "Administration and Judgements", VisibleIndex = 0 };
        public static SecondLevelQuestionGroup Vehicle = new SecondLevelQuestionGroup(2) { Name = "Vehicle", VisibleIndex = 1 };
        public static SecondLevelQuestionGroup AllRisk = new SecondLevelQuestionGroup(3) { Name = "All Risk", VisibleIndex = 2 };

        public SecondLevelQuestionGroups() : base(new[] { AdministrationandJudgements, Vehicle, AllRisk }) { }
    }
}