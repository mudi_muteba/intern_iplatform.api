namespace MasterData
{
    public class FinanceHouse : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public FinanceHouse() { }

        internal FinanceHouse(int id) { Id = id; }
    }
}