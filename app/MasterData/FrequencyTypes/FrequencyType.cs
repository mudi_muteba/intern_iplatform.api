﻿using System;

namespace MasterData
{
    [Serializable]
    public class FrequencyType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public FrequencyType() { }

        internal FrequencyType(int id) { Id = id; }
    }
}