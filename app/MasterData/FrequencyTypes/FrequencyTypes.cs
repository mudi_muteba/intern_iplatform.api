﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class FrequencyTypes : ReadOnlyCollection<FrequencyType>, iMasterDataCollection
    {
        public static FrequencyType Daily = new FrequencyType(1) { Name = "Daily" };
        public static FrequencyType Weekly = new FrequencyType(2) { Name = "Weekly" };
        public static FrequencyType Monthly = new FrequencyType(3) { Name = "Monthly" };

        public FrequencyTypes() : base(new[] { Daily, Weekly, Monthly }) { }
    }
}