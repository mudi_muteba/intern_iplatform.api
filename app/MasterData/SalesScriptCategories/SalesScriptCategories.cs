﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class SalesScriptCategories : ReadOnlyCollection<SalesScriptCategory>, iMasterDataCollection
    {
        public static SalesScriptCategory Introduction = new SalesScriptCategory(1) { Name = "Introduction" };
        public static SalesScriptCategory DiscoveryOfNeeds = new SalesScriptCategory(2) { Name = "Discovery of needs" };
        public static SalesScriptCategory GeneralInformation = new SalesScriptCategory(3) { Name = "General information" };
        public static SalesScriptCategory Presentation = new SalesScriptCategory(4) { Name = "Presentation" };
        public static SalesScriptCategory UWQuestions = new SalesScriptCategory(5) { Name = "UW Questions" };
        public static SalesScriptCategory VariousOptions = new SalesScriptCategory (6) { Name = "Various options" };
        public static SalesScriptCategory Disclosures = new SalesScriptCategory(7) { Name = "Disclosures" };
        public static SalesScriptCategory Exclusions = new SalesScriptCategory(8) { Name = "Exclusions" };
        public static SalesScriptCategory BankDetails = new SalesScriptCategory(9) { Name = "Bank details" };
        public static SalesScriptCategory Finalisations = new SalesScriptCategory(10) { Name = "Finalisations" };
        public static SalesScriptCategory CoverOptions = new SalesScriptCategory(11) { Name = "Cover options" };
        public static SalesScriptCategory Sasria = new SalesScriptCategory(12) { Name = "SASRIA" };
        public static SalesScriptCategory CashBackBonus = new SalesScriptCategory(13) { Name = "Cash back bonus" };
        public static SalesScriptCategory QraterDecline = new SalesScriptCategory(14) { Name = "Qrater decline" };
        public static SalesScriptCategory ApprovedQuote = new SalesScriptCategory(15) { Name = "Approved quote" };
        public static SalesScriptCategory TrackingDevice = new SalesScriptCategory(16) { Name = "Tracking device" };
        public static SalesScriptCategory WrapupQuote = new SalesScriptCategory(17) { Name = "Wrap-up quote" };
        public static SalesScriptCategory Vaps = new SalesScriptCategory(18) { Name = "VAPs" };
        public static SalesScriptCategory DoesProRataPremiumApply = new SalesScriptCategory(19) { Name = "Does Pro Rata Premium apply?" };
        public static SalesScriptCategory VehicleInspection = new SalesScriptCategory(20) { Name = "Vehicle inspection" };
        public static SalesScriptCategory Quote = new SalesScriptCategory(21) { Name = "Quote" };
        public SalesScriptCategories() : base(new[] { Introduction, DiscoveryOfNeeds, GeneralInformation, Presentation, UWQuestions, VariousOptions, Disclosures, Exclusions, BankDetails, Finalisations, CoverOptions, Sasria, CashBackBonus, QraterDecline, ApprovedQuote, TrackingDevice, WrapupQuote, Vaps, DoesProRataPremiumApply, VehicleInspection, Quote }) { }
    }
}
