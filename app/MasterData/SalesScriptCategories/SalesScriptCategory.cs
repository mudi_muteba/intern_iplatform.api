﻿using System;

namespace MasterData
{
    [Serializable]
    public class SalesScriptCategory : BaseEntity, iMasterData
    {
        public virtual int Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        public virtual string Name { get; set; }

        public SalesScriptCategory() { }

        internal SalesScriptCategory(int id) { Id = id; }
    }
}
