using System;

namespace MasterData
{
    [Serializable]
    public class ClaimsQuestionDefinition : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual int ClaimsTypeId { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual string ToolTip { get; set; }
        public virtual string DefaultValue { get; set; }
        public virtual string RegexPattern { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual bool? Required { get; set; }
        public virtual bool? FirstPhase { get; set; }
        public virtual bool? SecondPhase { get; set; }
        public virtual ClaimsQuestion ClaimsQuestion { get; set; }
        public virtual ClaimsQuestionGroup ClaimsQuestionGroup { get; set; }
        public ClaimsQuestionDefinition() { }

        internal ClaimsQuestionDefinition(int id) { Id = id; }
    }
}