﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MasterData
{
    public class LossHistoryQuestionGroups : ReadOnlyCollection<LossHistoryQuestionGroup>, iMasterDataCollection
    {
        public static LossHistoryQuestionGroup PreviousInsuranceGroup = new LossHistoryQuestionGroup(1) {Name = "Previous Insurance"};
        public static LossHistoryQuestionGroup BuildingClaimGroup = new LossHistoryQuestionGroup(2) { Name = "Building Claim" };
        public static LossHistoryQuestionGroup MotorClaimGroup = new LossHistoryQuestionGroup(3) { Name = "Motor Claim" };

        public LossHistoryQuestionGroups() : base(new[] { PreviousInsuranceGroup, BuildingClaimGroup, MotorClaimGroup }) { }
    }
}
