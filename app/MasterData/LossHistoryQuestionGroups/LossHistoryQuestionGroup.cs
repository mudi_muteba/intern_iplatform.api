﻿namespace MasterData
{
    public class LossHistoryQuestionGroup : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public LossHistoryQuestionGroup() { }

        internal LossHistoryQuestionGroup(int id) { Id = id; }
    }
}
