using System;

namespace MasterData
{
    [Serializable]
    public class ClaimsQuestionAnswer : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual ClaimsQuestion ClaimsQuestion { get; set; }

        public ClaimsQuestionAnswer() { }

        internal ClaimsQuestionAnswer(int id) { Id = id; }
    }
}