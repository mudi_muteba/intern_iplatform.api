﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class Languages : ReadOnlyCollection<Language>, iMasterDataCollection
    {
        public static Language Afrikaans = new Language(1) { Name = "Afrikaans", Code = "afr", ISO = "af-ZA" };
        public static Language English = new Language(2) { Name = "English", Code = "eng", ISO = "en-US" };
        public static Language SouthernNdebele = new Language(3) { Name = "Southern Ndebele", Code = "nbl", ISO = "nr" };
        public static Language NorthernSotho = new Language(4) { Name = "Northern Sotho", Code = "nso", ISO = "nso" };
        public static Language Sotho = new Language(5) { Name = "Sotho", Code = "sot", ISO = "st" };
        public static Language Swazi = new Language(6) { Name = "Swazi", Code = "ssw", ISO = "ss" };
        public static Language Tsonga = new Language(7) { Name = "Tsonga", Code = "tso", ISO = "ts" };
        public static Language Tswana = new Language(8) { Name = "Tswana", Code = "tsn", ISO = "tn-ZA" };
        public static Language Venda = new Language(9) { Name = "Venda", Code = "ven", ISO = "ve" };
        public static Language Xhosa = new Language(10) { Name = "Xhosa", Code = "xho", ISO = "xh" };
        public static Language Zulu = new Language(11) { Name = "Zulu", Code = "zul", ISO = "zu" };

        public Languages() : base(new[] { Afrikaans, English, SouthernNdebele, NorthernSotho, Sotho, Swazi, Tsonga, Tswana, Venda, Xhosa, Zulu }) { }
    }
}