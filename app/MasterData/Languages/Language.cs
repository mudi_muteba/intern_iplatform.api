﻿using System;

namespace MasterData
{
    [Serializable]
    public class Language : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string ISO { get; set; }

        public Language() { }

        internal Language(int id) { Id = id; }
    }
}