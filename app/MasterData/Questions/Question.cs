using System;

namespace MasterData
{
    [Serializable]
    public class Question : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual QuestionGroup QuestionGroup { get; set; }
        public virtual QuestionType QuestionType { get; set; }

        public Question() { }

        internal Question(int id) { Id = id; }
        protected bool Equals(Question other)
        {
            return Id == other.Id && string.Equals((string)Name, (string)other.Name) && Equals(QuestionGroup, other.QuestionGroup) && Equals(QuestionType, other.QuestionType);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Question)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Id;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (QuestionGroup != null ? QuestionGroup.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (QuestionType != null ? QuestionType.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}