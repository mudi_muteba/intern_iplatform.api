﻿using System;

namespace MasterData
{
    [Serializable]
    public class VehicleType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual int VisibleIndex { get; set; }

        public VehicleType() { }

        internal VehicleType(int id) { Id = id; }
    }
}