using System.Collections.ObjectModel;

namespace MasterData
{
    public class VehicleTypes : ReadOnlyCollection<VehicleType>, iMasterDataCollection
    {
        public static VehicleType Sedan = new VehicleType(1) { Name = "Sedan", Code = "Sedan", VisibleIndex = 0 };
        public static VehicleType Motorcycle = new VehicleType(2) { Name = "Motorcycle", Code = "Motorcycle", VisibleIndex = 1 };

        public VehicleTypes() : base(new[] { Sedan, Motorcycle }) { }
    }
}