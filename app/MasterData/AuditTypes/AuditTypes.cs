using System.Collections.ObjectModel;

namespace MasterData
{
    public class AuditTypes : ReadOnlyCollection<AuditType>, iMasterDataCollection
    {
        public static AuditType Lead = new AuditType(2) { Name = "Lead" };
        public static AuditType Note = new AuditType(1) { Name = "Note" };

        public AuditTypes() : base(new[] { Lead, Note }) { }
    }
}