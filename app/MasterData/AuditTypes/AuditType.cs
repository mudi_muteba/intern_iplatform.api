using System;

namespace MasterData
{
    [Serializable]
    public class AuditType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public AuditType() { }

        internal AuditType(int id) { Id = id; }
    }
}