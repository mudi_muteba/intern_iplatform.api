﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ExportTypes : ReadOnlyCollection<ExportType>, iMasterDataCollection
    {
        public static ExportType CSV = new ExportType(1) { Name = "Text CSV (Comma Seperated Values) Document", Key = "CSV", Extension = ".csv" };
        public static ExportType Excel = new ExportType(2) { Name = "Microsoft Excel Document", Key = "XLS", Extension = ".xls" };
        public static ExportType PDF = new ExportType(3) { Name = "Adobe Acrobat PDF", Key = "PDF", Extension = ".pdf" };

        public ExportTypes() : base(new[] { CSV, Excel, PDF }) { }
    }
}