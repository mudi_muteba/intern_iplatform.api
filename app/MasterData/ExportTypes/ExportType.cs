﻿using System;

namespace MasterData
{
    [Serializable]
    public class ExportType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Key { get; set; }
        public virtual string Extension { get; set; }

        public ExportType() { }

        internal ExportType(int id) { Id = id; }
    }
}