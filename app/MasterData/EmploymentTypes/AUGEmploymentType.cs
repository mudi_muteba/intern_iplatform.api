﻿using System;

namespace MasterData
{
    [Serializable]
    public class AUGEmploymentType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual int VisibleIndex { get; set; }

        public AUGEmploymentType() { }

        internal AUGEmploymentType(int id) { Id = id; }
    }
}