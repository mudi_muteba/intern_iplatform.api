﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class AUGEmploymentTypes : ReadOnlyCollection<AUGEmploymentType>, iMasterDataCollection
    {
        public static AUGEmploymentType Unknown = new AUGEmploymentType(1) { Name = "Unknown", Code = "Unknown", VisibleIndex = 0 };
        public static AUGEmploymentType Employed = new AUGEmploymentType(2) { Name = "Employed", Code = "Employed", VisibleIndex = 1 };
        public static AUGEmploymentType SelfEmployed = new AUGEmploymentType(3) { Name = "Self-Employed", Code = "SelfEmployed", VisibleIndex = 2 };
        public static AUGEmploymentType CivilServant = new AUGEmploymentType(4) { Name = "Civil Servant", Code = "CivilServant", VisibleIndex = 3 };
        public static AUGEmploymentType HouseWifeHusband = new AUGEmploymentType(5) { Name = "House Wife/Husband", Code = "HouseWifeHusband", VisibleIndex = 4 };
        public static AUGEmploymentType FullPartTimeEducation = new AUGEmploymentType(6) { Name = "Full/Part Time Education", Code = "FullPartTimeEducation", VisibleIndex = 5 };
        public static AUGEmploymentType NotEmployedDueToDisabilityIllness = new AUGEmploymentType(7) { Name = "Not Employed Due To Disability/Illness", Code = "NotEmployedDueToDisabilityIllness", VisibleIndex = 6 };
        public static AUGEmploymentType Unemployed = new AUGEmploymentType(8) { Name = "Unemployed", Code = "Unemployed", VisibleIndex = 7 };
        public static AUGEmploymentType Retired = new AUGEmploymentType(9) { Name = "Retired", Code = "Retired", VisibleIndex = 8 };
        public static AUGEmploymentType RetiredNq = new AUGEmploymentType(10) { Name = "Retired Nq", Code = "RetiredNq", VisibleIndex = 9 };

        public AUGEmploymentTypes() : base(new[] { Unknown, Employed, SelfEmployed, CivilServant, HouseWifeHusband, FullPartTimeEducation, NotEmployedDueToDisabilityIllness, Unemployed, Retired, RetiredNq }) { }
    }
}