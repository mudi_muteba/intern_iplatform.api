using System.Collections.ObjectModel;

namespace MasterData
{
    public class RelationshipTypes : ReadOnlyCollection<RelationshipType>, iMasterDataCollection
    {
        public static RelationshipType Branch = new RelationshipType(8) { Name = "Branch" };
        public static RelationshipType ComplianceOfficer = new RelationshipType(7) { Name = "ComplianceOfficer" };
        public static RelationshipType ContactPerson = new RelationshipType(9) { Name = "ContactPerson" };
        public static RelationshipType Department = new RelationshipType(1) { Name = "Department" };
        public static RelationshipType Dependant = new RelationshipType(2) { Name = "Dependant" };
        public static RelationshipType Director = new RelationshipType(3) { Name = "Director" };
        public static RelationshipType Member = new RelationshipType(4) { Name = "Member" };
        public static RelationshipType Owner = new RelationshipType(6) { Name = "Owner" };
        public static RelationshipType Spouse = new RelationshipType(10) { Name = "Spouse" };
        public static RelationshipType Trustee = new RelationshipType(5) { Name = "Trustee" };

        public RelationshipTypes() : base(new[] { Branch, ComplianceOfficer, ContactPerson, Department, Dependant, Director, Member, Owner, Spouse, Trustee }) { }
    }
}