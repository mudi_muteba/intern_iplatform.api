using System;

namespace MasterData
{
    [Serializable]
    public class RelationshipType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public RelationshipType() { }

        internal RelationshipType(int id) { Id = id; }
    }
}