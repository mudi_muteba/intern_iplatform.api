using System.Collections.ObjectModel;
using System.Linq;

namespace MasterData
{
    public class SecondLevelQuestionAnswers : ReadOnlyCollection<SecondLevelQuestionAnswer>, iMasterDataCollection
    {
        public static SecondLevelQuestionAnswer Business = new SecondLevelQuestionAnswer(1) { Name = "Business", Answer = "Business", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 5), VisibleIndex = 0 };
        public static SecondLevelQuestionAnswer Private = new SecondLevelQuestionAnswer(2) { Name = "Private", Answer = "Private", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 5), VisibleIndex = 0 };
        public static SecondLevelQuestionAnswer Yes1 = new SecondLevelQuestionAnswer(4) { Name = "Yes_1", Answer = "Yes", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 1), VisibleIndex = 1 };
        public static SecondLevelQuestionAnswer No1 = new SecondLevelQuestionAnswer(5) { Name = "No_1", Answer = "No", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 1), VisibleIndex = 2 };
        public static SecondLevelQuestionAnswer Yes2 = new SecondLevelQuestionAnswer(7) { Name = "Yes_2", Answer = "Yes", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 2), VisibleIndex = 1 };
        public static SecondLevelQuestionAnswer No2 = new SecondLevelQuestionAnswer(8) { Name = "No_2", Answer = "No", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 2), VisibleIndex = 2 };
        public static SecondLevelQuestionAnswer Yes3 = new SecondLevelQuestionAnswer(10) { Name = "Yes_3", Answer = "Yes", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 3), VisibleIndex = 1 };
        public static SecondLevelQuestionAnswer No3 = new SecondLevelQuestionAnswer(11) { Name = "No_3", Answer = "No", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 3), VisibleIndex = 2 };
        public static SecondLevelQuestionAnswer Yes4 = new SecondLevelQuestionAnswer(13) { Name = "Yes_4", Answer = "Yes", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 4), VisibleIndex = 1 };
        public static SecondLevelQuestionAnswer No4 = new SecondLevelQuestionAnswer(14) { Name = "No_4", Answer = "No", SecondLevelQuestion = new SecondLevelQuestions().FirstOrDefault(x => x.Id == 4), VisibleIndex = 2 };

        public SecondLevelQuestionAnswers() : base(new[] { Business, Private, Yes1, No1, Yes2, No2, Yes3, No3, Yes4, No4 }) { }
    }
}