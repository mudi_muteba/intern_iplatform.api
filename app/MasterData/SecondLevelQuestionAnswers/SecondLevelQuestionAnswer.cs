﻿using System;

namespace MasterData
{
    [Serializable]
    public class SecondLevelQuestionAnswer : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual SecondLevelQuestion SecondLevelQuestion { get; set; }

        public SecondLevelQuestionAnswer() { }

        internal SecondLevelQuestionAnswer(int id) { Id = id; }
    }
}