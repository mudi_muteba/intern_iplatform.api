﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ChartTypes : ReadOnlyCollection<ChartType>, iMasterDataCollection
    {
        //Based on Kendo UI Charts - http://demos.telerik.com/kendo-ui/
        //All chart types use the same generic structure except the types listed in the "Non-Generic Chart Types" region below

        #region Generic Chart Types

        public static ChartType Area = new ChartType(1) { Name = "Area" };
        public static ChartType Bar = new ChartType(2) { Name = "Bar" };
        public static ChartType BoxPlot = new ChartType(3) { Name = "BoxPlot" };
        public static ChartType Bubble = new ChartType(4) { Name = "Bubble" };
        public static ChartType Bullet = new ChartType(5) { Name = "Bullet" };
        public static ChartType Donut = new ChartType(6) { Name = "Donut" };
        public static ChartType Funnel = new ChartType(7) { Name = "Funnel" };
        public static ChartType Line = new ChartType(8) { Name = "Line" };
        public static ChartType Pie = new ChartType(9) { Name = "Pie" };
        public static ChartType Polar = new ChartType(10) { Name = "Polar" };
        public static ChartType Radar = new ChartType(11) { Name = "Radar" };
        public static ChartType Range = new ChartType(12) { Name = "Range" };
        public static ChartType Scatter = new ChartType(13) { Name = "Scatter" };
        public static ChartType Waterfall = new ChartType(17) { Name = "Waterfall" };

        #endregion

        #region Non-Generic Chart Types

        public static ChartType Sparklines = new ChartType(14) { Name = "Sparklines" };
        public static ChartType Stock = new ChartType(15) { Name = "Stock" };
        public static ChartType TreeMap = new ChartType(16) { Name = "TreeMap" };

        #endregion

        public ChartTypes() : base(new[] { Area, Bar, BoxPlot, Bubble, Bullet, Donut, Funnel, Line, Polar, Radar, Range, Scatter, Sparklines, Stock, TreeMap, Waterfall }) { }
    }
}