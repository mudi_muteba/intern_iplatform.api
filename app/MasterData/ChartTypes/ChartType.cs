﻿using System;

namespace MasterData
{
    [Serializable]
    public class ChartType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ChartType() { }

        internal ChartType(int id) { Id = id; }
    }
}