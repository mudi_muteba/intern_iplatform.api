﻿namespace MasterData.Authorisation
{
    public class AuthorisationRight
    {
        public AuthorisationRight(RequiredAuthorisationPoint point, AuthorisationAccess access)
        {
            Point = point;
            Access = access;
        }

        public RequiredAuthorisationPoint Point { get; private set; }
        public AuthorisationAccess Access { get; private set; }
    }
}