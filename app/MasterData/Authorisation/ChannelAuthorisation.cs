﻿using System.Collections.Generic;
using System.Linq;
using Shared.Extentions;

namespace MasterData.Authorisation
{
    public class ChannelAuthorisation
    {
        public int ChannelId { get; private set; }
        public List<RequiredAuthorisationPoint> Points { get; private set; }

        public ChannelAuthorisation(int channelId, params RequiredAuthorisationPoint[] points)
        {
            ChannelId = channelId;
            Points = points == null 
                ? new List<RequiredAuthorisationPoint>()
                : new List<RequiredAuthorisationPoint>(points);
        }

        internal bool Satisfies(List<RequiredAuthorisationPoint> needAuthorisationPoints)
        {
            var satisfied = true;

            foreach (var needPoint in needAuthorisationPoints)
            {
                if (Points.Any(p => p.Satisfies(needPoint)))
                    continue;

                satisfied = false;
                break;
            }

            return satisfied;
        }

        public override string ToString()
        {
            return string.Format("{0}", Points.FlattenListToString(p => p.ToString()));
        }
    }
}