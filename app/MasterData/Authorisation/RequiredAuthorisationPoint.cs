﻿using System.Collections.Generic;
using System.Linq;
using Shared.Extentions;

namespace MasterData.Authorisation
{
    public class RequiredAuthorisationPoint
    {
        public RequiredAuthorisationPoint(string category, string name, params RequiredAuthorisationPoint[] encapulates)
        {
            Category = category;
            Name = name;
            Encapulates = encapulates == null
                ? new List<RequiredAuthorisationPoint>()
                : new List<RequiredAuthorisationPoint>(encapulates);
        }

        public string Category { get; private set; }
        public string Name { get; private set; }
        public List<RequiredAuthorisationPoint> Encapulates { get; set; }

        public string Key
        {
            get { return string.Format("{0}_{1}", Category, Name); }
        }

        public string MessageKey
        {
            get { return string.Format("AUTH_POINT_{0}_{1}_MISSING", Name, Category); }
        }

        public override string ToString()
        {
            var encapsulation = !Encapulates.Any()
                ? string.Empty
                : Encapulates.FlattenListToString(p => p.ToString(), "(", ")");

            return string.Format("{0}.{1} {2}", Category, Name, encapsulation);
        }

        internal bool Satisfies(RequiredAuthorisationPoint needPoint)
        {
            var satisfies = needPoint.Equals(this);

            return satisfies || Encapulates.Any(point => point.Satisfies(needPoint));
        }

        protected bool Equals(RequiredAuthorisationPoint other)
        {
            return string.Equals(Category, other.Category) && string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RequiredAuthorisationPoint) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Category != null ? Category.GetHashCode() : 0)*397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }
    }
}