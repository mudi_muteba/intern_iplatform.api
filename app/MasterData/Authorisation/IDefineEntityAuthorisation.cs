﻿using System.Collections.Generic;

namespace MasterData.Authorisation
{
    public interface IDefineEntityAuthorisation
    {
        List<RequiredAuthorisationPoint> CallCentreAgent { get; }
        List<RequiredAuthorisationPoint> CallCentreManager { get; }
    }
}