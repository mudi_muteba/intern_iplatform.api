﻿namespace MasterData.Authorisation
{
    public enum AuthorisationAccess
    {
        No = 0,
        Yes = 1,
        Maybe = 2
    }
}