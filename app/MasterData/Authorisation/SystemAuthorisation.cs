﻿using System.Collections.Generic;
using System.Linq;
using Shared.Extentions;

namespace MasterData.Authorisation
{
    public class SystemAuthorisation
    {
        public SystemAuthorisation()
        {
            Channels = new List<ChannelAuthorisation>();
        }

        public SystemAuthorisation(params ChannelAuthorisation[] collections)
        {
            Channels = collections == null
                ? new List<ChannelAuthorisation>()
                : new List<ChannelAuthorisation>(collections);
        }

        public List<ChannelAuthorisation> Channels { get; private set; }

        public bool Authorised(int channelId, List<RequiredAuthorisationPoint> needAuthorisationPoints)
        {
            var matchingCollection = GetAuthorisation(channelId);
            if (matchingCollection == null)
                return false;

            return matchingCollection.Satisfies(needAuthorisationPoints);
        }

        public List<RequiredAuthorisationPoint> GetRights(int channelId)
        {
            var authorisation = GetAuthorisation(channelId);

            if (authorisation == null)
                return new List<RequiredAuthorisationPoint>();

            return authorisation.Points;
        }

        private ChannelAuthorisation GetAuthorisation(int channelId)
        {
            return Channels.FirstOrDefault(c => c.ChannelId == channelId);
        }

        public override string ToString()
        {
            return string.Format("\t\t{0}", Channels.FlattenListToString(c => c.ToString()));
        }
    }
}