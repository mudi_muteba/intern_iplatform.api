﻿namespace MasterData.Authorisation
{
    public enum EffectiveAuthorisationReason
    {
        Group = 0,
        User = 1,
    }
}