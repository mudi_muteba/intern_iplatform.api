using System.Collections.Generic;

namespace MasterData.Authorisation.Groups
{
    public interface IAuthorisationGroup
    {
        List<RequiredAuthorisationPoint> Authorisation { get; }
        AuthorisationGroup Group { get; }
    }
}