﻿using System.ComponentModel;

namespace MasterData.Authorisation.Groups
{
    public enum AuthorisationGroupType
    {
        [Description("Admin User")]
        Admin,
        [Description("Call Centre Agent")]
        CallCentreAgent,
        [Description("Call Centre Manager")]
        CallCentreManager,
        [Description("Broker")]
        Broker,
        [Description("Client")]
        Client,
        [Description("Guest")]
        Guest
    }
}