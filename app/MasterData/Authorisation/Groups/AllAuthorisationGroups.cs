﻿using System;
using System.Linq;

namespace MasterData.Authorisation.Groups
{
    public partial class AllAuthorisationGroups
    {
        public IAuthorisationGroup Find(AuthorisationGroup @group)
        {
            var match = groups.FirstOrDefault(g => g.Group.Id == @group.Id);

            if (match == null)
            {
                throw new Exception(
                    string.Format("Could not find corresponding authorisation group definition for {0}/{1}",
                        @group.Id, @group.Name));
            }

            return match;
        }
    }
}