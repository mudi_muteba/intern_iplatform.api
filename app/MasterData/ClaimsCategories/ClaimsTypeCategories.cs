using System.Collections.ObjectModel;
using System.Linq;

namespace MasterData
{
    public class ClaimsTypeCategories : ReadOnlyCollection<ClaimsTypeCategory>, iMasterDataCollection
    {
        public static ClaimsTypeCategory All = new ClaimsTypeCategory(1) { Name = "All" };
        public static ClaimsTypeCategory Motor = new ClaimsTypeCategory(2) { Name = "Motor" };
        public static ClaimsTypeCategory Building = new ClaimsTypeCategory(3) { Name = "Building" };
        public static ClaimsTypeCategory Other = new ClaimsTypeCategory(4) { Name = "Other" };
        public static ClaimsTypeCategory AllRiskSpecified = new ClaimsTypeCategory(5) { Name = "All Risk (Specified)" };
        public static ClaimsTypeCategory AllRiskUnSpecified = new ClaimsTypeCategory(6) { Name = "All Risk (UnSpecified)" };
        public static ClaimsTypeCategory Contents = new ClaimsTypeCategory(7) { Name = "Contents" };
        public static ClaimsTypeCategory PersonalLegalLiability = new ClaimsTypeCategory(8) { Name = "Personal Liability" };

        public ClaimsTypeCategories() : base(new[] { All, Motor, Building, Other, AllRiskSpecified, AllRiskUnSpecified, Contents, PersonalLegalLiability }) { }
    }

}