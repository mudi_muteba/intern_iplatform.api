using System;

namespace MasterData
{
    [Serializable]
    public class ClaimsTypeCategory : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual int VisibleIndex { get; set; }

        public ClaimsTypeCategory() { }

        internal ClaimsTypeCategory(int id) { Id = id; }
    }
}