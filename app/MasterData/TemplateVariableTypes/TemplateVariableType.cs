﻿using System;

namespace MasterData
{
    [Serializable]
    public class TemplateVariableType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Source { get; set; }
        public virtual string Field { get; set; }
        public virtual string Variable { get; set; }

        public TemplateVariableType() { }

        internal TemplateVariableType(int id)
        {
            Id = id;
        }
    }
}