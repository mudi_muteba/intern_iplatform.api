﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class TemplateVariableTypes : ReadOnlyCollection<TemplateVariableType>, iMasterDataCollection
    {
        public static TemplateVariableType ChannelLogo = new TemplateVariableType(1) { Name = "Channel Logo", Source = "", Field = "", Variable = "$data.Data.ChannelLogo" };
        public static TemplateVariableType ChannelCode = new TemplateVariableType(2) { Name = "Channel Code", Source = "", Field = "", Variable = "$data.Data.ChannelCode" };
        public static TemplateVariableType ChannelName = new TemplateVariableType(3) { Name = "Channel Name", Source = "", Field = "", Variable = "$data.Data.ChannelName" };
        public static TemplateVariableType ChannelTradingName = new TemplateVariableType(4) { Name = "Channel Trading Name", Source = "", Field = "", Variable = "$data.Data.ChannelTradingName" };
        public static TemplateVariableType ChannelContactNumber = new TemplateVariableType(5) { Name = "Channel Contact Number", Source = "", Field = "", Variable = "$data.Data.ChannelContactNumber" };
        public static TemplateVariableType ChannelWebsiteAddress = new TemplateVariableType(6) { Name = "Channel Website Address", Source = "", Field = "", Variable = "$data.Data.ChannelWebsiteAddress" };
        public static TemplateVariableType ChannelEmailAddress = new TemplateVariableType(7) { Name = "Channel Email Address", Source = "", Field = "", Variable = "$data.Data.ChannelEmailAddress" };
        public static TemplateVariableType ChannelPhysicalAddress = new TemplateVariableType(8) { Name = "Channel Physical Address", Source = "", Field = "", Variable = "$data.Data.ChannelPhysicalAddress" };
        public static TemplateVariableType ChannelPostalAddress = new TemplateVariableType(9) { Name = "Channel Postal Address", Source = "", Field = "", Variable = "$data.Data.ChannelPostalAddress" };
        public static TemplateVariableType ChannelFspNumber = new TemplateVariableType(10) { Name = "Channel FSP Number", Source = "", Field = "", Variable = "$data.Data.ChannelFspNumber" };
        public static TemplateVariableType ChannelQuoteExpirationPeriod = new TemplateVariableType(11) { Name = "Channel Quote Expiration Period", Source = "", Field = "", Variable = "$data.Data.ChannelQuoteExpirationPeriod" };
        public static TemplateVariableType ClientFirstName = new TemplateVariableType(12) { Name = "Client First Name", Source = "", Field = "", Variable = "$data.Data.ClientFirstName" };
        public static TemplateVariableType ClientLastName= new TemplateVariableType(13) { Name = "Client Surname", Source = "", Field = "", Variable = "$data.Data.ClientLastName" };
        public static TemplateVariableType AgentFirstName = new TemplateVariableType(14) { Name = "Agent First Name", Source = "", Field = "", Variable = "$data.Data.AgentFirstName" };
        public static TemplateVariableType AgentLastName = new TemplateVariableType(15) { Name = "Agent Surname", Source = "", Field = "", Variable = "$data.Data.AgentLastName" };
        public static TemplateVariableType ProductName = new TemplateVariableType(16) { Name = "Product Name", Source = "", Field = "", Variable = "$data.Data.ProductName" };
        public static TemplateVariableType QuoteNumber = new TemplateVariableType(17) { Name = "Quote Number", Source = "", Field = "", Variable = "$data.Data.QuoteNumber" };

        public TemplateVariableTypes() : base(new[] {
            ChannelLogo,
            ChannelCode,
            ChannelName,
            ChannelTradingName,
            ChannelContactNumber,
            ChannelWebsiteAddress,
            ChannelEmailAddress,
            ChannelPhysicalAddress,
            ChannelPostalAddress,
            ChannelFspNumber,
            ChannelQuoteExpirationPeriod,
            ClientFirstName,
            ClientLastName,
            AgentFirstName,
            AgentLastName,
            ProductName,
            QuoteNumber
        }) { }
    }
}