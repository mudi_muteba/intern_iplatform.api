﻿using System;

namespace MasterData
{
    [Serializable]
    public class SalesTagType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public SalesTagType() { }

        internal SalesTagType(int id) { Id = id; }
    }
}
