﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class SalesTagTypes : ReadOnlyCollection<SalesTagType>, iMasterDataCollection
    {
        public static SalesTagType Branch = new SalesTagType(1) { Name = "Branch sales tag" };
        public static SalesTagType Finance = new SalesTagType(2) { Name = "Finance sales tag" };
        public static SalesTagType Group = new SalesTagType(3) { Name = "Group sales tag" };

        public SalesTagTypes() : base(new[] { Branch, Finance, Group }) { }
    }
}
