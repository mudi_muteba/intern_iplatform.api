﻿using System;

namespace MasterData
{
    [Serializable]
    public class PartyType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public PartyType() { }

        internal PartyType(int id) { Id = id; }
    }
}