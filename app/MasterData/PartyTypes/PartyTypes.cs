using System.Collections.ObjectModel;

namespace MasterData
{
    public class PartyTypes : ReadOnlyCollection<PartyType>, iMasterDataCollection
    {
        public static PartyType Individual = new PartyType(1) { Name = "Individual" };
        public static PartyType Organization = new PartyType(2) { Name = "Organization" };
        public static PartyType Contact = new PartyType(3) { Name = "Contact" };
        public static PartyType FNI = new PartyType(4) { Name = "FNI" };
        public PartyTypes() : base(new[] { Contact, Individual, Organization, FNI }) { }
    }
}