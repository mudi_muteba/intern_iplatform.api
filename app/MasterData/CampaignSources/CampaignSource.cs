using System;

namespace MasterData
{
    [Serializable]
    public class CampaignSource : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public CampaignSource() { }

        internal CampaignSource(int id) { Id = id; }
    }
}