using System.Collections.ObjectModel;

namespace MasterData
{
    public class CampaignSources : ReadOnlyCollection<CampaignSource>, iMasterDataCollection
    {
        public static CampaignSource Email = new CampaignSource(1) { Name = "Email" };
        public static CampaignSource Other = new CampaignSource(7) { Name = "Other" };
        public static CampaignSource Phone = new CampaignSource(2) { Name = "Phone" };
        public static CampaignSource Print = new CampaignSource(5) { Name = "Print" };
        public static CampaignSource Radio = new CampaignSource(4) { Name = "Radio" };
        public static CampaignSource SMS = new CampaignSource(3) { Name = "SMS" };
        public static CampaignSource Website = new CampaignSource(6) { Name = "Website" };

        public CampaignSources() : base(new[] { Email, Other, Phone, Print, Radio, SMS, Website }) { }
    }
}