﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class DocumentFieldTypes : ReadOnlyCollection<DocumentFieldType>, iMasterDataCollection
    {
        public static DocumentFieldType Signature = new DocumentFieldType(1)
        {
            Name = "Signature",
        };

        public static DocumentFieldType PlainText = new DocumentFieldType(2)
        {
            Name = "PlainText",
        };

        public static DocumentFieldType PlainTextOptional = new DocumentFieldType(3)
        {
            Name = "PlainTextOptional",
        };

        public DocumentFieldTypes() : base(new[] { Signature, PlainText, PlainTextOptional }) { }
    }
}
