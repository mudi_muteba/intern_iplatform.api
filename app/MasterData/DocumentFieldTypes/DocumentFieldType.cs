﻿using System;

namespace MasterData
{
    [Serializable]
    public class DocumentFieldType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public DocumentFieldType() { }

        internal DocumentFieldType(int id) { Id = id; }
    }
}

