﻿using System.Collections.ObjectModel;
using System.Linq;

namespace MasterData
{
    public class ComponentQuestions : ReadOnlyCollection<ComponentQuestion>, iMasterDataCollection
    {
        #region Loss History Component
        //KINGPRICE
        #region Loss History Claims Component - Previous Insurance 
        public static ComponentQuestion KPCurrentlyInsured = new ComponentQuestion(20)
        {
            Name = "KP-Currently Insured",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion KPUninterruptedPolicy = new ComponentQuestion(21)
        {
            Name = "KP-Uninterrupted Insurance Duration",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion KPInsurerCancel = new ComponentQuestion(22)
        {
            Name = "KP-Previous Insurance Canceled",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion KPPreviousComprehensiveInsurance = new ComponentQuestion(23)
        {
            Name = "KP-Had Comprehensive Insurance",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion KPUninterruptedPolicyNoClaim = new ComponentQuestion(24)
        {
            Name = "KP-Uninterrupted Comprehensive Insurance Duration with no clams",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion KPInterruptedPolicyClaim = new ComponentQuestion(25)
        {
            Name = "KP-Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };

        public static ComponentQuestion KPMissedPremiumLast6Months = new ComponentQuestion(26)
        {
            Name = "KP-Missed Premium Last 6 Months",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };

        #endregion

        #region Loss History Claims Component - Motor
        public static ComponentQuestion KPClaimedForIncident = new ComponentQuestion(1)
        {
            Name = "KP-Claimed For Incident",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 3)
        };
        public static ComponentQuestion KPLossValue = new ComponentQuestion(2)
        {
            Name = "KP-LossValue",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 4),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 3)
        };
        public static ComponentQuestion KPIncidentType = new ComponentQuestion(3)
        {
            Name = "KP-IncidentType",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 3)
        };
        public static ComponentQuestion KPDateOfLoss = new ComponentQuestion(4)
        {
            Name = "KP-DateOfLoss",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 3)
        };
        #endregion
        //KINGPRICE

        //AIG
        #region Loss History Claims Component - Previous Insurance 
        public static ComponentQuestion AIGCurrentlyInsured = new ComponentQuestion(5)
        {
            Name = "AIG-Currently Insured",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion AIGUninterruptedPolicy = new ComponentQuestion(6)
        {
            Name = "AIG-Uninterrupted Insurance Duration",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion AIGInsurerCancel = new ComponentQuestion(7)
        {
            Name = "AIG-Previous Insurance Canceled",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion AIGPreviousComprehensiveInsurance = new ComponentQuestion(8)
        {
            Name = "AIG-Had Comprehensive Insurance",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion AIGUninterruptedPolicyNoClaim = new ComponentQuestion(9)
        {
            Name = "AIG-Uninterrupted Comprehensive Insurance Duration with no clams",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };
        public static ComponentQuestion AIGInterruptedPolicyClaim = new ComponentQuestion(10)
        {
            Name = "AIG-Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 1),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 1)
        };

        #endregion

        #region Loss History Claims Component - Motor
        public static ComponentQuestion AIGMotorTypeOfLoss = new ComponentQuestion(12)
        {
            Name = "AIG-Motor Type of Loss",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 3)
        };

        public static ComponentQuestion AIGMotorDateOfLoss = new ComponentQuestion(13)
        {
            Name = "AIG-Motor Date of Loss",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 3)
        };
        public static ComponentQuestion AIGMotorClaimAmount = new ComponentQuestion(14)
        {
            Name = "AIG-Motor Claim Amount",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 3)
        };
        #endregion

        #region Loss History Claims Component - Building
        public static ComponentQuestion AIGBuildingTypeOfLoss = new ComponentQuestion(15)
        {
            Name = "AIG-Building Type of loss",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 2)
        };

        public static ComponentQuestion AIGBuildingTypeOfLossOther = new ComponentQuestion(16)
        {
            Name = "AIG-Building Type of loss Other",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 4),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 2)
        };

        public static ComponentQuestion AIGBuildingDateOfLoss = new ComponentQuestion(17)
        {
            Name = "AIG-Building Date of Loss",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 2)
        };
        public static ComponentQuestion AIGBuildingClaimAmount = new ComponentQuestion(18)
        {
            Name = "AIG-Building Claim Amount",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 2)
        };
        public static ComponentQuestion AIGBuildingClaimLocation = new ComponentQuestion(19)
        {
            Name = "AIG-Building Claim Location",
            QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3),
            ComponentQuestionGroup = new ComponentQuestionGroups().FirstOrDefault(x => x.Id == 2)
        };
        #endregion
        //AIG

        #endregion
        public ComponentQuestions() : base(new[]
        {
            KPCurrentlyInsured,
            KPUninterruptedPolicy,
            KPInsurerCancel,
            KPPreviousComprehensiveInsurance,
            KPUninterruptedPolicyNoClaim,
            KPInterruptedPolicyClaim,
            KPMissedPremiumLast6Months,

            KPClaimedForIncident,
            KPLossValue,
            KPIncidentType,
            KPDateOfLoss,

            AIGCurrentlyInsured,
            AIGUninterruptedPolicy,
            AIGInsurerCancel,
            AIGPreviousComprehensiveInsurance,
            AIGUninterruptedPolicyNoClaim,
            AIGInterruptedPolicyClaim,

            AIGMotorTypeOfLoss,
            AIGMotorDateOfLoss,
            AIGMotorClaimAmount,

            AIGBuildingTypeOfLoss,
            AIGBuildingTypeOfLossOther,
            AIGBuildingDateOfLoss,
            AIGBuildingClaimAmount,
            AIGBuildingClaimLocation
        })
        { }
    }
}
