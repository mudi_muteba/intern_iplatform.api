﻿using System;

namespace MasterData
{
    [Serializable]
    public class ComponentQuestion : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual ComponentQuestionGroup ComponentQuestionGroup { get; set; }
        public virtual QuestionType QuestionType { get; set; }

        public ComponentQuestion() { }

        internal ComponentQuestion(int id) { Id = id; }
    }
}
