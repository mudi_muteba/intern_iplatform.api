﻿using System;

namespace MasterData
{
    [Serializable]
    public class ComponentAnswer : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual ComponentQuestion Question { get; set; }

        public ComponentAnswer() { }
        internal ComponentAnswer(int id) { Id = id; }
    }
}
