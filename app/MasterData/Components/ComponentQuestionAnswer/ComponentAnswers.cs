﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterData
{
    public class ComponentAnswers : ReadOnlyCollection<ComponentAnswer>, iMasterDataCollection
    {
        #region Loss History Component
        #region King Price

        #region Previous Insurance Claims
        //Will use same as aig and is out of scope for LH release on IPL
        //public static ComponentAnswer KPCurrentlyInsuredNo = new ComponentAnswer(1) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 1).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 1).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 20) };
        //public static ComponentAnswer KPCurrentlyInsuredAutoYes = new ComponentAnswer(2) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 2).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 2).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 20) };
        //public static ComponentAnswer KPCurrentlyInsuredHomeYes = new ComponentAnswer(3) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 3).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 3).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 20) };
        //public static ComponentAnswer KPCurrentlyInsuredAutoAndHome = new ComponentAnswer(4) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 4).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 4).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 20) };

        //public static ComponentAnswer KPUninterruptedPolicy0 = new ComponentAnswer(5) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 1).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 1).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy05A = new ComponentAnswer(6) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 2).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 2).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy10A = new ComponentAnswer(7) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 3).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 3).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy15A = new ComponentAnswer(8) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 4).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 4).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy20A = new ComponentAnswer(9) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 5).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 5).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy25A = new ComponentAnswer(10) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 6).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 6).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy3 = new ComponentAnswer(11) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 7).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 7).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy4 = new ComponentAnswer(12) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 8).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy5 = new ComponentAnswer(13) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 9).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 9).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy6 = new ComponentAnswer(14) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 10).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 10).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy7 = new ComponentAnswer(15) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 11).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 11).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy8 = new ComponentAnswer(16) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 12).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 12).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy9 = new ComponentAnswer(17) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 13).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 13).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy10B = new ComponentAnswer(18) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 14).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 14).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy11 = new ComponentAnswer(19) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 15).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 15).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy12 = new ComponentAnswer(20) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 16).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 16).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy13 = new ComponentAnswer(21) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 17).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 17).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy14 = new ComponentAnswer(22) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 18).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 18).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy15B = new ComponentAnswer(23) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 19).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 19).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy16 = new ComponentAnswer(24) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 20).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 20).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy17 = new ComponentAnswer(25) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 21).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 21).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy18 = new ComponentAnswer(26) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 22).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 22).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy19 = new ComponentAnswer(27) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 23).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 23).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy20B = new ComponentAnswer(28) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 24).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 24).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy21 = new ComponentAnswer(29) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 25).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 25).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy22 = new ComponentAnswer(30) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 26).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 26).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy23 = new ComponentAnswer(31) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 27).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 27).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy24 = new ComponentAnswer(32) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 28).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 28).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy25B = new ComponentAnswer(33) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 29).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 29).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy26 = new ComponentAnswer(34) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 30).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 30).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy27 = new ComponentAnswer(35) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 31).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 31).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy28 = new ComponentAnswer(36) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 32).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 32).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy29 = new ComponentAnswer(37) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 33).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 33).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy30 = new ComponentAnswer(38) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 34).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 34).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy31 = new ComponentAnswer(39) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 35).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 35).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy32 = new ComponentAnswer(40) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 36).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 36).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy33 = new ComponentAnswer(41) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 37).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 37).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy34 = new ComponentAnswer(42) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 38).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 38).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy35 = new ComponentAnswer(43) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 39).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 39).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy36 = new ComponentAnswer(44) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 40).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 40).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy37 = new ComponentAnswer(45) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 41).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 41).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy38 = new ComponentAnswer(46) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 42).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 42).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy39 = new ComponentAnswer(47) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 43).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 43).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };
        //public static ComponentAnswer KPUninterruptedPolicy40C = new ComponentAnswer(48) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 44).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 44).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 21) };

        //public static ComponentAnswer KPInsurerCancelTrue = new ComponentAnswer(49) { Name = "KP-InsurerCancelTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 22) };
        //public static ComponentAnswer KPInsurerCancelFalse = new ComponentAnswer(50) { Name = "KP-InsurerCancelFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 22) };

        //public static ComponentAnswer KPPreviousComprehensiveInsuranceTrue = new ComponentAnswer(51) { Name = "KP-PreviousComprehensiveInsuranceTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 23) };
        //public static ComponentAnswer KPPreviousComprehensiveInsuranceFalse = new ComponentAnswer(52) { Name = "KP-PreviousComprehensiveInsuranceFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 23) };

        //public static ComponentAnswer KPUninterruptedPolicyNoClaimTrue = new ComponentAnswer(53) { Name = "KP-UninterruptedPolicyNoClaimTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 24) };
        //public static ComponentAnswer KPUninterruptedPolicyNoClaimFalse = new ComponentAnswer(54) { Name = "KP-UninterruptedPolicyNoClaimFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 24) };

        //public static ComponentAnswer KPInterruptedPolicyClaimTrue = new ComponentAnswer(55) { Name = "KP-InterruptedPolicyClaimTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 25) };
        //public static ComponentAnswer KPInterruptedPolicyClaimFalse = new ComponentAnswer(56) { Name = "KP-InterruptedPolicyClaimFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 25) };

        //public static ComponentAnswer KPMissedPremiumLast6MonthsTrue = new ComponentAnswer(186) { Name = "KP-MissedPremiumLast6MonthsTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 26) };
        //public static ComponentAnswer KPMissedPremiumLast6MonthsFalse = new ComponentAnswer(187) { Name = "KP-MissedPremiumLast6MonthsFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 26) };

        #endregion

        #region Motor Claims
        public static ComponentAnswer KPClaimedForIncidentTrue = new ComponentAnswer(57) { Name = "KPClaimedForIncidentTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 1) };
        public static ComponentAnswer KPClaimedForIncidentFalse = new ComponentAnswer(58) { Name = "KPClaimedForIncidentFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 1) };

        public static ComponentAnswer KPIncidentTypeMotorAccidentCollisionPothole = new ComponentAnswer(59) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 9).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 9).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorAccidentCollisionAnimal = new ComponentAnswer(60) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 10).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 10).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorAccidentCollisionMultiplevehicle = new ComponentAnswer(61) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 11).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 11).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorAccidentCollisionSinglevehicle = new ComponentAnswer(62) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 12).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 12).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        //public static ComponentAnswer KPIncidentTypeMotorAccidentCollisionpothole = new ComponentAnswer(63) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 13).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 13).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        //public static ComponentAnswer KPIncidentTypeMotorAccidentCollisionanimal = new ComponentAnswer(64) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 14).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 14).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorAccidentCollisionSoundequipment = new ComponentAnswer(65) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 15).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 15).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorAccidentCollisionBursttyre = new ComponentAnswer(66) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 16).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 16).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorAccidentGlassonlyTreefellers = new ComponentAnswer(67) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 17).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 17).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorAccidentGlassonlyVehicle = new ComponentAnswer(68) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 18).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 18).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorFireIntentionalactmemberSinglevehiclenotdrivenstationary = new ComponentAnswer(69) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 19).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 19).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorFireIntentionalactnonmemberDamagestovehicleaccessories = new ComponentAnswer(70) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 20).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 20).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorIntentionalDamageMaliciousdamageIntentionalactmember = new ComponentAnswer(71) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 21).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 21).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorIntentionalDamageIntentionalactmemberSinglevehiclenotdrivenstationary = new ComponentAnswer(72) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 22).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 22).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorStormHailSinglevehicle = new ComponentAnswer(73) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 22).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 22).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorStormHailSinglevehiclenotdrivenstationary = new ComponentAnswer(74) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 23).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 23).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorTheftArmedrobbery = new ComponentAnswer(75) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 24).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 24).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorTheftAccessories = new ComponentAnswer(76) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 25).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 25).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorTheftStolen = new ComponentAnswer(78) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 26).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 26).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorTheftAttemptedTheft = new ComponentAnswer(79) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 27).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 27).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        public static ComponentAnswer KPIncidentTypeMotorTheftAttemptedHijack = new ComponentAnswer(80) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 28).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 28).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 3) };
        #endregion

        #region Home Claims
        //None Specified for KP
        #endregion

        #endregion

        #region AIG
        #region Previous Insurance Claims
        public static ComponentAnswer AIGCurrentlyInsuredNo = new ComponentAnswer(81) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 1).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 1).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 5) };
        public static ComponentAnswer AIGCurrentlyInsuredAutoYes = new ComponentAnswer(82) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 2).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 2).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 5) };
        public static ComponentAnswer AIGCurrentlyInsuredHomeYes = new ComponentAnswer(83) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 3).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 3).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 5) };
        public static ComponentAnswer AIGCurrentlyInsuredAutoAndHome = new ComponentAnswer(84) { Name = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 4).Name, Answer = new CurrentlyInsureds().FirstOrDefault(x => x.Id == 4).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 5) };

        public static ComponentAnswer AIGUninterruptedPolicy0 = new ComponentAnswer(85) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 1).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 1).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy05A = new ComponentAnswer(86) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 2).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 2).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy10A = new ComponentAnswer(87) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 3).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 3).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy15A = new ComponentAnswer(88) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 4).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 4).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy20A = new ComponentAnswer(89) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 5).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 5).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy25A = new ComponentAnswer(90) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 6).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 6).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy3 = new ComponentAnswer(91) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 7).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 7).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy4 = new ComponentAnswer(92) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 8).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy5 = new ComponentAnswer(93) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 9).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 9).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy6 = new ComponentAnswer(94) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 10).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 10).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy7 = new ComponentAnswer(95) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 11).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 11).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy8 = new ComponentAnswer(96) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 12).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 12).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy9 = new ComponentAnswer(97) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 13).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 13).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy10B = new ComponentAnswer(98) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 14).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 14).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy11 = new ComponentAnswer(99) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 15).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 15).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy12 = new ComponentAnswer(100) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 16).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 16).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy13 = new ComponentAnswer(101) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 17).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 17).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy14 = new ComponentAnswer(102) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 18).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 18).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy15B = new ComponentAnswer(103) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 19).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 19).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy16 = new ComponentAnswer(104) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 20).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 20).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy17 = new ComponentAnswer(105) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 21).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 21).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy18 = new ComponentAnswer(106) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 22).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 22).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy19 = new ComponentAnswer(107) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 23).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 23).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy20B = new ComponentAnswer(108) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 24).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 24).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy21 = new ComponentAnswer(109) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 25).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 25).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy22 = new ComponentAnswer(110) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 26).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 26).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy23 = new ComponentAnswer(111) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 27).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 27).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy24 = new ComponentAnswer(112) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 28).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 28).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy25B = new ComponentAnswer(113) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 29).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 29).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy26 = new ComponentAnswer(114) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 30).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 30).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy27 = new ComponentAnswer(115) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 31).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 31).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy28 = new ComponentAnswer(116) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 32).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 32).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy29 = new ComponentAnswer(117) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 33).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 33).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy30 = new ComponentAnswer(118) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 34).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 34).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy31 = new ComponentAnswer(119) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 35).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 35).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy32 = new ComponentAnswer(120) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 36).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 36).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy33 = new ComponentAnswer(121) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 37).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 37).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy34 = new ComponentAnswer(122) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 38).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 38).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy35 = new ComponentAnswer(123) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 39).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 39).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy36 = new ComponentAnswer(124) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 40).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 40).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy37 = new ComponentAnswer(125) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 41).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 41).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy38 = new ComponentAnswer(126) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 42).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 42).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy39 = new ComponentAnswer(127) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 43).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 43).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };
        public static ComponentAnswer AIGUninterruptedPolicy40C = new ComponentAnswer(128) { Name = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 44).Name, Answer = new UninterruptedPolicies().FirstOrDefault(x => x.Id == 44).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 6) };

        public static ComponentAnswer AIGInsurerCancelTrue = new ComponentAnswer(129) { Name = "InsurerCancelTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 7) };
        public static ComponentAnswer AIGInsurerCancelFalse = new ComponentAnswer(130) { Name = "InsurerCancelFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 7) };

        public static ComponentAnswer AIGPreviousComprehensiveInsuranceTrue = new ComponentAnswer(131) { Name = "PreviousComprehensiveInsuranceTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 8) };
        public static ComponentAnswer AIGPreviousComprehensiveInsuranceFalse = new ComponentAnswer(132) { Name = "PreviousComprehensiveInsuranceFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 8) };

        public static ComponentAnswer AIGUninterruptedPolicyNoClaimTrue = new ComponentAnswer(133) { Name = "UninterruptedPolicyNoClaimTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 9) };
        public static ComponentAnswer AIGUninterruptedPolicyNoClaimFalse = new ComponentAnswer(134) { Name = "UninterruptedPolicyNoClaimFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 9) };

        public static ComponentAnswer AIGInterruptedPolicyClaimTrue = new ComponentAnswer(135) { Name = "InterruptedPolicyClaimTrue", Answer = "true", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 10) };
        public static ComponentAnswer AIGInterruptedPolicyClaimFalse = new ComponentAnswer(188) { Name = "InterruptedPolicyClaimFalse", Answer = "false", Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 10) };

        #endregion

        #region Motor Claims
        public static ComponentAnswer AIGMotorTypeOfLossTheft = new ComponentAnswer(136) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 1).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 1).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 12) };
        public static ComponentAnswer AIGMotorTypeOfLossPipeleakage = new ComponentAnswer(137) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 2).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 2).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 12) };
        public static ComponentAnswer AIGMotorTypeOfLossFire = new ComponentAnswer(138) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 3).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 3).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 12) };
        public static ComponentAnswer AIGMotorTypeOfLossStorm = new ComponentAnswer(139) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 4).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 4).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 12) };
        public static ComponentAnswer AIGMotorTypeOfLossGeysers = new ComponentAnswer(140) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 5).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 5).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 12) };
        public static ComponentAnswer AIGMotorTypeOfLossAccidentaldamage = new ComponentAnswer(141) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 6).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 6).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 12) };
        public static ComponentAnswer AIGMotorTypeOfLosssubsidenceandlandslip = new ComponentAnswer(142) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 7).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 7).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 12) };
        public static ComponentAnswer AIGMotorTypeOfOtherifotherspecify = new ComponentAnswer(143) { Name = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 8).Name, Answer = new MotorTypeOfLosses().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 12) };
                      
        public static ComponentAnswer AIGMotorClaimAmountNone = new ComponentAnswer(144) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 1).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 1).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount12500 = new ComponentAnswer(145) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 2).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 2).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount25015000 = new ComponentAnswer(146) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 3).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 3).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount50017500 = new ComponentAnswer(147) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 4).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 4).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount750110000 = new ComponentAnswer(148) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 5).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 5).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount1000115000 = new ComponentAnswer(149) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 6).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 6).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount1500120000 = new ComponentAnswer(150) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 7).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 7).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount2000125000 = new ComponentAnswer(151) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount2500130000 = new ComponentAnswer(152) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 9).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 9).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount3000135000 = new ComponentAnswer(153) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 10).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 10).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount3500140000 = new ComponentAnswer(154) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 11).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 11).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount4000150000 = new ComponentAnswer(155) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 12).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 12).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount50001100000 = new ComponentAnswer(156) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 13).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 13).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount100001200000 = new ComponentAnswer(157) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 14).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 14).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        public static ComponentAnswer AIGMotorClaimAmount200001ormore = new ComponentAnswer(158) { Name = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 15).Name, Answer = new MotorClaimAmounts().FirstOrDefault(x => x.Id == 15).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 14) };
        #endregion

        #region Home Claims
        public static ComponentAnswer AIGBuildingTypeOfLossTheft = new ComponentAnswer(159) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 1).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 1).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 15) };
        public static ComponentAnswer AIGBuildingTypeOfLossPipeleakage = new ComponentAnswer(160) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 2).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 2).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 15) };
        public static ComponentAnswer AIGBuildingTypeOfLossFire = new ComponentAnswer(161) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 3).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 3).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 15) };
        public static ComponentAnswer AIGBuildingTypeOfLossStorm = new ComponentAnswer(162) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 4).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 4).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 15) };
        public static ComponentAnswer AIGBuildingTypeOfLossGeysers = new ComponentAnswer(163) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 5).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 5).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 15) };
        public static ComponentAnswer AIGBuildingTypeOfLossAccidentaldamage = new ComponentAnswer(164) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 6).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 6).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 15) };
        public static ComponentAnswer AIGBuildingTypeOfLosssubsidenceandlandslip = new ComponentAnswer(165) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 7).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 7).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 15) };
        public static ComponentAnswer AIGBuildingTypeOfOtherifotherspecify = new ComponentAnswer(166) { Name = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 15) };

        public static ComponentAnswer AIGBuildingClaimAmountNone = new ComponentAnswer(167) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 1).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 1).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount12500 = new ComponentAnswer(168) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 2).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 2).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount25015000 = new ComponentAnswer(169) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 3).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 3).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount50017500 = new ComponentAnswer(170) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 4).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 4).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount750110000 = new ComponentAnswer(171) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 5).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 5).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount1000115000 = new ComponentAnswer(172) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 6).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 6).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount1500120000 = new ComponentAnswer(173) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 7).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 7).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount2000125000 = new ComponentAnswer(174) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount2500130000 = new ComponentAnswer(175) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount3000135000 = new ComponentAnswer(176) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount3500140000 = new ComponentAnswer(177) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount4000150000 = new ComponentAnswer(178) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount50001100000 = new ComponentAnswer(179) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount100001200000 = new ComponentAnswer(180) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };
        public static ComponentAnswer AIGBuildingClaimAmount200001ormore = new ComponentAnswer(181) { Name = new HomeClaimAmounts().FirstOrDefault(x => x.Id == 8).Name, Answer = new HomeTypeOfLosses().FirstOrDefault(x => x.Id == 8).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 18) };

        public static ComponentAnswer AIGBuildingClaimLocationMainresidence = new ComponentAnswer(182) { Name = new HomeClaimLocations().FirstOrDefault(x => x.Id == 1).Name, Answer = new HomeClaimLocations().FirstOrDefault(x => x.Id == 1).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 19) };
        public static ComponentAnswer AIGBuildingClaimLocationSecondaryresidence = new ComponentAnswer(183) { Name = new HomeClaimLocations().FirstOrDefault(x => x.Id == 2).Name, Answer = new HomeClaimLocations().FirstOrDefault(x => x.Id == 2).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 19) };
        public static ComponentAnswer AIGBuildingClaimLocationPreviousresidence = new ComponentAnswer(184) { Name = new HomeClaimLocations().FirstOrDefault(x => x.Id == 3).Name, Answer = new HomeClaimLocations().FirstOrDefault(x => x.Id == 3).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 19) };
        public static ComponentAnswer AIGBuildingClaimLocationHolidayhome = new ComponentAnswer(185) { Name = new HomeClaimLocations().FirstOrDefault(x => x.Id == 4).Name, Answer = new HomeClaimLocations().FirstOrDefault(x => x.Id == 4).Answer, Question = new ComponentQuestions().FirstOrDefault(x => x.Id == 19) };

        #endregion
        #endregion
        #endregion
        public ComponentAnswers() : base(new[] {
            #region KP Prev Insurance
            //KPCurrentlyInsuredNo
            //,KPCurrentlyInsuredAutoYes
            //,KPCurrentlyInsuredHomeYes
            //,KPCurrentlyInsuredAutoAndHome

            //,KPUninterruptedPolicy0
            //,KPUninterruptedPolicy05A
            //,KPUninterruptedPolicy10A
            //,KPUninterruptedPolicy15A
            //,KPUninterruptedPolicy20A
            //,KPUninterruptedPolicy25A
            //,KPUninterruptedPolicy3
            //,KPUninterruptedPolicy4
            //,KPUninterruptedPolicy5
            //,KPUninterruptedPolicy6
            //,KPUninterruptedPolicy7
            //,KPUninterruptedPolicy8
            //,KPUninterruptedPolicy9
            //,KPUninterruptedPolicy10B
            //,KPUninterruptedPolicy11
            //,KPUninterruptedPolicy12
            //,KPUninterruptedPolicy13
            //,KPUninterruptedPolicy14
            //,KPUninterruptedPolicy15B
            //,KPUninterruptedPolicy16
            //,KPUninterruptedPolicy17
            //,KPUninterruptedPolicy18
            //,KPUninterruptedPolicy19
            //,KPUninterruptedPolicy20B
            //,KPUninterruptedPolicy21
            //,KPUninterruptedPolicy22
            //,KPUninterruptedPolicy23
            //,KPUninterruptedPolicy24
            //,KPUninterruptedPolicy25B
            //,KPUninterruptedPolicy26
            //,KPUninterruptedPolicy27
            //,KPUninterruptedPolicy28
            //,KPUninterruptedPolicy29
            //,KPUninterruptedPolicy30
            //,KPUninterruptedPolicy31
            //,KPUninterruptedPolicy32
            //,KPUninterruptedPolicy33
            //,KPUninterruptedPolicy34
            //,KPUninterruptedPolicy35
            //,KPUninterruptedPolicy36
            //,KPUninterruptedPolicy37
            //,KPUninterruptedPolicy38
            //,KPUninterruptedPolicy39
            //,KPUninterruptedPolicy40C

            //,KPInsurerCancelTrue
            //,KPInsurerCancelFalse

            //,KPPreviousComprehensiveInsuranceTrue
            //,KPPreviousComprehensiveInsuranceFalse

            //,KPUninterruptedPolicyNoClaimTrue
            //,KPUninterruptedPolicyNoClaimFalse

            //,KPInterruptedPolicyClaimTrue
            //,KPInterruptedPolicyClaimFalse

            //,KPMissedPremiumLast6MonthsTrue
            //,KPMissedPremiumLast6MonthsFalse
            #endregion

            #region KP Motor
            KPClaimedForIncidentTrue
            ,KPClaimedForIncidentFalse
            
            ,KPIncidentTypeMotorAccidentCollisionPothole
            ,KPIncidentTypeMotorAccidentCollisionAnimal
            ,KPIncidentTypeMotorAccidentCollisionMultiplevehicle
            ,KPIncidentTypeMotorAccidentCollisionSinglevehicle
            //,KPIncidentTypeMotorAccidentCollisionpothole
            //,KPIncidentTypeMotorAccidentCollisionanimal
            ,KPIncidentTypeMotorAccidentCollisionSoundequipment
            ,KPIncidentTypeMotorAccidentCollisionBursttyre
            ,KPIncidentTypeMotorAccidentGlassonlyTreefellers
            ,KPIncidentTypeMotorAccidentGlassonlyVehicle
            ,KPIncidentTypeMotorFireIntentionalactmemberSinglevehiclenotdrivenstationary
            ,KPIncidentTypeMotorFireIntentionalactnonmemberDamagestovehicleaccessories
            ,KPIncidentTypeMotorIntentionalDamageMaliciousdamageIntentionalactmember
            ,KPIncidentTypeMotorIntentionalDamageIntentionalactmemberSinglevehiclenotdrivenstationary
            ,KPIncidentTypeMotorStormHailSinglevehicle
            ,KPIncidentTypeMotorStormHailSinglevehiclenotdrivenstationary
            ,KPIncidentTypeMotorTheftArmedrobbery
            ,KPIncidentTypeMotorTheftAccessories
            ,KPIncidentTypeMotorTheftStolen
            ,KPIncidentTypeMotorTheftAttemptedTheft
            ,KPIncidentTypeMotorTheftAttemptedHijack
            #endregion

            #region AIG
            ,AIGCurrentlyInsuredNo
            ,AIGCurrentlyInsuredAutoYes
            ,AIGCurrentlyInsuredHomeYes
            ,AIGCurrentlyInsuredAutoAndHome
            ,AIGUninterruptedPolicy0
            ,AIGUninterruptedPolicy05A
            ,AIGUninterruptedPolicy10A
            ,AIGUninterruptedPolicy15A
            ,AIGUninterruptedPolicy20A
            ,AIGUninterruptedPolicy25A
            ,AIGUninterruptedPolicy3
            ,AIGUninterruptedPolicy4
            ,AIGUninterruptedPolicy5
            ,AIGUninterruptedPolicy6
            ,AIGUninterruptedPolicy7
            ,AIGUninterruptedPolicy8
            ,AIGUninterruptedPolicy9
            ,AIGUninterruptedPolicy10B
            ,AIGUninterruptedPolicy11
            ,AIGUninterruptedPolicy12
            ,AIGUninterruptedPolicy13
            ,AIGUninterruptedPolicy14
            ,AIGUninterruptedPolicy15B
            ,AIGUninterruptedPolicy16
            ,AIGUninterruptedPolicy17
            ,AIGUninterruptedPolicy18
            ,AIGUninterruptedPolicy19
            ,AIGUninterruptedPolicy20B
            ,AIGUninterruptedPolicy21
            ,AIGUninterruptedPolicy22
            ,AIGUninterruptedPolicy23
            ,AIGUninterruptedPolicy24
            ,AIGUninterruptedPolicy25B
            ,AIGUninterruptedPolicy26
            ,AIGUninterruptedPolicy27
            ,AIGUninterruptedPolicy28
            ,AIGUninterruptedPolicy29
            ,AIGUninterruptedPolicy30
            ,AIGUninterruptedPolicy31
            ,AIGUninterruptedPolicy32
            ,AIGUninterruptedPolicy33
            ,AIGUninterruptedPolicy34
            ,AIGUninterruptedPolicy35
            ,AIGUninterruptedPolicy36
            ,AIGUninterruptedPolicy37
            ,AIGUninterruptedPolicy38
            ,AIGUninterruptedPolicy39
            ,AIGUninterruptedPolicy40C
            ,AIGInsurerCancelTrue
            ,AIGInsurerCancelFalse
            ,AIGPreviousComprehensiveInsuranceTrue
            ,AIGPreviousComprehensiveInsuranceFalse
            ,AIGUninterruptedPolicyNoClaimTrue
            ,AIGUninterruptedPolicyNoClaimFalse
            ,AIGInterruptedPolicyClaimTrue
            ,AIGInterruptedPolicyClaimFalse
            ,AIGBuildingTypeOfLossTheft
            ,AIGBuildingTypeOfLossPipeleakage
            ,AIGBuildingTypeOfLossFire
            ,AIGBuildingTypeOfLossStorm
            ,AIGBuildingTypeOfLossGeysers
            ,AIGBuildingTypeOfLossAccidentaldamage
            ,AIGBuildingTypeOfLosssubsidenceandlandslip
            ,AIGBuildingTypeOfOtherifotherspecify
            ,AIGBuildingClaimAmountNone
            ,AIGBuildingClaimAmount12500
            ,AIGBuildingClaimAmount25015000
            ,AIGBuildingClaimAmount50017500
            ,AIGBuildingClaimAmount750110000
            ,AIGBuildingClaimAmount1000115000
            ,AIGBuildingClaimAmount1500120000
            ,AIGBuildingClaimAmount2000125000
            ,AIGBuildingClaimAmount2500130000
            ,AIGBuildingClaimAmount3000135000
            ,AIGBuildingClaimAmount3500140000
            ,AIGBuildingClaimAmount4000150000
            ,AIGBuildingClaimAmount50001100000
            ,AIGBuildingClaimAmount100001200000
            ,AIGBuildingClaimAmount200001ormore
            ,AIGBuildingClaimLocationMainresidence
            ,AIGBuildingClaimLocationSecondaryresidence
            ,AIGBuildingClaimLocationPreviousresidence
            ,AIGBuildingClaimLocationHolidayhome
            ,AIGMotorTypeOfLossTheft
            ,AIGMotorTypeOfLossPipeleakage
            ,AIGMotorTypeOfLossFire
            ,AIGMotorTypeOfLossStorm
            ,AIGMotorTypeOfLossGeysers
            ,AIGMotorTypeOfLossAccidentaldamage
            ,AIGMotorTypeOfLosssubsidenceandlandslip
            ,AIGMotorTypeOfOtherifotherspecify
            ,AIGMotorClaimAmountNone
            ,AIGMotorClaimAmount12500
            ,AIGMotorClaimAmount25015000
            ,AIGMotorClaimAmount50017500
            ,AIGMotorClaimAmount750110000
            ,AIGMotorClaimAmount1000115000
            ,AIGMotorClaimAmount1500120000
            ,AIGMotorClaimAmount2000125000
            ,AIGMotorClaimAmount2500130000
            ,AIGMotorClaimAmount3000135000
            ,AIGMotorClaimAmount3500140000
            ,AIGMotorClaimAmount4000150000
            ,AIGMotorClaimAmount50001100000
            ,AIGMotorClaimAmount100001200000
            ,AIGMotorClaimAmount200001ormore
            #endregion
        })
        { }
    }
}
