﻿using System.Collections.ObjectModel;
using System.Linq;

namespace MasterData
{
    public class ComponentQuestionGroups : ReadOnlyCollection<ComponentQuestionGroup>, iMasterDataCollection
    {
        public static ComponentQuestionGroup PreviousInsuranceGroup = new ComponentQuestionGroup(1) { Name = "Previous Insurance", VisibleIndex = 0, ComponentType = new ComponentTypes().FirstOrDefault(x => x.Id == 1) };
        public static ComponentQuestionGroup BuildingClaimGroup = new ComponentQuestionGroup(2) { Name = "Building Claim", VisibleIndex = 100, ComponentType = new ComponentTypes().FirstOrDefault(x => x.Id == 2) };
        public static ComponentQuestionGroup MotorClaimGroup = new ComponentQuestionGroup(3) { Name = "Motor Claim", VisibleIndex = 200, ComponentType = new ComponentTypes().FirstOrDefault(x => x.Id == 2) };

        public ComponentQuestionGroups() : base(new[] { PreviousInsuranceGroup, BuildingClaimGroup, MotorClaimGroup }) { }
    }
}
