﻿using System;

namespace MasterData
{
    [Serializable]
    public class ComponentQuestionGroup : BaseEntity, iMasterData
    {
        public virtual ComponentType ComponentType { get; set; }

        public virtual int VisibleIndex { get; set; }

        public virtual string Name { get; set; }

        public ComponentQuestionGroup() { }
        public ComponentQuestionGroup(int id) { Id = id; }
    }
}
