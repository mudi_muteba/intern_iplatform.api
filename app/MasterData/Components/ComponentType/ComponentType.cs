﻿
using System;

namespace MasterData
{
    [Serializable]
    public class ComponentType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ComponentType() { }
        internal ComponentType(int id) { Id = id; }
    }
}
