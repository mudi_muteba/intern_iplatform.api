﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterData
{
    public class ComponentTypes : ReadOnlyCollection<ComponentType>, iMasterDataCollection
    {
        #region LossHistoryComponents
        public static ComponentType LossHistoryPreviousInsuranceInfoComponent = new ComponentType(1) { Name = "LossHistoryPreviousInsuranceInfoComponent" };
        public static ComponentType LossHistoryClaimsComponent = new ComponentType(2) { Name = "LossHistoryClaimsComponent" };
        #endregion

        public ComponentTypes() : base(new[] {
            LossHistoryPreviousInsuranceInfoComponent
            ,LossHistoryClaimsComponent
        })
        { }
    }
}
