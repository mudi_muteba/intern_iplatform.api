﻿using System;

namespace MasterData
{
    [Serializable]
    public class HomeClaimAmount : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public HomeClaimAmount() { }

        internal HomeClaimAmount(int id) { Id = id; }
    }
}