﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class HomeClaimAmounts : ReadOnlyCollection<HomeClaimAmount>, iMasterDataCollection
    {
        public static HomeClaimAmount HomeClaimAmountNone = new HomeClaimAmount(1) { Name = "HomeClaimAmount None", Answer = "None", VisibleIndex = 0 };
        public static HomeClaimAmount HomeClaimAmount12500 = new HomeClaimAmount(2) { Name = "HomeClaimAmount 1 - 2500", Answer = "1 - 2500", VisibleIndex = 1 };
        public static HomeClaimAmount HomeClaimAmount25015000 = new HomeClaimAmount(3) { Name = "HomeClaimAmount 2501 - 5000", Answer = "2501 - 5000", VisibleIndex = 2 };
        public static HomeClaimAmount HomeClaimAmount50017500 = new HomeClaimAmount(4) { Name = "HomeClaimAmount 5001 - 7500", Answer = "5001 - 7500", VisibleIndex = 3 };
        public static HomeClaimAmount HomeClaimAmount750110000 = new HomeClaimAmount(5) { Name = "HomeClaimAmount 7501 - 10000", Answer = "7501 - 10000", VisibleIndex = 4 };
        public static HomeClaimAmount HomeClaimAmount1000115000 = new HomeClaimAmount(6) { Name = "HomeClaimAmount 10001 - 15000", Answer = "10001 - 15000", VisibleIndex = 5 };
        public static HomeClaimAmount HomeClaimAmount1500120000 = new HomeClaimAmount(7) { Name = "HomeClaimAmount 15001 - 20000", Answer = "15001 - 20000", VisibleIndex = 6 };
        public static HomeClaimAmount HomeClaimAmount2000125000 = new HomeClaimAmount(8) { Name = "HomeClaimAmount 20001 - 25000", Answer = "20001 - 25000", VisibleIndex = 7 };
        public static HomeClaimAmount HomeClaimAmount2500130000 = new HomeClaimAmount(9) { Name = "HomeClaimAmount 25001 - 30000", Answer = "25001 - 30000", VisibleIndex = 8 };
        public static HomeClaimAmount HomeClaimAmount3000135000 = new HomeClaimAmount(10) { Name = "HomeClaimAmount 30001 - 35000", Answer = "30001 - 35000", VisibleIndex = 9 };
        public static HomeClaimAmount HomeClaimAmount3500140000 = new HomeClaimAmount(11) { Name = "HomeClaimAmount 35001 - 40000", Answer = "35001 - 40000", VisibleIndex = 10 };
        public static HomeClaimAmount HomeClaimAmount4000150000 = new HomeClaimAmount(12) { Name = "HomeClaimAmount 40001 - 50000", Answer = "40001 - 50000", VisibleIndex = 11 };
        public static HomeClaimAmount HomeClaimAmount50001100000 = new HomeClaimAmount(13) { Name = "HomeClaimAmount 50001 - 100000", Answer = "50001 - 100000", VisibleIndex = 12 };
        public static HomeClaimAmount HomeClaimAmount100001200000 = new HomeClaimAmount(14) { Name = "HomeClaimAmount 100001 - 200000", Answer = "100001 - 200000", VisibleIndex = 13 };
        public static HomeClaimAmount HomeClaimAmount200001ormore = new HomeClaimAmount(15) { Name = "HomeClaimAmount 200001 or more", Answer = "200001 or more", VisibleIndex = 14 };

        public HomeClaimAmounts() : base(new[] { HomeClaimAmountNone, HomeClaimAmount12500, HomeClaimAmount25015000, HomeClaimAmount50017500, HomeClaimAmount750110000, HomeClaimAmount1000115000, HomeClaimAmount1500120000, HomeClaimAmount2000125000, HomeClaimAmount2500130000, HomeClaimAmount3000135000, HomeClaimAmount3500140000, HomeClaimAmount4000150000, HomeClaimAmount50001100000, HomeClaimAmount100001200000, HomeClaimAmount200001ormore }) { }
    }
}