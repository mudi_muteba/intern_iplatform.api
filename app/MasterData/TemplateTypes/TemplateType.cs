﻿using System;

namespace MasterData
{
    [Serializable]
    public class TemplateType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public TemplateType() { }

        internal TemplateType(int id)
        {
            Id = id;
        }
    }
}