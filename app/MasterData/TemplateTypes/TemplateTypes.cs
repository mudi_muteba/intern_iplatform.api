﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class TemplateTypes : ReadOnlyCollection<TemplateType>, iMasterDataCollection
    {
        public static TemplateType CssTemplate = new TemplateType(1) { Name = "CSS Template" };
        public static TemplateType FileTemplate = new TemplateType(2) { Name = "File Template" };
        public static TemplateType HtmlTemplate = new TemplateType(3) { Name = "HTML Template" };
        public static TemplateType PlainTextTemplate = new TemplateType(4) { Name = "Plain Text Template" };

        public TemplateTypes() : base(new[] {
            CssTemplate,
            FileTemplate,
            HtmlTemplate,
            PlainTextTemplate
        }) { }
    }
}