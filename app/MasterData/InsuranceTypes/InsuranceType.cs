﻿using System;

namespace MasterData
{
    [Serializable]
    public class InsuranceType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public InsuranceType() { }

        internal InsuranceType(int id) { Id = id; }
    }
}