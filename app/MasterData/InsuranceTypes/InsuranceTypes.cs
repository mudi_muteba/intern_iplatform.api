﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class InsuranceTypes : ReadOnlyCollection<InsuranceType>, iMasterDataCollection
    {
        /* Echo TCF Insurance Types
            1 Personal Lines
            2 Commercial
            3 Body Corporate
            4 Agriculture
            5 Engineering
            6 Aviation
            7 Marine
            8 Liability
         */
        public static InsuranceType PersonalLines = new InsuranceType(1) { Name = "Personal Lines" };
        public static InsuranceType Commercial = new InsuranceType(2) { Name = "Commercial" };
        public static InsuranceType BodyCorporate = new InsuranceType(3) { Name = "Body Corporate" };
        public static InsuranceType Agriculture = new InsuranceType(4) { Name = "Agriculture" };
        public static InsuranceType Engineering = new InsuranceType(5) { Name = "Engineering" };
        public static InsuranceType Aviation = new InsuranceType(6) { Name = "Aviation" };
        public static InsuranceType Marine = new InsuranceType(7) { Name = "Marine" };
        public static InsuranceType Liability = new InsuranceType(8) { Name = "Liability" };

        public InsuranceTypes() : base(new[] { PersonalLines, Commercial, BodyCorporate, Agriculture, Engineering, Aviation, Marine, Liability }) { }
    }
}