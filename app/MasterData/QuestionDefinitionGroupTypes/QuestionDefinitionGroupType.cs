﻿using System;

namespace MasterData
{
    [Serializable]
    public class QuestionDefinitionGroupType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public QuestionDefinitionGroupType() { }

        internal QuestionDefinitionGroupType(int id) { Id = id; }
    }
}