using System.Collections.ObjectModel;

namespace MasterData
{
    public class QuestionDefinitionGroupTypes : ReadOnlyCollection<QuestionDefinitionGroupType>, iMasterDataCollection
    {
        public static QuestionDefinitionGroupType Overnightaddress = new QuestionDefinitionGroupType(1) { Name = "Overnight address" };
        public static QuestionDefinitionGroupType Homeaddress = new QuestionDefinitionGroupType(2) { Name = "Home address" };
        public static QuestionDefinitionGroupType Workaddress = new QuestionDefinitionGroupType(3) { Name = "Work address" };
        public static QuestionDefinitionGroupType Vehicleasset = new QuestionDefinitionGroupType(4) { Name = "Vehicle asset" };
        public static QuestionDefinitionGroupType DriverInformation = new QuestionDefinitionGroupType(5) { Name = "Driver Information" };
        public static QuestionDefinitionGroupType AllRiskAsset = new QuestionDefinitionGroupType(6) { Name = "All Risk Asset" };

        public QuestionDefinitionGroupTypes() : base(new[] { Overnightaddress, Homeaddress, Workaddress, Vehicleasset, DriverInformation, AllRiskAsset }) { }
    }
}