using System.Collections.ObjectModel;

namespace MasterData
{
    public class RiskItemTypes : ReadOnlyCollection<RiskItemType>, iMasterDataCollection
    {
        public static RiskItemType Motor = new RiskItemType(1) { Name = "Motor", Code = "MOTOR" };
        public static RiskItemType AllRisks = new RiskItemType(2) { Name = "All Risks", Code = "ALLRISKS" };
        public static RiskItemType Buildings = new RiskItemType(3) { Name = "Buildings", Code = "BUILDINGS" };
        public static RiskItemType Content = new RiskItemType(4) { Name = "Content", Code = "CONTENT" };
        public static RiskItemType PersonalLiability = new RiskItemType(5) { Name = "Personal Liability", Code = "PERSONALLIABILITY" };

        public RiskItemTypes() : base(new[] { Motor, AllRisks, Buildings, Content, PersonalLiability }) { }
    }
}