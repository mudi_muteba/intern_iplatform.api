using System;

namespace MasterData
{
    [Serializable]
    public class RiskItemType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }

        public RiskItemType() { }

        internal RiskItemType(int id) { Id = id; }
    }
}