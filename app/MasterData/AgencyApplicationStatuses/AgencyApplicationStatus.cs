﻿namespace MasterData
{
    public class AgencyApplicationStatus : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public AgencyApplicationStatus() { }

        internal AgencyApplicationStatus(int id) { Id = id; }
    }
}

