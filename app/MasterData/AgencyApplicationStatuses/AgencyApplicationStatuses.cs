﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class AgencyApplicationStatuses : ReadOnlyCollection<AgencyApplicationStatus>, iMasterDataCollection
    {
        public static AgencyApplicationStatus NotSend = new AgencyApplicationStatus(1) { Name = "NotSend" };
        public static AgencyApplicationStatus Pending = new AgencyApplicationStatus(2) { Name = "Pending" };
        public static AgencyApplicationStatus Approved = new AgencyApplicationStatus(3) { Name = "Approved" };

        public AgencyApplicationStatuses() : base(new[] { NotSend, Pending, Approved }) { }
    }
}

