using System.Collections.ObjectModel;

namespace MasterData
{
    public class BankAccountTypes : ReadOnlyCollection<BankAccountType>, iMasterDataCollection
    {
        public static BankAccountType Current = new BankAccountType(1) { Name = "Current" };
        public static BankAccountType Savings = new BankAccountType(2) { Name = "Savings" };
        public static BankAccountType Transmission = new BankAccountType(3) { Name = "Transmission" };
        public static BankAccountType Bond = new BankAccountType(4) { Name = "Bond" };
        public static BankAccountType Subscription = new BankAccountType(5) { Name = "Subscription" };

        public BankAccountTypes() : base(new[] { Current, Savings, Transmission, Bond, Subscription }) { }
    }
}