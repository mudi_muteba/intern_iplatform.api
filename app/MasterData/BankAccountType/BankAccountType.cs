using System;

namespace MasterData
{
    [Serializable]
    public class BankAccountType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public BankAccountType() { }

        internal BankAccountType(int id) { Id = id; }
    }
}