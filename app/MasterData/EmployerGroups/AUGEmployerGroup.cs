﻿using System;

namespace MasterData
{
    [Serializable]
    public class AUGEmployerGroup : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual int VisibleIndex { get; set; }

        public AUGEmployerGroup() { }
        internal AUGEmployerGroup(int id) { Id = id; }
    }
}