﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class AUGEmployerGroups : ReadOnlyCollection<AUGEmployerGroup>, iMasterDataCollection
    {
        public static AUGEmployerGroup Unknown = new AUGEmployerGroup(1) { Name = "Unknown", Code = "Unknown", VisibleIndex = 0 };
        public static AUGEmployerGroup PostOfficeTelekom = new AUGEmployerGroup(2) { Name = "Post Office/Telekom", Code = "PostOfficeTelekom", VisibleIndex = 1 };
        public static AUGEmployerGroup OtherEmployers = new AUGEmployerGroup(3) { Name = "Other Employers", Code = "OtherEmployers", VisibleIndex = 2 };
        public static AUGEmployerGroup Retired = new AUGEmployerGroup(4) { Name = "Retired", Code = "Retired", VisibleIndex = 3 };
        public static AUGEmployerGroup Spoornet = new AUGEmployerGroup(5) { Name = "Spoornet", Code = "Spoornet", VisibleIndex = 4 };
        public static AUGEmployerGroup SAPoliceSAPolisie = new AUGEmployerGroup(6) { Name = "SA Police/SA Polisie", Code = "SAPoliceSAPolisie", VisibleIndex = 5 };
        public static AUGEmployerGroup Eskom = new AUGEmployerGroup(7) { Name = "Eskom", Code = "Eskom", VisibleIndex = 6 };
        public static AUGEmployerGroup EducationInstitute = new AUGEmployerGroup(8) { Name = "EducationInstitute", Code = "EducationInstitute", VisibleIndex = 7 };
        public static AUGEmployerGroup Transnet = new AUGEmployerGroup(9) { Name = "Transnet", Code = "Transnet", VisibleIndex = 8 };
        public static AUGEmployerGroup Postnet = new AUGEmployerGroup(10) { Name = "Postnet", Code = "Postnet", VisibleIndex = 9 };
        public static AUGEmployerGroup Sabc = new AUGEmployerGroup(11) { Name = "Sabc", Code = "Sabc", VisibleIndex = 10 };
        public static AUGEmployerGroup SAA = new AUGEmployerGroup(12) { Name = "S.A.A.", Code = "SAA", VisibleIndex = 11 };
        public static AUGEmployerGroup PublicService = new AUGEmployerGroup(13) { Name = "Public Service", Code = "PublicService", VisibleIndex = 12 };
        public static AUGEmployerGroup Absa = new AUGEmployerGroup(14) { Name = "Absa", Code = "Absa", VisibleIndex = 13 };

        public AUGEmployerGroups() : base(new[] { Unknown, PostOfficeTelekom, OtherEmployers, Retired, Spoornet, SAPoliceSAPolisie, Eskom, EducationInstitute, Transnet, Postnet, Sabc, SAA, PublicService, Absa }) { }
    }
}