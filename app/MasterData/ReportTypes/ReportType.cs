﻿using System;

namespace MasterData
{
    [Serializable]
    public class ReportType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Icon { get; set; }

        public ReportType() { }

        internal ReportType(int id)
        {
            Id = id;
        }
    }
}