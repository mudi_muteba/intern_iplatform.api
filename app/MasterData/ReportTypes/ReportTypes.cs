﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ReportTypes : ReadOnlyCollection<ReportType>, iMasterDataCollection
    {
        public static ReportType Quotation = new ReportType(1) { Name = "Quotation", Icon = "fa fa-money" };
        public static ReportType Report = new ReportType(2) { Name = "Report", Icon = "fa fa-pdf-o" };
        public static ReportType SignFlow = new ReportType(3) { Name = "SignFlow", Icon = "fa fa-question-circle" };

        public ReportTypes() : base(new[] { Quotation, Report, SignFlow }) { }
    }
}