﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class CorrespondenceTypes : ReadOnlyCollection<CorrespondenceType>, iMasterDataCollection
    {
        public static CorrespondenceType PolicyDocuments = new CorrespondenceType(1) { Name = "Policy Documents", Code = "PolicyDocuments", VisibleIndex = 0 };
        public static CorrespondenceType GeneralDocuments = new CorrespondenceType(2) { Name = "General Documents", Code = "GeneralDocuments", VisibleIndex = 1 };
        public static CorrespondenceType MarketingInformation = new CorrespondenceType(3) { Name = "Marketing Information", Code = "MarketingInformation", VisibleIndex = 2 };

        public CorrespondenceTypes() : base(new[] { PolicyDocuments, GeneralDocuments, MarketingInformation }) { }
    }
}