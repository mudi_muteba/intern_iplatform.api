﻿using System;

namespace MasterData
{
    [Serializable]
    public class CorrespondenceType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual int VisibleIndex { get; set; }

        public CorrespondenceType() { }

        internal CorrespondenceType(int id) { Id = id; }
    }
}