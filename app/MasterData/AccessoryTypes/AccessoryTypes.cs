﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class AccessoryTypes : ReadOnlyCollection<AccessoryType>, iMasterDataCollection
    {
        public static AccessoryType AccessoryTypeSoundequipment = new AccessoryType(1) { Name = "AccessoryType Sound equipment", Answer = "Sound equipment", VisibleIndex = 0 };
        public static AccessoryType AccessoryTypeMagWheels = new AccessoryType(2) { Name = "AccessoryType Mag Wheels", Answer = "Mag Wheels", VisibleIndex = 0 };
        public static AccessoryType AccessoryTypeSunRoof = new AccessoryType(3) { Name = "AccessoryType Sun Roof", Answer = "Sun Roof", VisibleIndex = 0 };
        public static AccessoryType AccessoryTypeXenonLights = new AccessoryType(4) { Name = "AccessoryType Xenon Lights", Answer = "Xenon Lights", VisibleIndex = 0 };
        public static AccessoryType AccessoryTypeBullBarsorTowBars = new AccessoryType(5) { Name = "AccessoryType Bull Bars or Tow Bars", Answer = "Bull Bars or Tow Bars", VisibleIndex = 0 };
        public static AccessoryType AccessoryTypeBodyKitSpoilers = new AccessoryType(6) { Name = "AccessoryType Body Kit (Spoilers)", Answer = "Body Kit (Spoilers)", VisibleIndex = 0 };
        public static AccessoryType AccessoryTypeAntiSmashandGrab = new AccessoryType(7) { Name = "AccessoryType Anti-Smash and Grab", Answer = "Anti-Smash and Grab", VisibleIndex = 0 };
        public static AccessoryType AccessoryTypeOther = new AccessoryType(8) { Name = "AccessoryType Other", Answer = "Other", VisibleIndex = 0 };

        public AccessoryTypes() : base(new[] { AccessoryTypeSoundequipment, AccessoryTypeMagWheels, AccessoryTypeSunRoof, AccessoryTypeXenonLights, AccessoryTypeBullBarsorTowBars, AccessoryTypeBodyKitSpoilers, AccessoryTypeAntiSmashandGrab, AccessoryTypeOther }) { }
    }
}