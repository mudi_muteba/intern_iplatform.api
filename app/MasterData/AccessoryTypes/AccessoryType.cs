﻿using System;

namespace MasterData
{
    [Serializable]
    public class AccessoryType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public AccessoryType() { }

        internal AccessoryType(int id) { Id = id; }
    }


}