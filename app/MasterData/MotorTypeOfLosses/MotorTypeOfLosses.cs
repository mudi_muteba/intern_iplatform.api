using System.Collections.ObjectModel;

namespace MasterData
{
    public class MotorTypeOfLosses : ReadOnlyCollection<MotorTypeOfLoss>, iMasterDataCollection
    {
        public static MotorTypeOfLoss MotorTypeOfLossHijack = new MotorTypeOfLoss(1) { Name = "MotorTypeOfLoss Hijack", Answer = "Hijack", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTypeOfLossAccident = new MotorTypeOfLoss(2) { Name = "MotorTypeOfLoss Accident", Answer = "Accident", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTypeOfLossTheft = new MotorTypeOfLoss(3) { Name = "MotorTypeOfLoss Theft", Answer = "Theft", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTypeOfLossWindscreen = new MotorTypeOfLoss(4) { Name = "MotorTypeOfLoss Windscreen", Answer = "Windscreen", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTypeOfLossWeatherandElements = new MotorTypeOfLoss(5) { Name = "MotorTypeOfLoss Weather and Elements", Answer = "Weather and Elements", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTypeOfLossStrikeandRiot = new MotorTypeOfLoss(6) { Name = "MotorTypeOfLoss Strike and Riot", Answer = "Strike and Riot", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTypeOfLossThirdpartyliability = new MotorTypeOfLoss(7) { Name = "MotorTypeOfLoss Third party liability", Answer = "Third party liability", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTypeOfLossFireandExplosion = new MotorTypeOfLoss(8) { Name = "MotorTypeOfLoss Fire and Explosion", Answer = "Fire and Explosion", VisibleIndex = 0 };

        public static MotorTypeOfLoss MotorAccidentCollisionPothole = new MotorTypeOfLoss(9) { Name = "MotorTypeOfLoss Accident -Collision -Pothole", Answer = "Accident -Collision -Pothole", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorAccidentCollisionAnimal = new MotorTypeOfLoss(10) { Name = "MotorTypeOfLoss Accident -Collision -Animal", Answer = "Accident -Collision -Animal", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorAccidentCollisionMultiplevehicle = new MotorTypeOfLoss(11) { Name = "MotorTypeOfLoss Accident -Collision -Multiple vehicle", Answer = "Accident -Collision -Multiple vehicle", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorAccidentCollisionSinglevehicle = new MotorTypeOfLoss(12) { Name = "MotorTypeOfLoss Accident -Collision -Single vehicle", Answer = "Accident -Collision -Single vehicle", VisibleIndex = 0 };
        //public static MotorTypeOfLoss MotorAccidentCollisionpothole = new MotorTypeOfLoss(13) { Name = "MotorTypeOfLoss Accident -Collision -pothole", Answer = "Accident -Collision -pothole", VisibleIndex = 0 };
        //public static MotorTypeOfLoss MotorAccidentCollisionanimal = new MotorTypeOfLoss(14) { Name = "MotorTypeOfLoss Accident -Collision -animal", Answer = "Accident -Collision -animal", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorAccidentCollisionSoundequipment = new MotorTypeOfLoss(15) { Name = "MotorTypeOfLoss Accident -Collision -Sound equipment", Answer = "Accident -Collision -Sound equipment", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorAccidentCollisionBursttyre = new MotorTypeOfLoss(16) { Name = "MotorTypeOfLoss Accident -Collision -Burst tyre", Answer = "Accident -Collision -Burst tyre", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorAccidentGlassonlyTreefellers = new MotorTypeOfLoss(17) { Name = "MotorTypeOfLoss Accident -Glass only -Tree fellers", Answer = "Accident -Glass only -Tree fellers", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorAccidentGlassonlyVehicle = new MotorTypeOfLoss(18) { Name = "MotorTypeOfLoss Accident -Glass only -Vehicle", Answer = "Accident -Glass only -Vehicle", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorFireIntentionalactmemberSinglevehiclenotdrivenstationary = new MotorTypeOfLoss(19) { Name = "MotorTypeOfLoss Fire -Intentional act member -Single vehicle (not driven stationary)", Answer = "Fire -Intentional act member -Single vehicle (not driven stationary)", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorFireIntentionalactnonmemberDamagestovehicleaccessories = new MotorTypeOfLoss(20) { Name = "MotorTypeOfLoss Fire -Intentional act non member -Damages to vehicle accessories", Answer = "Fire -Intentional act non member -Damages to vehicle accessories", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorIntentionalDamageMaliciousdamageIntentionalactmember = new MotorTypeOfLoss(21) { Name = "MotorTypeOfLoss Intentional Damage - Malicious damage - Intentional act member", Answer = "Intentional Damage - Malicious damage - Intentional act member", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorIntentionalDamageIntentionalactmemberSinglevehiclenotdrivenstationary = new MotorTypeOfLoss(22) { Name = "MotorTypeOfLoss Intentional Damage - Intentional act member - Single vehicle (not driven stationary)", Answer = "Intentional Damage - Intentional act member - Single vehicle (not driven stationary)", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorStormHailSinglevehicle = new MotorTypeOfLoss(23) { Name = "MotorTypeOfLoss Storm - Hail - Single vehicle", Answer = "Storm - Hail - Single vehicle", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorStormHailSinglevehiclenotdrivenstationary = new MotorTypeOfLoss(24) { Name = "MotorTypeOfLoss Storm - Hail - Single vehicle (not driven stationary)", Answer = "Storm - Hail - Single vehicle (not driven stationary)", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTheftArmedrobbery = new MotorTypeOfLoss(25) { Name = "MotorTypeOfLoss Theft - Armed robbery", Answer = "Theft - Armed robbery", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTheftAccessories = new MotorTypeOfLoss(26) { Name = "MotorTypeOfLoss Theft - Accessories", Answer = "Theft - Accessories", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTheftStolen = new MotorTypeOfLoss(27) { Name = "MotorTypeOfLoss Theft - Stolen", Answer = "Theft - Stolen", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTheftAttemptedTheft = new MotorTypeOfLoss(28) { Name = "MotorTypeOfLoss Theft - Attempted Theft", Answer = "Theft - Attempted Theft", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTheftAttemptedHijack = new MotorTypeOfLoss(29) { Name = "MotorTypeOfLoss Theft - Attempted Hi-jack", Answer = "Theft - Attempted Hi-jack", VisibleIndex = 0 };
        public static MotorTypeOfLoss MotorTheftHijack = new MotorTypeOfLoss(30) { Name = "MotorTypeOfLoss Theft - Hi-jack", Answer = "Theft - Hi-jack", VisibleIndex = 0 };

        public MotorTypeOfLosses() : base(new[] {
            MotorTypeOfLossHijack,
            MotorTypeOfLossAccident,
            MotorTypeOfLossTheft,
            MotorTypeOfLossWindscreen,
            MotorTypeOfLossWeatherandElements,
            MotorTypeOfLossStrikeandRiot,
            MotorTypeOfLossThirdpartyliability,
            MotorTypeOfLossFireandExplosion,

            MotorAccidentCollisionPothole,
            MotorAccidentCollisionAnimal,
            MotorAccidentCollisionMultiplevehicle,
            MotorAccidentCollisionSinglevehicle,
            //MotorAccidentCollisionpothole,
            //MotorAccidentCollisionanimal,
            MotorAccidentCollisionSoundequipment,
            MotorAccidentCollisionBursttyre,
            MotorAccidentGlassonlyTreefellers,
            MotorAccidentGlassonlyVehicle,
            MotorFireIntentionalactmemberSinglevehiclenotdrivenstationary,
            MotorFireIntentionalactnonmemberDamagestovehicleaccessories,
            MotorIntentionalDamageMaliciousdamageIntentionalactmember,
            MotorIntentionalDamageIntentionalactmemberSinglevehiclenotdrivenstationary,
            MotorStormHailSinglevehicle,
            MotorStormHailSinglevehiclenotdrivenstationary,
            MotorTheftArmedrobbery,
            MotorTheftAccessories,
            MotorTheftStolen,
            MotorTheftAttemptedTheft,
            MotorTheftAttemptedHijack,
            MotorTheftHijack
        })
        { }
    }
}