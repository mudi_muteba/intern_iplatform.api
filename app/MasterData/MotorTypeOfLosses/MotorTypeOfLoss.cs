﻿namespace MasterData
{
    public class MotorTypeOfLoss : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public MotorTypeOfLoss() { }

        internal MotorTypeOfLoss(int id) { Id = id; }
    }
}