using System.Collections.ObjectModel;

namespace MasterData
{
    public class ProductTypes : ReadOnlyCollection<ProductType>, iMasterDataCollection
    {
        public static ProductType Accident = new ProductType(1) { Name = "Accident" };
        public static ProductType CommercialPackage = new ProductType(2) { Name = "Commercial Package" };
        public static ProductType CommercialProperty = new ProductType(3) { Name = "Commercial Property" };
        public static ProductType CommercialVehicle = new ProductType(4) { Name = "Commercial Vehicle" };
        public static ProductType Liability = new ProductType(5) { Name = "Liability" };
        public static ProductType Package = new ProductType(6) { Name = "Package" };
        public static ProductType PersonalPackage = new ProductType(7) { Name = "Personal Package" };
        public static ProductType PersonalProperty = new ProductType(8) { Name = "Personal Property" };
        public static ProductType PersonalVehicle = new ProductType(9) { Name = "Personal Vehicle" };
        public static ProductType SpecifiedBusinessArticles = new ProductType(10) { Name = "Specified Business Articles" };
        public static ProductType SpecifiedPersonalArticles = new ProductType(11) { Name = "Specified Personal Articles" };
        public static ProductType VAP = new ProductType(13) { Name = "VAP" };
        public static ProductType Watercraft = new ProductType(12) { Name = "Watercraft" };

        public ProductTypes() : base(new[] { Accident, CommercialPackage, CommercialProperty, CommercialVehicle, Liability, Package, PersonalPackage, PersonalProperty, PersonalVehicle, SpecifiedBusinessArticles, SpecifiedPersonalArticles, VAP, Watercraft }) { }
    }
}