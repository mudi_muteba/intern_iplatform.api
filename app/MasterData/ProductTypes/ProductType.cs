﻿using System;

namespace MasterData
{
    [Serializable]
    public class ProductType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ProductType() { }

        internal ProductType(int id) { Id = id; }

        protected bool Equals(ProductType other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProductType)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }
    }
}