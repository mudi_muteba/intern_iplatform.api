namespace MasterData
{
    public interface iMasterData
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}