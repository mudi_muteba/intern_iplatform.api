﻿using System;

namespace MasterData
{
    [Serializable]
    public class ClaimsType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ClaimsType() { }

        internal ClaimsType(int id) { Id = id; }
    }
}