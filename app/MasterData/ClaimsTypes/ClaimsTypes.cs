﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ClaimsTypes : ReadOnlyCollection<ClaimsType>, iMasterDataCollection
    {
        public static ClaimsType MotorTheft = new ClaimsType(1) { Name = "Motor Theft" };
        public static ClaimsType MotorAccident = new ClaimsType(2) { Name = "Motor Accident" };
        public static ClaimsType PublicLiability = new ClaimsType(3) { Name = "Public Liability" };
        public static ClaimsType PersonalLiability = new ClaimsType(4) { Name = "Personal Liability" };
        public static ClaimsType PersonalAccident = new ClaimsType(5) { Name = "Personal Accident" };
        public static ClaimsType TheftOfPropertyOutOfMotor = new ClaimsType(6) { Name = "Theft Of Property Out Of Motor" };
        public static ClaimsType NonMotorClaimsUpToR10000 = new ClaimsType(7) { Name = "Non-Motor Claims Up To R10 000" };
        public static ClaimsType PropertyLossDamage = new ClaimsType(8) { Name = "Property Loss / Damage" };
        public static ClaimsType MarineGIT = new ClaimsType(9) { Name = "Marine / GIT" };
        public static ClaimsType PleasureCraft = new ClaimsType(10) { Name = "Pleasure Craft" };
        public static ClaimsType ContractWorks = new ClaimsType(11) { Name = "Contract Works" };
        public static ClaimsType MotorAccidentNo3rdParty = new ClaimsType(12) { Name = "Motor Accident (No 3rd Party)" };
        public static ClaimsType MotorAccidentYes3rdParty = new ClaimsType(13) { Name = "Motor Accident (Yes 3rd Party)" };
        public static ClaimsType MotorHijack = new ClaimsType(14) { Name = "Motor Hijack" };
        public static ClaimsType GlassWindscreen = new ClaimsType(15) { Name = "Glass (Windscreen)" };
        public static ClaimsType PropertySpecified = new ClaimsType(16) { Name = "Property (Specified)" };
        public static ClaimsType PropertyNotSpecified = new ClaimsType(17) { Name = "Property (Not Specified)" };
        public static ClaimsType FuneralAccidental = new ClaimsType(18) { Name = "Funeral (Accidental)" };
        public static ClaimsType FuneralNonaccidental = new ClaimsType(19) { Name = "Funeral (Non-accidental)" };
        public static ClaimsType Theft = new ClaimsType(20) { Name = "Theft" };
        public static ClaimsType Damage = new ClaimsType(21) { Name = "Damage" };
        public static ClaimsType MotorAccidentVehicleDrivable = new ClaimsType(22) { Name = "Motor Accident - Vehicle Drivable" };
        public static ClaimsType MotorAccidentVehicleNotDrivable = new ClaimsType(23) { Name = "Motor Accident - Vehicle Not Drivable" };
        public static ClaimsType MotorWindscreen = new ClaimsType(24) { Name = "Motor Windscreen" };
        public static ClaimsType MotorTheftClaim = new ClaimsType(25) { Name = "Motor Theft Claim" };
        public static ClaimsType MotorHiJackingClaim = new ClaimsType(26) { Name = "Motor Hi-Jacking Claim" };
        public static ClaimsType MotorRadio = new ClaimsType(27) { Name = "Motor Radio" };
        public static ClaimsType GeneralAllRisk = new ClaimsType(28) { Name = "General All Risk" };
        public static ClaimsType Geyser = new ClaimsType(29) { Name = "Geyser" };
        public static ClaimsType CellPhone = new ClaimsType(30) { Name = "Cell Phone" };
        public static ClaimsType Firearm = new ClaimsType(31) { Name = "Firearm" };
        public static ClaimsType Jewellery = new ClaimsType(32) { Name = "Jewellery" };
        public static ClaimsType PersonalDocuments = new ClaimsType(33) { Name = "Personal Documents" };
        public static ClaimsType Holeinonefullhouse = new ClaimsType(34) { Name = "Hole in one / full house" };
        public static ClaimsType PrescriptionGlassessunglasses = new ClaimsType(35) { Name = "Prescription Glasses / sunglasses" };
        public static ClaimsType AccidentalDamage = new ClaimsType(36) { Name = "Accidental Damage" };
        public static ClaimsType TheftorRobbery = new ClaimsType(37) { Name = "Theft or Robbery" };
        public static ClaimsType Glass = new ClaimsType(38) { Name = "Glass" };
        public static ClaimsType KeysandLocks = new ClaimsType(39) { Name = "Keys and Locks" };
        public static ClaimsType LightningDamage = new ClaimsType(40) { Name = "Lightning Damage" };
        public static ClaimsType Death = new ClaimsType(41) { Name = "Death" };
        public static ClaimsType Hijack = new ClaimsType(42) { Name = "Hijack" };
        public static ClaimsType MotorDamage = new ClaimsType(43) { Name = "Motor Damage" };
        // new AIG changes
        public static ClaimsType MotorFireandExplosion = new ClaimsType(44) { Name = "Motor Fire and Explosion" };
        public static ClaimsType MotorWeatherandElements = new ClaimsType(45) { Name = "Motor Weather and Elements" };
        public static ClaimsType Earthquake = new ClaimsType(46) { Name = "Earthquake" };
        public static ClaimsType FireExplosion = new ClaimsType(47) { Name = "Fire/Explosion" };
        public static ClaimsType GeyserPipeandRelatedDamage = new ClaimsType(48) { Name = "Geyser / Pipes and Related Damage" };
        public static ClaimsType SubsidenceLandslip = new ClaimsType(49) { Name = "Subsidence / Landslip" };
        public static ClaimsType WeatherStorm = new ClaimsType(50) { Name = "Weather / Storm" };
        public static ClaimsType Other = new ClaimsType(51) { Name = "Other" };

        //Smart
        public static ClaimsType MotorDamageSMART = new ClaimsType(52) { Name = "Motor Damage (SMART)" };
        public ClaimsTypes() : base(new[] { MotorTheft, MotorAccident, PublicLiability, PersonalLiability, PersonalAccident, TheftOfPropertyOutOfMotor,
            NonMotorClaimsUpToR10000, PropertyLossDamage, MarineGIT, PleasureCraft, ContractWorks, MotorAccidentNo3rdParty, MotorAccidentYes3rdParty,
            MotorHijack, GlassWindscreen, PropertySpecified, PropertyNotSpecified, FuneralAccidental, FuneralNonaccidental, Theft, Damage, MotorAccidentVehicleDrivable,
            MotorAccidentVehicleNotDrivable, MotorWindscreen, MotorTheftClaim, MotorHiJackingClaim, MotorRadio, GeneralAllRisk, Geyser, CellPhone, Firearm, Jewellery,
            PersonalDocuments, Holeinonefullhouse, PrescriptionGlassessunglasses, AccidentalDamage, TheftorRobbery, Glass, KeysandLocks, LightningDamage, Death, Hijack,
            MotorDamage, MotorFireandExplosion, MotorWeatherandElements, Earthquake, FireExplosion, GeyserPipeandRelatedDamage,SubsidenceLandslip, WeatherStorm, Other,
            MotorDamageSMART
        }) { }
    }
}