﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class InsurersBranchTypes : ReadOnlyCollection<InsurersBranchType>, iMasterDataCollection
    {
        public static InsurersBranchType LongTerm = new InsurersBranchType(2) { Name = "LongTerm" };
        public static InsurersBranchType ShortTerm = new InsurersBranchType(1) { Name = "ShortTerm" };

        public InsurersBranchTypes() : base(new[] { LongTerm, ShortTerm }) { }
    }
}