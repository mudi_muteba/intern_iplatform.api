namespace MasterData
{
    public class InsurersBranchType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public InsurersBranchType() { }

        internal InsurersBranchType(int id) { Id = id; }
    }
}