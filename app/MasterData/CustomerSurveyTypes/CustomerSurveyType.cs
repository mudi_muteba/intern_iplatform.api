﻿namespace MasterData
{
    public class CustomerSurveyType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public CustomerSurveyType() { }

        internal CustomerSurveyType(int id) { Id = id; }
    }
}