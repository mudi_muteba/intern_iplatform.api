﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class CustomerSurveyTypes : ReadOnlyCollection<CustomerSurveyType>, iMasterDataCollection
    {
        public static CustomerSurveyType Unknown = new CustomerSurveyType(1) { Name = "Unknown"};
        public static CustomerSurveyType Policy = new CustomerSurveyType(2) { Name = "Policy" };
        public CustomerSurveyTypes() : base(new[] { Unknown, Policy }) { }
    }
}
