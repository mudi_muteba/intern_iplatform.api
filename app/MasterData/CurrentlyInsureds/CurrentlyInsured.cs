namespace MasterData
{
    public class CurrentlyInsured : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public CurrentlyInsured() { }

        internal CurrentlyInsured(int id) { Id = id; }
    }
}