﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class CurrentlyInsureds : ReadOnlyCollection<CurrentlyInsured>, iMasterDataCollection
    {
        public static CurrentlyInsured CurrentlyInsuredno = new CurrentlyInsured(1) { Name = "CurrentlyInsured no", Answer = "no", VisibleIndex = 0 };
        public static CurrentlyInsured CurrentlyInsuredautoyes = new CurrentlyInsured(2) { Name = "CurrentlyInsured auto-yes", Answer = "auto-yes", VisibleIndex = 0 };
        public static CurrentlyInsured CurrentlyInsuredhomeyes = new CurrentlyInsured(3) { Name = "CurrentlyInsured home-yes", Answer = "home-yes", VisibleIndex = 0 };
        public static CurrentlyInsured CurrentlyInsuredautohomeyes = new CurrentlyInsured(4) { Name = "CurrentlyInsured auto & home-yes", Answer = "auto & home-yes", VisibleIndex = 0 };

        public CurrentlyInsureds() : base(new[] { CurrentlyInsuredno, CurrentlyInsuredautoyes, CurrentlyInsuredhomeyes, CurrentlyInsuredautohomeyes }) { }
    }
}