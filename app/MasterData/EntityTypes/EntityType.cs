﻿using System;

namespace MasterData
{
    [Serializable]
    public class EntityType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public EntityType() { }

        internal EntityType(int id) { Id = id; }
    }
}