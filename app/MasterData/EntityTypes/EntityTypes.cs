﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class EntityTypes : ReadOnlyCollection<EntityType>, iMasterDataCollection
    {
        /* Echo TCF Entity Types
            1 Brokerage
            2 Underwriter
            3 Administrator
            4 Insurer
         */
        public static EntityType Brokerage = new EntityType(1) { Name = "Brokerage" };
        public static EntityType Underwriter = new EntityType(2) { Name = "Underwriter" };
        public static EntityType Administrator = new EntityType(3) { Name = "Administrator" };
        public static EntityType Insurer = new EntityType(4) { Name = "Insurer" };


        public EntityTypes() : base(new[] { Brokerage, Underwriter, Administrator, Insurer}) { }
    }
}