using System;
using iPlatform.Enums.Workflows;

namespace MasterData
{
    [Serializable]
    public class WorkflowMessageEnumType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual WorkflowMessageType EnumType { get; set; }

        public WorkflowMessageEnumType() { }

        internal WorkflowMessageEnumType(int id) { Id = id; }
    }
}