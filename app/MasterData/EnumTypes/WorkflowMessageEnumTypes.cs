using System;
using System.Collections.ObjectModel;
using System.Linq;
using iPlatform.Enums.Workflows;

namespace MasterData
{
    public class WorkflowMessageEnumTypes : ReadOnlyCollection<WorkflowMessageEnumType>, iMasterDataCollection
    {
        public static WorkflowMessageEnumType Sms = new WorkflowMessageEnumType(1) { Name = WorkflowMessageType.Sms.ToString(), EnumType = WorkflowMessageType.Sms };
        public static WorkflowMessageEnumType CreateProposalFromExternalRatingRequest = new WorkflowMessageEnumType(2) { Name = WorkflowMessageType.CreateProposalFromExternalRatingRequest.ToString(), EnumType = WorkflowMessageType.CreateProposalFromExternalRatingRequest };
        public static WorkflowMessageEnumType LeadTransferal = new WorkflowMessageEnumType(3) { Name = WorkflowMessageType.LeadTransferal.ToString(), EnumType = WorkflowMessageType.LeadTransferal };
        public static WorkflowMessageEnumType Email = new WorkflowMessageEnumType(4) { Name = WorkflowMessageType.Email.ToString(), EnumType = WorkflowMessageType.Email };

        public WorkflowMessageEnumTypes() : base(new[] { Sms, CreateProposalFromExternalRatingRequest, LeadTransferal, Email }) { }

        public WorkflowMessageEnumType this[string workflowMessageType]
        {
            get { return this.FirstOrDefault(x => x.EnumType == (WorkflowMessageType)Enum.Parse(typeof(WorkflowMessageType), workflowMessageType)); }
        }
    }
}