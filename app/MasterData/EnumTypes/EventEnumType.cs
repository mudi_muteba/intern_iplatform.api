using System;
using iPlatform.Enums.Workflows;

namespace MasterData
{
    [Serializable]
    public class EventEnumType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual EventType EnumType { get; set; }

        public EventEnumType() { }

        internal EventEnumType(int id) { Id = id; }
    }
}