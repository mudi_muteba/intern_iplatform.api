using System;
using System.Collections.ObjectModel;
using System.Linq;
using iPlatform.Enums.Workflows;

namespace MasterData
{
    public class EventEnumTypes : ReadOnlyCollection<EventEnumType>, iMasterDataCollection
    {
        public static EventEnumType LeadCallBack = new EventEnumType(1) { Name = EventType.LeadCallBack.ToString(), EnumType = EventType.LeadCallBack };

        public EventEnumTypes() : base(new[] { LeadCallBack }) { }

        public EventEnumType this[string eventType]
        {
            get { return this.FirstOrDefault(x => x.EnumType == (EventType)Enum.Parse(typeof(EventType), eventType)); }
        }
    }
}