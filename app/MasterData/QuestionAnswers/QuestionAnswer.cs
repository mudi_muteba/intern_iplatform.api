﻿using System;

namespace MasterData
{
    [Serializable]
    public class QuestionAnswer : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual Question Question { get; set; }

        public QuestionAnswer() { }

        internal QuestionAnswer(int id) { Id = id; }

        protected bool Equals(QuestionAnswer other)
        {
            return Id == other.Id 
                && string.Equals(Name, other.Name) 
                && string.Equals(Answer, other.Answer) 
                && VisibleIndex == other.VisibleIndex 
                && Equals(Question, other.Question);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((QuestionAnswer) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Id;
                hashCode = (hashCode*397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Answer != null ? Answer.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ VisibleIndex ;
                hashCode = (hashCode*397) ^ (Question != null ? Question.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}