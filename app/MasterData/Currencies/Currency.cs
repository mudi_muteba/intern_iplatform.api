﻿using System;

namespace MasterData
{
    [Serializable]
    public class Currency : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Code { get; set; }
        public virtual int NumericCode { get; set; }
        public virtual bool Front { get; set; }

        public Currency() { }

        internal Currency(int id) { Id = id; }
    }
}