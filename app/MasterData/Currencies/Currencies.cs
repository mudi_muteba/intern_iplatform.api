﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class Currencies : ReadOnlyCollection<Currency>, iMasterDataCollection
    {
        public static Currency SouthAfricanRand = new Currency(1) { Name = "South African Rand", Description = "South Africa Rand", Code = "ZAR", NumericCode = 710, Front = true };
        public static Currency KenyanShilling = new Currency(2) { Name = "Kenyan Shilling", Description = "Kenyan Shilling", Code = "KES", NumericCode = 404, Front = true };
        public static Currency MauritiusRupee = new Currency(3) { Name = "Mauritius Rupee", Description = "Mauritius Rupee", Code = "MUR", NumericCode = 480, Front = true };
        public static Currency USDollar = new Currency(4) { Name = "US Dollar", Description = "US Dollar", Code = "USD", NumericCode = 840, Front = true };
        public static Currency Euro = new Currency(5) { Name = "Euro", Description = "Euro", Code = "EUR", NumericCode = 978, Front = true };
        public static Currency PoundSterling = new Currency(6) { Name = "Pound Sterling", Description = "UK Pound Sterling", Code = "GBP", NumericCode = 826, Front = true };

        public Currencies() : base(new[] { SouthAfricanRand, KenyanShilling, MauritiusRupee, USDollar, Euro, PoundSterling }) { }
    }
}