﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class EnvironmentTypes : ReadOnlyCollection<EnvironmentType>, iMasterDataCollection
    {
        //Not currenty in use, may never be.
        public static EnvironmentType Development = new EnvironmentType(1){ Key = "DEV", Name = "Development"};
        public static EnvironmentType Staging = new EnvironmentType(2) { Key = "STAGING", Name = "Staging" };
        public static EnvironmentType SystemIntergrationTesting = new EnvironmentType(3) { Key = "SIT", Name = "System Integrations Testing" };
        public static EnvironmentType UserAcceptanceTesting = new EnvironmentType(4) { Key = "UAT", Name = "User Acceptance Testing" };
        public static EnvironmentType PreProduction = new EnvironmentType(5) { Key = "PREPROD", Name = "Pre-Production" };
        public static EnvironmentType Production = new EnvironmentType(6) { Key = "LIVE", Name = "Production" };


        public EnvironmentTypes() : base(new[] { Development, Staging, SystemIntergrationTesting, UserAcceptanceTesting, PreProduction, Production }) { }
    }
}
