﻿namespace MasterData
{
    public class EnvironmentType : BaseEntity, iMasterData
    {
        public virtual string Key { get; set; }
        public virtual string Name { get; set; }

        public EnvironmentType() { }

        internal EnvironmentType(int id) { Id = id; }
    }
}

