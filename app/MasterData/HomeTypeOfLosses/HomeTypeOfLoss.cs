﻿namespace MasterData
{
    public class HomeTypeOfLoss : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public HomeTypeOfLoss() { }

        internal HomeTypeOfLoss(int id) { Id = id; }
    }
}