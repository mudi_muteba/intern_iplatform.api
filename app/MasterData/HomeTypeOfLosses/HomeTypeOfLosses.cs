using System.Collections.ObjectModel;

namespace MasterData
{
    public class HomeTypeOfLosses : ReadOnlyCollection<HomeTypeOfLoss>, iMasterDataCollection
    {
        public static HomeTypeOfLoss HomeTypeOfLossTheft = new HomeTypeOfLoss(1) { Name = "HomeTypeOfLoss Theft", Answer = "Theft", VisibleIndex = 0 };
        public static HomeTypeOfLoss HomeTypeOfLossPipeleakage = new HomeTypeOfLoss(2) { Name = "HomeTypeOfLoss Pipe leakage", Answer = "Pipe leakage", VisibleIndex = 0 };
        public static HomeTypeOfLoss HomeTypeOfLossFire = new HomeTypeOfLoss(3) { Name = "HomeTypeOfLoss Fire", Answer = "Fire", VisibleIndex = 0 };
        public static HomeTypeOfLoss HomeTypeOfLossStorm = new HomeTypeOfLoss(4) { Name = "HomeTypeOfLoss Storm", Answer = "Storm", VisibleIndex = 0 };
        public static HomeTypeOfLoss HomeTypeOfLossgeysers = new HomeTypeOfLoss(5) { Name = "HomeTypeOfLoss geysers", Answer = "geysers", VisibleIndex = 0 };
        public static HomeTypeOfLoss HomeTypeOfLossAccidentaldamage = new HomeTypeOfLoss(6) { Name = "HomeTypeOfLoss Accidental damage", Answer = "Accidental damage", VisibleIndex = 0 };
        public static HomeTypeOfLoss HomeTypeOfLosssubsidenceandlandslip = new HomeTypeOfLoss(7) { Name = "HomeTypeOfLoss subsidence and landslip", Answer = "subsidence and landslip", VisibleIndex = 0 };
        public static HomeTypeOfLoss HomeTypeOfLossOtherifotherspecify = new HomeTypeOfLoss(8) { Name = "HomeTypeOfLoss Other (if other specify)", Answer = "Other (if other specify)", VisibleIndex = 0 };

        public HomeTypeOfLosses() : base(new[] { HomeTypeOfLossTheft, HomeTypeOfLossPipeleakage, HomeTypeOfLossFire, HomeTypeOfLossStorm, HomeTypeOfLossgeysers, HomeTypeOfLossAccidentaldamage, HomeTypeOfLosssubsidenceandlandslip, HomeTypeOfLossOtherifotherspecify }) { }
    }
}