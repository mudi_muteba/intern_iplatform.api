﻿using System;

namespace MasterData
{
    [Serializable]
    public class ProductLineOfLoss : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public ProductLineOfLoss() { }

        internal ProductLineOfLoss(int id) { Id = id; }
    }
}