using System.Collections.ObjectModel;

namespace MasterData
{
    public class ProductLineOfLosses : ReadOnlyCollection<ProductLineOfLoss>, iMasterDataCollection
    {
        public static ProductLineOfLoss ProductLineOfLossMotor = new ProductLineOfLoss(1) { Name = "ProductLineOfLoss Motor", Answer = "Motor", VisibleIndex = 0 };
        public static ProductLineOfLoss ProductLineOfLossHome = new ProductLineOfLoss(2) { Name = "ProductLineOfLoss Home", Answer = "Home", VisibleIndex = 0 };

        public ProductLineOfLosses() : base(new[] { ProductLineOfLossMotor, ProductLineOfLossHome }) { }
    }
}