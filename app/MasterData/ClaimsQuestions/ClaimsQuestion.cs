using System;

namespace MasterData
{
    [Serializable]
    public class ClaimsQuestion : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual ClaimsQuestionGroup ClaimsQuestionGroup { get; set; }
        public virtual ClaimsQuestionType ClaimsQuestionType { get; set; }
        public virtual ClaimsQuestionType MobileClaimsQuestionType { get; set; }
        public ClaimsQuestion() { }

        internal ClaimsQuestion(int id) { Id = id; }
    }
}