﻿using System;

namespace MasterData
{
    [Serializable]
    public class LeadImportStatus : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual int VisibleIndex { get; set; }

        public LeadImportStatus() { }

        internal LeadImportStatus(int id) { Id = id; }
    }
}