﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class LeadImportStatuses : ReadOnlyCollection<LeadImportStatus>, iMasterDataCollection
    {
        public static LeadImportStatus Submitted = new LeadImportStatus(1) { Name = "Submitted", VisibleIndex = 0 };
        public static LeadImportStatus Processing = new LeadImportStatus(2) { Name = "Processing", VisibleIndex = 1 };
        public static LeadImportStatus Published = new LeadImportStatus(3) { Name = "Published", VisibleIndex = 2 };
        public static LeadImportStatus Completed = new LeadImportStatus(4) { Name = "Completed", VisibleIndex = 3 };
        public static LeadImportStatus Duplicate = new LeadImportStatus(5) { Name = "Duplicate", VisibleIndex = 4 };
        public static LeadImportStatus Failure = new LeadImportStatus(6) { Name = "Failure", VisibleIndex = 5 };
        public static LeadImportStatus Success = new LeadImportStatus(7) { Name = "Success", VisibleIndex = 6 };


        public static LeadImportStatus Imported = new LeadImportStatus(9) { Name = "Imported", VisibleIndex = 7 };
        public static LeadImportStatus Sentforunderwriting = new LeadImportStatus(10) { Name = "Sent for underwriting", VisibleIndex = 8 };
        public static LeadImportStatus Quoted = new LeadImportStatus(11) { Name = "Quoted", VisibleIndex = 9};
        public static LeadImportStatus Rejectedatunderwriting = new LeadImportStatus(12) { Name = "Rejected at underwriting", VisibleIndex = 10 };
        public static LeadImportStatus Sold = new LeadImportStatus(13) { Name = "Sold", VisibleIndex = 11 };
        public static LeadImportStatus Proposal = new LeadImportStatus(14) { Name = "Proposal", VisibleIndex = 12 };
        public static LeadImportStatus QuoteAccepted = new LeadImportStatus(15) { Name = "QuoteAccepted", VisibleIndex = 13 };
        public static LeadImportStatus Loss = new LeadImportStatus(16) { Name = "Loss", VisibleIndex = 14 };
        public static LeadImportStatus Dead = new LeadImportStatus(17) { Name = "Dead", VisibleIndex = 15 };
        public static LeadImportStatus Delay = new LeadImportStatus(18) { Name = "Delay", VisibleIndex = 16 };
        public LeadImportStatuses() : base(new[]
        {
            Submitted, Processing, Published, Completed, Duplicate, Failure, Success,
            Imported,Sentforunderwriting,Quoted,Rejectedatunderwriting,Sold,Proposal,
            QuoteAccepted,Loss,Dead,Delay
        }) { }
    }
}