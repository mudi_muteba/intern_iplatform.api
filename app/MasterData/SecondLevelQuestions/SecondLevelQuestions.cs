using System.Collections.ObjectModel;
using System.Linq;

namespace MasterData
{
    public class SecondLevelQuestions : ReadOnlyCollection<SecondLevelQuestion>, iMasterDataCollection
    {
        public static SecondLevelQuestion Policycancelledorrefusedrenewal = new SecondLevelQuestion(1) { Name = "Policy cancelled or refused renewal", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 1) };
        public static SecondLevelQuestion Underdebtrevieworadministration = new SecondLevelQuestion(2) { Name = "Under debt review or administration", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 1) };
        public static SecondLevelQuestion Anydefaultsorjudgements = new SecondLevelQuestion(3) { Name = "Any defaults or judgements", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 1) };
        public static SecondLevelQuestion Insolventsequestratedorliquidated = new SecondLevelQuestion(4) { Name = "Insolvent, sequestrated or liquidated", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 1) };
        public static SecondLevelQuestion VehicleUse = new SecondLevelQuestion(5) { Name = "Vehicle Use", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 3), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 1) };
        public static SecondLevelQuestion VehicleRegistrationNumber = new SecondLevelQuestion(6) { Name = "Vehicle Registration Number", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 4), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 2) };
        public static SecondLevelQuestion VINNumber = new SecondLevelQuestion(7) { Name = "VIN Number", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 4), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 2) };
        public static SecondLevelQuestion EngineNumber = new SecondLevelQuestion(8) { Name = "Engine Number", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 4), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 2) };
        public static SecondLevelQuestion MFAllRiskSerialIMEINumber = new SecondLevelQuestion(9) { Name = "MF AllRisk - Serial/IMEI Number", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 4), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 3) };
        public static SecondLevelQuestion DriverLicenseNumber = new SecondLevelQuestion(10) { Name = "Driver License Number", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 4), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 2) };
        public static SecondLevelQuestion NATISRegisternumber = new SecondLevelQuestion(11) { Name = "NATIS / Register number", QuestionType = new QuestionTypes().FirstOrDefault(x => x.Id == 4), SecondLevelQuestionGroup = new SecondLevelQuestionGroups().FirstOrDefault(x => x.Id == 2) };

        public SecondLevelQuestions() : base(new[] { Policycancelledorrefusedrenewal, Underdebtrevieworadministration, Anydefaultsorjudgements, Insolventsequestratedorliquidated, VehicleUse, VehicleRegistrationNumber, VINNumber, EngineNumber, MFAllRiskSerialIMEINumber, DriverLicenseNumber, NATISRegisternumber }) { }
    }
}