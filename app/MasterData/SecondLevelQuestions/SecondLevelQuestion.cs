using System;

namespace MasterData
{
    [Serializable]
    public class SecondLevelQuestion : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual QuestionType QuestionType { get; set; }
        public virtual SecondLevelQuestionGroup SecondLevelQuestionGroup { get; set; }

        public SecondLevelQuestion() { }

        internal SecondLevelQuestion(int id) { Id = id; }
    }
}