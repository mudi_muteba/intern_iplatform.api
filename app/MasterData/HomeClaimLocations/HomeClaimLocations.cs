﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class HomeClaimLocations : ReadOnlyCollection<HomeClaimLocation>, iMasterDataCollection
    {
        public static HomeClaimLocation HomeClaimLocationMainresidence = new HomeClaimLocation(1) { Name = "HomeClaimLocation Main residence", Answer = "Main residence", VisibleIndex = 0 };
        public static HomeClaimLocation HomeClaimLocationSecondaryresidence = new HomeClaimLocation(2) { Name = "HomeClaimLocation Secondary residence", Answer = "Secondary residence", VisibleIndex = 0 };
        public static HomeClaimLocation HomeClaimLocationPreviousresidence = new HomeClaimLocation(3) { Name = "HomeClaimLocation Previous residence", Answer = "Previous residence", VisibleIndex = 0 };
        public static HomeClaimLocation HomeClaimLocationHolidayhome = new HomeClaimLocation(4) { Name = "HomeClaimLocation Holiday home", Answer = "Holiday home", VisibleIndex = 0 };

        public HomeClaimLocations() : base(new[] { HomeClaimLocationMainresidence, HomeClaimLocationSecondaryresidence, HomeClaimLocationPreviousresidence, HomeClaimLocationHolidayhome }) { }
    }
}