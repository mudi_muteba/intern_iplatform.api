namespace MasterData
{
    public class HomeClaimLocation : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public HomeClaimLocation() { }

        internal HomeClaimLocation(int id) { Id = id; }
    }
}