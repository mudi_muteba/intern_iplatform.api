﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ClaimsQuestionGroups : ReadOnlyCollection<ClaimsQuestionGroup>, iMasterDataCollection
    {
        public static ClaimsQuestionGroup MotorTheft = new ClaimsQuestionGroup(1) { Name = "Motor Theft", VisibleIndex = 100 };
        public static ClaimsQuestionGroup MotorAccident = new ClaimsQuestionGroup(2) { Name = "Motor Accident", VisibleIndex = 100 };
        public static ClaimsQuestionGroup PublicLiabilityPersonalLiability = new ClaimsQuestionGroup(3) { Name = "Public Liability / Personal Liability", VisibleIndex = 100 };
        public static ClaimsQuestionGroup PersonalAccident = new ClaimsQuestionGroup(5) { Name = "Personal Accident", VisibleIndex = 100 };
        public static ClaimsQuestionGroup TheftOfPropertyOutOfMotor = new ClaimsQuestionGroup(6) { Name = "Theft Of Property Out Of Motor", VisibleIndex = 100 };
        public static ClaimsQuestionGroup NonMotorClaimsUpToR10000 = new ClaimsQuestionGroup(7) { Name = "Non-Motor Claims Up To R10 000", VisibleIndex = 100 };
        public static ClaimsQuestionGroup PropertyLossDamage = new ClaimsQuestionGroup(8) { Name = "Property Loss / Damage", VisibleIndex = 100 };
        public static ClaimsQuestionGroup MarineGIT = new ClaimsQuestionGroup(9) { Name = "Marine / GIT", VisibleIndex = 100 };
        public static ClaimsQuestionGroup PleasureCraft = new ClaimsQuestionGroup(10) { Name = "Pleasure Craft", VisibleIndex = 100 };
        public static ClaimsQuestionGroup ContractWorks = new ClaimsQuestionGroup(11) { Name = "Contract Works", VisibleIndex = 100 };
        public static ClaimsQuestionGroup Insured = new ClaimsQuestionGroup(12) { Name = "Insured", VisibleIndex = 200 };
        public static ClaimsQuestionGroup Vehicle = new ClaimsQuestionGroup(13) { Name = "Vehicle", VisibleIndex = 300 };
        public static ClaimsQuestionGroup Driver = new ClaimsQuestionGroup(14) { Name = "Driver", VisibleIndex = 400 };
        public static ClaimsQuestionGroup Passengers = new ClaimsQuestionGroup(15) { Name = "Passengers", VisibleIndex = 500 };
        public static ClaimsQuestionGroup OtherParty = new ClaimsQuestionGroup(16) { Name = "Other Party", VisibleIndex = 600 };
        public static ClaimsQuestionGroup Witnesses = new ClaimsQuestionGroup(17) { Name = "Witnesses", VisibleIndex = 700 };
        public static ClaimsQuestionGroup Accident = new ClaimsQuestionGroup(18) { Name = "Accident", VisibleIndex = 800 };
        public static ClaimsQuestionGroup Police = new ClaimsQuestionGroup(19) { Name = "Police", VisibleIndex = 900 };
        public static ClaimsQuestionGroup AccidentDescription = new ClaimsQuestionGroup(20) { Name = "Accident Description", VisibleIndex = 1000 };
        public static ClaimsQuestionGroup AccidentDeclaration = new ClaimsQuestionGroup(21) { Name = "Accident Declaration", VisibleIndex = 1100 };
        public static ClaimsQuestionGroup Inspection = new ClaimsQuestionGroup(22) { Name = "Inspection", VisibleIndex = 1200 };
        public static ClaimsQuestionGroup TheftDeclaration = new ClaimsQuestionGroup(23) { Name = "Theft Declaration", VisibleIndex = 1300 };
        public static ClaimsQuestionGroup SupportingDocumentation = new ClaimsQuestionGroup(24) { Name = "Supporting Documentation", VisibleIndex = 1400 };
        public static ClaimsQuestionGroup TheftDetails = new ClaimsQuestionGroup(25) { Name = "Theft Details", VisibleIndex = 1250 };
        public static ClaimsQuestionGroup InjuredPersons = new ClaimsQuestionGroup(26) { Name = "Injured Persons", VisibleIndex = 650 };
        public static ClaimsQuestionGroup LossDamages = new ClaimsQuestionGroup(27) { Name = "Loss / Damages", VisibleIndex = 1350 };
        public static ClaimsQuestionGroup MedicalInformation = new ClaimsQuestionGroup(28) { Name = "Medical Information", VisibleIndex = 1260 };
        public static ClaimsQuestionGroup OtherInsuranceDetails = new ClaimsQuestionGroup(29) { Name = "Other Insurance Details", VisibleIndex = 1270 };
        public static ClaimsQuestionGroup PreviousClaims = new ClaimsQuestionGroup(30) { Name = "Previous Claims", VisibleIndex = 1280 };
        public static ClaimsQuestionGroup DetailsOfProperty = new ClaimsQuestionGroup(31) { Name = "Details Of Property", VisibleIndex = 1360 };
        public static ClaimsQuestionGroup PropertyLost = new ClaimsQuestionGroup(32) { Name = "Property Lost", VisibleIndex = 1370 };
        public static ClaimsQuestionGroup OtherInterest = new ClaimsQuestionGroup(33) { Name = "Other Interest", VisibleIndex = 1365 };
        public static ClaimsQuestionGroup Cost = new ClaimsQuestionGroup(34) { Name = "Cost", VisibleIndex = 350 };
        public static ClaimsQuestionGroup ConvictionsOffenses = new ClaimsQuestionGroup(35) { Name = "Convictions/Offenses", VisibleIndex = 360 };
        public static ClaimsQuestionGroup IncidentDetails = new ClaimsQuestionGroup(36) { Name = "Incident Details", VisibleIndex = 850 };
        public static ClaimsQuestionGroup VesselDetails = new ClaimsQuestionGroup(37) { Name = "Vessel Details", VisibleIndex = 370 };
        public static ClaimsQuestionGroup PartiesInvolved = new ClaimsQuestionGroup(38) { Name = "Parties Involved", VisibleIndex = 1320 };
        public static ClaimsQuestionGroup ContractInformation = new ClaimsQuestionGroup(39) { Name = "Contract Information", VisibleIndex = 1150 };
        public static ClaimsQuestionGroup ProjectInsurance = new ClaimsQuestionGroup(40) { Name = "Project Insurance", VisibleIndex = 1160 };
        public static ClaimsQuestionGroup ContractWorksCost = new ClaimsQuestionGroup(41) { Name = "Contract Works Cost", VisibleIndex = 1380 };
        public static ClaimsQuestionGroup GeneralDetails = new ClaimsQuestionGroup(42) { Name = "General Details", VisibleIndex = 1100 };
        public static ClaimsQuestionGroup NondriveableDetails = new ClaimsQuestionGroup(43) { Name = "Non-driveable Details", VisibleIndex = 950 };
        public static ClaimsQuestionGroup DriveableDetails = new ClaimsQuestionGroup(44) { Name = "Driveable Details", VisibleIndex = 1000 };
        public static ClaimsQuestionGroup DeathInformation = new ClaimsQuestionGroup(45) { Name = "Death Information", VisibleIndex = 1050 };
        public static ClaimsQuestionGroup Declaration = new ClaimsQuestionGroup(46) { Name = "Declaration", VisibleIndex = 1410 };

        public ClaimsQuestionGroups() : base(new[] { MotorTheft, MotorAccident, PublicLiabilityPersonalLiability, PersonalAccident, TheftOfPropertyOutOfMotor, NonMotorClaimsUpToR10000, PropertyLossDamage, MarineGIT, PleasureCraft, ContractWorks, Insured, Vehicle, Driver, Passengers, OtherParty, Witnesses, Accident, Police, AccidentDescription, AccidentDeclaration, Inspection, TheftDeclaration, SupportingDocumentation, TheftDetails, InjuredPersons, LossDamages, MedicalInformation, OtherInsuranceDetails, PreviousClaims, DetailsOfProperty, PropertyLost, OtherInterest, Cost, ConvictionsOffenses, IncidentDetails, VesselDetails, PartiesInvolved, ContractInformation, ProjectInsurance, ContractWorksCost, GeneralDetails, NondriveableDetails, DriveableDetails, DeathInformation, Declaration }) { }
    }
}