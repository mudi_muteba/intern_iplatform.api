using System;

namespace MasterData
{
    [Serializable]
    public class ClaimsQuestionGroup : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual int VisibleIndex { get; set; }

        public ClaimsQuestionGroup() { }

        internal ClaimsQuestionGroup(int id) { Id = id; }
    }
}