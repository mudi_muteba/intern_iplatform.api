﻿namespace MasterData
{
    public class PolicyStatus : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual int VisibleIndex { get; set; }

        public PolicyStatus() { }

        internal PolicyStatus(int id) { Id = id; }
    }
}