using System.Collections.ObjectModel;

namespace MasterData
{
    public class PolicyStatuses : ReadOnlyCollection<PolicyStatus>, iMasterDataCollection
    {
        public static PolicyStatus Cancelled = new PolicyStatus(1) { Name = "Cancelled", VisibleIndex = 0 };
        public static PolicyStatus Potential = new PolicyStatus(2) { Name = "Potential", VisibleIndex = 1 };
        public static PolicyStatus Active = new PolicyStatus(3) { Name = "Active", VisibleIndex = 2 };

        public PolicyStatuses() : base(new[] { Cancelled, Potential, Active }) { }
    }
}