﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterData
{
    public class OptionalExtra : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual AccessoryType AccessoryType { get; set; }

        public OptionalExtra() { }

        internal OptionalExtra(int id) { Id = id; }
    }
}
