﻿using System;

namespace MasterData
{
    [Serializable]
    public class IdentificationType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public IdentificationType() { }

        internal IdentificationType(int id) { Id = id; }
    }
}
