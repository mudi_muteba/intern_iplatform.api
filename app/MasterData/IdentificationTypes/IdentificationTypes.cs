﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class IdentificationTypes : ReadOnlyCollection<IdentificationType>, iMasterDataCollection
    {
        public static IdentificationType SouthAfricanID = new IdentificationType(1) { Name = "SouthAfricanID" };
        public static IdentificationType Passport = new IdentificationType(2) { Name = "Passport" };

        public IdentificationTypes() : base(new[] { SouthAfricanID, Passport }) { }
    }
}