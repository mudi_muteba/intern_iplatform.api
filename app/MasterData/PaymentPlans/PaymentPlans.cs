using System.Collections.ObjectModel;

namespace MasterData
{
    public class PaymentPlans : ReadOnlyCollection<PaymentPlan>, iMasterDataCollection
    {
        public static PaymentPlan Annual = new PaymentPlan(1) { Name = "Annual" };
        public static PaymentPlan BiAnnual = new PaymentPlan(2) { Name = "Bi-Annual" };
        public static PaymentPlan Monthly = new PaymentPlan(3) { Name = "Monthly" };
        public static PaymentPlan OnceOff = new PaymentPlan(5) { Name = "Once-Off" };
        public static PaymentPlan Quarterly = new PaymentPlan(4) { Name = "Quarterly" };
        public static PaymentPlan Term = new PaymentPlan(6) { Name = "Term" }; 

        public PaymentPlans() : base(new[] { Annual, BiAnnual, Monthly, OnceOff, Quarterly, Term }) { }
    }
}