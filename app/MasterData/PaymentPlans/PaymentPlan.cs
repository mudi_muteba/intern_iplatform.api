﻿using System;

namespace MasterData
{
    [Serializable]
    public class PaymentPlan : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public PaymentPlan() { }

        internal PaymentPlan(int id) { Id = id; }
    }
}