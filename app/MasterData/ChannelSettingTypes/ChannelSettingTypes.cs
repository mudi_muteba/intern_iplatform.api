﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class ChannelSettingTypes : ReadOnlyCollection<ChannelSettingType>, iMasterDataCollection
    {
        public static ChannelSettingType Branding = new ChannelSettingType(1){ Name = "Branding"};
        public static ChannelSettingType Connectors = new ChannelSettingType(2) { Name = "Connectors" };
        public static ChannelSettingType Reports = new ChannelSettingType(3) { Name = "Reports" };
        public static ChannelSettingType Payments = new ChannelSettingType(4) { Name = "Payments" };
        public static ChannelSettingType Users = new ChannelSettingType(5) { Name = "Users" };
        public static ChannelSettingType Clients = new ChannelSettingType(6) { Name = "Clients" };
        public static ChannelSettingType Cultures = new ChannelSettingType(7) { Name = "Cultures" };
        public static ChannelSettingType General = new ChannelSettingType(8) { Name = "General" };
        public static ChannelSettingType Addresses = new ChannelSettingType(9) { Name = "Addresses" };
        public static ChannelSettingType Proposals = new ChannelSettingType(10) { Name = "Proposals" };
        public static ChannelSettingType Leads = new ChannelSettingType(11) { Name = "Leads" };
        public static ChannelSettingType Quotes = new ChannelSettingType(12) { Name = "Quotes" };
        public static ChannelSettingType Underwriting = new ChannelSettingType(13) { Name = "Underwriting" };

        public ChannelSettingTypes() : base(new[] {
            Branding, Connectors, Reports, Payments, Users, Clients, Cultures, General,
            Addresses, Proposals, Leads, Quotes, Underwriting
        }) { }
    }
}
