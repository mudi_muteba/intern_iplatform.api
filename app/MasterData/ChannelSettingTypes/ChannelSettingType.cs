﻿namespace MasterData
{
    public class ChannelSettingType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public ChannelSettingType() { }

        internal ChannelSettingType(int id) { Id = id; }
    }
}

