﻿using System.Collections.ObjectModel;

namespace MasterData
{
    public class Genders : ReadOnlyCollection<Gender>, iMasterDataCollection
    {
        public static Gender Male = new Gender(1) { Name = "Male", Code = "Male" };
        public static Gender Female = new Gender(2) { Name = "Female", Code = "Female" };
        public static Gender Unknown = new Gender(3) { Name = "Unknown", Code = "Unknown" };

        public Genders() : base(new[] { Male, Female, Unknown }) { }
    }
}