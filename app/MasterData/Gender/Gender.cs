using System;

namespace MasterData
{
    [Serializable]
    public class Gender : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }

        public Gender() { }

        internal Gender(int id) { Id = id; }
    }
}