﻿using System;

namespace MasterData
{
    [Serializable]
    public class QuestionType : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }

        public QuestionType() { }

        internal QuestionType(int id) { Id = id; }

        protected bool Equals(QuestionType other)
        {
            return Id == other.Id && string.Equals((string)Name, (string)other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((QuestionType)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }
    }
}