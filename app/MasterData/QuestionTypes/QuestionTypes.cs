using System.Collections.ObjectModel;

namespace MasterData
{
    public class QuestionTypes : ReadOnlyCollection<QuestionType>, iMasterDataCollection
    {
        public static QuestionType Checkbox = new QuestionType(1) { Name = "Checkbox" };
        public static QuestionType Date = new QuestionType(2) { Name = "Date" };
        public static QuestionType Dropdown = new QuestionType(3) { Name = "Dropdown" };
        public static QuestionType Textbox = new QuestionType(4) { Name = "Textbox" };
        public static QuestionType Address = new QuestionType(5) { Name = "Address" };
        public static QuestionType Asset = new QuestionType(6) { Name = "Asset" };
        public static QuestionType Bank = new QuestionType(7) { Name = "Bank" };
        public static QuestionType BankBranch = new QuestionType(8) { Name = "BankBranch" };
        public static QuestionType BankAccountType = new QuestionType(9) { Name = "BankAccountType" };
        public static QuestionType Label = new QuestionType(10) { Name = "Label" };

        public QuestionTypes() : base(new[] { Address, Asset, Checkbox, Date, Dropdown, Textbox, Bank, BankBranch, BankAccountType, Label }) { }
    }
}