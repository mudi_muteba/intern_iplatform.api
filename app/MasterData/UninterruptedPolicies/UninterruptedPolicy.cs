﻿using System;

namespace MasterData
{
    [Serializable]
    public class UninterruptedPolicy : BaseEntity, iMasterData
    {
        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }

        public UninterruptedPolicy() { }

        internal UninterruptedPolicy(int id) { Id = id; }
    }
}