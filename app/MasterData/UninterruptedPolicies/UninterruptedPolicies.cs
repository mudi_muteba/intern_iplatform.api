using System.Collections.ObjectModel;

namespace MasterData
{
    public class UninterruptedPolicies : ReadOnlyCollection<UninterruptedPolicy>, iMasterDataCollection
    {
        public static UninterruptedPolicy UninterruptedPolicy0 = new UninterruptedPolicy(1) { Name = "UninterruptedPolicy 0", Answer = "0", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy05A = new UninterruptedPolicy(2) { Name = "UninterruptedPolicy 0.5A", Answer = "0.5", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy10A = new UninterruptedPolicy(3) { Name = "UninterruptedPolicy 1.0A", Answer = "1.0", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy15A = new UninterruptedPolicy(4) { Name = "UninterruptedPolicy 1.5A", Answer = "1.5", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy20A = new UninterruptedPolicy(5) { Name = "UninterruptedPolicy 2.0A", Answer = "2.0", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy25A = new UninterruptedPolicy(6) { Name = "UninterruptedPolicy 2.5A", Answer = "2.5", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy3 = new UninterruptedPolicy(7) { Name = "UninterruptedPolicy 3", Answer = "3", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy4 = new UninterruptedPolicy(8) { Name = "UninterruptedPolicy 4", Answer = "4", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy5 = new UninterruptedPolicy(9) { Name = "UninterruptedPolicy 5", Answer = "5", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy6 = new UninterruptedPolicy(10) { Name = "UninterruptedPolicy 6", Answer = "6", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy7 = new UninterruptedPolicy(11) { Name = "UninterruptedPolicy 7", Answer = "7", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy8 = new UninterruptedPolicy(12) { Name = "UninterruptedPolicy 8", Answer = "8", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy9 = new UninterruptedPolicy(13) { Name = "UninterruptedPolicy 9", Answer = "9", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy10B = new UninterruptedPolicy(14) { Name = "UninterruptedPolicy 10B", Answer = "10", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy11 = new UninterruptedPolicy(15) { Name = "UninterruptedPolicy 11", Answer = "11", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy12 = new UninterruptedPolicy(16) { Name = "UninterruptedPolicy 12", Answer = "12", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy13 = new UninterruptedPolicy(17) { Name = "UninterruptedPolicy 13", Answer = "13", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy14 = new UninterruptedPolicy(18) { Name = "UninterruptedPolicy 14", Answer = "14", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy15B = new UninterruptedPolicy(19) { Name = "UninterruptedPolicy 15B", Answer = "15", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy16 = new UninterruptedPolicy(20) { Name = "UninterruptedPolicy 16", Answer = "16", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy17 = new UninterruptedPolicy(21) { Name = "UninterruptedPolicy 17", Answer = "17", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy18 = new UninterruptedPolicy(22) { Name = "UninterruptedPolicy 18", Answer = "18", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy19 = new UninterruptedPolicy(23) { Name = "UninterruptedPolicy 19", Answer = "19", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy20B = new UninterruptedPolicy(24) { Name = "UninterruptedPolicy 20B", Answer = "20", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy21 = new UninterruptedPolicy(25) { Name = "UninterruptedPolicy 21", Answer = "21", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy22 = new UninterruptedPolicy(26) { Name = "UninterruptedPolicy 22", Answer = "22", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy23 = new UninterruptedPolicy(27) { Name = "UninterruptedPolicy 23", Answer = "23", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy24 = new UninterruptedPolicy(28) { Name = "UninterruptedPolicy 24", Answer = "24", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy25B = new UninterruptedPolicy(29) { Name = "UninterruptedPolicy 25B", Answer = "25", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy26 = new UninterruptedPolicy(30) { Name = "UninterruptedPolicy 26", Answer = "26", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy27 = new UninterruptedPolicy(31) { Name = "UninterruptedPolicy 27", Answer = "27", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy28 = new UninterruptedPolicy(32) { Name = "UninterruptedPolicy 28", Answer = "28", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy29 = new UninterruptedPolicy(33) { Name = "UninterruptedPolicy 29", Answer = "29", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy30 = new UninterruptedPolicy(34) { Name = "UninterruptedPolicy 30", Answer = "30", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy31 = new UninterruptedPolicy(35) { Name = "UninterruptedPolicy 31", Answer = "31", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy32 = new UninterruptedPolicy(36) { Name = "UninterruptedPolicy 32", Answer = "32", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy33 = new UninterruptedPolicy(37) { Name = "UninterruptedPolicy 33", Answer = "33", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy34 = new UninterruptedPolicy(38) { Name = "UninterruptedPolicy 34", Answer = "34", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy35 = new UninterruptedPolicy(39) { Name = "UninterruptedPolicy 35", Answer = "35", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy36 = new UninterruptedPolicy(40) { Name = "UninterruptedPolicy 36", Answer = "36", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy37 = new UninterruptedPolicy(41) { Name = "UninterruptedPolicy 37", Answer = "37", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy38 = new UninterruptedPolicy(42) { Name = "UninterruptedPolicy 38", Answer = "38", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy39 = new UninterruptedPolicy(43) { Name = "UninterruptedPolicy 39", Answer = "39", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy40 = new UninterruptedPolicy(44) { Name = "UninterruptedPolicy 40", Answer = "40", VisibleIndex = 0 };
        public static UninterruptedPolicy UninterruptedPolicy40C = new UninterruptedPolicy(45) { Name = "UninterruptedPolicy >40C", Answer = ">40", VisibleIndex = 0 };

        public UninterruptedPolicies() : base(new[] { UninterruptedPolicy0, UninterruptedPolicy05A, UninterruptedPolicy10A, UninterruptedPolicy15A, UninterruptedPolicy20A, UninterruptedPolicy25A, UninterruptedPolicy3, UninterruptedPolicy4, UninterruptedPolicy5, UninterruptedPolicy6, UninterruptedPolicy7, UninterruptedPolicy8, UninterruptedPolicy9, UninterruptedPolicy10B, UninterruptedPolicy11, UninterruptedPolicy12, UninterruptedPolicy13, UninterruptedPolicy14, UninterruptedPolicy15B, UninterruptedPolicy16, UninterruptedPolicy17, UninterruptedPolicy18, UninterruptedPolicy19, UninterruptedPolicy20B, UninterruptedPolicy21, UninterruptedPolicy22, UninterruptedPolicy23, UninterruptedPolicy24, UninterruptedPolicy25B, UninterruptedPolicy26, UninterruptedPolicy27, UninterruptedPolicy28, UninterruptedPolicy29, UninterruptedPolicy30, UninterruptedPolicy31, UninterruptedPolicy32, UninterruptedPolicy33, UninterruptedPolicy34, UninterruptedPolicy35, UninterruptedPolicy36, UninterruptedPolicy37, UninterruptedPolicy38, UninterruptedPolicy39, UninterruptedPolicy40, UninterruptedPolicy40C }) { }
    }
}