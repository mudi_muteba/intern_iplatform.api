﻿using System.Linq;
using Castle.MicroKernel.Registration;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.VehicleGuide;
using Domain.Lookups.VehicleGuide.Queries;
using Domain.Ratings;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Context;
using iGuide.DTOs.Makes;
using iGuide.DTOs.Responses;
using Moq;
using TestHelper;
using TestObjects.Mothers.Channels;
using TestObjects.Mothers.Users;
using Unit.Tests.Lookups.VehicleGuide._Helpers;
using Xunit.Extensions;

namespace Unit.Tests.Lookups.VehicleGuide.Makes
{
    public class when_querying_all_makes : BaseExecutionPlanTest
    {
        private readonly GuideConnectorStub connector = new GuideConnectorStub();
        private readonly Mock<MakesConnector> makesConnector = new Mock<MakesConnector>();
        private readonly Mock<IRepository> repository = new Mock<IRepository>();
        private ExternalQueryResult<MakeSearchResultsDto> result;
        private readonly IQueryable<VehicleGuideSetting> expectedGuideConfig;

        public when_querying_all_makes() : base(ApiUserObjectMother.AsAdmin())
        {
            makesConnector.Setup(c => c.Get(It.IsAny<GuideExecutionContext>()))
                .Returns(new GETResponseDto<MakeSearchResultsDto>(MakeSearchResultsDtoMother.Create()));

            connector.Makes = makesConnector.Object;

            expectedGuideConfig = VehicleGuideConfigurationObjectMother.ValidVehicleGuideConfiguration().AsQueryable();

            repository.Setup(r => r.GetAll<VehicleGuideSetting>(It.IsAny<ExecutionContext>()))
                .Returns(() => expectedGuideConfig);

            repository.Setup(r => r.GetById<Channel>(It.IsAny<int>()))
                .Returns(() => new Channel(1));

            Container.Register(Component.For<IRepository>().Instance(repository.Object));
            Container.Register(Component.For<IGuideConnector>().Instance(connector));
        }

        public override void Observe()
        {
            result = ExecutionPlan.ExternalQuery<VehicleMakesQuery, MakeSearchResultsDto, MakeSearchResultsDto>()
                .OnSuccess(x => x)
                .Execute();
        }

        [Observation]
        public void the_iguide_connector_is_called()
        {
            makesConnector.Verify(c => c.Get(It.Is<GuideExecutionContext>(ctx => ContextCorrect(ctx))), Times.Once);
        }

        [Observation]
        public void the_results_are_returned()
        {
            result.Response.ShouldNotBeNull();
        }

        private bool ContextCorrect(GuideExecutionContext actualContext)
        {
            if (actualContext == null)
                return false;

            var expectedContext = expectedGuideConfig.FirstOrDefault();

            return actualContext.Country == GuideCountry.ZA
                && actualContext.Sources.All(s => s == GuideInformationSource.MMBook || s == GuideInformationSource.LightstoneAuto)
                && actualContext.API.ApiKey == expectedContext.ApiKey
                && actualContext.API.Email == expectedContext.Email
                ;
        }
    }
}