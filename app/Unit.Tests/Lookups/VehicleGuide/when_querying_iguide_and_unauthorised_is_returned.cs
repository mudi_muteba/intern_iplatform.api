using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.VehicleGuide;
using Domain.Lookups.VehicleGuide.Queries;
using Domain.Ratings;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Context;
using iGuide.DTOs.Makes;
using iGuide.DTOs.Responses;
using Moq;
using TestHelper;
using TestObjects.Mothers.Channels;
using TestObjects.Mothers.Users;
using Unit.Tests.Lookups.VehicleGuide._Helpers;
using Xunit.Extensions;

namespace Unit.Tests.Lookups.VehicleGuide
{
    public class when_querying_iguide_and_unauthorised_is_returned : BaseExecutionPlanTest
    {
        private readonly GuideConnectorStub connector = new GuideConnectorStub();
        private readonly Mock<MakesConnector> makesConnector = new Mock<MakesConnector>();
        private readonly Mock<IRepository> repository = new Mock<IRepository>();
        private ExternalQueryResult<MakeSearchResultsDto> result;
        private readonly IQueryable<VehicleGuideSetting> expectedGuideConfig;

        public when_querying_iguide_and_unauthorised_is_returned() : base(ApiUserObjectMother.AsAdmin())
        {
            makesConnector.Setup(c => c.Get(It.IsAny<GuideExecutionContext>()))
                .Returns(() =>
                {
                    var response = new GETResponseDto<MakeSearchResultsDto>();
                    response.AuthorisationFailed(new List<ResponseErrorMessage>());

                    return response;
                });

            connector.Makes = makesConnector.Object;

            expectedGuideConfig = VehicleGuideConfigurationObjectMother.ValidVehicleGuideConfiguration().AsQueryable();

            repository.Setup(r => r.GetAll<VehicleGuideSetting>(It.IsAny<ExecutionContext>()))
                .Returns(() => expectedGuideConfig);

            Container.Register(Component.For<IRepository>().Instance(repository.Object));
            Container.Register(Component.For<IGuideConnector>().Instance(connector));
        }

        public override void Observe()
        {
            result = ExecutionPlan.ExternalQuery<VehicleMakesQuery, MakeSearchResultsDto, MakeSearchResultsDto>()
                .OnSuccess(x => x)
                .Execute();
        }

        [Observation]
        public void no_results_are_returned()
        {
            result.Response.ShouldBeNull();
        }
    }
}