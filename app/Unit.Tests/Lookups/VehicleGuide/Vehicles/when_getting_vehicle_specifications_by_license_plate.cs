﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.VehicleGuide;
using Domain.Lookups.VehicleGuide.Queries;
using Domain.Ratings;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Context;
using iGuide.DTOs.Responses;
using iGuide.DTOs.Specifications;
using iGuide.DTOs.Values;
using Moq;
using TestHelper;
using TestObjects.Mothers.Channels;
using TestObjects.Mothers.Users;
using Unit.Tests.Lookups.VehicleGuide._Helpers;
using Xunit.Extensions;

namespace Unit.Tests.Lookups.VehicleGuide.Vehicles
{
    public class when_getting_vehicle_specifications_by_license_plate : BaseExecutionPlanTest
    {
        private readonly GuideConnectorStub connector = new GuideConnectorStub();
        private readonly Mock<VehicleSpecsConnector> vehicleConnector = new Mock<VehicleSpecsConnector>();
        private readonly Mock<IRepository> repository = new Mock<IRepository>();
        private ExternalQueryResult<VehicleSpecsResultsDto> result;
        private readonly IQueryable<VehicleGuideSetting> expectedGuideConfig;

        private const string licensePlateNumber = "XMC167GP";

        public when_getting_vehicle_specifications_by_license_plate()
            : base(ApiUserObjectMother.AsAdmin())
        {
            vehicleConnector.Setup(c => c.SearchLicensePlate(It.IsAny<string>(), It.IsAny<GuideExecutionContext>()))
                .Returns(new GETResponseDto<VehicleSpecsResultsDto>(new VehicleSpecsResultsDto()));

            connector.VehicleSpecifications = vehicleConnector.Object;

            expectedGuideConfig = VehicleGuideConfigurationObjectMother.ValidVehicleGuideConfiguration().AsQueryable();

            repository.Setup(r => r.GetAll<VehicleGuideSetting>(It.IsAny<ExecutionContext>()))
                .Returns(() => expectedGuideConfig);

            repository.Setup(r => r.GetById<Channel>(It.IsAny<int>()))
                .Returns(() => new Channel(1));

            Container.Register(Component.For<IRepository>().Instance(repository.Object));
            Container.Register(Component.For<IGuideConnector>().Instance(connector));

        }

        public override void Observe()
        {
            result = ExecutionPlan.ExternalQuery<VehicleSpecificationsQuery, VehicleSpecsResultsDto, VehicleSpecsResultsDto>()
                .Configure(q => q.ForLicensePlate(licensePlateNumber))
                .OnSuccess(x => x)
                .Execute();

        }

        [Observation]
        public void the_iguide_connector_is_called()
        {
            vehicleConnector.Verify(c => c.SearchLicensePlate(It.Is<string>(x => x == licensePlateNumber), It.Is<GuideExecutionContext>(ctx => ContextCorrect(ctx))), Times.Once);
        }

        [Observation]
        public void the_results_are_returned()
        {
            result.Response.ShouldNotBeNull();
        }

        private bool ContextCorrect(GuideExecutionContext actualContext)
        {
            if (actualContext == null)
                return false;

            var expectedContext = expectedGuideConfig.FirstOrDefault();

            return actualContext.Country == GuideCountry.ZA
                && actualContext.Sources.All(s => s == GuideInformationSource.LightstoneAuto || s == GuideInformationSource.MMBook)
                && actualContext.API.ApiKey == expectedContext.ApiKey
                && actualContext.API.Email == expectedContext.Email
                ;
        }
    }
}
