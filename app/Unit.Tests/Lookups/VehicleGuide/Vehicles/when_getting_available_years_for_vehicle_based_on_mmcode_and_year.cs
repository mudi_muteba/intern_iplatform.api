using System.Linq;
using Castle.MicroKernel.Registration;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.VehicleGuide;
using Domain.Lookups.VehicleGuide.Queries;
using iGuide.DTOs.Connector;
using iGuide.DTOs.Context;
using iGuide.DTOs.Responses;
using iGuide.DTOs.Years;
using Moq;
using TestHelper;
using TestObjects.Mothers.Channels;
using TestObjects.Mothers.Users;
using Unit.Tests.Lookups.VehicleGuide._Helpers;
using Xunit.Extensions;

namespace Unit.Tests.Lookups.VehicleGuide.Vehicles
{
    public class when_getting_available_years_for_vehicle_based_on_mmcode_and_year : BaseExecutionPlanTest
    {
        private const string mmCode = "02081285";
        private const int year = 2004;
        private readonly GuideConnectorStub connector = new GuideConnectorStub();
        private readonly IQueryable<VehicleGuideSetting> expectedGuideConfig;
        private readonly Mock<IRepository> repository = new Mock<IRepository>();
        private readonly Mock<YearConnector> yearConnector = new Mock<YearConnector>();
        private ExternalQueryResult<VehicleYearsDto> result;

        public when_getting_available_years_for_vehicle_based_on_mmcode_and_year() : base(ApiUserObjectMother.AsAdmin())
        {
            yearConnector.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<GuideExecutionContext>()))
                .Returns(new GETResponseDto<VehicleYearsDto>(new VehicleYearsDto()));

            connector.Years = yearConnector.Object;

            expectedGuideConfig = VehicleGuideConfigurationObjectMother.ValidVehicleGuideConfiguration().AsQueryable();

            repository.Setup(r => r.GetAll<VehicleGuideSetting>(It.IsAny<ExecutionContext>()))
                .Returns(() => expectedGuideConfig);

            repository.Setup(r => r.GetById<Channel>(It.IsAny<int>()))
                .Returns(() => new Channel(1));

            Container.Register(Component.For<IRepository>().Instance(repository.Object));
            Container.Register(Component.For<IGuideConnector>().Instance(connector));
        }

        public override void Observe()
        {
            result = ExecutionPlan.ExternalQuery<VehicleYearsQuery, VehicleYearsDto, VehicleYearsDto>()
                .Configure(q => q.ForMMCode(mmCode))
                .OnSuccess(x => x)
                .Execute();
        }

        [Observation]
        public void the_iguide_connector_is_called()
        {
            yearConnector.Verify(c => c.Get(It.Is<string>(x => x == mmCode)
                , It.Is<GuideExecutionContext>(ctx => ContextCorrect(ctx))), Times.Once);
        }

        [Observation]
        public void the_results_are_returned()
        {
            result.Response.ShouldNotBeNull();
        }

        private bool ContextCorrect(GuideExecutionContext actualContext)
        {
            if (actualContext == null)
                return false;

            var expectedContext = expectedGuideConfig.FirstOrDefault();

            return actualContext.Country == GuideCountry.ZA
                   && actualContext.Sources.All(s => s == GuideInformationSource.MMBook || s == GuideInformationSource.LightstoneAuto)
                   && actualContext.API.ApiKey == expectedContext.ApiKey
                   && actualContext.API.Email == expectedContext.Email
                ;
        }
    }
}