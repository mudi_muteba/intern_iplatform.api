using iGuide.DTOs.Connector;

namespace Unit.Tests.Lookups.VehicleGuide._Helpers
{
    public class GuideConnectorStub : IGuideConnector
    {
        public MakesConnector Makes { get; set; }
        public ModelsConnector Models { get; set; }
        public VehicleConnector Vehicle { get; set; }
        public YearConnector Years { get; set; }
        public AdminConnector Admin { get; set; }
        public TransactionConnector Transactions { get; set; }
        public VehicleSpecsConnector VehicleSpecifications { get; set; }
    }
}