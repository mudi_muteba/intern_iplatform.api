﻿using System.Collections.Generic;
using iGuide.DTOs;
using iGuide.DTOs.Makes;

namespace Unit.Tests.Lookups.VehicleGuide._Helpers
{
    internal class MakeSearchResultsDtoMother
    {
        public static MakeSearchResultsDto Create()
        {
            return new MakeSearchResultsDto()
            {
                AvailableMakes = new List<MakeSearchDto>()
                {
                    new MakeSearchDto()
                    {
                        Source = new VehicleInfoSource()
                        {
                            Name = "MMBook",
                            Id = 1
                        },
                        Makes = new List<MakeSearchResultDto>()
                        {
                            new MakeSearchResultDto()
                            {
                                DisconYear = 2014,
                                IntroYear = 2000,
                                Make = "Make 1"
                            },
                            new MakeSearchResultDto()
                            {
                                DisconYear = 2002,
                                IntroYear = 2000,
                                Make = "Make 3"
                            },
                            new MakeSearchResultDto()
                            {
                                DisconYear = 2014,
                                IntroYear = 1987,
                                Make = "Make 2"
                            },
                            new MakeSearchResultDto()
                            {
                                DisconYear = 2014,
                                IntroYear = 1987,
                                Make = "Must match search term 1"
                            },
                            new MakeSearchResultDto()
                            {
                                DisconYear = 1990,
                                IntroYear = 1987,
                                Make = "Must match search term 2"
                            },
                        }
                    }
                }
            };
        }
    }
}