﻿using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.Person;
using Domain.Lookups.Person.Queries;
using iPerson.Api.DTOs.Connector;
using iPerson.Api.DTOs.PersonInformation;
using iPerson.Api.DTOs.Responses;
using iPlatform.Api.DTOs.Lookups.Person;
using Moq;
using TestHelper;
using Unit.Tests.Lookups.PersonLookup._Helpers;
using Xunit.Extensions;

namespace Unit.Tests.Lookups.PersonLookup.PCubed
{
    public class when_quering_pcubed : BaseExecutionPlanTest
    {
        private readonly Mock<PersonConnector> connector = new Mock<PersonConnector>();

        private readonly Mock<IRepository> repository = new Mock<IRepository>();

        private readonly PersonLookupRequest request;

        private ExternalQueryResult<PersonInformationResponsesDto> result;
        private readonly Mock<IProvideContext> provider = new Mock<IProvideContext>();

        public when_quering_pcubed() : base(ApiUserObjectMother.AsAdmin())
        {
            connector.Setup(c => c.PersonInformation.Get(
                It.IsAny<PersonInformationContext>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new GETResponseDto<PersonInformationResponsesDto>(PersonInformationMother.Create()));

            request = new PersonLookupRequest() { Email = "", IdNumber = "", PhoneNumber = "" };

            var expectedGuideConfig = TestObjects.Mothers.Persons.PersonSettingsConfigurationMother.ValidPersonLookupConfiguration()
                    .AsQueryable();

            provider.Setup(r => r.Get());

            repository.Setup(r => r.GetAll<PersonLookupSetting>(It.IsAny<ExecutionContext>()))
                .Returns(() => expectedGuideConfig);


            Container.Register(Component.For<IPersonConnector>().Instance(connector.Object));
            Container.Register(Component.For<IProvideContext>().Instance(provider.Object));
            Container.Register(Component.For<IRepository>().Instance(repository.Object));
        }

        public override void Observe()
        {
            result = ExecutionPlan
              .ExternalQuery<PersonLookupQuery, PersonInformationResponsesDto, PersonInformationResponsesDto>()
              .Configure(q => q.WithRequest(request))
              .OnSuccess(x => x)
              .Execute();
        }

        [Observation]
        public void the_person_connector_is_called()
        {
            connector.Verify(c => c.PersonInformation.Get(It.Is<PersonInformationContext>(ctx => ContextCorrect(ctx)), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }


        [Observation]
        public void the_results_are_returned()
        {
            result.Response.ShouldNotBeNull();
        }

        private bool ContextCorrect(PersonInformationContext actualContext)
        {
            if (actualContext == null)
                return false;
            return true;
            //var expectedContext = expectedGuideConfig.FirstOrDefault();

            //return actualContext.Country == GuideCountry.ZA
            //    && actualContext.Sources.All(s => s == GuideInformationSource.MMBook || s == GuideInformationSource.LightstoneAuto)
            //    && actualContext.API.ApiKey == expectedContext.ApiKey
            //    && actualContext.API.Email == expectedContext.Email
            //    ;
        }
    }
}