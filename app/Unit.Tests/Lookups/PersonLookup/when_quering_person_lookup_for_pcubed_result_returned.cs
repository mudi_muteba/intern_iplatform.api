﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Lookups.Person;
using Domain.Lookups.Person.Queries;
using iPerson.Api.DTOs.Connector;
using iPerson.Api.DTOs.PersonInformation;
using iPerson.Api.DTOs.Responses;
using iPlatform.Api.DTOs.Lookups.Person;
using Moq;
using RestSharp;
using TestHelper;
using TestObjects.Mothers.Persons;
using Unit.Tests.Lookups.VehicleGuide._Helpers;
using Xunit.Extensions;

namespace Unit.Tests.Lookups.PersonLookup
{
    public class when_quering_person_lookup_for_pcubed_result_returned : BaseExecutionPlanTest
    {
        private readonly Mock<PersonConnector> connector = new Mock<PersonConnector>();

        private readonly Mock<IRepository> repository = new Mock<IRepository>();

        //private readonly PersonConnector connector = new PersonConnector();

        private readonly PersonLookupRequest request;

        private ExternalQueryResult<PersonInformationResponsesDto> result;
        private readonly Mock<IProvideContext> provider = new Mock<IProvideContext>();
        public when_quering_person_lookup_for_pcubed_result_returned() : base(ApiUserObjectMother.AsAdmin())
        {

            connector.Setup(c => c.PersonInformation.Get(
                 It.IsAny<PersonInformationContext>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(() =>
               {

                   var response = new GETResponseDto<PersonInformationResponsesDto>();
                   response.ValidationFailed(new List<ResponseErrorMessage>());

                   return response;
               });

            request = new PersonLookupRequest() { Email = "", IdNumber = "", PhoneNumber = "" };

            var expectedGuideConfig = TestObjects.Mothers.Persons.PersonSettingsConfigurationMother.ValidPersonLookupConfiguration().AsQueryable();

            provider.Setup(r => r.Get());

            repository.Setup(r => r.GetAll<PersonLookupSetting>(It.IsAny<ExecutionContext>()))
                .Returns(() => expectedGuideConfig);


            Container.Register(Component.For<IPersonConnector>().Instance(connector.Object));
            Container.Register(Component.For<IProvideContext>().Instance(provider.Object));
            Container.Register(Component.For<IRepository>().Instance(repository.Object));
        }

        public override void Observe()
        {
            result = ExecutionPlan
               .ExternalQuery<PersonLookupQuery, PersonInformationResponsesDto, PersonInformationResponsesDto>()
               .Configure(q => q.WithRequest(request))
               .OnSuccess(x => x)
               .Execute();
        }

        [Observation]
        public void no_results_are_returned()
        {
            result.Response.ShouldBeNull();
        }

    }
}
