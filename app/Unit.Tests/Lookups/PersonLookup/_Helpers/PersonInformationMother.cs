﻿using System.Collections.Generic;
using iPerson.Api.DTOs.PersonInformation;

namespace Unit.Tests.Lookups.PersonLookup._Helpers
{
    internal class PersonInformationMother
    {
        public static PersonInformationResponsesDto Create()
        {
            return new PersonInformationResponsesDto()
            {
                Results = new List<PersonInformationCollectionDto>()
                {
                    new PersonInformationCollectionDto()
                    {
                        People = new List<PersonInformationResponseDto>()
                        ,
                        Source = new PersonInformationSourceDto()
                        {
                            Id = 1,
                            Name = "PCUBED"
                        }
                    }
                }
            };
        }
    }
}