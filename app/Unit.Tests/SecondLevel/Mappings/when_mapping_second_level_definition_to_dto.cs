﻿using AutoMapper;
using Domain.SecondLevel.Questions;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using TestHelper.Installers;
using TestObjects.Mothers.SecondLevel;
using Xunit.Extensions;

namespace Unit.Tests.SecondLevel.Mappings
{
    public class when_mapping_second_level_definition_to_dto : Specification
    {
      
        private readonly SecondLevelQuestionDefinition _definition;
        private SecondLevelQuestionDefinitionDto _dto;

        public when_mapping_second_level_definition_to_dto()
        {

            DomainMarkerMapperConfiguration.Configure();
            _definition = SecondLevelQuestionDefinitionMother.ForSecondLevelQuestionDefinition();
        }

        public override void Observe()
        {

        }

        [Observation]
        public void then_second_Level_question_definition_should_be_mapped_to_correct_dto()
        {
            _dto = Mapper.Map<SecondLevelQuestionDefinition, SecondLevelQuestionDefinitionDto>(_definition);

            _dto.DisplayName.ShouldEqual(_definition.DisplayName);
            _dto.ToolTip.ShouldEqual(_definition.ToolTip);

            _dto.SecondLevelQuestion.ShouldBeType<SecondLevelQuestionDto>();
            _dto.SecondLevelQuestion.Name.ShouldEqual(_definition.SecondLevelQuestion.Name);
            _dto.SecondLevelQuestion.Id.ShouldEqual(_definition.SecondLevelQuestion.Id);

            _dto.SecondLevelQuestion.Type.ShouldBeType<SecondLevelQuestionTypeDto>();
            _dto.SecondLevelQuestion.Type.Name.ShouldEqual(_definition.SecondLevelQuestion.QuestionType.Name);
            _dto.SecondLevelQuestion.Type.Id.ShouldEqual(_definition.SecondLevelQuestion.QuestionType.Id);

            _dto.SecondLevelQuestion.Group.ShouldBeType<SecondLevelQuestionGroupDto>();
            _dto.SecondLevelQuestion.Group.Name.ShouldEqual(_definition.SecondLevelQuestion.SecondLevelQuestionGroup.Name);
            _dto.SecondLevelQuestion.Group.Id.ShouldEqual(_definition.SecondLevelQuestion.SecondLevelQuestionGroup.Id);


            _dto.CoverDefinition.ShouldBeType<CoverDefinitionDto>();
        }
    }
}