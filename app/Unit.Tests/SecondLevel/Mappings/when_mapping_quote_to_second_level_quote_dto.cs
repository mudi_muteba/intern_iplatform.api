﻿using AutoMapper;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using TestHelper.Installers;
using TestObjects.Mothers.SecondLevel;
using Xunit.Extensions;

namespace Unit.Tests.SecondLevel.Mappings
{
    public class when_mapping_quote_to_second_level_quote_dto : Specification
    {
        //private readonly Mock<Quote> _quote = new Mock<Quote>();
        private Quote _quote;
        private SecondLevelQuoteDto _dto;

        public override void Observe()
        {
        
             _quote = SecondLevelQuestionDefinitionMother.ForSecondLevelQuote();
            DomainMarkerMapperConfiguration.Configure();
        }

        [Observation]
        public void then_second_level_quote_dto_should_be_mapped_correctly()
        {
            _dto = Mapper.Map<Quote, SecondLevelQuoteDto>(_quote);
            _dto.ProductId.ShouldEqual(_quote.Product.Id);
        }
    }
}
