﻿using System.Linq;
using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.SecondLevel.Queries;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using Moq;
using TestHelper;
using TestObjects.Mothers.SecondLevel;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Unit.Tests.SecondLevel.Questions
{
    public class when_querying_second_level_questions_for_quote : BaseExecutionPlanTest
    {
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private readonly Mock<IQueryProcedure> _viewQuery = new Mock<IQueryProcedure>();
        private HandlerResult<SecondLevelQuestionsForQuoteDto> _result;
        private readonly SecondLevelQuoteDto _dto;


        public when_querying_second_level_questions_for_quote()
            : base(ApiUserObjectMother.AsAdmin())
        {
            _dto = SecondLevelQuestionDefinitionMother.ForSecondLevelQuoteDto();
            Container.Register(Component.For<IRepository>().Instance(_repository.Object));
            Container.Register(Component.For<IQueryProcedure>().Instance(_viewQuery.Object));
        }

        public override void Observe()
        {
            _result = ExecutionPlan.Execute<SecondLevelQuoteDto, SecondLevelQuestionsForQuoteDto>(_dto);
        }

        [Observation]
        public void then_second_level_questions_for_quote_should_exist()
        {
            _result.Response.ShouldNotBeNull();
            _result.Response.Questions.ShouldNotBeNull();
            _result.Response.QuoteItems.ShouldNotBeNull();
            _result.Response.QuoteItems.Count.ShouldEqual(1);
            _result.Response.QuoteItems.FirstOrDefault(w => w.Description == "No Description Found").ShouldNotBeNull();
        }
    }
}
