﻿using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.SecondLevel.Answers;
using Moq;
using TestHelper;
using TestObjects.Mothers.SecondLevel;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Unit.Tests.SecondLevel.Answers
{
    public class when_saving_second_level_answers_for_quote : BaseExecutionPlanTest
    {
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private readonly Mock<IQueryProcedure> _viewQuery = new Mock<IQueryProcedure>();
        private readonly SecondLevelQuestionsAnswersSaveDto _dto;
        private HandlerResult<bool> _result;

        public when_saving_second_level_answers_for_quote()
            : base(ApiUserObjectMother.AsAdmin())
        {
            _dto = SecondLevelQuestionDefinitionMother.ForSecondLevelQuestionsSaveDto();

           // string json = JsonConvert.SerializeObject(_dto);

            Container.Register(Component.For<IRepository>().Instance(_repository.Object));
            Container.Register(Component.For<IQueryProcedure>().Instance(_viewQuery.Object));
        }

        public override void Observe()
        {
            _result = ExecutionPlan.Execute<SecondLevelQuestionsAnswersSaveDto, bool>(_dto);
        }

        [Observation]
        public void then_second_level_answers_saved_handler_should_exist()
        {
            _result.Response.ShouldNotBeNull();
        }
    }
}
