﻿using System;
using AutoMapper;
using Castle.MicroKernel.Registration;
using Domain.Admin;
using Domain.Base.Repository;
using Moq;
using TestHelper;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Unit.Tests.Base
{
    public class when_mapping_id_to_entity : BaseExecutionPlanTest
    {
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private readonly Guid _systemId = Guid.NewGuid();
        private readonly Channel _channel;

        public when_mapping_id_to_entity() : base(ApiUserObjectMother.AsAdmin())
        {
            _repository.Setup(r => r.GetById<Channel>(It.IsAny<int>())).Returns(() => new Channel(1){SystemId = _systemId});

            Container.Register(Component.For<IRepository>().Instance(_repository.Object));

            _channel = Mapper.Map<int, Channel>(1);
        }

        public override void Observe()
        {
        }

        [Observation]
        public void should_map()
        {
            _channel.ShouldNotBeNull();
            _channel.Id.ShouldEqual(1);
            _channel.SystemId.ShouldEqual(_systemId);
        }
    }
}