﻿using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Users;
using iPlatform.Api.DTOs.Users;
using Moq;
using TestHelper;
using TestHelper.Helpers.Extensions;
using TestObjects.Mothers.Users;
using ValidationMessages;
using ValidationMessages.Users;
using Xunit.Extensions;

namespace Unit.Tests.Users
{
    public class when_creating_a_user_with_a_duplication_user_name : BaseExecutionPlanTest
    {
        private readonly CreateUserDto _duplicateUserDto;
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private HandlerResult<int> _result;

        public when_creating_a_user_with_a_duplication_user_name() : base(ApiUserObjectMother.AsAdmin())
        {
            _duplicateUserDto = UserDtoObjectMother.DuplicateUser();
            var existingUser = UserObjectMother.ExistingFixUser();

            _repository.Setup(r => r.GetAll<User>(null)).Returns(() => new List<User> { existingUser }.AsQueryable());

            Container.Register(Component.For<IRepository>().Instance(_repository.Object));
        }

        public override void Observe()
        {
            _result = ExecutionPlan.Execute<CreateUserDto, int>(_duplicateUserDto);
        }

        [Observation]
        public void the_user_creation_does_not_complete()
        {
            _result.Completed.ShouldBeFalse();
        }

        [Observation]
        public void validation_error_messages_are_returned()
        {
            var validationMessagesCorrect = _result.AllErrorDTOMessages.ContainsAll(new List<ValidationErrorMessage>()
            {
                UserValidationMessages.DuplicateUserName("xxx"),
            }, (message, errorMessage) => message.MessageKey.Equals(errorMessage.MessageKey));

            validationMessagesCorrect.ShouldBeTrue();
        }
    }
}