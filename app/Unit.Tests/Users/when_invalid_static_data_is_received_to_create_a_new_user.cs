﻿using System.Collections.Generic;
using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Users;
using Moq;
using TestHelper;
using TestHelper.Helpers.Extensions;
using TestObjects.Mothers.Users;
using ValidationMessages;
using ValidationMessages.Settings;
using ValidationMessages.Users;
using Xunit.Extensions;

namespace Unit.Tests.Users
{
    public class when_invalid_static_data_is_received_to_create_a_new_user : BaseExecutionPlanTest
    {
        private readonly CreateUserDto _invalidCreateUserDto;
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private HandlerResult<int> _result;

        public when_invalid_static_data_is_received_to_create_a_new_user() : base(ApiUserObjectMother.AsAdmin())
        {
            _invalidCreateUserDto = UserDtoObjectMother.InvalidStaticDataForNewUser();

            Container.Register(Component.For<IRepository>().Instance(_repository.Object));
        }

        public override void Observe()
        {
            _result = ExecutionPlan.Execute<CreateUserDto, int>(_invalidCreateUserDto);
        }

        [Observation]
        public void the_user_creation_does_not_complete()
        {
            _result.Completed.ShouldBeFalse();
        }

        [Observation]
        public void validation_error_messages_are_returned()
        {
            var validationMessagesCorrect = _result.AllErrorDTOMessages.ContainsAll(new List<ValidationErrorMessage>
            {
                UserValidationMessages.NoChannelsAllocated,
                UserValidationMessages.NoRolesAllocated,
                UserValidationMessages.UserNameRequired,
                UserValidationMessages.PasswordRequired,

            }, (message, errorMessage) => message.MessageKey.Equals(errorMessage.MessageKey));

            validationMessagesCorrect.ShouldBeTrue();
        }
    }
}