using System.Linq;
using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Users;
using iPlatform.Api.DTOs.Users;
using Moq;
using TestHelper;
using TestObjects.Mothers.Users;
using ValidationMessages;
using Xunit.Extensions;

namespace Unit.Tests.Users
{
    public class when_editing_a_user_but_the_user_does_not_exist : BaseExecutionPlanTest
    {
        private readonly EditUserDto missingUserEdit;
        private readonly Mock<IRepository> repository = new Mock<IRepository>();
        private HandlerResult<int> result;

        public when_editing_a_user_but_the_user_does_not_exist() : base(ApiUserObjectMother.AsAdmin())
        {
            missingUserEdit = UserDtoObjectMother.MissingUserToEdit(1);

            repository.Setup(r => r.GetById<User>(1)).Returns(() => null);

            Container.Register(Component.For<IRepository>().Instance(repository.Object));
        }

        public override void Observe()
        {
            result = ExecutionPlan.Execute<EditUserDto, int>(missingUserEdit);
        }

        [Observation]
        public void the_user_creation_does_not_complete()
        {
            result.Completed.ShouldBeFalse();
        }

        [Observation]
        public void validation_error_messages_are_returned()
        {
            result.MissingEntity.Passed.ShouldBeFalse();

            var expectedValidationMessageKey = SystemErrorMessages.MissingEntityErrorMessage(typeof (User), 1).MessageKey;

            result.MissingEntity.DTOMessages.Any(m => m.MessageKey.Equals(expectedValidationMessageKey)).ShouldBeTrue();
        }
    }
}