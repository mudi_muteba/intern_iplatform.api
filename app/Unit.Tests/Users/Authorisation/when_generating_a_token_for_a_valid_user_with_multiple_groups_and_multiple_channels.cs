﻿using Domain.Users;
using Domain.Users.Authentication;
using iPlatform.Api.DTOs.Users.Authentication;
using TestHelper.Helpers.Extensions;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Unit.Tests.Users.Authorisation
{
    public class when_generating_a_token_for_a_valid_user_with_multiple_groups_and_multiple_channels : Specification
    {
        private readonly User user;
        private readonly AuthenticationRequestDto authenticationRequest;
        private TokenGenerator.TokenReader tokenReader;

        public when_generating_a_token_for_a_valid_user_with_multiple_groups_and_multiple_channels()
        {
            user = UserObjectMother.UserWithMultipleGroupsInMultipleChannels();

            authenticationRequest = new AuthenticationRequestDto()
            {
                Password = "This is the password",
                Email = user.UserName
            };
        }

        public override void Observe()
        {
            user.Authenticate(authenticationRequest);
            tokenReader = new TokenGenerator.TokenReader(user.Tokens.Token);
        }

        [Observation]
        public void a_token_is_generated()
        {
            string.IsNullOrWhiteSpace(user.Tokens.Token).ShouldBeFalse();
        }

        //[Observation]
        //public void the_users_username_is_included_in_the_token()
        //{
        //    tokenReader.Email.ShouldEqual(user.UserName);
        //}

        //[Observation]
        //public void the_token_contains_the_channels_to_which_the_user_belongs()
        //{
        //    var channels = tokenReader.Channels;
        //    channels.Count.ShouldEqual(user.Channels.Count);

        //    channels.IsEqual(user.Channels, (i, uc) => i == uc.Channel.Id)
        //        .ShouldBeTrue();
        //}

        //[Observation]
        //public void the_token_contains_the_groups_to_which_the_user_belongs()
        //{
        //    var groups = tokenReader.Groups;
        //    groups.Count.ShouldEqual(user.AuthorisationGroups.Count);

        //    groups.IsEqual<UserAuthorisationGroup>(user.AuthorisationGroups, Compare);
        //}

        private static bool Compare(UserAuthorisationGroup @group, UserAuthorisationGroup authorisationGroup)
        {
            return @group.AuthorisationGroup.Id == authorisationGroup.AuthorisationGroup.Id &&
                @group.Channel.Id == authorisationGroup.Channel.Id;
        }
    }
}