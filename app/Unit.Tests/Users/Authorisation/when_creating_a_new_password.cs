﻿using System;
using Domain.Base.Encryption;
using Domain.Users.Authentication;
using Xunit.Extensions;

namespace Unit.Tests.Users.Authorisation
{
    public class when_creating_a_new_password : Specification
    {
        private string originalPassword = "q1w2e3r4t5 ?_";
        private string password;

        public override void Observe()
        {
            password = PasswordHash.CreateHash(originalPassword);
        }

        [Observation]
        public void the_hashed_password_is_not_the_same_as_the_original_password()
        {
            password.Equals(originalPassword, StringComparison.InvariantCultureIgnoreCase)
                .ShouldBeFalse();
        }

        [Observation]
        public void validating_the_hashed_password_succeeds()
        {
            PasswordHash.ValidatePassword(originalPassword, password)
                .ShouldBeTrue();
        }
    }
}