﻿using System.Linq;
using Domain.Users;
using Domain.Users.Authorisation;
using MasterData.Authorisation;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Unit.Tests.Users.Authorisation
{
    public class when_a_user_does_not_belong_to_any_groups : Specification
    {
        private readonly User user;
        private readonly EffectiveAuthorisationCalculator calculator;
        private SystemAuthorisation effectiveAuthorisation;

        public when_a_user_does_not_belong_to_any_groups()
        {
            user = UserObjectMother.UserWithNoGroups();
            calculator = new EffectiveAuthorisationCalculator();
        }

        public override void Observe()
        {
            effectiveAuthorisation = calculator.Calculate(user);
        }

        [Observation]
        public void no_authorisation_is_available()
        {
            effectiveAuthorisation.Channels.Any().ShouldBeFalse();
        }
    }
}