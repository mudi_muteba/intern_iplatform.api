using System.Collections.Generic;
using System.Linq;
using Domain.Users;
using Domain.Users.Authorisation;
using MasterData.Authorisation;
using MasterData.Authorisation.Groups;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Unit.Tests.Users.Authorisation
{
    public class when_a_user_belongs_to_multiple_groups_in_one_channel : Specification
    {
        private readonly User user;
        private readonly EffectiveAuthorisationCalculator calculator;
        private SystemAuthorisation effectiveAuthorisation;

        public when_a_user_belongs_to_multiple_groups_in_one_channel()
        {
            user = UserObjectMother.UserWithMultipleGroupsInOneChannel();
            calculator = new EffectiveAuthorisationCalculator();
        }

        public override void Observe()
        {
            effectiveAuthorisation = calculator.Calculate(user);
        }

        [Observation]
        public void authorisation_is_allocated_from_all_the_groups_for_the_channel()
        {
            effectiveAuthorisation.Channels.Any().ShouldBeTrue();

            var expectedGroups = user.AuthorisationGroups;

            foreach (var expected in expectedGroups)
            {
                var expectedAuthorisationGroup = new AllAuthorisationGroups().Find(expected.AuthorisationGroup);

                var actualChannel = effectiveAuthorisation.Channels.FirstOrDefault(ef => ef.ChannelId == expected.Channel.Id);
                actualChannel.ShouldNotBeNull();

                foreach (var expectedPoint in expectedAuthorisationGroup.Authorisation)
                {
                    var actualPoint = actualChannel.Points.FirstOrDefault(p => expectedPoint.Key == p.Key);

                    actualPoint.ShouldNotBeNull();
                }
            }
        }
    }
}