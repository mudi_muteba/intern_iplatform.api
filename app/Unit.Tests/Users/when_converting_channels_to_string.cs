using System.Collections.Generic;
using Domain.Users;
using Xunit.Extensions;

namespace Unit.Tests.Users
{
    public class when_converting_channels_to_string : Specification
    {
        public override void Observe()
        {

        }

        [Observation]
        public void should_convert()
        {
            var user = new User("", "", new List<int> { 1, 2, 3, 4, 5, 6 });
            user.ChannelIdsString.ShouldEqual("1,2,3,4,5,6");
        }
    }
}