﻿using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.ItcQuestionDefinition;
using iPlatform.Api.DTOs.ItcQuestion;
using Moq;
using TestHelper;
using TestObjects.Mothers.ItcQuestionDefinitions;
using Xunit.Extensions;

namespace Unit.Tests.ITCQuestionDefinitions
{
    public class when_deleting_an_existing_itc_question : BaseExecutionPlanTest
    {
        private readonly RemoveItcQuestionDto _itcQestionDto;
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private HandlerResult<int> _result;
        private readonly IEnumerable<Channel> _existingChannels;
        private readonly IEnumerable<ItcQuestionDefinition> _existingItcQuestionDefinitions;

        public when_deleting_an_existing_itc_question() : base(ApiUserObjectMother.AsAdmin())
        {
            _itcQestionDto = ItcQuestionDefinitionDtoObjectMother.ExistingITCQuestionToBeRemoved();
            _existingChannels = ItcQuestionDefinitionDtoObjectMother.GetExistingChannels();
            _existingItcQuestionDefinitions = ItcQuestionDefinitionDtoObjectMother.GetExistingItcQuestionDefinitions();

            _repository.Setup(r => r.GetAll<Channel>(null)).Returns(() => _existingChannels.AsQueryable());
            
            _repository.Setup(r => r.GetAll<ItcQuestionDefinition>(null))
                .Returns(() => _existingItcQuestionDefinitions.AsQueryable());

            _repository.Setup(r => r.GetById<ItcQuestionDefinition>(_itcQestionDto.Id))
                .Returns(() =>
                {
                    return _existingItcQuestionDefinitions.FirstOrDefault(i => i.Id == _itcQestionDto.Id);
                });

            Container.Register(Component.For<IRepository>().Instance(_repository.Object));
        }

        public override void Observe()
        {
            _result = ExecutionPlan.Execute<RemoveItcQuestionDto, int>(_itcQestionDto);
        }

        [Observation]
        public void the_itc_question_deletion_should_pass()
        {
            _result.Completed.ShouldBeTrue();
        }

    }
}