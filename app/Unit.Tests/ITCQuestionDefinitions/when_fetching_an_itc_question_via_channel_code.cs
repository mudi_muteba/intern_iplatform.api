﻿using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.ItcQuestionDefinition;
using Domain.ItcQuestionDefinition.Queries;
using iPlatform.Api.DTOs.ItcQuestion;
using Moq;
using TestHelper;
using TestObjects.Mothers.ItcQuestionDefinitions;
using Xunit.Extensions;

namespace Unit.Tests.ITCQuestionDefinitions
{
    public class when_fetching_an_itc_question_via_channel_code : BaseExecutionPlanTest
    {
        private readonly GetItcQuestionDto _itcQestionDto;
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private QueryResult<ItcQuestionDefinitionDto> _result;
        private readonly IEnumerable<Channel> _existingChannels;
        private readonly IEnumerable<ItcQuestionDefinition> _existingItcQuestionDefinitions;

        public when_fetching_an_itc_question_via_channel_code() : base(ApiUserObjectMother.AsAdmin())
        {
            _itcQestionDto = ItcQuestionDefinitionDtoObjectMother.GetITCQuestionWithChannelCode();
            _existingChannels = ItcQuestionDefinitionDtoObjectMother.GetExistingChannels();
            _existingItcQuestionDefinitions = ItcQuestionDefinitionDtoObjectMother.GetExistingItcQuestionDefinitions();

            _repository.Setup(r => r.GetAll<Channel>(null)).Returns(() => _existingChannels.AsQueryable());
            _repository.Setup(r => r.GetAll<ItcQuestionDefinition>(null))
                .Returns(() => _existingItcQuestionDefinitions.AsQueryable());

            Container.Register(Component.For<IRepository>().Instance(_repository.Object));
        }

        public override void Observe()
        {
            _result = ExecutionPlan
                .Query<ItcQuestionByChannelCodeQuery, ItcQuestionDefinition, ItcQuestionDefinitionDto>()
                .Configure(q => q.WithChannelCode(_itcQestionDto.ChannelName))
                .Execute();
        }

        [Observation]
        public void the_itc_question_should_complete_successfully()
        {
            _result.Completed.ShouldBeTrue();
        }
    }
}