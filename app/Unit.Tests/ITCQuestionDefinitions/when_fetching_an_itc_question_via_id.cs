﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Castle.MicroKernel.Registration;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.ItcQuestionDefinition;
using Domain.ItcQuestionDefinition.Queries;
using iPlatform.Api.DTOs.ItcQuestion;
using Moq;
using TestHelper;
using TestObjects.Mothers.ItcQuestionDefinitions;
using Xunit.Extensions;

namespace Unit.Tests.ITCQuestionDefinitions
{
    public class when_fetching_an_itc_question_via_id : BaseExecutionPlanTest
    {
        private readonly GetItcQuestionDto _itcQestionDto;
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private QueryResult<ItcQuestionDefinitionDto> _result;
        private readonly IEnumerable<Channel> _existingChannels;
        private readonly IEnumerable<ItcQuestionDefinition> _existingItcQuestionDefinitions;

        public when_fetching_an_itc_question_via_id() : base(ApiUserObjectMother.AsAdmin())
        {
            _itcQestionDto = ItcQuestionDefinitionDtoObjectMother.GetITCQuestionWithId();
            _existingChannels = ItcQuestionDefinitionDtoObjectMother.GetExistingChannels();
            _existingItcQuestionDefinitions = ItcQuestionDefinitionDtoObjectMother.GetExistingItcQuestionDefinitions();

            _repository.Setup(r => r.GetAll<Channel>(null)).Returns(() => _existingChannels.AsQueryable());
            _repository.Setup(r => r.GetAll<ItcQuestionDefinition>(null))
                .Returns(() => _existingItcQuestionDefinitions.AsQueryable());

            Container.Register(Component.For<IRepository>().Instance(_repository.Object));
        }

        public override void Observe()
        {
            _result = ExecutionPlan
                .Query<ItcQuestionByIdQuery, ItcQuestionDefinition, ItcQuestionDefinitionDto>()
                .Configure(q =>
                {
                    if (_itcQestionDto.Id != null) q.WithCriteria(_itcQestionDto.Id.Value);
                })
                .OnSuccess(r => Mapper.Map<ItcQuestionDefinition, ItcQuestionDefinitionDto>(r.FirstOrDefault()))
                .Execute();
        }

        [Observation]
        public void the_itc_question_fetch_should_successfully()
        {
            _result.Completed.ShouldBeTrue();
        }
    }
}