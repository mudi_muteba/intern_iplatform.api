﻿using AutoMapper;
using Domain.Party.Assets;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using TestHelper.Installers;
using Xunit.Extensions;

namespace Unit.Tests.Assets
{
    public class Asset_Mapping_Test : Specification
    {
        public Asset_Mapping_Test()
        {
            DomainMarkerMapperConfiguration.Configure();
        }

        public override void Observe() { }

        [Observation]
        public void should_map()
        {
            var dto = new AssetExtrasDto();
            Mapper.Map<AssetExtrasDto, AssetExtras>(dto).ShouldNotBeNull();
        }
    }
}
