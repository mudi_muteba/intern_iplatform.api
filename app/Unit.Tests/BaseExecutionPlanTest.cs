﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Infrastructure;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Domain.Base.Execution;
using Domain.Users;
using EasyNetQ;
using Moq;
using TestHelper;
using Xunit.Extensions;

namespace Unit.Tests
{
    public abstract class BaseExecutionPlanTest : Specification, IDisposable
    {
        protected readonly IExecutionPlan ExecutionPlan;
        protected readonly WindsorContainer Container;

        protected BaseExecutionPlanTest(ApiUser user)
        {
            Container = new WindsorContainer();
            Container.Register(Component.For<IBus>().Instance(new Mock<IBus>().Object).Named("RouterBus"));
            Container.Install(new TestWindsorInstallerCollection().Installers);
            
            Container.BeginScope();

            var executionPlan = Container.Resolve<IExecutionPlan>();
            var contextProvider = Container.Resolve<IProvideContext>();
            var createContext = Container.Resolve<ICreateContext>();
            Container.Release(ExecutionPlan);
            Container.Release(contextProvider);
            Container.Release(createContext);
            contextProvider.SetContext(createContext.Create(string.Empty, user, null, Guid.NewGuid(), user.Channels.ToList(), user.Groups));
            
            ExecutionPlan = executionPlan;
        }

        public void Dispose()
        {
            Container.Dispose();
        }
    }
}