using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using TestHelper.Helpers.Extensions;
using TestObjects.Mothers.Ratings;
using ValidationMessages;
using ValidationMessages.Ratings;
using Xunit.Extensions;

namespace Unit.Tests.Ratings
{
    public class when_validating_a_rating_request_with_items_with_answers_with_empty_persons : Specification
    {
        private readonly RatingRequestDto ratingRequest;
        private List<ValidationErrorMessage> validationErrors;

        public when_validating_a_rating_request_with_items_with_answers_with_empty_persons()
        {
            ratingRequest = RatingRequestObjectMother.WithItemsWithAnswersAndEmptyPersons();
        }

        public override void Observe()
        {
            validationErrors = ratingRequest.Validate();
        }

        [Observation]
        public void validation_errors_are_returned()
        {
            validationErrors.Count.ShouldEqual(4);
        }

        [Observation]
        public void the_correct_error_messages_are_returned()
        {
            var expectedMessages = new List<ValidationErrorMessage>()
            {
                RatingRequestValidationMessages.InvalidPerson,
                RatingRequestValidationMessages.InvalidPerson,
                RatingRequestValidationMessages.NoAddresses,
                RatingRequestValidationMessages.NoAddresses,
            };

            validationErrors.IsEqual<ValidationErrorMessage, ValidationErrorMessage>(expectedMessages,
                (message, errorMessage) => message.Equals(errorMessage)).ShouldBeTrue();
        }
    }
}