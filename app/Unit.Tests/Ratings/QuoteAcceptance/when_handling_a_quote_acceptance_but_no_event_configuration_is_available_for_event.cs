using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Admin.Queries;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;
using Domain.Base.Workflow;
using Domain.Ratings.Handlers;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData.Authorisation;
using MasterData.Authorisation.Groups;
using Moq;
using Shared;
using TestObjects.Mothers.Context;
using TestObjects.Mothers.Ratings;
using Xunit.Extensions;
using Domain.Admin.SettingsQuoteUploads.Queries;
using Domain.Admin.SettingsQuoteUploads;
using Domain.Admin.ChannelEvents;

namespace Unit.Tests.Ratings.QuoteAcceptance
{
    public class when_handling_a_quote_acceptance_but_no_event_configuration_is_available_for_event : Specification
    {
        private readonly IProvideContext contextProvider = new DefaultContextProvider();
        private readonly PublishQuoteUploadMessageDtoHandler handler;
        private readonly Mock<IRepository> repository = new Mock<IRepository>();
        private GetChannelEventConfigurationQuery channelEventQuery;
        private GetChannelBySystemIdQuery channelQuery;
        private GetSettingsQuoteAcceptanceQuery settingsQuoteAcceptanceQuery;
        private Exception actualException;
        private PublishQuoteUploadMessageDto _quote;
        private readonly HandlerResult<QuoteAcceptanceResponseDto> result = new HandlerResult<QuoteAcceptanceResponseDto>();
        private readonly Mock<IPublishEvents> eventPublisher = new Mock<IPublishEvents>();

        public when_handling_a_quote_acceptance_but_no_event_configuration_is_available_for_event()
        {
            SetContext();

            handler = new PublishQuoteUploadMessageDtoHandler(contextProvider, channelQuery, channelEventQuery, settingsQuoteAcceptanceQuery, eventPublisher.Object);
        }

        public override void Observe()
        {
            try
            {
                handler.Handle(_quote, result);
            }
            catch (ValidationException e)
            {
                actualException = e;
            }
        }

        [Observation]
        public void a_validation_exception_is_thrown()
        {
            actualException.ShouldNotBeNull();
        }

        private void SetContext()
        {
            _quote = QuoteAcceptanceDtoObjectMother.ValidQuoteAcceptance();

            var context = new ContextObjectMother()
                .WithAuthorisation(new ChannelAuthorisation(1, new CallCentreAgent().Authorisation.ToArray()))
                .WithAuthorisation(new ChannelAuthorisation(2, new CallCentreAgent().Authorisation.ToArray()))
                .Build();

            contextProvider.SetContext(context);

            SetupQueries();
        }

        private void SetupQueries()
        {
            var channelEventForAnotherEvent = new ChannelEvent(new Channel(1), "AnotherEventName");

            repository.Setup(r => r.GetAll<ChannelEvent>(It.IsAny<ExecutionContext>()))
                .Returns(() =>
                {
                    return new List<ChannelEvent>() { channelEventForAnotherEvent }.AsQueryable();
                });

            var channel = new Channel(1, true, _quote.Request.RatingContext.ChannelId, SystemTime.Now(), null, true);

            repository.Setup(r => r.GetAll<Channel>(It.IsAny<ExecutionContext>()))
                .Returns(() =>
                {
                    return new List<Channel>() { channel }.AsQueryable();
                });

            repository.Setup(r => r.GetAll<SettingsQuoteUpload>(It.IsAny<ExecutionContext>())).Returns(() => { return QuoteAcceptanceConfigurationMother.DotsureEntityConfig().AsQueryable(); });

            channelEventQuery = new GetChannelEventConfigurationQuery(contextProvider, repository.Object);

            channelQuery = new GetChannelBySystemIdQuery(contextProvider, repository.Object);
            settingsQuoteAcceptanceQuery = new GetSettingsQuoteAcceptanceQuery(contextProvider, repository.Object);

        }
    }
}