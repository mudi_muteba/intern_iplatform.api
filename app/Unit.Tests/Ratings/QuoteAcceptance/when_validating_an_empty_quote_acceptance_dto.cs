﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Quoting;
using TestHelper.Helpers.Extensions;
using ValidationMessages;
using ValidationMessages.Ratings;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.QuoteAcceptance
{
    public class when_validating_an_empty_quote_acceptance_dto : Specification
    {
        private readonly PublishQuoteUploadMessageDto quoteDto;
        private List<ValidationErrorMessage> validation;

        public when_validating_an_empty_quote_acceptance_dto()
        {
            quoteDto = new PublishQuoteUploadMessageDto();
        }

        public override void Observe()
        {
            validation = quoteDto.Validate();
        }

        [Observation]
        public void validation_errors_are_returned()
        {
            validation.Count.ShouldEqual(6);
        }

        [Observation]
        public void the_correct_error_messages_are_returned()
        {
            var expectedMessages = new List<ValidationErrorMessage>()
            {
                QuoteAcceptanceValidationMessages.NoPolicyAccepted,
                QuoteAcceptanceValidationMessages.InvalidInsured,
                RatingRequestValidationMessages.NoItemsAddedToRequest,
                RatingRequestValidationMessages.NoPersons,
                RatingRequestValidationMessages.NoRatingReference,
                RatingRequestValidationMessages.NoChannelProvided,
            };

            validation.IsEqual<ValidationErrorMessage, ValidationErrorMessage>(expectedMessages,
                (message, errorMessage) => message.Equals(errorMessage)).ShouldBeTrue();
        }
    }
}