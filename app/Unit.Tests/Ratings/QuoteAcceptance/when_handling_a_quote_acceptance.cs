using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Domain.Admin;
using Domain.Admin.Queries;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Leads;
using Domain.Ratings.Handlers;
using EasyNetQ;
using iPlatform.Api.DTOs.Ratings.Quoting;
using MasterData.Authorisation;
using MasterData.Authorisation.Groups;
using Moq;
using Shared;
using TestHelper;
using TestHelper.Stubs;
using TestObjects.Mothers.Channels;
using TestObjects.Mothers.Context;
using TestObjects.Mothers.Ratings;
using Workflow.Messages;
using Xunit.Extensions;
using Domain.Admin.SettingsQuoteUploads.Queries;
using Domain.Admin.SettingsQuoteUploads;
using Domain.Admin.ChannelEvents;

namespace Unit.Tests.Ratings.QuoteAcceptance
{
    public class when_handling_a_quote_acceptance : BaseExecutionPlanTest
    {
        private readonly List<ChannelEvent> channelEventConfiguration =
            ChannelEventObjectMother.ValidChannelProductEvents().Where(ce => ce.Channel.Id == 1).ToList();

        private readonly IProvideContext contextProvider = new DefaultContextProvider();
        private readonly PublishQuoteUploadMessageDtoHandler handler;
        private readonly Mock<IRepository> repository = new Mock<IRepository>();

        private readonly HandlerResult<QuoteAcceptanceResponseDto> result =
            new HandlerResult<QuoteAcceptanceResponseDto>();

        private readonly FakeBus bus = new FakeBus();
        private GetChannelEventConfigurationQuery channelEventQuery;
        private GetChannelBySystemIdQuery channelQuery;
        private GetSettingsQuoteAcceptanceQuery settingsQuoteAcceptanceQuery;
        private PublishQuoteUploadMessageDto _quote;
        private IPublishEvents eventPublisher;

        public when_handling_a_quote_acceptance() : base(ApiUserObjectMother.AsAdmin())
        {
            SetContext();

            Container.Register(Component.For<IBus>().Instance(bus));
            eventPublisher = new EventWorkflowMessagePublisher(new WorkflowRouter(new WorkflowBus(bus)));
            handler = new PublishQuoteUploadMessageDtoHandler(contextProvider, channelQuery, channelEventQuery, settingsQuoteAcceptanceQuery, eventPublisher);
        }

        public override void Observe()
        {
            handler.Handle(_quote, result);
        }

        [Observation]
        public void the_quote_acceptance_tasks_are_published_to_the_workflow()
        {
            bus.PublishWasCalled.ShouldBeTrue();

            var publishedMessage = bus.MessagesPublished.First().Value.FirstOrDefault();

            publishedMessage.ShouldNotBeNull();
            
            publishedMessage.ExecutionPlan.ExecutionMessages.Any(t => t is QuoteUploadMessage)
                .ShouldBeTrue();
        }

        [Observation]
        public void a_quote_reference_is_available()
        {
            result.Response.ShouldNotBeNull();
            result.Response.QuoteReference.ShouldNotEqual(Guid.Empty);
        }

        private void SetContext()
        {
            _quote = QuoteAcceptanceDtoObjectMother.ValidQuoteAcceptance();

            var baseAuthorisation = new CallCentreAgent().Authorisation;
            baseAuthorisation.Remove(SecondLevelUnderwritingAuthorisation.CanAcceptQuote);

            var context = new ContextObjectMother()
                .WithAuthorisation(new ChannelAuthorisation(1, new CallCentreAgent().Authorisation.ToArray()))
                .WithAuthorisation(new ChannelAuthorisation(2, new CallCentreAgent().Authorisation.ToArray()))
                .Build();

            contextProvider.SetContext(context);

            SetupQueries();
        }

        private void SetupQueries()
        {
            repository.Setup(r => r.GetAll<ChannelEvent>(It.IsAny<ExecutionContext>())).Returns(() => { return channelEventConfiguration.AsQueryable(); });

            var channel = new Channel(1, true, _quote.Request.RatingContext.ChannelId, SystemTime.Now(), null, true);

            repository.Setup(r => r.GetAll<Channel>(It.IsAny<ExecutionContext>())).Returns(() => { return new List<Channel>() {channel}.AsQueryable(); });

            repository.Setup(r => r.GetAll<SettingsQuoteUpload>(It.IsAny<ExecutionContext>())).Returns(() => { return QuoteAcceptanceConfigurationMother.DotsureEntityConfig().AsQueryable(); });

            channelEventQuery = new GetChannelEventConfigurationQuery(contextProvider, repository.Object);

            channelQuery = new GetChannelBySystemIdQuery(contextProvider, repository.Object);

            settingsQuoteAcceptanceQuery = new GetSettingsQuoteAcceptanceQuery(contextProvider, repository.Object);
        }
    }
}