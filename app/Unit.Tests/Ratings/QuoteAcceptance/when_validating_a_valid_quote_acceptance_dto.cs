﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Quoting;
using TestObjects.Mothers.Ratings;
using ValidationMessages;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.QuoteAcceptance
{
    public class when_validating_a_valid_quote_acceptance_dto : Specification
    {
        private readonly PublishQuoteUploadMessageDto quoteDto;
        private List<ValidationErrorMessage> validation;

        public when_validating_a_valid_quote_acceptance_dto()
        {
            quoteDto = QuoteAcceptanceDtoObjectMother.ValidQuoteAcceptance();
        }

        public override void Observe()
        {
            validation = quoteDto.Validate();
        }

        [Observation]
        public void no_validation_errors_are_returned()
        {
            validation.Count.ShouldEqual(0);
        }
    }
}