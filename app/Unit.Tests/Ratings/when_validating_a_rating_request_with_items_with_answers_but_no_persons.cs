using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using TestHelper.Helpers.Extensions;
using TestObjects.Mothers.Ratings;
using TestObjects.Mothers.Users;
using ValidationMessages;
using ValidationMessages.Ratings;
using Xunit.Extensions;

namespace Unit.Tests.Ratings
{
    public class when_validating_a_rating_request_with_items_with_answers_but_no_persons : Specification
    {
        private readonly RatingRequestDto ratingRequest;
        private List<ValidationErrorMessage> validationErrors;

        public when_validating_a_rating_request_with_items_with_answers_but_no_persons()
        {
            ratingRequest = RatingRequestObjectMother.WithItemsWithAnswersNoPersons();
        }

        public override void Observe()
        {
            validationErrors = ratingRequest.Validate();
        }

        [Observation]
        public void validation_errors_are_returned()
        {
            validationErrors.Count.ShouldEqual(1);
        }

        [Observation]
        public void the_correct_error_messages_are_returned()
        {
            var expectedMessages = new List<ValidationErrorMessage>()
            {
                RatingRequestValidationMessages.NoPersons,
            };

            validationErrors.IsEqual<ValidationErrorMessage, ValidationErrorMessage>(expectedMessages,
                (message, errorMessage) => message.Equals(errorMessage)).ShouldBeTrue();
        }
    }
}