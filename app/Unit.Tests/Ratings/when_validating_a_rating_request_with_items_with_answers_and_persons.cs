using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using TestObjects.Mothers.Ratings;
using ValidationMessages;
using Xunit.Extensions;

namespace Unit.Tests.Ratings
{
    public class when_validating_a_rating_request_with_items_with_answers_and_persons : Specification
    {
        private readonly RatingRequestDto ratingRequest;
        private List<ValidationErrorMessage> validationErrors;

        public when_validating_a_rating_request_with_items_with_answers_and_persons()
        {
            ratingRequest = RatingRequestObjectMother.WithItemsWithAnswersAndValidPersons();
        }

        public override void Observe()
        {
            validationErrors = ratingRequest.Validate();
        }

        [Observation]
        public void no_validation_errors_are_returned()
        {
            validationErrors.Count.ShouldEqual(0);
        }
    }
}