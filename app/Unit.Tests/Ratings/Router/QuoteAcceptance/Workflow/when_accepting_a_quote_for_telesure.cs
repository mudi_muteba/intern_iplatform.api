﻿using System.Linq;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Leads;
using TestObjects.Builders;
using TestObjects.Mothers.Router;
using Workflow.Messages;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.Router.QuoteAcceptance.Workflow
{
    public class when_accepting_a_quote_for_telesure : Specification
    {
        private readonly ICreateWorkflowRoutingMessage<RouteAcceptedQuoteTask> _routeFactory;
        private readonly RouteAcceptedQuoteTask _task;
        private IWorkflowRoutingMessage _message;


        public when_accepting_a_quote_for_telesure()
        {
            _task = RouteAcceptedQuoteTaskMother.ForTelesure();
            _routeFactory = QuoteAcceptanceRouterFactoryBuilder.CreateWorkflowRRouteAcceptedQuoteTask();
        }

        public override void Observe()
        {
            _message = _routeFactory.Create(_task);
        }

        [Observation]
        public void then_messages_should_exist()
        {
            _message.ShouldNotBeNull();
            _message.ExecutionPlan.ShouldNotBeNull();
        }

        [Observation]
        public void then_message_should_be_of_correct_type()
        {
            _message.ExecutionPlan.ExecutionMessages.FirstOrDefault().ShouldBeType<QuoteUploadMessage>();
        }
    }
}
