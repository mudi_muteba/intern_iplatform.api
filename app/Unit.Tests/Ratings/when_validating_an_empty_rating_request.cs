﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using TestHelper.Helpers.Extensions;
using ValidationMessages;
using ValidationMessages.Ratings;
using Xunit.Extensions;

namespace Unit.Tests.Ratings
{
    public class when_validating_an_empty_rating_request : Specification
    {
        private readonly RatingRequestDto ratingRequest;
        private List<ValidationErrorMessage> validationErrors;

        public when_validating_an_empty_rating_request()
        {
            ratingRequest = new RatingRequestDto();
        }

        public override void Observe()
        {
            validationErrors = ratingRequest.Validate();
        }

        [Observation]
        public void validation_errors_are_returned()
        {
            validationErrors.Count.ShouldEqual(4);
        }

        [Observation]
        public void the_correct_error_messages_are_returned()
        {
            var expectedMessages = new List<ValidationErrorMessage>()
            {
                RatingRequestValidationMessages.NoRatingReference,
                RatingRequestValidationMessages.NoItemsAddedToRequest,
                RatingRequestValidationMessages.NoPersons,
                RatingRequestValidationMessages.NoChannelProvided,
            };

            validationErrors.IsEqual<ValidationErrorMessage, ValidationErrorMessage>(expectedMessages,
                (message, errorMessage) => message.Equals(errorMessage)).ShouldBeTrue();
        }
    }
}