using System;
using Domain.Ratings.Engines.iRate;
using iPlatform.Api.DTOs.Ratings.Request;
using Moq;
using TestObjects.Mothers.Ratings;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.Engines.iRate
{
    public class when_calling_irate_and_external_call_fails : Specification
    {
        private readonly Mock<IIRateProvider> provider = new Mock<IIRateProvider>();
        private readonly RatingEngine ratingEngine;
        private readonly RatingRequestDto request;
        private Exception actualException;

        public when_calling_irate_and_external_call_fails()
        {
            provider.Setup(p => p.GetRates(It.IsAny<RatingRequestDto>())).Throws<Exception>();

            request = RatingRequestObjectMother.WithItemsWithAnswersAndValidPersons();
            ratingEngine = new RatingEngine(provider.Object);
        }

        public override void Observe()
        {
            try
            {
                ratingEngine.PerformRating(request);
            }
            catch (Exception e)
            {
                actualException = e;
            }
        }

        [Observation]
        public void the_failure_is_bubbled_up()
        {
            actualException.ShouldNotBeNull();
            actualException.GetType().ShouldEqual(typeof (Exception));
        }
    }
}