﻿using System.Collections.Generic;
using Domain.Ratings;
using Domain.Ratings.Engines.iRate;
using iPlatform.Api.DTOs.Ratings.Request;
using Moq;
using TestHelper.Helpers.Extensions;
using TestObjects.Mothers.Ratings;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.Engines.iRate
{
    public class when_calling_irate : Specification
    {
        private readonly List<RatingConfiguration> ratingConfiguration;
        private readonly RatingEngine ratingEngine;
        private readonly RatingRequestDto request;
        private readonly Mock<IIRateProvider> provider = new Mock<IIRateProvider>();
        private RatingRequestDto actualRatingRequest;

        public when_calling_irate()
        {
            request = RatingRequestObjectMother.WithItemsWithAnswersAndValidPersons();
            ratingConfiguration = RatingConfigurationObjectMother.ValidRatingSettings();
            provider.Setup(p => p.GetRates(It.IsAny<RatingRequestDto>()))
                .Callback((RatingRequestDto dto) => actualRatingRequest = dto);

            ratingEngine = new RatingEngine(provider.Object);
        }

        public override void Observe()
        {
            ratingEngine.PerformRating(request);
        }

        [Observation]
        public void the_irate_provider_is_called()
        {
            provider.Verify(p => p.GetRates(It.IsAny<RatingRequestDto>()));
        }

        [Observation]
        public void the_rating_request_is_provided()
        {
            actualRatingRequest.ShouldNotBeNull();
        }

    }
}