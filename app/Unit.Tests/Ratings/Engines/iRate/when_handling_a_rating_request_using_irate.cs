﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Ratings;
using Domain.Ratings.Engines;
using Domain.Ratings.Engines.RatingRequestProcessors;
using Domain.Ratings.Handlers;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData.Authorisation;
using MasterData.Authorisation.Groups;
using Moq;
using TestHelper.Helpers.Extensions;
using TestObjects.Mothers.Context;
using TestObjects.Mothers.Ratings;
using Unit.Tests.Ratings.Engines.iRate.stubs;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.Engines.iRate
{
    public class when_handling_a_rating_request_using_irate : Specification
    {
        private readonly IProvideContext contextProvider = new DefaultContextProvider();

        private readonly List<RatingConfiguration> expectedRatingConfiguration =
            RatingConfigurationObjectMother.ValidRatingSettings().Where(c => c.ChannelId == 1).ToList();

        private readonly RatingRequestDtoHandler _dtoHandler;
        private readonly TestResponseProcessor1 processor1 = new TestResponseProcessor1();
        private readonly TestResponseProcessor2 processor2 = new TestResponseProcessor2();
        private readonly Mock<IRatingEngine> ratingEngine = new Mock<IRatingEngine>();
        private readonly Mock<IRepository> repository = new Mock<IRepository>();
        private readonly RatingRequestDto request;
        private List<IProcessRatingResult> responseProcessors;
        private readonly HandlerResult<RatingResultDto> result = new HandlerResult<RatingResultDto>();
        private GetRatingConfigurationByChannelQuery query;
        private GetRatingUserConfigurationByUser userQuery;
        private RatingRequestDto actualRatingRequest;

        public when_handling_a_rating_request_using_irate()
        {
            SetupContext();

            ratingEngine.Setup(
                re => re.PerformRating(It.IsAny<RatingRequestDto>()))
                .Callback((RatingRequestDto r) =>
                    {
                        actualRatingRequest = r;
                    })
                .Returns(() => new RatingResultDto());

            request = RatingRequestObjectMother.WithItemsWithAnswersAndValidPersons();
            _dtoHandler = new RatingRequestDtoHandler(contextProvider, ratingEngine.Object, query, responseProcessors, userQuery);
        }

        public override void Observe()
        {
            _dtoHandler.Handle(request, result);
        }

        [Observation]
        public void irate_is_invoked()
        {
            ratingEngine.Verify(re => re.PerformRating(It.IsAny<RatingRequestDto>()));
        }

        [Observation]
        public void the_response_from_irate_is_returned_in_the_result()
        {
            result.Response.ShouldNotBeNull();
        }

        [Observation]
        public void the_rating_response_is_processed()
        {
            processor1.WasCalled.ShouldBeTrue();
            processor2.WasCalled.ShouldBeTrue();
        }

        [Observation]
        public void the_context_for_the_rating_request_is_set()
        {
            actualRatingRequest.RatingContext.ShouldNotBeNull();
            actualRatingRequest.RatingContext.RatingEngines.Count.ShouldEqual(expectedRatingConfiguration.Count);

            var ratingEngines = actualRatingRequest.RatingContext.RatingEngines;

            ratingEngines.IsEqual(expectedRatingConfiguration, (dto, configuration) =>
            {
                return dto.ProductCode.Equals(configuration.ProductCode)
                       && dto.InsurerCode.Equals(configuration.InsurerCode)
                       && dto.AgentCode.Equals(configuration.RatingAgentCode)
                       && dto.AuthCode.Equals(configuration.RatingAuthCode)
                       && dto.BrokerCode.Equals(configuration.RatingBrokerCode)
                       && dto.Password.Equals(configuration.RatingPassword)
                       && dto.SchemeCode.Equals(configuration.RatingSchemeCode)
                       && dto.SubscriberCode.Equals(configuration.RatingSubscriberCode)
                       && dto.Token.Equals(configuration.RatingToken)
                       && dto.UserName.Equals(configuration.RatingUserId)
                       && dto.Environment.Equals(configuration.RatingEnvironment)
                       && dto.CompanyCode.Equals(configuration.RatingCompanyCode)
                       && dto.UwCompanyCode.Equals(configuration.RatingUwCompanyCode)
                       && dto.UwProductCode.Equals(configuration.RatingUwProductCode)

                       && dto.ITCSettings != null
                       && dto.ITCSettings.CheckRequired.Equals(configuration.ITCCheckRequired)
                       && dto.ITCSettings.SubscriberCode.Equals(configuration.ITCSubscriberCode)
                       && dto.ITCSettings.BranchNumber.Equals(configuration.ITCBranchNumber)
                       && dto.ITCSettings.BatchNumber.Equals(configuration.ITCBatchNumber)
                       && dto.ITCSettings.SecurityCode.Equals(configuration.ITCSecurityCode)
                       && dto.ITCSettings.EnquirerContactName.Equals(configuration.ITCEnquirerContactName)
                       && dto.ITCSettings.EnquirerContactPhoneNo.Equals(configuration.ITCEnquirerContactPhoneNo)
                       && dto.ITCSettings.Environment.Equals(configuration.ITCEnvironment)
                    ;
            });
        }

        private void SetupContext()
        {
            var context = new ContextObjectMother()
                .WithAuthorisation(new ChannelAuthorisation(1, new CallCentreAgent().Authorisation.ToArray()))
                .Build();

            contextProvider.SetContext(context);

            repository.Setup(r => r.GetAll<RatingConfiguration>(It.IsAny<ExecutionContext>()))
                .Returns(() => { return expectedRatingConfiguration.AsQueryable(); });

            query = new GetRatingConfigurationByChannelQuery(contextProvider, repository.Object);
            userQuery = new GetRatingUserConfigurationByUser(contextProvider, repository.Object);

            responseProcessors = new List<IProcessRatingResult>()
            {
                processor1, processor2
            };
        }
    }
}