﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;
using Domain.Ratings;
using Domain.Ratings.Engines;
using Domain.Ratings.Engines.RatingRequestProcessors;
using Domain.Ratings.Handlers;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData.Authorisation;
using MasterData.Authorisation.Groups;
using Moq;
using TestObjects.Mothers.Context;
using TestObjects.Mothers.Ratings;
using Unit.Tests.Ratings.Engines.iRate.stubs;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.Engines.iRate
{
    public class when_handling_a_rating_request_using_irate_and_user_does_not_have_authorisation : Specification
    {
        private readonly TestResponseProcessor1 processor1 = new TestResponseProcessor1();
        private readonly TestResponseProcessor2 processor2 = new TestResponseProcessor2();
        private readonly IProvideContext contextProvider = new DefaultContextProvider();
        private readonly RatingRequestDtoHandler _dtoHandler;
        private readonly Mock<IRatingEngine> ratingEngine = new Mock<IRatingEngine>();
        private readonly RatingRequestDto request;
        private readonly HandlerResult<RatingResultDto> result = new HandlerResult<RatingResultDto>();
        private readonly Mock<IRepository> repository = new Mock<IRepository>();
        private UnauthorisationException actualException;

        public when_handling_a_rating_request_using_irate_and_user_does_not_have_authorisation()
        {
            var responseProcessors = new List<IProcessRatingResult>()
            {
                processor1, processor2
            };

            var baseAuthorisation = new CallCentreAgent().Authorisation;
            baseAuthorisation.Remove(QuotingAuthorisation.CanQuote);

            var context = new ContextObjectMother()
                .WithAuthorisation(new ChannelAuthorisation(1, baseAuthorisation.ToArray()))
                .Build();

            contextProvider.SetContext(context);

            repository.Setup(r => r.GetAll<RatingConfiguration>(It.IsAny<ExecutionContext>())).Returns(() =>
            {
                return RatingConfigurationObjectMother.ValidRatingSettings().AsQueryable();
            });

            var query = new GetRatingConfigurationByChannelQuery(contextProvider, repository.Object);
            var userQuery = new GetRatingUserConfigurationByUser(contextProvider, repository.Object);

            request = RatingRequestObjectMother.WithItemsWithAnswersAndValidPersons();
            _dtoHandler = new RatingRequestDtoHandler(contextProvider, ratingEngine.Object, query, responseProcessors, userQuery);
        }

        public override void Observe()
        {
            try
            {
                _dtoHandler.Handle(request, result);
            }
            catch (UnauthorisationException e)
            {
                actualException = e;
            }
        }

        [Observation]
        public void an_unauthorised_exception_is_thrown()
        {
            actualException.ShouldNotBeNull();
        }

        [Observation]
        public void rating_response_processors_are_not_invoked()
        {
            processor1.WasCalled.ShouldBeFalse();
            processor2.WasCalled.ShouldBeFalse();
        }

    }
}