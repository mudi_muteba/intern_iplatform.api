﻿using Domain.Ratings;
using Domain.Ratings.Engines.RatingRequestProcessors;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace Unit.Tests.Ratings.Engines.iRate.stubs
{
    public class TestResponseProcessor2 : IProcessRatingResult
    {
        public bool WasCalled { get; private set; }

        public void Process(RatingRequestDto request, RatingResultDto result, RatingConfiguration ratingConfiguration)
        {
            WasCalled = true;
        }
    }
}