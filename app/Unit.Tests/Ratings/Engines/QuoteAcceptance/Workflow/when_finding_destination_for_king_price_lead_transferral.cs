﻿using System.Collections.Generic;
using System.Linq;
using Domain.QuoteAcceptance.Leads;
using Moq;
using TestObjects.Builders;
using TestObjects.Mothers.Router;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Transferer;
using Workflow.QuoteAcceptance.Transferer.Factories;
using Workflow.QuoteAcceptance.Transferer.Leads;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.Engines.QuoteAcceptance.Workflow
{
    public class when_finding_destination_for_king_price_lead_transferral : Specification
    {
        private readonly ICreateTransferOfLead<QuoteUploadMessage, WorkflowEngine> _finder;
        private readonly IWorkflowExecutionMessage _message;
        private readonly IConfigureTransferOfAcceptedQuote _configuration = new TransferLeadConfiguration();

        private readonly Mock<IWorkflowRouter> _router;
        private readonly Mock<IWorkflowExecutor> _executor;
        private IEnumerable<ITransferLead> _leadDestination;

        public when_finding_destination_for_king_price_lead_transferral()
        {
            _message = WorkflowExecutionMessageBuilder.ForLeadTransferralMessage(RouteAcceptedQuoteTaskMother.ForKingPrice());
            _finder = new CreateLeadTransferDestinationFactory(_configuration);
            _router = new Mock<IWorkflowRouter>();
            _executor = new Mock<IWorkflowExecutor>();
        }

        public override void Observe()
        {
            _leadDestination = _finder.Create((QuoteUploadMessage) _message, new WorkflowEngine(_router.Object, _executor.Object));
        }

        [Observation(Skip ="need to be fixed")]
        public void then_lead_transfer_to_insurer_should_exist()
        {
            _leadDestination.ToList().ForEach(f =>
            {
                f.ShouldNotBeNull();
                f.ShouldBeType<TransferLeadToKingPrice>();
                f.ShouldBeType<TransferLeadToKingPrice>().Result.ShouldBeNull();
            });
        }
    }
}