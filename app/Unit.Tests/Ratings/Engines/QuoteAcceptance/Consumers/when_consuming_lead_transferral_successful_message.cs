﻿using System;
using Domain.QuoteAcceptance.Infrastructure.Factories;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Monitoring;
using iPlatform.Api.DTOs.Monitoring;
using Moq;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;
using Workflow.QuoteAcceptance.Domain.Consumers;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.Engines.QuoteAcceptance.Consumers
{
    public class when_consuming_lead_transferral_successful_message : Specification
    {
        private SuccessfulLeadTransferConsumer _consumer;
        private readonly Mock<IWorkflowRouter> _router;
        private readonly Mock<IWorkflowExecutor> _executor;
        private readonly IMonitorLeadTransfer _monitorLeadTransfer;
        private readonly Mock<IDispatchLeadTransferMessage> _dispatcher;
        private readonly IBuildMonitoringDto<IWorkflowExecutionMessage> _monitoringDto;

        private IWorkflowExecutionMessage _message;
        private ITaskMetadata _metadata;

        public when_consuming_lead_transferral_successful_message()
        {
            _monitoringDto = new ConsumerWorkflowMonitoringFactory();
            _dispatcher = new Mock<IDispatchLeadTransferMessage>();
            _router = new Mock<IWorkflowRouter>();
            _executor = new Mock<IWorkflowExecutor>();
            _monitorLeadTransfer = new MonitorLeadTransfer(_dispatcher.Object);

        }

        public override void Observe()
        {
            _metadata = new LeadTransferSuccessfulMetadata("Its started", Guid.Empty );
            _message = new LeadTransferSuccessful(_metadata);

            _consumer = new SuccessfulLeadTransferConsumer(_router.Object, _executor.Object, _monitorLeadTransfer, _monitoringDto);
            _consumer.Consume(_message);
        }

        [Observation]
        public void then_successful_message_should_be_sent_monitoring()
        {
            _dispatcher.Verify(p => p.Dispatch(It.IsAny<MonitoringDto>()));
        }
    }
}