﻿using System;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Infrastructure.Factories;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Monitoring;
using iPlatform.Api.DTOs.Monitoring;
using Moq;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;
using Workflow.QuoteAcceptance.Domain.Consumers;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.Engines.QuoteAcceptance.Consumers
{
    public class when_consuming_lead_transferral_started_message : Specification
    {
        private readonly Mock<IWorkflowRouter> _router;
        private readonly Mock<IWorkflowExecutor> _executor;
        private readonly IMonitorLeadTransfer _monitorLeadTransfer;
        private readonly Mock<IDispatchLeadTransferMessage> _dispatcher;
        private readonly IBuildMonitoringDto<IWorkflowExecutionMessage> _monitoringDto;

        private IWorkflowExecutionMessage _message;
        private ITaskMetadata _metadata;

        public when_consuming_lead_transferral_started_message()
        {
            _monitoringDto = new ConsumerWorkflowMonitoringFactory();
            _dispatcher = new Mock<IDispatchLeadTransferMessage>();
            _router = new Mock<IWorkflowRouter>();
            _executor = new Mock<IWorkflowExecutor>();
            _monitorLeadTransfer = new MonitorLeadTransfer(_dispatcher.Object);

        }

        public override void Observe()
        {
        
        }

        [Observation]
        public void then_started_king_price_message_should_be_sent_monitoring()
        {
            _metadata = new LeadTransferKingPriceTaskMetadata("1","2","3",Guid.Empty);
            _message = LeadTransferStarted.WithChannelId("Started transferring lead for King Price", Guid.Empty,((LeadTransferKingPriceTaskMetadata)_metadata).ToString());

            var consumer = new StartedLeadTransferConsumer(_router.Object, _executor.Object, _monitorLeadTransfer, _monitoringDto);
            consumer.Consume(_message);

            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeNull();
            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeEmpty();

            _dispatcher.Verify(p => p.Dispatch(It.IsAny<MonitoringDto>()));
        }

        [Observation]
        public void then_started_hollard_message_should_be_sent_monitoring()
        {
            _metadata = new LeadTransferHollardTaskMetadata("1", "2", "3", "4", "5", "6", "7", "8", "9", Guid.Empty);
            _message = LeadTransferStarted.WithChannelId("Started transferring lead for Hollard", Guid.Empty, ((LeadTransferHollardTaskMetadata)_metadata).ToString());

            var consumer = new StartedLeadTransferConsumer(_router.Object, _executor.Object, _monitorLeadTransfer, _monitoringDto);
            consumer.Consume(_message);

            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeNull();
            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeEmpty();

            _dispatcher.Verify(p => p.Dispatch(It.IsAny<MonitoringDto>()));
        }

        [Observation]
        public void then_started_dotsure_message_should_be_sent_monitoring()
        {
            _metadata = new LeadTransferDotsureTaskMetadata("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", Guid.Empty);
            _message = LeadTransferStarted.WithChannelId("Started transferring lead for Dotsure", Guid.Empty, ((LeadTransferDotsureTaskMetadata)_metadata).ToString());

            var consumer = new StartedLeadTransferConsumer(_router.Object, _executor.Object, _monitorLeadTransfer, _monitoringDto);
            consumer.Consume(_message);

            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeNull();
            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeEmpty();

            _dispatcher.Verify(p => p.Dispatch(It.IsAny<MonitoringDto>()));
        }

        [Observation]
        public void then_started_oakhurst_message_should_be_sent_monitoring()
        {
            _metadata = new LeadTransferOakhurstTaskMetadata("1", "2", "3", "4", "5", "6", "7", "8", "9", Guid.Empty, "6");
            _message = LeadTransferStarted.WithChannelId("Started transferring lead for Oakhurst", Guid.Empty, ((LeadTransferOakhurstTaskMetadata)_metadata).ToString());

            var consumer = new StartedLeadTransferConsumer(_router.Object, _executor.Object, _monitorLeadTransfer, _monitoringDto);
            consumer.Consume(_message);

            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeNull();
            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeEmpty();

            _dispatcher.Verify(p => p.Dispatch(It.IsAny<MonitoringDto>()));
        }

        [Observation]
        public void then_started_telesure_message_should_be_sent_monitoring()
        {
            _metadata = new LeadTransferTelesureTaskMetadata("10", Guid.Empty,  new LeadTransferTelesureTaskMetadataInfo("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"));
            _message = LeadTransferStarted.WithChannelId("Started transferring lead for Telesure", Guid.Empty, ((LeadTransferTelesureTaskMetadata)_metadata).ToString());

            var consumer = new StartedLeadTransferConsumer(_router.Object, _executor.Object, _monitorLeadTransfer, _monitoringDto);
            consumer.Consume(_message);

            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeNull();
            _message.GetMetadata<LeadTransferStartedMetadata>().Parameters.ShouldNotBeEmpty();

            _dispatcher.Verify(p => p.Dispatch(It.IsAny<MonitoringDto>()));
        }
    }
}