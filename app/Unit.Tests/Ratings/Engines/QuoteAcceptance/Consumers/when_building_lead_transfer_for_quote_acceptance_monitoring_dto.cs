﻿using System;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure.Factories;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata.Shared;
using Domain.QuoteAcceptance.Monitoring;
using iPlatform.Api.DTOs.Monitoring;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;
using Xunit.Extensions;

namespace Unit.Tests.Ratings.Engines.QuoteAcceptance.Consumers
{
    public class when_building_lead_transfer_for_quote_acceptance_monitoring_dto : Specification
    {
        private readonly IBuildMonitoringDto<IWorkflowExecutionMessage> _monitoringDto;
        private IWorkflowExecutionMessage _message;
        private ITaskMetadata _metadata;
        private MonitoringDto _dto;

        public when_building_lead_transfer_for_quote_acceptance_monitoring_dto()
        {
            _monitoringDto = new ConsumerWorkflowMonitoringFactory();
        }

        public override void Observe()
        {
           
        }

        [Observation]
        public void then_exception_monitoring_dto_should_be_built_correctly()
        {
            var channelId = Guid.NewGuid();
            const string exceptionMessage = "Its an exception";
            _metadata = new LeadTransferExceptionMetadata(exceptionMessage,"This is an exception", "Unit Tests", channelId);
            _message = new LeadTransferException(_metadata);
            _dto = _monitoringDto.Exception(_message);

            _dto.ShouldNotBeNull();
            _dto.ChannelId.ShouldEqual(channelId);
            _dto.Category.ShouldEqual(LeadCategory.LeadTransfer.Name());
            _dto.Result.ShouldEqual(exceptionMessage);
        }

        [Observation]
        public void then_failure_monitoring_dto_should_be_built_correclty()
        {
            var channelId = Guid.NewGuid();
            const string result = "Its a failure";
            //const string error = "This is the failure error";
            _metadata = new LeadTransferFailedMetadata(result, channelId);
            _message = new LeadTransferFailed(_metadata);
            _dto = _monitoringDto.Faliure(_message);

            _dto.ShouldNotBeNull();
            _dto.ChannelId.ShouldEqual(channelId);
            _dto.Category.ShouldEqual(LeadCategory.LeadTransfer.Name());
            _dto.Result.ShouldEqual(result);
        }

        [Observation]
        public void then_finished_monitoring_dto_should_be_built_correclty()
        {
            var channelId = Guid.NewGuid();
            const string message = "Its finished";
            _metadata = new LeadTransferFinishedMetadata(message, channelId);
            _message = new LeadTransferFinished(_metadata);
            _dto = _monitoringDto.Finished(_message);
            _dto.ShouldNotBeNull();
            _dto.ChannelId.ShouldEqual(channelId);
            _dto.Category.ShouldEqual(LeadCategory.LeadTransfer.Name());
            _dto.Result.ShouldEqual(message);

        }

        [Observation]
        public void then_successful_monitoring_dto_should_be_built_correclty()
        {
            var channelId = Guid.NewGuid();
            const string message = "Its started";
            _metadata = new LeadTransferSuccessfulMetadata(message, channelId);
            _message = new LeadTransferSuccessful(_metadata);
            _dto = _monitoringDto.Successful(_message);
            _dto.ShouldNotBeNull();
            _dto.ChannelId.ShouldEqual(channelId);
            _dto.Category.ShouldEqual(LeadCategory.LeadTransfer.Name());
            _dto.Result.ShouldEqual(message);
        }
    }
}
