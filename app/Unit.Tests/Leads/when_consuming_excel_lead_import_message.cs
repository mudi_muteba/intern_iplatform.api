﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Installers;
using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Imports.Leads;
using Domain.Imports.Leads.Dtos;
using Domain.Imports.Leads.Handlers;
using Domain.Imports.Leads.Workflow.Messages;
using Domain.Imports.Leads.Workflow.Metadata;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Excel;
using iPlatform.Api.DTOs.Party.ContactDetail;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Moq;
using TestHelper;
using TestHelper.Stubs;
using TestObjects.Mothers.Lead;
using Workflow.Messages;
using Xunit.Extensions;
using MasterData;

namespace Unit.Tests.Leads
{
    /// <summary>
    /// How fix System.InvalidOperationExceptionThe 'Microsoft.ACE.OLEDB.12.0' provider is not registered on the local machine
    /// if tests fail:
    /// 
    /// Install Microsoft Access Database Engine 2010 Redistributable - https://www.microsoft.com/en-us/download/details.aspx?id=13255
    /// 
    /// Note: If you are running 32-bit version of SQL then your issue will be solved by installing above, else 
    /// 
    /// Start -> Run -> AccessDatabaseEngine_x64.exe /passive
    /// </summary>
    public class when_consuming_excel_lead_import_message : BaseExecutionPlanTest
    {
        readonly FakeBus _fakeBus = new FakeBus();
        readonly ImportLeadExcelHandler _handler;
        private readonly IEnumerable<ExcelLeadRecord> _excelLeadRecords;
        private readonly IEnumerable<IWorkflowExecutionMessage> _workflowExecutionMessages;
        private readonly IEnumerable<ImportLeadDto> _leads;
        public when_consuming_excel_lead_import_message()
            : base(ApiUserObjectMother.AsAdmin())
        {
            Container.Register(Component.For<IRepository>().Instance(new Mock<IRepository>().Object));
            Container.Install(new QueryInstaller());

            var router = new WorkflowRouter(new WorkflowBus(_fakeBus));
            _handler = new ImportLeadExcelHandler(new DefaultContextProvider(), router);

            _excelLeadRecords = _handler.ExcelLeadRecords(ExcelImportMetadataMother.ValidFileMetadata);
            _leads = _handler.LeadDtos(ExcelImportMetadataMother.ValidFileMetadata, _excelLeadRecords);
            _workflowExecutionMessages = _handler.WorkflowExecutionMessages(_leads);
        }

        public override void Observe()
        {
            _handler.Handle(new LeadImportExcelDto(Guid.NewGuid(), "ValidLeads.xlsx", "Description", 3, 2, 1, LeadExcelFileMother.ValidFile));
        }

        //[Observation(Skip = "reviewing")]
        public void should_publish_message()
        {
            _fakeBus.PublishWasCalled.ShouldBeTrue();
            _fakeBus.MessagesPublished.Count().ShouldEqual(1);
            _fakeBus.MessageTypesPublished.First().Key.ToString().ShouldEqual(typeof(WorkflowRoutingMessage).ToString());
            _fakeBus.MessageTypesPublished.First().Value.ShouldBeType<int>();
        }

        //[Observation(Skip = "reviewing")]
        public void should_create_workflow_routing_message()
        {
            var routingMessage = _handler.WorkflowRoutingMessage(ExcelImportMetadataMother.ValidFileMetadata, _excelLeadRecords, _leads);
            routingMessage.ExecutionPlan.ExecutionMessages.Count().ShouldEqual(18);
            routingMessage.ExecutionPlan.ExecutionMessages.First().ShouldBeType<CreateIndividualUploadHeaderMessage>();
            routingMessage.ExecutionPlan.ExecutionMessages.Last().ShouldBeType<CreateLeadMessage>();
        }

       // [Observation(Skip = "reviewing")]
        public void should_create_header_message()
        {
            var headerMessage = _handler.IndividualUploadHeaderMessage(ExcelImportMetadataMother.ValidFileMetadata, _excelLeadRecords, _workflowExecutionMessages);
            headerMessage.ExecutionPlan.ExecutionMessages.Count().ShouldEqual(0);
            headerMessage.ShouldBeType<CreateIndividualUploadHeaderMessage>();
            headerMessage.WorkflowExecutionMessageType.ShouldEqual(WorkflowExecutionMessageType.Default);
            headerMessage.Metadata.ShouldBeType<CreateIndividualUploadHeaderMetadata>();
            var metadata = headerMessage.Metadata as CreateIndividualUploadHeaderMetadata;
            metadata.ChannelId.ShouldEqual(2);
            metadata.CampaignId.ShouldEqual(3);
            metadata.UserId.ShouldEqual(1);
            metadata.DuplicateCount.ShouldEqual(0);
            metadata.DuplicateFileRecordCount.ShouldEqual(2);
            metadata.FailureCount.ShouldBeNull();
            metadata.FileName.ShouldEqual("ValidLeads.xlsx");
            metadata.FileRecordCount.ShouldEqual(22);
            metadata.LeadImportReference.ShouldNotEqual(new Guid());
            metadata.NewCount.ShouldEqual(17);
            metadata.Status.ShouldEqual(LeadImportStatuses.Submitted);
        }

        //[Observation(Skip = "reviewing")]
        public void should_create_execution_messages()
        {
            _workflowExecutionMessages.Count().ShouldEqual(17);
            _workflowExecutionMessages.First().ShouldBeType<CreateLeadMessage>();
            var metadata = _workflowExecutionMessages.First().Metadata as CreateLeadMetadata;
            metadata.CreateIndividualDto.ShouldBeType<CreateIndividualDto>();
            metadata.CreateIndividualDto.ChannelId.ShouldEqual(2);
            metadata.CreateIndividualDto.ContactDetail.ShouldBeType<CreateContactDetailDto>();
            metadata.CreateIndividualDto.ContactDetail.Cell.ShouldEqual("0821234567");
            metadata.CreateIndividualDto.DisplayName.ShouldEqual("John, Doe");
            metadata.CreateIndividualDto.FirstName.ShouldEqual("John");
            metadata.CreateIndividualDto.Surname.ShouldEqual("Doe");
        }

       // [Observation(Skip = "reviewing")]
        public void should_contain_success_execution_messages()
        {
            _workflowExecutionMessages.First().ExecutionPlan.SuccessMessages.Count().ShouldEqual(1);

            foreach (var message in _workflowExecutionMessages)
            {
                message.ShouldBeType<CreateLeadMessage>();
                message.ExecutionPlan.SuccessMessages.Count().ShouldEqual(1);
                var successMsg = message.ExecutionPlan.SuccessMessages.First();
                successMsg.ShouldBeType<CreateIndividualUploadDetailMessage>();
                var successMetadata = successMsg.Metadata as CreateIndividualUploadDetailMetadata;
                successMetadata.CampaignId.ShouldEqual(3);
                successMetadata.ChannelId.ShouldEqual(2);
                successMetadata.UserId.ShouldEqual(1);
                successMetadata.FileName.ShouldEqual("ValidLeads.xlsx");
                successMetadata.LeadImportReference.ShouldNotEqual(new Guid());

                if (successMetadata.ContactNumber.Equals("0821234567"))
                    successMetadata.DuplicateFileRecordCount.ShouldEqual(4);
                else if (successMetadata.ContactNumber.Equals("0821234582"))
                    successMetadata.DuplicateFileRecordCount.ShouldEqual(3);
                else
                    successMetadata.DuplicateFileRecordCount.ShouldEqual(0);
            }
        }

       // [Observation(Skip = "reviewing")]
        public void should_get_duplicates()
        {
            var dups = _handler.DuplicatesExcelLeadRecords(_excelLeadRecords);
            dups.Count.ShouldEqual(2);
            dups.First().Key.ShouldEqual("0821234567");
            dups.First().Value.ShouldEqual(4);
            dups.Last().Key.ShouldEqual("0821234582");
            dups.Last().Value.ShouldEqual(3);
        }

       // [Observation(Skip = "reviewing")]
        public void should_extract_excel_records()
        {
            _excelLeadRecords.Count().ShouldEqual(22);
            var excelLeadRecord = _excelLeadRecords.FirstOrDefault();
            excelLeadRecord.ShouldBeType<ExcelLeadRecord>();
            excelLeadRecord.Firstname.ShouldEqual("John");
            excelLeadRecord.Surname.ShouldEqual("Doe");
            excelLeadRecord.ContactNumber.ShouldEqual("0821234567");
        }

       // [Observation(Skip = "reviewing")]
        public void should_map_extract_excel_records_to_dtos()
        {
            var dtos = _handler.LeadDtos(ExcelImportMetadataMother.ValidFileMetadata, _excelLeadRecords);
            dtos.Count().ShouldEqual(17);

            var dup1 = dtos.FirstOrDefault(x => x.InsuredInfo.ContactNumber.Equals("0821234567"));
            dup1.ChannelId.ShouldEqual(2);
            dup1.CampaignId.ShouldEqual(3);
            dup1.UserId.ShouldEqual(1);
            dup1.FileName.ShouldEqual("ValidLeads.xlsx");
            dup1.DuplicateFileRecordCount.ShouldEqual(4);

            var dup2 = dtos.FirstOrDefault(x => x.InsuredInfo.ContactNumber.Equals("0821234582"));
            dup2.ChannelId.ShouldEqual(2);
            dup2.CampaignId.ShouldEqual(3);
            dup2.UserId.ShouldEqual(1);
            dup2.FileName.ShouldEqual("ValidLeads.xlsx");
            dup2.DuplicateFileRecordCount.ShouldEqual(3);

            var insuredInfoDto = dtos.FirstOrDefault().InsuredInfo;
            insuredInfoDto.ShouldBeType<InsuredInfoDto>();
            insuredInfoDto.Firstname.ShouldEqual("John");
            insuredInfoDto.Surname.ShouldEqual("Doe");
            insuredInfoDto.ContactNumber.ShouldEqual("0821234567");
        }

       // [Observation(Skip = "reviewing")]
        public void should_log_error_when_file_has_no_data()
        {
            var records = _handler.ExcelLeadRecords(ExcelImportMetadataMother.EmptyFileMetadata);
            records.ShouldBeEmpty();
        }
    }
}