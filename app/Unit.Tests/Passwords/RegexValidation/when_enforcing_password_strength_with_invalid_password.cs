﻿using Domain.Admin;
using Xunit.Extensions;

namespace Unit.Tests.Passwords.RegexValidation
{
    public class when_enforcing_password_strength_with_invalid_password : Specification
    {
        public override void Observe()
        {
            
        }

        [Observation]
        public void should_return_regex_mismatch_if_password_is_empty()
        {
            CheckPasswordStrength("");
        }

        [Observation]
        public void should_return_regex_mismatch_if_password_missing_alpha_characters()
        {
            CheckPasswordStrength("12345678");
        }

        [Observation]
        public void should_return_regex_mismatch_if_password_missing_numeric_characters()
        {
            CheckPasswordStrength("abcdefgh");
        }

        [Observation]
        public void should_return_regex_mismatch_if_password_missing_special_characters()
        {
            CheckPasswordStrength("Aa123456");
        }

        [Observation]
        public void should_return_regex_mismatch_if_password_missing_lowercase_characters()
        {
            CheckPasswordStrength("AA123456");
        }

        [Observation]
        public void should_return_regex_mismatch_if_password_missing_uppercase_characters()
        {
            CheckPasswordStrength("aa123456");
        }

        [Observation]
        public void should_return_regex_mismatch_if_password_is_too_short()
        {
            CheckPasswordStrength("Aa1*");
        }

        private static void CheckPasswordStrength(string password)
        {
            new Channel().IsStrongPassword(password).ShouldBeFalse();
        }
    }
}