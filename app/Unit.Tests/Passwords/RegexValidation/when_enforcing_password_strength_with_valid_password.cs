using Domain.Admin;
using Xunit.Extensions;

namespace Unit.Tests.Passwords.RegexValidation
{
    public class when_enforcing_password_strength_with_valid_password : Specification
    {
        public override void Observe()
        {

        }

        [Observation]
        public void should_return_regex_mismatch_if_password_is_empty()
        {
            new Channel().IsStrongPassword("Aa1*5678").ShouldBeTrue();
        }
    }
}