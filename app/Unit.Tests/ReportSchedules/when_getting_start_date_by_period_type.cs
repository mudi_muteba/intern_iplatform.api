﻿using System;
using Domain.ReportSchedules.Extensions;
using iPlatform.Enums.ReportSchedules;
using Xunit.Extensions;

namespace Unit.Tests.ReportSchedules
{
    public class when_getting_start_date_by_period_type : Specification
    {
        readonly DateTime _endDate = new DateTime(2017, 02, 8);

        public override void Observe() { }

        [Observation] 
        public void should_get_beginning_of_day()
        {
            ReportSchedulePeriodType.CurrentDay.GetStartDate(DateTime.Now).ShouldEqual(DateTime.Today);
        }

        [Observation]
        public void should_get_beginning_of_week()
        {
            ReportSchedulePeriodType.CurrentWeek.GetStartDate(_endDate).ShouldEqual(DateTime.Parse("2017/02/06 12:00:00 AM"));
        }

        [Observation]
        public void should_get_beginning_of_month()
        {
            ReportSchedulePeriodType.CurrentMonth.GetStartDate(_endDate).ShouldEqual(DateTime.Parse("2017/02/01 12:00:00 AM"));
        }

        [Observation]
        public void should_get_beginning_of_year()
        {
            ReportSchedulePeriodType.CurrentYear.GetStartDate(_endDate).ShouldEqual(DateTime.Parse("2017/01/01 12:00:00 AM"));
        }

        [Observation]
        public void should_get_previous_month()
        {
            ReportSchedulePeriodType.PreviousMonth.GetStartDate(_endDate).ShouldEqual(DateTime.Parse("2017/01/08 12:00:00 AM"));
        }

        [Observation]
        public void should_get_previous_3_month()
        {
            ReportSchedulePeriodType.Previous3Months.GetStartDate(_endDate).ShouldEqual(DateTime.Parse("2016/11/08 12:00:00 AM"));
        }
    }
}