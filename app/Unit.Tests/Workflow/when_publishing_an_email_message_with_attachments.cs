﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

using Castle.Windsor;

using Domain.Emailing;

using Workflow.Engine.Installers;
using Workflow.Messages;

using Xunit.Extensions;
using ToolBox.Communication.Interfaces;
using TestObjects.Mothers.EmailSetting;

namespace Unit.Tests.Workflow
{
    public class when_publishing_an_email_message_with_attachments : Specification, IDisposable
    {
        private IWorkflowRouter _router;

        public when_publishing_an_email_message_with_attachments()
        {
            var container = new WindsorContainer().Install(new WorkflowInstaller(), new ConsumerInstaller());
            _router = container.Resolve<IWorkflowRouter>();
            container.Release(_router);
            var message = new Dictionary<string, string>
            {
                {"FirstName", "Benjamin"},
                {"BrokerName", "iPlatform"},
                {"PhoneNumber", "0844485820" },
                {"QuoteExpiration", "30"}
            };

            var attachment = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Assets\blue.png");

            var attachments = new List<Attachment>();
                attachments.Add(new Attachment(attachment));

            var resources = new List<EmbeddedResource>();

            var embeddedResource = new EmbeddedResource();
                embeddedResource.Attribute = "information";
                embeddedResource.Value = @"<img src=""cid:{0}"" title=""Embedded Resource"" />";
                embeddedResource.LinkedResource = new LinkedResource(attachment);

            var embeddedResources = new List<EmbeddedResource>();
                embeddedResources.Add(embeddedResource);

            var communicationMessageData = new CommunicationMessageData(message);

            var emailCommunicationMessage = new EmailCommunicationMessage(
                @"Emailing\EmailTemplateQuoteDefault",
                communicationMessageData,
                "benjamin@iplatform.co.za",
                "Attachment Test",
                attachments,
                embeddedResources,
                NewEmailSettingDtoObjectMother.ValidEmailSetting()
            );

            var routingMessage = new WorkflowRoutingMessage();
                routingMessage.AddMessage(new EmailWorkflowExecutionMessage(emailCommunicationMessage));

            _router.Publish(routingMessage);
        }
        
        public override void Observe()
        {
            
        }

        //[Observation]
        public void then_test()
        {
            true.ShouldBeTrue();
        }

        public void Dispose()
        {

        }
    }

}