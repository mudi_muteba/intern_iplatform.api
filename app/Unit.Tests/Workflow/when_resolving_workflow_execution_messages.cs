﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Workflow;
using iPlatform.Enums.Workflows;
using TestHelper;
using Workflow.Messages;
using Xunit.Extensions;

namespace Unit.Tests.Workflow
{
    public class when_resolving_workflow_execution_messages : BaseExecutionPlanTest
    {
        private IEnumerable<IWorkflowExecutionMessage> _messages;
        public when_resolving_workflow_execution_messages() : base(ApiUserObjectMother.AsAdmin()) { }

        public override void Observe()
        {
            _messages = GetMessages().Where(x => x != null);
        }

        private IEnumerable<IWorkflowExecutionMessage> GetMessages()
        {
            foreach (WorkflowMessageType taskType in Enum.GetValues(typeof (WorkflowMessageType)))
                yield return Mapper.Map<WorkflowMessageType, WorkflowExecutionMessage>(taskType);
        }

        /// <summary>
        /// This test ensures that every assignable WorkflowTaskType decorates only one WorkflowExecutionMessage
        /// </summary>
        //[Observation]
        public void should_resolve_all_WorkflowTaskTypes()
        {
            var taskTypes = Enum.GetValues(typeof(WorkflowMessageType));
            _messages.Count().ShouldEqual(taskTypes.Length);
        }

        /// <summary>
        /// This test ensures that every WorkflowExecutionMessage is decorated with the WorkflowExecutionMessageTypeAttribute
        /// </summary>
        //[Observation(Skip = "This test should be manually run to determine which WorkflowExecutionMessages need to be decorated")]
        public void all_workflow_execution_messages_are_decorated()
        {
            var decoratedWorkflowExecutionMessages = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                from type in assembly.GetTypes()
                from a in type.GetCustomAttributes(typeof (WorkflowExecutionMessageTypeAttribute), false)
                select type);

            var workflowExecutionMessages = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                from type in assembly.GetTypes()
                where type.BaseType == typeof (WorkflowExecutionMessage)
                select type);

            decoratedWorkflowExecutionMessages.Count().ShouldEqual(workflowExecutionMessages.Count());
        }
    }
}