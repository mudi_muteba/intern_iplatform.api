﻿using System.Collections.Generic;
using System.Linq;

using Castle.MicroKernel.Registration;

using Domain.Admin;
using Domain.Admin.ChannelEvents;
using Domain.Admin.Channels.SMSCommunicationSettings;

using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Workflow;

using Domain.Party.Quotes.Events;

using iPlatform.Enums.Workflows;

using Moq;

using TestHelper;
using TestHelper.Stubs;

using Workflow.Messages;

using Xunit.Extensions;

namespace Unit.Tests.Workflow
{
    public class when_publishing_event_workflow_messages : BaseExecutionPlanTest
    {
        FakeBus _fakeBus = new FakeBus();
        private readonly EventWorkflowMessagePublisher _publisher;
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private QuoteDistributionViaSMSRequestedEvent _event;
        private IEnumerable<IWorkflowExecutionMessage> _messages;

        public when_publishing_event_workflow_messages() : base(ApiUserObjectMother.AsAdmin())
        {
            _repository.Setup(r => r.GetAll<SMSCommunicationSetting>(It.IsAny<ExecutionContext>()))
                .Returns(() => new List<SMSCommunicationSetting>{new SMSCommunicationSetting()}.AsQueryable());

            Container.Register(Component.For<IRepository>().Instance(_repository.Object));

            _publisher = new EventWorkflowMessagePublisher(new WorkflowRouter(new WorkflowBus(_fakeBus)));
        }

        public override void Observe()
        {
            var audit = new EventAudit(new EventAuditUser(1, "UserName"));
            var quotes = new List<DistributedQuoteEvent>(new[] { new DistributedQuoteEvent("InsurerCode", "InsurerName", "ProductCode", "ProductName", 10m), });
            var channel = new Channel(1001);
            var channelEvent = new ChannelEvent(channel, typeof(QuoteDistributionViaSMSRequestedEvent).Name);
            channelEvent.SetTask(WorkflowMessageType.Sms);
            channel.AddChannelConfiguration(new[] { channelEvent, });
            _event = new QuoteDistributionViaSMSRequestedEvent(1001, 1002, "ExternalReference", "0821234567", quotes, audit, channel);
            _messages = _publisher.GetWorkflowTasks(_event).ToList();
        }

        //[Observation]
        public void should_return_all_workflow_execution_messages()
        {
            _messages.Count().ShouldEqual(1);
        }

        //[Observation]
        public void should_publish_routing_message()
        {
            _publisher.Publish(_event);
            _fakeBus.PublishWasCalled.ShouldBeTrue();
            _fakeBus.MessagesPublished.Count().ShouldEqual(1);
            _fakeBus.MessageTypesPublished.First().Key.ToString().ShouldEqual(typeof(WorkflowRoutingMessage).ToString());
            _fakeBus.MessageTypesPublished.First().Value.ShouldBeType<int>();
        }
    }
}