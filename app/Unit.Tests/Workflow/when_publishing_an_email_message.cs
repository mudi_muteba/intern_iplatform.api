﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Castle.Windsor;
using Domain.Emailing;
using Workflow.Engine.Installers;
using Workflow.Messages;
using Xunit.Extensions;
using TestObjects.Mothers.EmailSetting;

namespace Unit.Tests.Workflow
{
    public class when_publishing_an_email_message : Specification, IDisposable
    {
        private IWorkflowRouter _router;

        public when_publishing_an_email_message()
        {
            var container = new WindsorContainer().Install(new WorkflowInstaller(), new ConsumerInstaller());
            _router = container.Resolve<IWorkflowRouter>();
            container.Release(_router);
            var message = new Dictionary<string, string>
            {
                {"UserName", "brian@customapp.co.za"},
                {"ResetUrl", ConfigurationManager.AppSettings["iPlatform/web/resepatPasswordUrl"] + Guid.NewGuid()}
            };
            var communicationMessageData = new CommunicationMessageData(message);
            var emailCommunicationMessage = new EmailCommunicationMessage(@"Emailing\PasswordResetRequestTemplate", communicationMessageData, "brian@customapp.co.za", "Password reset", NewEmailSettingDtoObjectMother.ValidEmailSetting());

            var routingMessage = new WorkflowRoutingMessage();
            routingMessage.AddMessage(new EmailWorkflowExecutionMessage(emailCommunicationMessage));

            _router.Publish(routingMessage);
            //_router.PublishDelayed(routingMessage, TimeSpan.FromSeconds(0));
        }
        
        public override void Observe()
        {
            
        }

        //[Observation]
        public void then_test()
        {
            true.ShouldBeTrue();
        }

        public void Dispose()
        {

        }
    }

}