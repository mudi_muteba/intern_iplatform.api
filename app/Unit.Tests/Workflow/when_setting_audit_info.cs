﻿using Domain.Base.Extentions;
using Domain.Emailing;
using Domain.Escalations.Workflow.Messages;
using Xunit.Extensions;

namespace Unit.Tests.Workflow
{
    public class when_setting_audit_info : Specification
    {
        public override void Observe() { }

        [Observation]
        public void should_set_audit_properties()
        {
            var message1 = new EmailWorkflowExecutionMessage();
            var message2 = new CreateEscalationWorkflowMessage();
            message2.AddMessage(message1);
            var message3 = new EditEscalationWorkflowMessage().AddMessage(message2);
            WorkflowExtensions.SetAuditInfo(new [] { message3 }, 1, "test", "1,2,3", 2);

            message3.UserId.ShouldEqual(1);
            message3.UserName.ShouldEqual("test");
            message3.ChannelIds.ShouldEqual("1,2,3");
            message3.ActiveChannelId.ShouldEqual(2);

            message2.UserId.ShouldEqual(1);
            message2.UserName.ShouldEqual("test");
            message2.ChannelIds.ShouldEqual("1,2,3");
            message2.ActiveChannelId.ShouldEqual(2);

            message1.UserId.ShouldEqual(1);
            message1.UserName.ShouldEqual("test");
            message1.ChannelIds.ShouldEqual("1,2,3");
            message1.ActiveChannelId.ShouldEqual(2);
        }
    }
}