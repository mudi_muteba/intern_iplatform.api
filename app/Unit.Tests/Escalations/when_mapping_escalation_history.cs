﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using Domain.Escalations;
using Domain.Users;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Enums.Escalations;
using iPlatform.Enums.Workflows;
using TestHelper.Installers;
using Xunit.Extensions;

namespace Unit.Tests.Escalations
{
    public class when_mapping_escalation_history : Specification
    {
        private EscalationHistoryDto _item;
        private PagedResultDto<EscalationHistoryDto> _list;
        public when_mapping_escalation_history()
        {
            DomainMarkerMapperConfiguration.Configure();
        }

        public override void Observe()
        {
            var escalationPlan = new EscalationPlan("Plan 1", EventType.LeadCallBack.ToString());
            var escalationPlanStep = new EscalationPlanStep(escalationPlan, "Step 1", 1, DelayType.Interval, IntervalType.Days, 365, DateTime.UtcNow);
            var escalationPlanStepWorkflowMessage = new EscalationPlanStepWorkflowMessage(escalationPlanStep, WorkflowMessageType.Sms.ToString());
            var escalationPlanExecutionHistory = new EscalationPlanExecutionHistory(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(),
                escalationPlanStep, escalationPlanStepWorkflowMessage, new User(1), EscalationWorkflowMessageStatus.Complete);
            _item = Mapper.Map<EscalationPlanExecutionHistory, EscalationHistoryDto>(escalationPlanExecutionHistory);
            var pagedResults = new PagedResults<EscalationPlanExecutionHistory>
            {
                PageNumber = 1,
                Results = new List<EscalationPlanExecutionHistory> {escalationPlanExecutionHistory},
                TotalCount = 1,
                TotalPages = 1
            };
            _list = Mapper.Map<PagedResults<EscalationPlanExecutionHistory>, PagedResultDto<EscalationHistoryDto>>(pagedResults);
        }

        [Observation]
        public void should_map_item()
        {
            _item.UserId.ShouldEqual(1);
            _item.EscalationPlanStepWorkflowMessageId.ShouldEqual(0);
            _item.EscalationWorkflowMessageStatus.ShouldEqual(EscalationWorkflowMessageStatus.Complete);
        }

        [Observation]
        public void should_map_list()
        {
            _list.PageNumber.ShouldEqual(1);
            _list.TotalCount.ShouldEqual(1);
            _list.TotalPages.ShouldEqual(1);
            _list.Results.FirstOrDefault().UserId.ShouldEqual(1);
            _list.Results.FirstOrDefault().EscalationPlanStepWorkflowMessageId.ShouldEqual(0);
            _list.Results.FirstOrDefault().EscalationWorkflowMessageStatus.ShouldEqual(EscalationWorkflowMessageStatus.Complete);
        }
    }
}