﻿using System;
using Domain.Base.Extentions;
using Domain.Escalations.Workflow.Messages;
using iPlatform.Enums.Escalations;
using iPlatform.Enums.Workflows;
using Xunit.Extensions;

namespace Unit.Tests.Extensions
{
    public class when_printing_property_names_values : Specification
    {
        public override void Observe() { }

        [Observation]
        public void should_print_all_names_and_values()
        {
           new CreateEscalationWorkflowMessage
                {
                    ChannelId = 1001,
                    DateTimeDelay = DateTime.Now,
                    IntervalType = IntervalType.Days,
                    DelayType = DelayType.Interval,
                    EventType = EventType.LeadCallBack + "",
                    IntervalDelay = 2,
                    JsonData = "{ IsTest: true }",
                    UserId = 101
                }.PrintProperties().ShouldNotBeEmpty();
        }
    }
}