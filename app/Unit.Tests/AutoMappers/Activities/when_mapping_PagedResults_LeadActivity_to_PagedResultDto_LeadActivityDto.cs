﻿using System;
using System.Collections.Generic;
using Api.Installers;
using AutoMapper;
using Castle.Windsor;
using Domain.Activities;
using Domain.Base;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Leads;
using Domain.Users;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using MasterData;
using Xunit.Extensions;

namespace Unit.Tests.AutoMappers.Activities
{
    public class when_mapping_PagedResults_LeadActivity_to_PagedResultDto_LeadActivityDto : Specification
    {
        private PagedResultDto<LeadActivityDto> _destination;

        public when_mapping_PagedResults_LeadActivity_to_PagedResultDto_LeadActivityDto()
        {
            AutoMapperConfiguration.Configure();
            new WindsorContainer().Install(new AutoMapperInstaller());
        }

        public override void Observe()
        {
            var leadActivity = new LeadActivity
            {
                Id = 1, 
                ActivityType = ActivityTypes.Created, 
                DateCreated = DateTime.UtcNow, 
                DateUpdated = DateTime.UtcNow,
                Campaign = new Campaign { Id = 2 },
                User = new User(),
                Lead = new Lead(),
                Party = new Domain.Party.Party(),
                CampaignSourceId = 1,
                GetImpliedStatus = new LeadStatus(),
                IsDeleted = false,
                SimpleAudit = new EntityAudit()
            };
            var pagedResults = new PagedResults<LeadActivity> { PageNumber = 1, TotalCount = 1, TotalPages = 1, Results = new List<LeadActivity> { leadActivity} };
            _destination = Mapper.Map<PagedResults<LeadActivity>, PagedResultDto<LeadActivityDto>>(pagedResults);
        }

        [Observation]
        public void should_map()
        {
            _destination.PageNumber.ShouldEqual(1);
            _destination.TotalCount.ShouldEqual(1);
            _destination.TotalPages.ShouldEqual(1);
            _destination.Results.Count.ShouldEqual(1);
        }
    }
}