﻿using System;
using System.Collections.Generic;
using Api.Installers;
using AutoMapper;
using Castle.Windsor;
using Domain.Activities;
using Domain.Base;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Leads;
using Domain.Party.ProposalDefinitions;
using Domain.Products;
using Domain.Users;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData;
using Xunit.Extensions;

namespace Unit.Tests.AutoMappers.Activities
{
    public class when_mapping_ProposalDefinition_to_ListProposalDefinition : Specification
    {
        private ListProposalDefinitionDto _destination;

        public when_mapping_ProposalDefinition_to_ListProposalDefinition()
        {
            AutoMapperConfiguration.Configure();
            new WindsorContainer().Install(new AutoMapperInstaller());
        }

        public override void Observe()
        {
            var prod = new Product();
            prod.CoverDefinitions.Add(new CoverDefinition
            {
                Cover = Covers.MotorWarranty,
                DisplayName = "This is a Test Warranty",
                Product = new Product(68, "ProductTest")
            });

            var leadActivity = new ProposalDefinition
            {
                Id = 1,             
                User = new User(),
                Party = new Domain.Party.Party(),
                IsDeleted = false,
                SimpleAudit = new EntityAudit(),
                Cover = Covers.MotorWarranty,
                Product = prod
            };

            _destination = Mapper.Map<ProposalDefinition, ListProposalDefinitionDto>(leadActivity);
        }

        [Observation]
        public void should_map()
        {
            _destination.Cover.Id.ShouldEqual(Covers.MotorWarranty.Id);
            _destination.CoverDefinition.Product.Id.ShouldEqual(68);
        }

    }
}