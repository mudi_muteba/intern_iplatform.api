﻿using Api.Installers;
using AutoMapper;
using Xunit.Extensions;

namespace Unit.Tests.AutoMappers
{
    public class TestAllMappings : Specification
    {
        public TestAllMappings()
        {
            AutoMapperConfiguration.Configure();
        }

        public override void Observe()
        {
        }

        [Observation]
        public void the_mappings_are_valid()
        {
            //Mapper.AssertConfigurationIsValid();
        }
    }
}