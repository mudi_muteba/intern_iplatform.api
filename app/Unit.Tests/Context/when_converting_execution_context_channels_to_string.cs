﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Xunit.Extensions;

namespace Unit.Tests.Context
{
    public class when_converting_execution_context_channels_to_string : Specification
    {
        public override void Observe() { }

        [Observation]
        public void should_convert()
        {
            var context = new ExecutionContext("", 1, "", new List<int> {1, 2, 3, 4, 5, 6}, 1, null);
            context.ChannelIds.ShouldEqual("1,2,3,4,5,6");
        }
    }
}