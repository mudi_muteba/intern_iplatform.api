﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using Castle.MicroKernel.Registration;

using Domain.Admin;
using Domain.Admin.Channels.SMSCommunicationSettings;
using Domain.Admin.Channels.SMSCommunicationSettings.Queries;

using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;

using Domain.Party.Quotes.Events;
using Domain.Party.Quotes.Workflow.Messages;
using Domain.Party.Quotes.Workflow.Tasks;

using Moq;

using TestHelper;

using Xunit.Extensions;

namespace Unit.Tests.Party.Quotes.Workflow
{
    public class when_mapping_sms_quote_message : BaseExecutionPlanTest
    {
        private SMSQuoteMessage _message;
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private readonly Mock<GetSMSCommunicationSettingsQuery> _query = new Mock<GetSMSCommunicationSettingsQuery>();

        public when_mapping_sms_quote_message() : base(ApiUserObjectMother.AsAdmin())
        {
            var channelId = Guid.NewGuid();
            var communicationSetting = new SMSCommunicationSetting { Id = 1001, Channel = new Channel(1001) { SystemId = channelId }, IsDeleted = false, SMSProviderName = "SMSProviderName", SMSUrl = "http://SMSUrl", SMSUserName = "SMSUserName", SMSPassword = "SMSPassword" };
            
            _repository.Setup(r => r.GetAll<SMSCommunicationSetting>(It.IsAny<ExecutionContext>())).Returns(() => new List<SMSCommunicationSetting> { communicationSetting }.AsQueryable());
            _repository.Setup(r => r.GetAll<Channel>(It.IsAny<ExecutionContext>())).Returns(() => new List<Channel> { new Channel(1001){SystemId = channelId} }.AsQueryable());
            _query.Setup(x => x.Execute(null)).Returns(new []{communicationSetting}.AsQueryable());

            Container.Register(Component.For<IRepository>().Instance(_repository.Object));
            Container.Register(Component.For<GetSMSCommunicationSettingsQuery>().Instance(_query.Object));
        }

        public override void Observe()
        {
            var audit = new EventAudit(new EventAuditUser(1, "UserName"));
            var quote1 = new DistributedQuoteEvent("InsurerCode1", "InsurerName1", "ProductCode1", "ProductName1", 10m);
            var quote2 = new DistributedQuoteEvent("InsurerCode2", "InsurerName2", "ProductCode2", "ProductName2", 20m);
            var quotes = new List<DistributedQuoteEvent>(new[] { quote1, quote2 });
            var @event = new QuoteDistributionViaSMSRequestedEvent(1001, 1002, "ExternalReference", "0821234567", quotes, audit, new Channel(1001));

            _message = Mapper.Map<QuoteDistributionViaSMSRequestedEvent, SMSQuoteMessage>(@event);
        }

        [Observation]
        public void should_map_meta_data()
        {
            var metaData = _message.Metadata as DistributeQuoteViaSMSTaskMetadata;

            metaData.QuoteId.ShouldEqual(1002);
            metaData.CellPhoneNumber.ShouldEqual("0821234567");
            metaData.ExternalReference.ShouldEqual("ExternalReference");

            metaData.Quotes.Count().ShouldEqual(2);
            metaData.Quotes.ElementAt(0).InsurerCode.ShouldEqual("InsurerCode1");
            metaData.Quotes.ElementAt(0).InsurerName.ShouldEqual("InsurerName1");
            metaData.Quotes.ElementAt(0).ProductCode.ShouldEqual("ProductCode1");
            metaData.Quotes.ElementAt(0).ProductName.ShouldEqual("ProductName1");
            metaData.Quotes.ElementAt(0).Premium.ShouldEqual(10m);
            metaData.Quotes.ElementAt(1).InsurerCode.ShouldEqual("InsurerCode2");
            metaData.Quotes.ElementAt(1).InsurerName.ShouldEqual("InsurerName2");
            metaData.Quotes.ElementAt(1).ProductCode.ShouldEqual("ProductCode2");
            metaData.Quotes.ElementAt(1).ProductName.ShouldEqual("ProductName2");
            metaData.Quotes.ElementAt(1).Premium.ShouldEqual(20m);

            metaData.Providers.Count().ShouldEqual(1);
            metaData.Providers.ElementAt(0).ProviderName.ShouldEqual("SMSProviderName");
            metaData.Providers.ElementAt(0).Url.ShouldEqual("http://SMSUrl");
            metaData.Providers.ElementAt(0).UserName.ShouldEqual("SMSUserName");
            metaData.Providers.ElementAt(0).Password.ShouldEqual("SMSPassword");
        }
    }
}