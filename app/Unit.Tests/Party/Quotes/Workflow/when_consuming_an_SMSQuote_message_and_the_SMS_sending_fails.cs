﻿using System;
using System.Collections.Generic;
using Domain.Party.Quotes.Events;
using Domain.Party.Quotes.Workflow.Consumers;
using Domain.Party.Quotes.Workflow.Messages;
using Domain.Party.Quotes.Workflow.Tasks;
using iPlatform.OutboundCommunication.SMS;
using Moq;
using Unit.Tests.Party.Quotes.Workflow.Stubs;
using Workflow.Messages;
using Xunit.Extensions;

namespace Unit.Tests.Party.Quotes.Workflow
{
    public class when_consuming_an_SMSQuote_message_and_the_SMS_sending_fails : Specification
    {
        private readonly string cellPhoneNumber = "0825469926";
        private readonly SMSQuoteMessageConsumer consumer;
        private readonly Mock<IWorkflowExecutor> executor = new Mock<IWorkflowExecutor>();
        private readonly string externalReference = "external_reference";
        private readonly SMSQuoteMessage message;
        private readonly List<SMSProviderConfiguration> providers = new List<SMSProviderConfiguration>();
        private readonly int quoteId = 1;
        private readonly List<DistributedQuoteEvent> quotes = new List<DistributedQuoteEvent>();
        private readonly Mock<IWorkflowRouter> router = new Mock<IWorkflowRouter>();
        private Exception actualException;

        public when_consuming_an_SMSQuote_message_and_the_SMS_sending_fails()
        {
            var factory = new StubSMSSendingFactory(new FailingSMSSender());

            providers.Add(new SMSProviderConfiguration("CellFind", "username", "password", "url"));
            quotes.Add(new DistributedQuoteEvent("INS_1", "INSURER 1", "PRODUCT_1", "Product 1", 9.12m));

            var metadata = new DistributeQuoteViaSMSTaskMetadata(providers
                , quoteId
                , externalReference
                , cellPhoneNumber
                , quotes);

            message = new SMSQuoteMessage(metadata);
            consumer = new SMSQuoteMessageConsumer(router.Object, executor.Object, factory);
        }

        public override void Observe()
        {
            try
            {
                consumer.Consume(message);
            }
            catch (Exception e)
            {
                actualException = e;
            }
        }

        [Observation]
        public void an_exception_is_thrown()
        {
            actualException.ShouldNotBeNull();
        }
    }
}