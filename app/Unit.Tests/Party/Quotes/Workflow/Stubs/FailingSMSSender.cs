using System;
using iPlatform.OutboundCommunication.SMS;

namespace Unit.Tests.Party.Quotes.Workflow.Stubs
{
    public class FailingSMSSender : ISendSMS
    {
        public SMSSendingResult Send(SMSProviderConfiguration configuration, SMSMessage message)
        {
            throw new Exception();
        }
    }
}