using System.Collections.Generic;
using iPlatform.OutboundCommunication.SMS;

namespace Unit.Tests.Party.Quotes.Workflow.Stubs
{
    public class StubSMSSendingFactory : ICreateSMSSenders
    {
        private readonly ISendSMS sender;
        private readonly Dictionary<string, ISendSMS> senderMapping = new Dictionary<string, ISendSMS>();

        public StubSMSSendingFactory(ISendSMS sender)
        {
            this.sender = sender;
        }

        public StubSMSSendingFactory()
        {
        }

        public StubSMSSendingFactory AddSender(string providerName, ISendSMS sender)
        {
            senderMapping.Add(providerName, sender);
            return this;
        }

        public ISendSMS Create(SMSProviderConfiguration provider)
        {
            if(sender != null)
                return sender;

            return senderMapping[provider.ProviderName];
        }
    }
}