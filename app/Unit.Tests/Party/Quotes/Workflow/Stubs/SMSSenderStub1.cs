using iPlatform.OutboundCommunication.SMS;

namespace Unit.Tests.Party.Quotes.Workflow.Stubs
{
    public class SMSSenderStub1 : ISendSMS
    {
        public SMSMessage Message { get; private set; }

        public SMSProviderConfiguration Configuration { get; private set; }

        public bool WasCalled { get; private set; }

        public int NumberOfTimesCalled { get; private set; }

        public SMSSendingResult Send(SMSProviderConfiguration configuration, SMSMessage message)
        {
            WasCalled = true;
            NumberOfTimesCalled++;

            this.Configuration = configuration;
            this.Message = message;

            return new SMSSendingResult(true, "sender_1", "Ok");
        }
    }
}