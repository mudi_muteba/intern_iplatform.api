﻿using System.Collections.Generic;
using Domain.Party.Quotes.Events;
using Domain.Party.Quotes.Workflow.Consumers;
using Domain.Party.Quotes.Workflow.Messages;
using Domain.Party.Quotes.Workflow.Tasks;
using iPlatform.OutboundCommunication.SMS;
using Moq;
using Unit.Tests.Party.Quotes.Workflow.Stubs;
using Workflow.Messages;
using Xunit.Extensions;

namespace Unit.Tests.Party.Quotes.Workflow
{
    public class when_consuming_an_SMSQuote_message_with_no_quotes_associated : Specification
    {
        private readonly string cellPhoneNumber = "0825469926";
        private readonly SMSQuoteMessageConsumer consumer;
        private readonly Mock<IWorkflowExecutor> executor = new Mock<IWorkflowExecutor>();
        private readonly string externalReference = "external_reference";
        private readonly SMSQuoteMessage message;
        private readonly DistributeQuoteViaSMSTaskMetadata metadata;
        private readonly List<SMSProviderConfiguration> providers = new List<SMSProviderConfiguration>();
        private readonly int quoteId = 1;
        private readonly List<DistributedQuoteEvent> quotes = new List<DistributedQuoteEvent>();
        private readonly Mock<IWorkflowRouter> router = new Mock<IWorkflowRouter>();
        private readonly ISendSMS smsSender = new SMSSenderStub1();

        public when_consuming_an_SMSQuote_message_with_no_quotes_associated()
        {
            var factory = new StubSMSSendingFactory(smsSender);

            providers.Add(new SMSProviderConfiguration("CellFind", "username", "password", "url"));

            metadata = new DistributeQuoteViaSMSTaskMetadata(providers
                , quoteId
                , externalReference
                , cellPhoneNumber
                , quotes);

            message = new SMSQuoteMessage(metadata);

            consumer = new SMSQuoteMessageConsumer(router.Object, executor.Object, factory);
        }

        public override void Observe()
        {
            consumer.Consume(message);
        }

        [Observation]
        public void no_SMS_messages_are_sent()
        {
            var sender = smsSender as SMSSenderStub1;

            sender.NumberOfTimesCalled.ShouldEqual(0);
        }
    }
}