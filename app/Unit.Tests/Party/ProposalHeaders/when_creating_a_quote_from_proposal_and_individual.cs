﻿using Castle.MicroKernel.Registration;
using Domain.Base.Execution;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Quoting;
using Moq;
using TestHelper;
using TestObjects.Mothers.Proposals;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Unit.Tests.Party.ProposalHeaders
{
    public class when_creating_a_quote_from_proposal_and_individual : BaseExecutionPlanTest
    {
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private HandlerResult<QuoteHeaderDto> _result;

        public when_creating_a_quote_from_proposal_and_individual()
            : base(ApiUserObjectMother.AsAdmin())
        {
            Container.Register(Component.For<IRepository>().Instance(_repository.Object));
        }

        public override void Observe()
        {
            var quoteDto = CreateQuoteDtoObjectMother.ForNewQuote();
            _result = ExecutionPlan.Execute<CreateQuoteDto, QuoteHeaderDto>(quoteDto);
        }

        [Observation]
        public void then_handler_should_be_executed()
        {
            _result.ShouldNotBeNull();
        }
    }
}
