### **Important:** ###

Due to legacy COM support required for Bank Account Validation, when you clone and try to build iplaform.api (and ibroker.api), you will receive errors if you don't run the following command  on both x86 and x64 versions of PowerShell:

* Set-ExecutionPolicy -Scope CurrentUser RemoteSigned

**Paths:**

* %SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe
* %SystemRoot%\SysWOW64\WindowsPowerShell\v1.0\powershell.exe